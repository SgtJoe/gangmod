#pragma once

#include "Common.h"
#include "CampRaidLaw.h"

class CampRaidPinkertons : public CampRaidLaw
{
	Vehicle SpawnedWagon = 0;
	int WagonSpawnIndex = 0;

	bool WagonArrived = false;
	bool WagonFiring = false;

	std::vector<Vector3> WagonDestinations;

	void SpawnFirstWave() override;
	void SpawnSecondWave() override;
	void SpawnFinalWave() override;

	void SpawnWagon();

	void WagonLogic();

	bool SetMissionParamsForCamp(int camp) override;

	void CommonInit() override;

public:
	CampRaidPinkertons();
	~CampRaidPinkertons();

	void Update() override;

	static const MissionStaticData& GetStaticMissionData();
};
