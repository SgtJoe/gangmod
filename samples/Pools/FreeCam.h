#pragma once

#include "Common.h"
#include "script.h"
#include "keyboard.h"

class FreeCam
{
	const float DefaultFieldOfView = 70.0f;

	int _camHandle = 0;

	Vector3 _camPos = {0, 0, 0};
	Vector3 _camRot = {0, 0, 0};
	float _camFOV = DefaultFieldOfView;

	bool _enabled = false;

	void setCamState(bool on);

	void resetCam();

public:
	inline void ToggleEnabledState() { _enabled = !_enabled; }

	inline bool GetEnabled() const { return _enabled; }

	void HandleCam();
};

