#pragma once

#include "Common.h"
#include "MissionBase.h"

class JailbreakTown : public MissionBase
{
	const std::string PrisonerModel = "G_M_M_UNICRIMINALS_01";
	
	struct CellInfo
	{
		Hash DoorHash;
		Vector3 BlipCoords;
		std::vector<Vector3> PrisonerSpawns;
		std::vector<Ped> SpawnedPrisoners;
		Blip SpawnedBlip;
		bool DoorOpen = false;
	};
	
	struct TownInfo
	{
		Vector3 JailCenter;
		LocationData JailerSpawn;
		std::vector<LocationData> GuardSpawns;
		std::vector<CellInfo> JailCells;
		std::vector<Vector3> BackupSpawns;
		std::vector<Hash> ForceUnlockDoors;
		Hash TownScriptHash;
		int TownRegion;
		int TownDistrict;
		LocationData StartLoc;
	};
	
	const TownInfo ValentineTownInfo =
	{
		{-276.51f, 808.29f, 118.38f},
		{{-277.91f, 804.93f, 118.38f}, 298},
		{
			{{-276.08f, 805.63f, 118.38f}, 131},
			{{-277.83f, 807.57f, 118.38f}, 279},
			{{-275.27f, 801.65f, 118.40f}, 190},
			{{-269.52f, 809.59f, 118.23f}, 274}
		},
		{
			{193903155, {-272.76f, 808.13f, 118.37f}, {{-271.16f, 807.12f, 118.37f}, {-273.75f, 807.03f, 118.37f}}},
			{295355979, {-272.91f, 810.00f, 118.37f}, {{-274.60f, 810.59f, 118.37f}, {-272.82f, 811.74f, 118.37f}}}
		},
		{{-246.23f, 710.68f, 112.34f}, {-343.93f, 773.20f, 114.60f}},
		{395506985, 1988748538},
		MISC::GET_HASH_KEY("valentine"),
		REGION_HRT_VALENTINE,
		DISTRICT_HEARTLAND,
		{{-287.14f, 743.30f, 116.63f}, 14}
	};

	const TownInfo SaintDenisTownInfo =
	{
		{2502.08f, -1308.83f, 47.95f},
		{{2506.67f, -1308.79f, 47.95f}, 94},
		{
			{{2517.25f, -1307.05f, 47.95f}, 269},
			{{2517.20f, -1310.63f, 47.92f}, 264},
			{{2511.55f, -1322.18f, 47.47f}, 270},
			{{2494.37f, -1313.12f, 47.95f}, 310},
			{{2507.54f, -1301.81f, 47.95f}, 180},
			{{2478.01f, -1319.80f, 47.87f}, 278},
			{{2479.37f, -1316.10f, 47.87f}, 237},
			{{2485.24f, -1303.10f, 47.87f}, 188}
		},
		{
			{2515591150, {2503.10f, -1309.15f, 47.95f}, {{2501.79f, -1310.86f, 47.95f}}},
			{1711767580, {2502.97f, -1308.39f, 47.95f}, {{2504.31f, -1307.26f, 47.95f}}},
			{1995743734, {2499.22f, -1309.39f, 47.95f}, {{2497.70f, -1310.65f, 47.95f}}},
			{3365520707, {2499.04f, -1308.34f, 47.95f}, {{2500.43f, -1307.13f, 47.95f}}},
		},
		{{2497.46f, -1247.91f, 48.17f}, {2558.60f, -1262.90f, 49.61f}, {2511.29f, -1370.21f, 45.48f}},
		{},
		MISC::GET_HASH_KEY("saintdenis"),
		REGION_BAY_SAINT_DENIS,
		DISTRICT_BAYOU_NWA,
		{{2524.90f, -1272.30f, 48.14f}, 169}
	};

	const TownInfo ArmadilloTownInfo =
	{
		{-3621.39f, -2603.27f, -14.34f},
		{{-3625.13f, -2601.99f, -14.34f}, 210},
		{
			{{-3622.46f, -2606.87f, -14.34f}, 113},
			{{-3621.08f, -2604.27f, -14.34f}, 114},
			{{-3626.12f, -2603.73f, -14.34f}, 108},
			{{-3625.28f, -2606.46f, -14.35f}, 115},
			{{-3625.60f, -2599.19f, -14.75f}, 24}
		},
		{
			{4016307508, {-3621.23f, -2600.95f, -14.34f}, {{-3619.20f, -2601.63f, -14.34f}, {-3617.52f, -2600.30f, -14.34f}}},
			{4235597664, {-3619.48f, -2604.96f, -14.34f}, {{-3616.91f, -2604.96f, -12.63f}, {-3617.89f, -2605.63f, -14.34f}}}
		},
		{{-3687.52f, -2595.61f, -14.85f}, {-3673.69f, -2628.15f, -14.75f}},
		{},
		MISC::GET_HASH_KEY("armadillo"),
		REGION_CHO_ARMADILLO,
		DISTRICT_RIO_BRAVO,
		{{-3670.61f, -2612.20f, -15.08f}, 277}
	};

	TownInfo CurrentTownInfo = {};

	Ped ActiveJailer = 0;
	std::vector<Ped> ActiveGuards;
	std::vector<CellInfo> ActiveJailCells;

	int PrisonerCasualties = 0;
	std::vector<Ped> ActivePrisoners;

	bool GuardsAlerted = false;
	bool GuardsCombat = false;
	int GuardSpotTime = 0;

	int BlipSetTimer = 0;

	void SetUpJailGuard(Ped guard);

	bool SetMissionParamsForCamp(int camp) override;

	void CommonInit() override;
	void TrackRewards() override;

	bool ActivateObjective_0 = true;
	bool ActivateObjective_1 = false;
	bool ActivateObjective_2 = false;
	bool ActivateObjective_3 = false;

	bool RunObjective_0 = false;
	bool RunObjective_1 = false;
	bool RunObjective_2 = false;
	bool RunObjective_3 = false;

	void ObjectiveInit_0();
	void ObjectiveInit_1();
	void ObjectiveInit_2();
	void ObjectiveInit_3();

	void ObjectiveLogic_0();
	void ObjectiveLogic_1();
	void ObjectiveLogic_2();
	void ObjectiveLogic_3();

public:
	JailbreakTown();
	~JailbreakTown();

	void Setup(int campid, const std::string& gangstr, const std::vector<MissionCompanionInfo>& companions) override;

	void Update() override;

	static const MissionStaticData& GetStaticMissionData();
};
