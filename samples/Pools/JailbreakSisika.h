#pragma once

#include "Common.h"
#include "MissionBase.h"

class JailbreakSisika : public MissionBase
{
	bool SetMissionParamsForCamp(int camp) override;

	void CommonInit() override;
	void TrackRewards() override;

	const Vector3 IslandCenter = {3267.54f, -625.40f, 41.51f};
	const float IslandRadius = 350.0f;

	const Vector3 PrisonCenter = {3363.76f, -681.45f, 45.47f};
	const float PrisonRadius = 60.0f;

	const Vector3 BridgeCoords = {3323.13f, -611.13f, 42.10f};
	const float BridgeRadius = 35.0f;

	const Vector3 YardCoords = {3345.79f, -687.26f, 43.06f};
	const float YardRadius = 17.0f;

	const Vector3 RegroupCoords = {3108.90f, -494.54f, 40.23f};
	const float RegroupRadius = 50.0f;

	const std::string UniformGuardModel = "S_M_M_SKPGUARD_01";
	const std::string MercGuardModel = "A_M_M_JAMESONGUARD_01";
	const std::string PrisonerModel = "A_M_M_SKPPRISONER_01";

	const std::string CannonAnim = "both_push_cannon_arthur";
	const std::string CannonDict = "script_story@smg2@ig@ig3_cannon@ig3_cannon_p1";

	void ClearIsland();

	void ApplyGuardHat(Ped ped);
	void ApplyGuardUniformRemoved(Ped ped);
	void ApplyGuardUniformToPlayer();

	bool ReadyToDisguise = false;
	bool PlayerDisguised = false;

	bool StealthActive = true;
	bool StealthEscorting = false;
	bool StealthPassed = false;

	bool SpawnLoudPeds = false;

	enum class SusState
	{
		Idle,
		Patrolling,
		Confronting
	};

	struct SusGuardData
	{
		Ped ped;
		SusState state = SusState::Idle;
		std::vector<Vector3> patrolPath;
		int patrolProgress = -1;
		int updateTimer = 0;
		int stateTimer = 0;
	};

	std::vector<SusGuardData> ActiveSusGuards;

	Ped SpawnGuard(const Vector3& pos, float rot, PedType weapons, bool sus);

	Ped SpawnReinforcement(const Vector3& pos, float rot, PedType weapons, bool defensive, bool merc = false, bool mounted = false);

	Ped ConfrontingGuard = 0;

	void StealthLogic();
	void StealthBroken();

	void SetAllowSprinting(bool allow);

	int AlarmTimer = 0;
	bool AlarmPlaying = false;

	void SetAlarm(bool on);

	int GenericTimer = 0;

	std::vector<Vehicle> GangBoats;

	std::vector<Ped> SpawnedGuardsNeutral;
	std::vector<Ped> SpawnedGuardsSus;
	std::vector<Ped> SpawnedGuardsAllGlobal;

	std::vector<Ped> SpawnedPrisonersOutside;
	std::vector<Ped> SpawnedPrisonersInside;

	Ped WatchtowerGuard = 0;
	Ped Milliken = 0;
	Ped MillikenPartner = 0;
	Ped GateGuard = 0;
	Ped CourtyardGuard = 0;
	Ped Warden = 0;

	std::vector<Ped> EscortConfronters;
	std::vector<Ped> BridgeDefenders;
	std::vector<Ped> Horses;
	std::vector<Ped> Boatmen;

	Vehicle GuardBoat = 0;

	Blip ObjectiveBlip = 0;

	Vehicle Cannon = 0;

	const Vector3 CannonPos = {3334.39f, -628.45f, 43.30f};
	const float CannonRot = 225;

	enum class CannonStatus
	{
		IDLE,
		TURNING,
		FINISHED
	};

	CannonStatus CurrentCannonStatus = CannonStatus::IDLE;

	Prompt CannonPrompt = 0;

	void SpawnOutsidePeds();
	void RemoveOutsidePeds();
	void SpawnInsidePeds();

	void SetFrontGate(bool unlocked, bool forceOpened = false);

	void TeleportAndFinish();

	void Objective0_Stealth_Sentry_Init();
	void Objective0_Stealth_Sentry_Run();
	void Objective1_Stealth_Treeline_Init();
	void Objective1_Stealth_Treeline_Run();
	void Objective2_Stealth_Disguise_Init();
	void Objective2_Stealth_Disguise_Run();
	void Objective3_Stealth_Bridge_Init();
	void Objective3_Stealth_Bridge_Run();
	void Objective4_Stealth_Courtyard_Init();
	void Objective4_Stealth_Courtyard_Run();
	void Objective5_Stealth_Escort_Init();
	void Objective5_Stealth_Escort_Run();

	void Objective6_Loud_Bridge_Init();
	void Objective6_Loud_Bridge_Run();
	void Objective7_Loud_Cannon_Init();
	void Objective7_Loud_Cannon_Run();
	void Objective8_Loud_Courtyard_Init();
	void Objective8_Loud_Courtyard_Run();
	void Objective9_Loud_Regroup_Init();
	void Objective9_Loud_Regroup_Run();
	void Objective10_Loud_Defend_Init();
	void Objective10_Loud_Defend_Run();

	int ObjectiveIndexStealth = 0;
	int ObjectiveIndexLoud = -1;

	std::function<void()> StealthObjectives[12] = {
		[&]() { Objective0_Stealth_Sentry_Init(); },
		[&]() { Objective0_Stealth_Sentry_Run(); },
		[&]() { Objective1_Stealth_Treeline_Init(); },
		[&]() { Objective1_Stealth_Treeline_Run(); },
		[&]() { Objective2_Stealth_Disguise_Init(); },
		[&]() { Objective2_Stealth_Disguise_Run(); },
		[&]() { Objective3_Stealth_Bridge_Init(); },
		[&]() { Objective3_Stealth_Bridge_Run(); },
		[&]() { Objective4_Stealth_Courtyard_Init(); },
		[&]() { Objective4_Stealth_Courtyard_Run(); },
		[&]() { Objective5_Stealth_Escort_Init(); },
		[&]() { Objective5_Stealth_Escort_Run(); }
	};

	std::function<void()> LoudObjectives[10] = {
		[&]() { Objective6_Loud_Bridge_Init(); },
		[&]() { Objective6_Loud_Bridge_Run(); },
		[&]() { Objective7_Loud_Cannon_Init(); },
		[&]() { Objective7_Loud_Cannon_Run(); },
		[&]() { Objective8_Loud_Courtyard_Init(); },
		[&]() { Objective8_Loud_Courtyard_Run(); },
		[&]() { Objective9_Loud_Regroup_Init(); },
		[&]() { Objective9_Loud_Regroup_Run(); },
		[&]() { Objective10_Loud_Defend_Init(); },
		[&]() { Objective10_Loud_Defend_Run(); }
	};

public:
	JailbreakSisika();
	~JailbreakSisika();

	void Setup(int campid, const std::string& gangstr, const std::vector<MissionCompanionInfo>& companions) override;

	void Update() override;

	static const MissionStaticData& GetStaticMissionData();
};
