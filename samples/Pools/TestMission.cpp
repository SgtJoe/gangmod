
#include "precomp.h"
#include "TestMission.h"

const MissionStaticData& TestMission::GetStaticMissionData()
{
	static MissionStaticData missiondata = {MissionType::TESTMISSION, 0, 0, 0, 0, 2, "Test Mission 1", "Quick and simple test mission.", COMPCOUNT_MIN, true, MUSIC_SEAN_RESCUE, false};
	return missiondata;
}

bool TestMission::SetMissionParamsForCamp(int camp)
{
	switch (camp)
	{
		case DISTRICT_GRIZZLIES_WEST:
			DutchCoords = {-1016.76f, 1681.21f, 240.62f};
			HideCoords = {-1019.01f, 1669.82f, 238.43f};
			EnemySpawn1 = {-1013.25f, 1682.86f, 240.52f};
			EnemyHeading1 = 117;
			EnemySpawn2 = {-1023.56f, 1700.86f, 248.04f};
			EnemyHeading2 = 196;
			return true;
	}

	DioTrace(DIOTAG1("ERROR") "CANNOT SET MISSION %d PARAMS FOR CAMP %d - ABORTING MISSION", TestMission::GetStaticMissionData().StaticMissionID, camp);
	return false;
}

void TestMission::CommonInit()
{
	const MissionStaticData& thisMissionData = TestMission::GetStaticMissionData();
	SetVarsFromStaticData(thisMissionData);

	MissionAssetsToLoad = {
		{"PED", "CS_dutch"},
		{"PED", "CS_MicahBell"}
	};

	ObjectivesList = {
		"Go to the ~COLOR_YELLOW~area",
		"Save the ~COLOR_YELLOW~hostage",
		"Eliminate the ~COLOR_RED~enemies"
	};

	LoadAllMissionAssets();
	SpawnGangMembers(thisMissionData.NumMembersToSpawn);

	WAIT(2000);
}

TestMission::TestMission()
{
	DioScope(DIOTAG);
}

TestMission::~TestMission()
{
	DioScope(DIOTAG);
}

void TestMission::Setup(int campid, const std::string& gangstr, const std::vector<MissionCompanionInfo>& companions)
{
	DioScope(DIOTAG "campid %d gangstr %s", campid, gangstr.c_str());
	
	GangName = gangstr;
	GangMemberTemplates = companions;

	CommonInit();
	
	if (!SetMissionParamsForCamp(campid))
	{
		ExitMissionFlag = M_EXIT_ERROR_TERMINATE;
		return;
	}

	Common::ClearArea(DutchCoords, 50);

	dutch = SpawnPed("CS_dutch", DutchCoords, 131, -1);
	PED::SET_PED_RELATIONSHIP_GROUP_HASH(dutch, REL_IGNORE_EVERYTHING);
	TASK::TASK_COWER(dutch, -1, 0, 0);
	PED::SET_PED_KEEP_TASK(dutch, true);
	ENTITY::SET_ENTITY_HEALTH(dutch, 75, 0);
	PED::SET_PED_CONFIG_FLAG(dutch, PCF_CanBeAttackedByFriendlyPed, true);
	PED::SET_PED_CONFIG_FLAG(dutch, PCF_DisableDeadEyeTagging, true);

	micahs.clear();
	micahs.emplace_back(SpawnPed("CS_MicahBell", Common::ApplyRandomSmallOffsetToCoords(EnemySpawn1), EnemyHeading1, -1));

	Ped micah = micahs[0];
	WEAPON::GIVE_WEAPON_TO_PED(micah, WEAPON_REVOLVER_CATTLEMAN, 6, false, true, WEAPON_ATTACH_POINT_PISTOL_R, false, 0.5f, 1.0f, 752097756, true, 0, false);
	PED::SET_PED_ACCURACY(micah, 40);
	PED::SET_PED_COMBAT_MOVEMENT(micah, 0);
	ENTITY::SET_ENTITY_HEALTH(micah, 100, 0);
	PED::SET_PED_RELATIONSHIP_GROUP_HASH(micah, REL_IGNORE_EVERYTHING);
}

void TestMission::ObjectiveInit_0()
{
	ObjectiveSet(0);
	ActivateObjective_0 = false;
	RunObjective_0 = true;

	AreaBlip = ObjectiveBlipCoords(DutchCoords, 35, true, ObjectivesList[0]);

#ifdef DEBUG_FAST_MISSION
	ENTITY::_SET_ENTITY_COORDS_AND_HEADING(PlayerPed, -1007.02f, 1639.41f, 238.43f, 13, true, true, true);
	ENTITY::PLACE_ENTITY_ON_GROUND_PROPERLY(PlayerPed, true);
	CAM::SET_GAMEPLAY_CAM_RELATIVE_HEADING(0, 1);
	CAM::SET_GAMEPLAY_CAM_RELATIVE_PITCH(0, 1);
#endif
}

void TestMission::ObjectiveLogic_0()
{
	if (Common::GetDistBetweenCoords(PlayerCoords, DutchCoords) < 30)
	{
		MAP::REMOVE_BLIP(&AreaBlip);

		RunObjective_0 = false;
		ActivateObjective_1 = true;
	}
}

void TestMission::ObjectiveInit_1()
{
	ObjectiveSet(1);
	ActivateObjective_1 = false;
	RunObjective_1 = true;

	PED::SET_PED_RELATIONSHIP_GROUP_HASH(dutch, REL_PLAYER_ALLY);
	TASK::TASK_COWER(dutch, -1, 0, 0);

	Blip dutchblip = MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_COMPANION, dutch);
	MAP::BLIP_ADD_MODIFIER(dutchblip, BLIP_MODIFIER_COMPANION_OBJECTIVE);
	MAP::SET_BLIP_SPRITE(dutchblip, MISC::GET_HASH_KEY("blip_camp_request"), true);
	MAP::_SET_BLIP_NAME_FROM_PLAYER_STRING(dutchblip, "Protect");

	for (int i = 0; i < micahs.size(); i++)
	{
		Ped micah = micahs[i];

		MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_ENEMY, micah);

		PED::SET_PED_RELATIONSHIP_GROUP_HASH(micah, REL_PLAYER_ENEMY);

		WEAPON::SET_CURRENT_PED_WEAPON(micah, WEAPON::GET_BEST_PED_WEAPON(micah, false, false), true, 0, false, false);

		TASK::TASK_COMBAT_HATED_TARGETS(micah, 20);
	}

	CompanionsBeginCombat();

	CurrentMusicStage = STAGE_ACTION1;
}

void TestMission::ObjectiveLogic_1()
{
	bool alldead = true;

	for (int i = 0; i < micahs.size(); i++)
	{
		if (!PED::IS_PED_DEAD_OR_DYING(micahs[i], true))
		{
			alldead = false;
		}
	}

	if (alldead)
	{
		RunObjective_1 = false;
		ActivateObjective_2 = true;
	}
}

void TestMission::ObjectiveInit_2()
{
	ObjectiveSet(2);
	ActivateObjective_2 = false;
	RunObjective_2 = true;

	int sequence = 0;
	TASK::OPEN_SEQUENCE_TASK(&sequence);
	TASK::TASK_PAUSE(0, 500);
	TASK::TASK_FOLLOW_NAV_MESH_TO_COORD(0, HideCoords.x, HideCoords.y, HideCoords.z, 3.0f, -1, 0.5f, 4194304, 40000.0f);
	TASK::TASK_COWER(0, -1, 0, 0);
	TASK::CLOSE_SEQUENCE_TASK(sequence);
	TASK::TASK_PERFORM_SEQUENCE(dutch, sequence);
	TASK::CLEAR_SEQUENCE_TASK(&sequence);

	micahs.clear();
	micahs.emplace_back(SpawnPed("CS_MicahBell", Common::ApplyRandomSmallOffsetToCoords(EnemySpawn2), EnemyHeading2, -1));
	micahs.emplace_back(SpawnPed("CS_MicahBell", Common::ApplyRandomSmallOffsetToCoords(EnemySpawn2), EnemyHeading2, -1));
	micahs.emplace_back(SpawnPed("CS_MicahBell", Common::ApplyRandomSmallOffsetToCoords(EnemySpawn2), EnemyHeading2, -1));

	for (int i = 0; i < micahs.size(); i++)
	{
		Ped micah = micahs[i];

		Blip micahblip = MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_ENEMY, micah);
		MAP::SET_BLIP_SPRITE(micahblip, MISC::GET_HASH_KEY("blip_ambient_gang_leader"), true);
		MAP::_SET_BLIP_NAME_FROM_PLAYER_STRING(micahblip, "Eliminate");

		WEAPON::GIVE_WEAPON_TO_PED(micah, WEAPON_REVOLVER_CATTLEMAN, 6, false, true, WEAPON_ATTACH_POINT_PISTOL_R, false, 0.5f, 1.0f, 752097756, true, 0, false);
		WEAPON::SET_PED_INFINITE_AMMO(micah, true, WEAPON_REVOLVER_CATTLEMAN);
		PED::SET_PED_ACCURACY(micah, 40);
		ENTITY::SET_ENTITY_HEALTH(micah, 100, 0);

		PED::SET_PED_COMBAT_MOVEMENT(micah, 3);
		PED::SET_PED_COMBAT_RANGE(micah, CR_NEAR);

		PED::SET_PED_RELATIONSHIP_GROUP_HASH(micah, REL_PLAYER_ENEMY);

		TASK::TASK_COMBAT_HATED_TARGETS(micah, 50);
	}

	CompanionsBeginCombat();

	CurrentMusicStage = STAGE_ACTION2;
}

void TestMission::ObjectiveLogic_2()
{
	bool alldead = true;

	for (int i = 0; i < micahs.size(); i++)
	{
		if (!PED::IS_PED_DEAD_OR_DYING(micahs[i], true))
		{
			alldead = false;
		}
	}

	if (alldead)
	{
		CurrentMusicStage = STAGE_AFTERACTION;
		RunObjective_2 = false;
		MissionSuccess();
	}
}

void TestMission::TrackRewards()
{
	RewardBounty_Active = RewardBounty_Potential;
	RewardCash_Active = RewardCash_Potential;
	RewardMembers_Active = RewardMembers_Potential;
}

void TestMission::Update()
{
	if (PED::IS_PED_DEAD_OR_DYING(dutch, true) && TimeMissionFail < 0)
	{
		DioTrace("RIP Dutch");
		MissionFailStr = "The hostage has died.";
		MissionFail();
	}

	if (TimeMissionFail > 0 && MISC::GET_GAME_TIMER() - TimeMissionFail > TimeMissionFailDelay)
	{
		DioTrace("Exit Fail Triggered");

		ExitMissionFlag = M_EXIT_FAILURE;
	}
	else if (TimeMissionSuccess > 0 && MISC::GET_GAME_TIMER() - TimeMissionSuccess > TimeMissionSuccessDelay)
	{
		DioTrace("Exit Win Triggered");

		ExitMissionFlag = M_EXIT_SUCCESS;
	}
	else if (ActivateObjective_0)
	{
		ObjectiveInit_0();
	}
	else if (ActivateObjective_1)
	{
		ObjectiveInit_1();
	}
	else if (ActivateObjective_2)
	{
		ObjectiveInit_2();
	}
	else if (RunObjective_0)
	{
		ObjectiveLogic_0();
	}
	else if (RunObjective_1)
	{
		ObjectiveLogic_1();
	}
	else if (RunObjective_2)
	{
		ObjectiveLogic_2();
	}
}
