#pragma once

#include "Common.h"
#include "script.h"
#include "scriptmenu.h"
#include "keyboard.h"

#include "InventoryManager.h"
#include "GlobalVarsAccess.h"

class MissionBase
{
	std::vector<MissionCompanionInfo> _currentMissionCasualties;

	Vector3 _storyCamps[6];
	int _lastStoryCampReminder = 0;
	float _lastStoryCampReminderTriggerDistance = 0.0;

	bool _usingController = false;

	bool _dutchGang = false;

	int _companionControlKey = VK_NUMPAD5;
	std::string _companionControlKeyString;

	static const int AlwaysKillScriptsLength = 368;
	const Hash AlwaysKillScripts[AlwaysKillScriptsLength] = {
		MISC::GET_HASH_KEY("ambush_bnd_cliff1"),
		MISC::GET_HASH_KEY("ambush_bnd_ridge_ambush"),
		MISC::GET_HASH_KEY("ambush_bnd_sniper_attack"),
		MISC::GET_HASH_KEY("ambush_exc_bridge_trap"),
		MISC::GET_HASH_KEY("ambush_exc_hide_cover"),
		MISC::GET_HASH_KEY("ambush_exc_lookout_attack"),
		MISC::GET_HASH_KEY("ambush_exc_road_robbery"),
		MISC::GET_HASH_KEY("ambush_exc_scm_prec"),
		MISC::GET_HASH_KEY("ambush_exc_wagon_bomb"),
		MISC::GET_HASH_KEY("ambush_exc_wagon_turret"),
		MISC::GET_HASH_KEY("ambush_gen_night_rob"),
		MISC::GET_HASH_KEY("ambush_inb_bridge_ambush"),
		MISC::GET_HASH_KEY("ambush_inb_forest"),
		MISC::GET_HASH_KEY("ambush_inb_forest_attack"),
		MISC::GET_HASH_KEY("ambush_inb_harass"),
		MISC::GET_HASH_KEY("ambush_inb_road_attack"),
		MISC::GET_HASH_KEY("ambush_odr_bridge_ambush"),
		MISC::GET_HASH_KEY("ambush_odr_bridge_prevent"),
		MISC::GET_HASH_KEY("ambush_odr_bridge_trap"),
		MISC::GET_HASH_KEY("ambush_odr_hso"),
		MISC::GET_HASH_KEY("ambush_odr_lookout_attack"),
		MISC::GET_HASH_KEY("ambush_odr_ride_out"),
		MISC::GET_HASH_KEY("ambush_odr_road_prec"),
		MISC::GET_HASH_KEY("ambush_odr_road_robbery"),
		MISC::GET_HASH_KEY("ambush_pnk_type1"),
		MISC::GET_HASH_KEY("ambush_rnc_type1"),
		MISC::GET_HASH_KEY("ambush_sav_corner"),
		MISC::GET_HASH_KEY("ambush_sav_forest_attack"),
		MISC::GET_HASH_KEY("ambush_sav_lookout_attack"),
		MISC::GET_HASH_KEY("ambush_sav_river_ambush"),
		MISC::GET_HASH_KEY("ambush_sav_tree_line"),
		MISC::GET_HASH_KEY("beat_animal_attack"),
		MISC::GET_HASH_KEY("beat_animal_mauling"),
		MISC::GET_HASH_KEY("beat_approach"),
		MISC::GET_HASH_KEY("beat_arms_deal"),
		MISC::GET_HASH_KEY("beat_author"),
		MISC::GET_HASH_KEY("beat_bandito_breakout"),
		MISC::GET_HASH_KEY("beat_bandito_execution"),
		MISC::GET_HASH_KEY("beat_bear_trap"),
		MISC::GET_HASH_KEY("beat_boat_attack"),
		MISC::GET_HASH_KEY("beat_booby_trap"),
		MISC::GET_HASH_KEY("beat_bounty_transport"),
		MISC::GET_HASH_KEY("beat_bronte_patrol"),
		MISC::GET_HASH_KEY("beat_brontes_town_encounter"),
		MISC::GET_HASH_KEY("beat_burning_bodies"),
		MISC::GET_HASH_KEY("beat_campfire_ambush"),
		MISC::GET_HASH_KEY("beat_chain_gang"),
		MISC::GET_HASH_KEY("beat_checkpoint"),
		MISC::GET_HASH_KEY("beat_coach_robbery"),
		MISC::GET_HASH_KEY("beat_consequence"),
		MISC::GET_HASH_KEY("beat_corpse_cart"),
		MISC::GET_HASH_KEY("beat_crashed_wagon"),
		MISC::GET_HASH_KEY("beat_dark_alley_ambush"),
		MISC::GET_HASH_KEY("beat_dark_alley_bum"),
		MISC::GET_HASH_KEY("beat_dark_alley_stabber"),
		MISC::GET_HASH_KEY("beat_dead_bodies"),
		MISC::GET_HASH_KEY("beat_dead_john"),
		MISC::GET_HASH_KEY("beat_del_lobo_posse"),
		MISC::GET_HASH_KEY("beat_diagnostics"),
		MISC::GET_HASH_KEY("beat_disabled_beggar"),
		MISC::GET_HASH_KEY("beat_domestic_dispute"),
		MISC::GET_HASH_KEY("beat_drown_murder"),
		MISC::GET_HASH_KEY("beat_drunk_camp"),
		MISC::GET_HASH_KEY("beat_drunk_dueler"),
		MISC::GET_HASH_KEY("beat_duel_boaster"),
		MISC::GET_HASH_KEY("beat_duel_winner"),
		MISC::GET_HASH_KEY("beat_escort"),
		MISC::GET_HASH_KEY("beat_executions"),
		MISC::GET_HASH_KEY("beat_fleeing_family"),
		MISC::GET_HASH_KEY("beat_fleeing_trespasser"),
		MISC::GET_HASH_KEY("beat_foot_robbery"),
		MISC::GET_HASH_KEY("beat_friendly_outdoorsman"),
		MISC::GET_HASH_KEY("beat_frozen_to_death"),
		MISC::GET_HASH_KEY("beat_fussar_chase"),
		MISC::GET_HASH_KEY("beat_gang_camp_reminder"),
		MISC::GET_HASH_KEY("beat_gang_ped1_encounter"),
		MISC::GET_HASH_KEY("beat_gold_panner"),
		MISC::GET_HASH_KEY("beat_herbalist_camp"),
		MISC::GET_HASH_KEY("beat_horse_race"),
		MISC::GET_HASH_KEY("beat_hostage_rescue"),
		MISC::GET_HASH_KEY("beat_hunter"),
		MISC::GET_HASH_KEY("beat_inbred_kidnap"),
		MISC::GET_HASH_KEY("beat_injured_rider"),
		MISC::GET_HASH_KEY("beat_intimidation_tactics"),
		MISC::GET_HASH_KEY("beat_kidnap_victim"),
		MISC::GET_HASH_KEY("beat_laramie_gang_rustling"),
		MISC::GET_HASH_KEY("beat_layout_editor"),
		MISC::GET_HASH_KEY("beat_lemoyne_town_encounter"),
		MISC::GET_HASH_KEY("beat_locked_safe"),
		MISC::GET_HASH_KEY("beat_lone_prisoner"),
		MISC::GET_HASH_KEY("beat_lost_dog"),
		MISC::GET_HASH_KEY("beat_lost_drunk"),
		MISC::GET_HASH_KEY("beat_lost_friend"),
		MISC::GET_HASH_KEY("beat_lost_man"),
		MISC::GET_HASH_KEY("beat_moonshine_camp"),
		MISC::GET_HASH_KEY("beat_murder_campfire"),
		MISC::GET_HASH_KEY("beat_naked_swimmer"),
		MISC::GET_HASH_KEY("beat_odriscoll_town_encounter"),
		MISC::GET_HASH_KEY("beat_on_the_run"),
		MISC::GET_HASH_KEY("beat_outlaw_looter"),
		MISC::GET_HASH_KEY("beat_outlaw_transport"),
		MISC::GET_HASH_KEY("beat_parlor_ambush"),
		MISC::GET_HASH_KEY("beat_peeping_tom"),
		MISC::GET_HASH_KEY("beat_people_in_need_snake_bite"),
		MISC::GET_HASH_KEY("beat_pickpocket"),
		MISC::GET_HASH_KEY("beat_piss_pot"),
		MISC::GET_HASH_KEY("beat_player_camp_attack"),
		MISC::GET_HASH_KEY("beat_player_camp_stranger"),
		MISC::GET_HASH_KEY("beat_poisoned"),
		MISC::GET_HASH_KEY("beat_police_chase"),
		MISC::GET_HASH_KEY("beat_posse_breakout"),
		MISC::GET_HASH_KEY("beat_prison_wagon"),
		MISC::GET_HASH_KEY("beat_public_hanging"),
		MISC::GET_HASH_KEY("beat_rally"),
		MISC::GET_HASH_KEY("beat_rally_dispute"),
		MISC::GET_HASH_KEY("beat_rally_setup"),
		MISC::GET_HASH_KEY("beat_rat_infestation"),
		MISC::GET_HASH_KEY("beat_rifle_practice"),
		MISC::GET_HASH_KEY("beat_rowdy_drunks"),
		MISC::GET_HASH_KEY("beat_savage_aftermath"),
		MISC::GET_HASH_KEY("beat_savage_fight"),
		MISC::GET_HASH_KEY("beat_savage_wagon"),
		MISC::GET_HASH_KEY("beat_savage_warning"),
		MISC::GET_HASH_KEY("beat_sharp_shooter"),
		MISC::GET_HASH_KEY("beat_show_off"),
		MISC::GET_HASH_KEY("beat_skipping_stones"),
		MISC::GET_HASH_KEY("beat_slum_ambush"),
		MISC::GET_HASH_KEY("beat_spooked_horse"),
		MISC::GET_HASH_KEY("beat_stalking_shadows"),
		MISC::GET_HASH_KEY("beat_stranded_rider"),
		MISC::GET_HASH_KEY("beat_street_fight"),
		MISC::GET_HASH_KEY("beat_taunting"),
		MISC::GET_HASH_KEY("beat_torch_procession"),
		MISC::GET_HASH_KEY("beat_torturing_captive"),
		MISC::GET_HASH_KEY("beat_town_burial"),
		MISC::GET_HASH_KEY("beat_town_confrontation"),
		MISC::GET_HASH_KEY("beat_town_robbery"),
		MISC::GET_HASH_KEY("beat_town_trouble"),
		MISC::GET_HASH_KEY("beat_town_widow"),
		MISC::GET_HASH_KEY("beat_traffic_attack"),
		MISC::GET_HASH_KEY("beat_train_holdup"),
		MISC::GET_HASH_KEY("beat_trapped_woman"),
		MISC::GET_HASH_KEY("beat_treasure_hunter"),
		MISC::GET_HASH_KEY("beat_voice"),
		MISC::GET_HASH_KEY("beat_wagon_threat"),
		MISC::GET_HASH_KEY("beat_washed_ashore"),
		MISC::GET_HASH_KEY("beat_wealthy_couple"),
		MISC::GET_HASH_KEY("beat_wild_man"),
		MISC::GET_HASH_KEY("beat_wild_man_cave"),
		MISC::GET_HASH_KEY("beat_wilderness_hanging"),
		MISC::GET_HASH_KEY("campfire_gang"),
		MISC::GET_HASH_KEY("campfire_gang_es"),
		MISC::GET_HASH_KEY("campfire_gang_launch"),
		MISC::GET_HASH_KEY("hideout_beaverhollow"),
		MISC::GET_HASH_KEY("hideout_fortmercer"),
		MISC::GET_HASH_KEY("hideout_gaptoothbreach"),
		MISC::GET_HASH_KEY("hideout_hangingdogranch"),
		MISC::GET_HASH_KEY("hideout_hangingdogranch_es"),
		MISC::GET_HASH_KEY("hideout_shadybelle"),
		MISC::GET_HASH_KEY("hideout_sixpointcabin"),
		MISC::GET_HASH_KEY("hideout_smugglerdocks"),
		MISC::GET_HASH_KEY("hideout_solomonsfolly"),
		MISC::GET_HASH_KEY("hideout_thieveslanding"),
		MISC::GET_HASH_KEY("hideout_twinrocks"),
		MISC::GET_HASH_KEY("rcm_abigail11"),
		MISC::GET_HASH_KEY("rcm_abigail22"),
		MISC::GET_HASH_KEY("rcm_abigail31"),
		MISC::GET_HASH_KEY("rcm_beau_and_penelope10"),
		MISC::GET_HASH_KEY("rcm_beau_and_penelope11"),
		MISC::GET_HASH_KEY("rcm_beau_and_penelope12"),
		MISC::GET_HASH_KEY("rcm_beau_and_penelope20"),
		MISC::GET_HASH_KEY("rcm_beau_and_penelope21"),
		MISC::GET_HASH_KEY("rcm_beechers11"),
		MISC::GET_HASH_KEY("rcm_beechers12"),
		MISC::GET_HASH_KEY("rcm_beechers21"),
		MISC::GET_HASH_KEY("rcm_bh_bandito_mine"),
		MISC::GET_HASH_KEY("rcm_bh_bandito_shack"),
		MISC::GET_HASH_KEY("rcm_bh_blackwater_hunt"),
		MISC::GET_HASH_KEY("rcm_bh_camp_return"),
		MISC::GET_HASH_KEY("rcm_bh_laramie_sleeping"),
		MISC::GET_HASH_KEY("rcm_bh_sd_saloon"),
		MISC::GET_HASH_KEY("rcm_bh_shack_escape"),
		MISC::GET_HASH_KEY("rcm_bh_skinner_brother"),
		MISC::GET_HASH_KEY("rcm_bh_skinner_search"),
		MISC::GET_HASH_KEY("rcm_bh_wife_and_lover"),
		MISC::GET_HASH_KEY("rcm_bounty_duel1"),
		MISC::GET_HASH_KEY("rcm_bounty_exconfed1"),
		MISC::GET_HASH_KEY("rcm_bounty_rancher1"),
		MISC::GET_HASH_KEY("rcm_braithwaites01"),
		MISC::GET_HASH_KEY("rcm_calderon1"),
		MISC::GET_HASH_KEY("rcm_calderon2"),
		MISC::GET_HASH_KEY("rcm_calderon21"),
		MISC::GET_HASH_KEY("rcm_calderon22"),
		MISC::GET_HASH_KEY("rcm_callaway1"),
		MISC::GET_HASH_KEY("rcm_callaway2"),
		MISC::GET_HASH_KEY("rcm_callaway3"),
		MISC::GET_HASH_KEY("rcm_callaway12"),
		MISC::GET_HASH_KEY("rcm_chain_gang1"),
		MISC::GET_HASH_KEY("rcm_chain_gang2"),
		MISC::GET_HASH_KEY("rcm_chain_gang3"),
		MISC::GET_HASH_KEY("rcm_chain_gang4"),
		MISC::GET_HASH_KEY("rcm_chain_gang5"),
		MISC::GET_HASH_KEY("rcm_chain_gang6"),
		MISC::GET_HASH_KEY("rcm_chain_gang7"),
		MISC::GET_HASH_KEY("rcm_coach_robbery1"),
		MISC::GET_HASH_KEY("rcm_collect_cigarette_cards1"),
		MISC::GET_HASH_KEY("rcm_collect_dinosaur_bones11"),
		MISC::GET_HASH_KEY("rcm_collect_dinosaur_bones12"),
		MISC::GET_HASH_KEY("rcm_collect_exotics1"),
		MISC::GET_HASH_KEY("rcm_collect_exotics2"),
		MISC::GET_HASH_KEY("rcm_collect_exotics3"),
		MISC::GET_HASH_KEY("rcm_collect_exotics4"),
		MISC::GET_HASH_KEY("rcm_collect_exotics5"),
		MISC::GET_HASH_KEY("rcm_collect_exotics6"),
		MISC::GET_HASH_KEY("rcm_collect_rare_fish1"),
		MISC::GET_HASH_KEY("rcm_collect_rare_fish2"),
		MISC::GET_HASH_KEY("rcm_collect_rock_faces1"),
		MISC::GET_HASH_KEY("rcm_collect_rock_faces2"),
		MISC::GET_HASH_KEY("rcm_collect_taxidermy1"),
		MISC::GET_HASH_KEY("rcm_collect_taxidermy2"),
		MISC::GET_HASH_KEY("rcm_crackpot1"),
		MISC::GET_HASH_KEY("rcm_crackpot2"),
		MISC::GET_HASH_KEY("rcm_crackpot3"),
		MISC::GET_HASH_KEY("rcm_crawley1"),
		MISC::GET_HASH_KEY("rcm_creole1"),
		MISC::GET_HASH_KEY("rcm_doctors_opinion1"),
		MISC::GET_HASH_KEY("rcm_down1"),
		MISC::GET_HASH_KEY("rcm_down1_2"),
		MISC::GET_HASH_KEY("rcm_down2"),
		MISC::GET_HASH_KEY("rcm_down3"),
		MISC::GET_HASH_KEY("rcm_dusters21"),
		MISC::GET_HASH_KEY("rcm_dusters61"),
		MISC::GET_HASH_KEY("rcm_dusters62"),
		MISC::GET_HASH_KEY("rcm_dutch11"),
		MISC::GET_HASH_KEY("rcm_dutch21"),
		MISC::GET_HASH_KEY("rcm_dutch31"),
		MISC::GET_HASH_KEY("rcm_edith_down21"),
		MISC::GET_HASH_KEY("rcm_edith_down22"),
		MISC::GET_HASH_KEY("rcm_evelyn_miller1"),
		MISC::GET_HASH_KEY("rcm_evelyn_miller2"),
		MISC::GET_HASH_KEY("rcm_evelyn_miller3"),
		MISC::GET_HASH_KEY("rcm_evelyn_miller4"),
		MISC::GET_HASH_KEY("rcm_evelyn_miller5"),
		MISC::GET_HASH_KEY("rcm_exconfed11"),
		MISC::GET_HASH_KEY("rcm_for_my_art1"),
		MISC::GET_HASH_KEY("rcm_for_my_art2"),
		MISC::GET_HASH_KEY("rcm_for_my_art3"),
		MISC::GET_HASH_KEY("rcm_for_my_art4"),
		MISC::GET_HASH_KEY("rcm_fundraiser"),
		MISC::GET_HASH_KEY("rcm_gang01"),
		MISC::GET_HASH_KEY("rcm_gang02"),
		MISC::GET_HASH_KEY("rcm_gunslinger1_1"),
		MISC::GET_HASH_KEY("rcm_gunslinger1_2"),
		MISC::GET_HASH_KEY("rcm_gunslinger2_1"),
		MISC::GET_HASH_KEY("rcm_gunslinger3_1"),
		MISC::GET_HASH_KEY("rcm_gunslinger5_1"),
		MISC::GET_HASH_KEY("rcm_herbalist_camp"),
		MISC::GET_HASH_KEY("rcm_here_kitty_kitty1"),
		MISC::GET_HASH_KEY("rcm_here_kitty_kitty2"),
		MISC::GET_HASH_KEY("rcm_here_kitty_kitty3"),
		MISC::GET_HASH_KEY("rcm_here_kitty_kitty4"),
		MISC::GET_HASH_KEY("rcm_here_kitty_kitty5"),
		MISC::GET_HASH_KEY("rcm_homerob00"),
		MISC::GET_HASH_KEY("rcm_homerob01"),
		MISC::GET_HASH_KEY("rcm_jack2"),
		MISC::GET_HASH_KEY("rcm_mary01"),
		MISC::GET_HASH_KEY("rcm_mary02"),
		MISC::GET_HASH_KEY("rcm_mary31"),
		MISC::GET_HASH_KEY("rcm_marybeth1"),
		MISC::GET_HASH_KEY("rcm_mason1"),
		MISC::GET_HASH_KEY("rcm_mason2"),
		MISC::GET_HASH_KEY("rcm_mason3"),
		MISC::GET_HASH_KEY("rcm_mason4"),
		MISC::GET_HASH_KEY("rcm_mason5"),
		MISC::GET_HASH_KEY("rcm_mob01"),
		MISC::GET_HASH_KEY("rcm_mob02"),
		MISC::GET_HASH_KEY("rcm_monroe11"),
		MISC::GET_HASH_KEY("rcm_mr_mayor1"),
		MISC::GET_HASH_KEY("rcm_mr_mayor2"),
		MISC::GET_HASH_KEY("rcm_mr_mayor3"),
		MISC::GET_HASH_KEY("rcm_mudtown3_1"),
		MISC::GET_HASH_KEY("rcm_mudtown3_2"),
		MISC::GET_HASH_KEY("rcm_mudtown3_3"),
		MISC::GET_HASH_KEY("rcm_native1"),
		MISC::GET_HASH_KEY("rcm_native2"),
		MISC::GET_HASH_KEY("rcm_oh_brother1"),
		MISC::GET_HASH_KEY("rcm_oh_brother2"),
		MISC::GET_HASH_KEY("rcm_oh_brother3"),
		MISC::GET_HASH_KEY("rcm_pearson1"),
		MISC::GET_HASH_KEY("rcm_pearson1_outro"),
		MISC::GET_HASH_KEY("rcm_poisoned_well1"),
		MISC::GET_HASH_KEY("rcm_poisoned_well2"),
		MISC::GET_HASH_KEY("rcm_poisoned_well3"),
		MISC::GET_HASH_KEY("rcm_poisoned_well4"),
		MISC::GET_HASH_KEY("rcm_poisoned_well5"),
		MISC::GET_HASH_KEY("rcm_rains_fall1"),
		MISC::GET_HASH_KEY("rcm_ride_the_lightning1"),
		MISC::GET_HASH_KEY("rcm_ride_the_lightning2"),
		MISC::GET_HASH_KEY("rcm_ride_the_lightning3"),
		MISC::GET_HASH_KEY("rcm_ride_the_lightning4"),
		MISC::GET_HASH_KEY("rcm_ride_the_lightning5"),
		MISC::GET_HASH_KEY("rcm_ride_the_lightning6"),
		MISC::GET_HASH_KEY("rcm_ride_the_lightning7"),
		MISC::GET_HASH_KEY("rcm_sadie11"),
		MISC::GET_HASH_KEY("rcm_serial_killer1"),
		MISC::GET_HASH_KEY("rcm_slave_catcher1"),
		MISC::GET_HASH_KEY("rcm_slave_catcher2"),
		MISC::GET_HASH_KEY("rcm_strauss11"),
		MISC::GET_HASH_KEY("rcm_strauss21"),
		MISC::GET_HASH_KEY("rcm_strauss31"),
		MISC::GET_HASH_KEY("rcm_strauss32"),
		MISC::GET_HASH_KEY("rcm_strauss33"),
		MISC::GET_HASH_KEY("rcm_the_odd_fellows1"),
		MISC::GET_HASH_KEY("rcm_the_odd_fellows2"),
		MISC::GET_HASH_KEY("rcm_tilly1"),
		MISC::GET_HASH_KEY("rcm_treasure_hunter"),
		MISC::GET_HASH_KEY("rcm_war_veteran1"),
		MISC::GET_HASH_KEY("rcm_war_veteran2"),
		MISC::GET_HASH_KEY("rcm_war_veteran3"),
		MISC::GET_HASH_KEY("rcm_war_veteran4"),
		MISC::GET_HASH_KEY("re_murder_camp_note"),
		MISC::GET_HASH_KEY("spd_agnesdowd1"),
		MISC::GET_HASH_KEY("spd_andershelgerson"),
		MISC::GET_HASH_KEY("spd_armadillotowncrier"),
		MISC::GET_HASH_KEY("spd_blandpreacher"),
		MISC::GET_HASH_KEY("spd_blindmancassidy_horse_buck"),
		MISC::GET_HASH_KEY("spd_blindmancassidy1"),
		MISC::GET_HASH_KEY("spd_bummicky1"),
		MISC::GET_HASH_KEY("spd_bummicky2"),
		MISC::GET_HASH_KEY("spd_bummicky3"),
		MISC::GET_HASH_KEY("spd_bummicky4"),
		MISC::GET_HASH_KEY("spd_bummicky5"),
		MISC::GET_HASH_KEY("spd_bummicky6"),
		MISC::GET_HASH_KEY("spd_bummicky7"),
		MISC::GET_HASH_KEY("spd_chelonianmaster1"),
		MISC::GET_HASH_KEY("spd_civilwarcommando"),
		MISC::GET_HASH_KEY("spd_crackpotrobot"),
		MISC::GET_HASH_KEY("spd_dorotheawicklow1"),
		MISC::GET_HASH_KEY("spd_dorotheawicklow2"),
		MISC::GET_HASH_KEY("spd_endlessstrainer1"),
		MISC::GET_HASH_KEY("spd_eugenicsproponent"),
		MISC::GET_HASH_KEY("spd_gavin1"),
		MISC::GET_HASH_KEY("spd_gavin2"),
		MISC::GET_HASH_KEY("spd_giant"),
		MISC::GET_HASH_KEY("spd_giant_birds"),
		MISC::GET_HASH_KEY("spd_grizzledjon1"),
		MISC::GET_HASH_KEY("spd_joebutler1"),
		MISC::GET_HASH_KEY("spd_joebutler2"),
		MISC::GET_HASH_KEY("spd_joebutler3"),
		MISC::GET_HASH_KEY("spd_joebutler4"),
		MISC::GET_HASH_KEY("spd_johnmadman"),
		MISC::GET_HASH_KEY("spd_lillianpowell1"),
		MISC::GET_HASH_KEY("spd_lillianpowell2"),
		MISC::GET_HASH_KEY("spd_lillianpowell3"),
		MISC::GET_HASH_KEY("spd_lillianpowell4"),
		MISC::GET_HASH_KEY("spd_madscientist"),
		MISC::GET_HASH_KEY("spd_mayorofstrawberry1"),
		MISC::GET_HASH_KEY("spd_philosopher1"),
		MISC::GET_HASH_KEY("spd_poorjoe1"),
		MISC::GET_HASH_KEY("spd_sheriffoftumbleweed"),
		MISC::GET_HASH_KEY("spd_soothsayer1"),
		MISC::GET_HASH_KEY("spd_sunworshipper1"),
		MISC::GET_HASH_KEY("spd_swampweirdo1"),
		MISC::GET_HASH_KEY("spd_thomasdowne1"),
		MISC::GET_HASH_KEY("spd_timothydonahue1"),
		MISC::GET_HASH_KEY("spd_timothydonahue2"),
		MISC::GET_HASH_KEY("spd_tinyhermit1"),
		MISC::GET_HASH_KEY("spd_vampire")
	};

protected:
	Ped PlayerPed = 0;
	Ped PlayerHorse = 0;
	Vector3 PlayerCoords = {0,0,0};
	Vector3 PlayerHorseCoords = {0,0,0};
	int PlayerHorseGroup = 0;

	std::string GangName;

	std::vector<std::string> ObjectivesList = {"MISSION ~COLOR_YELLOW~OBJECTIVE"};

	virtual bool SetMissionParamsForCamp(int camp) = 0;

	std::vector<Hash> ScriptsKillList;

	struct MissionAsset
	{
		std::string type;
		std::string asset;
	};
	
	struct NavBlocker
	{
		Vector3 pos;
		Vector3 rot;
		Vector3 scl;
	};

	struct ExtraProps
	{
		std::string model;
		bool propset;
		Vector3 pos;
		float rot;
		std::vector<NavBlocker> blockers;
	};

	std::vector<MissionAsset> MissionAssetsToLoad;
	void AddHorseToMissionAssets(Hash horse);
	void LoadAllMissionAssets();

	virtual void CommonInit() = 0;

	void SetVarsFromStaticData(const MissionStaticData& thisMissionData, bool hasMusicOverride = false);

	std::vector<Entity> SpawnedMissionEntities;
	std::vector<Vehicle> SpawnedMissionTrains;
	std::vector<PropSet> SpawnedMissionPropsets;
	std::vector<Volume> SpawnedMissionVolumes;
	std::vector<Blip> SpawnedMissionBlips;

	struct SpawnedGangMember
	{
		Ped member;
		int status;
		Hash armor;
		Hash sidearm;
		Hash longarm;
		Ped OwnedHorse;
		int TimeOfDeath;
	};

	std::vector<SpawnedGangMember> SpawnedGangMembers;
	std::vector<MissionCompanionInfo> GangMemberTemplates;

	void SpawnGangMembers(int amount, bool noHorses = false);
	void TrackSpawnedGangMembers();

	void TrackPlayer();

	int RewardBounty_Potential, RewardCash_Potential, RewardMembers_Potential;
	int RewardBounty_Active, RewardCash_Active, RewardMembers_Active;
	virtual void TrackRewards() = 0;

	void ArmyHelper(Ped spawned, const std::string& name);

	Ped SpawnPed(const std::string& name, const Vector3& pos, float heading, int skin, int customrandmax = -1);
	Vehicle SpawnVehicle(const std::string& name, const Vector3& pos, float heading, bool nohorses = false);
	Vehicle SpawnTrain(const std::string& name, const Vector3& pos, bool reverseDirection);
	Object SpawnProp(const std::string& name, const Vector3& pos, float heading, bool dynamic = false);
	Object SpawnProp(Hash hash, const Vector3& pos, float heading, bool dynamic = false);
	PropSet SpawnPropSet(const std::string& name, const Vector3& pos, float heading);
	Volume SpawnVolume(const Vector3& pos, const Vector3& rot, const Vector3& scl, bool navblocker);
	
	void DefaultHorseSetup(Ped horse);

	void DetachAllHorsesFromWagon(Vehicle wagon);

	void ReleaseSpawnedMissionEntities();

	enum class PedType
	{
		SIDEARM_WEAK,
		SIDEARM_REGULAR,
		SIDEARM_EXPERT,
		LONGARMS_MINIMUM,
		REPEATER_WEAK,
		REPEATER_REGULAR,
		REPEATER_EXPERT,
		RIFLE_WEAK,
		RIFLE_REGULAR,
		RIFLE_EXPERT,
		SHOTGUN_WEAK,
		SHOTGUN_REGULAR,
		SHOTGUN_EXPERT,
		SNIPER_WEAK,
		SNIPER_REGULAR,
		SNIPER_EXPERT,
		MAX_TOTAL
	};

	void SetPedType(Ped ped, PedType type);

	void PlayPlayerSpeech(const std::string& line);
	void PlayCompanionSpeech(const std::string& line);

	enum class CompanionControlStatus
	{
		FOLLOWING,
		ATTACKING
	};

	const std::string CompanionControlAttack = "Send The Gang";
	const std::string CompanionControlRegroup = "Regroup Gang";

	bool AllowCompanionManualControl = false;
	CompanionControlStatus CompanionManualControlStatus = CompanionControlStatus::FOLLOWING;

	void TrackCompanionControl();
	bool CompanionsControlPressed();

	bool CompanionsInCombat = false;
	int CompanionTaskTimer = 0;
	int CompanionTaskControl = 0;
	void CompanionsBeginCombat(bool noComment = false);
	void CompanionsClearTasks();
	void CompanionsDisband();
	void CompanionsRegroup();

	void SetCompanionPlayerCollision(bool on);

	bool AllowWanted = false;
	bool AllowAmbientLaw = false;
	bool WantedZoneStaysWithPlayer = false;
	void EnableLaw();
	void DisableLaw();
	void SetDispatchService(bool bParam0);
	void BecomeWanted(Hash crime, int intensity, bool noWait = false);
	void ClearWanted(bool modifyrelation);

	std::string GenericLawLeader;
	std::string GenericLawFollower;
	void SetGenericLawModels(int district);

	struct WantedLogicParams
	{
		std::vector<Vector3> ScriptedLawSpawnPoints;
		std::vector<Ped> ScriptedLawPeds;
		bool AllScriptedPedsKilled = false;
		int DispatchResetDelay = 30000;
		int MusicStageOnSpotted = STAGE_ACTION2;
		bool CompanionCombatOnSpotted = false;
	};

	int TimeWantedStarted = 0;
	bool PlayerIncognito = true;
	bool PlayerPursuit = false;
	WantedLogicParams ActivePursuitParams;
	void CreateScriptedRespondingLawPed(const std::string& model, PedType weapons, Hash HORSE_TYPE, Vector3 spawn, Vector3 destination);
	bool WantedLogic(bool forceNoWantedOverride = false);

	enum MusicOptions
	{
		MUSIC_DEFAULT,
		MUSIC_SEAN_RESCUE,
		MUSIC_ODRISCOLLTHEME,
		MUSIC_SHADYBELLE,
		MUSIC_PINKERTONS,
		MUSIC_BASIN,
		MUSIC_SISIKA
	};

	enum MusicStage
	{
		STAGE_MUTED,
		STAGE_START,
		STAGE_CALM,
		STAGE_ACTION1,
		STAGE_ACTION2,
		STAGE_AFTERACTION,
	};

	std::vector<std::string> GetAllMusicEvents();
	int CurrentMusicTrack = MUSIC_DEFAULT;
	int CurrentMusicStage = STAGE_START;
	void UpdateMusic();
	void MusicStartupDelay();
	bool MusicLoopOverride = false;
	bool MusicOverride = false;

	bool CheckAbandon = false;
	Vector3 AbandonCoords = {0, 0, 0};
	float AbandonRadius = 0;
	int AbandonTime = 0;
	int MaxAbandonTime = 10000;

	int TimeMissionSuccess = -1;
	int TimeMissionFail = -1;
	const int TimeMissionSuccessDelay = 3000;
	const float TimeMissionFailSlowdown = 0.2;
	const int TimeMissionFailDelay = 800;

	bool FailScreenActive = false;

	void MissionSuccess();
	void MissionFail();

	void ObjectiveSet(int index);
	void ObjectiveClear();
	void ObjectiveTempOverride(const std::string& str);

	Blip ObjectiveBlipCoords(const Vector3& coords, float radius, bool GPS, const std::string& str);
	Blip ObjectiveBlipEntity(Entity ent, bool GPS, const std::string& str);

public:
	MissionBase();
	virtual ~MissionBase();
	
	virtual void Setup(int campid, const std::string& gangstr, const std::vector<MissionCompanionInfo>& companions) = 0;

	void common_update();
	virtual void Update() = 0;

	bool TriggeredGenericFailureCondition();

	MissionType ThisMissionID = MissionType::INVALID;

	int ExitMissionFlag = M_EXIT_NONE;

	std::string MissionFailStr = "NONE";

	virtual MissionResults GetMissionResults();

	bool MissionLawOverride = false;

	bool FinishedLoadingAssets = false;

};
