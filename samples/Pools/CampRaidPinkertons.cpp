
#include "precomp.h"
#include "CampRaidPinkertons.h"

const MissionStaticData& CampRaidPinkertons::GetStaticMissionData()
{
	static MissionStaticData missiondata = {
		MissionType::CAMPRAID_PINKERTONS,
		0, 0, 0, 0, 0,
		"CAMP DEFENSE",
		"The Pinkerton Detective Agency is closing in!\nPrepare to fight for your lives!",
		COMPCOUNT_MAX,
		false,
		MUSIC_PINKERTONS,
		true
	};
	return missiondata;
}

CampRaidPinkertons::CampRaidPinkertons()
{
	DioScope(DIOTAG);
}

CampRaidPinkertons::~CampRaidPinkertons()
{
	DioScope(DIOTAG);
}

bool CampRaidPinkertons::SetMissionParamsForCamp(int camp)
{
	GenericLawLeader = "S_M_M_PINLAW_01";
	GenericLawFollower = "S_M_M_PINLAW_01";
	
	switch (camp)
	{
		case DISTRICT_GRIZZLIES_WEST:
			CampPoint = {-922.07f, 1563.32f, 237.43f};
			CampRadius = 18.0f;
			RoadSpawns = {{{-894.09f, 1476.51f, 240.46f}, 35}};
			WagonDestinations = {{-926.15f, 1530.90f, 240.15f}};
			FootSpawns = {
				{{-919.04f, 1499.44f, 244.43f}, 349},
				{{-889.25f, 1503.64f, 245.29f}, 25},
				{{-869.62f, 1509.33f, 248.15f}, 31}
			};
			FlankingSpawns = {
				{{-866.66f, 1544.62f, 251.39f}, 76},
				{{-959.23f, 1517.06f, 244.68f}, 333}
			};
			SniperSpawnDatas = {
				{{{-951.40f, 1512.06f, 247.92f}, 326}, {-945.13f, 1520.80f, 250.80f}},
				{{{-901.57f, 1594.39f, 245.87f}, 219}, {-900.11f, 1593.07f, 245.60f}},
				{{{-965.17f, 1629.00f, 246.19f}, 204}, {-965.17f, 1629.00f, 246.19f}}
			};
			return true;

		case DISTRICT_CUMBERLAND_FOREST:
			CampPoint = {741.61f, 1192.49f, 142.76f};
			CampRadius = 17;
			RoadSpawns = {{{844.80f, 1155.78f, 145.92f}, 102}};
			WagonDestinations = {{814.13f, 1152.76f, 140.55f}};
			FootSpawns = {
				{{801.34f, 1141.83f, 136.89f}, 36},
				{{772.41f, 1145.04f, 136.82f}, 30},
				{{815.99f, 1207.64f, 138.25f}, 110}
			};
			FlankingSpawns = {
				{{758.81f, 1260.00f, 154.39f}, 170}
			};
			SniperSpawnDatas = {
				{{{845.32f, 1202.45f, 159.72f}, 113}, {845.32f, 1202.45f, 159.72f}},
				{{{838.54f, 1202.20f, 156.04f}, 105}, {838.54f, 1202.20f, 156.04f}},
				{{{678.57f, 1215.72f, 180.64f}, 249}, {678.57f, 1215.72f, 180.64f}}
			};
			return true;

		case DISTRICT_BLUEGILL_MARSH:
			CampPoint = {1953.97f, -478.75f, 40.73f};
			CampRadius = 14;
			RoadSpawns = {{{1994.59f, -416.41f, 40.62f}, 83}};
			WagonDestinations = {{1946.78f, -407.82f, 43.78f}};
			FootSpawns = {
				{{2034.08f, -451.37f, 40.90f}, 112},
				{{2029.26f, -460.23f, 40.72f}, 101},
				{{2020.23f, -485.15f, 40.68f}, 124}
			};
			FlankingSpawns = {
				{{1980.68f, -552.96f, 40.69f}, 27}
			};
			SniperSpawnDatas = {
				{{{1959.51f, -413.73f, 42.26f}, 174}, {1959.51f, -413.73f, 42.26f}},
				{{{1908.69f, -405.74f, 42.40f}, 234}, {1908.69f, -405.74f, 42.40f}},
				{{{1902.88f, -468.36f, 41.49f}, 266}, {1902.88f, -468.36f, 41.49f}}
			};
			return true;

		case DISTRICT_RIO_BRAVO:
			CampPoint = {-4916.46f, -3145.15f, -14.17f};
			CampRadius = 9;
			RoadSpawns = {{{-5009.00f, -3111.81f, -17.52f}, 267}};
			WagonDestinations = {{-4936.34f, -3109.45f, -14.90f}};
			FootSpawns = {
				{{-4934.76f, -3095.29f, -18.40f}, 199},
				{{-4953.01f, -3097.65f, -18.66f}, 203},
				{{-4981.38f, -3124.64f, -16.16f}, 255}
			};
			FlankingSpawns = {
				{-4816.19f, -3139.94f, 4.70f}
			};
			SniperSpawnDatas = {
				{{{-4835.04f, -3146.51f, 4.45f}, 93}, {-4835.04f, -3146.51f, 4.45f}},
				{{{-4861.16f, -3180.56f, 4.80f}, 51}, {-4861.16f, -3180.56f, 4.80f}},
				{{{-4932.91f, -3161.18f, -1.89f}, 322}, {-4932.91f, -3161.18f, -1.89f}}
			};
			return true;
	}

	DioTrace(DIOTAG1("ERROR") "CANNOT SET MISSION %d PARAMS FOR CAMP %d - ABORTING MISSION", CampRaidLaw::GetStaticMissionData().StaticMissionID, camp);
	return false;
}

void CampRaidPinkertons::CommonInit()
{
	const MissionStaticData& thisMissionData = CampRaidPinkertons::GetStaticMissionData();
	SetVarsFromStaticData(thisMissionData);

	MissionAssetsToLoad = {
		{"VEH", "GATCHUCK_2"}
	};

	LawHorse = HORSE_ARABIAN;

	AddHorseToMissionAssets(LawHorse);

	ObjectivesList = {
		"~COLOR_YELLOW~Defend your camp"
	};

	LoadAllMissionAssets();
	SpawnGangMembers(thisMissionData.NumMembersToSpawn, true);

	WAIT(2000);
}

void CampRaidPinkertons::SpawnFirstWave()
{
	DioTrace(DIOTAG);

	// Sniper
	SpawnEnemySniper(SniperSpawnDatas[0].spawn.p, SniperSpawnDatas[0].spawn.h, SniperSpawnDatas[0].destination, PedType::SNIPER_EXPERT);
	
	// Sidearm Riders
	for (int i = 0; i < 5; ++i)
	{
		LocationData spawn = RoadSpawns[rand() % RoadSpawns.size()];

		SpawnEnemy(GenericLawFollower, spawn.p, spawn.h, PedType::SIDEARM_EXPERT, true);
	}

	// Shotgun Chargers
	for (int i = 0; i < 6; ++i)
	{
		LocationData spawn = FootSpawns[rand() % FootSpawns.size()];

		PED::SET_PED_COMBAT_MOVEMENT(SpawnEnemy(GenericLawFollower, spawn.p, spawn.h, PedType::SHOTGUN_EXPERT, false), 3);
	}
}

void CampRaidPinkertons::SpawnSecondWave()
{
	DioTrace(DIOTAG);

	// Flanking Riflemen + Repeaters
	for (int i = 0; i < FlankingSpawns.size(); ++i)
	{
		LocationData spawn = FlankingSpawns[i];

		for (int j = 0; j < 2; ++j)
		{
			PED::SET_PED_COMBAT_MOVEMENT(SpawnEnemy(GenericLawFollower, spawn.p, spawn.h, PedType::RIFLE_EXPERT, false), 3);
		}

		for (int j = 0; j < 3; ++j)
		{
			PED::SET_PED_COMBAT_MOVEMENT(SpawnEnemy(GenericLawFollower, spawn.p, spawn.h, PedType::REPEATER_EXPERT, false), 3);
		}
	}

	// Sniper
	SpawnEnemySniper(SniperSpawnDatas[1].spawn.p, SniperSpawnDatas[1].spawn.h, SniperSpawnDatas[1].destination, PedType::SNIPER_EXPERT);
}

void CampRaidPinkertons::SpawnWagon()
{
	LocationData spawn = RoadSpawns[WagonSpawnIndex];

	SpawnedWagon = SpawnVehicle("GATCHUCK_2", spawn.p, spawn.h);

	ENTITY::SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(SpawnedWagon, true);

	VEHICLE::SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(SpawnedWagon, false);

	Ped GatlingDriver = SpawnEnemy(GenericLawLeader, spawn.p, 0, PedType::SIDEARM_EXPERT, false, true);

	TASK::CLEAR_PED_TASKS_IMMEDIATELY(GatlingDriver, true, true);
	EVENT::SET_DECISION_MAKER(GatlingDriver, MISC::GET_HASH_KEY("EMPTY"));

	PED::SET_PED_INTO_VEHICLE(GatlingDriver, SpawnedWagon, SEAT_DRIVER);

	int sequence = 0;
	TASK::OPEN_SEQUENCE_TASK(&sequence);

	TASK::TASK_VEHICLE_GOTO_NAVMESH(0, SpawnedWagon, WagonDestinations[WagonSpawnIndex].x, WagonDestinations[WagonSpawnIndex].y, WagonDestinations[WagonSpawnIndex].z, 50.0f, 0, 1.0f);
	TASK::TASK_LEAVE_ANY_VEHICLE(0, 0, 0);
	TASK::TASK_COMBAT_PED(0, PLAYER::PLAYER_PED_ID(), 0, 0);

	TASK::CLOSE_SEQUENCE_TASK(sequence);
	TASK::TASK_PERFORM_SEQUENCE(GatlingDriver, sequence);
	TASK::CLEAR_SEQUENCE_TASK(&sequence);

	PED::SET_PED_KEEP_TASK(GatlingDriver, true);

	Ped GatlingGunner = SpawnEnemy(GenericLawLeader, spawn.p, 0, PedType::SIDEARM_EXPERT, false, true);

	TASK::CLEAR_PED_TASKS_IMMEDIATELY(GatlingGunner, true, true);
	EVENT::SET_DECISION_MAKER(GatlingGunner, MISC::GET_HASH_KEY("EMPTY"));

	PED::SET_PED_INTO_VEHICLE(GatlingGunner, SpawnedWagon, SEAT_BACK_LEFT);

	PED::SET_PED_COMBAT_ATTRIBUTES(GatlingGunner, CA_0xA21AD34D, true);
	PED::SET_PED_COMBAT_ATTRIBUTES(GatlingGunner, CA_0xA706084B, true);
	PED::SET_PED_COMBAT_ATTRIBUTES(GatlingGunner, CA_0x1CB77C49, false);
	PED::SET_PED_COMBAT_ATTRIBUTES(GatlingGunner, CA_LEAVE_VEHICLES, false);

	PED::SET_PED_CONFIG_FLAG(GatlingGunner, PCF_NoCriticalHits, false);
	PED::SET_PED_CONFIG_FLAG(GatlingGunner, PCF_OneShotWillKillPed, true);
	PED::SET_PED_CONFIG_FLAG(GatlingGunner, PCF_DontExitVehicleIfNoDraftAnimals, true);

	ENTITY::SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(GatlingGunner, true);

	PED::SET_PED_CAN_RAGDOLL(GatlingGunner, false);

	PED::SET_PED_FLEE_ATTRIBUTES(GatlingGunner, 512, true);
}

void CampRaidPinkertons::SpawnFinalWave()
{
	DioTrace(DIOTAG);

	// Shotgun Riders
	for (int i = 0; i < 4; ++i)
	{
		LocationData spawn = RoadSpawns[rand() % RoadSpawns.size()];

		SpawnEnemy(GenericLawFollower, spawn.p, spawn.h, PedType::SHOTGUN_EXPERT, true, true);
	}

	// Sniper
	SpawnEnemySniper(SniperSpawnDatas[2].spawn.p, SniperSpawnDatas[2].spawn.h, SniperSpawnDatas[2].destination, PedType::SNIPER_EXPERT);

	// Gatling Wagon
	WagonSpawnIndex = 0;
	SpawnWagon();
}

void CampRaidPinkertons::WagonLogic()
{
	if (!ENTITY::DOES_ENTITY_EXIST(SpawnedWagon) || VEHICLE::IS_VEHICLE_WRECKED(SpawnedWagon))
	{
		return;
	}
	
	Ped Driver = VEHICLE::GET_PED_IN_VEHICLE_SEAT(SpawnedWagon, SEAT_DRIVER);
	Ped Gunner = VEHICLE::GET_PED_IN_VEHICLE_SEAT(SpawnedWagon, SEAT_BACK_LEFT);

	if (!ENTITY::DOES_ENTITY_EXIST(Gunner))
	{
		return;
	}
	else if (PED::IS_PED_DEAD_OR_DYING(Gunner, true) && PED::GET_PED_CONFIG_FLAG(Gunner, PCF_DontInfluenceWantedLevel, true))
	{
		CAM::_FORCE_CINEMATIC_DEATH_CAM_ON_PED(Gunner);

		PED::SET_PED_CONFIG_FLAG(Gunner, PCF_DontInfluenceWantedLevel, false);
	}
	else if (PED::IS_PED_DEAD_OR_DYING(Gunner, true) && !PED::GET_PED_CONFIG_FLAG(Gunner, PCF_DontInfluenceWantedLevel, true))
	{
		return;
	}

	if (!WagonFiring && (PED::CAN_PED_SEE_ENTITY(Gunner, PlayerPed, true, true) == 1 || WagonArrived || PED::CAN_PED_SEE_ENTITY(Driver, PlayerPed, true, true) == 1))
	{
		DioTrace(DIOTAG "Gatling opening fire");

		WagonFiring = true;

		WEAPON::SET_CURRENT_PED_VEHICLE_WEAPON(Gunner, -1193642378);
		VEHICLE::DISABLE_VEHICLE_WEAPON(false, -1193642378, SpawnedWagon, Gunner);

		ENTITY::SET_ENTITY_LOAD_COLLISION_FLAG(SpawnedWagon, 1);
		WEAPON::_SET_VEHICLE_WEAPON_HEADING_LIMITS_2(SpawnedWagon, 1, -175.0f, 175.0f);

		PED::SET_PED_ACCURACY(Gunner, 25);

		TASK::TASK_COMBAT_PED(Gunner, PlayerPed, 0, 0);

		PED::SET_PED_CONFIG_FLAG(Gunner, PCF_DontInfluenceWantedLevel, true);
	}

	if (WagonArrived)
	{
		return;
	}
	else if (
		!ENTITY::DOES_ENTITY_EXIST(Driver) ||
		PED::IS_PED_DEAD_OR_DYING(Driver, true) ||
		ENTITY::IS_ENTITY_AT_COORD(SpawnedWagon, WagonDestinations[WagonSpawnIndex].x, WagonDestinations[WagonSpawnIndex].y, WagonDestinations[WagonSpawnIndex].z, 3.0f, 3.0f, 3.0f, false, true, 0))
	{
		DioTrace(DIOTAG "Gatling arrived or driver killed");

		WagonArrived = true;

		int iVar0 = 0;

		while (iVar0 <= (6 - 1))
		{
			Ped iVar1 = VEHICLE::_GET_PED_IN_DRAFT_HARNESS(SpawnedWagon, iVar0);

			if (ENTITY::DOES_ENTITY_EXIST(iVar1))
			{
				VEHICLE::_DETACH_DRAFT_VEHICLE_HARNESS_FROM_INDEX(SpawnedWagon, iVar0);
			}
			iVar0++;
		}

		VEHICLE::BRING_VEHICLE_TO_HALT(SpawnedWagon, 1.5f, -1, true);
	}
}

void CampRaidPinkertons::Update()
{
#ifndef DEBUG_FAST_MISSION
	CLOCK::SET_CLOCK_TIME(0, 0, 0);
	MISC::SET_WEATHER_TYPE(0xB677829F /*THUNDER*/, true, false, false, 0, false);
#endif

	DoggoLogic();

	WagonLogic();

	for (Ped horse : SpawnedMissionEntities)
	{
		if (Common::PedIsHorse(horse) && !ENTITY::DOES_ENTITY_EXIST(PED::_GET_RIDER_OF_MOUNT(horse, true)) && !PED::GET_PED_CONFIG_FLAG(horse, PCF_AlwaysRejectPlayerRobberyAttempt, true))
		{
			TASK::TASK_FLEE_PED(horse, PlayerPed, 4, 0, -1082130432 /* Float: -1f */, -1, 0);
			PED::SET_PED_CONFIG_FLAG(horse, PCF_AlwaysRejectPlayerRobberyAttempt, true);
		}
	}
	
	if (TimeMissionFail > 0 && MISC::GET_GAME_TIMER() - TimeMissionFail > TimeMissionFailDelay)
	{
		DioTrace("Exit Fail Triggered");

		ExitMissionFlag = M_EXIT_FAILURE;
	}
	else if (TimeMissionSuccess > 0 && MISC::GET_GAME_TIMER() - TimeMissionSuccess > TimeMissionSuccessDelay)
	{
		DioTrace("Exit Win Triggered");

		ExitMissionFlag = M_EXIT_SUCCESS;
	}
	else if (ActivateObjective_0)
	{
		ObjectiveInit_0();
	}
	else if (ActivateObjective_1)
	{
		ObjectiveInit_1();
	}
	else if (ActivateObjective_2)
	{
		ObjectiveInit_2();
	}
	else if (ActivateObjective_3)
	{
		ObjectiveInit_3();
	}
	else if (RunObjective_0)
	{
		ObjectiveLogic_0();
	}
	else if (RunObjective_1)
	{
		ObjectiveLogic_1();
	}
	else if (RunObjective_2)
	{
		ObjectiveLogic_2();
	}
	else if (RunObjective_3)
	{
		ObjectiveLogic_3();
	}
}
