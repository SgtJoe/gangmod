
#include "precomp.h"
#include "MissionFactory.h"

#include "TestMission.h"
#include "TestMission2.h"
#include "JailbreakWagon.h"
#include "JailbreakChain.h"
#include "JailbreakTown.h"
#include "JailbreakSisika.h"
#include "RobberyCoach.h"
#include "RobberyStore.h"
#include "RobberyHome.h"
#include "RobberyTrain.h"
#include "CampRaidGang.h"
#include "CampRaidLaw.h"
#include "CampRaidPinkertons.h"
#include "FreeRoam.h"
#include "Stronghold.h"

namespace MissionFactory
{
	MissionBase* SpawnMission(MissionType missionID)
	{
		switch (missionID)
		{
			case MissionType::TESTMISSION:
				return new TestMission();

			case MissionType::TESTMISSION2:
				return new TestMission2();

			case MissionType::JAILBREAK_WAGON:
				return new JailbreakWagon();

			case MissionType::JAILBREAK_CHAIN:
				return new JailbreakChain();

			case MissionType::JAILBREAK_TOWN:
				return new JailbreakTown();

			case MissionType::JAILBREAK_SISIKA:
				return new JailbreakSisika();

			case MissionType::ROBBERY_COACH:
				return new RobberyCoach();

			case MissionType::ROBBERY_STORE:
				return new RobberyStore();

			case MissionType::ROBBERY_HOME:
				return new RobberyHome();

			case MissionType::ROBBERY_TRAIN:
				return new RobberyTrain();

			case MissionType::CAMPRAID_GANG:
				return new CampRaidGang();

			case MissionType::CAMPRAID_LAW:
				return new CampRaidLaw();

			case MissionType::CAMPRAID_PINKERTONS:
				return new CampRaidPinkertons();

			case MissionType::FREEROAM:
				return new FreeRoam();

			case MissionType::STRONGHOLD:
				return new Stronghold();

			default:
				DioTrace(DIOTAG1("ERROR") "COULD NOT FIND CLASS FOR MISSION %d", missionID);
				return nullptr;
		}
	}

	const MissionStaticData& GetMissionStaticData(MissionType missionID)
	{
		static const MissionStaticData error = {MissionType::INVALID};
		
		switch (missionID)
		{
			case MissionType::TESTMISSION:
				return TestMission::GetStaticMissionData();

			case MissionType::TESTMISSION2:
				return TestMission2::GetStaticMissionData();

			case MissionType::JAILBREAK_WAGON:
				return JailbreakWagon::GetStaticMissionData();

			case MissionType::JAILBREAK_CHAIN:
				return JailbreakChain::GetStaticMissionData();

			case MissionType::JAILBREAK_TOWN:
				return JailbreakTown::GetStaticMissionData();

			case MissionType::JAILBREAK_SISIKA:
				return JailbreakSisika::GetStaticMissionData();

			case MissionType::ROBBERY_COACH:
				return RobberyCoach::GetStaticMissionData();

			case MissionType::ROBBERY_STORE:
				return RobberyStore::GetStaticMissionData();

			case MissionType::ROBBERY_HOME:
				return RobberyHome::GetStaticMissionData();

			case MissionType::ROBBERY_TRAIN:
				return RobberyTrain::GetStaticMissionData();

			case MissionType::CAMPRAID_GANG:
				return CampRaidGang::GetStaticMissionData();

			case MissionType::CAMPRAID_LAW:
				return CampRaidLaw::GetStaticMissionData();

			case MissionType::CAMPRAID_PINKERTONS:
				return CampRaidPinkertons::GetStaticMissionData();

			case MissionType::FREEROAM:
				return FreeRoam::GetStaticMissionData();

			case MissionType::STRONGHOLD:
				return Stronghold::GetStaticMissionData();
			
			default:
				DioTrace(DIOTAG1("ERROR") "COULD NOT GET STATIC DATA FOR MISSION %d", missionID);
				return error;
		}
	}

	std::vector<MissionType> GetAvailableMissionsForCampUpgrade(int camp, int upgrade)
	{
		std::vector<MissionType> missions = { MissionType::FREEROAM };

		switch (upgrade)
		{
			case 3:

			case 2:
				missions.emplace_back(MissionType::JAILBREAK_SISIKA);
				missions.emplace_back(MissionType::ROBBERY_TRAIN);

			case 1:
				missions.emplace_back(MissionType::JAILBREAK_TOWN);
				missions.emplace_back(MissionType::STRONGHOLD);
				missions.emplace_back(MissionType::JAILBREAK_CHAIN);
				missions.emplace_back(MissionType::ROBBERY_HOME);

			case 0:
				missions.emplace_back(MissionType::ROBBERY_STORE);
				missions.emplace_back(MissionType::ROBBERY_COACH);
				missions.emplace_back(MissionType::JAILBREAK_WAGON);
		}

		return missions;
	}

}
