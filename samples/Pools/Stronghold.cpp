
#include "precomp.h"
#include "Stronghold.h"

const MissionStaticData& Stronghold::GetStaticMissionData()
{
	static MissionStaticData missiondata = {
		MissionType::STRONGHOLD,
		2000, 10, 0, 8000, 0,
		"Gang Stronghold",
		"Your gang rivals are sure to have their own camp in the area.\nPay them a visit and liberate their loot stash.",
		COMPCOUNT_LARGE,
		false,
		MUSIC_BASIN,
		false
	};
	return missiondata;
}

Stronghold::Stronghold()
{
	DioScope(DIOTAG);
}

Stronghold::~Stronghold()
{
	DioScope(DIOTAG);

	TASK::_SET_SCENARIO_TYPE_ENABLED_HASH(MISC::GET_HASH_KEY("RANSACK_ATTACHED_CHEST_LARGE"), true);
}

void Stronghold::Setup(int campid, const std::string& gangstr, const std::vector<MissionCompanionInfo>& companions)
{
	DioScope(DIOTAG "campid %d gangstr %s", campid, gangstr.c_str());

	GangName = gangstr;
	GangMemberTemplates = companions;

	if (!SetMissionParamsForCamp(campid))
	{
		ExitMissionFlag = M_EXIT_ERROR_TERMINATE;
		return;
	}

	CommonInit();

	MusicLoopOverride = true;
	MusicOverride = true;
}

bool Stronghold::SetMissionParamsForCamp(int camp)
{
	SetGenericLawModels(camp);

	bool success = false;

	StrongholdData variant1, variant2, variant3;
	
	switch (camp)
	{
		case DISTRICT_GRIZZLIES_WEST:
			GangModel = "G_M_M_UNIMICAHGOONS_01";
			
			variant1.DebugName = "Ewing Basin";
			variant1.BlipPoint = {-1894.46f, 1347.59f, 200.03f};
			variant1.BlipRadius = 70.0f;
			variant1.GangSpawns = {
				{{-1881.96f, 1321.91f, 201.44f}, 18},
				{{-1875.86f, 1333.44f, 202.24f}, 169},
				{{-1876.44f, 1330.15f, 202.22f}, 285},
				{{-1893.91f, 1331.31f, 199.10f}, 268},
				{{-1918.41f, 1332.57f, 202.82f}, 269},
				{{-1907.46f, 1349.85f, 201.35f}, 251},
				{{-1904.19f, 1347.86f, 200.89f}, 50},
				{{-1898.95f, 1359.38f, 203.35f}, 265},
				{{-1890.09f, 1371.76f, 206.33f}, 297},
				{{-1884.55f, 1351.53f, 203.28f}, 338},
				{{-1883.82f, 1354.94f, 203.28f}, 176},
				{{-1899.31f, 1338.40f, 200.70f}, 307},
				{{-1893.95f, 1335.05f, 200.10f}, 303},
				{{-1875.33f, 1374.63f, 205.02f}, 75},
				{{-1878.49f, 1375.62f, 205.09f}, 255},
				{{-1898.98f, 1336.01f, 203.93f}, 0}
			};
			variant1.GangSniperSpawns = {
				{{-1876.14f, 1359.67f, 211.49f}, 138},
				{{-1910.05f, 1361.17f, 207.61f}, 230}
			};
			variant1.GangReinforcementSpawns = {
				{-1932.95f, 1288.51f, 196.80f}
			};
			variant1.StashSpawns = {
				{{-1889.97f, 1373.63f, 205.51f}, 265},
				{{-1883.61f, 1352.00f, 202.27f}, 260},
				{{-1901.60f, 1360.45f, 202.50f}, 350},
				{{-1893.10f, 1333.38f, 199.14f}, 270}
			};
			variant1.StashBlips = {
				{-1890.94f, 1371.76f, 205.37f},
				{-1884.26f, 1354.23f, 202.30f},
				{-1899.65f, 1358.56f, 202.51f},
				{-1906.68f, 1349.00f, 201.23f},
				{-1917.76f, 1332.58f, 199.79f},
				{-1896.59f, 1332.19f, 199.10f}
			};
			variant1.StashAmount = 7000;

			variant2.DebugName = "Mount Hagen";
			variant2.BlipPoint = {-1626.16f, 1246.48f, 347.18f};
			variant2.BlipRadius = 50.0f;
			variant2.GangSpawns = {
				{{-1637.09f, 1215.54f, 351.42f}, 95},
				{{-1644.63f, 1227.50f, 350.81f}, 341},
				{{-1638.06f, 1225.66f, 350.45f}, 195},
				{{-1649.51f, 1247.08f, 350.04f}, 338},
				{{-1646.50f, 1249.84f, 350.03f}, 126},
				{{-1644.31f, 1264.21f, 353.60f}, 312},
				{{-1631.02f, 1253.04f, 347.27f}, 225},
				{{-1629.55f, 1251.32f, 347.45f}, 41},
				{{-1621.96f, 1257.35f, 347.48f}, 153},
				{{-1623.86f, 1244.32f, 346.87f}, 11},
				{{-1591.58f, 1245.75f, 338.40f}, 153}
			};
			variant2.GangSniperSpawns = {
				{{-1657.18f, 1236.56f, 357.01f}, 238},
				{{-1624.94f, 1286.66f, 370.81f}, 163},
				{{-1587.30f, 1239.94f, 338.20f}, 267}
			};
			variant2.GangReinforcementSpawns = {
				{-1573.42f, 1245.24f, 330.86f},
				{-1641.69f, 1300.82f, 359.03f},
				{-1671.62f, 1212.33f, 343.01f}
			};
			variant2.GangReinforcementSpawns = {variant2.GangReinforcementSpawns[rand() % variant2.GangReinforcementSpawns.size()]};
			variant2.StashSpawns = {
				{{-1633.37f, 1213.69f, 351.43f}, 220},
				{{-1627.13f, 1259.51f, 347.66f}, 130},
				{{-1627.30f, 1256.57f, 347.52f}, 0},
				{{-1587.88f, 1242.78f, 338.50f}, 60}
			};
			variant2.StashBlips = {
				{-1634.10f, 1214.45f, 351.42f},
				{-1631.14f, 1230.06f, 351.06f},
				{-1645.60f, 1227.45f, 350.81f},
				{-1647.81f, 1249.07f, 350.08f},
				{-1643.00f, 1263.99f, 353.63f},
				{-1623.94f, 1255.01f, 347.50f},
				{-1592.89f, 1243.73f, 338.45f}
			};
			variant2.StashAmount = 5500;

			variant3.DebugName = "Chez Porter";
			variant3.BlipPoint = {-406.82f, 1720.55f, 215.25f};
			variant3.BlipRadius = 60.0f;
			variant3.ExtraProps = {
				{"pg_ambient_camp_add_packwagonconvoy01", true, {-414.36f, 1732.24f, 215.25f}, 183,
					{
						{{-412.34f, 1721.58f, 214.06f}, {0.00f, 0.00f, 14.00f}, {5.13f, 2.52f, 4.12f}},
						{{-417.16f, 1724.43f, 214.06f}, {0.00f, 0.00f, 95.50f}, {4.02f, 2.11f, 4.12f}}
					}
				},
				{"p_sandbagcover01x", false, {-412.29f, 1744.51f, 215.15f}, 25},
				{"p_sandbagcover01x", false, {-414.19f, 1744.02f, 215.15f}, 5}
			};
			variant3.GangSpawns = {
				{{-403.72f, 1711.29f, 214.80f}, 206},
				{{-413.52f, 1717.85f, 215.25f}, 4},
				{{-413.32f, 1724.84f, 215.23f}, 12},
				{{-415.35f, 1727.62f, 215.17f}, 232},
				{{-424.37f, 1734.38f, 215.57f}, 207},
				{{-421.43f, 1736.31f, 215.56f}, 220},
				{{-395.38f, 1720.16f, 215.12f}, 74},
				{{-397.03f, 1725.51f, 215.43f}, 37},
				{{-401.85f, 1730.97f, 215.45f}, 188},
				{{-404.07f, 1735.99f, 215.19f}, 47},
				{{-412.31f, 1750.75f, 215.25f}, 213},
				{{-417.03f, 1747.92f, 215.26f}, 199},
				{{-413.91f, 1756.20f, 215.26f}, 183}
			};
			variant3.GangSniperSpawns = {{{-414.75f, 1749.31f, 219.27f}, 204}};
			variant3.GangMaximSpawns = {{{-413.57f, 1745.20f, 215.17f}, 201}};
			variant3.GangReinforcementSpawns = {
				{-411.31f, 1675.42f, 213.10f},
				{-390.36f, 1689.85f, 209.94f}
			};
			variant3.StashSpawns = {
				{{-391.22f, 1731.80f, 215.44f}, 20},
				{{-403.63f, 1733.82f, 215.45f}, 20},
				{{-424.95f, 1737.37f, 215.57f}, 30},
				{{-418.56f, 1752.93f, 215.26f}, 105}
			};
			variant3.StashBlips = {
				{-397.36f, 1726.20f, 215.43f},
				{-399.33f, 1732.35f, 215.43f},
				{-391.76f, 1730.06f, 215.44f},
				{-412.83f, 1714.83f, 215.21f},
				{-423.65f, 1736.64f, 215.56f},
				{-415.14f, 1752.39f, 215.25f}
			};
			variant3.StashAmount = 8000;

			AllAvailableVariants = {variant1, variant2, variant3};
			success = true;
			break;

		case DISTRICT_CUMBERLAND_FOREST:
			GangModel = "G_M_M_UNICRIMINALS_01";

			variant1.DebugName = "Six Point Cabin";
			variant1.BlipPoint = {-45.89f, 1227.01f, 170.94f};
			variant1.BlipRadius = 60.0f;
			variant1.GangSpawns = {
				{{-53.83f, 1251.88f, 171.12f}, 316},
				{{-64.18f, 1238.45f, 169.77f}, 217},
				{{-60.27f, 1239.10f, 169.78f}, 120},
				{{-39.65f, 1236.00f, 171.40f}, 291},
				{{-37.10f, 1227.24f, 171.66f}, 265},
				{{-29.55f, 1232.26f, 171.91f}, 185},
				{{-25.81f, 1229.24f, 172.06f}, 123},
				{{-26.38f, 1223.55f, 172.11f}, 30},
				{{-30.79f, 1219.20f, 171.97f}, 30},
				{{-34.58f, 1216.95f, 171.80f}, 158},
				{{-37.60f, 1220.68f, 171.66f}, 308},
				{{-20.18f, 1234.55f, 172.14f}, 253},
				{{-16.54f, 1216.94f, 172.61f}, 40},
				{{-63.37f, 1232.45f, 169.75f}, 212}
			};
			variant1.GangReinforcementSpawns = {
				{-71.62f, 1141.43f, 162.30f},
				{-99.31f, 1277.45f, 168.49f}
			};
			variant1.StashSpawns = {
				{{-23.69f, 1223.09f, 172.21f}, 134},
				{{-16.56f, 1233.40f, 172.23f}, 194},
				{{-29.07f, 1237.47f, 171.87f}, 194},
				{{-37.92f, 1214.28f, 171.58f}, 268},
				{{-62.94f, 1240.45f, 169.81f}, 22},
				{{-67.04f, 1237.42f, 169.78f}, 42},
				{{-62.01f, 1235.38f, 169.83f}, 216}
			};
			variant1.StashBlips = {
				{-63.23f, 1237.87f, 169.77f},
				{-23.69f, 1223.09f, 172.21f},
				{-16.56f, 1233.40f, 172.23f},
				{-29.07f, 1237.47f, 171.87f},
				{-37.92f, 1214.28f, 171.58f}
			};
			variant1.StashAmount = 8000;

			variant2.DebugName = "Firwood Rise";
			variant2.BlipPoint = {215.55f, 996.83f, 188.82f};
			variant2.BlipRadius = 60.0f;
			variant2.ExtraProps = {
				{"pg_ambient_campfireexconfed01x", true, {202.42f, 998.13f, 188.88f}, 20,
					{
						{{207.15f, 996.23f, 188.42f}, {-0.00f, 0.00f, 57.50f}, {4.63f, 2.41f, 3.42f}},
						{{198.08f, 994.87f, 187.91f}, {-0.00f, 0.00f, 28.65f}, {4.63f, 3.22f, 4.33f}},
						{{196.93f, 999.52f, 187.91f}, {-0.00f, 0.00f, -7.60f}, {4.63f, 3.22f, 4.33f}},
						{{203.38f, 1004.12f, 187.91f}, {-0.00f, 0.00f, -2.40f}, {3.42f, 4.63f, 4.33f}}
					}
				}
			};
			variant2.GangSpawns = {
				{{215.80f, 1011.63f, 187.98f}, 343},
				{{203.91f, 1000.74f, 188.86f}, 176},
				{{200.35f, 1000.91f, 188.80f}, 223},
				{{200.34f, 997.92f, 188.96f}, 280},
				{{203.09f, 994.90f, 188.99f}, 190},
				{{205.30f, 993.02f, 189.06f}, 146},
				{{202.88f, 986.16f, 189.27f}, 209},
				{{235.37f, 991.50f, 188.56f}, 270},
				{{224.68f, 988.13f, 189.89f}, 260},
				{{218.57f, 985.71f, 189.90f}, 175},
				{{219.89f, 987.01f, 189.90f}, 349}
			};
			variant2.GangReinforcementSpawns = {
				{183.53f, 917.44f, 195.28f}
			};
			variant2.StashSpawns = {
				{{218.68f, 987.51f, 189.91f}, 350},
				{{225.25f, 986.31f, 189.89f}, 259},
				{{222.21f, 987.71f, 189.89f}, 80},
				{{207.16f, 996.26f, 190.08f}, 150},
				{{199.16f, 992.79f, 189.04f}, 42},
				{{201.57f, 1003.72f, 188.68f}, 268}
			};
			variant2.StashBlips = {
				{202.54f, 998.19f, 188.97f},
				{221.44f, 986.38f, 189.88f}
			};
			variant2.StashAmount = 6500;

			AllAvailableVariants = {variant1, variant2};
			success = true;
			break;

		case DISTRICT_BLUEGILL_MARSH:
			GangModel = "G_M_M_UNICRIMINALS_01";

			variant1.DebugName = "Canebreak Manor";
			variant1.BlipPoint = {2494.50f, -419.43f, 43.35f};
			variant1.BlipRadius = 90.0f;
			variant1.GangSpawns = {
				{{2498.70f, -419.89f, 43.37f}, 38},
				{{2495.53f, -422.43f, 43.37f}, 115},
				{{2491.93f, -417.95f, 43.36f}, 37},
				{{2494.08f, -396.72f, 40.31f}, 4},
				{{2480.23f, -410.93f, 40.07f}, 62},
				{{2487.50f, -429.04f, 40.60f}, 347},
				{{2486.97f, -426.23f, 40.66f}, 191}
			};
			variant1.GangSniperSpawns = {
				{{2488.76f, -420.41f, 43.24f}, 79},
				{{2495.71f, -415.30f, 43.36f}, 104},
				{{2492.35f, -426.03f, 43.26f}, 194}
			};
			variant1.GangReinforcementSpawns = {
				{2505.08f, -316.79f, 41.27f},
				{2470.98f, -519.28f, 40.42f}
			};
			variant1.StashSpawns = {
				{{2494.56f, -420.73f, 40.57f}, 126.0f},
				{{2489.67f, -423.99f, 40.99f}, 302.0f},
				{{2493.60f, -423.36f, 43.24f}, 302.0f},
				{{2499.17f, -422.75f, 43.38f}, 216.0f}
			};
			variant1.StashBlips = {
				{2494.50f, -419.43f, 43.35f}
			};
			variant1.StashAmount = 7000;

			variant2.DebugName = "Macombs End";
			variant2.BlipPoint = {1886.90f, -732.20f, 40.97f};
			variant2.BlipRadius = 80.0f;
			variant2.ExtraProps = {
				{"pg_stuckwagon01x", true, {1902.85f, -723.45f, 41.13f}, 113, {}},
				{"p_sandbagcover01x", false, {1888.46f, -735.37f, 40.94f}, 0},
				{"p_sandbagcover01x", false, {1890.48f, -735.35f, 40.91f}, 0},
				{"p_sandbagcover01x", false, {1885.90f, -739.73f, 40.87f}, 0},
				{"p_sandbagcover01x", false, {1883.79f, -739.74f, 40.96f}, 0},
				{"p_sandbagcover01x", false, {1883.82f, -743.93f, 40.95f}, 0},
				{"p_sandbagcover01x", false, {1885.89f, -743.96f, 40.85f}, 0},
				{"p_sandbagcover01x", false, {1889.81f, -744.03f, 40.88f}, 0},
				{"p_sandbagcover01x", false, {1887.44f, -753.40f, 41.31f}, 0},
				{"p_sandbagcover01x", false, {1888.28f, -753.28f, 40.48f}, 0},
				{"p_sandbagcover01x", false, {1886.20f, -753.31f, 40.49f}, 0},
				{"p_cartmaximgun01x", false, {1887.54f, -754.80f, 40.40f}, 180},
				{"p_coverboar01x", false, {1886.97f, -754.67f, 41.18f}, 0},
				{"p_coverboar01x", false, {1888.09f, -754.67f, 41.18f}, 0},
				{"p_coverboar01x", false, {1888.09f, -755.50f, 41.18f}, 0},
				{"p_coverboar01x", false, {1886.98f, -755.50f, 41.18f}, 0}
			};
			variant2.GangSpawns = {
				{{1890.37f, -721.12f, 40.92f}, 4},
				{{1888.56f, -736.60f, 40.93f}, 2},
				{{1883.95f, -740.83f, 40.93f}, 0},
				{{1883.75f, -745.91f, 40.95f}, 260},
				{{1885.82f, -746.39f, 40.85f}, 72},
				{{1892.48f, -746.21f, 40.88f}, 185},
				{{1894.21f, -737.26f, 41.35f}, 77},
				{{1896.26f, -731.32f, 41.11f}, 305},
				{{1877.15f, -734.71f, 41.59f}, 347},
				{{1881.11f, -755.00f, 40.68f}, 161},
				{{1891.34f, -772.90f, 41.29f}, 190},
				{{1868.92f, -751.17f, 41.80f}, 50},
				{{1867.93f, -749.04f, 42.10f}, 186}
			};
			variant2.GangMaximSpawns = {{{1887.54f, -755.01f, 41.25f}, 0}};
			variant2.GangReinforcementSpawns = {
				{1900.11f, -673.72f, 41.15f},
				{1919.69f, -698.97f, 40.86f}
			};
			variant2.StashSpawns = {
				{{1873.90f, -741.69f, 41.12f}, 272},
				{{1879.82f, -753.34f, 40.67f}, 0},
				{{1892.05f, -743.04f, 41.32f}, 94},
				{{1876.86f, -734.44f, 41.70f}, 0},
				{{1875.67f, -739.65f, 41.74f}, 98},
				{{1879.21f, -740.48f, 41.65f}, 268}
			};
			variant2.StashBlips = {
				{1895.39f, -740.36f, 41.16f},
				{1878.41f, -738.42f, 45.49f},
				{1878.63f, -750.55f, 45.32f}
			};
			variant2.StashAmount = 8000;

			AllAvailableVariants = {variant1, variant2};
			success = true;
			break;

		case DISTRICT_RIO_BRAVO:
			GangModel = "G_M_M_UNICRIMINALS_01";

			variant1.DebugName = "Solomon's Folly";
			variant1.BlipPoint = {-5407.22f, -3654.10f, -23.12f};
			variant1.BlipRadius = 75.0f;
			variant1.GangSpawns = {
				{{-5386.11f, -3655.35f, -23.21f}, 287},
				{{-5390.76f, -3668.02f, -22.99f}, 15},
				{{-5391.09f, -3663.39f, -23.12f}, 197},
				{{-5415.04f, -3641.57f, -23.12f}, 82},
				{{-5417.03f, -3638.67f, -23.13f}, 166},
				{{-5420.38f, -3640.71f, -23.14f}, 278},
				{{-5424.26f, -3645.43f, -23.15f}, 300},
				{{-5435.72f, -3648.88f, -23.13f}, 49},
				{{-5418.21f, -3670.77f, -23.03f}, 154},
				{{-5420.46f, -3672.84f, -22.95f}, 313}
			};
			variant1.GangMaximSpawns = {{{-5420.50f, -3655.02f, -19.73f}, 270.1f}};
			variant1.GangReinforcementSpawns = {
				{-5311.43f, -3672.53f, -23.01f},
				{-5315.82f, -3681.62f, -23.01f}
			};
			variant1.StashSpawns = {
				{{-5396.36f, -3668.70f, -23.11f}, 178.0f},
				{{-5420.88f, -3650.52f, -23.01f}, 284.0f},
				{{-5427.68f, -3652.41f, -23.10f}, 8.0f}
			};
			variant1.StashBlips = {
				{-5424.74f, -3653.61f, -23.15f},
				{-5395.45f, -3666.08f, -23.12f}
			};
			variant1.StashAmount = 6500;

			variant2.DebugName = "Fort Mercer";
			variant2.BlipPoint = {-4219.28f, -3453.82f, 40.49f};
			variant2.BlipRadius = 95.0f;
			variant2.GangSpawns = {
				{{-4222.48f, -3510.47f, 36.09f}, 211},
				{{-4245.37f, -3484.60f, 36.03f}, 337},
				{{-4244.18f, -3481.65f, 36.05f}, 149},
				{{-4224.10f, -3481.10f, 36.09f}, 340},
				{{-4219.64f, -3484.21f, 36.09f}, 47},
				{{-4228.53f, -3469.30f, 39.96f}, 181},
				{{-4263.21f, -3468.62f, 40.25f}, 59},
				{{-4240.95f, -3469.70f, 40.48f}, 215},
				{{-4212.30f, -3455.29f, 40.48f}, 335},
				{{-4207.84f, -3457.72f, 40.48f}, 339},
				{{-4178.92f, -3455.91f, 40.47f}, 245},
				{{-4189.23f, -3441.64f, 40.48f}, 359},
				{{-4209.12f, -3435.42f, 36.09f}, 121},
				{{-4187.88f, -3429.62f, 36.08f}, 28},
				{{-4233.17f, -3417.22f, 40.48f}, 61},
				{{-4189.80f, -3412.56f, 40.48f}, 315},
				{{-4181.74f, -3424.56f, 41.48f}, 308}
			};
			variant2.GangSniperSpawns = {
				{{-4185.88f, -3417.38f, 41.50f}, 308},
				{{-4230.85f, -3427.46f, 44.48f}, 247},
				{{-4230.97f, -3444.24f, 44.49f}, 282},
				{{-4217.43f, -3465.82f, 43.55f}, 170},
				{{-4214.06f, -3505.00f, 41.66f}, 179}
			};
			variant2.GangReinforcementSpawns = {
				{-4209.76f, -3531.19f, 35.85f},
				{-4154.73f, -3404.07f, 36.16f},
				{-4200.01f, -3394.67f, 36.09f}
			};
			variant2.StashSpawns = {
				{{-4261.36f, -3475.90f, 36.09f}, 110.0f},
				{{-4238.96f, -3442.16f, 42.59f}, 334.0f},
				{{-4185.25f, -3458.15f, 40.49f}, 60.0f},
			};
			variant2.StashBlips = {
				{-4261.36f, -3475.90f, 36.09f},
				{-4238.96f, -3442.16f, 42.59f},
				{-4185.25f, -3458.15f, 40.49f}
			};
			variant2.StashAmount = 8000;

			AllAvailableVariants = {variant1, variant2};
			success = true;
			break;
	}

	if (success)
	{
		ChosenVariant = AllAvailableVariants[rand() % AllAvailableVariants.size()];

		StashIndex = rand() % ChosenVariant.StashSpawns.size();

		DioTrace("Chose %s Stash %d", ChosenVariant.DebugName.c_str(), StashIndex);

		ScriptsKillList = {
			MISC::GET_HASH_KEY("chezporter"),
			MISC::GET_HASH_KEY("dinolady")
		};
	}
	else
	{
		DioTrace(DIOTAG1("ERROR") "CANNOT SET MISSION %d PARAMS FOR CAMP %d - ABORTING MISSION", Stronghold::GetStaticMissionData().StaticMissionID, camp);
	}

	return success;
}

void Stronghold::CommonInit()
{
	const MissionStaticData& thisMissionData = Stronghold::GetStaticMissionData();
	SetVarsFromStaticData(thisMissionData);

	ObjectivesList = {
		"Approach the ~COLOR_YELLOW~stronghold",
		"Eliminate the rival ~COLOR_RED~gang",
		"Find and steal the ~COLOR_YELLOW~money stash",
		"Eliminate the ~COLOR_RED~gang reinforcements"
	};

	MissionAssetsToLoad = {
		{"PED", GangModel},
		{"VEH", "GATLINGMAXIM02"},
		{"OBJ", "s_lootablebedchest"},
		{"OBJ", "p_moneybag02x"},
	};

	for (ExtraProps prop : ChosenVariant.ExtraProps)
	{
		bool duplicate = false;

		for (MissionAsset asset : MissionAssetsToLoad)
		{
			if (prop.model == asset.asset)
			{
				duplicate = true;
				break;
			}
		}

		if (!duplicate)
		{
			MissionAssetsToLoad.push_back({prop.propset ? "SET" : "OBJ", prop.model});
		}
	}

	LoadAllMissionAssets();
	SpawnGangMembers(thisMissionData.NumMembersToSpawn);

	WAIT(2000);
}

void Stronghold::ObjectiveInit_0()
{
	ObjectiveSet(0);
	ActivateObjective_0 = false;
	RunObjective_0 = true;

	CurrentMusicStage = STAGE_CALM;
	MusicOverride = true;

	Common::ClearArea(ChosenVariant.BlipPoint, 100.0f);

	for (ExtraProps prop : ChosenVariant.ExtraProps)
	{
		if (prop.propset)
		{
			SpawnPropSet(prop.model, prop.pos, prop.rot);
		}
		else
		{
			SpawnProp(prop.model, prop.pos, prop.rot);
		}

		for (const NavBlocker& blocker : prop.blockers)
		{
			SpawnVolume(blocker.pos, blocker.rot, blocker.scl, true);
		}
	}

	AreaBlip = ObjectiveBlipCoords(ChosenVariant.BlipPoint, ChosenVariant.BlipRadius, true, ObjectivesList[0]);

	for (int i = 0; i < ChosenVariant.GangSpawns.size(); ++i)
	{
		const LocationData& spawn = ChosenVariant.GangSpawns[i];

		Ped gangster = SpawnPed(GangModel, spawn.p, spawn.h, 255);

		if (i % 7 == 0)
		{
			SetPedType(gangster, PedType::SHOTGUN_WEAK);
		}
		else if (i % 6 == 0)
		{
			SetPedType(gangster, PedType::REPEATER_REGULAR);
		}
		else if (i % 4 == 0)
		{
			SetPedType(gangster, PedType::REPEATER_WEAK);
		}
		else
		{
			SetPedType(gangster, PedType::SIDEARM_WEAK);
		}

		PED::SET_PED_RELATIONSHIP_GROUP_HASH(gangster, REL_IGNORE_EVERYTHING);

		PED::SET_PED_COMBAT_MOVEMENT(gangster, 1);
		
		Gangsters.emplace_back(gangster);
	}

	for (const LocationData& spawn : ChosenVariant.GangSniperSpawns)
	{
		Ped sniper = SpawnPed(GangModel, spawn.p, spawn.h, 255);

		SetPedType(sniper, PedType::SNIPER_REGULAR);

		PED::SET_PED_RELATIONSHIP_GROUP_HASH(sniper, REL_IGNORE_EVERYTHING);

		ENTITY::SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(sniper, true);

		PED::SET_PED_COMBAT_MOVEMENT(sniper, 0);

		Gangsters.emplace_back(sniper);
	}

	for (const LocationData& data : ChosenVariant.GangMaximSpawns)
	{
		Vehicle maxim = SpawnVehicle("GATLINGMAXIM02", data.p, data.h, true);

		ENTITY::SET_ENTITY_INVINCIBLE(maxim, true);
		Common::SetEntityProofs(maxim, true, true);
		//VEHICLE::SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(maxim, false);

		Maxims.emplace_back(maxim);
	}

	TASK::_SET_SCENARIO_TYPE_ENABLED_HASH(MISC::GET_HASH_KEY("RANSACK_ATTACHED_CHEST_LARGE"), false);

	Object stashbox = SpawnProp("s_lootablebedchest", ChosenVariant.StashSpawns[StashIndex].p, ChosenVariant.StashSpawns[StashIndex].h);
	Common::SetEntityProofs(stashbox, true, true);
	Common::SetEntityProofs(SpawnProp("p_moneybag02x", ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(stashbox, -0.1f, 0.0f, 0.17f), 0), true, true);
	Common::SetEntityProofs(SpawnProp("p_moneybag02x", ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(stashbox, 0.1f, 0.1f, 0.17f), 90), true, true);

	if (ChosenVariant.DebugName == "Mount Hagen")
	{
		GlobalVarsAccess::SET_WORLD_STATE_FLAG(WorldState::WS_MICAH_HIDEOUT, true);
		GlobalVarsAccess::SET_WORLD_STATE_FLAG(WorldState::WS_MICAH_HIDEOUT_ABANDON, true);
	}

#ifdef DEBUG_FAST_MISSION
	ENTITY::_SET_ENTITY_COORDS_AND_HEADING(PlayerPed, ChosenVariant.GangReinforcementSpawns[0],
		MISC::GET_HEADING_FROM_VECTOR_2D(ChosenVariant.BlipPoint.x - ChosenVariant.GangReinforcementSpawns[0].x, ChosenVariant.BlipPoint.y - ChosenVariant.GangReinforcementSpawns[0].y),
		true, true, true);
	ENTITY::PLACE_ENTITY_ON_GROUND_PROPERLY(PlayerPed, true);
	CAM::SET_GAMEPLAY_CAM_RELATIVE_HEADING(0, 1);
	CAM::SET_GAMEPLAY_CAM_RELATIVE_PITCH(0, 1);
#endif
}

void Stronghold::ObjectiveLogic_0()
{
	if (Common::GetDistBetweenCoords(PlayerCoords, ChosenVariant.BlipPoint) < ChosenVariant.BlipRadius - 5)
	{
		MAP::REMOVE_BLIP(&AreaBlip);

		RunObjective_0 = false;
		ActivateObjective_1 = true;
	}
}

void Stronghold::ObjectiveInit_1()
{
	ObjectiveSet(1);
	ActivateObjective_1 = false;
	RunObjective_1 = true;

	DisableLaw();

	CheckAbandon = true;
	AbandonCoords = ChosenVariant.BlipPoint;
	AbandonRadius = ChosenVariant.BlipRadius;

	for (Ped gangster : Gangsters)
	{
		MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_ENEMY, gangster);
		PED::SET_PED_RELATIONSHIP_GROUP_HASH(gangster, REL_PLAYER_ENEMY);

		PED::SET_PED_SEEING_RANGE(gangster, 25.0f);

		PED::SET_PED_CONFIG_FLAG(gangster, PCF_DisableLadderClimbing, true);

		PED::SET_PED_SPHERE_DEFENSIVE_AREA(gangster, ENTITY::GET_ENTITY_COORDS(gangster, true, true), 8, false, false, false);

		TASK::TASK_STAND_GUARD(gangster, ENTITY::GET_ENTITY_COORDS(gangster, true, true), ENTITY::GET_ENTITY_HEADING(gangster), "");
	}

	AllowCompanionManualControl = true;
}

void Stronghold::ObjectiveLogic_1()
{
	if (!GuardsAlerted &&
		(
			FIRE::IS_EXPLOSION_IN_SPHERE(-1, ChosenVariant.BlipPoint, ChosenVariant.BlipRadius) ||
			PED::IS_ANY_PED_SHOOTING_IN_AREA(ChosenVariant.BlipPoint.x - 100, ChosenVariant.BlipPoint.y - 100, ChosenVariant.BlipPoint.z - 100,
											ChosenVariant.BlipPoint.x + 100, ChosenVariant.BlipPoint.y + 100, ChosenVariant.BlipPoint.z + 100, false, false)
		)
	)
	{
		DioTrace("Guards alerted by gunfire or explosion");
		GuardsAlerted = true;
	}

	if (!GuardsAlerted && GuardSpotTime == 0)
	{
		for (Ped gangster : Gangsters)
		{
			bool spottedplayer = PED::CAN_PED_SEE_ENTITY(gangster, PlayerPed, true, true) == 1;

			if (GuardSpotTime == 0 && PED::IS_PED_IN_COMBAT(gangster, PlayerPed))
			{
				DioTrace("Guards alerted by player in combat");
				GuardsAlerted = true;
				spottedplayer = true;
				GuardSpotTime = MISC::GET_GAME_TIMER();
			}

			if (GuardSpotTime == 0 && spottedplayer)
			{
				DioTrace("Guard spotted player");
				GuardSpotTime = MISC::GET_GAME_TIMER();

				ScriptedSpeechParams params{"OPENS_FIRE", 0, 0, MISC::GET_HASH_KEY("SPEECH_PARAMS_BEAT_SHOUTED_CLEAR_NOSUB"), 0, true, 0, 0 };
				AUDIO::PLAY_PED_AMBIENT_SPEECH_NATIVE(gangster, (Any*)&params);
				
				TASK::CLEAR_PED_TASKS(gangster, true, true);
				TASK::TASK_AIM_AT_ENTITY(gangster, PlayerPed, 9000, 0, 0);
			}
		}
	}
	else if (!GuardsAlerted && MISC::GET_GAME_TIMER() - GuardSpotTime > 2000)
	{
		DioTrace("Guards alerted by spotted player");
		GuardsAlerted = true;
	}
	else if (!GuardsCombat && GuardsAlerted)
	{
		DioTrace("Guards in combat");
		GuardsCombat = true;

		AllowCompanionManualControl = false;
		CompanionsBeginCombat();

		for (Ped gangster : Gangsters)
		{
			PED::SET_PED_SEEING_RANGE(gangster, 150.0f); 

			TASK::CLEAR_PED_TASKS(gangster, true, true);

			PED::SET_PED_SPHERE_DEFENSIVE_AREA(gangster, ENTITY::GET_ENTITY_COORDS(gangster, true, true), 5.0f, 0, false, 0);

			PED::SET_PED_COMBAT_ATTRIBUTES(gangster, CA_PERMIT_CHARGE_BEYOND_DEFENSIVE_AREA, false);
			PED::SET_PED_COMBAT_ATTRIBUTES(gangster, CA_CAN_CHARGE, false);
			PED::SET_PED_COMBAT_ATTRIBUTES(gangster, CA_CAN_CHASE_TARGET_ON_FOOT, false);
			PED::SET_PED_COMBAT_ATTRIBUTES(gangster, CA_USE_COVER, true);
			PED::SET_PED_COMBAT_ATTRIBUTES(gangster, CA_SWITCH_TO_ADVANCE_IF_CANT_FIND_COVER, false);
			PED::SET_PED_COMBAT_ATTRIBUTES(gangster, CA_DISABLE_RETREAT_DUE_TO_TARGET_PROXIMITY, true);
			
			TASK::TASK_COMBAT_HATED_TARGETS(gangster, 100);
		}

		CurrentMusicStage = STAGE_ACTION1;
		MusicOverride = true;
	}
	
	for (Ped gangster : Gangsters)
	{
		if (PED::GET_PED_CONFIG_FLAG(gangster, PCF_DisableLadderClimbing, true) && PED::IS_PED_DEAD_OR_DYING(gangster, true))
		{
			DioTrace("Gangster down");
			PED::SET_PED_CONFIG_FLAG(gangster, PCF_DisableLadderClimbing, false);
			GangsterCasualties++;
		}
	}

	if (!Maxims.empty() && !MaximSpawned && GangsterCasualties >= 2)
	{
		DioTrace("Manning Maxim");

		PlayCompanionSpeech("GENERIC_SHOCKED_HIGH");

		for (Vehicle maxim : Maxims)
		{
			Ped gunner = SpawnPed(GangModel, ChosenVariant.BlipPoint, 0, 255);
			
			MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_ENEMY, gunner);
			PED::SET_PED_RELATIONSHIP_GROUP_HASH(gunner, REL_PLAYER_ENEMY);
			PED::SET_PED_CONFIG_FLAG(gunner, PCF_DisableLadderClimbing, true);
			Gangsters.emplace_back(gunner);

			SetPedType(gunner, PedType::SIDEARM_WEAK);

			TASK::CLEAR_PED_TASKS_IMMEDIATELY(gunner, true, true);
			EVENT::SET_DECISION_MAKER(gunner, MISC::GET_HASH_KEY("EMPTY"));

			PED::SET_PED_INTO_VEHICLE(gunner, maxim, SEAT_DRIVER);

			PED::SET_PED_COMBAT_ATTRIBUTES(gunner, CA_0xA21AD34D, true);
			PED::SET_PED_COMBAT_ATTRIBUTES(gunner, CA_0xA706084B, true);
			PED::SET_PED_COMBAT_ATTRIBUTES(gunner, CA_0x1CB77C49, false);
			PED::SET_PED_COMBAT_ATTRIBUTES(gunner, CA_LEAVE_VEHICLES, false);
			PED::SET_PED_CONFIG_FLAG(gunner, PCF_NoCriticalHits, false);
			PED::SET_PED_CONFIG_FLAG(gunner, PCF_OneShotWillKillPed, true);
			PED::SET_PED_CONFIG_FLAG(gunner, PCF_DontExitVehicleIfNoDraftAnimals, true);

			ENTITY::SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(gunner, true);

			PED::SET_PED_CAN_RAGDOLL(gunner, false);
			PED::SET_PED_FLEE_ATTRIBUTES(gunner, 512, true);

			PED::SET_PED_ACCURACY(gunner, 25);

			TASK::TASK_COMBAT_PED(gunner, PlayerPed, 0, 0);
		}

		MaximSpawned = true;
	}
	
	bool maximsDown = true;

	for (Vehicle maxim : Maxims)
	{
		if (!PED::IS_PED_DEAD_OR_DYING(VEHICLE::GET_DRIVER_OF_VEHICLE(maxim), true))
		{
			maximsDown = false;
			break;
		}
	}

	if (GangsterCasualties >= Gangsters.size() - 2 && maximsDown)
	{
		RunObjective_1 = false;
		ActivateObjective_2 = true;
	}
}

void Stronghold::ObjectiveInit_2()
{
	ActivateObjective_2 = false;
	RunObjective_2 = true;

	CurrentMusicStage = STAGE_CALM;
	MusicOverride = true;

	for (Ped fleer : Gangsters)
	{
		if (!PED::IS_PED_DEAD_OR_DYING(fleer, true))
		{
			PED::SET_PED_RELATIONSHIP_GROUP_HASH(fleer, REL_CIV);

			Blip dblip = MAP::GET_BLIP_FROM_ENTITY(fleer);
			MAP::REMOVE_BLIP(&dblip);

			PED::SET_PED_CONFIG_FLAG(fleer, PCF_DisableLadderClimbing, false);

			TASK::CLEAR_PED_TASKS(fleer, true, true);
			TASK::TASK_FLEE_COORD(fleer, ChosenVariant.BlipPoint, 2, 128, -1, -1, 0);
		}
	}

	Gangsters.clear();

	CommentTimer = MISC::GET_GAME_TIMER();
}

void Stronghold::ObjectiveLogic_2()
{
	if (CommentTimer > 0 && MISC::GET_GAME_TIMER() - CommentTimer > 2500)
	{
		PlayCompanionSpeech("CALLOUT_FORGET_FLEEING_NEUTRAL");
		
		for (const SpawnedGangMember& gangMember : SpawnedGangMembers)
		{
			if (gangMember.status == COMP_OK)
			{
				if (rand() % 100 < 50)
				{
					int sequence = 0;
					TASK::OPEN_SEQUENCE_TASK(&sequence);

					TASK::TASK_STAND_STILL(0, 3000);
					TASK::TASK_LOOT_NEAREST_ENTITY(0, ENTITY::GET_ENTITY_COORDS(gangMember.member, false, true), 1, -1);
					TASK::TASK_LOOT_NEAREST_ENTITY(0, ENTITY::GET_ENTITY_COORDS(gangMember.member, false, true), 1, -1);
					TASK::TASK_WANDER_IN_AREA(0, ChosenVariant.BlipPoint, ChosenVariant.BlipRadius * 0.25f, 1077936128, 1086324736, 0);

					TASK::CLOSE_SEQUENCE_TASK(sequence);
					TASK::TASK_PERFORM_SEQUENCE(gangMember.member, sequence);
					TASK::CLEAR_SEQUENCE_TASK(&sequence);
				}
				else
				{
					int sequence = 0;
					TASK::OPEN_SEQUENCE_TASK(&sequence);
					TASK::SET_SEQUENCE_TO_REPEAT(sequence, true);

					TASK::TASK_STAND_STILL(0, 3000);
					TASK::TASK_WANDER_IN_AREA(0, ChosenVariant.BlipPoint, ChosenVariant.BlipRadius * 0.25f, 1077936128, 1086324736, 0);

					TASK::CLOSE_SEQUENCE_TASK(sequence);
					TASK::TASK_PERFORM_SEQUENCE(gangMember.member, sequence);
					TASK::CLEAR_SEQUENCE_TASK(&sequence);
				}
			}
		}

		ObjectiveSet(2);

		for (const Vector3& stash : ChosenVariant.StashBlips)
		{
			StashBlips.push_back(ObjectiveBlipCoords(stash, 5.0f, false, ObjectivesList[2]));
		}

		TASK::_SET_SCENARIO_TYPE_ENABLED_HASH(MISC::GET_HASH_KEY("RANSACK_ATTACHED_CHEST_LARGE"), true);

		CommentTimer = 0;
	}

	if (Common::GetDistBetweenCoords(PlayerCoords, ChosenVariant.StashSpawns[StashIndex].p) < 2.0f && TASK::GET_RANSACK_SCENARIO_POINT_PED_IS_USING(PlayerPed) != 0)
	{
		RunObjective_2 = false;
		ActivateObjective_3 = true;
	}
}

void Stronghold::ObjectiveInit_3()
{
	ObjectiveSet(3);
	ActivateObjective_3 = false;
	RunObjective_3 = true;

	CurrentMusicStage = STAGE_ACTION2;
	MusicOverride = true;

	CompanionsClearTasks();

	if (ChosenVariant.DebugName == "Chez Porter")
	{
		SetCompanionPlayerCollision(false);

		const Vector3 coords = {-415.05f, 1751.50f, 215.25f};

		for (const SpawnedGangMember& m : SpawnedGangMembers)
		{
			PED::SET_PED_SPHERE_DEFENSIVE_AREA(m.member, coords, 10.0f, 0, false, 0);

			PED::SET_PED_COMBAT_ATTRIBUTES(m.member, CA_PERMIT_CHARGE_BEYOND_DEFENSIVE_AREA, false);
			PED::SET_PED_COMBAT_ATTRIBUTES(m.member, CA_CAN_CHARGE, false);
			PED::SET_PED_COMBAT_ATTRIBUTES(m.member, CA_CAN_CHASE_TARGET_ON_FOOT, false);
			PED::SET_PED_COMBAT_ATTRIBUTES(m.member, CA_USE_COVER, true);
			PED::SET_PED_COMBAT_ATTRIBUTES(m.member, CA_SWITCH_TO_ADVANCE_IF_CANT_FIND_COVER, false);
			PED::SET_PED_COMBAT_ATTRIBUTES(m.member, CA_DISABLE_RETREAT_DUE_TO_TARGET_PROXIMITY, true);

			int sequence = 0;
			TASK::OPEN_SEQUENCE_TASK(&sequence);

			TASK::TASK_GO_STRAIGHT_TO_COORD(0, coords, 10.0f, 0, 0, 0, 0);
			TASK::TASK_STAY_IN_COVER(0);

			TASK::CLOSE_SEQUENCE_TASK(sequence);
			TASK::TASK_PERFORM_SEQUENCE(m.member, sequence);
			TASK::CLEAR_SEQUENCE_TASK(&sequence);
		}
	}
	else
	{
		CompanionsRegroup();
	}

	CommentTimer = MISC::GET_GAME_TIMER();

	for (Blip b : StashBlips)
	{
		MAP::REMOVE_BLIP(&b);
	}

	StashBlips.clear();

	for (Vehicle maxim : Maxims)
	{
		VEHICLE::SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(maxim, true);
	}
}

void Stronghold::ObjectiveLogic_3()
{
	if (CommentTimer > 0 && MISC::GET_GAME_TIMER() - CommentTimer > 8000)
	{
		PlayCompanionSpeech("TAUNT_GEN_GROUP");

		for (const Vector3& spawn : ChosenVariant.GangReinforcementSpawns)
		{
			for (int i = 0; i < 5; ++i)
			{
				Ped gangster = SpawnPed(GangModel, Common::ApplyRandomSmallOffsetToCoords(spawn, 5.0f), 0, 255);

				switch (i)
				{
					case 0:
						SetPedType(gangster, PedType::SIDEARM_REGULAR);
						break;

					case 1:
						SetPedType(gangster, PedType::SHOTGUN_WEAK);
						break;

					default:
						SetPedType(gangster, PedType::REPEATER_REGULAR);
				}

				PED::SET_PED_RELATIONSHIP_GROUP_HASH(gangster, REL_PLAYER_ENEMY);
				MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_ENEMY, gangster);

				PED::SET_PED_COMBAT_MOVEMENT(gangster, 1);

				TASK::TASK_COMBAT_HATED_TARGETS(gangster, 100.0f);

				Gangsters.emplace_back(gangster);
			}
		}

		CompanionsBeginCombat();

		CommentTimer = 0;
	}
	else if (CommentTimer > 0)
	{
		return;
	}
	
	bool allDead = true;

	for (Ped reinforcement : Gangsters)
	{
		if (!PED::IS_PED_DEAD_OR_DYING(reinforcement, true))
		{
			if (Common::GetDistBetweenCoords(ENTITY::GET_ENTITY_COORDS(reinforcement, true, true), PlayerCoords) > 120)
			{
				DioTrace(DIOTAG1("WARNING") "Reinforcement got too far away, killing");
				PED::EXPLODE_PED_HEAD(reinforcement, WEAPON_REVOLVER_CATTLEMAN);
			}

			allDead = false;
			break;
		}
	}

	if (allDead)
	{
		CurrentMusicStage = STAGE_AFTERACTION;
		MusicOverride = true;
		UpdateMusic();

		CompanionsClearTasks();
		CompanionsRegroup();

		RunObjective_3 = false;
		MissionSuccess();
	}
}

void Stronghold::TrackRewards()
{
	RewardBounty_Active = RewardBounty_Potential;
	RewardMembers_Active = RewardMembers_Potential;

	RewardCash_Active = ChosenVariant.StashAmount;
}

void Stronghold::Update()
{
	// Six Point Cabin Door
	Common::SetDoorState(202296518, DOORSTATE_UNLOCKED);
	
	if (TimeMissionFail > 0 && MISC::GET_GAME_TIMER() - TimeMissionFail > TimeMissionFailDelay)
	{
		DioTrace("Exit Fail Triggered");

		ExitMissionFlag = M_EXIT_FAILURE;
	}
	else if (TimeMissionSuccess > 0 && MISC::GET_GAME_TIMER() - TimeMissionSuccess > TimeMissionSuccessDelay)
	{
		DioTrace("Exit Win Triggered");

		ExitMissionFlag = M_EXIT_SUCCESS;
	}
	else if (ActivateObjective_0)
	{
		ObjectiveInit_0();
	}
	else if (ActivateObjective_1)
	{
		ObjectiveInit_1();
	}
	else if (ActivateObjective_2)
	{
		ObjectiveInit_2();
	}
	else if (ActivateObjective_3)
	{
		ObjectiveInit_3();
	}
	else if (RunObjective_0)
	{
		ObjectiveLogic_0();
	}
	else if (RunObjective_1)
	{
		ObjectiveLogic_1();
	}
	else if (RunObjective_2)
	{
		ObjectiveLogic_2();
	}
	else if (RunObjective_3)
	{
		ObjectiveLogic_3();
	}
}
