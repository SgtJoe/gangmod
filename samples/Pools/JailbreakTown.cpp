
#include "precomp.h"
#include "JailbreakTown.h"

const MissionStaticData& JailbreakTown::GetStaticMissionData()
{
	static MissionStaticData missiondata = {
		MissionType::JAILBREAK_TOWN,
		2000, 12, 3500, 0, 8,
		"Town Jailbreak",
		"There's always prisoners in the local town jail.\nIf we free them, they can be persuaded to join us.",
		COMPCOUNT_SMALL,
		true,
		MUSIC_DEFAULT,
		false
	};
	return missiondata;
}

JailbreakTown::JailbreakTown()
{
	DioScope(DIOTAG);
}

JailbreakTown::~JailbreakTown()
{
	DioScope(DIOTAG);

	GlobalVarsAccess::SetRegionLocked(CurrentTownInfo.TownRegion, false);
}

void JailbreakTown::Setup(int campid, const std::string& gangstr, const std::vector<MissionCompanionInfo>& companions)
{
	DioScope(DIOTAG "campid %d gangstr %s", campid, gangstr.c_str());

	GangName = gangstr;
	GangMemberTemplates = companions;

	if (!SetMissionParamsForCamp(campid))
	{
		ExitMissionFlag = M_EXIT_ERROR_TERMINATE;
		return;
	}

	CommonInit();
}

bool JailbreakTown::SetMissionParamsForCamp(int camp)
{
	bool success = false;

	switch (camp)
	{
		case DISTRICT_GRIZZLIES_WEST:
		case DISTRICT_CUMBERLAND_FOREST:
			CurrentTownInfo = ValentineTownInfo;
			success = true;
			break;

		case DISTRICT_BLUEGILL_MARSH:
			CurrentTownInfo = SaintDenisTownInfo;
			success = true;
			break;

		case DISTRICT_RIO_BRAVO:
			CurrentTownInfo = ArmadilloTownInfo;
			success = true;
			break;
	}

	if (success)
	{
		ScriptsKillList = {CurrentTownInfo.TownScriptHash};

		SetGenericLawModels(CurrentTownInfo.TownDistrict);
	}
	else
	{
		DioTrace(DIOTAG1("ERROR") "CANNOT SET MISSION %d PARAMS FOR CAMP %d - ABORTING MISSION", JailbreakTown::GetStaticMissionData().StaticMissionID, camp);
	}

	return success;
}

void JailbreakTown::CommonInit()
{
	const MissionStaticData& thisMissionData = JailbreakTown::GetStaticMissionData();
	SetVarsFromStaticData(thisMissionData);

	MissionAssetsToLoad = {
		{"PED", PrisonerModel}
	};

	ObjectivesList = {
		"Eliminate the ~COLOR_RED~guards",
		"~COLOR_YELLOW~Loot the guards~COLOR_WHITE~ to find the cell keys",
		"Unlock all the ~COLOR_YELLOW~cells",
		"Eliminate the ~COLOR_RED~reinforcements"
	};

	LoadAllMissionAssets();
	SpawnGangMembers(thisMissionData.NumMembersToSpawn);

	WAIT(2000);
}

void JailbreakTown::SetUpJailGuard(Ped guard)
{
	PED::SET_PED_SEEING_RANGE(guard, 25.0f);

	EVENT::SET_DECISION_MAKER(guard, MISC::GET_HASH_KEY("EMPTY"));
	
	Blip guardBlip = MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_COP, guard);
	MAP::BLIP_ADD_MODIFIER(guardBlip, BLIP_MODIFIER_RADAR_EDGE_ALWAYS);

	PED::SET_PED_RELATIONSHIP_GROUP_HASH(guard, REL_PLAYER_ENEMY);

	PED::SET_PED_COMBAT_MOVEMENT(guard, 1);
	PED::SET_PED_COMBAT_ATTRIBUTES(guard, CA_PERMIT_CHARGE_BEYOND_DEFENSIVE_AREA, false);

	PED::SET_PED_SPHERE_DEFENSIVE_AREA(guard, CurrentTownInfo.JailCenter, 20.0f, false, false, false);

	TASK::TASK_STAND_GUARD(guard, ENTITY::GET_ENTITY_COORDS(guard, true, true), ENTITY::GET_ENTITY_HEADING(guard), "");

	Common::SetEntityProofs(guard, true, true);

	ENTITY::_0xE31FC20319874CB3(guard, 1, 0);
	ENTITY::_0xE31FC20319874CB3(guard, 1, 1);
	ENTITY::_0xE31FC20319874CB3(guard, 1, 2);
	DECORATOR::DECOR_SET_BOOL(guard, "scripted_loot_only", true);
	ENTITY::_0x8C03CD6B5E0E85E8(guard, MISC::GET_HASH_KEY("EMPTY"));

#ifdef DEBUG_FAST_MISSION
	PED::SET_PED_CONFIG_FLAG(guard, PCF_OneShotWillKillPed, true);
#endif
}

void JailbreakTown::ObjectiveInit_0()
{
	ObjectiveSet(0);
	ActivateObjective_0 = false;
	RunObjective_0 = true;

	CurrentMusicStage = STAGE_MUTED;
	UpdateMusic();

	GlobalVarsAccess::SetRegionLocked(CurrentTownInfo.TownRegion, false);

	ENTITY::_SET_ENTITY_COORDS_AND_HEADING(PlayerPed, CurrentTownInfo.StartLoc.p.x, CurrentTownInfo.StartLoc.p.y, CurrentTownInfo.StartLoc.p.z, CurrentTownInfo.StartLoc.h, true, true, true);

	Vector3 HorseStart = ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PlayerPed, 2, -4, 0);
	ENTITY::_SET_ENTITY_COORDS_AND_HEADING(PlayerHorse, HorseStart.x, HorseStart.y, HorseStart.z, CurrentTownInfo.StartLoc.h, true, true, true);

	ENTITY::PLACE_ENTITY_ON_GROUND_PROPERLY(PlayerPed, true);
	ENTITY::PLACE_ENTITY_ON_GROUND_PROPERLY(PlayerHorse, true);
	CAM::SET_GAMEPLAY_CAM_RELATIVE_HEADING(0, 1);
	CAM::SET_GAMEPLAY_CAM_RELATIVE_PITCH(0, 1);

	CurrentMusicStage = STAGE_START;
	UpdateMusic();

	Ped jailer = SpawnPed(GenericLawLeader, CurrentTownInfo.JailerSpawn.p, CurrentTownInfo.JailerSpawn.h, 255);
	SetPedType(jailer, PedType::SIDEARM_EXPERT);
	ActiveJailer = jailer;
	SetUpJailGuard(jailer);
	ActiveGuards.emplace_back(jailer);

	for (const LocationData& spawn : CurrentTownInfo.GuardSpawns)
	{
		Ped guard = SpawnPed(GenericLawFollower, spawn.p, spawn.h, 255);
		SetPedType(guard, PedType::REPEATER_EXPERT);
		SetUpJailGuard(guard);
		ActiveGuards.emplace_back(guard);
	}

	for (int i = 0; i < CurrentTownInfo.JailCells.size(); ++i)
	{
		for (const Vector3& spawn : CurrentTownInfo.JailCells[i].PrisonerSpawns)
		{
			Ped prisoner = SpawnPed(PrisonerModel, spawn, 0, 255);

			PED::SET_PED_RELATIONSHIP_GROUP_HASH(prisoner, REL_CIV);

			PED::SET_PED_CONFIG_FLAG(prisoner, PCF_DisableLadderClimbing, true);

			Blip pBlip = MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_COMPANION, prisoner);
			MAP::_SET_BLIP_NAME_FROM_PLAYER_STRING(pBlip, "Prisoner");

			SetPedType(prisoner, PedType::SIDEARM_EXPERT);
			WEAPON::REMOVE_ALL_PED_WEAPONS(prisoner, true, true);

			EVENT::SET_DECISION_MAKER(prisoner, MISC::GET_HASH_KEY("EMPTY"));

			PED::SET_PED_LASSO_HOGTIE_FLAG(prisoner, 0, false);
			PED::SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(prisoner, false);
			PED::SET_PED_CONFIG_FLAG(prisoner, PCF_DisableDeadEyeTagging, true);

			TASK::TASK_COWER(prisoner, -1, PlayerPed, "");
			PED::SET_PED_KEEP_TASK(prisoner, true);

			CurrentTownInfo.JailCells[i].SpawnedPrisoners.emplace_back(prisoner);

			ActivePrisoners.emplace_back(prisoner);
		}
	}

	if (ActivePrisoners.size() > 4)
	{
		DioTrace(DIOTAG1("ERROR") "TOTAL PRISONER COUNT SHOULD NOT BE MORE THAN 4");
	}

	CheckAbandon = true;
	AbandonCoords = CurrentTownInfo.JailCenter;
	AbandonRadius = 75.0f;

	AllowCompanionManualControl = true;

	AllowWanted = true;
	DisableLaw();
	SetDispatchService(false);
}

void JailbreakTown::ObjectiveLogic_0()
{
	DisableLaw();
	SetDispatchService(false);
	
	if (PED::IS_ANY_PED_SHOOTING_IN_AREA({CurrentTownInfo.JailCenter.x - 100.0f, CurrentTownInfo.JailCenter.y - 100.0f, CurrentTownInfo.JailCenter.z - 100.0f},
										{CurrentTownInfo.JailCenter.x + 100.0f, CurrentTownInfo.JailCenter.y + 100.0f, CurrentTownInfo.JailCenter.z + 100.0f}, false, false) ||
		FIRE::IS_EXPLOSION_IN_SPHERE(-1, CurrentTownInfo.JailCenter, 100.0f))
	{
		GuardsAlerted = true;
		AllowCompanionManualControl = false;
	}

	if (!GuardsAlerted && GuardSpotTime == 0)
	{
		for (Ped guard : ActiveGuards)
		{
			bool spottedplayer = PED::CAN_PED_SEE_ENTITY(guard, PlayerPed, true, true) == 1;

			if (GuardSpotTime == 0 && PED::IS_PED_IN_COMBAT(guard, PlayerPed))
			{
				GuardsAlerted = true;
				spottedplayer = true;
				GuardSpotTime = MISC::GET_GAME_TIMER();
			}

			if (GuardSpotTime == 0 && spottedplayer)
			{
				GuardSpotTime = MISC::GET_GAME_TIMER();

				ScriptedSpeechParams params{"ITS_THEM_EXTREME", 0, 0, MISC::GET_HASH_KEY("SPEECH_PARAMS_BEAT_SHOUTED_CLEAR_NOSUB"), 0, true, 0, 0 };
				AUDIO::PLAY_PED_AMBIENT_SPEECH_NATIVE(guard, (Any*)&params);
				
				TASK::CLEAR_PED_TASKS(guard, true, true);
				TASK::TASK_AIM_AT_ENTITY(guard, PlayerPed, 9000, 0, 0);
			}
		}
	}
	else if (!GuardsAlerted && MISC::GET_GAME_TIMER() - GuardSpotTime > 2000)
	{
		GuardsAlerted = true;
	}
	else if (!GuardsCombat && GuardsAlerted)
	{
		GuardsCombat = true;

		if (!CompanionsInCombat)
		{
			CompanionsBeginCombat();
		}

		CurrentMusicStage = STAGE_ACTION1;

		for (Ped guard : ActiveGuards)
		{
			Blip guardBlip = MAP::GET_BLIP_FROM_ENTITY(guard); 
			MAP::BLIP_REMOVE_MODIFIER(guardBlip, BLIP_MODIFIER_ENEMY_ON_GUARD);
			MAP::BLIP_ADD_MODIFIER(guardBlip, BLIP_MODIFIER_ENEMY_IS_ALERTED);

			TASK::CLEAR_PED_TASKS(guard, true, true);
			TASK::TASK_COMBAT_HATED_TARGETS(guard, 100);
		}
	}
	else
	{
		bool alldead = true;

		for (Ped guard : ActiveGuards)
		{
			if (!PED::IS_PED_DEAD_OR_DYING(guard, true))
			{
				alldead = false;
			}
		}

		if (alldead)
		{
			RunObjective_0 = false;
			ActivateObjective_1 = true;
		}
	}
}

void JailbreakTown::ObjectiveInit_1()
{
	ObjectiveSet(1);
	ActivateObjective_1 = false;
	RunObjective_1 = true;

	CurrentMusicStage = STAGE_AFTERACTION;

	ActivePursuitParams.DispatchResetDelay = -1;
	ActivePursuitParams.ScriptedLawSpawnPoints = CurrentTownInfo.BackupSpawns;
	ActivePursuitParams.MusicStageOnSpotted = STAGE_ACTION1;

	for (const Vector3& spawn : ActivePursuitParams.ScriptedLawSpawnPoints)
	{
		CreateScriptedRespondingLawPed(GenericLawFollower, PedType::SHOTGUN_WEAK, HORSE_INVALID, Common::ApplyRandomSmallOffsetToCoords(spawn, 1.5f), CurrentTownInfo.JailCenter);
		CreateScriptedRespondingLawPed(GenericLawFollower, PedType::REPEATER_EXPERT, HORSE_INVALID, Common::ApplyRandomSmallOffsetToCoords(spawn, 1.5f), CurrentTownInfo.JailCenter);
		CreateScriptedRespondingLawPed(GenericLawFollower, PedType::REPEATER_WEAK, HORSE_INVALID, Common::ApplyRandomSmallOffsetToCoords(spawn, 1.5f), CurrentTownInfo.JailCenter);
	}

	SetCompanionPlayerCollision(false);
}

void JailbreakTown::ObjectiveLogic_1()
{
	WantedLogic(true);

	if (ActivePursuitParams.AllScriptedPedsKilled)
	{
		CurrentMusicStage = STAGE_AFTERACTION;

		ActivePursuitParams.AllScriptedPedsKilled = false;
	}
	
	if (MISC::GET_GAME_TIMER() - BlipSetTimer > 1000)
	{
		for (Ped guard : ActiveGuards)
		{
			if (!ENTITY::_IS_ENTITY_FULLY_LOOTED(guard))
			{
				Blip guardBlip = MAP::GET_BLIP_FROM_ENTITY(guard);
				MAP::_BLIP_SET_STYLE(guardBlip, BLIP_STYLE_LOOT_OBJECTIVE);
				MAP::SET_BLIP_SPRITE(guardBlip, MISC::GET_HASH_KEY("blip_ambient_secret"), true);
				MAP::_SET_BLIP_NAME_FROM_PLAYER_STRING(guardBlip, ObjectivesList[1].c_str());
			}
		}

		BlipSetTimer = MISC::GET_GAME_TIMER();
	}

	if (ENTITY::_IS_ENTITY_FULLY_LOOTED(ActiveJailer))
	{
		RunObjective_1 = false;
		ActivateObjective_2 = true;
	}
}

void JailbreakTown::ObjectiveInit_2()
{
	ObjectiveSet(2);
	ActivateObjective_2 = false;
	RunObjective_2 = true;

	for (Ped guard : ActiveGuards)
	{
		Blip guardBlip = MAP::GET_BLIP_FROM_ENTITY(guard);
		MAP::_BLIP_SET_STYLE(guardBlip, BLIP_STYLE_LOOT);
		MAP::SET_BLIP_SPRITE(guardBlip, MISC::GET_HASH_KEY("blip_ambient_corpse"), true);
		MAP::BLIP_ADD_MODIFIER(guardBlip, BLIP_MODIFIER_LOOTED);
	}

	for (Ped prisoner : ActivePrisoners)
	{
		if (!PED::IS_PED_DEAD_OR_DYING(prisoner, true))
		{
			ScriptedSpeechParams params{"CALL_FOR_SUPPORT", 0, 0, MISC::GET_HASH_KEY("SPEECH_PARAMS_BEAT_SHOUTED_CLEAR_NOSUB"), PlayerPed, true, 0, 0};
			AUDIO::PLAY_PED_AMBIENT_SPEECH_NATIVE(prisoner, (Any*)&params);
			break;
		}
	}

	for (int i = 0; i < CurrentTownInfo.JailCells.size(); ++i)
	{
		Common::SetDoorState(CurrentTownInfo.JailCells[i].DoorHash, DOORSTATE_LOCKED_BREAKABLE);

		CurrentTownInfo.JailCells[i].SpawnedBlip = ObjectiveBlipCoords(CurrentTownInfo.JailCells[i].BlipCoords, 1.0f, false, ObjectivesList[2]);
	}

	if (CurrentTownInfo.TownScriptHash == MISC::GET_HASH_KEY("valentine"))
	{
		CurrentTownInfo.ForceUnlockDoors.emplace_back(535323366); // Outer cell door
		CurrentTownInfo.ForceUnlockDoors.emplace_back(1508776842); // Reinforced side door
	}
}

void JailbreakTown::ObjectiveLogic_2()
{
	WantedLogic(true);
	
	for (int i = 0; i < CurrentTownInfo.JailCells.size(); ++i)
	{
		const CellInfo cell = CurrentTownInfo.JailCells[i];

		bool doorOpen = OBJECT::DOOR_SYSTEM_GET_OPEN_RATIO(cell.DoorHash) > 0.0f ||
						OBJECT::DOOR_SYSTEM_GET_OPEN_RATIO(cell.DoorHash) < 0.0f ||
						OBJECT::DOOR_SYSTEM_GET_DOOR_STATE(cell.DoorHash) == DOORSTATE_UNLOCKED;

		if (!cell.DoorOpen && doorOpen)
		{
			for (int j = 0; j < cell.SpawnedPrisoners.size(); ++j)
			{
				Ped prisoner = cell.SpawnedPrisoners[j];
				
				if (!PED::IS_PED_DEAD_OR_DYING(prisoner, true))
				{
					ENTITY::SET_ENTITY_NO_COLLISION_ENTITY(prisoner, PlayerPed, false);

					ScriptedSpeechParams params{j == 0 ? "GENERIC_THANKS" : "COMBAT_FLEE_CALL_OUT", 0, 0, MISC::GET_HASH_KEY("SPEECH_PARAMS_BEAT_SHOUTED_CLEAR_NOSUB"), PlayerPed, true, 0, 0};
					AUDIO::PLAY_PED_AMBIENT_SPEECH_NATIVE(prisoner, (Any*)&params);

					TASK::CLEAR_PED_TASKS(prisoner, true, true);
					TASK::TASK_SMART_FLEE_COORD(prisoner, CurrentTownInfo.JailCenter, 1000, -1, 0, 5);

					Blip delblip = MAP::GET_BLIP_FROM_ENTITY(prisoner);
					MAP::REMOVE_BLIP(&delblip);
				}
			}

			MAP::REMOVE_BLIP(&CurrentTownInfo.JailCells[i].SpawnedBlip);

			CurrentTownInfo.JailCells[i].DoorOpen = true;
		}
	}

	bool allOpen = true;

	for (const CellInfo& cell : CurrentTownInfo.JailCells)
	{
		if (!cell.DoorOpen)
		{
			allOpen = false;
		}
	}

	if (allOpen)
	{
		RunObjective_2 = false;
		ActivateObjective_3 = true;
	}
}

void JailbreakTown::ObjectiveInit_3()
{
	ObjectiveSet(3);
	ActivateObjective_3 = false;
	RunObjective_3 = true;

	CheckAbandon = false;

	//CompanionsClearTasks();
	//CompanionsRegroup();

	AllowCompanionManualControl = true;

	//EnableLaw();
	//SetDispatchService(true);

	//GlobalVarsAccess::SetRegionLocked(CurrentTownInfo.TownRegion, true);

	//LAW::_SET_LAW_REGION(0, LAW_REGION_VALENTINE, STATE_NEW_HANOVER);

	//BecomeWanted(CRIME_JAIL_BREAK, 5, true);

	ActivePursuitParams.AllScriptedPedsKilled = false;
	ActivePursuitParams.DispatchResetDelay = -1;
	ActivePursuitParams.MusicStageOnSpotted = STAGE_ACTION2;
	ActivePursuitParams.CompanionCombatOnSpotted = true;
	PlayerIncognito = true;
	PlayerPursuit = false;

	for (const Vector3& spawn : ActivePursuitParams.ScriptedLawSpawnPoints)
	{
		CreateScriptedRespondingLawPed(GenericLawLeader, PedType::SIDEARM_REGULAR, HORSE_INVALID, Common::ApplyRandomSmallOffsetToCoords(spawn, 1.5f), CurrentTownInfo.JailCenter);
		CreateScriptedRespondingLawPed(GenericLawFollower, PedType::SHOTGUN_REGULAR, HORSE_INVALID, Common::ApplyRandomSmallOffsetToCoords(spawn, 1.5f), CurrentTownInfo.JailCenter);
		CreateScriptedRespondingLawPed(GenericLawFollower, PedType::REPEATER_WEAK, HORSE_INVALID, Common::ApplyRandomSmallOffsetToCoords(spawn, 1.5f), CurrentTownInfo.JailCenter);
		CreateScriptedRespondingLawPed(GenericLawFollower, PedType::REPEATER_WEAK, HORSE_INVALID, Common::ApplyRandomSmallOffsetToCoords(spawn, 1.5f), CurrentTownInfo.JailCenter);
	}

	CurrentMusicStage = STAGE_ACTION2;
}

void JailbreakTown::ObjectiveLogic_3()
{
	WantedLogic(true);
	
	if (ActivePursuitParams.AllScriptedPedsKilled)
	{
		CurrentMusicStage = STAGE_AFTERACTION;
		UpdateMusic();

		RunObjective_3 = false;
		MissionSuccess();
	}
}

void JailbreakTown::TrackRewards()
{
	RewardBounty_Active = RewardBounty_Potential;

	RewardMembers_Active = RewardMembers_Potential - (PrisonerCasualties * 2);

	RewardCash_Active = RewardCash_Potential;
}

void JailbreakTown::Update()
{
	for (Hash door : CurrentTownInfo.ForceUnlockDoors)
	{
		Common::SetDoorState(door, DOORSTATE_UNLOCKED);
	}

	if (!ActivePrisoners.empty())
	{
		for (Ped prisoner : ActivePrisoners)
		{
			if (PED::GET_PED_CONFIG_FLAG(prisoner, PCF_DisableLadderClimbing, true) && PED::IS_PED_DEAD_OR_DYING(prisoner, true))
			{
				DioTrace("Prisoner down");
				PED::SET_PED_CONFIG_FLAG(prisoner, PCF_DisableLadderClimbing, false);
				PrisonerCasualties++;
			}
		}

		if (PrisonerCasualties >= ActivePrisoners.size() && TimeMissionFail < 0)
		{
			MissionFailStr = "All prisoners were killed.";
			MissionFail();
		}
	}

	if (TimeMissionFail > 0 && MISC::GET_GAME_TIMER() - TimeMissionFail > TimeMissionFailDelay)
	{
		DioTrace("Exit Fail Triggered");

		ExitMissionFlag = M_EXIT_FAILURE;
	}
	else if (TimeMissionSuccess > 0 && MISC::GET_GAME_TIMER() - TimeMissionSuccess > TimeMissionSuccessDelay)
	{
		DioTrace("Exit Win Triggered");

		ExitMissionFlag = M_EXIT_SUCCESS;
	}
	else if (ActivateObjective_0)
	{
		ObjectiveInit_0();
	}
	else if (ActivateObjective_1)
	{
		ObjectiveInit_1();
	}
	else if (ActivateObjective_2)
	{
		ObjectiveInit_2();
	}
	else if (ActivateObjective_3)
	{
		ObjectiveInit_3();
	}
	else if (RunObjective_0)
	{
		ObjectiveLogic_0();
	}
	else if (RunObjective_1)
	{
		ObjectiveLogic_1();
	}
	else if (RunObjective_2)
	{
		ObjectiveLogic_2();
	}
	else if (RunObjective_3)
	{
		ObjectiveLogic_3();
	}
}
