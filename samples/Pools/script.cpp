
#include "precomp.h"

#include "script.h"
#include "scriptmenu.h"
#include "keyboard.h"
#include "FreeCam.h"
#include "MissionController.h"
#include "MissionFactory.h"
#include "InventoryManager.h"

//////////////////////////////////// GLOBAL VARIABLES //////////////////////////////////

const char WIPstr[] = "[COMING SOON]";
const char LockdownStr[] = "[LOCKED]";

const char ManagerPedName[] = "Camp Management";
const char ManagerPedAction[] = "View Camp Stats & Upgrades";
const char ArmoryPedName[] = "Camp Armory";
const char ArmoryPedAction[] = "View & Upgrade Gang Weapons";
const char HorsePedName[] = "Camp Stable";
const char HorsePedAction[] = "View & Upgrade Gang Horses";
const char MissionPedName[] = "Heist Planning";
const char MissionPedAction[] = "View Available Missions";

std::string GenericPedName = "Your Gang Member";

const int ScreenFadeTime = 1000;

bool firstLoad = true;
bool hasCamp = false;
bool campgroundLoaded = false;
bool campSpawned = false;
bool campPopulated = false;
bool AllowCampPopulation = true;

enum MaxMembersForCampUpgrade
{
	UPGR_0_MEMBERS = 6,
	UPGR_1_MEMBERS = 15,
	UPGR_2_MEMBERS = 24,
	UPGR_3_MEMBERS = 99
};

enum MaxFundsForCampUpgrade
{
	UPGR_0_FUNDS = 10000,
	UPGR_1_FUNDS = 50000,
	UPGR_2_FUNDS = 100000,
	UPGR_3_FUNDS = 999999
};

const int MAX_BOUNTY = 500000;

struct UserData
{
	int DAT_GANG;
	int DAT_CAMP;
	int DAT_CAMP_UPGR;
	int DAT_MEMBERCOUNT;
	int DAT_FUNDS;
	int DAT_BOUNTY;
};

UserData DAT = {GANG_NONE, DISTRICT_INVALID, -1, -1, -1, -1};

const UserData DAT_DEFAULT = {GANG_ODRISCOLLS, DISTRICT_GRIZZLIES_WEST, 0, 2, 0, 0};

#ifdef DEBUG_DAT_OVERRIDE
const UserData DAT_OVERRIDE = {GANG_ODRISCOLLS, DISTRICT_GRIZZLIES_WEST, 3, 20, 5000000, 0};
#endif

int G_GANG, G_CAMP, G_CAMP_UPGR, G_MEMBERCOUNT, G_FUNDS, G_BOUNTY;

int G_MISSIONS_THIS_SESSION = 0;
bool G_PENDING_RAID = false;

Ped PlayerPed, PlayerHorse;

#ifdef DEBUG_CAMERA
FreeCam* freecam;
#endif

InventoryManager* CampInventory;
MissionController* CampMissionController;

///////////////////////////////// MENU OPENING/CLOSING /////////////////////////////////

enum MenuReopenIndex
{
	M_MENU_NONE,
	M_MENU_CAMP,
	M_MENU_WEAPON_BUY,
	M_MENU_WEAPON_SELL,
	M_MENU_ARMOR,
	M_MENU_HORSE_BUY,
	M_MENU_HORSE_SELL
};

bool open_init, close_init, running_init = false;
bool open_camp = false;
bool open_armory = false;
bool open_horse = false;
bool open_mission = false;
bool close_menu_gen = false;

bool relocation = false;

int ReopenMenuNextTick = M_MENU_NONE;

bool ActivateLawLoopNextTick = false;

//////////////////////////////////// UTILITY FUNCTIONS /////////////////////////////////

void display(const std::string& str)
{
	UILOG::_UILOG_SET_CACHED_OBJECTIVE(str.c_str());
	UILOG::_UILOG_PRINT_CACHED_OBJECTIVE();
	UILOG::_UILOG_CLEAR_HAS_DISPLAYED_CACHED_OBJECTIVE();
	UILOG::_UILOG_CLEAR_CACHED_OBJECTIVE();
}

bool VectorsEqual(const Vector3& v1, const Vector3& v2)
{
	return (
		v1.x == v2.x &&
		v1.y == v2.y &&
		v1.z == v2.z
	);
}

////////////////////////////////////// GAME FUNCTIONS //////////////////////////////////

void SanityCheck()
{
	if (!hasCamp)
	{
		DioTrace("Not performing sanity check");
		return;
	}

	bool bogusdata = (
		G_GANG < 0 ||
		G_CAMP < 0 ||
		G_CAMP_UPGR < 0
	);

	if (bogusdata)
	{
		DioTrace(DIOTAG1("ERROR") "BOGUS DATA DETECTED IN PRIMARY GLOBAL, REVERTING ALL GLOBALS TO DEFAULT");
		G_GANG = DAT_DEFAULT.DAT_GANG;
		G_CAMP = DAT_DEFAULT.DAT_CAMP;
		G_CAMP_UPGR = DAT_DEFAULT.DAT_CAMP_UPGR;
		G_MEMBERCOUNT = DAT_DEFAULT.DAT_MEMBERCOUNT;
		G_FUNDS = DAT_DEFAULT.DAT_FUNDS;
		G_BOUNTY = DAT_DEFAULT.DAT_BOUNTY;
	}

	if (G_MEMBERCOUNT < 2)
	{
		DioTrace(DIOTAG1("WARNING") "Member Count dropped below 2, reverting");
		G_MEMBERCOUNT = 2;
	}
	else
	{
		int maxvalue = 0;

		switch (G_CAMP_UPGR)
		{
			case 0:	maxvalue = UPGR_0_MEMBERS; break;
			case 1:	maxvalue = UPGR_1_MEMBERS; break;
			case 2:	maxvalue = UPGR_2_MEMBERS; break;
			case 3:	maxvalue = UPGR_3_MEMBERS; break;
		}

		if (G_MEMBERCOUNT > maxvalue)
		{
			DioTrace(DIOTAG1("WARNING") "Member Count went above maximum of %d, reverting", maxvalue);
			G_MEMBERCOUNT = maxvalue;
		}
	}

	if (G_FUNDS < 0)
	{
		DioTrace(DIOTAG1("WARNING") "Funds dropped below 0, reverting");
		G_FUNDS = 0;
	}
	else
	{
		int maxvalue = 0;

		switch (G_CAMP_UPGR)
		{
			case 0:	maxvalue = UPGR_0_FUNDS; break;
			case 1:	maxvalue = UPGR_1_FUNDS; break;
			case 2:	maxvalue = UPGR_2_FUNDS; break;
			case 3:	maxvalue = UPGR_3_FUNDS; break;
		}

		if (G_FUNDS > maxvalue)
		{
			DioTrace(DIOTAG1("WARNING") "Funds went above maximum of %d, reverting", maxvalue);
			G_FUNDS = maxvalue;
		}
	}

	if (G_BOUNTY < 0)
	{
		DioTrace(DIOTAG1("WARNING") "Bounty dropped below 0, reverting");
		G_BOUNTY = 0;
	}
	else if (G_BOUNTY > MAX_BOUNTY)
	{
		DioTrace(DIOTAG1("WARNING") "Bounty went above maximum of %d, reverting", MAX_BOUNTY);
		G_BOUNTY = MAX_BOUNTY;
	}
}

void LoadPed(const std::string& pedname)
{
	Hash pedhash = MISC::GET_HASH_KEY(pedname.c_str());

	if (!STREAMING::IS_MODEL_VALID(pedhash) || !STREAMING::IS_MODEL_IN_CDIMAGE(pedhash))
	{
		DioTrace(DIOTAG1("ERROR") "INVALID PED %s", pedname.c_str());
		return;
	}

	if (!STREAMING::HAS_MODEL_LOADED(pedhash))
	{
		DIOTRACE_EXTRA("Loading ped %s", pedname.c_str());
		STREAMING::REQUEST_MODEL(pedhash, true);

		while (!STREAMING::HAS_MODEL_LOADED(pedhash))
		{
			WAIT(1);
		}
	}
	else
	{
		DIOTRACE_EXTRA("Loaded ped %s", pedname.c_str());
	}
}

void LoadScenario(const std::string& name)
{
	if (name == "")
	{
		DioTrace(DIOTAG1("ERROR") "BOGUS DATA PASSED TO LOADSCENARIO");
		return;
	}
	
	Hash scenario = MISC::GET_HASH_KEY(name.c_str());

	int timeout = 0;

	if (!STREAMING::_HAS_SCENARIO_TYPE_LOADED(scenario, false))
	{
		DIOTRACE_EXTRA("Loading scenario %s", name.c_str());
		STREAMING::_REQUEST_SCENARIO_TYPE(scenario, 15, 0, 0);

		while (!STREAMING::_HAS_SCENARIO_TYPE_LOADED(scenario, false))
		{
			timeout++;

			if (timeout > 500)
			{
				DioTrace(DIOTAG1("ERROR") "TIMED OUT LOADING SCENARIO %s", name.c_str());
			}

			WAIT(1);
		}
	}
	else
	{
		DIOTRACE_EXTRA("Loaded scenario %s", name.c_str());
	}
}

void PlayPedChatter(Ped ped, const std::string& comment, const std::string& type = "SPEECH_PARAMS_BEAT_SHOUTED_CLEAR_NOSUB")
{
	ScriptedSpeechParams params{comment.c_str(), 0, 0, MISC::GET_HASH_KEY(type.c_str()), 0, true, 0, 0};

	AUDIO::PLAY_PED_AMBIENT_SPEECH_NATIVE(ped, (Any*)&params);
}

void KillCustomFailEffects()
{
	if (GRAPHICS::ANIMPOSTFX_IS_RUNNING("MissionFail01"))
	{
		MISC::SET_TIME_SCALE(1.0f);
		AUDIO::STOP_AUDIO_SCENE("MISSION_FAILED_SCENE");
		AUDIO::STOP_AUDIO_SCENE("DYING_SCENE");

		CAM::DO_SCREEN_FADE_OUT(1);
		WAIT(2);

		GRAPHICS::ANIMPOSTFX_STOP("MissionFail01");

		CAM::DO_SCREEN_FADE_IN(ScreenFadeTime);
	}
}

////////////////////////////////////////////////////////////////////////////////////////

namespace GEN
{
	bool InEpilogue()
	{
		return ENTITY::GET_ENTITY_MODEL(PlayerPed) != MISC::GET_HASH_KEY("PLAYER_ZERO");
	}

	void ClearWantedStatus()
	{
		LAW::_SET_ALLOW_DISABLED_LAW_RESPONSES(false);
		LAW::_SET_BOUNTY_HUNTER_PURSUIT_CLEARED();
		LAW::_CLEAR_WANTED_INTENSITY_FOR_PLAYER(PLAYER::PLAYER_ID());
		LAW::_0xBCC6DC59E32A2BDC(PLAYER::PLAYER_ID());
		LAW::_0x292AD61A33A7A485();
		LAW::_ENABLE_DISPATCH_LAW(true);
		LAW::_ENABLE_DISPATCH_LAW_2(true);
		LAW::_SET_LAW_DISABLED(false);
		LAW::_0xDE5FAA741A781F73(PLAYER::PLAYER_ID(), 0);
		LAW::_SET_DISPATCH_MULTIPLIER_OVERRIDE(-1.0f);
	}

	void DingPlayerHonor()
	{
		DioTrace(DIOTAG1("TODO"));
		return;
		
		DioTrace("Dinging player honor");
	}

	int ScriptCam = 0;

	void SetScriptCamera(Vector3 pos, Vector3 rot, float fov = 70.0f)
	{
#ifdef DEBUG_CAMERA
		DioTrace(DIOTAG1("WARNING") "Cannot set script cam with DEBUG_CAMERA enabled");
		return;
#endif
		DioTrace(DIOTAG "{%.2f, %.2f, %.2f} {%.2f, %.2f, %.2f}", pos.x, pos.y, pos.z, rot.x, rot.y, rot.z);
		
		if (!CAM::DOES_CAM_EXIST(ScriptCam))
		{
			ScriptCam = CAM::CREATE_CAMERA(26379945, true);
			CAM::SET_CAM_ACTIVE(ScriptCam, false);
		}

		CAM::SET_CAM_COORD(ScriptCam, pos.x, pos.y, pos.z);
		CAM::SET_CAM_ROT(ScriptCam, rot.x, rot.y, rot.z, 0);
		CAM::SET_CAM_FOV(ScriptCam, fov);

		if (!CAM::IS_CAM_ACTIVE(ScriptCam))
		{
			CAM::SET_CAM_ACTIVE(ScriptCam, true);
			CAM::RENDER_SCRIPT_CAMS(true, true, 800, true, false, 1);
		}
	}

	void ClearScriptCamera(bool force = false)
	{
		if (ReopenMenuNextTick <= M_MENU_NONE || force)
		{
			DIOTRACE_EXTRA(DIOTAG);
			CAM::RENDER_SCRIPT_CAMS(false, false, 0, true, false, 1);
			CAM::SET_CAM_ACTIVE(ScriptCam, false);
			CAM::DESTROY_CAM(ScriptCam, false);
		}
	}

	void FreezePlayer(bool on, bool allowcamera = false)
	{
		if (on)
		{
			TASK::CLEAR_PED_TASKS_IMMEDIATELY(PED::GET_MOUNT(PlayerPed), true, true);
			TASK::TASK_DISMOUNT_ANIMAL(PlayerPed, 0, 0, 0, 0, 0);
		}
		else
		{
			ClearScriptCamera();
			MISC::SET_TIME_SCALE(1.0);
			HUD::DISPLAY_HUD(true);
		}

		ENTITY::SET_ENTITY_INVINCIBLE(PlayerPed, on);
		//PLAYER::SET_EVERYONE_IGNORE_PLAYER(PLAYER::PLAYER_ID(), on);
		PLAYER::SET_PLAYER_CONTROL(PLAYER::PLAYER_ID(), !on, allowcamera ? 256 : 0, !on);
	}

	void fadeout()
	{
#ifdef DEBUG_FADE
		DioTrace("Fade out");
#else
		CAM::DO_SCREEN_FADE_OUT(ScreenFadeTime);
#endif
		WAIT(ScreenFadeTime);
	}

	void fadein()
	{
#ifdef DEBUG_FADE
		DioTrace("Fade in");
#else
		CAM::DO_SCREEN_FADE_IN(ScreenFadeTime);
#endif
		WAIT(ScreenFadeTime);
	}
}

namespace M_GEN
{
	std::string CalcMoneyStr(int money, bool showcents)
	{
		if (money == 0)
		{
			return showcents ? "$0.00" : "$0";
		}

		int dollars = money / 100;
		int cents = money % 100;

		if (!showcents)
		{
			return Common::Format("%s%d", "$", dollars);
		}

		std::string centstr = (cents < 10) ? Common::Format("0%d", cents) : Common::Format("%d", cents);

		return Common::Format("$%d.%s", dollars, centstr);
	}

	void PlayPurchaseNoise(bool canafford = true)
	{
		while (!AUDIO::PREPARE_SOUNDSET("Ledger_Sounds", false))
		{
			WAIT(1);
		}

		if (!canafford)
		{
			AUDIO::_STOP_SOUND_WITH_NAME("UNAFFORDABLE", "Ledger_Sounds");
			AUDIO::PLAY_SOUND_FRONTEND("UNAFFORDABLE", "Ledger_Sounds", true, 0);
		}
		else
		{
			AUDIO::_STOP_SOUND_WITH_NAME("PURCHASE", "Ledger_Sounds");
			AUDIO::PLAY_SOUND_FRONTEND("PURCHASE", "Ledger_Sounds", true, 0);
		}
	}

	class MenuItemBack : public MenuItemDefault
	{
		MenuController* menucontroller;

		virtual void OnSelect()
		{
			bool backstatus = menucontroller->AllowBack;
			
			menucontroller->AllowBack = true;
			menucontroller->PopMenu();
			menucontroller->AllowBack = backstatus;

			if (!menucontroller->HasActiveMenu())
			{
				GEN::FreezePlayer(false);
			}
		}
	public: MenuItemBack(MenuController* controller) : MenuItemDefault("BACK"), menucontroller(controller) {}
	};

	class MenuItemBlank : public MenuItemDefault
	{
	public: MenuItemBlank() : MenuItemDefault(" ") {}
	};

	void ClearAllOpenMenus(MenuController* controller)
	{
		bool backstatus = controller->AllowBack;
		
		controller->AllowBack = true;

		while (controller->HasActiveMenu())
		{
			controller->PopMenu();
		}

		controller->AllowBack = backstatus;

		GEN::FreezePlayer(false);
	}
}

namespace IO
{
	std::fstream datFile;

	const int ProperHeaderFlag = 69189969;

	void ReadDAT()
	{
		DioScope(DIOTAG);

		int header = ProperHeaderFlag;

		datFile.close();
		datFile.open("GangMod.dat", std::ios::in | std::ios::binary);

		datFile.read((char*)&header, sizeof(header));
		datFile.read((char*)&DAT.DAT_GANG, sizeof(DAT.DAT_GANG));
		datFile.read((char*)&DAT.DAT_CAMP, sizeof(DAT.DAT_CAMP));
		datFile.read((char*)&DAT.DAT_CAMP_UPGR, sizeof(DAT.DAT_CAMP_UPGR));
		datFile.read((char*)&DAT.DAT_MEMBERCOUNT, sizeof(DAT.DAT_MEMBERCOUNT));
		datFile.read((char*)&DAT.DAT_FUNDS, sizeof(DAT.DAT_FUNDS));
		datFile.read((char*)&DAT.DAT_BOUNTY, sizeof(DAT.DAT_BOUNTY));

		datFile.read((char*)&G_PENDING_RAID, sizeof(G_PENDING_RAID));

		if (CampInventory != nullptr)
		{
			CampInventory->SetDefaultInventory();
			
			while (!datFile.eof())
			{
				Hash item = 0;

				datFile.read((char*)&item, sizeof(item));

				if (item > 0)
				{
					CampInventory->AddItem(item);
				}
			}
		}
		else
		{
			DioTrace(DIOTAG1("WARNING") "CampInventory not initialized yet");
		}

#ifdef DEBUG_DAT_OVERRIDE
		DAT = DAT_OVERRIDE;
#endif

		if (DAT.DAT_CAMP > -1)
		{
			hasCamp = true;
		}

		G_GANG = DAT.DAT_GANG;
		G_CAMP = DAT.DAT_CAMP;
		G_CAMP_UPGR = DAT.DAT_CAMP_UPGR;
		G_MEMBERCOUNT = DAT.DAT_MEMBERCOUNT;
		G_FUNDS = DAT.DAT_FUNDS;
		G_BOUNTY = DAT.DAT_BOUNTY;

		SanityCheck();

#ifdef DEBUG_DAT_OVERRIDE
		const char override[] = "(OVERRIDDEN)";
#else
		const char override[] = "";
#endif
		DioTrace("FINAL READ RESULTS %s", override);
		DioTrace("G_GANG: %d", G_GANG);
		DioTrace("G_CAMP: %d", G_CAMP);
		DioTrace("G_CAMP_UPGR: %d", G_CAMP_UPGR);
		DioTrace("G_MEMBERCOUNT: %d", G_MEMBERCOUNT);
		DioTrace("G_FUNDS: %d", G_FUNDS);
		DioTrace("G_BOUNTY: %d", G_BOUNTY);
		DioTrace("G_PENDING_RAID: %d", (int)G_PENDING_RAID);
	}

	void WriteDAT()
	{
		DioScope(DIOTAG);

		SanityCheck();

		if (hasCamp)
		{
			DAT.DAT_GANG = G_GANG;
			DAT.DAT_CAMP = G_CAMP;
			DAT.DAT_CAMP_UPGR = G_CAMP_UPGR;
			DAT.DAT_MEMBERCOUNT = G_MEMBERCOUNT;
			DAT.DAT_FUNDS = G_FUNDS;
			DAT.DAT_BOUNTY = G_BOUNTY;
		}

		datFile.close();
		datFile.open("GangMod.dat", std::ios::out | std::ios::binary);

		datFile.write((char*)&ProperHeaderFlag, sizeof(ProperHeaderFlag));
		datFile.write((char*)&DAT.DAT_GANG, sizeof(DAT.DAT_GANG));
		datFile.write((char*)&DAT.DAT_CAMP, sizeof(DAT.DAT_CAMP));
		datFile.write((char*)&DAT.DAT_CAMP_UPGR, sizeof(DAT.DAT_CAMP_UPGR));
		datFile.write((char*)&DAT.DAT_MEMBERCOUNT, sizeof(DAT.DAT_MEMBERCOUNT));
		datFile.write((char*)&DAT.DAT_FUNDS, sizeof(DAT.DAT_FUNDS));
		datFile.write((char*)&DAT.DAT_BOUNTY, sizeof(DAT.DAT_BOUNTY));

		datFile.write((char*)&G_PENDING_RAID, sizeof(G_PENDING_RAID));
		
		if (CampInventory != nullptr)
		{
			for (Hash item : CampInventory->GetAvailableWeapons(false))
			{
				datFile.write((char*)&item, sizeof(item));
			}

			for (Hash item : CampInventory->GetAvailableWeapons(true))
			{
				datFile.write((char*)&item, sizeof(item));
			}

			for (Hash item : CampInventory->GetAvailableArmor())
			{
				datFile.write((char*)&item, sizeof(item));
			}

			for (Hash item : CampInventory->GetAllHorses())
			{
				datFile.write((char*)&item, sizeof(item));
			}
		}
		else
		{
			DioTrace(DIOTAG1("WARNING") "CampInventory not initialized yet");
		}

		datFile.flush();
		
		DioTrace("FINAL WRITE RESULTS");
		DioTrace("DAT_GANG: %d", DAT.DAT_GANG);
		DioTrace("DAT_CAMP: %d", DAT.DAT_CAMP);
		DioTrace("DAT_CAMP_UPGR: %d", DAT.DAT_CAMP_UPGR);
		DioTrace("DAT_MEMBERCOUNT: %d", DAT.DAT_MEMBERCOUNT);
		DioTrace("DAT_FUNDS: %d", DAT.DAT_FUNDS);
		DioTrace("DAT_BOUNTY: %d", DAT.DAT_BOUNTY);
		DioTrace("G_PENDING_RAID: %d", (int)G_PENDING_RAID);
	}

	bool startup()
	{
		DioScope(DIOTAG);

		if (datFile.is_open())
		{
			datFile.close();
		}

		std::ifstream access_test("GangMod.dat");

		if (!access_test.is_open())
		{
			DioTrace(DIOTAG1("ERROR") "COULD NOT OPEN DAT");
			return false;
		}
		access_test.close();

		int ActualHeaderFlag = 0;

		datFile.open("GangMod.dat", std::ios::in | std::ios::binary);

		if (datFile.good())
		{
			datFile.read((char*)&ActualHeaderFlag, sizeof(ProperHeaderFlag));
			datFile.close();

			if (ActualHeaderFlag != ProperHeaderFlag)
			{
				DioTrace(DIOTAG1("WARNING") "Invalid header, overwriting file");
				WriteDAT();
			}
		}
		else
		{
			DioTrace(DIOTAG1("ERROR") "DAT NOT GOOD");
			datFile.close();
			return false;
		}
		

		DioTrace("DAT successfully validated");
		return true;
	}
}

namespace INIT
{
	bool performedInit = false;
	
	Blip blip;
	Ped ped = 0;
	Prompt initPrompt;

	void startup()
	{
		DioTrace(DIOTAG "Creating init prompt");
		initPrompt = HUD::_UIPROMPT_REGISTER_BEGIN();
		HUD::_UIPROMPT_SET_CONTROL_ACTION(initPrompt, MISC::GET_HASH_KEY("INPUT_INTERACT_LOCKON_POS"));
		HUD::_UIPROMPT_SET_TEXT(initPrompt, MISC::VAR_STRING(10, "LITERAL_STRING", "Purchase Initial Camp Supplies - $50"));
		HUD::_UIPROMPT_SET_STANDARD_MODE(initPrompt, 1);
		HUD::_UIPROMPT_REGISTER_END(initPrompt);
		HUD::_UIPROMPT_SET_ENABLED(initPrompt, false);
		HUD::_UIPROMPT_SET_VISIBLE(initPrompt, false);
	}

	void init()
	{
		if (performedInit || MISC::GET_MISSION_FLAG() || hasCamp)
		{
			return;
		}

		DioTrace(DIOTAG "Spawning init ped");
		
		const Hash model = MISC::GET_HASH_KEY("CS_MysteriousStranger");
			
		if (!STREAMING::HAS_MODEL_LOADED(model))
		{
			STREAMING::REQUEST_MODEL(model, true);

			while (!STREAMING::HAS_MODEL_LOADED(model))
			{
				WAIT(1);
			}
		}

		const Vector3 spawnloc = {1794.07f, -814.076f, 41.6f};

		Common::ClearArea(spawnloc, 30);

		ped = PED::CREATE_PED(model, spawnloc.x, spawnloc.y, spawnloc.z, 311.0f, false, true, false, false);
		PED::_SET_PED_OUTFIT_PRESET(ped, 0, false);
		PED::_UPDATE_PED_VARIATION(ped, true, true, true, true, true);
		WEAPON::REMOVE_ALL_PED_WEAPONS(ped, true, true);

		ENTITY::SET_ENTITY_AS_MISSION_ENTITY(ped, true, true);
		ENTITY::FREEZE_ENTITY_POSITION(ped, true);

		const Hash scenario = MISC::GET_HASH_KEY("WORLD_HUMAN_SMOKE_CIGAR");

		if (!STREAMING::_HAS_SCENARIO_TYPE_LOADED(scenario, false))
		{
			STREAMING::_REQUEST_SCENARIO_TYPE(scenario, 15, 0, 0);

			while (!STREAMING::_HAS_SCENARIO_TYPE_LOADED(scenario, false))
			{
				WAIT(0);
			}
		}

		TASK::TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped, true);
		PED::SET_PED_CONFIG_FLAG(ped, PCF_ForceInteractionLockonOnTargetPed, true);

		WAIT(500);

		TASK::CLEAR_PED_TASKS_IMMEDIATELY(ped, true, true);
		TASK::TASK_START_SCENARIO_IN_PLACE_HASH(ped, scenario, -1, true, 0, -1.0f, true);
		PED::SET_PED_KEEP_TASK(ped, true);

		PED::SET_PED_RELATIONSHIP_GROUP_HASH(ped, MISC::GET_HASH_KEY("REL_GANG_DUTCHS"));
		ENTITY::SET_ENTITY_INVINCIBLE(ped, true);
		PED::SET_PED_CAN_RAGDOLL(ped, false);
		PED::SET_PED_LASSO_HOGTIE_FLAG(ped, 0, false);

		blip = MAP::BLIP_ADD_FOR_RADIUS(BLIP_STYLE_LOOT_OBJECTIVE, ENTITY::GET_ENTITY_COORDS(ped, true, true), 35.0f);
		MAP::SET_BLIP_SPRITE(blip, MISC::GET_HASH_KEY("blip_ambient_bounty_hunter"), true);
		MAP::_SET_BLIP_NAME_FROM_PLAYER_STRING(blip, "Create Your Own Gang");
		MAP::BLIP_ADD_MODIFIER(blip, BLIP_MODIFIER_HIDE_HEIGHT_MARKER);
		MAP::BLIP_ADD_MODIFIER(blip, BLIP_MODIFIER_NEUTRAL_AREA_ON_SNOW);

		PED::_SET_PED_PROMPT_NAME(ped, "Create Your Own Gang");

		if (ENTITY::DOES_ENTITY_EXIST(ped))
		{
			performedInit = true;
		}
	}

	void update()
	{
		if ((MISC::GET_MISSION_FLAG() || hasCamp) && !close_init)
		{
			PED::DELETE_PED(&ped);
			MAP::REMOVE_BLIP(&blip);
			performedInit = false;
			HUD::_UIPROMPT_SET_ENABLED(initPrompt, false);
			HUD::_UIPROMPT_SET_VISIBLE(initPrompt, false);
			return;
		}
		
		if (ENTITY::DOES_ENTITY_EXIST(ped) && !open_init && !running_init &&
			Common::GetDistBetweenCoords(ENTITY::GET_ENTITY_COORDS(PlayerPed, true, true), ENTITY::GET_ENTITY_COORDS(ped, true, true)) < 5.0f)
		{
			HUD::_UIPROMPT_SET_VISIBLE(initPrompt, true);
			HUD::_UIPROMPT_SET_ENABLED(initPrompt, MONEY::_MONEY_GET_CASH_BALANCE() >= 5000);

			if (HUD::_UIPROMPT_HAS_STANDARD_MODE_COMPLETED(initPrompt, 0) || IsKeyJustUp(VK_F1))
			{
				DioTrace(DIOTAG "running_init");

				running_init = true;

				MONEY::_MONEY_DECREMENT_CASH_BALANCE(5000);

				HUD::_UIPROMPT_SET_ENABLED(initPrompt, false);
				HUD::_UIPROMPT_SET_VISIBLE(initPrompt, false);

				open_init = true;

				MAP::REMOVE_BLIP(&blip);
			}
		}
		else
		{
			HUD::_UIPROMPT_SET_ENABLED(initPrompt, false);
			HUD::_UIPROMPT_SET_VISIBLE(initPrompt, false);
		}
		
		if (close_init)
		{
			DioTrace(DIOTAG "close_init");
			ENTITY::SET_ENTITY_AS_NO_LONGER_NEEDED(&ped);

			HUD::_UIPROMPT_SET_ENABLED(initPrompt, false);
			HUD::_UIPROMPT_SET_VISIBLE(initPrompt, false);
		}
	}
}

namespace M_INIT
{
	class MenuItemLocation : public MenuItemDefault
	{
		int m_gang; 
		int m_value;
		bool m_relocation;

		virtual void OnSelect()
		{
			if (m_value == DISTRICT_RIO_BRAVO && !GEN::InEpilogue())
			{
				display("Cannot enter New Austin as Arthur!");
				return;
			}
			
			relocation = m_relocation;
			
			close_init = true;
			hasCamp = true;
			G_CAMP = m_value;
			G_CAMP_UPGR = m_relocation ? G_CAMP_UPGR : 0;
			G_GANG = m_gang;
		}
		public: MenuItemLocation(bool relocation, int ChosenGang, std::string caption, int value) : MenuItemDefault(caption), m_relocation(relocation), m_gang(ChosenGang), m_value(value) {}
	};

	class MenuItemLocationLocked : public MenuItemDefault
	{
		virtual void OnSelect()
		{
			display("This camp is ~COLOR_YELLOW~not implemented ~COLOR_WHITE~yet.");
		}
		public: MenuItemLocationLocked(std::string caption) : MenuItemDefault(caption) {}
	};
	
	LocSelectMenu* CreateLocationMenu(MenuController* controller, int ChosenGang, bool relocation = false)
	{
		LocSelectMenu* menu = new LocSelectMenu(new MenuItemTitle("CHOOSE YOUR CAMP"));
		controller->RegisterMenu(menu);

		// Remember to update LocSelectMenu::OnDraw() as well
		menu->AddItem(new MenuItemLocation(relocation, ChosenGang, "West Grizzlies, Ambarino", DISTRICT_GRIZZLIES_WEST));
		//menu->AddItem(new MenuItemLocationLocked(Common::Format("%s %s", WIPstr, "Heartlands, New Hanover")));
		menu->AddItem(new MenuItemLocation(relocation, ChosenGang, "Cumberland Forest, New Hanover", DISTRICT_CUMBERLAND_FOREST));
		//menu->AddItem(new MenuItemLocationLocked(Common::Format("%s %s", WIPstr, "Scarlett Meadows, Lemoyne")));
		menu->AddItem(new MenuItemLocation(relocation, ChosenGang, "Bluewater Marsh, Lemoyne", DISTRICT_BLUEGILL_MARSH));
		//menu->AddItem(new MenuItemLocationLocked(Common::Format("%s %s", WIPstr, "Roanoke Ridge, New Hanover")));
		menu->AddItem(new MenuItemLocation(relocation, ChosenGang, "[JOHN ONLY] Rio Bravo, New Austin", DISTRICT_RIO_BRAVO));

		menu->AddItem(new M_GEN::MenuItemBack(controller));

		return menu;
	}
	
	class MenuItemGangLocked : public MenuItemDefault
	{
		virtual void OnSelect()
		{
			display("This gang is ~COLOR_YELLOW~not implemented ~COLOR_WHITE~yet.");
		}
		public: MenuItemGangLocked(std::string caption) : MenuItemDefault(caption) {}
	};
	
	GangSelectMenu* CreateInitMenu(MenuController* controller)
	{
		auto menu = new GangSelectMenu(new MenuItemTitle("CHOOSE YOUR GANG"));
		controller->RegisterMenu(menu);

		// This list must be in sync with the GangIDs enum
		menu->AddItem(new MenuItemMenu("Outlaws", CreateLocationMenu(controller, GANG_ODRISCOLLS)));
		menu->AddItem(new MenuItemMenu("Confederates", CreateLocationMenu(controller, GANG_EXCONFEDS)));
		menu->AddItem(new MenuItemMenu("Braves", CreateLocationMenu(controller, GANG_WAPITI)));
		menu->AddItem(new MenuItemMenu("Savages", CreateLocationMenu(controller, GANG_SKINNERS)));
		menu->AddItem(new MenuItemMenu("Banditos", CreateLocationMenu(controller, GANG_MEXICANS)));
		menu->AddItem(new MenuItemMenu("Van Der Linde Gang", CreateLocationMenu(controller, GANG_DUTCH)));
		menu->AddItem(new MenuItemMenu("Custom Gang", CreateLocationMenu(controller, GANG_CUSTOM)));

		return menu;
	}
}

namespace GANG
{
	struct PedInfo
	{
		std::string model;
		int maxskins;
		bool DontCareAboutSkin;
		int SpecificSkin;
		std::vector<int> ProhibitedSkins;
		std::vector<int> AllowedSkins;
	};

	struct PedCollection
	{
		PedInfo ManagerPed;
		PedInfo ArmoryPed;
		PedInfo HorsePed;
		PedInfo MissionPed;
		std::vector<PedInfo> GenericPeds;
	};

	enum PedOverrides
	{
		OVERRIDE_SNOW,
		OVERRIDE_ROBBERY,
		OVERRIDE__MAX
	};

	struct GangStruct
	{
		std::vector<std::string> Voices;
		PedCollection Default;
		bool HasOverrides[OVERRIDE__MAX];
		PedCollection Override_Snow;
		PedCollection Override_Robbery;
	};

	GangStruct DefaultGangData[GANG__MAXCOUNT];

	void startup()
	{
		PedCollection collection;
		PedInfo ManagerPed, ArmoryPed, HorsePed, MissionPed, GenericPed1, GenericPed2;
		int gng;

		///////////////////////////////////////////////////////////// O'DRISCOLLS //////////////////////////////////////////////////////////// 

		gng = GANG_ODRISCOLLS;

		DefaultGangData[gng].Voices = {
			"0356_G_M_M_UNIDUSTER_01_WHITE_02",
			"1018_G_M_M_UNIDUSTER_01_IRISH_01",
			"0357_G_M_M_UNIDUSTER_02_WHITE_01",
			"0359_G_M_M_UNIDUSTER_02_IRISH_01",
			"0360_G_M_M_UNIDUSTER_03_IRISH_01",
			"0361_G_M_M_UNIDUSTER_03_WHITE_01",
			"0362_G_M_M_UNIDUSTER_03_WHITE_02"
		};

		ManagerPed = {"G_M_M_UniDuster_04", 9, false, 3};
		ArmoryPed =  {"G_M_M_UniDuster_04", 9, false, 8};
		HorsePed =   {"G_M_M_UniDuster_04", 9, false, 16};
		MissionPed = {"G_M_M_UniDuster_03", 37, false, 17};

		collection = {ManagerPed, ArmoryPed, HorsePed, MissionPed, {{"G_M_M_UniDuster_01", 184, true}}};

		DefaultGangData[gng].Default = collection;

		DefaultGangData[gng].HasOverrides[OVERRIDE_ROBBERY] = true;
		DefaultGangData[gng].Override_Robbery = collection;
		DefaultGangData[gng].Override_Robbery.GenericPeds = {{"G_M_M_UniDuster_03", 37, true}};

		DefaultGangData[gng].HasOverrides[OVERRIDE_SNOW] = true;
		DefaultGangData[gng].Override_Snow = collection;
		DefaultGangData[gng].Override_Snow.HorsePed = {"CS_kieran", 11, false, 0};
		DefaultGangData[gng].Override_Snow.MissionPed = {"G_M_M_UniDuster_03", 37, false, 26};
		DefaultGangData[gng].Override_Snow.GenericPeds = {{"G_M_M_UniDuster_02", 56, true}};

		////////////////////////////////////////////////////////////// DEL LOBOS ///////////////////////////////////////////////////////////// 

		gng = GANG_MEXICANS;

		DefaultGangData[gng].Voices = {
			"1019_G_M_M_UNIBANDITOS_01_HISPANIC_01",
			"1020_G_M_M_UNIBANDITOS_01_HISPANIC_02",
			"1021_G_M_M_UNIBANDITOS_01_HISPANIC_03",
			"1022_G_M_M_UNIBANDITOS_01_HISPANIC_04"
		};

		ManagerPed = {"U_M_M_BHT_BANDITOMINE", 1, true, 0};
		ArmoryPed =  {"G_M_M_UniBanditos_01", 166, false, 139 };
		HorsePed =   {"G_M_M_UniBanditos_01", 166, false, 18 };
		MissionPed = {"G_M_M_UniBanditos_01", 166, false, 106 };

		GenericPed1 = {"G_M_M_UniBanditos_01", 166, false, -1, {}, {
			0, 1, 2, 4, 7, 9, 10, 13, 15, 26, 27, 34, 36, 39, 43, 44, 49, 51, 52, 55, 57, 58, 62, 65, 69, 70, 76, 77, 78, 81, 84, 85, 87, 88, 89, 90, 
			93, 96, 98, 100, 101, 103, 107, 109, 110, 112, 114, 117, 118, 119, 121, 122, 124, 125, 131, 132, 144, 145, 147, 151, 154, 158, 159, 161, 163, 166
		}};

		collection = {ManagerPed, ArmoryPed, HorsePed, MissionPed, {GenericPed1}};

		DefaultGangData[gng].Default = collection;

		DefaultGangData[gng].HasOverrides[OVERRIDE_ROBBERY] = false;

		DefaultGangData[gng].HasOverrides[OVERRIDE_SNOW] = true;
		DefaultGangData[gng].Override_Snow = collection;
		DefaultGangData[gng].Override_Snow.ArmoryPed = {"CS_famousgunslinger_02", 1, false, 0};

		GenericPed1 = {"G_M_M_UniBanditos_01", 166, false, -1, {}, {
			26, 32, 55, 61, 69, 85, 117, 123, 125, 
			110, 134, 135, 136, 137, 138
		}};
		DefaultGangData[gng].Override_Snow.GenericPeds = {GenericPed1};

		//////////////////////////////////////////////////////////// LEMOYNE RAIDERS ///////////////////////////////////////////////////////// 

		gng = GANG_EXCONFEDS;

		DefaultGangData[gng].Voices = {};

		ManagerPed = {"U_M_M_UniExConfedsBounty_01", 1, true, 0};
		ArmoryPed =  {"CS_EXCONFEDSLEADER_01", 1, true, 0};
		HorsePed =   {"CS_EXCONFEDINFORMANT", 1, true, 0};
		MissionPed = {"G_M_Y_UNIEXCONFEDS_02", 32, false, 13};

		collection = {ManagerPed, ArmoryPed, HorsePed, MissionPed, {{"G_M_Y_UNIEXCONFEDS_01", 119, true}, {"G_M_O_UNIEXCONFEDS_01", 92, true}}};

		DefaultGangData[gng].Default = collection;

		DefaultGangData[gng].HasOverrides[OVERRIDE_ROBBERY] = true;
		DefaultGangData[gng].Override_Robbery = collection;
		DefaultGangData[gng].Override_Robbery.GenericPeds = {{"G_M_Y_UNIEXCONFEDS_02", 32, true}};

		DefaultGangData[gng].HasOverrides[OVERRIDE_SNOW] = false;

		//////////////////////////////////////////////////////////////// NATIVES ///////////////////////////////////////////////////////////// 

		gng = GANG_WAPITI;

		DefaultGangData[gng].Voices = {};

		ManagerPed = {"CS_PAYTAH", 1, true, 0};
		ArmoryPed =  {"A_M_M_WAPWARRIORS_01", 68, false, 68};
		HorsePed =   {"A_M_M_WAPWARRIORS_01", 68, false, 3};
		MissionPed = {"A_M_M_WAPWARRIORS_01", 68, false, 41};

		GenericPed1 = {"A_M_M_WAPWARRIORS_01", 68, false, -1, {3, 29, 33, 37, 41, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 57, 58, 65, 66, 67}};

		collection = {ManagerPed, ArmoryPed, HorsePed, MissionPed, {GenericPed1}};

		DefaultGangData[gng].Default = collection;

		DefaultGangData[gng].HasOverrides[OVERRIDE_ROBBERY] = true;
		DefaultGangData[gng].Override_Robbery = collection;
		DefaultGangData[gng].Override_Robbery.GenericPeds = {{"A_M_M_WAPWARRIORS_01", 68, false, -1, {}, {29, 33, 41, 65, 66, 67}}};

		DefaultGangData[gng].HasOverrides[OVERRIDE_SNOW] = false;

		//////////////////////////////////////////////////////////////// SKINNERS //////////////////////////////////////////////////////////// 

		gng = GANG_SKINNERS;

		DefaultGangData[gng].Voices = {};

		ManagerPed = {"G_M_M_UNIMOUNTAINMEN_01", 128, false, 77};
		ArmoryPed =  {"G_M_M_UNIMOUNTAINMEN_01", 128, false, 73};
		HorsePed =   {"G_M_M_UNIMOUNTAINMEN_01", 128, false, 42};
		MissionPed = {"G_M_M_UNIMOUNTAINMEN_01", 128, false, 30};

		collection = {ManagerPed, ArmoryPed, HorsePed, MissionPed, {{"G_M_M_UNIMOUNTAINMEN_01", 128, true}}};

		DefaultGangData[gng].Default = collection;

		DefaultGangData[gng].HasOverrides[OVERRIDE_ROBBERY] = false;
		DefaultGangData[gng].HasOverrides[OVERRIDE_SNOW] = false;

		//////////////////////////////////////////////////////////// VAN DER LINDE GANG ////////////////////////////////////////////////////// 

		gng = GANG_DUTCH;

		DefaultGangData[gng].Voices = {"Dutch"};

		PedInfo arthur = {"player_zero", 1, false, 0};

		PedCollection dutchCollection;
		
		dutchCollection.ManagerPed = arthur;
		dutchCollection.ArmoryPed = arthur;
		dutchCollection.HorsePed = arthur;
		dutchCollection.MissionPed = arthur;
		dutchCollection.GenericPeds = {
			arthur,
			{"CS_johnmarston", 1, false, 0},
			{"CS_dutch", 1, false, 0},
			{"CS_hoseamatthews", 1, false, 0},
			{"CS_billwilliamson", 1, false, 0},
			{"CS_charlessmith", 1, false, 0},
			{"CS_javierescuella", 1, false, 0},
			{"CS_sean", 1, false, 0},
			{"CS_lenny", 1, false, 0},
			{"CS_kieran", 1, false, 0},
			{"CS_MicahBell", 1, false, 0},
			{"CS_CLEET", 1, false, 0},
			{"CS_JOE", 1, false, 0},
			{"CS_mrpearson", 1, false, 0},
			{"CS_uncle", 1, false, 0},
			{"CS_leostrauss", 1, false, 0},
			{"CS_josiahtrelawny", 1, false, 0},
			{"CS_revswanson", 1, false, 0},
			{"CS_karen", 1, false, 0},
			{"CS_marybeth", 1, false, 0},
			{"CS_tilly", 1, false, 0},
			{"CS_mollyoshea", 1, false, 0},
			{"CS_mrsadler", 1, false, 0},
			{"CS_susangrimshaw", 1, false, 0},
			{"CS_abigailroberts", 1, false, 0}
		};

		DefaultGangData[gng].Default = dutchCollection;

		DefaultGangData[gng].HasOverrides[OVERRIDE_ROBBERY] = false;

		DefaultGangData[gng].HasOverrides[OVERRIDE_SNOW] = true;
		DefaultGangData[gng].Override_Snow = dutchCollection;

		/////////////////////////////////////////////////////////////// CUSTOM GANG ////////////////////////////////////////////////////////// 

		gng = GANG_CUSTOM;

		DefaultGangData[gng].Voices = {"Custom"};

		ManagerPed = {INI::GetCustomGangFixedPed(FPED_MANAGER), 1, false, 0};
		ArmoryPed =  {INI::GetCustomGangFixedPed(FPED_ARMORY), 1, false, 0};
		HorsePed =   {INI::GetCustomGangFixedPed(FPED_HORSE), 1, false, 0};
		MissionPed = {INI::GetCustomGangFixedPed(FPED_MISSION), 1, false, 0};

		collection = {ManagerPed, ArmoryPed, HorsePed, MissionPed, {}};

		std::vector<std::string> ambients = INI::GetCustomGangGenericPeds();

		for (const std::string& ped : ambients)
		{
			collection.GenericPeds.push_back({ped, 1, true, 0});
		}

		DefaultGangData[gng].Default = collection;

		DefaultGangData[gng].HasOverrides[OVERRIDE_ROBBERY] = false;
		DefaultGangData[gng].HasOverrides[OVERRIDE_SNOW] = false;
	}

	void LoadGangAssets()
	{
		DioScope(DIOTAG);

		DioTrace("Loading peds for gang %d", G_GANG);

		if (G_GANG == GANG_CUSTOM)
		{
			LoadPed(DEFAULT_PED);
		}

		if (G_CAMP_UPGR >= 3)
		{
			DioTrace("Camp upgrade %d is high enough, loading doggo", G_CAMP_UPGR);
			LoadPed(CampDogModel);
		}

		if (G_CAMP == DISTRICT_GRIZZLIES_WEST && DefaultGangData[G_GANG].HasOverrides[OVERRIDE_SNOW])
		{
			DioTrace("Camp %d is snow and gang has snow override, loading snow peds only", G_CAMP);
			LoadPed(DefaultGangData[G_GANG].Override_Snow.ManagerPed.model);
			LoadPed(DefaultGangData[G_GANG].Override_Snow.ArmoryPed.model);
			LoadPed(DefaultGangData[G_GANG].Override_Snow.HorsePed.model);
			LoadPed(DefaultGangData[G_GANG].Override_Snow.MissionPed.model);

			for (int i = 0; i < DefaultGangData[G_GANG].Override_Snow.GenericPeds.size(); i++)
			{
				LoadPed(DefaultGangData[G_GANG].Override_Snow.GenericPeds[i].model);
			}
		}
		else if (DefaultGangData[G_GANG].HasOverrides[OVERRIDE_ROBBERY])
		{
			DioTrace("Gang has robbery override, loading default and robbery peds");
			LoadPed(DefaultGangData[G_GANG].Default.ManagerPed.model);
			LoadPed(DefaultGangData[G_GANG].Default.ArmoryPed.model);
			LoadPed(DefaultGangData[G_GANG].Default.HorsePed.model);
			LoadPed(DefaultGangData[G_GANG].Default.MissionPed.model);

			for (int i = 0; i < DefaultGangData[G_GANG].Default.GenericPeds.size(); i++)
			{
				LoadPed(DefaultGangData[G_GANG].Default.GenericPeds[i].model);
			}

			LoadPed(DefaultGangData[G_GANG].Override_Robbery.ManagerPed.model);
			LoadPed(DefaultGangData[G_GANG].Override_Robbery.ArmoryPed.model);
			LoadPed(DefaultGangData[G_GANG].Override_Robbery.HorsePed.model);
			LoadPed(DefaultGangData[G_GANG].Override_Robbery.MissionPed.model);

			for (int i = 0; i < DefaultGangData[G_GANG].Override_Robbery.GenericPeds.size(); i++)
			{
				LoadPed(DefaultGangData[G_GANG].Override_Robbery.GenericPeds[i].model);
			}
		}
		else
		{
			DioTrace("Loading default peds only");
			LoadPed(DefaultGangData[G_GANG].Default.ManagerPed.model);
			LoadPed(DefaultGangData[G_GANG].Default.ArmoryPed.model);
			LoadPed(DefaultGangData[G_GANG].Default.HorsePed.model);
			LoadPed(DefaultGangData[G_GANG].Default.MissionPed.model);

			for (int i = 0; i < DefaultGangData[G_GANG].Default.GenericPeds.size(); i++)
			{
				LoadPed(DefaultGangData[G_GANG].Default.GenericPeds[i].model);
			}
		}
	}

	int ChoosePedSkin(PedInfo info)
	{
		if ((info.DontCareAboutSkin || info.SpecificSkin == 255) && info.maxskins > 0)
		{
			return rand() % info.maxskins;
		}

		if (info.SpecificSkin > -1 && info.SpecificSkin != 255)
		{
			return info.SpecificSkin;
		}

		if (info.ProhibitedSkins.size() > 0 && info.maxskins > 0)
		{
			int ChosenSkin = 0;
			bool RerollSkin = true;
			bool FoundProhibited = false;

			while (RerollSkin)
			{
				ChosenSkin = rand() % info.maxskins;
				
				DIOTRACE_EXTRA("Chose skin %d", ChosenSkin);

				FoundProhibited = false;

				for (int i = 0; i < info.ProhibitedSkins.size(); i++)
				{
					DIOTRACE_EXTRA("Checking prohitibed index %d value %d", i, info.ProhibitedSkins[i]);

					if (ChosenSkin == info.ProhibitedSkins[i])
					{
						DIOTRACE_EXTRA("Skin is prohibited");
						FoundProhibited = true;
					}
				}

				RerollSkin = FoundProhibited;

				WAIT(1);
			}

			return ChosenSkin;
		}

		if (info.AllowedSkins.size() > 0 && info.maxskins > 0)
		{
			int ChosenSkin = 0;
			bool RerollSkin = true;
			bool FoundAllowed = false;

			while (RerollSkin)
			{
				ChosenSkin = rand() % info.maxskins;

				DIOTRACE_EXTRA("Chose skin %d", ChosenSkin);

				for (int i = 0; i < info.AllowedSkins.size(); i++)
				{
					DIOTRACE_EXTRA("Checking allowed index %d value %d", i, info.AllowedSkins[i]);

					if (ChosenSkin == info.AllowedSkins[i])
					{
						DIOTRACE_EXTRA("Skin is allowed");
						FoundAllowed = true;
					}
				}

				RerollSkin = !FoundAllowed;

				WAIT(1);
			}

			return ChosenSkin;
		}

		DioTrace(DIOTAG1("WARNING") "All options fell through, choosing default skin for ped %s", info.model.c_str());
		return 0;
	}
}

namespace CAMP
{
	struct Prop
	{
		Hash hash;
		std::string hashname;
		LocationData pos;
		bool singleprop;
		Hash ModelToUseForNavBlockerPosition;
		Vector3 NavBlockerBounds;
	};

	struct ScenarioPoint
	{
		LocationData pos;
		bool NeedsSpecialHandling;
		std::string scenario;
		bool occupied;
		Ped occupant;
		Vector3 CamPos;
		Vector3 CamRot;
	};

	struct CampUpgrade
	{
		Volume campvol;
		Blip mainblip;
		Blip fixedpedblips[FPED__MAX];
		std::vector<Prop> props;
		std::vector<LocationData> navblockerpositions;
		std::vector<int> activeprops;
		std::vector<Volume> activeblockers;
		ScenarioPoint fixedpeds[FPED__MAX];
		int IdlePedCount;
		std::vector<Vector3> IdlePedSpawnPoints;
		int NumCampFireScenarios;
		std::vector<ScenarioPoint> IdleScenarioPoints;
		int GuardPedCount;
		std::vector<ScenarioPoint> GuardScenarioPoints;
	};

	struct Camp
	{
		Vector3 CampPoint;
		float CampRadius;
		LocationData WarpPoint, HorsePoint;
		CampUpgrade upgrades[4];
	};

	const char MASTER_PROP[] = "s_crateseat03x";

	Camp DefaultCampData[NUM_DISTRICTS];

	CampUpgrade CurrentCamp;
	std::vector<Ped> CurrentCampPeds;
	std::vector<ScenarioPoint> CurrentCampIdleScenarioPoints;
	std::vector<ScenarioPoint> CurrentCampGuardScenarioPoints;
	Vehicle CurrentCampMaxim = 0;

	std::vector<Hash> AvailableLongArms;

	Ped CampHorse;
	Ped CampHorseManager;

	bool TempBlockCampBlips = false;

	int LastSpawnWarnTime = 0;

	int LastShooTaskAssigned = 0;

	const int MinTimeOutsideCamp = 120000;
	bool PlayerInCampRecently = false;
	int TimeLastSeenPlayerInCamp = 0;

	struct PlayerPedConvo
	{
		std::string name;
		std::vector<std::string> queries;
		std::vector<std::string> responses;
	};

	PlayerPedConvo CurrentConvo = {"NONE"};
	Ped CurrentConvoPed = 0;
	int TimeConvoStarted = 0;
	bool PlayerRespondToGuardCallout = false;
	int GuardRespondToPlayerResponse = 0;
	Ped IdleChatterPed = 0;
	int TimeLastPlayedIdleChatter = 0;

	Prompt GreetAmbientPed;
	Prompt AntagonizeAmbientPed;
	int PlayerGreetAmbientPed = 0;
	int PlayerAntagonizeAmbientPed = 0;

	Prompt FixedPedCampInteraction;
	Prompt FixedPedArmoryInteraction;
	Prompt FixedPedHorseInteraction;
	Prompt FixedPedMissionInteraction;
	Prompt FixedPedInteractions[FPED__MAX];

	bool IsCampGroundLoaded()
	{
		Vector3 cpos = DefaultCampData[G_CAMP].CampPoint;

		return STREAMING::_HAS_COLLISION_LOADED_AT_COORD(cpos.x, cpos.y, cpos.z);
	}

	void ShutDownMissionController()
	{
		if (CampMissionController != NULL)
		{
			delete CampMissionController;
			CampMissionController = NULL;
		}
	}

	void ClearCampArea(bool NoClearOwnedEntites = false)
	{
		Vector3 area = DefaultCampData[G_CAMP].CampPoint;
		float radius = DefaultCampData[G_CAMP].CampRadius * 5;

		int iVar1 = 344970;
		iVar1 |= 1 | 1048576 | 524288 | 24 | 40;
		MISC::CLEAR_AREA(area.x, area.y, area.z, radius, iVar1);

		MISC::CLEAR_AREA(area.x, area.y, area.z, radius, 2097152);

		PERSISTENCE::PERSISTENCE_REMOVE_ALL_ENTITIES_IN_AREA(area.x, area.y, area.z, radius);

#ifdef REMOVED
		Ped peds[1024];
		int count = worldGetAllPeds(peds, 1024);

		for (int i = 0; i < count; i++)
		{
			Ped p = peds[i];
			Vector3 ppos = ENTITY::GET_ENTITY_COORDS(p, true, true);
			
			if (MISC::GET_DISTANCE_BETWEEN_COORDS(ppos.x, ppos.y, ppos.z, loc.x, loc.y, loc.z, false) < radius && p != PlayerPed && p != PlayerHorse)
			{
				if (ENTITY::DOES_ENTITY_BELONG_TO_THIS_SCRIPT(p, false) && !NoClearOwnedEntites)
				{
					PED::DELETE_PED(&p);
				}
				
				if (!ENTITY::DOES_ENTITY_BELONG_TO_THIS_SCRIPT(p, false) && p != PLAYER::_GET_SADDLE_HORSE_FOR_PLAYER(0))
				{
					ENTITY::SET_ENTITY_COORDS(p, -3105.839600f, 3507.430420f, 596.533203f, true, true, true, true);
				}
			}
		}
#endif
	}

	bool PedInCampRadius(Ped ped = PLAYER::PLAYER_PED_ID())
	{
		Vector3 ppos = ENTITY::GET_ENTITY_COORDS(ped, true, true);
		Vector3 cpos = CAMP::DefaultCampData[G_CAMP].CampPoint;

		return (MISC::GET_DISTANCE_BETWEEN_COORDS(ppos.x, ppos.y, ppos.z, cpos.x, cpos.y, cpos.z, false) < CAMP::DefaultCampData[G_CAMP].CampRadius);
	}

	void CreateCampPrompts()
	{
		GreetAmbientPed = HUD::_UIPROMPT_REGISTER_BEGIN();
		HUD::_UIPROMPT_SET_CONTROL_ACTION(GreetAmbientPed, MISC::GET_HASH_KEY("INPUT_INTERACT_LOCKON_POS"));
		HUD::_UIPROMPT_SET_TEXT(GreetAmbientPed, MISC::VAR_STRING(10, "LITERAL_STRING", "Greet"));
		HUD::_UIPROMPT_SET_STANDARD_MODE(GreetAmbientPed, 1);
		HUD::_UIPROMPT_REGISTER_END(GreetAmbientPed);
		HUD::_UIPROMPT_SET_ENABLED(GreetAmbientPed, false);
		HUD::_UIPROMPT_SET_VISIBLE(GreetAmbientPed, false);

		AntagonizeAmbientPed = HUD::_UIPROMPT_REGISTER_BEGIN();
		HUD::_UIPROMPT_SET_CONTROL_ACTION(AntagonizeAmbientPed, MISC::GET_HASH_KEY("INPUT_INTERACT_LOCKON_NEG"));
		HUD::_UIPROMPT_SET_TEXT(AntagonizeAmbientPed, MISC::VAR_STRING(10, "LITERAL_STRING", "Antagonize"));
		HUD::_UIPROMPT_SET_STANDARD_MODE(AntagonizeAmbientPed, 1);
		HUD::_UIPROMPT_REGISTER_END(AntagonizeAmbientPed);
		HUD::_UIPROMPT_SET_ENABLED(AntagonizeAmbientPed, false);
		HUD::_UIPROMPT_SET_VISIBLE(AntagonizeAmbientPed, false);

		FixedPedCampInteraction = HUD::_UIPROMPT_REGISTER_BEGIN();
		HUD::_UIPROMPT_SET_CONTROL_ACTION(FixedPedCampInteraction, MISC::GET_HASH_KEY("INPUT_INTERACT_LOCKON_POS"));
		HUD::_UIPROMPT_SET_TEXT(FixedPedCampInteraction, MISC::VAR_STRING(10, "LITERAL_STRING", ManagerPedAction));
		HUD::_UIPROMPT_SET_STANDARD_MODE(FixedPedCampInteraction, 1);
		HUD::_UIPROMPT_REGISTER_END(FixedPedCampInteraction);
		HUD::_UIPROMPT_SET_ENABLED(FixedPedCampInteraction, false);
		HUD::_UIPROMPT_SET_VISIBLE(FixedPedCampInteraction, false);

		FixedPedArmoryInteraction = HUD::_UIPROMPT_REGISTER_BEGIN();
		HUD::_UIPROMPT_SET_CONTROL_ACTION(FixedPedArmoryInteraction, MISC::GET_HASH_KEY("INPUT_INTERACT_LOCKON_POS"));
		HUD::_UIPROMPT_SET_TEXT(FixedPedArmoryInteraction, MISC::VAR_STRING(10, "LITERAL_STRING", ArmoryPedAction));
		HUD::_UIPROMPT_SET_STANDARD_MODE(FixedPedArmoryInteraction, 1);
		HUD::_UIPROMPT_REGISTER_END(FixedPedArmoryInteraction);
		HUD::_UIPROMPT_SET_ENABLED(FixedPedArmoryInteraction, false);
		HUD::_UIPROMPT_SET_VISIBLE(FixedPedArmoryInteraction, false);

		FixedPedHorseInteraction = HUD::_UIPROMPT_REGISTER_BEGIN();
		HUD::_UIPROMPT_SET_CONTROL_ACTION(FixedPedHorseInteraction, MISC::GET_HASH_KEY("INPUT_INTERACT_LOCKON_POS"));
		HUD::_UIPROMPT_SET_TEXT(FixedPedHorseInteraction, MISC::VAR_STRING(10, "LITERAL_STRING", HorsePedAction));
		HUD::_UIPROMPT_SET_STANDARD_MODE(FixedPedHorseInteraction, 1);
		HUD::_UIPROMPT_REGISTER_END(FixedPedHorseInteraction);
		HUD::_UIPROMPT_SET_ENABLED(FixedPedHorseInteraction, false);
		HUD::_UIPROMPT_SET_VISIBLE(FixedPedHorseInteraction, false);

		FixedPedMissionInteraction = HUD::_UIPROMPT_REGISTER_BEGIN();
		HUD::_UIPROMPT_SET_CONTROL_ACTION(FixedPedMissionInteraction, MISC::GET_HASH_KEY("INPUT_INTERACT_LOCKON_POS"));
		HUD::_UIPROMPT_SET_TEXT(FixedPedMissionInteraction, MISC::VAR_STRING(10, "LITERAL_STRING", MissionPedAction));
		HUD::_UIPROMPT_SET_STANDARD_MODE(FixedPedMissionInteraction, 1);
		HUD::_UIPROMPT_REGISTER_END(FixedPedMissionInteraction);
		HUD::_UIPROMPT_SET_ENABLED(FixedPedMissionInteraction, false);
		HUD::_UIPROMPT_SET_VISIBLE(FixedPedMissionInteraction, false);

		FixedPedInteractions[FPED_MANAGER] = FixedPedCampInteraction;
		FixedPedInteractions[FPED_ARMORY] = FixedPedArmoryInteraction;
		FixedPedInteractions[FPED_HORSE] = FixedPedHorseInteraction;
		FixedPedInteractions[FPED_MISSION] = FixedPedMissionInteraction;
	}

	void startup()
	{
		CampUpgrade UPGRADE_0, UPGRADE_1, UPGRADE_2, UPGRADE_3;
		int cid;

		/////////////////////////////////////////////////////// WEST GRIZZLIES ///////////////////////////////////////////////////// 

		cid = DISTRICT_GRIZZLIES_WEST;

		DefaultCampData[cid].CampPoint = {-925.723f, 1563.88f, 237.44f};
		DefaultCampData[cid].CampRadius = 14;
		DefaultCampData[cid].WarpPoint = {{-911.102f, 1551.82f, 239.654f}, 50};
		DefaultCampData[cid].HorsePoint = {{-908.816f, 1559.48f, 240.312f}, 320};

		UPGRADE_0.props = {
			{0, "p_hitchingpost05x", {-907.05f, 1560.70f, 239.84f, 300}, true},
			{0, "p_hitchingpost05x", {-904.55f, 1558.95f, 240.24f, 345}, true},
			{0, "pg_ambcamp01x_tent_canvas_wedge_stripe",	{-925.30f, 1564.75f, 237.44f, 105.0f}, false, 0x602C5D78, {3.3f, 2.2f, 5}},
			{0, "pg_ambcamp01x_tent_canvas_leanto03",		{-921.75f, 1570.85f, 237.44f, 132.5f}, false, 0xBEB90174, {1.4f, 1.6f, 5}},
			{0, "pg_ambcamp01x_tent_canvas_leanto04",		{-921.75f, 1569.81f, 237.44f, 290.0f}, false, 0x1A5D7465, {1.9f, 2.2f, 5}},
			{0, "pg_ambient_camp_add_makeshifttable01",		{-920.48f, 1568.63f, 237.44f, 132.5f}, false, 0xC7A36431, {0.6f, 0.6f, 5}},
			{0, "pg_ambcamp01x_tent_canvas_leanto01",		{-923.52f, 1561.36f, 237.44f, 187.5f}, false, 0x11C8A30F, {2.6f, 2.0f, 5}},
			{0, "pg_ambient_camp_add_makeshifttable02",		{-927.01f, 1563.69f, 237.44f, 162.5f}, false, 0x24D86583, {0.8f, 0.8f, 5}},
			{0, "pg_ambient_camp_add_cart01",				{-917.97f, 1577.96f, 237.44f, 170.0f}, false, 0xA47E4554, {0.7f, 1.4f, 5}},
			{0, "p_lantern04xlowfuel",						{-923.91f, 1555.64f, 238.51f, 185.0f}, true,  0x00000001, {4.0f, 1.0f, 5}},
			{0, "p_firebarrel01x", {-923.74f, 1565.24f, 237.43f , 0}, true},
			{0, "p_campfire_under01x", {-923.68f, 1565.24f, 237.62f , 0}, true},
			{0, "p_lantern04xlowfuel", {-922.78f, 1560.70f, 237.90f, 125}, true},
			{0, "p_lantern04xlowfuel", {-923.83f, 1572.08f, 237.43f, 60}, true},
			{0, "p_lantern04xlowfuel", {-926.78f, 1560.78f, 237.43f, 120}, true},
			{0, "p_shopmap01x", {-919.26f, 1563.95f, 237.91f, 300}, true},
			{0, "WEAPON_REVOLVER_CATTLEMAN", {-923.44f, 1560.74f, 237.93f, -1}, true, 0, {90, 90, -100}},
			{0, "WEAPON_REVOLVER_DOUBLEACTION", {-923.16f, 1560.88f, 237.93f, -1}, true, 0, {90, 90, -75}},
		};

		UPGRADE_0.fixedpeds[FPED_MANAGER].pos = {{-925.81f, 1566.70f, 238.43f}, 174};
		UPGRADE_0.fixedpeds[FPED_MANAGER].NeedsSpecialHandling = false;
		UPGRADE_0.fixedpeds[FPED_MANAGER].scenario = "WORLD_HUMAN_WRITE_NOTEBOOK";
		UPGRADE_0.fixedpeds[FPED_MANAGER].CamPos = {-926.94f, 1565.81f, 238.88f};
		UPGRADE_0.fixedpeds[FPED_MANAGER].CamRot = {-12.94f, 0.00f, -30.54f};

		UPGRADE_0.fixedpeds[FPED_ARMORY].pos = {{-923.82f, 1559.75f, 237.95f}, 30};
		UPGRADE_0.fixedpeds[FPED_ARMORY].NeedsSpecialHandling = true;
		UPGRADE_0.fixedpeds[FPED_ARMORY].scenario = "PROP_CAMP_MICAH_SEAT_CHAIR_CLEAN_GUN";
		UPGRADE_0.fixedpeds[FPED_ARMORY].CamPos = {-922.65f, 1561.31f, 238.47f};
		UPGRADE_0.fixedpeds[FPED_ARMORY].CamRot = {-20.24f, 0.00f, 151.09f};

		UPGRADE_0.fixedpeds[FPED_HORSE].pos = {{-921.37f, 1573.26f, 238.43f}, 360};
		UPGRADE_0.fixedpeds[FPED_HORSE].NeedsSpecialHandling = false;
		UPGRADE_0.fixedpeds[FPED_HORSE].scenario = "WORLD_HUMAN_INSPECT";
		UPGRADE_0.fixedpeds[FPED_HORSE].CamPos = {-923.18f, 1573.71f, 238.87f};
		UPGRADE_0.fixedpeds[FPED_HORSE].CamRot = {-11.98f, 0.00f, -49.28f};

		UPGRADE_0.fixedpeds[FPED_MISSION].pos = {{-919.42f, 1564.54f, 238.43f}, 67};
		UPGRADE_0.fixedpeds[FPED_MISSION].NeedsSpecialHandling = false;
		UPGRADE_0.fixedpeds[FPED_MISSION].scenario = "WORLD_HUMAN_SIT_GROUND_READ_NEWSPAPER";
		UPGRADE_0.fixedpeds[FPED_MISSION].CamPos = {-919.83f, 1565.61f, 238.08f};
		UPGRADE_0.fixedpeds[FPED_MISSION].CamRot = {-8.66f, 0.00f, -143.29f};

		UPGRADE_0.IdlePedSpawnPoints = {
			{-920.89f, 1562.67f, 238.43f},
			{-922.25f, 1569.15f, 238.43f},
			{-924.43f, 1576.05f, 238.43f},
			{-931.79f, 1561.61f, 238.43f}
		};
		UPGRADE_0.IdlePedSpawnPoints.emplace_back(DefaultCampData[cid].CampPoint);

		UPGRADE_0.IdlePedCount = 4;
		UPGRADE_0.NumCampFireScenarios = 4;
		UPGRADE_0.IdleScenarioPoints = {
			{{{-923.09f, 1566.04f, 238.43f}, 138}},
			{{{-924.31f, 1566.01f, 238.43f}, 230}},
			{{{-924.26f, 1564.48f, 238.43f}, 337}},
			{{{-923.17f, 1564.40f, 238.43f}, 48}},

			{{{-923.36f, 1570.82f, 238.43f}, 230}},
			{{{-926.07f, 1573.74f, 238.43f}, 334}},
			{{{-929.11f, 1572.20f, 238.43f}, 34}},
			{{{-931.42f, 1564.45f, 238.43f}, 165}},
			{{{-925.63f, 1561.30f, 238.43f}, 9}},
			{{{-921.45f, 1560.49f, 238.43f}, 324}},
			{{{-917.43f, 1564.29f, 238.43f}, 110}},
			{{{-919.87f, 1569.00f, 238.43f}, 65}},
			{{{-918.85f, 1569.97f, 238.43f}, 98}}
		};

		UPGRADE_0.GuardPedCount = 2;
		UPGRADE_0.GuardScenarioPoints = {
			{{{-905.38f, 1545.32f, 241.83f}, 157}},
			{{{-927.97f, 1548.57f, 238.57f}, 184}},
			{{{-919.39f, 1543.65f, 239.38f}, 195}},
			{{{-936.91f, 1557.41f, 238.43f}, 136}}
		};

		UPGRADE_1 = UPGRADE_0;

		UPGRADE_1.props = {
			{0, "p_hitchingpost05x", {-907.05f, 1560.70f, 239.84f, 300}, true},
			{0, "p_hitchingpost05x", {-904.55f, 1558.95f, 240.24f, 345}, true},
			{0, "pg_ambcamp01x_tent_canvas_wedge_stripe",	{-926.03f, 1566.47f, 237.44f, 105.0f}, false, 0x602C5D78, {3.3f, 2.2f, 5}},
			{0, "pg_ambcamp01x_tent_canvas_pup02",			{-922.36f, 1573.00f, 237.44f, 070.0f}, false, 0xFF70C1FF, {2.4f, 2.6f, 5}},
			{0, "pg_ambcamp01x_tent_burlap",				{-920.63f, 1568.98f, 237.44f, 290.0f}, false, 0x38A7E176, {1.9f, 2.2f, 5}},
			{0, "pg_ambient_camp_add_makeshifttable01",		{-920.48f, 1568.63f, 237.44f, 132.5f}, false, 0xC7A36431, {0.6f, 0.6f, 5}},
			{0, "pg_ambcamp01x_tent_blanket",				{-924.92f, 1561.36f, 237.44f, 187.5f}, false, 0xD33EE4C2, {2.6f, 2.0f, 5}},
			{0, "pg_ambient_camp_add_makeshifttable02",		{-927.01f, 1563.69f, 237.44f, 162.5f}, false, 0x24D86583, {0.8f, 0.8f, 5}},
			{0, "p_lantern04xlowfuel",						{-923.91f, 1555.64f, 238.51f, 185.0f}, true,  0x00000001, {4.0f, 1.0f, 5}},
			{0, "p_campfirebasin01x", {-923.74f, 1565.24f, 237.34f , 0}, true},
			{0, "p_lantern04xlowfuel", {-922.78f, 1560.70f, 237.90f, 125}, true},
			{0, "p_lantern04xlowfuel", {-920.97f, 1575.16f, 237.43f, 60}, true},
			{0, "p_lantern04xlowfuel", {-927.58f, 1561.76f, 237.43f, 120}, true},
			{0, "p_shopmap01x", {-919.26f, 1563.95f, 237.91f, 300}, true},
			{0, "WEAPON_REPEATER_CARBINE", {-923.44f, 1560.79f, 237.93f, -1}, true, 0, {90, 90, -100}},
			{0, "WEAPON_REVOLVER_SCHOFIELD", {-923.16f, 1560.88f, 237.93f, -1}, true, 0, {90, 90, -75}},
		};

		UPGRADE_1.fixedpeds[FPED_MANAGER].pos = {{-927.79f, 1567.64f, 238.43f}, 234};
		UPGRADE_1.fixedpeds[FPED_MANAGER].CamPos = {-927.41f, 1566.09f, 238.82f};
		UPGRADE_1.fixedpeds[FPED_MANAGER].CamRot = {-10.57f, 0.00f, 44.76f};

		UPGRADE_1.fixedpeds[FPED_HORSE].pos = {{-920.29f, 1571.10f, 238.43f}, 320};
		UPGRADE_1.fixedpeds[FPED_HORSE].CamPos = {-921.15f, 1572.39f, 238.95f};
		UPGRADE_1.fixedpeds[FPED_HORSE].CamRot = {-14.80f, 0.00f, -86.23f};
		
		UPGRADE_1.IdlePedSpawnPoints = {
			{-920.89f, 1562.67f, 238.43f},
			{-922.25f, 1569.15f, 238.43f},
			{-931.79f, 1561.61f, 238.43f},
			{-926.14f, 1572.56f, 238.43f}
		};
		UPGRADE_1.IdlePedSpawnPoints.emplace_back(DefaultCampData[cid].CampPoint);

		UPGRADE_1.NumCampFireScenarios = 2;
		UPGRADE_1.IdleScenarioPoints = {
			{{{-922.71f, 1564.37f, 238.43f}, 63}},
			{{{-923.09f, 1566.53f, 238.43f}, 172}},

			{{{-926.19f, 1561.41f, 238.43f}, 354}},
			{{{-930.90f, 1564.99f, 238.43f}, 141}},
			{{{-929.84f, 1572.39f, 238.43f}, 47}},
			{{{-926.04f, 1576.18f, 238.43f}, 101}},
			{{{-923.25f, 1573.25f, 238.43f}, 199}},
			{{{-920.52f, 1567.87f, 238.43f}, 60}},
			{{{-918.49f, 1562.56f, 238.43f}, 126}}
		};

		UPGRADE_2.props = {
			{0, "p_hitchingpost05x", {-907.05f, 1560.70f, 239.84f, 300}, true},
			{0, "p_hitchingpost05x", {-904.55f, 1558.95f, 240.24f, 345}, true},
			{0, "pg_bh_rancher01x",					{-924.03f, 1567.47f, 237.44f, 000.0f}, false, 0x7B4CC188, {3.7f, 2.6f, 5}},
			{0, "pg_companionactivity_robbery",		{-928.62f, 1568.33f, 237.44f, 122.0f}, false, 0x9F350FBB, {2.4f, 1.6f, 5}},
			{0, "pg_companionactivity_rustling",	{-919.35f, 1569.29f, 237.44f, 270.0f}, false, 0xF980CBD,  {1.9f, 0.9f, 5}},
			{0, "pg_ambcamp01x_tent_burlap",		{-924.12f, 1560.82f, 237.44f, 196.0f}, false, 0x38A7E176, {2.6f, 2.0f, 5}},
			{0, "p_tableset01x",					{-923.58f, 1560.50f, 237.44f, 234.0f}, true,  0x00000001, {1.1f, 1.3f, 5}},
			{0, "p_lantern04xlowfuel",				{-923.91f, 1555.64f, 238.51f, 185.0f}, true,  0x00000001, {4.0f, 1.0f, 5}},
			{0, "p_campfirebasin01x", {-930.88f, 1566.36f, 237.34f , 25}, true},
			{0, "p_lantern04xlowfuel", {-918.65f, 1565.78f, 237.89f, 90}, true},
			{0, "WEAPON_REPEATER_WINCHESTER",	{-923.91f, 1560.47f, 238.26f, -1}, true, 0, {-90, -135, 0}},
			{0, "WEAPON_RIFLE_SPRINGFIELD",		{-923.46f, 1560.82f, 238.26f, -1}, true, 0, {-90, -160, 0}},
			{0, "WEAPON_SHOTGUN_PUMP",			{-923.16f, 1560.22f, 238.26f, -1}, true, 0, {-90, 140, 0}}
		};

		UPGRADE_2.fixedpeds[FPED_MANAGER].pos = {{-923.26f, 1574.39f, 237.43f}, 178};
		UPGRADE_2.fixedpeds[FPED_MANAGER].NeedsSpecialHandling = false;
		UPGRADE_2.fixedpeds[FPED_MANAGER].scenario = "WORLD_HUMAN_WRITE_NOTEBOOK";
		UPGRADE_2.fixedpeds[FPED_MANAGER].CamPos = {-923.50f, 1572.83f, 238.88f};
		UPGRADE_2.fixedpeds[FPED_MANAGER].CamRot = {-12.14f, 0.00f, 19.33f};

		UPGRADE_2.fixedpeds[FPED_ARMORY].pos = {{-923.48f, 1561.58f, 237.43f}, 326};
		UPGRADE_2.fixedpeds[FPED_ARMORY].NeedsSpecialHandling = true;
		UPGRADE_2.fixedpeds[FPED_ARMORY].scenario = "WORLD_HUMAN_LEAN_CHECK_PISTOL";
		UPGRADE_2.fixedpeds[FPED_ARMORY].CamPos = {-921.78f, 1561.31f, 238.93f};
		UPGRADE_2.fixedpeds[FPED_ARMORY].CamRot = {-18.98f, 0.00f, 121.73f};

		UPGRADE_2.fixedpeds[FPED_HORSE].pos = {{-917.95f, 1565.14f, 237.43f}, 315};
		UPGRADE_2.fixedpeds[FPED_HORSE].NeedsSpecialHandling = false;
		UPGRADE_2.fixedpeds[FPED_HORSE].scenario = "WORLD_HUMAN_INSPECT";
		UPGRADE_2.fixedpeds[FPED_HORSE].CamPos = {-919.15f, 1566.97f, 238.94f};
		UPGRADE_2.fixedpeds[FPED_HORSE].CamRot = {-12.98f, 0.00f, -93.92f};

		UPGRADE_2.fixedpeds[FPED_MISSION].pos = {{-928.07f, 1568.74f, 237.43f}, 115};
		UPGRADE_2.fixedpeds[FPED_MISSION].NeedsSpecialHandling = false;
		UPGRADE_2.fixedpeds[FPED_MISSION].scenario = "WORLD_CAMP_LENNY_LEAN_READING";
		UPGRADE_2.fixedpeds[FPED_MISSION].CamPos = {-928.50f, 1567.42f, 238.77f};
		UPGRADE_2.fixedpeds[FPED_MISSION].CamRot = {-11.29f, 0.00f, 20.78f};

		UPGRADE_2.IdlePedSpawnPoints = {
			{-920.88f, 1563.21f, 237.43f},
			{-930.29f, 1561.62f, 237.43f},
			{-935.26f, 1566.08f, 237.43f}
		};
		UPGRADE_2.IdlePedSpawnPoints.emplace_back(DefaultCampData[cid].CampPoint);

		UPGRADE_2.IdlePedCount = 5;
		UPGRADE_2.NumCampFireScenarios = 4;
		UPGRADE_2.IdleScenarioPoints = {
			{{{-929.42f, 1565.67f, 237.43f}, 72}},
			{{{-930.97f, 1564.72f, 237.43f}, 352}},
			{{{-930.96f, 1567.86f, 237.43f}, 195}},
			{{{-932.37f, 1567.66f, 237.43f}, 249}},

			{{{-932.99f, 1564.18f, 237.43f}, 131}},
			{{{-933.01f, 1569.91f, 237.43f}, 69}},
			{{{-930.98f, 1570.96f, 237.43f}, 35}},
			{{{-928.45f, 1560.09f, 237.43f}, 32}},
			{{{-925.55f, 1561.43f, 237.43f}, 17}},
			{{{-926.46f, 1565.83f, 237.43f}, 216}},
			{{{-924.18f, 1565.66f, 237.43f}, 16}},
			{{{-922.45f, 1566.56f, 237.43f}, 49}},
			{{{-920.44f, 1566.70f, 237.43f}, 148}}
		};

		UPGRADE_2.GuardPedCount = 3;
		UPGRADE_2.GuardScenarioPoints = UPGRADE_1.GuardScenarioPoints;

		UPGRADE_3 = UPGRADE_2;

		UPGRADE_3.props = {
			{0, "p_hitchingpost05x", {-907.05f, 1560.70f, 239.84f, 300}, true},
			{0, "p_hitchingpost05x", {-904.55f, 1558.95f, 240.24f, 345}, true},
			{0, "pg_bh_rancher01x",					{-924.03f, 1567.47f, 237.44f, 000.0f}, false, 0x7B4CC188, {3.7f, 2.6f, 5}},
			{0, "pg_companionactivity_robbery",		{-928.62f, 1568.33f, 237.44f, 122.0f}, false, 0x9F350FBB, {2.4f, 1.6f, 5}},
			{0, "pg_companionactivity_rustling",	{-919.35f, 1569.29f, 237.44f, 270.0f}, false, 0xF980CBD,  {1.9f, 0.9f, 5}},
			{0, "pg_ambcamp01x_tent_burlap",		{-924.12f, 1560.82f, 237.44f, 196.0f}, false, 0x38A7E176, {2.6f, 2.0f, 5}},
			{0, "p_tableset01x",					{-923.58f, 1560.50f, 237.44f, 234.0f}, true,  0x00000001, {1.1f, 1.3f, 5}},
			{0, "p_lantern04xlowfuel",				{-923.91f, 1555.64f, 238.51f, 185.0f}, true,  0x00000001, {4.0f, 1.0f, 5}},
			{0, "pg_ambient_camp_add_packwagoncookprep01",	{-933.28f, 1567.09f, 237.44f, 004.0f}, false, 0x820D7387, {1.6f, 3.0f, 5}},
			{0, "pg_ambient_camp_add_packwagonpacked01",	{-941.42f, 1566.57f, 237.44f, 236.0f}, false, 0x2949B2E5, {1.5f, 2.4f, 5}},
			{0, "p_cartmaximgun01x",	{-915.33f, 1575.28f, 238.20f, 275.0f}, true},
			{0, "GATLINGMAXIM02",		{-914.95f, 1575.30f, 239.15f, -2}, true, 0, {0, 0, 270}},
			{0, "p_campfirebasin01x", {-930.88f, 1566.36f, 237.34f , 25}, true},
			{0, "p_lantern04xlowfuel", {-918.65f, 1565.78f, 237.89f, 90}, true},
			{0, "WEAPON_SNIPERRIFLE_ROLLINGBLOCK",	{-923.91f, 1560.47f, 238.26f, -1}, true, 0, {-90, -135, 0}},
			{0, "WEAPON_SNIPERRIFLE_ROLLINGBLOCK",	{-923.46f, 1560.82f, 238.26f, -1}, true, 0, {-90, -160, 0}},
			{0, "WEAPON_RIFLE_SPRINGFIELD",			{-923.16f, 1560.22f, 238.26f, -1}, true, 0, {-90, 140, 0}}
		};

		DefaultCampData[cid].upgrades[0] = UPGRADE_0;
		DefaultCampData[cid].upgrades[1] = UPGRADE_1;
		DefaultCampData[cid].upgrades[2] = UPGRADE_2;
		DefaultCampData[cid].upgrades[3] = UPGRADE_3;

		////////////////////////////////////////////////////// CUMBERLAND FOREST /////////////////////////////////////////////////// 

		cid = DISTRICT_CUMBERLAND_FOREST;

		DefaultCampData[cid].CampPoint = {741.61f, 1192.49f, 142.76f};
		DefaultCampData[cid].CampRadius = 17;
		DefaultCampData[cid].WarpPoint = {{753.09f, 1187.28f, 143.24f}, 61};
		DefaultCampData[cid].HorsePoint = {{760.89f, 1172.08f, 141.88f}, 48};

		UPGRADE_0.props = {
			{0, "p_hitchingpost05x", {756.93f, 1172.86f, 142.72f, 117}, true},
			{0, "p_hitchingpost05x", {758.05f, 1170.21f, 142.50f, 120}, true},
			{0, "pg_ambcamp01x_tent_canvas_wedge_stripe",	{737.64f, 1193.62f, 143.80f, 090.0f}, false, 0x602C5D78, {3.3f, 2.2f, 5}},
			{0, "pg_ambcamp01x_tent_canvas_leanto03",		{746.84f, 1197.41f, 143.04f, 142.5f}, false, 0xBEB90174, {1.4f, 1.6f, 5}},
			{0, "pg_ambcamp01x_tent_canvas_leanto04",		{748.83f, 1197.84f, 142.49f, 312.5f}, false, 0x1A5D7465, {1.9f, 2.2f, 5}},
			{0, "pg_ambient_camp_add_makeshifttable01",		{750.25f, 1199.78f, 142.77f, 130.0f}, false, 0xC7A36431, {0.6f, 0.6f, 5}},
			{0, "pg_ambcamp01x_tent_canvas_leanto01",		{744.10f, 1189.31f, 143.83f, 187.5f}, false, 0x11C8A30F, {2.6f, 2.0f, 5}},
			{0, "pg_ambient_camp_add_makeshifttable02",		{743.27f, 1191.26f, 143.86f, 140.0f}, false, 0x24D86583, {0.8f, 0.8f, 5}},
			{0, "p_campfire_under01x", {747.77f, 1197.85f, 142.75f, 0}, true},
			{0, "p_lantern04xlowfuel", {746.17f, 1186.72f, 144.32f, 80.0f}, true},
			{0, "WEAPON_REVOLVER_CATTLEMAN", {745.66f, 1187.06f, 144.34f, -1}, true, 0, {-90, 0, 140}},
			{0, "WEAPON_REVOLVER_DOUBLEACTION", {745.78f, 1187.21f, 144.34f, -1}, true, 0, {-90, 0, 100}}
		};

		UPGRADE_0.fixedpeds[FPED_MANAGER].pos = {{736.39f, 1195.11f, 143.60f}, 216};
		UPGRADE_0.fixedpeds[FPED_MANAGER].NeedsSpecialHandling = false;
		UPGRADE_0.fixedpeds[FPED_MANAGER].scenario = "WORLD_HUMAN_WRITE_NOTEBOOK";
		UPGRADE_0.fixedpeds[FPED_MANAGER].CamPos = {736.82f, 1193.62f, 144.93f};
		UPGRADE_0.fixedpeds[FPED_MANAGER].CamRot = {-9.57f, 0.00f, 39.53f};

		UPGRADE_0.fixedpeds[FPED_ARMORY].pos = {{744.84f, 1186.57f, 144.38f}, 30};
		UPGRADE_0.fixedpeds[FPED_ARMORY].NeedsSpecialHandling = true;
		UPGRADE_0.fixedpeds[FPED_ARMORY].scenario = "PROP_CAMP_MICAH_SEAT_CHAIR_CLEAN_GUN";
		UPGRADE_0.fixedpeds[FPED_ARMORY].CamPos = {746.28f, 1187.54f, 144.80f};
		UPGRADE_0.fixedpeds[FPED_ARMORY].CamRot = {-14.86f, 0.00f, 134.82f};

		UPGRADE_0.fixedpeds[FPED_HORSE].pos = {{746.04f, 1200.84f, 142.54f}, 40};
		UPGRADE_0.fixedpeds[FPED_HORSE].NeedsSpecialHandling = false;
		UPGRADE_0.fixedpeds[FPED_HORSE].scenario = "WORLD_HUMAN_INSPECT";
		UPGRADE_0.fixedpeds[FPED_HORSE].CamPos = {743.97f, 1199.73f, 144.07f};
		UPGRADE_0.fixedpeds[FPED_HORSE].CamRot = {-12.70f, 0.00f, -15.16f};

		UPGRADE_0.fixedpeds[FPED_MISSION].pos = {{750.81f, 1194.84f, 142.83f}, 112};
		UPGRADE_0.fixedpeds[FPED_MISSION].NeedsSpecialHandling = false;
		UPGRADE_0.fixedpeds[FPED_MISSION].scenario = "WORLD_HUMAN_SIT_GROUND_READ_NEWSPAPER";
		UPGRADE_0.fixedpeds[FPED_MISSION].CamPos = {749.46f, 1195.28f, 143.65f};
		UPGRADE_0.fixedpeds[FPED_MISSION].CamRot = {-11.62f, 0.00f, -88.89f};

		UPGRADE_0.IdlePedSpawnPoints = {
			{749.81f, 1201.62f, 141.97f},
			{747.64f, 1193.66f, 142.64f},
			{738.82f, 1192.53f, 142.98f},
			{751.45f, 1185.60f, 143.41f}
		};
		UPGRADE_0.IdlePedSpawnPoints.emplace_back(DefaultCampData[cid].CampPoint);

		UPGRADE_0.IdlePedCount = 4;
		UPGRADE_0.NumCampFireScenarios = 0;
		UPGRADE_0.IdleScenarioPoints = {
			{{{742.31f, 1188.85f, 143.75f}, 307}},
			{{{734.64f, 1192.33f, 143.24f}, 277}},
			{{{740.61f, 1196.15f, 143.25f}, 206}},
			{{{751.85f, 1200.03f, 142.19f}, 60}},
			{{{746.03f, 1197.97f, 142.91f}, 277}},
			{{{749.76f, 1197.67f, 142.65f}, 92}},
			{{{747.78f, 1192.76f, 142.41f}, 27}}
		};

		UPGRADE_0.GuardPedCount = 2;
		UPGRADE_0.GuardScenarioPoints = {
			{{{770.95f, 1178.16f, 141.70f}, 236}},
			{{{778.29f, 1193.68f, 140.72f}, 267}},
			{{{778.91f, 1205.41f, 141.81f}, 298}},
			{{{766.78f, 1184.48f, 142.67f}, 292}}
		};

		UPGRADE_1 = UPGRADE_0;

		UPGRADE_1.props = {
			{0, "p_hitchingpost05x", {756.93f, 1172.86f, 142.72f, 117}, true},
			{0, "p_hitchingpost05x", {758.05f, 1170.21f, 142.50f, 120}, true},
			{0, "pg_ambcamp01x_tent_canvas_wedge_stripe",	{737.48f, 1194.00f, 143.80f, 090.0f}, false, 0x602C5D78, {3.3f, 2.2f, 5}},
			{0, "pg_ambcamp01x_tent_canvas_pup02",			{746.26f, 1197.61f, 142.92f, 142.5f}, false, 0xFF70C1FF, {2.4f, 2.6f, 5}},
			{0, "pg_ambcamp01x_tent_burlap",				{748.82f, 1197.75f, 142.40f, 312.5f}, false, 0x38A7E176, {1.9f, 2.2f, 5}},
			{0, "pg_ambient_camp_add_makeshifttable01",		{750.25f, 1199.78f, 142.77f, 130.0f}, false, 0xC7A36431, {0.6f, 0.6f, 5}},
			{0, "pg_ambcamp01x_tent_blanket",				{743.68f, 1188.72f, 143.65f, 187.5f}, false, 0xD33EE4C2, {2.6f, 2.0f, 5}},
			{0, "pg_ambient_camp_add_makeshifttable02",		{743.27f, 1191.26f, 143.86f, 140.0f}, false, 0x24D86583, {0.8f, 0.8f, 5}},
			{0, "p_campfirerocksml01x", {747.77f, 1197.85f, 142.68f, 0}, true},
			{0, "p_lantern04xlowfuel", {746.17f, 1186.72f, 144.32f, 80.0f}, true},
			{0, "WEAPON_REPEATER_CARBINE", {745.66f, 1187.06f, 144.34f, -1}, true, 0, {-90, 0, 140}},
			{0, "WEAPON_REVOLVER_SCHOFIELD", {745.78f, 1187.21f, 144.34f, -1}, true, 0, {-90, 0, 100}}
		};

		UPGRADE_2.props = {
			{0, "p_hitchingpost05x", {756.93f, 1172.86f, 142.72f, 117}, true},
			{0, "p_hitchingpost05x", {758.05f, 1170.21f, 142.50f, 120}, true},
			{0, "pg_bh_rancher01x",					{728.09f, 1195.29f, 144.10f, 102.0f}, false, 0x7B4CC188, {3.7f, 2.6f, 5}},
			{0, "pg_companionactivity_robbery",		{745.73f, 1199.24f, 142.73f, -160.0f}, false, 0x9F350FBB, {2.4f, 1.6f, 5}},
			{0, "pg_companionactivity_rustling",	{732.69f, 1198.52f, 143.82f, 0.0f}, false, 0xF980CBD,  {1.9f, 0.9f, 5}},
			{0, "pg_ambcamp01x_tent_burlap",		{744.02f, 1187.57f, 143.61f, 196.0f}, false, 0x38A7E176, {2.6f, 2.0f, 5}},
			{0, "p_tableset01x",					{745.91f, 1186.12f, 143.89f, 24.0f}, true,  0x00000001, {1.1f, 1.3f, 5}},
			{0, "p_campfirerocksml01x", {736.67f, 1196.63f, 143.70f, 0}, true},
			{0, "WEAPON_REPEATER_WINCHESTER",	{745.78f, 1186.28f, 144.72, -1}, true, 0, {-90, -120, 0}},
			{0, "WEAPON_RIFLE_SPRINGFIELD",		{746.18f, 1186.48f, 144.72f, -1}, true, 0, {-90, -105, 0}},
			{0, "WEAPON_SHOTGUN_PUMP",			{746.28f, 1185.68f, 144.72f, -1}, true, 0, {-90, 175, 0}}
		};

		UPGRADE_2.fixedpeds[FPED_MANAGER].pos = {{724.27f, 1195.05f, 144.39f}, 271};
		UPGRADE_2.fixedpeds[FPED_MANAGER].NeedsSpecialHandling = false;
		UPGRADE_2.fixedpeds[FPED_MANAGER].scenario = "WORLD_HUMAN_WRITE_NOTEBOOK";
		UPGRADE_2.fixedpeds[FPED_MANAGER].CamPos = {725.93f, 1194.49f, 145.75f};
		UPGRADE_2.fixedpeds[FPED_MANAGER].CamRot = {-10.15f, 0.00f, 93.88f};

		UPGRADE_2.fixedpeds[FPED_ARMORY].pos = {{746.57f, 1186.90f, 143.81f}, 302};
		UPGRADE_2.fixedpeds[FPED_ARMORY].NeedsSpecialHandling = true;
		UPGRADE_2.fixedpeds[FPED_ARMORY].scenario = "WORLD_HUMAN_LEAN_CHECK_PISTOL";
		UPGRADE_2.fixedpeds[FPED_ARMORY].CamPos = {747.85f, 1186.06f, 145.29f};
		UPGRADE_2.fixedpeds[FPED_ARMORY].CamRot = {-17.90f, 0.00f, 96.31f};

		UPGRADE_2.fixedpeds[FPED_HORSE].pos = {{731.27f, 1197.85f, 143.90f}, 43};
		UPGRADE_2.fixedpeds[FPED_HORSE].NeedsSpecialHandling = false;
		UPGRADE_2.fixedpeds[FPED_HORSE].scenario = "WORLD_HUMAN_INSPECT";
		UPGRADE_2.fixedpeds[FPED_HORSE].CamPos = {729.26f, 1196.90f, 145.17f};
		UPGRADE_2.fixedpeds[FPED_HORSE].CamRot = {-11.74f, -0.00f, -14.53f};

		UPGRADE_2.fixedpeds[FPED_MISSION].pos = {{745.43f, 1199.90f, 142.75f}, 200};
		UPGRADE_2.fixedpeds[FPED_MISSION].NeedsSpecialHandling = false;
		UPGRADE_2.fixedpeds[FPED_MISSION].scenario = "WORLD_CAMP_LENNY_LEAN_READING";
		UPGRADE_2.fixedpeds[FPED_MISSION].CamPos = {746.64f, 1198.95f, 144.16f};
		UPGRADE_2.fixedpeds[FPED_MISSION].CamRot = {-18.14f, 0.00f, 90.16f};

		UPGRADE_2.IdlePedSpawnPoints = {
			{733.22f, 1194.52f, 143.87f},
			{740.18f, 1196.14f, 143.27f},
			{737.78f, 1190.15f, 143.07f},
			{750.75f, 1188.58f, 143.14f}
		};
		UPGRADE_2.IdlePedSpawnPoints.emplace_back(DefaultCampData[cid].CampPoint);

		UPGRADE_2.IdlePedCount = 5;
		UPGRADE_2.NumCampFireScenarios = 4;
		UPGRADE_2.IdleScenarioPoints = {
			{{{735.77f, 1197.48f, 143.79f}, 240}},
			{{{737.97f, 1197.65f, 143.65f}, 149}},
			{{{729.02f, 1196.43f, 144.08f}, 148}},
			{{{729.66f, 1194.21f, 144.14f}, 67}},

			{{{730.27f, 1192.31f, 143.73f}, 276}},
			{{{733.25f, 1196.87f, 143.79f}, 187}},
			{{{733.50f, 1192.90f, 143.47f}, 347}},
			{{{736.65f, 1190.90f, 142.71f}, 302}},
			{{{740.00f, 1187.88f, 143.68f}, 25}},
			{{{742.76f, 1188.24f, 143.83f}, 336}},
			{{{742.83f, 1198.45f, 143.07f}, 171}},
			{{{750.17f, 1199.53f, 142.46f}, 215}},
			{{{746.18f, 1191.82f, 142.32f}, 70}}
		};

		UPGRADE_2.GuardPedCount = 3;
		UPGRADE_2.GuardScenarioPoints = UPGRADE_1.GuardScenarioPoints;

		UPGRADE_3 = UPGRADE_2;
		
		UPGRADE_3.props = {
			{0, "p_hitchingpost05x", {756.93f, 1172.86f, 142.72f, 117}, true},
			{0, "p_hitchingpost05x", {758.05f, 1170.21f, 142.50f, 120}, true},
			{0, "pg_bh_rancher01x",					{728.09f, 1195.29f, 144.10f, 102.0f}, false, 0x7B4CC188, {3.7f, 2.6f, 5}},
			{0, "pg_companionactivity_robbery",		{745.73f, 1199.24f, 142.73f, -160.0f}, false, 0x9F350FBB, {2.4f, 1.6f, 5}},
			{0, "pg_companionactivity_rustling",	{732.69f, 1198.52f, 143.82f, 0.0f}, false, 0xF980CBD,  {1.9f, 0.9f, 5}},
			{0, "pg_ambcamp01x_tent_burlap",		{744.02f, 1187.57f, 143.61f, 196.0f}, false, 0x38A7E176, {2.6f, 2.0f, 5}},
			{0, "p_tableset01x",					{745.91f, 1186.12f, 143.89f, 24.0f}, true,  0x00000001, {1.1f, 1.3f, 5}},
			{0, "p_campfirerocksml01x", {736.67f, 1196.63f, 143.70f, 0}, true},
			{0, "pg_ambient_camp_add_packwagoncookprep01",	{723.76f, 1189.00f, 145.09f, 066.0f}, false, 0x820D7387, {1.6f, 3.0f, 5}},
			{0, "pg_ambient_camp_add_packwagonpacked01",	{734.08f, 1188.35f, 145.05f, 122.0f}, false, 0x2949B2E5, {1.5f, 2.4f, 5}},
			{0, "p_cartmaximgun01x",	{743.18f, 1214.01f, 146.44f, 50.0f}, true},
			{0, "GATLINGMAXIM02",		{742.93f, 1214.19f, 147.28f, -2}, true, 0, {0, 0, 180}},
			{0, "WEAPON_SNIPERRIFLE_ROLLINGBLOCK",	{745.78f, 1186.28f, 144.72, -1}, true, 0, {-90, -120, 0}},
			{0, "WEAPON_SNIPERRIFLE_ROLLINGBLOCK",		{746.18f, 1186.48f, 144.72f, -1}, true, 0, {-90, -105, 0}},
			{0, "WEAPON_RIFLE_SPRINGFIELD",			{746.28f, 1185.68f, 144.72f, -1}, true, 0, {-90, 175, 0}}
		};

		DefaultCampData[cid].upgrades[0] = UPGRADE_0;
		DefaultCampData[cid].upgrades[1] = UPGRADE_1;
		DefaultCampData[cid].upgrades[2] = UPGRADE_2;
		DefaultCampData[cid].upgrades[3] = UPGRADE_3;

		//////////////////////////////////////////////////////// BLUEWATER MARSH /////////////////////////////////////////////////// 

		cid = DISTRICT_BLUEGILL_MARSH;

		DefaultCampData[cid].CampPoint = {1953.97f, -478.75f, 40.73f};
		DefaultCampData[cid].CampRadius = 16;
		DefaultCampData[cid].WarpPoint = {{1965.92f, -473.46f, 40.86f}, 135};
		DefaultCampData[cid].HorsePoint = {{1950.55f, -460.24f, 40.70f}, 211};

		UPGRADE_0.props = {
			{0, "p_hitchingpost01x", {1948.82f, -460.83f, 40.73f, 112.0f}, true},
			{0, "pg_ambcamp01x_tent_canvas_wedge_stripe",	{1951.92f, -480.57f, 40.80f, 156.0f}, false, 0x602C5D78, {3.3f, 2.2f, 5}},
			{0, "pg_ambcamp01x_tent_canvas_leanto03",		{1950.38f, -475.22f, 40.65f, 118.0f}, false, 0xBEB90174, {1.4f, 1.6f, 5}},
			{0, "pg_ambcamp01x_tent_canvas_leanto04",		{1957.64f, -477.30f, 40.68f, 062.0f}, false, 0x1A5D7465, {1.9f, 2.2f, 5}},
			{0, "pg_ambient_camp_add_makeshifttable01",		{1953.84f, -477.60f, 40.81f, 210.0f}, false, 0xC7A36431, {0.6f, 0.6f, 5}},
			{0, "pg_ambcamp01x_tent_canvas_leanto01",		{1957.81f, -483.46f, 40.87f, 292.0f}, false, 0x11C8A30F, {2.6f, 2.0f, 5}},
			{0, "pg_ambient_camp_add_makeshifttable02",		{1951.89f, -481.41f, 40.78f, 172.0f}, false, 0x24D86583, {0.8f, 0.8f, 5}},
			{0, "p_campfire_under01x", {1955.82f, -480.00f, 40.82f, 0.0f}, true},
			{0, "p_lantern04xlowfuel", {1956.65f, -483.72f, 41.25f, 256.0f}, true},
			{0, "WEAPON_REVOLVER_CATTLEMAN", {1955.77f, -483.89f, 41.27f, -1}, true, 0, {-90, 0, -130}},
			{0, "WEAPON_REVOLVER_DOUBLEACTION", {1956.01f, -483.61f, 41.27f, -1}, true, 0, {-90, 0, -110}}
		};

		UPGRADE_0.fixedpeds[FPED_MANAGER].pos = {{1949.82f, -481.77f, 40.78f}, 283};
		UPGRADE_0.fixedpeds[FPED_MANAGER].NeedsSpecialHandling = false;
		UPGRADE_0.fixedpeds[FPED_MANAGER].scenario = "WORLD_HUMAN_WRITE_NOTEBOOK";
		UPGRADE_0.fixedpeds[FPED_MANAGER].CamPos = {1951.57f, -482.39f, 42.32f};
		UPGRADE_0.fixedpeds[FPED_MANAGER].CamRot = {-17.80f, 0.00f, 102.93f};

		UPGRADE_0.fixedpeds[FPED_ARMORY].pos = {{1955.57f, -484.86f, 41.33f}, 52};
		UPGRADE_0.fixedpeds[FPED_ARMORY].NeedsSpecialHandling = true;
		UPGRADE_0.fixedpeds[FPED_ARMORY].scenario = "PROP_CAMP_MICAH_SEAT_CHAIR_CLEAN_GUN";
		UPGRADE_0.fixedpeds[FPED_ARMORY].CamPos = {1956.06f, -482.48f, 42.02f};
		UPGRADE_0.fixedpeds[FPED_ARMORY].CamRot = {-20.67f, 0.00f, -165.69f};

		UPGRADE_0.fixedpeds[FPED_HORSE].pos = {{1951.42f, -475.61f, 40.74f}, 38};
		UPGRADE_0.fixedpeds[FPED_HORSE].NeedsSpecialHandling = false;
		UPGRADE_0.fixedpeds[FPED_HORSE].scenario = "WORLD_HUMAN_INSPECT";
		UPGRADE_0.fixedpeds[FPED_HORSE].CamPos = {1949.38f, -476.18f, 42.14f};
		UPGRADE_0.fixedpeds[FPED_HORSE].CamRot = {-13.39f, -0.00f, -22.51f};

		UPGRADE_0.fixedpeds[FPED_MISSION].pos = {{1957.63f, -476.75f, 40.67f}, 98};
		UPGRADE_0.fixedpeds[FPED_MISSION].NeedsSpecialHandling = false;
		UPGRADE_0.fixedpeds[FPED_MISSION].scenario = "WORLD_HUMAN_SIT_GROUND_READ_NEWSPAPER";
		UPGRADE_0.fixedpeds[FPED_MISSION].CamPos = {1956.55f, -475.85f, 41.40f};
		UPGRADE_0.fixedpeds[FPED_MISSION].CamRot = {-13.14f, 0.00f, -107.79f};

		UPGRADE_0.IdlePedSpawnPoints = {
			{1952.14f, -486.85f, 40.84f},
			{1955.71f, -488.48f, 40.87f},
			{1958.39f, -480.80f, 40.86f}
		};
		UPGRADE_0.IdlePedSpawnPoints.emplace_back(DefaultCampData[cid].CampPoint);

		UPGRADE_0.IdlePedCount = 4;
		UPGRADE_0.NumCampFireScenarios = 0;
		UPGRADE_0.IdleScenarioPoints = {
			{{{1954.55f, -480.95f, 40.77f}, 309}},
			{{{1957.18f, -479.69f, 40.82f}, 108}},
			{{{1951.07f, -478.60f, 40.76f}, 241}},
			{{{1954.14f, -474.92f, 40.80f}, 158}},
			{{{1958.99f, -478.33f, 40.75f}, 188}},
			{{{1963.54f, -482.63f, 40.68f}, 48}},
			{{{1959.03f, -483.33f, 40.88f}, 42}},
			{{{1949.71f, -485.69f, 40.86f}, 193}}
		};

		UPGRADE_0.GuardPedCount = 2;
		UPGRADE_0.GuardScenarioPoints = {
			{{{1959.63f, -443.85f, 40.74f}, 328}},
			{{{1985.26f, -464.56f, 40.60f}, 267}},
			{{{1985.23f, -478.42f, 40.69f}, 282}},
			{{{1986.22f, -507.70f, 40.79f}, 277}}
		};

		UPGRADE_1 = UPGRADE_0;

		UPGRADE_1.props = {
			{0, "p_hitchingpost01x", {1948.82f, -460.83f, 40.73f, 112.0f}, true},
			{0, "pg_ambcamp01x_tent_canvas_wedge_stripe",	{1951.92f, -480.57f, 40.80f, 156.0f}, false, 0x602C5D78, {3.3f, 2.2f, 5}},
			{0, "pg_ambcamp01x_tent_canvas_pup02",			{1950.38f, -475.22f, 40.65f, 118.0f}, false, 0xFF70C1FF, {2.4f, 2.6f, 5}},
			{0, "pg_ambcamp01x_tent_burlap",				{1957.64f, -477.30f, 40.68f, 062.0f}, false, 0x38A7E176, {1.9f, 2.2f, 5}},
			{0, "pg_ambient_camp_add_makeshifttable01",		{1953.84f, -477.60f, 40.81f, 210.0f}, false, 0xC7A36431, {0.6f, 0.6f, 5}},
			{0, "pg_ambcamp01x_tent_blanket",				{1957.81f, -483.46f, 40.87f, 292.0f}, false, 0xD33EE4C2, {2.6f, 2.0f, 5}},
			{0, "pg_ambient_camp_add_makeshifttable02",		{1951.89f, -481.41f, 40.78f, 172.0f}, false, 0x24D86583, {0.8f, 0.8f, 5}},
			{0, "p_campfirerocksml01x", {1955.82f, -480.00f, 40.82f, 0.0f}, true},
			{0, "p_lantern04xlowfuel", {1956.65f, -483.72f, 41.25f, 256.0f}, true},
			{0, "WEAPON_REVOLVER_SCHOFIELD", {1955.77f, -483.89f, 41.27f, -1}, true, 0, {-90, 0, -130}},
			{0, "WEAPON_REPEATER_CARBINE", {1956.01f, -483.61f, 41.27f, -1}, true, 0, {-90, 0, -110}}
		};

		UPGRADE_2.props = {
			{0, "p_hitchingpost01x", {1948.82f, -460.83f, 40.73f, 112.0f}, true},
			{0, "pg_bh_rancher01x",					{1952.82f, -480.57f, 40.75f, 100.0f}, false, 0x7B4CC188, {3.7f, 2.6f, 5}},
			{0, "pg_companionactivity_robbery",		{1952.94f, -485.18f, 40.72f, 240.0f}, false, 0x9F350FBB, {2.4f, 1.6f, 5}},
			{0, "pg_companionactivity_rustling",	{1948.03f, -473.93f, 40.54f, 080.0f}, false, 0xF980CBD,  {1.9f, 0.9f, 5}},
			{0, "pg_ambcamp01x_tent_burlap",		{1958.95f, -481.61f, 40.86f, 300.0f}, false, 0x38A7E176, {2.6f, 2.0f, 5}},
			{0, "p_tableset01x",					{1959.28f, -480.89f, 40.84f, 146.0f}, true,  0x00000001, {1.1f, 1.3f, 5}},
			{0, "p_campfirerocksml01x", {1954.99f, -487.31f, 40.84f, 0.0f}, true},
			{0, "WEAPON_REPEATER_WINCHESTER",	{1959.12f, -480.57f, 41.66f, -1}, true, 0, {-90, 0, -130}},
			{0, "WEAPON_SHOTGUN_PUMP",			{1959.52f, -480.83f, 41.66f, -1}, true, 0, {-90, 0, -120}}
		};

		UPGRADE_2.fixedpeds[FPED_MANAGER].pos = {{1946.46f, -480.82f, 40.70f}, 285};
		UPGRADE_2.fixedpeds[FPED_MANAGER].NeedsSpecialHandling = false;
		UPGRADE_2.fixedpeds[FPED_MANAGER].scenario = "WORLD_HUMAN_WRITE_NOTEBOOK";
		UPGRADE_2.fixedpeds[FPED_MANAGER].CamPos = {1947.91f, -481.28f, 42.22f};
		UPGRADE_2.fixedpeds[FPED_MANAGER].CamRot = {-17.99f, 0.00f, 104.16f};

		UPGRADE_2.fixedpeds[FPED_ARMORY].pos = {{1958.18f, -480.95f, 40.86f}, 59};
		UPGRADE_2.fixedpeds[FPED_ARMORY].NeedsSpecialHandling = true;
		UPGRADE_2.fixedpeds[FPED_ARMORY].scenario = "WORLD_HUMAN_LEAN_CHECK_PISTOL";
		UPGRADE_2.fixedpeds[FPED_ARMORY].CamPos = {1958.08f, -479.63f, 42.44f};
		UPGRADE_2.fixedpeds[FPED_ARMORY].CamRot = {-22.94f, 0.00f, -129.76f};

		UPGRADE_2.fixedpeds[FPED_HORSE].pos = {{1951.01f, -474.07f, 40.58f}, 68};
		UPGRADE_2.fixedpeds[FPED_HORSE].NeedsSpecialHandling = false;
		UPGRADE_2.fixedpeds[FPED_HORSE].scenario = "WORLD_HUMAN_INSPECT";
		UPGRADE_2.fixedpeds[FPED_HORSE].CamPos = {1949.34f, -475.77f, 42.15f};
		UPGRADE_2.fixedpeds[FPED_HORSE].CamRot = {-18.53f, 0.00f, 8.18f};

		UPGRADE_2.fixedpeds[FPED_MISSION].pos = {{1952.35f, -484.81f, 40.72f}, 231};
		UPGRADE_2.fixedpeds[FPED_MISSION].NeedsSpecialHandling = false;
		UPGRADE_2.fixedpeds[FPED_MISSION].scenario = "WORLD_CAMP_LENNY_LEAN_READING";
		UPGRADE_2.fixedpeds[FPED_MISSION].CamPos = {1953.79f, -484.79f, 42.21f};
		UPGRADE_2.fixedpeds[FPED_MISSION].CamRot = {-22.18f, 0.00f, 132.36f};

		UPGRADE_2.IdlePedSpawnPoints = {
			{1955.37f, -478.25f, 40.74f},
			{1955.99f, -473.08f, 40.75f},
			{1960.18f, -476.57f, 40.70f},
			{1957.76f, -483.99f, 40.91f}
		};

		UPGRADE_2.IdlePedCount = 5;
		UPGRADE_2.NumCampFireScenarios = 0;
		UPGRADE_2.IdleScenarioPoints = {
			{{{1953.85f, -481.64f, 40.73f}, 42}},
			{{{1954.16f, -479.49f, 40.70f}, 116}},
			{{{1963.61f, -480.29f, 40.68f}, 340}},
			{{{1958.43f, -482.92f, 40.88f}, 99}},
			{{{1955.58f, -486.14f, 40.88f}, 168}},
			{{{1956.26f, -487.90f, 40.88f}, 69}},
			{{{1957.89f, -485.94f, 40.90f}, 355}},
			{{{1961.75f, -486.28f, 40.69f}, 150}}
		};

		UPGRADE_2.GuardPedCount = 3;
		UPGRADE_2.GuardScenarioPoints = UPGRADE_1.GuardScenarioPoints;

		UPGRADE_3 = UPGRADE_2;

		UPGRADE_3.props = {
			{0, "p_hitchingpost01x", {1948.82f, -460.83f, 40.73f, 112.0f}, true},
			{0, "pg_bh_rancher01x",					{1952.82f, -480.57f, 40.75f, 100.0f}, false, 0x7B4CC188, {3.7f, 2.6f, 5}},
			{0, "pg_companionactivity_robbery",		{1952.94f, -485.18f, 40.72f, 240.0f}, false, 0x9F350FBB, {2.4f, 1.6f, 5}},
			{0, "pg_companionactivity_rustling",	{1948.03f, -473.93f, 40.54f, 080.0f}, false, 0xF980CBD,  {1.9f, 0.9f, 5}},
			{0, "pg_ambcamp01x_tent_burlap",		{1958.95f, -481.61f, 40.86f, 300.0f}, false, 0x38A7E176, {2.6f, 2.0f, 5}},
			{0, "p_tableset01x",					{1959.28f, -480.89f, 40.84f, 146.0f}, true,  0x00000001, {1.1f, 1.3f, 5}},
			{0, "p_campfirerocksml01x", {1954.99f, -487.31f, 40.84f, 0.0f}, true},
			{0, "pg_ambient_camp_add_packwagoncookprep01",	{1957.32f, -495.34f, 40.39f, 308.0f}, false, 0x820D7387, {1.6f, 3.0f, 5}},
			{0, "pg_ambient_camp_add_packwagonpacked01",	{1958.21f, -491.84f, 40.64f, 118.0f}, false, 0x2949B2E5, {1.5f, 2.4f, 5}},
			{0, "p_cartmaximgun01x",	{1944.94f, -475.38f, 40.45f, 280.0f}, true},
			{0, "GATLINGMAXIM02",		{1945.26f, -475.34f, 41.40f, -2}, true, 0, {0, 0, 0}},
			{0, "WEAPON_SNIPERRIFLE_ROLLINGBLOCK",	{1959.52f, -480.83f, 41.65f, -1}, true, 0, {90, 0, -120}},
			{0, "WEAPON_RIFLE_SPRINGFIELD",			{1959.12f, -480.57f, 41.65f, -1}, true, 0, {-90, 0, -130}}
		};
		
		DefaultCampData[cid].upgrades[0] = UPGRADE_0;
		DefaultCampData[cid].upgrades[1] = UPGRADE_1;
		DefaultCampData[cid].upgrades[2] = UPGRADE_2;
		DefaultCampData[cid].upgrades[3] = UPGRADE_3;

		///////////////////////////////////////////////////////////// RIO BRAVO //////////////////////////////////////////////////// 

		cid = DISTRICT_RIO_BRAVO;

		DefaultCampData[cid].CampPoint = {-4916.46f, -3145.15f, -14.17f};
		DefaultCampData[cid].CampRadius = 11;
		DefaultCampData[cid].WarpPoint = {{-4919.67f, -3129.14f, -14.56f}, 195};
		DefaultCampData[cid].HorsePoint = {{-4907.20f, -3129.84f, -14.54f}, 233};

		UPGRADE_0.props = {
			{0, "p_hitchingpost01x", {-4905.20f, -3131.74f, -14.48f, 224.0f}, true},
			{0, "pg_ambcamp01x_tent_canvas_wedge_stripe",	{-4914.10f, -3145.89f, -14.16f, 258.0f}, false, 0x602C5D78, {3.3f, 2.2f, 5}},
			{0, "pg_ambcamp01x_tent_canvas_leanto03",		{-4917.38f, -3144.53f, -14.16f, 144.0f}, false, 0xBEB90174, {1.4f, 1.6f, 5}},
			{0, "pg_ambcamp01x_tent_canvas_leanto04",		{-4918.27f, -3142.09f, -14.23f, 116.0f}, false, 0x1A5D7465, {1.9f, 2.2f, 5}},
			{0, "pg_ambient_camp_add_makeshifttable01",		{-4916.65f, -3135.70f, -14.45f, 056.0f}, false, 0xC7A36431, {0.6f, 0.6f, 5}},
			{0, "pg_ambcamp01x_tent_canvas_leanto01",		{-4915.27f, -3137.15f, -14.39f, 316.0f}, false, 0x11C8A30F, {2.6f, 2.0f, 5}},
			{0, "pg_ambient_camp_add_makeshifttable02",		{-4917.69f, -3142.33f, -14.36f, 252.0f}, false, 0x24D86583, {0.8f, 0.8f, 5}},
			{0, "p_campfire_under01x", {-4917.64f, -3141.56f, -14.31f, 0.0f}, true},
			{0, "p_lantern04xlowfuel", {-4914.61f, -3138.09f, -13.90f, 162.0f}, true},
			{0, "WEAPON_REVOLVER_DOUBLEACTION", {-4914.70f, -3138.71f, -13.88f, -1}, true, 0, {-90, 0, -110}},
			{0, "WEAPON_REVOLVER_CATTLEMAN", {-4914.95f, -3138.57f, -13.88f, -1}, true, 0, {-90, 0, -130}}
		};

		UPGRADE_0.fixedpeds[FPED_MANAGER].pos = {{-4914.09f, -3147.37f, -14.15f}, 18};
		UPGRADE_0.fixedpeds[FPED_MANAGER].NeedsSpecialHandling = false;
		UPGRADE_0.fixedpeds[FPED_MANAGER].scenario = "WORLD_HUMAN_WRITE_NOTEBOOK";
		UPGRADE_0.fixedpeds[FPED_MANAGER].CamPos = {-4913.88f, -3145.53f, -12.79f};
		UPGRADE_0.fixedpeds[FPED_MANAGER].CamRot = {-9.88f, 0.00f, -158.56f};

		UPGRADE_0.fixedpeds[FPED_ARMORY].pos = {{-4913.85f, -3139.23f, -13.83f}, 125};
		UPGRADE_0.fixedpeds[FPED_ARMORY].NeedsSpecialHandling = true;
		UPGRADE_0.fixedpeds[FPED_ARMORY].scenario = "PROP_CAMP_MICAH_SEAT_CHAIR_CLEAN_GUN";
		UPGRADE_0.fixedpeds[FPED_ARMORY].CamPos = {-4915.43f, -3138.16f, -13.35f};
		UPGRADE_0.fixedpeds[FPED_ARMORY].CamRot = {-23.47f, 0.00f, -108.28f};

		UPGRADE_0.fixedpeds[FPED_HORSE].pos = {{-4925.11f, -3145.11f, -13.97f}, 107};
		UPGRADE_0.fixedpeds[FPED_HORSE].NeedsSpecialHandling = false;
		UPGRADE_0.fixedpeds[FPED_HORSE].scenario = "WORLD_HUMAN_INSPECT";
		UPGRADE_0.fixedpeds[FPED_HORSE].CamPos = {-4925.03f, -3147.43f, -12.23f};
		UPGRADE_0.fixedpeds[FPED_HORSE].CamRot = {-17.59f, 0.00f, 53.10f};

		UPGRADE_0.fixedpeds[FPED_MISSION].pos = {{-4920.05f, -3138.23f, -14.38f}, 299};
		UPGRADE_0.fixedpeds[FPED_MISSION].NeedsSpecialHandling = false;
		UPGRADE_0.fixedpeds[FPED_MISSION].scenario = "WORLD_HUMAN_SIT_GROUND_READ_NEWSPAPER";
		UPGRADE_0.fixedpeds[FPED_MISSION].CamPos = {-4918.83f, -3138.55f, -13.74f};
		UPGRADE_0.fixedpeds[FPED_MISSION].CamRot = {-9.13f, -0.00f, 95.20f};

		UPGRADE_0.IdlePedSpawnPoints = {
			{-4910.48f, -3143.13f, -14.21f},
			{-4920.82f, -3148.79f, -14.03f}
		};
		UPGRADE_0.IdlePedSpawnPoints.emplace_back(DefaultCampData[cid].CampPoint);

		UPGRADE_0.IdlePedCount = 4;
		UPGRADE_0.NumCampFireScenarios = 0;
		UPGRADE_0.IdleScenarioPoints = {
			{{{-4908.16f, -3147.85f, -14.13f}, 298}},
			{{{-4907.70f, -3144.40f, -14.18f}, 96}},
			{{{-4908.45f, -3139.10f, -14.30f}, 152}},
			{{{-4916.40f, -3147.12f, -14.13f}, 327}},
			{{{-4919.15f, -3149.12f, -14.07f}, 1}},
			{{{-4918.66f, -3143.62f, -14.18f}, 272}},
			{{{-4919.00f, -3140.67f, -14.28f}, 261}},
			{{{-4916.60f, -3140.72f, -14.29f}, 135}}
		};

		UPGRADE_0.GuardPedCount = 2;
		UPGRADE_0.GuardScenarioPoints = {
			{{{-4945.91f, -3137.28f, -13.02f}, 69}},
			{{{-4935.56f, -3121.81f, -14.74f}, 40}},
			{{{-4916.92f, -3116.51f, -13.35f}, 9}},
			{{{-4897.17f, -3116.31f, -11.34f}, 321}}
		};

		UPGRADE_1 = UPGRADE_0;

		UPGRADE_1.props = {
			{0, "p_hitchingpost01x", {-4905.20f, -3131.74f, -14.48f, 224.0f}, true},
			{0, "pg_ambcamp01x_tent_canvas_wedge_stripe",	{-4914.10f, -3145.89f, -14.16f, 258.0f}, false, 0x602C5D78, {3.3f, 2.2f, 5}},
			{0, "pg_ambcamp01x_tent_canvas_pup02",			{-4917.38f, -3144.53f, -14.16f, 144.0f}, false, 0xFF70C1FF, {2.4f, 2.6f, 5}},
			{0, "pg_ambcamp01x_tent_burlap",				{-4919.42f, -3139.14f, -14.34f, 166.0f}, false, 0x38A7E176, {1.9f, 2.2f, 5}},
			{0, "pg_ambient_camp_add_makeshifttable01",		{-4916.65f, -3135.70f, -14.45f, 056.0f}, false, 0xC7A36431, {0.6f, 0.6f, 5}},
			{0, "pg_ambcamp01x_tent_blanket",				{-4915.27f, -3137.15f, -14.39f, 316.0f}, false, 0xD33EE4C2, {2.6f, 2.0f, 5}},
			{0, "pg_ambient_camp_add_makeshifttable02",		{-4917.69f, -3142.33f, -14.36f, 252.0f}, false, 0x24D86583, {0.8f, 0.8f, 5}},
			{0, "p_campfirerocksml01x", {-4917.64f, -3141.56f, -14.31f, 0.0f}, true},
			{0, "p_lantern04xlowfuel", {-4914.61f, -3138.09f, -13.90f, 162.0f}, true},
			{0, "WEAPON_REVOLVER_SCHOFIELD", {-4914.70f, -3138.71f, -13.88f, -1}, true, 0, {-90, 0, -130}},
			{0, "WEAPON_REPEATER_CARBINE", {-4914.95f, -3138.57f, -13.88f, -1}, true, 0, {-90, 0, -110}}
		};

		UPGRADE_1.IdlePedSpawnPoints = {
			{-4910.48f, -3143.13f, -14.21f},
			{-4917.50f, -3136.47f, -14.43f},
			DefaultCampData[cid].CampPoint
		};

		UPGRADE_2.props = {
			{0, "p_hitchingpost01x", {-4905.20f, -3131.74f, -14.48f, 224.0f}, true},
			{0, "pg_bh_rancher01x",					{-4914.98f, -3147.46f, -14.13f, 206.0f}, false, 0x7B4CC188, {3.7f, 2.6f, 5}},
			{0, "pg_companionactivity_robbery",		{-4919.22f, -3139.74f, -14.34f, 220.0f}, false, 0x9F350FBB, {2.4f, 1.6f, 5}},
			{0, "pg_companionactivity_rustling",	{-4921.45f, -3144.85f, -14.10f, 292.0f}, false, 0xF980CBD,  {1.9f, 0.9f, 5}},
			{0, "pg_ambcamp01x_tent_burlap",		{-4911.98f, -3137.97f, -14.36f, 312.0f}, false, 0x38A7E176, {2.6f, 2.0f, 5}},
			{0, "p_tableset01x",					{-4911.81f, -3138.76f, -14.34f, 338.0f}, true,  0x00000001, {1.1f, 1.3f, 5}},
			{0, "p_tableset01x",					{-4912.29f, -3146.05f, -14.13f, 202.0f}, true,  0x00000001, {1.1f, 1.3f, 5}},
			{0, "p_campfirerocksml01x", {-4917.64f, -3141.56f, -14.31f, 0.0f}, true},
			{0, "WEAPON_REPEATER_WINCHESTER",	{-4911.53f, -3138.80f, -13.53f, -1}, true, 0, {-90, 0, -130}},
			{0, "WEAPON_SHOTGUN_PUMP",			{-4911.94f, -3138.48f, -13.53f, -1}, true, 0, {-90, 0, -120}}
		};

		UPGRADE_2.fixedpeds[FPED_MANAGER].pos = {{-4912.36f, -3150.71f, -14.12f}, 36};
		UPGRADE_2.fixedpeds[FPED_MANAGER].NeedsSpecialHandling = false;
		UPGRADE_2.fixedpeds[FPED_MANAGER].scenario = "WORLD_HUMAN_WRITE_NOTEBOOK";
		UPGRADE_2.fixedpeds[FPED_MANAGER].CamPos = {-4913.57f, -3149.11f, -12.69f};
		UPGRADE_2.fixedpeds[FPED_MANAGER].CamRot = {-10.80f, 0.00f, -114.27f};

		UPGRADE_2.fixedpeds[FPED_ARMORY].pos = {{-4912.85f, -3139.04f, -14.33f}, 63};
		UPGRADE_2.fixedpeds[FPED_ARMORY].NeedsSpecialHandling = true;
		UPGRADE_2.fixedpeds[FPED_ARMORY].scenario = "WORLD_HUMAN_LEAN_CHECK_PISTOL";
		UPGRADE_2.fixedpeds[FPED_ARMORY].CamPos = {-4912.78f, -3137.32f, -12.87f};
		UPGRADE_2.fixedpeds[FPED_ARMORY].CamRot = {-17.48f, 0.00f, -138.63f};

		UPGRADE_2.fixedpeds[FPED_HORSE].pos = {{-4923.23f, -3144.16f, -14.09f}, 70};
		UPGRADE_2.fixedpeds[FPED_HORSE].NeedsSpecialHandling = false;
		UPGRADE_2.fixedpeds[FPED_HORSE].scenario = "WORLD_HUMAN_INSPECT";
		UPGRADE_2.fixedpeds[FPED_HORSE].CamPos = {-4924.20f, -3146.14f, -12.45f};
		UPGRADE_2.fixedpeds[FPED_HORSE].CamRot = {-15.15f, 0.00f, 25.40f};

		UPGRADE_2.fixedpeds[FPED_MISSION].pos = {{-4919.66f, -3139.22f, -14.35f}, 209};
		UPGRADE_2.fixedpeds[FPED_MISSION].NeedsSpecialHandling = false;
		UPGRADE_2.fixedpeds[FPED_MISSION].scenario = "WORLD_CAMP_LENNY_LEAN_READING";
		UPGRADE_2.fixedpeds[FPED_MISSION].CamPos = {-4918.19f, -3140.04f, -12.82f};
		UPGRADE_2.fixedpeds[FPED_MISSION].CamRot = {-23.14f, 0.00f, 96.51f};

		UPGRADE_2.IdlePedSpawnPoints = {
			{-4910.48f, -3143.13f, -14.21f},
			{-4914.78f, -3135.92f, -14.44f}
		};

		UPGRADE_2.IdlePedCount = 5;
		UPGRADE_2.NumCampFireScenarios = 0;
		UPGRADE_2.IdleScenarioPoints = {
			{{{-4914.23f, -3145.79f, -14.17f}, 156}},
			{{{-4916.69f, -3146.86f, -14.14f}, 255}},
			{{{-4918.68f, -3146.63f, -14.12f}, 327}},
			{{{-4916.56f, -3142.26f, -14.24f}, 65}},
			{{{-4916.34f, -3140.57f, -14.29f}, 135}},
			{{{-4913.10f, -3140.99f, -14.27f}, 127}},
			{{{-4913.17f, -3144.88f, -14.18f}, 30}}
		};

		UPGRADE_2.GuardPedCount = 3;
		UPGRADE_2.GuardScenarioPoints = UPGRADE_1.GuardScenarioPoints;

		UPGRADE_3 = UPGRADE_2;

		UPGRADE_3.props = {
			{0, "p_hitchingpost01x", {-4905.20f, -3131.74f, -14.48f, 224.0f}, true},
			{0, "pg_bh_rancher01x",					{-4914.98f, -3147.46f, -14.13f, 206.0f}, false, 0x7B4CC188, {3.7f, 2.6f, 5}},
			{0, "pg_companionactivity_robbery",		{-4919.22f, -3139.74f, -14.34f, 220.0f}, false, 0x9F350FBB, {2.4f, 1.6f, 5}},
			{0, "pg_companionactivity_rustling",	{-4921.45f, -3144.85f, -14.10f, 292.0f}, false, 0xF980CBD,  {1.9f, 0.9f, 5}},
			{0, "pg_ambcamp01x_tent_burlap",		{-4911.98f, -3137.97f, -14.36f, 312.0f}, false, 0x38A7E176, {2.6f, 2.0f, 5}},
			{0, "p_tableset01x",					{-4911.81f, -3138.76f, -14.34f, 338.0f}, true,  0x00000001, {1.1f, 1.3f, 5}},
			{0, "p_tableset01x",					{-4912.29f, -3146.05f, -14.13f, 202.0f}, true,  0x00000001, {1.1f, 1.3f, 5}},
			{0, "p_campfirerocksml01x", {-4917.64f, -3141.56f, -14.31f, 0.0f}, true},
			{0, "pg_ambient_camp_add_packwagoncookprep01",	{-4912.90f, -3148.55f, -14.13f, 288.0f}, false, 0x820D7387, {1.6f, 3.0f, 5}},
			{0, "pg_ambient_camp_add_packwagonpacked01",	{-4920.73f, -3141.59f, -14.21f, 194.0f}, false, 0x2949B2E5, {1.5f, 2.4f, 5}},
			{0, "p_cartmaximgun01x",	{-4929.80f, -3147.87f, -13.79f, 28.0f}, true},
			{0, "GATLINGMAXIM02",		{-4929.80f, -3147.87f, -13.60f, -2}, true, 0, {0, 0, 0}},
			{0, "WEAPON_SNIPERRIFLE_ROLLINGBLOCK",	{-4911.53f, -3138.80f, -13.53f, -1}, true, 0, {90, 0, -120}},
			{0, "WEAPON_RIFLE_SPRINGFIELD",			{-4911.94f, -3138.48f, -13.53f, -1}, true, 0, {-90, 0, -130}}
		};
		
		DefaultCampData[cid].upgrades[0] = UPGRADE_0;
		DefaultCampData[cid].upgrades[1] = UPGRADE_1;
		DefaultCampData[cid].upgrades[2] = UPGRADE_2;
		DefaultCampData[cid].upgrades[3] = UPGRADE_3;
	}

	bool LoadCampAssets()
	{
		DioScope(DIOTAG);

		int totalprops = (int)DefaultCampData[G_CAMP].upgrades[G_CAMP_UPGR].props.size();

		DioTrace("Loading props for camp %d upgrade %d - %d entries total", G_CAMP, G_CAMP_UPGR, totalprops);

		Hash master = MISC::GET_HASH_KEY("s_crateseat03x");

		if (!STREAMING::HAS_MODEL_LOADED(master))
		{
			DioTrace("Loading master prop s_crateseat03x");
			STREAMING::REQUEST_MODEL(master, true);

			while (!STREAMING::HAS_MODEL_LOADED(master))
			{
				WAIT(1);
			}
		}

		if (G_CAMP_UPGR >= 3)
		{
			Hash maxim = MISC::GET_HASH_KEY("GATLINGMAXIM02");

			if (!STREAMING::HAS_MODEL_LOADED(maxim))
			{
				DioTrace("Loading GATLINGMAXIM02");
				STREAMING::REQUEST_MODEL(maxim, true);

				while (!STREAMING::HAS_MODEL_LOADED(maxim))
				{
					WAIT(1);
				}
			}
		}

		for (int i = 0; i < totalprops; i++)
		{
			Prop p = DefaultCampData[G_CAMP].upgrades[G_CAMP_UPGR].props[i];

			Hash prophash = (p.hash == 0) ? MISC::GET_HASH_KEY(p.hashname.c_str()) : p.hash;
			
			int timeout = 0;

			if (p.singleprop)
			{
				if (!STREAMING::HAS_MODEL_LOADED(prophash) && p.pos.h >= 0)
				{
					DIOTRACE_EXTRA("Loading single prop %s", p.hashname.c_str());
					STREAMING::REQUEST_MODEL(prophash, true);

					while (!STREAMING::HAS_MODEL_LOADED(prophash))
					{
						if (timeout > 500)
						{
							DioTrace(DIOTAG1("ERROR") "TIMED OUT LOADING PROP %s", p.hashname.c_str());
						}
						
						timeout++;
						WAIT(1);
					}
				}
				else
				{
					DIOTRACE_EXTRA("Loaded single prop %s", p.hashname.c_str());
				}
			}
			else
			{
				if (!PROPSET::_HAS_PROP_SET_LOADED(prophash))
				{
					DIOTRACE_EXTRA("Loading propset %s", p.hashname.c_str());
					PROPSET::_REQUEST_PROP_SET(prophash);

					while (!PROPSET::_HAS_PROP_SET_LOADED(prophash))
					{
						WAIT(1);
					}
				}
				else
				{
					DIOTRACE_EXTRA("Loaded propset %s", p.hashname.c_str());
				}
			}
		}

		return true;
	}

	void SpawnCampProps()
	{
		DioScope(DIOTAG);

		int totalprops = (int)DefaultCampData[G_CAMP].upgrades[G_CAMP_UPGR].props.size();

		DioTrace("Spawning camp %d upgrade %d - %d entries total", G_CAMP, G_CAMP_UPGR, totalprops);

		CurrentCamp.activeprops;
		CurrentCamp.activeblockers;

		for (int i = 0; i < totalprops; i++)
		{
			Prop prop = DefaultCampData[G_CAMP].upgrades[G_CAMP_UPGR].props[i];

			Hash prophash = (prop.hash == 0) ? MISC::GET_HASH_KEY(prop.hashname.c_str()) : prop.hash;

			Vector3 pos = prop.pos.p;
			float heading = prop.pos.h;

#ifdef DEBUG_PROP_PLACEMENT
			const int verticaloffset = 0;
#else
			const int verticaloffset = 5;
#endif
			if (prop.singleprop)
			{
				DIOTRACE_EXTRA("Spawning single prop %s", prop.hashname.c_str());

				Object obj = 0;

				if (heading == -1)
				{
					obj = WEAPON::_CREATE_WEAPON_OBJECT(prophash, 0, pos.x, pos.y, pos.z, true, 1.0);

					Vector3 rot = prop.NavBlockerBounds;
					ENTITY::SET_ENTITY_ROTATION(obj, rot.x, rot.y, rot.z, 2, false);
					ENTITY::SET_ENTITY_COORDS(obj, pos.x, pos.y, pos.z, true, true, true, false);
					ENTITY::FREEZE_ENTITY_POSITION(obj, true);
				}
				else if (heading == -2)
				{
					CurrentCampMaxim = VEHICLE::CREATE_VEHICLE(prophash, pos, 0, true, true, false, true);
					ENTITY::SET_ENTITY_AS_MISSION_ENTITY(CurrentCampMaxim, true, true);
					VEHICLE::SET_FORCE_HD_VEHICLE(CurrentCampMaxim, true);
					VEHICLE::SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(CurrentCampMaxim, false);
					ENTITY::SET_ENTITY_INVINCIBLE(CurrentCampMaxim, true);

					for (Object cart : CAMP::CurrentCamp.activeprops)
					{
						if (ENTITY::GET_ENTITY_MODEL(cart) == 0xB4CE6006 /*p_cartmaximgun01x*/)
						{
							ENTITY::ATTACH_ENTITY_TO_ENTITY_PHYSICALLY(CurrentCampMaxim, cart,
								-1, -1, -0.1f, 0.25f, 0.82f, 0, 0, 0, 0, 0, prop.NavBlockerBounds.z, 100.0f, 1, 1, 0, 1, 2, 0, 1065353216 /* Float: 1f */, 1065353216 /* Float: 1f */);

							break;
						}
					}

					continue;
				}
				else
				{
					obj = OBJECT::CREATE_OBJECT(prophash, pos.x, pos.y, pos.z, true, true, false, false, true);

					ENTITY::SET_ENTITY_HEADING(obj, heading);
				}

				ENTITY::SET_ENTITY_AS_MISSION_ENTITY(obj, true, true);
				ENTITY::FREEZE_ENTITY_POSITION(obj, true);
				ENTITY::SET_ENTITY_INVINCIBLE(obj, true);
				OBJECT::SET_OBJECT_TARGETTABLE(obj, false);
				PHYSICS::SET_DISABLE_BREAKING(obj, true);
				PHYSICS::SET_DISABLE_FRAG_DAMAGE(obj, true);
				ENTITY::SET_CAN_CLIMB_ON_ENTITY(obj, false);
				ENTITY::_SET_ENTITY_DECALS_DISABLED(obj, true);

				if (prophash == 0xB4CE6006 /*p_cartmaximgun01x*/)
				{
					ENTITY::SET_CAN_CLIMB_ON_ENTITY(obj, true);
				}

				CAMP::CurrentCamp.activeprops.emplace_back(obj);

				if (prop.ModelToUseForNavBlockerPosition == 1)
				{
					DIOTRACE_EXTRA("Spawning navblocker for single object %s", prop.hashname.c_str());

					Vector3 pos = ENTITY::GET_ENTITY_COORDS(obj, true, true);
					Vector3 rot = { 0, 0, ENTITY::GET_ENTITY_HEADING(obj) };
					Vector3 scale = prop.NavBlockerBounds;

					Volume blocker = VOLUME::_CREATE_VOLUME_BOX(pos.x, pos.y, pos.z, rot.x, rot.y, rot.z, scale.x, scale.y, scale.z);

					if (!PATHFIND::_ADD_NAVMESH_BLOCKING_VOLUME(blocker, 7))
					{
						DioTrace(DIOTAG1("WARNING") "Failed to add navblocker");
					}

					ENTITY::SET_ENTITY_AS_MISSION_ENTITY(blocker, true, true);

					CurrentCamp.activeblockers.emplace_back(blocker);
				}
			}
			else
			{
#ifndef DEBUG_FAST_CAMP
				DIOTRACE_EXTRA("Spawning propset %s", prop.hashname.c_str());

				Object root = OBJECT::CREATE_OBJECT(MISC::GET_HASH_KEY(MASTER_PROP), pos.x, pos.y, pos.z, true, true, false, false, false);

				ENTITY::SET_ENTITY_AS_MISSION_ENTITY(root, true, true);
				ENTITY::SET_ENTITY_COMPLETELY_DISABLE_COLLISION(root, true, false);
				ENTITY::SET_ENTITY_INVINCIBLE(root, true);

				ENTITY::SET_ENTITY_COORDS(root, pos.x, pos.y, pos.z, true, true, true, true);
				ENTITY::PLACE_ENTITY_ON_GROUND_PROPERLY(root, true);
				ENTITY::SET_ENTITY_HEADING(root, heading);
				ENTITY::SET_ENTITY_COORDS(root, pos.x, pos.y, pos.z - verticaloffset, true, true, true, true);
				ENTITY::FREEZE_ENTITY_POSITION(root, true);

				CurrentCamp.activeprops.emplace_back(root);

				WAIT(100);

				PropSet propset = PROPSET::CREATE_PROP_SET_INSTANCE_ATTACHED_TO_ENTITY(prophash, 0, 0, 0 + verticaloffset, root, 0, false, 1, false);
				ItemSet props = ITEMSET::CREATE_ITEMSET(false);

				WAIT(500);

				int i = 0, j = 0;

				if (PROPSET::_GET_ENTITIES_FROM_PROP_SET(propset, props, 0, true, false) <= 0)
				{
					DioTrace(DIOTAG1("WARNING") "Could not get entities from %s", prop.hashname.c_str());
				}
				else
				{
					j = (int)ITEMSET::GET_ITEMSET_SIZE(props);
				}

				while (i <= (j - 1))
				{
					Object obj = MISC::_GET_OBJECT_FROM_INDEXED_ITEM(ITEMSET::GET_INDEXED_ITEM_IN_ITEMSET(i, props));
					if (ENTITY::DOES_ENTITY_EXIST(obj))
					{
						ENTITY::SET_ENTITY_AS_MISSION_ENTITY(obj, true, true);
						ENTITY::SET_ENTITY_INVINCIBLE(obj, true);
						OBJECT::SET_OBJECT_TARGETTABLE(obj, false);
						PHYSICS::SET_DISABLE_BREAKING(obj, true);
						PHYSICS::SET_DISABLE_FRAG_DAMAGE(obj, true);
						ENTITY::SET_CAN_CLIMB_ON_ENTITY(obj, false);
						ENTITY::_SET_ENTITY_DECALS_DISABLED(obj, true);

						CurrentCamp.activeprops.emplace_back(obj);

						if (ENTITY::GET_ENTITY_MODEL(obj) == prop.ModelToUseForNavBlockerPosition)
						{
							DIOTRACE_EXTRA("Spawning navblocker for propset %s based on entity %u", prop.hashname.c_str(), ENTITY::GET_ENTITY_MODEL(obj));

							Vector3 pos = ENTITY::GET_ENTITY_COORDS(obj, true, true);
							Vector3 rot = { 0, 0, ENTITY::GET_ENTITY_HEADING(obj) };
							Vector3 scale = prop.NavBlockerBounds;

							Volume blocker = VOLUME::_CREATE_VOLUME_BOX(pos.x, pos.y, pos.z, rot.x, rot.y, rot.z, scale.x, scale.y, scale.z);

							if (!PATHFIND::_ADD_NAVMESH_BLOCKING_VOLUME(blocker, 7))
							{
								DioTrace(DIOTAG1("WARNING") "Failed to add navblocker");
							}

							ENTITY::SET_ENTITY_AS_MISSION_ENTITY(blocker, true, true);

							CurrentCamp.activeblockers.emplace_back(blocker);
						}
					}

					i++;
				}

				ITEMSET::DESTROY_ITEMSET(props);

				CurrentCamp.activeprops.emplace_back(propset);
#endif
			}
		}
	}

	bool PedIsFixedPed(Ped ped)
	{
		return PED::GET_PED_CONFIG_FLAG(ped, PCF_DisableLockonToRandomPeds, true);
	}

	bool PedIsCampGuard(Ped ped)
	{
		return PED::GET_PED_CONFIG_FLAG(ped, PCF_BlockWeaponSwitching, true);
	}
	
	void SetPedAsCampGuard(Ped* ped)
	{
		PED::SET_PED_CONFIG_FLAG(*ped, PCF_BlockWeaponSwitching, true);
	}

	void SetupCampPed(Ped ped)
	{
		ENTITY::SET_ENTITY_AS_MISSION_ENTITY(ped, true, true);
		PED::SET_PED_RELATIONSHIP_GROUP_HASH(ped, REL_GANG_DUTCHS);
		ENTITY::SET_ENTITY_INVINCIBLE(ped, true);
		PED::SET_PED_CAN_RAGDOLL(ped, false);
		PED::SET_PED_LASSO_HOGTIE_FLAG(ped, 0, false);

		if (PED::IS_PED_HUMAN(ped))
		{
			if (!PedIsFixedPed(ped))
			{
				PED::_SET_PED_PROMPT_NAME(ped, GenericPedName.c_str());

				int groupprompt = HUD::_UIPROMPT_GET_GROUP_ID_FOR_TARGET_ENTITY(ped);

				HUD::_UIPROMPT_SET_GROUP(GreetAmbientPed, groupprompt, 0);
				HUD::_UIPROMPT_SET_VISIBLE(GreetAmbientPed, true);
				HUD::_UIPROMPT_SET_ENABLED(GreetAmbientPed, false);

				HUD::_UIPROMPT_SET_GROUP(AntagonizeAmbientPed, groupprompt, 0);
				HUD::_UIPROMPT_SET_VISIBLE(AntagonizeAmbientPed, true);
				HUD::_UIPROMPT_SET_ENABLED(AntagonizeAmbientPed, false);
			}
			else
			{
				DIOTRACE_EXTRA("Setting up fixed ped");
				PED::SET_PED_SEEING_RANGE(ped, 0.01);
			}
			
			WEAPON::REMOVE_ALL_PED_WEAPONS(ped, true, true);
			WEAPON::GIVE_WEAPON_TO_PED(ped, WEAPON_REVOLVER_CATTLEMAN, 6, false, true, WEAPON_ATTACH_POINT_PISTOL_R, false, 0.5f, 1.0f, 752097756, true, 0, false);

			if (!GANG::DefaultGangData[G_GANG].Voices.empty() && G_GANG != GANG_DUTCH && G_GANG != GANG_CUSTOM)
			{
				int vox = rand() % GANG::DefaultGangData[G_GANG].Voices.size();
				AUDIO::SET_AMBIENT_VOICE_NAME(ped, GANG::DefaultGangData[G_GANG].Voices[vox].c_str());
			}

			PED::SET_PED_SEEING_RANGE(ped, PedIsCampGuard(ped) ? 20.0f : 5.0f);
		}

		for (int i = 0; i < CurrentCamp.activeblockers.size(); i++)
		{
			DIOTRACE_EXTRA("Telling ped to stay out of navblocker %d", i);

			if (PED::_ADD_PED_STAY_OUT_VOLUME(ped, CurrentCamp.activeblockers[i]))
			{
				DIOTRACE_EXTRA("SET_PED_STAY_OUT_VOLUME: Successfully assigned stay out volume for ped ");
			}
			else
			{
				DioTrace(DIOTAG1("WARNING") "Failed to create navblocker for ped");
			}
		}
	}

	void ClearConvoFlags()
	{
		PlayerInCampRecently = false;
		TimeLastSeenPlayerInCamp = 0;
		CurrentConvoPed = 0;
		TimeConvoStarted = 0;
		PlayerRespondToGuardCallout = false;
		GuardRespondToPlayerResponse = 0;
		IdleChatterPed = 0;
		TimeLastPlayedIdleChatter = 0;
		PlayerGreetAmbientPed = 0;
		PlayerAntagonizeAmbientPed = 0;

		CurrentConvo = {"NONE"};

		HUD::_UIPROMPT_SET_ENABLED(GreetAmbientPed, false);
		HUD::_UIPROMPT_SET_ENABLED(AntagonizeAmbientPed, false);
	}

	void CreateMainCampBlip()
	{
		if (TempBlockCampBlips) return;

		Vector3 cpos = DefaultCampData[G_CAMP].CampPoint;

		if (!MAP::DOES_BLIP_EXIST(CurrentCamp.mainblip))
		{
			Blip CampBlip = MAP::BLIP_ADD_FOR_COORDS(1664425300, cpos.x, cpos.y, cpos.z);
			MAP::SET_BLIP_SPRITE(CampBlip, MISC::GET_HASH_KEY("blip_region_hideout"), true);
			MAP::BLIP_ADD_MODIFIER(CampBlip, BLIP_MODIFIER_HIDE_HEIGHT_MARKER);
			MAP::_SET_BLIP_NAME_FROM_PLAYER_STRING(CampBlip, "Your Gang Camp");

			CurrentCamp.mainblip = CampBlip;
		}
	}

	void DeleteMainCampBlip()
	{
		MAP::REMOVE_BLIP(&CurrentCamp.mainblip);
	}

	void CreateCampBlips()
	{
		if (TempBlockCampBlips) return;

		for (int i = 0; i < FPED__MAX; i++)
		{
			Vector3 cpos = DefaultCampData[G_CAMP].upgrades[G_CAMP_UPGR].fixedpeds[i].pos.p;
			std::string sprite = "blip_ambient_secret";
			std::string name = "ERROR: UNKNOWN FIXED PED";

			switch (i)
			{
				case FPED_MANAGER:
					sprite = "blip_proc_home"; // "blip_ambient_quartermaster";
					name = ManagerPedAction;
					break;

				case FPED_ARMORY:
					sprite = "blip_shop_gunsmith";
					name = ArmoryPedAction;
					break;

				case FPED_HORSE:
					sprite = "blip_shop_horse";
					name = HorsePedAction;
					break;

				case FPED_MISSION:
					sprite = "blip_mission_camp"; // "blip_region_hunting";
					name = MissionPedAction;
					break;
			}

			if (!MAP::DOES_BLIP_EXIST(CurrentCamp.fixedpedblips[i]))
			{
				Blip CampBlip = MAP::BLIP_ADD_FOR_COORDS(1664425300, cpos.x, cpos.y, cpos.z);
				MAP::SET_BLIP_SPRITE(CampBlip, MISC::GET_HASH_KEY(sprite.c_str()), true);
				MAP::BLIP_ADD_MODIFIER(CampBlip, BLIP_MODIFIER_HIDE_HEIGHT_MARKER);
				MAP::_SET_BLIP_NAME_FROM_PLAYER_STRING(CampBlip, name.c_str());

				CurrentCamp.fixedpedblips[i] = CampBlip;
			}
		}
	}

	void DeleteCampBlips()
	{
		for (int i = 0; i < FPED__MAX; i++)
		{
			MAP::REMOVE_BLIP(&CurrentCamp.fixedpedblips[i]);
		}
	}

	void SpawnCampHorse()
	{
		if (!ENTITY::DOES_ENTITY_EXIST(CampHorseManager))
		{
			DioTrace(DIOTAG1("WARNING") "Cannot spawn camp horse without a horse manager");
			return;
		}
		
		int InsertSlot = -1;

		for (int i = 0; i < CurrentCampPeds.size(); i++)
		{
			if (CurrentCampPeds[i] == CampHorse)
			{
				InsertSlot = i;
				DioTrace("Found camp horse at index %d", InsertSlot);
			}
		}

		if (InsertSlot > -1 && ENTITY::DOES_ENTITY_EXIST(CampHorse))
		{
			ENTITY::DELETE_ENTITY(&CampHorse);
		}

		LocationData hpos = {ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(CampHorseManager, 0.7f, 0.9f, 3), ENTITY::GET_ENTITY_HEADING(CampHorseManager) + 75};

		std::vector<std::string> chosenhorses = InventoryManager::GetStaticHorseData(CampInventory->GetBestHorse()).PedModels;

		std::string horsename = chosenhorses[rand() % chosenhorses.size()];

		LoadPed(horsename);

		DioTrace("Creating horse %s", horsename.c_str());

		Ped horse = PED::CREATE_PED(MISC::GET_HASH_KEY(horsename.c_str()), hpos.p.x, hpos.p.y, hpos.p.z, hpos.h, false, true, false, false);

		PED::_SET_RANDOM_OUTFIT_VARIATION(horse, true);
		PED::_UPDATE_PED_VARIATION(horse, true, true, true, true, true);

		ENTITY::PLACE_ENTITY_ON_GROUND_PROPERLY(horse, true);
		ENTITY::FREEZE_ENTITY_POSITION(horse, true);

		PED::SET_PED_CONFIG_FLAG(horse, PCF_BlockMountHorsePrompt, true);
		PED::SET_PED_CONFIG_FLAG(horse, PCF_DisableHorseGunshotFleeResponse, true);
		PED::SET_PED_CONFIG_FLAG(horse, PCF_DisableHorseShunting, true);
		PED::SET_PED_RELATIONSHIP_GROUP_HASH(horse, REL_GANG_DUTCHS);
		ENTITY::SET_ENTITY_INVINCIBLE(horse, true);
		PED::SET_PED_CAN_RAGDOLL(horse, false);
		PED::SET_PED_LASSO_HOGTIE_FLAG(horse, 0, false);

		DECORATOR::DECOR_SET_BOOL(horse, "HorseCompanion", true);
		PED::_SET_PED_BODY_COMPONENT(horse, MISC::GET_HASH_KEY("META_HORSE_SADDLE_ONLY"));
		PED::_SET_PED_FACE_FEATURE(horse, 41611, float(rand() % 2));

		if (InsertSlot > -1)
		{
			CurrentCampPeds[InsertSlot] = horse;
		}
		else
		{
			CurrentCampPeds.insert(CurrentCampPeds.begin(), horse);
		}

		CampHorse = horse;
	}

	void SpawnCampDog()
	{
		DioTrace("Camp upgrade %d is high enough, creating doggo", G_CAMP_UPGR);

		Ped dog = PED::CREATE_PED(MISC::GET_HASH_KEY(CampDogModel.c_str()), DefaultCampData[G_CAMP].CampPoint, 0, false, true, false, false);
		PED::_SET_PED_OUTFIT_PRESET(dog, 0, true);

		ENTITY::SET_ENTITY_AS_MISSION_ENTITY(dog, true, true);
		PED::SET_PED_RELATIONSHIP_GROUP_HASH(dog, REL_GANG_DUTCHS);
		ENTITY::SET_ENTITY_INVINCIBLE(dog, true);
		PED::SET_PED_CAN_RAGDOLL(dog, false);
		PED::SET_PED_LASSO_HOGTIE_FLAG(dog, 0, false);

		TASK::TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(dog, true);

		PED::SET_PED_CONFIG_FLAG(dog, PCF_ForceInteractionLockonOnTargetPed, true);
		
		CurrentCampPeds.emplace_back(dog);
	}

	void PopulateCamp()
	{
		DioScope(DIOTAG);

		if (campPopulated)
		{
			DioTrace(DIOTAG1("WARNING") "Camp already populated"); 
			return;
		}

		if (!AllowCampPopulation)
		{
			DioTrace(DIOTAG1("WARNING") "Camp population not allowed");
			return;
		}

#ifdef DEBUG_PROP_PLACEMENT
		DioTrace(DIOTAG1("WARNING") "Camp population aborted by DEBUG_PROP_PLACEMENT");
		return;
#endif
		bool snow = (G_CAMP == DISTRICT_GRIZZLIES_WEST && GANG::DefaultGangData[G_GANG].HasOverrides[GANG::OVERRIDE_SNOW]);

		int idlercount = DefaultCampData[G_CAMP].upgrades[G_CAMP_UPGR].IdlePedCount;
		int guardcount = DefaultCampData[G_CAMP].upgrades[G_CAMP_UPGR].GuardPedCount;
		int ambientcount = (G_MEMBERCOUNT >= idlercount + guardcount) ? idlercount + guardcount : G_MEMBERCOUNT;
		
		DioTrace("Populating camp %d upgrade %d with %d members of gang %d - %d fixed peds - %d ambient peds (snow override - %s)", G_CAMP, G_CAMP_UPGR, G_MEMBERCOUNT, G_GANG, FPED__MAX, ambientcount, (snow ? "YES" : "NO"));

		GANG::LoadGangAssets();

		GANG::PedInfo fixedpedinfos[FPED__MAX];
		std::vector<GANG::PedInfo> ambientpedinfos;

		Vector3 pos = DefaultCampData[G_CAMP].CampPoint;

		CreateCampPrompts();

		if (snow)
		{
			fixedpedinfos[FPED_MANAGER] = GANG::DefaultGangData[G_GANG].Override_Snow.ManagerPed;
			fixedpedinfos[FPED_ARMORY] = GANG::DefaultGangData[G_GANG].Override_Snow.ArmoryPed;
			fixedpedinfos[FPED_HORSE] = GANG::DefaultGangData[G_GANG].Override_Snow.HorsePed;
			fixedpedinfos[FPED_MISSION] = GANG::DefaultGangData[G_GANG].Override_Snow.MissionPed;

			for (int i = 0; i < GANG::DefaultGangData[G_GANG].Override_Snow.GenericPeds.size(); i++)
			{
				ambientpedinfos.insert(ambientpedinfos.begin(), GANG::DefaultGangData[G_GANG].Override_Snow.GenericPeds[i]);
			}
		}
		else
		{
			fixedpedinfos[FPED_MANAGER] = GANG::DefaultGangData[G_GANG].Default.ManagerPed;
			fixedpedinfos[FPED_ARMORY] = GANG::DefaultGangData[G_GANG].Default.ArmoryPed;
			fixedpedinfos[FPED_HORSE] = GANG::DefaultGangData[G_GANG].Default.HorsePed;
			fixedpedinfos[FPED_MISSION] = GANG::DefaultGangData[G_GANG].Default.MissionPed;

			for (int i = 0; i < GANG::DefaultGangData[G_GANG].Default.GenericPeds.size(); i++)
			{
				ambientpedinfos.insert(ambientpedinfos.begin(), GANG::DefaultGangData[G_GANG].Default.GenericPeds[i]);
			}
		}

		if (G_GANG == GANG_DUTCH)
		{
			fixedpedinfos[FPED_MANAGER].model = DUTCH_HELPERS::GetFixedPed(FPED_MANAGER);
			fixedpedinfos[FPED_ARMORY].model = DUTCH_HELPERS::GetFixedPed(FPED_ARMORY);
			fixedpedinfos[FPED_HORSE].model = DUTCH_HELPERS::GetFixedPed(FPED_HORSE);
			fixedpedinfos[FPED_MISSION].model = DUTCH_HELPERS::GetFixedPed(FPED_MISSION);

			std::vector<std::string> ambients = DUTCH_HELPERS::GetAmbientPeds();

			ambientpedinfos.clear();

			for (const std::string& pedName : ambients)
			{
				ambientpedinfos.push_back({pedName, 1, false, 0});
			}

			if (ambientpedinfos.size() < ambientcount)
			{
				ambientcount = (int)ambientpedinfos.size();
				DioTrace("Reducing ambient peds to only %d", ambientcount);
			}
		}

		for (int i = 0; i < FPED__MAX; i++)
		{
			DioTrace("Creating fixed ped %d", i);
			
			Hash pedhash = MISC::GET_HASH_KEY(fixedpedinfos[i].model.c_str());
			
			Ped ped = PED::CREATE_PED(pedhash, pos.x, pos.y, pos.z, 0, false, true, false, false);

			if (fixedpedinfos[i].DontCareAboutSkin)
			{
				PED::_SET_RANDOM_OUTFIT_VARIATION(ped, true);
			}
			else
			{
				PED::_SET_PED_OUTFIT_PRESET(ped, GANG::ChoosePedSkin(fixedpedinfos[i]), false);
			}

			if (G_GANG == GANG_DUTCH)
			{
				DUTCH_HELPERS::SetPedAppearance(ped, fixedpedinfos[i].model, snow, false);
			}

			PED::_UPDATE_PED_VARIATION(ped, true, true, true, true, true);

			WEAPON::REMOVE_ALL_PED_WEAPONS(ped, true, true);

			PED::SET_PED_CONFIG_FLAG(ped, PCF_CanAmbientHeadtrack, false);
			PED::SET_PED_CONFIG_FLAG(ped, PCF_DisableLockonToRandomPeds, true);

			switch (i)
			{
				case FPED_MANAGER:
					PED::_SET_PED_PROMPT_NAME(ped, ManagerPedName);
					break;

				case FPED_ARMORY:
					PED::_SET_PED_PROMPT_NAME(ped, ArmoryPedName);
					break;

				case FPED_HORSE:
					PED::_SET_PED_PROMPT_NAME(ped, HorsePedName);
					break;

				case FPED_MISSION:
					PED::_SET_PED_PROMPT_NAME(ped, MissionPedName);
					break;

				default:
					PED::_SET_PED_PROMPT_NAME(ped, "ERROR: UNKNOWN FIXED PED");
			}

			ScenarioPoint pointdata = DefaultCampData[G_CAMP].upgrades[G_CAMP_UPGR].fixedpeds[i];

			Vector3 scpos = pointdata.pos.p;
			float sch = pointdata.pos.h;
			std::string sc = pointdata.scenario;

			ENTITY::_SET_ENTITY_COORDS_AND_HEADING(ped, scpos.x, scpos.y, scpos.z, sch, true, true, true);
			ENTITY::PLACE_ENTITY_ON_GROUND_PROPERLY(ped, true);
			ENTITY::FREEZE_ENTITY_POSITION(ped, true);

			LoadScenario(sc);

			TASK::TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped, true);
			PED::SET_PED_CONFIG_FLAG(ped, PCF_ForceInteractionLockonOnTargetPed, true);

			WAIT(250);

			TASK::CLEAR_PED_TASKS_IMMEDIATELY(ped, true, true);

			if (pointdata.NeedsSpecialHandling)
			{
				SetupCampPed(ped);

				TASK::TASK_START_SCENARIO_AT_POSITION(ped, MISC::GET_HASH_KEY(sc.c_str()), scpos.x, scpos.y, scpos.z, sch, -1, false, true, 0, -1, false);
			}
			else
			{
				TASK::TASK_START_SCENARIO_IN_PLACE_HASH(ped, MISC::GET_HASH_KEY(sc.c_str()), -1, true, 0, -1.0f, true);
			}

			PED::SET_PED_KEEP_TASK(ped, true);

			int groupprompt = HUD::_UIPROMPT_GET_GROUP_ID_FOR_TARGET_ENTITY(ped);

			HUD::_UIPROMPT_SET_GROUP(FixedPedInteractions[i], groupprompt, 0);
			HUD::_UIPROMPT_SET_VISIBLE(FixedPedInteractions[i], true);
			HUD::_UIPROMPT_SET_ENABLED(FixedPedInteractions[i], true);

			CurrentCampPeds.insert(CurrentCampPeds.begin(), ped);

			if (i == FPED_HORSE)
			{
				CampHorseManager = ped;
				SpawnCampHorse();
			}
		}

#ifdef DEBUG_NO_ALLIES
		DioTrace("Not creating ambient peds");
#else
		for (int i = 1; i <= ambientcount; i++)
		{
			int pedindex = rand() % ambientpedinfos.size();
			
			std::string pedmodel = ambientpedinfos[pedindex].model;
			Hash pedhash = MISC::GET_HASH_KEY(pedmodel.c_str());

			int spawnpoint = -1;
			int maxspawnpoints = (int)DefaultCampData[G_CAMP].upgrades[G_CAMP_UPGR].IdlePedSpawnPoints.size();

			pos = DefaultCampData[G_CAMP].CampPoint;

			if (maxspawnpoints == 0)
			{
				DioTrace(DIOTAG1("WARNING") "Camp does not have spawn points defined, using center point");
			}
			else
			{
				spawnpoint = rand() % maxspawnpoints;

				pos = DefaultCampData[G_CAMP].upgrades[G_CAMP_UPGR].IdlePedSpawnPoints[spawnpoint];
			}
			
			float randomoffsetX = ((((rand() % 10) + 5) * 0.1f) - 1) * 2.5f;
			float randomoffsetY = ((((rand() % 10) + 5) * 0.1f) - 1) * 2.5f;
			float randomheading = (float)(rand() % 359);

			if (G_GANG == GANG_DUTCH)
			{
				ambientpedinfos.erase(ambientpedinfos.begin() + pedindex);
			}

			DioTrace("Creating ambient ped index %d %s (%d/%d) Spawn: %d X: %.2f Y: %.2f H: %.2f", pedindex, pedmodel.c_str(), i, ambientcount, spawnpoint, randomoffsetX, randomoffsetY, randomheading);

			Ped ped = PED::CREATE_PED(pedhash, pos.x + randomoffsetX, pos.y + randomoffsetY, pos.z + 1, randomheading, false, true, false, false);

			if (ambientpedinfos[pedindex].DontCareAboutSkin)
			{
				PED::_SET_RANDOM_OUTFIT_VARIATION(ped, true);
			}
			else
			{
				PED::_SET_PED_OUTFIT_PRESET(ped, GANG::ChoosePedSkin(ambientpedinfos[pedindex]), false);
			}

			if (G_GANG == GANG_DUTCH)
			{
				DUTCH_HELPERS::SetPedAppearance(ped, pedmodel, snow, false);
			}

			PED::_UPDATE_PED_VARIATION(ped, true, true, true, true, true);

			WEAPON::REMOVE_ALL_PED_WEAPONS(ped, true, true);

			ENTITY::PLACE_ENTITY_ON_GROUND_PROPERLY(ped, true);

			TASK::TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped, true);
			PED::SET_PED_CONFIG_FLAG(ped, PCF_ForceInteractionLockonOnTargetPed, true);

			WAIT(250);

			TASK::CLEAR_PED_TASKS_IMMEDIATELY(ped, true, true);

			if (i <= guardcount)
			{
				DIOTRACE_EXTRA("Marking ped as guard");
				SetPedAsCampGuard(&ped);
			}

			CurrentCampPeds.insert(CurrentCampPeds.begin(), ped);
		}

		if (G_CAMP_UPGR >= 3)
		{
			SpawnCampDog();
		}
#endif
		for (Ped ped : CurrentCampPeds)
		{
			if (!WEAPON::HAS_PED_GOT_WEAPON(ped, WEAPON_REVOLVER_CATTLEMAN, 0, false))
			{
				SetupCampPed(ped);
			}
			else
			{
				DIOTRACE_EXTRA("Skipping ped");
			}
		}

		for (int i = 0; i < DefaultCampData[G_CAMP].upgrades[G_CAMP_UPGR].GuardScenarioPoints.size(); i++)
		{
			ScenarioPoint newpoint;
			newpoint.pos = DefaultCampData[G_CAMP].upgrades[G_CAMP_UPGR].GuardScenarioPoints[i].pos;
			newpoint.occupied = false;
			newpoint.occupant = 0;

			CurrentCampGuardScenarioPoints.insert(CurrentCampGuardScenarioPoints.begin(), DefaultCampData[G_CAMP].upgrades[G_CAMP_UPGR].GuardScenarioPoints[i]);
		}

		AvailableLongArms = CampInventory->GetAvailableWeapons(true);

		for (int i = 0; i < DefaultCampData[G_CAMP].upgrades[G_CAMP_UPGR].IdleScenarioPoints.size(); i++)
		{
			ScenarioPoint newpoint;
			newpoint.pos = DefaultCampData[G_CAMP].upgrades[G_CAMP_UPGR].IdleScenarioPoints[i].pos;
			newpoint.occupied = false;
			newpoint.occupant = 0;

			CurrentCampIdleScenarioPoints.insert(CurrentCampIdleScenarioPoints.begin(), DefaultCampData[G_CAMP].upgrades[G_CAMP_UPGR].IdleScenarioPoints[i]);
		}

		campPopulated = true;
	}

	void DepopulateCamp()
	{
		int pl = (int)CurrentCampPeds.size();
		int gl = (int)CurrentCampGuardScenarioPoints.size();
		int il = (int)CurrentCampIdleScenarioPoints.size();

		DioTrace("Depopulating current camp - %d peds %d guard points %d idle points", pl, gl, il);

		for (int i = 0; i < pl; i++)
		{
			if (ENTITY::DOES_ENTITY_EXIST(CurrentCampPeds[i]))
			{
				ENTITY::DELETE_ENTITY(&CurrentCampPeds[i]);
			}
		}

		CurrentCampPeds.clear();
		CurrentCampGuardScenarioPoints.clear();
		CurrentCampIdleScenarioPoints.clear();

		ClearConvoFlags();

		campPopulated = false;
	}

	bool SpawnCampVolume()
	{
		DioTrace("Spawning camp volume");
		
		if (VOLUME::DOES_VOLUME_EXIST(CurrentCamp.campvol))
		{
			DIOTRACE_EXTRA("We already have a camp volume");
			return true;
		}

		Vector3 pos = DefaultCampData[G_CAMP].CampPoint;
		float scale = DefaultCampData[G_CAMP].CampRadius;

		Volume test = VOLUME::_CREATE_VOLUME_SPHERE(pos.x, pos.y, pos.z, 0, 0, 0, scale, scale, scale);

		if (VOLUME::DOES_VOLUME_EXIST(test))
		{
			if (VOLUME::DOES_VOLUME_COLLIDE_WITH_ANY_VOLUME_LOCK(pos.x, pos.y, pos.z, scale, 0, 0, 0))
			{
				DioTrace(DIOTAG1("WARNING") "Camp is inside a volume lock area");
			}
			
			CurrentCamp.campvol = test;
		}
		else
		{
			DioTrace(DIOTAG1("ERROR") "FAILED CREATING CAMP VOLUME");
			return false;
		}

		return true;
	}

	void SpawnAndPopulateCamp()
	{
		if (!hasCamp)
		{
			DioTrace(DIOTAG1("ERROR") "ATTEMPTING TO SPAWN NONEXISTENT CAMP");
			return;
		}
		
		CreateMainCampBlip();

		if (campSpawned)
		{
			DioTrace(DIOTAG1("WARNING") "Camp already spawned");
			return;
		}

		if (!campgroundLoaded)
		{
			if (MISC::GET_GAME_TIMER() - LastSpawnWarnTime > 15000)
			{
				LastSpawnWarnTime = MISC::GET_GAME_TIMER();
				DioTrace(DIOTAG1("WARNING") "Too far away for camp to spawn");
			}

			return;
		}
		
		DioScope(DIOTAG);

		while (!LoadCampAssets())
		{
			WAIT(1);
		}

		if (!SpawnCampVolume())
		{
			return;
		}

		ClearCampArea();
		SpawnCampProps();
		PopulateCamp();

		campSpawned = true;

		if (CampMissionController == NULL)
		{
			DioTrace("Creating new MissionController");
			CampMissionController = new MissionController();
		}
	}

	void DeleteCamp(bool keepmission = false)
	{
		if (!keepmission)
		{
			ClearCampArea();
			ShutDownMissionController();
		}

		int length = (int)CurrentCamp.activeprops.size();

		DioTrace("Deleting current camp - %d objects total", length);

		for (int i = 0; i < length; i++)
		{
			if (OBJECT::DOES_PICKUP_EXIST(CurrentCamp.activeprops[i]))
			{
				DIOTRACE_EXTRA("Deleting pickup");
				OBJECT::REMOVE_PICKUP(CurrentCamp.activeprops[i]);
			}
		}

		for (int i = 0; i < length; i++)
		{
			if (ENTITY::DOES_ENTITY_EXIST(CurrentCamp.activeprops[i]))
			{
				DIOTRACE_EXTRA("Deleting single entity");
				ENTITY::DELETE_ENTITY(&CurrentCamp.activeprops[i]);
			}
		}

		for (int i = 0; i < length; i++)
		{
			if (PROPSET::DOES_PROP_SET_EXIST(CurrentCamp.activeprops[i]))
			{
				DIOTRACE_EXTRA("Deleting propset");
				PROPSET::_DELETE_PROP_SET(CurrentCamp.activeprops[i], true, true);
			}
		}

		CurrentCamp.activeprops.clear();

		if (ENTITY::DOES_ENTITY_EXIST(CurrentCampMaxim))
		{
			DIOTRACE_EXTRA("Deleting Maxim");
			ENTITY::DELETE_ENTITY(&CurrentCampMaxim);
		}

		DIOTRACE_EXTRA("Deleting UI prompts");
		HUD::_UIPROMPT_DELETE(FixedPedCampInteraction);
		HUD::_UIPROMPT_DELETE(FixedPedMissionInteraction);
		HUD::_UIPROMPT_DELETE(FixedPedHorseInteraction);
		HUD::_UIPROMPT_DELETE(FixedPedArmoryInteraction);
		HUD::_UIPROMPT_DELETE(GreetAmbientPed);
		HUD::_UIPROMPT_DELETE(AntagonizeAmbientPed);

		int blength = (int)CurrentCamp.activeblockers.size();

		for (int i = 0; i < blength; i++)
		{
			Volume blocker = CurrentCamp.activeblockers[i];

			if (VOLUME::DOES_VOLUME_EXIST(blocker))
			{
				DIOTRACE_EXTRA("Deleting navigation blocker");
				PATHFIND::_REMOVE_NAVMESH_BLOCKING_VOLUME(blocker);
				VOLUME::_DELETE_VOLUME(blocker);
			}
		}
		
		CurrentCamp.activeblockers.clear();
		
		DeleteMainCampBlip();
		DeleteCampBlips();

		if (VOLUME::DOES_VOLUME_EXIST(CurrentCamp.campvol))
		{
			VOLUME::_DELETE_VOLUME(CurrentCamp.campvol);
		}

		campSpawned = false;
	}

	void TeleportToCamp()
	{
		if (!hasCamp)
		{
			DioTrace(DIOTAG1("ERROR") "ATTEMPTING TO TELEPORT TO NONEXISTENT CAMP");
			return;
		}
		
		SanityCheck();
		
		TASK::CLEAR_PED_TASKS_IMMEDIATELY(PlayerPed, true, true);
		TASK::CLEAR_PED_TASKS_IMMEDIATELY(PlayerHorse, true, true);
		GEN::FreezePlayer(true);

		Camp campdata = DefaultCampData[G_CAMP];

		Vector3 cpos = campdata.CampPoint;
		Vector3 pp = campdata.WarpPoint.p;
		float ph = campdata.WarpPoint.h;
		Vector3 hp = campdata.HorsePoint.p;
		float hh = campdata.HorsePoint.h;

		ENTITY::_SET_ENTITY_COORDS_AND_HEADING(PlayerPed, pp.x, pp.y, pp.z, ph, true, true, true);
		ENTITY::_SET_ENTITY_COORDS_AND_HEADING(PlayerHorse, hp.x, hp.y, hp.z, hh, true, true, true);

		ENTITY::FREEZE_ENTITY_POSITION(PlayerPed, true);
		ENTITY::FREEZE_ENTITY_POSITION(PlayerHorse, true);

		while (!IsCampGroundLoaded())
		{
			WAIT(1);
		}

		campgroundLoaded = true;

		if (!campSpawned)
		{
			while (!LoadCampAssets())
			{
				WAIT(1);
			}

			SpawnAndPopulateCamp();
		}

		ENTITY::PLACE_ENTITY_ON_GROUND_PROPERLY(PlayerPed, true);
		ENTITY::PLACE_ENTITY_ON_GROUND_PROPERLY(PlayerHorse, true);

		ENTITY::FREEZE_ENTITY_POSITION(PlayerPed, false);
		ENTITY::FREEZE_ENTITY_POSITION(PlayerHorse, false);

		TASK::CLEAR_PED_TASKS_IMMEDIATELY(PlayerHorse, true, true);

		CAM::SET_GAMEPLAY_CAM_RELATIVE_HEADING(0, 1);
		CAM::SET_GAMEPLAY_CAM_RELATIVE_PITCH(0, 1);

		GEN::FreezePlayer(false);

		PlayerInCampRecently = true;
		TimeLastSeenPlayerInCamp = MISC::GET_GAME_TIMER();
	}

	void LawArrivedInCamp()
	{
		DioScope(DIOTAG);
		
		if (!CampMissionController->HasActiveMission())
		{
			display("The ~COLOR_RED~law ~COLOR_WHITE~now knows the location of your camp!");
		}

		G_PENDING_RAID = true;

		GEN::DingPlayerHonor();

		PED::SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, REL_GANG_DUTCHS, REL_COP);
		PED::SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, REL_GANG_DUTCHS, REL_BOUNTY_HUNTER);

		DeleteMainCampBlip();
		DeleteCampBlips();
		TempBlockCampBlips = true;

		ClearConvoFlags();

		HUD::_UIPROMPT_SET_VISIBLE(FixedPedCampInteraction, false);
		HUD::_UIPROMPT_SET_VISIBLE(FixedPedMissionInteraction, false);
		HUD::_UIPROMPT_SET_VISIBLE(FixedPedHorseInteraction, false);
		HUD::_UIPROMPT_SET_VISIBLE(FixedPedArmoryInteraction, false);
		HUD::_UIPROMPT_SET_ENABLED(FixedPedCampInteraction, false);
		HUD::_UIPROMPT_SET_ENABLED(FixedPedMissionInteraction, false);
		HUD::_UIPROMPT_SET_ENABLED(FixedPedHorseInteraction, false);
		HUD::_UIPROMPT_SET_ENABLED(FixedPedArmoryInteraction, false);

		bool playedvox = false;

		for (Ped ped : CurrentCampPeds)
		{
			ENTITY::FREEZE_ENTITY_POSITION(ped, false);
			TASK::TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped, true);

			PED::SET_PED_SHOULD_PLAY_COMBAT_SCENARIO_EXIT(ped, 0, 0, 0, 0);

			TASK::CLEAR_PED_TASKS(ped, true, true);

			if (PED::IS_PED_HUMAN(ped))
			{
				PED::_SET_PED_PROMPT_NAME(ped, GenericPedName.c_str());
				PED::SET_PED_SEEING_RANGE(ped, 20);
				
				if (!WEAPON::HAS_PED_GOT_WEAPON(ped, WEAPON_REVOLVER_CATTLEMAN, 0, false))
				{
					TASK::TASK_FLEE_PED(ped, PlayerPed, 4, 0, -1082130432 /* Float: -1f */, -1, 0);
				}
				else
				{
					Blip b = MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_COMPANION, ped);
					MAP::_SET_BLIP_NAME_FROM_PLAYER_STRING(b, GenericPedName.c_str());

					PED::SET_PED_ACCURACY(ped, 70);
					PED::SET_PED_CONFIG_FLAG(ped, PCF_DisableMountSpawning, true);
					PED::SET_PED_CONFIG_FLAG(ped, PCF_DontFindTransportToFollowLeader, true);

					PED::SET_PED_COMBAT_MOVEMENT(ped, 1);
					PED::SET_PED_COMBAT_ATTRIBUTES(ped, CA_PERMIT_CHARGE_BEYOND_DEFENSIVE_AREA, false);

					Vector3 p = DefaultCampData[G_CAMP].CampPoint;

					PED::SET_PED_SPHERE_DEFENSIVE_AREA(ped, p.x, p.y, p.z, DefaultCampData[G_CAMP].CampRadius, 0, false, 0);

					TASK::TASK_COMBAT_HATED_TARGETS(ped, DefaultCampData[G_CAMP].CampRadius * 5);
				}

				if (!playedvox)
				{
					PlayPedChatter(ped, "GANG_CALLOUT_LAW_SPOTTED", "SPEECH_PARAMS_BEAT_SHOUTED_CLEAR_NOSUB");
					playedvox = true;
				}
			}
			else if (PED::IS_PED_MODEL(ped, MISC::GET_HASH_KEY(CampDogModel.c_str())))
			{
				PED::_SET_PED_PROMPT_NAME(ped, GenericPedName.c_str());

				Blip b = MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_COMPANION, ped);
				MAP::_SET_BLIP_NAME_FROM_PLAYER_STRING(b, GenericPedName.c_str());

				TASK::TASK_COMBAT_HATED_TARGETS(ped, DefaultCampData[G_CAMP].CampRadius);
			}
			else
			{
				PED::SET_PED_CONFIG_FLAG(ped, PCF_DisableHorseGunshotFleeResponse, false);
				TASK::TASK_FLEE_PED(ped, PlayerPed, 4, 0, -1082130432 /* Float: -1f */, -1, 0);
			}

			PED::SET_PED_KEEP_TASK(ped, true);
		}

		for (int i = 0; i < CurrentCamp.activeprops.size(); i++)
		{
			Object obj = CurrentCamp.activeprops[i];
			
			ENTITY::FREEZE_ENTITY_POSITION(obj, false);
			ENTITY::SET_ENTITY_INVINCIBLE(obj, false);
			PHYSICS::SET_DISABLE_BREAKING(obj, false);
			PHYSICS::SET_DISABLE_FRAG_DAMAGE(obj, false);
			ENTITY::_SET_ENTITY_DECALS_DISABLED(obj, false);
		}
		
		if (ENTITY::DOES_ENTITY_EXIST(CurrentCampMaxim))
		{
			VEHICLE::SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(CurrentCampMaxim, true);
		}
		
		int CombatTime = MISC::GET_GAME_TIMER();
		bool InCombat = true;
		const int MaxCombatSeconds = 180;
		bool FrozePeds = false;

		while (InCombat)
		{
			if (!LAW::IS_LAW_INCIDENT_ACTIVE(0))
			{
				InCombat = false;
				WAIT(2000);
			}

			if (MISC::GET_GAME_TIMER() - CombatTime >= MaxCombatSeconds * 1000)
			{
				DioTrace("Law in camp loop timed out");
				InCombat = false;
			}

#ifdef DEBUG_COMMANDS
			if (IsKeyJustUp(VK_DELETE))
			{
				DioTrace("Manually exiting law in camp loop");
				InCombat = false;
			}
#endif
			WAIT(0);
		}

		GEN::ClearWantedStatus();
		
		PED::SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, REL_GANG_DUTCHS, REL_COP);
		PED::SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, REL_GANG_DUTCHS, REL_BOUNTY_HUNTER);
		
		GEN::fadeout();
		GEN::FreezePlayer(true);

		TempBlockCampBlips = false;

		DepopulateCamp();
		DeleteCamp(CampMissionController->HasActiveMission());
		campSpawned = false;

		TeleportToCamp();

		GEN::fadein();
	}

	void PrepPlayerforMission()
	{
		GEN::ClearWantedStatus();
		Common::ClearArea(ENTITY::GET_ENTITY_COORDS(PlayerPed, true, true), 200);

		close_menu_gen = true;

		if (!hasCamp)
		{
			DioTrace(DIOTAG1("ERROR") "ATTEMPTING TO TELEPORT TO NONEXISTENT CAMP");
			return;
		}

		SanityCheck();

		TASK::CLEAR_PED_TASKS_IMMEDIATELY(PlayerPed, true, true);
		TASK::CLEAR_PED_TASKS_IMMEDIATELY(PlayerHorse, true, true);

		GEN::FreezePlayer(true);
		GEN::ClearScriptCamera(true);
		GEN::fadeout();

		Camp campdata = CAMP::DefaultCampData[G_CAMP];

		Vector3 pp = campdata.WarpPoint.p;
		float ph = campdata.WarpPoint.h;
		Vector3 hp = campdata.HorsePoint.p;
		float hh = campdata.HorsePoint.h;

		ENTITY::_SET_ENTITY_COORDS_AND_HEADING(PlayerPed, pp.x, pp.y, pp.z, ph + 180, true, true, true);
		ENTITY::_SET_ENTITY_COORDS_AND_HEADING(PlayerHorse, hp.x, hp.y, hp.z, hh, true, true, true);

		ENTITY::FREEZE_ENTITY_POSITION(PlayerPed, true);
		ENTITY::FREEZE_ENTITY_POSITION(PlayerHorse, true);

		while (!IsCampGroundLoaded())
		{
			WAIT(1);
		}

		ENTITY::PLACE_ENTITY_ON_GROUND_PROPERLY(PlayerPed, true);
		ENTITY::PLACE_ENTITY_ON_GROUND_PROPERLY(PlayerHorse, true);

		ENTITY::FREEZE_ENTITY_POSITION(PlayerPed, false);
		ENTITY::FREEZE_ENTITY_POSITION(PlayerHorse, false);

		TASK::CLEAR_PED_TASKS_IMMEDIATELY(PlayerHorse, true, true);

		CAM::SET_GAMEPLAY_CAM_RELATIVE_HEADING(0, 1);
		CAM::SET_GAMEPLAY_CAM_RELATIVE_PITCH(0, 1);

		// fading in and unfreezing is done by mission logic in main()
	}

	void StartMission(MissionType id)
	{
		DioScope(DIOTAG);

		GEN::ClearWantedStatus();
		PED::CLEAR_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, REL_PLAYER, REL_COP);

		if (CampMissionController == NULL)
		{
			DioTrace(DIOTAG1("ERROR") "NO MISSION CONTROLLER");
		}
		else if (CampMissionController->HasActiveMission())
		{
			DioTrace(DIOTAG1("WARNING") "Can't start mission while mission is already active.");
		}
		else
		{
			PrepPlayerforMission();
			DepopulateCamp();
			AllowCampPopulation = false;

			std::vector<MissionCompanionInfo> companions;

			Hash ChosenHorse = CampInventory->GetBestHorse();

			std::vector<std::string> allhorses = InventoryManager::GetStaticHorseData(ChosenHorse).PedModels;

			for (int i = 0; i < allhorses.size(); i++)
			{
				MissionCompanionInfo horse = {allhorses[i], 255, {}, false};
				companions.emplace_back(horse);
			}

			int PedsToSpawn = MissionFactory::GetMissionStaticData(id).NumMembersToSpawn;

			std::vector<GANG::PedInfo> pedslist;

			if (MissionFactory::GetMissionStaticData(id).UseRobberyPeds && GANG::DefaultGangData[G_GANG].HasOverrides[GANG::OVERRIDE_ROBBERY])
			{
				pedslist = GANG::DefaultGangData[G_GANG].Override_Robbery.GenericPeds;
			}
			else if (G_CAMP == DISTRICT_GRIZZLIES_WEST && GANG::DefaultGangData[G_GANG].HasOverrides[GANG::OVERRIDE_SNOW])
			{
				pedslist = GANG::DefaultGangData[G_GANG].Override_Snow.GenericPeds;
			}
			else
			{
				pedslist = GANG::DefaultGangData[G_GANG].Default.GenericPeds;
			}

			if (G_GANG == GANG_DUTCH)
			{
				std::vector<std::string> heisters = DUTCH_HELPERS::GetHeistPeds();

				if (heisters.empty())
				{
					DioTrace(DIOTAG1("WARNING") "Setting default heist peds");
					heisters = {"CS_johnmarston", "CS_billwilliamson", "CS_charlessmith", "CS_javierescuella", "CS_sean", "CS_lenny", "cs_micahbell"};
				}

				pedslist.clear();

				for (const std::string& pedName : heisters)
				{
					DioTrace("Pushing %s", pedName.c_str());
					pedslist.push_back({pedName, 1, false, 0});
				}

				if (pedslist.size() < PedsToSpawn)
				{
					PedsToSpawn = (int)pedslist.size();
					DioTrace("Reducing heist peds to only %d", PedsToSpawn);
				}
			}

			std::vector<Hash> AvailableLongarms = CampInventory->GetAvailableWeapons(true);
			std::vector<Hash> AvailableSidearms = CampInventory->GetAvailableWeapons(false);
			std::vector<Hash> AvailableArmor = CampInventory->GetAvailableArmor();
			
			Hash ChosenLongarm = 0;
			Hash ChosenSidearm = WEAPON_REVOLVER_CATTLEMAN;

			for (int i = 0; i < PedsToSpawn; ++i)
			{
				int chosenPedInfoIndex = rand() % pedslist.size();

				GANG::PedInfo ChosenPedInfo = pedslist[chosenPedInfoIndex];

				if (G_GANG == GANG_DUTCH)
				{
					pedslist.erase(pedslist.begin() + chosenPedInfoIndex);
				}

				if (!AvailableLongarms.empty())
				{
					for (int i = 0; i < AvailableLongarms.size(); i++)
					{
						DIOTRACE_EXTRA("ALL AVAILABLE LONGARMS: %s", Common::HashToStr(AvailableLongarms[i]).c_str());
					}

					ChosenLongarm = AvailableLongarms.back();
					AvailableLongarms.pop_back();

					DIOTRACE_EXTRA("Taking longarm: %s", Common::HashToStr(ChosenLongarm).c_str());
				}
				else
				{
					ChosenLongarm = 0;
				}

				if (!AvailableSidearms.empty() && ChosenLongarm == 0)
				{
					for (int i = 0; i < AvailableSidearms.size(); i++)
					{
						DIOTRACE_EXTRA("ALL AVAILABLE SIDEARMS: %s", Common::HashToStr(AvailableSidearms[i]).c_str());
					}

					ChosenSidearm = AvailableSidearms.back();
					AvailableSidearms.pop_back();

					DIOTRACE_EXTRA("Taking sidearm: %s", Common::HashToStr(ChosenSidearm).c_str());
				}
				else
				{
					ChosenSidearm = WEAPON_REVOLVER_CATTLEMAN;
				}

				int ChosenAccuracy = ChosenLongarm > 0 ? int(InventoryManager::GetStaticWeaponData(ChosenLongarm).accuracy * 0.75) : int(InventoryManager::GetStaticWeaponData(ChosenSidearm).accuracy * 0.9);

				Hash ChosenArmor = ARMOR_LEVEL_0;

				if (!AvailableArmor.empty())
				{
					for (int i = 0; i < AvailableArmor.size(); i++)
					{
						DIOTRACE_EXTRA("ALL AVAILABLE ARMOR: %s", Common::HashToStr(AvailableArmor[i]).c_str());
					}

					ChosenArmor = AvailableArmor.back();
					AvailableArmor.pop_back();

					DIOTRACE_EXTRA("Taking armor: %s", Common::HashToStr(ChosenArmor).c_str());
				}
				else
				{
					ChosenArmor = ARMOR_LEVEL_0;
				}

				MissionCompanionInfo comp = {ChosenPedInfo.model, GANG::ChoosePedSkin(ChosenPedInfo), GANG::DefaultGangData[G_GANG].Voices, true, 
					ChosenAccuracy,
					InventoryManager::GetStaticArmorData(ChosenArmor).health, 
					ChosenSidearm, 
					ChosenLongarm, 
					ChosenArmor
				};
				
				if (G_GANG == GANG_DUTCH && MissionFactory::GetMissionStaticData(id).UseRobberyPeds)
				{
					comp.CompanionVoices = {"Dutch_Robbery"};
				}

				companions.emplace_back(comp);
			}

			if (G_CAMP_UPGR >= 3 && MissionFactory::GetMissionStaticData(id).OverrideCampLaw)
			{
				MissionCompanionInfo dog = {CampDogModel, 0, {}, false};
				companions.emplace_back(dog);
			}

			if (!CampMissionController->StartMission(id, G_CAMP, GenericPedName, companions, MissionFactory::SpawnMission))
			{
				AllowCampPopulation = true;
				GEN::fadein();
				GEN::FreezePlayer(false);
			}

			if (ENTITY::DOES_ENTITY_EXIST(CurrentCampMaxim) && MissionFactory::GetMissionStaticData(id).OverrideCampLaw)
			{
				VEHICLE::SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(CurrentCampMaxim, true);
			}
		}
	}
	
	bool WhitelistedPed(Ped ped)
	{
		if (Common::PedIsHorse(ped) || ENTITY::_GET_IS_BIRD(ped) || ENTITY::IS_ENTITY_IN_WATER(ped))
		{
			return true;
		}

		const Hash relationship = PED::GET_PED_RELATIONSHIP_GROUP_HASH(ped);

		if (relationship == MISC::GET_HASH_KEY("PLAYER") ||
			relationship == MISC::GET_HASH_KEY("REL_PLAYER_ALLY") ||
			relationship == MISC::GET_HASH_KEY("REL_GANG_DUTCHS") ||
			relationship == MISC::GET_HASH_KEY("REL_GANG_DUTCHS_HORSES") ||
			relationship == MISC::GET_HASH_KEY("REL_COMPANION_GROUP") ||
			relationship == MISC::GET_HASH_KEY("REL_COP"))
		{
			return true;
		}
		
		const int length = 13;
		std::string allowedanimals[length] = {
			"A_C_DogAmericanFoxhound_01",
			"A_C_DogAustralianSheperd_0",
			"A_C_DogBluetickCoonhound_01",
			"A_C_DogCatahoulaCur_01",
			"A_C_DogChesBayRetriever_01",
			"A_C_DogCollie_01",
			"A_C_DogHobo_01",
			"A_C_DogHusky_01",
			"A_C_DogLab_01",
			"A_C_DogPoodle_01",
			"A_C_DogRufus_01",
			"A_C_DogStreet_01",
			"A_C_Cat_01"
		};

		for (int i = 0; i < length; i++)
		{
			if (ENTITY::GET_ENTITY_MODEL(ped) == MISC::GET_HASH_KEY(allowedanimals[i].c_str()))
			{
				return true;
			}
		}

		return false;
	}

	void ShooAwayAmbientPeds()
	{
		Ped peds[1024];
		int count = worldGetAllPeds(peds, 1024);

		for (int i = 0; i < count; i++)
		{
			Ped p = peds[i];

			Vector3 ppos = ENTITY::GET_ENTITY_COORDS(p, true, true);
			Vector3 cpos = DefaultCampData[G_CAMP].CampPoint;

			float multiplier = PED::IS_PED_HUMAN(p) ? 2.5f : 4.0f;

			bool PedInRange = (MISC::GET_DISTANCE_BETWEEN_COORDS(ppos.x, ppos.y, ppos.z, cpos.x, cpos.y, cpos.z, false) < DefaultCampData[G_CAMP].CampRadius * multiplier);

			bool incapacitated = PED::IS_PED_INCAPACITATED(p) || PED::IS_PED_DEAD_OR_DYING(p, true) || PED::IS_PED_HOGTIED(p) || PED::IS_PED_LASSOED(p) || PED::IS_PED_BEING_DRAGGED(p);

			if (PedInRange && !WhitelistedPed(p) && TASK::GET_SEQUENCE_PROGRESS(p) < 0 && !incapacitated && MISC::GET_GAME_TIMER() - LastShooTaskAssigned > 500)
			{
#ifdef DEBUG_PROP_PLACEMENT
				ENTITY::SET_ENTITY_AS_MISSION_ENTITY(p, true, true);
				ENTITY::DELETE_ENTITY(&p);
#else
				DIOTRACE_EXTRA("Shooing away rando");

				LastShooTaskAssigned = MISC::GET_GAME_TIMER();

				TASK::CLEAR_PED_TASKS_IMMEDIATELY(p, true, true);
				TASK::TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(p, true);
				EVENT::SET_DECISION_MAKER(p, MISC::GET_HASH_KEY("EMPTY"));

				int sequence;
				TASK::OPEN_SEQUENCE_TASK(&sequence);

				TASK::TASK_PAUSE(0, 1000);
				TASK::TASK_SMART_FLEE_COORD(0, cpos, 1000, -1, 0, 5);
				TASK::TASK_PAUSE(0, 5000);

				TASK::CLOSE_SEQUENCE_TASK(sequence);
				TASK::TASK_PERFORM_SEQUENCE(p, sequence);
				TASK::CLEAR_SEQUENCE_TASK(&sequence);
#endif
			}
		}
	}

	ScenarioPoint FindUnoccupiedIdlePedPosition(Ped ped, bool PedIsGuard)
	{
		int arrsize = PedIsGuard ? (int)DefaultCampData[G_CAMP].upgrades[G_CAMP_UPGR].GuardScenarioPoints.size() : (int)DefaultCampData[G_CAMP].upgrades[G_CAMP_UPGR].IdleScenarioPoints.size();

		Vector3 pedloc = ENTITY::GET_ENTITY_COORDS(ped, true, true);

		bool AllSpotsOccupied = true;
		int TestPoint = 0;
		bool RechoosePoint = true;
		bool FoundFreeSpot = false;

		for (int i = 0; i < arrsize; i++)
		{
			ScenarioPoint TestScenario = PedIsGuard ? CurrentCampGuardScenarioPoints[i] : CurrentCampIdleScenarioPoints[i];
			
			if (!TestScenario.occupied)
			{
				AllSpotsOccupied = false;
			}
		}

		if (AllSpotsOccupied)
		{
			DIOTRACE_EXTRA(DIOTAG1("WARNING") "No potential spots for idle ped, going to camp center instead");
			return {DefaultCampData[G_CAMP].CampPoint, 0};
		}

		int attempts = 0;

		while (RechoosePoint)
		{
			TestPoint = rand() % arrsize;

			if (attempts > 20)
			{
				DIOTRACE_EXTRA(DIOTAG1("WARNING") "Timed out while searching for spots for ped, going to camp center instead");

				if (PedIsGuard)
				{
					for (int i = 0; i < arrsize; i++)
					{
						if (CurrentCampGuardScenarioPoints[i].occupant == ped)
						{
							CurrentCampGuardScenarioPoints[i].occupied = false;
							CurrentCampGuardScenarioPoints[i].occupant = 0;
						}
					}
				}
				else
				{
					for (int i = 0; i < arrsize; i++)
					{
						if (CurrentCampIdleScenarioPoints[i].occupant == ped)
						{
							CurrentCampIdleScenarioPoints[i].occupied = false;
							CurrentCampIdleScenarioPoints[i].occupant = 0;
						}
					}
				}

				return { DefaultCampData[G_CAMP].CampPoint, 0 };
			}

			DIOTRACE_EXTRA("Testing point %d", TestPoint);

			ScenarioPoint TestScenario = PedIsGuard ? CurrentCampGuardScenarioPoints[TestPoint] : CurrentCampIdleScenarioPoints[TestPoint];

			Vector3 scloc = TestScenario.pos.p;

			if (!(PedIsGuard ? CurrentCampGuardScenarioPoints[TestPoint] : CurrentCampIdleScenarioPoints[TestPoint]).occupied)
			{
				FoundFreeSpot = true;
			}
			else
			{
				DIOTRACE_EXTRA("Spot is occupied");
			}

			if (MISC::GET_DISTANCE_BETWEEN_COORDS(pedloc.x, pedloc.y, pedloc.z, scloc.x, scloc.y, scloc.z, true) < 0.5)
			{
				DIOTRACE_EXTRA("Spot is the one we were just at");
				FoundFreeSpot = false;
			}

			RechoosePoint = !FoundFreeSpot;

			attempts++;

			if (attempts > 5)
			{
				WAIT(0);
			}
		}

		if (PedIsGuard)
		{
			CurrentCampGuardScenarioPoints[TestPoint].occupied = true;
			CurrentCampGuardScenarioPoints[TestPoint].occupant = ped;
		}
		else
		{
			CurrentCampIdleScenarioPoints[TestPoint].occupied = true;
			CurrentCampIdleScenarioPoints[TestPoint].occupant = ped;
		}

		DIOTRACE_EXTRA("Chose point %d", TestPoint);

		return PedIsGuard ? CurrentCampGuardScenarioPoints[TestPoint] : CurrentCampIdleScenarioPoints[TestPoint];
	}

	void LookAtMe(Ped convoped)
	{
		TASK::TASK_LOOK_AT_ENTITY(convoped, PlayerPed, 6000, 0, 51, 0);
	}

	PlayerPedConvo ChooseRandomConvo(bool positive)
	{
		std::vector<std::string> GenGreet = {
			"GREET_GENERAL",
			"GREET_GROUP",
			"GREET_GROUP_MALE",
			"GREET_GROUP_MED",
			"GREET_GROUP_SHOUTED",
			"GREET_MALE",
			"GREET_MALE_MED",
			"GREET_SECOND_GROUP_GENERAL_CONV",
			"GREET_SECOND_WILDERNESS_CONV",
			"GREET_TOUGH"
		};

		std::vector<std::string> GenGreetResponse = {
			"CHAT_PEDTYPE_DIALOG",
			"GREET_AGAIN",
			"GREET_GENERAL_FAMILIAR",
			"GREET_MALE",
			"GREET_SHOUTED",
			"HOWS_IT_GOING",
			"PLAYER_HELPED_TOWN",
			"RESPONSE_GENERIC"
		};

		std::vector<std::string> GenAntagonize = {
			"GREET_DRUNK_MALE",
			"GREET_THIRD_INSULT_DRUNK_MALE_CONV",
			"GREET_THIRD_INSULT_GEN_RESPONSE_CONV",
			"GREET_THIRD_INSULT_GROUP_GENERAL_CONV"
		};

		std::vector<std::string> GenAntagonizeResponse = {
			"DEFUSE_RESPONSE",
			"GANG_INTERACT_CHAT_BEEN_OUT_TOO_LONG",
			"GANG_INTERACT_CHAT_BIG_WARNING",
			"GANG_INTERACT_CHAT_WARNING",
			"GANG_INTERACT_CHAT_WARNING_WILDERNESS",
			"GENERIC_INSULT_MED_NEUTRAL",
			"GENERIC_MOCK",
			"INSULT_RESPONSE",
			"PLAYER_HARMED_TOWN",
			"SHAME_ON_YOU",
			"TEASE_RESPONSE",
			"WHATS_YOUR_PROBLEM"
		};
		
		if (G_GANG == GANG_DUTCH)
		{
			GenGreet = {
				"GREET_GENERAL",
				"GREET_GROUP"
			};
			
			GenGreetResponse = {
				"GREET_GENERAL_FAMILIAR",
				"GREET_LONG_TIME",
				"GREET_PLAYER_CASUAL",
				"GREET_PLAYER_CAMPFIRE",
				"GREET_PLAYER_FORMAL"
			};

			GenAntagonizeResponse = {
				"EXPLAIN_YOURSELF",
				"IGNORING_YOU",
				"IN_CAMP_TOO_MUCH",
				"INSULT_RESPONSE",
				"PLAYER_CAUSING_TROUBLE",
				"PLAYER_LOITERING",
				"WHATS_YOUR_PROBLEM"
			};
		}

		if (positive)
		{
			std::vector<PlayerPedConvo> PositiveConvos = {
				{"Generic Hello 1",
					GenGreet,
					GenGreetResponse
				},
				{"Generic Hello 2",
					GenGreet,
					GenGreetResponse
				},
				{"Hows It Going?",
					{"GREET_SECOND_HOWS_IT_GOING_CONV"},
					{"GOING_BADLY","GOING_WELL"}
				}
			};

			if (G_GANG != GANG_DUTCH)
			{
				PositiveConvos.push_back(
				{"Working Hard",
					{"GREET_SECOND_WORKING_HARD_CONV"},
					{"RESPONSE_WORKING_HARD"}
				});
			}

			PlayerPedConvo chosenconvo = PositiveConvos[rand() % PositiveConvos.size()];

			DioTrace("Choosing positive convo: %s", chosenconvo.name.c_str());

			return chosenconvo;
		}

		std::vector<PlayerPedConvo> NegativeConvos = {
			{"Generic Insult 1",
				GenAntagonize,
				GenAntagonizeResponse
			},
			{"Generic Insult 2",
				GenAntagonize,
				GenAntagonizeResponse
			}
		};

		if (G_GANG != GANG_DUTCH)
		{
			NegativeConvos.push_back(
			{"Hardly Working",
				{"GREET_CAUGHT_OUT"},
				{"RESPONSE_IDLING"}
			});
		}

		PlayerPedConvo chosenconvo = NegativeConvos[rand() % NegativeConvos.size()];

		DioTrace("Choosing negative convo: %s", chosenconvo.name.c_str());

		return chosenconvo;
	}

	void ConversationLogic()
	{
		if (CurrentConvoPed != 0 && (!ENTITY::DOES_ENTITY_EXIST(CurrentConvoPed) || !PED::IS_PED_HUMAN(CurrentConvoPed)))
		{
			DioTrace(DIOTAG1("WARNING") "Bogus convo ped");
			ClearConvoFlags();
			return;
		}

		int currentpedslength = (int)CurrentCampPeds.size();

		if (currentpedslength <= 0)
		{
			ClearConvoFlags();
			return;
		}

		Entity target = 0;
		PLAYER::GET_PLAYER_INTERACTION_TARGET_ENTITY(0, &target, false, false);
		
		bool allowchatprompts = ENTITY::DOES_ENTITY_EXIST(target) && CurrentConvoPed == 0 &&
			!PlayerRespondToGuardCallout && GuardRespondToPlayerResponse == 0 &&
			PlayerGreetAmbientPed == 0 && PlayerAntagonizeAmbientPed == 0;

		HUD::_UIPROMPT_SET_ENABLED(GreetAmbientPed, true);
		HUD::_UIPROMPT_SET_ENABLED(AntagonizeAmbientPed, true);
		HUD::_UIPROMPT_SET_ENABLED(GreetAmbientPed, allowchatprompts);
		HUD::_UIPROMPT_SET_ENABLED(AntagonizeAmbientPed, allowchatprompts);

		if (HUD::_UIPROMPT_HAS_STANDARD_MODE_COMPLETED(GreetAmbientPed, 0) || HUD::_UIPROMPT_HAS_STANDARD_MODE_COMPLETED(AntagonizeAmbientPed, 0))
		{
			TimeConvoStarted = MISC::GET_GAME_TIMER();
			
			Ped getped;
			if (!PLAYER::GET_PLAYER_TARGET_ENTITY(0, &getped))
			{
				DioTrace(DIOTAG1("WARNING") "Could not get target");
				PlayerInCampRecently = true;
				TimeLastSeenPlayerInCamp = MISC::GET_GAME_TIMER();
			}
			else
			{
				CurrentConvoPed = getped;
				LookAtMe(CurrentConvoPed);
				PlayerInCampRecently = true;
				TimeLastSeenPlayerInCamp = MISC::GET_GAME_TIMER();
			}
		}

		if (HUD::_UIPROMPT_HAS_STANDARD_MODE_COMPLETED(GreetAmbientPed, 0))
		{
			DIOTRACE_EXTRA("Player Greeting");
			CurrentConvo = ChooseRandomConvo(true);
			PlayerGreetAmbientPed = 1;
			PlayPedChatter(PlayerPed, CurrentConvo.queries[rand() % CurrentConvo.queries.size()]);
		}

		if (HUD::_UIPROMPT_HAS_STANDARD_MODE_COMPLETED(AntagonizeAmbientPed, 0))
		{
			DIOTRACE_EXTRA("Player Antagonizing");
			CurrentConvo = ChooseRandomConvo(false);
			PlayerAntagonizeAmbientPed = 1;
			PlayPedChatter(PlayerPed, CurrentConvo.queries[rand() % CurrentConvo.queries.size()]);
		}

		for (int i = 0; i < currentpedslength; i++)
		{
			Ped ped = CurrentCampPeds[i];
			Vector3 pedloc = ENTITY::GET_ENTITY_COORDS(ped, true, true);

#ifdef DEBUG_MARKERS
			if (ped == CurrentConvoPed)
			{
				Vector3 cloc = {pedloc.x, pedloc.y, pedloc.z + 1.5f};
				Common::DrawDebugCircle(cloc, 0.4, 0.2, "COLOR_YELLOW", true, true);
			}
			if (ped == IdleChatterPed)
			{
				Vector3 cloc = { pedloc.x, pedloc.y, pedloc.z + 2.5f};
				Common::DrawDebugCircle(cloc, 0.1, 0.5, "COLOR_WHITE", true, true);
			}
#endif
			if (PED::IS_PED_HUMAN(ped) && !PedIsFixedPed(ped))
			{
				if (!PlayerInCampRecently && ENTITY::HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(ped, PlayerPed, 17))
				{
					DIOTRACE_EXTRA("Greeting Player");

					CurrentConvoPed = ped;
					TimeConvoStarted = MISC::GET_GAME_TIMER();

					LookAtMe(CurrentConvoPed);

					if (PedIsCampGuard(ped))
					{
						PlayPedChatter(ped, "WHO_GOES_THERE", "SPEECH_PARAMS_BEAT_SHOUTED_CLEAR_NOSUB");
						PlayerRespondToGuardCallout = true;
					}
					else
					{
						PlayPedChatter(ped, "GREET_GENERAL_FAMILIAR");
						CurrentConvoPed = 0;
						TimeConvoStarted = 0;
					}

					PlayerInCampRecently = true;
					TimeLastSeenPlayerInCamp = MISC::GET_GAME_TIMER();
				}
			}
		}
		
		if (PlayerRespondToGuardCallout && MISC::GET_GAME_TIMER() - TimeConvoStarted > 2500)
		{
			DIOTRACE_EXTRA("Player Responding");
			PlayPedChatter(PlayerPed, "DEFUSE_ARGUMENT_TOUGH_SHOUTED", "SPEECH_PARAMS_BEAT_SHOUTED_CLEAR_NOSUB");
			PlayerRespondToGuardCallout = false;
			GuardRespondToPlayerResponse = 1;
			LookAtMe(CurrentConvoPed);
		}

		if (GuardRespondToPlayerResponse == 1 && MISC::GET_GAME_TIMER() - TimeConvoStarted > 4000)
		{
			DIOTRACE_EXTRA("Guard Responding To Player Response");
			GuardRespondToPlayerResponse = 2;
			PlayPedChatter(CurrentConvoPed, "GANG_INTERACT_CHAT_RECOGNIZE_YOU");
			LookAtMe(CurrentConvoPed);
		}
		else if (GuardRespondToPlayerResponse == 2 && MISC::GET_GAME_TIMER() - TimeConvoStarted > 9500)
		{
			DIOTRACE_EXTRA("Guard Player Convo Finished");
			GuardRespondToPlayerResponse = 0;
			CurrentConvoPed = 0;
			TimeConvoStarted = 0;
		}

		if (PlayerGreetAmbientPed == 1 && MISC::GET_GAME_TIMER() - TimeConvoStarted > 2000)
		{
			DIOTRACE_EXTRA("Ped Responding To Player Greet");
			LookAtMe(CurrentConvoPed);
			PlayerGreetAmbientPed = 2;
			PlayPedChatter(CurrentConvoPed, CurrentConvo.responses[rand() % CurrentConvo.responses.size()]);
		}
		else if (PlayerGreetAmbientPed == 2 && MISC::GET_GAME_TIMER() - TimeConvoStarted > 4500)
		{
			DIOTRACE_EXTRA("Player Greet Convo Finished");
			CurrentConvoPed = 0;
			TimeConvoStarted = 0;
			PlayerGreetAmbientPed = 0;
			CurrentConvo = {"NONE"};
		}

		if (PlayerAntagonizeAmbientPed == 1 && MISC::GET_GAME_TIMER() - TimeConvoStarted > 3500)
		{
			DIOTRACE_EXTRA("Ped Responding To Player Antagonize");
			LookAtMe(CurrentConvoPed);
			PlayerAntagonizeAmbientPed = 2;
			PlayPedChatter(CurrentConvoPed, CurrentConvo.responses[rand() % CurrentConvo.responses.size()]);
		}
		else if (PlayerAntagonizeAmbientPed == 2 && MISC::GET_GAME_TIMER() - TimeConvoStarted > 6000)
		{
			DIOTRACE_EXTRA("Player Antagonize Convo Finished");
			CurrentConvoPed = 0;
			TimeConvoStarted = 0;
			PlayerAntagonizeAmbientPed = 0;
			CurrentConvo = {"NONE"};
		}

		if ((IdleChatterPed == 0 && CurrentConvoPed == 0) && MISC::GET_GAME_TIMER() - TimeLastPlayedIdleChatter > ((rand() % 30) + 45) * 1000)
		{
			int chatterboxid = rand() % currentpedslength;
			Ped chatterbox = CurrentCampPeds[chatterboxid];

			if (PED::IS_PED_HUMAN(chatterbox) && chatterbox != CurrentConvoPed && !PedIsFixedPed(chatterbox))
			{
				std::vector<std::string> idlechatter = {
					"CAMPFIRE_IDLE_CHATTER",
					"CHAT_PATROL",
					"CHAT_PEDTYPE_DIALOG",
					"CHAT_ROADBLOCK"
				};
				
				if (GEN::InEpilogue())
				{
					idlechatter.emplace_back("CHAT_1907");
				}

				if (G_GANG == GANG_DUTCH)
				{
					idlechatter = {
						"POKER_BANTER"
					};
				}
				
				std::string chosenchatter = idlechatter[rand() % idlechatter.size()];

				DIOTRACE_EXTRA("Ped %d chattering about %s", chatterboxid, chosenchatter.c_str());
				PlayPedChatter(chatterbox, chosenchatter);
				IdleChatterPed = chatterbox;
				TimeLastPlayedIdleChatter = MISC::GET_GAME_TIMER();
			}
			else
			{
				DIOTRACE_EXTRA("Ped %d not a suitable chatterbox - %s", chatterboxid, !PED::IS_PED_HUMAN(chatterbox) ? "can't talk" : "shouldn't talk");
			}
		}
		else if (IdleChatterPed > 0 && IdleChatterPed == CurrentConvoPed)
		{
			DioTrace("Canceling chatter");
			IdleChatterPed = 0;
		}
		else if (IdleChatterPed > 0 && TimeLastPlayedIdleChatter > 0 && MISC::GET_GAME_TIMER() - TimeLastPlayedIdleChatter > 5000)
		{
			DIOTRACE_EXTRA("Finished chattering");
			IdleChatterPed = 0;
		}
	}

	void AmbientPedLogic()
	{
		const int numrandomguardscenarios = 3;
		const std::string guardscenarios[numrandomguardscenarios] = {
			"WORLD_HUMAN_GUARD_SCOUT",
			"WORLD_HUMAN_GUARD_MILITARY",
			"WORLD_HUMAN_GUARD_LAZY",
		};

		const int numrandomguardscenarios_nolongarm = 3;
		const std::string guardscenarios_nolongarm[numrandomguardscenarios_nolongarm] = {
			"WORLD_HUMAN_WAITING_IMPATIENT",
			"WORLD_HUMAN_STERNGUY_IDLES",
			"WORLD_HUMAN_BADASS"
		};

		const std::string& guardscenario_longarm_snow_override = "WORLD_CAMP_GUARD_COLD";

		const int numidlescenarios = 8;
		const std::string idlescenarios[numidlescenarios] = {
			"WORLD_HUMAN_SMOKE_CIGAR",
			"WORLD_HUMAN_SIT_SMOKE",
			"WORLD_HUMAN_SIT_GUITAR_DOWNBEAT",
			"WORLD_HUMAN_SIT_GROUND_WHITTLE",
			"WORLD_HUMAN_SIT_GROUND_DRINK_DRUNK",
			"WORLD_HUMAN_SIT_GROUND_COFFEE_DRINK",
			"WORLD_HUMAN_SIT_FALL_ASLEEP",
			"WORLD_HUMAN_DRINKING_DRUNK"
		};

		const int numcampfirescenarios = 3;
		const std::string campfirescenarios[numcampfirescenarios] = {
			"WORLD_HUMAN_FIRE_STAND",
			"WORLD_PLAYER_CAMP_FIRE_SIT",
			"WORLD_PLAYER_CAMP_FIRE_SQUAT"
		};

		const int numdogscenarios = 6;
		const std::string dogscenarios[numdogscenarios] = {
			"WORLD_ANIMAL_DOG_BEGGING",
			"WORLD_ANIMAL_DOG_DIGGING",
			"WORLD_ANIMAL_DOG_RESTING",
			"WORLD_ANIMAL_DOG_ROLL_GROUND",
			"WORLD_ANIMAL_DOG_SITTING",
			"WORLD_ANIMAL_DOG_SNIFFING_GROUND"
		};

		int LongarmCount = (int)AvailableLongArms.size();
		int AssignedLongarm = 0;

		for (Ped ped : CurrentCampPeds)
		{
			Vector3 pedloc = ENTITY::GET_ENTITY_COORDS(ped, true, true);

			int LastTaskAssignedTime = 0;

			if (PED::IS_PED_HUMAN(ped) && !PedIsFixedPed(ped) && !PED::IS_PED_USING_ANY_SCENARIO(ped) && TASK::GET_SEQUENCE_PROGRESS(ped) < 0 && MISC::GET_GAME_TIMER() - LastTaskAssignedTime > 500)
			{
				bool guard = PedIsCampGuard(ped);

				LastTaskAssignedTime = MISC::GET_GAME_TIMER();

				int tasktime = 5;
				float speed = 1.0f;

				tasktime = (rand() % 60) + 240;
				DIOTRACE_EXTRA("Assigning task for %d seconds", tasktime);

				tasktime *= 1000;

				int sequence;
				std::string scenario;

				ScenarioPoint loc = FindUnoccupiedIdlePedPosition(ped, guard);

				if (guard)
				{
					bool HasLongarm = LongarmCount > 0 && AssignedLongarm < LongarmCount;

					if (HasLongarm)
					{
						WEAPON::GIVE_WEAPON_TO_PED(ped, AvailableLongArms[AssignedLongarm], 12, true, false, WEAPON_ATTACH_POINT_HAND_PRIMARY, false, 0.5f, 1.0f, ADD_REASON_DEFAULT, true, 0, false);

						AssignedLongarm++;
					}

					if (HasLongarm)
					{
						scenario = guardscenarios[rand() % numrandomguardscenarios];

						if (G_CAMP == DISTRICT_GRIZZLIES_WEST)
						{
							scenario = guardscenario_longarm_snow_override;
						}
					}
					else
					{
						scenario = guardscenarios_nolongarm[rand() % numrandomguardscenarios_nolongarm];
					}
				}
				else
				{
					int s = rand() % DefaultCampData[G_CAMP].upgrades[G_CAMP_UPGR].IdleScenarioPoints.size();
					bool campfirepoint = s < DefaultCampData[G_CAMP].upgrades[G_CAMP_UPGR].NumCampFireScenarios;

					DIOTRACE_EXTRA("Point %d at campfire? %s", s, campfirepoint ? "YES" : "NO");

					scenario = campfirepoint ? campfirescenarios[rand() % numcampfirescenarios] : idlescenarios[rand() % numidlescenarios];
				}

				LoadScenario(scenario);

				TASK::OPEN_SEQUENCE_TASK(&sequence);

				/*0*/ TASK::TASK_FOLLOW_NAV_MESH_TO_COORD(0, loc.pos.p.x, loc.pos.p.y, loc.pos.p.z, speed, -1, 0.5f, 4194304, 40000.0f);
				/*1*/ TASK::TASK_ACHIEVE_HEADING(0, loc.pos.h, 3500);
				/*2*/ TASK::TASK_START_SCENARIO_IN_PLACE_HASH(0, MISC::GET_HASH_KEY(scenario.c_str()), tasktime, true, 0, -1.0f, false);
				/*3*/ TASK::TASK_PAUSE(0, 1000);
				/*4*/ TASK::TASK_STAND_STILL(0, 1000);

				TASK::CLOSE_SEQUENCE_TASK(sequence);
				TASK::TASK_PERFORM_SEQUENCE(ped, sequence);
				TASK::CLEAR_SEQUENCE_TASK(&sequence);
			}
			else if (PED::IS_PED_HUMAN(ped) && !PedIsFixedPed(ped) && TASK::GET_SEQUENCE_PROGRESS(ped) >= 3)
			{
				TASK::CLEAR_PED_TASKS(ped, true, true);

				if (PedIsCampGuard(ped))
				{
					DIOTRACE_EXTRA("Guard ped is finished with point");

					for (int i = 0; i < CurrentCampGuardScenarioPoints.size(); i++)
					{
						if (CurrentCampGuardScenarioPoints[i].occupant == ped)
						{
							DIOTRACE_EXTRA("Resetting guard point %d", i);
							CurrentCampGuardScenarioPoints[i].occupied = false;
							CurrentCampGuardScenarioPoints[i].occupant = 0;
						}
					}
				}
				else
				{
					DIOTRACE_EXTRA("Idle ped is finished with point");

					for (int i = 0; i < CurrentCampIdleScenarioPoints.size(); i++)
					{
						if (CurrentCampIdleScenarioPoints[i].occupant == ped)
						{
							DIOTRACE_EXTRA("Resetting idle point %d", i);
							CurrentCampIdleScenarioPoints[i].occupied = false;
							CurrentCampIdleScenarioPoints[i].occupant = 0;
						}
					}
				}
			}
			else if (PED::IS_PED_MODEL(ped, MISC::GET_HASH_KEY(CampDogModel.c_str())) && (TASK::GET_SEQUENCE_PROGRESS(ped) < 0 || TASK::GET_SEQUENCE_PROGRESS(ped) >= 4))
			{
				std::string scenario = dogscenarios[rand() % numdogscenarios];

				DIOTRACE_EXTRA("Assigning doggo sequence - scenario %s", scenario.c_str());

				LoadScenario(scenario);

				Ped target = PlayerPed;

				if (!CurrentCampPeds.empty())
				{
					target = CurrentCampPeds[rand() % CurrentCampPeds.size()];
				}

				int sequence;
				TASK::OPEN_SEQUENCE_TASK(&sequence);

				TASK::TASK_GO_TO_ENTITY(0, target, -1, 1.4f, 1.0f, 0, 0);
				TASK::TASK_TURN_PED_TO_FACE_ENTITY(0, target, 5000, 0, 0, 0);
				TASK::TASK_START_SCENARIO_IN_PLACE_HASH(0, MISC::GET_HASH_KEY(scenario.c_str()), 12000, true, 0, 0, false);
				TASK::TASK_STAND_STILL(0, 2500);
				TASK::TASK_PAUSE(0, 5000);
				TASK::TASK_PAUSE(0, 5000);

				TASK::CLOSE_SEQUENCE_TASK(sequence);
				TASK::TASK_PERFORM_SEQUENCE(ped, sequence);
				TASK::CLEAR_SEQUENCE_TASK(&sequence);
			}
		}
	}

	void MenuLogic()
	{
		if (CampMissionController != NULL)
		{
			bool enabled = !CampMissionController->HasActiveMission();
			const char nastr[] = "Cannot Interact During Mission";

			HUD::_UIPROMPT_SET_ENABLED(FixedPedCampInteraction, enabled);
			HUD::_UIPROMPT_SET_ENABLED(FixedPedMissionInteraction, enabled);
			HUD::_UIPROMPT_SET_ENABLED(FixedPedHorseInteraction, enabled);
			HUD::_UIPROMPT_SET_ENABLED(FixedPedArmoryInteraction, enabled);

			HUD::_UIPROMPT_SET_TEXT(FixedPedCampInteraction, MISC::VAR_STRING(10, "LITERAL_STRING", enabled ? ManagerPedAction : nastr));
			HUD::_UIPROMPT_SET_TEXT(FixedPedArmoryInteraction, MISC::VAR_STRING(10, "LITERAL_STRING", enabled ? ArmoryPedAction : nastr));
			HUD::_UIPROMPT_SET_TEXT(FixedPedHorseInteraction, MISC::VAR_STRING(10, "LITERAL_STRING", enabled ? HorsePedAction : nastr));
			HUD::_UIPROMPT_SET_TEXT(FixedPedMissionInteraction, MISC::VAR_STRING(10, "LITERAL_STRING", enabled ? MissionPedAction : nastr));
		}
		
		if (HUD::_UIPROMPT_HAS_STANDARD_MODE_COMPLETED(FixedPedCampInteraction, 0))
		{
			open_camp = true;

			Vector3 camPos = DefaultCampData[G_CAMP].upgrades[G_CAMP_UPGR].fixedpeds[FPED_MANAGER].CamPos;
			Vector3 camRot = DefaultCampData[G_CAMP].upgrades[G_CAMP_UPGR].fixedpeds[FPED_MANAGER].CamRot;

			if ((int)camPos.x == 0 || (int)camRot.x == 0)
			{
				DioTrace(DIOTAG1("WARNING") "Camp %d Upgrade %d has no camera for FPED_MANAGER", G_CAMP, G_CAMP_UPGR);
			}
			else
			{
				GEN::SetScriptCamera(camPos, camRot);
			}
		}

		if (HUD::_UIPROMPT_HAS_STANDARD_MODE_COMPLETED(FixedPedArmoryInteraction, 0))
		{
			open_armory = true;

			Vector3 camPos = DefaultCampData[G_CAMP].upgrades[G_CAMP_UPGR].fixedpeds[FPED_ARMORY].CamPos;
			Vector3 camRot = DefaultCampData[G_CAMP].upgrades[G_CAMP_UPGR].fixedpeds[FPED_ARMORY].CamRot;

			if ((int)camPos.x == 0 || (int)camRot.x == 0)
			{
				DioTrace(DIOTAG1("WARNING") "Camp %d Upgrade %d has no camera for FPED_ARMORY", G_CAMP, G_CAMP_UPGR);
			}
			else
			{
				GEN::SetScriptCamera(camPos, camRot);
			}
		}

		if (HUD::_UIPROMPT_HAS_STANDARD_MODE_COMPLETED(FixedPedHorseInteraction, 0))
		{
			open_horse = true;

			Vector3 camPos = DefaultCampData[G_CAMP].upgrades[G_CAMP_UPGR].fixedpeds[FPED_HORSE].CamPos;
			Vector3 camRot = DefaultCampData[G_CAMP].upgrades[G_CAMP_UPGR].fixedpeds[FPED_HORSE].CamRot;

			if ((int)camPos.x == 0 || (int)camRot.x == 0)
			{
				DioTrace(DIOTAG1("WARNING") "Camp %d Upgrade %d has no camera for FPED_HORSE", G_CAMP, G_CAMP_UPGR);
			}
			else
			{
				GEN::SetScriptCamera(camPos, camRot);
			}
		}

		if (HUD::_UIPROMPT_HAS_STANDARD_MODE_COMPLETED(FixedPedMissionInteraction, 0))
		{
			open_mission = true;

			Vector3 camPos = DefaultCampData[G_CAMP].upgrades[G_CAMP_UPGR].fixedpeds[FPED_MISSION].CamPos;
			Vector3 camRot = DefaultCampData[G_CAMP].upgrades[G_CAMP_UPGR].fixedpeds[FPED_MISSION].CamRot;

			if ((int)camPos.x == 0 || (int)camRot.x == 0)
			{
				DioTrace(DIOTAG1("WARNING") "Camp %d Upgrade %d has no camera for FPED_MISSION", G_CAMP, G_CAMP_UPGR);
			}
			else
			{
				GEN::SetScriptCamera(camPos, camRot);
			}
		}
	}

	void init()
	{
		SpawnAndPopulateCamp();
	}

	void update()
	{
		if (!CampMissionController->ActiveMissionHasLawOverride())
		{
			ShooAwayAmbientPeds();

#ifdef DEBUG_MARKERS
			const float TallOverride = 1.0f;

			// Main Camp Radius
			Common::DrawDebugCircle(DefaultCampData[G_CAMP].CampPoint, DefaultCampData[G_CAMP].CampRadius, 0.9, "COLOR_GREEN", false);
			Common::DrawDebugCircle(DefaultCampData[G_CAMP].CampPoint, 0.05, 2.5f * TallOverride, "COLOR_GREEN", false);

			// Ambient Animal Flee Radius
			Common::DrawDebugCircle(DefaultCampData[G_CAMP].CampPoint, DefaultCampData[G_CAMP].CampRadius * 2.5f, 2.5f * TallOverride, "COLOR_WHITE", false);

			// Law Zones
			Common::DrawDebugCircle(DefaultCampData[G_CAMP].CampPoint, DefaultCampData[G_CAMP].CampRadius * 4, 10 * TallOverride, "COLOR_ORANGE", false);

			// Player & Horse Spawn Points
			Vector3 adjloc = DefaultCampData[G_CAMP].WarpPoint.p;
			Common::DrawDebugRectangle(adjloc, {0, 0, DefaultCampData[G_CAMP].WarpPoint.h}, {0.5, 0.5, 0.5}, "COLOR_BLUE", false);
			adjloc = DefaultCampData[G_CAMP].HorsePoint.p;
			Common::DrawDebugCircle(adjloc, 1.5, 0.5, "COLOR_BLUE", false);

			// Random Camp Spawn Points
			for (int i = 0; i < DefaultCampData[G_CAMP].upgrades[G_CAMP_UPGR].IdlePedSpawnPoints.size(); i++)
			{
				Vector3 adjloc = DefaultCampData[G_CAMP].upgrades[G_CAMP_UPGR].IdlePedSpawnPoints[i];
				Common::DrawDebugCircle(adjloc, 1.25, 0.2, "COLOR_BLUE", false);
			}

			// Navigation Blockers
			for (int i = 0; i < CurrentCamp.activeblockers.size(); i++)
			{
				Volume vol = CurrentCamp.activeblockers[i];
				Vector3 min, max;
				VOLUME::_GET_VOLUME_BOUNDS(vol, &min, &max);
				Vector3 scale = VOLUME::_GET_VOLUME_SCALE(vol);
				Vector3 pos = VOLUME::_GET_VOLUME_COORDS(vol);
				float z = min.z;
				Common::DrawDebugRectangle({ pos.x, pos.y, z }, VOLUME::_GET_VOLUME_ROTATION(vol), { scale.x, scale.y, 2.0 }, "COLOR_RED", false);
			}

			// Guard Scenarios
			for (int i = 0; i < CurrentCampGuardScenarioPoints.size(); i++)
			{
				adjloc = CurrentCampGuardScenarioPoints[i].pos.p;
				Common::DrawDebugRectangle(adjloc, { 0, 0, CurrentCampGuardScenarioPoints[i].pos.h }, { 0.3, 0.3, 0.4 }, CurrentCampGuardScenarioPoints[i].occupied ? "COLOR_BLACK" : "COLOR_WHITE", false);
			}

			// Ambient Scenarios
			for (int i = 0; i < CurrentCampIdleScenarioPoints.size(); i++)
			{
				adjloc = CurrentCampIdleScenarioPoints[i].pos.p;
				Common::DrawDebugRectangle(adjloc, { 0, 0, CurrentCampIdleScenarioPoints[i].pos.h }, { 0.3, 0.3, 0.4 }, CurrentCampIdleScenarioPoints[i].occupied ? "COLOR_BLACK" : "COLOR_WHITE", false);
			}
#endif
		}

		Vector3 ppos = ENTITY::GET_ENTITY_COORDS(PlayerPed, true, true);
		Vector3 cpos = DefaultCampData[G_CAMP].CampPoint;
		bool PlayerInCampNoMusicZone = MISC::GET_DISTANCE_BETWEEN_COORDS(ppos.x, ppos.y, ppos.z, cpos.x, cpos.y, cpos.z, false) < DefaultCampData[G_CAMP].CampRadius * 4;
		bool PlayerBroughtLawToCamp = LAW::IS_LAW_INCIDENT_ACTIVE(0) && MISC::GET_DISTANCE_BETWEEN_COORDS(ppos.x, ppos.y, ppos.z, cpos.x, cpos.y, cpos.z, false) < DefaultCampData[G_CAMP].CampRadius * 4;
		
		if (CampMissionController->ActiveMissionHasLawOverride())
		{
			PlayerBroughtLawToCamp = false;
		}

		if (PedInCampRadius() && !CampMissionController->HasActiveMission())
		{
			DeleteMainCampBlip();
			CreateCampBlips();
		}
		else if (CampMissionController->ActiveMissionHasLawOverride())
		{
			DeleteMainCampBlip();
			DeleteCampBlips();
		}
		else
		{
			DeleteCampBlips();
			CreateMainCampBlip();
		}

		if (PlayerInCampNoMusicZone && !CampMissionController->HasActiveMission())
		{
			AUDIO::SET_AUDIO_FLAG("EnableIdleMusic", false);
			AUDIO::TRIGGER_MUSIC_EVENT("STOP_MUSIC_8S");
		}
		else
		{
			AUDIO::SET_AUDIO_FLAG("EnableIdleMusic", true);
		}

		if (PedInCampRadius())
		{
			PlayerInCampRecently = true;
			TimeLastSeenPlayerInCamp = MISC::GET_GAME_TIMER();
		}
		else if(MISC::GET_GAME_TIMER() - TimeLastSeenPlayerInCamp > MinTimeOutsideCamp)
		{
			PlayerInCampRecently = false;
		}

		if (PlayerBroughtLawToCamp && !ActivateLawLoopNextTick)
		{
			ActivateLawLoopNextTick = true;
			close_menu_gen = true;
		}
		else if (PlayerBroughtLawToCamp && ActivateLawLoopNextTick)
		{
			ActivateLawLoopNextTick = false;
			LawArrivedInCamp();
		}
		else if (campPopulated)
		{
			AmbientPedLogic();
			ConversationLogic();
			MenuLogic();
		}
	}
}

namespace M_CAMP
{
	std::string GetCampStatusStr()
	{
		if (G_PENDING_RAID)
		{
			return "ATTACK IMMINENT";
		}

		if (G_BOUNTY > 100000)
		{
			return "WANTED: Federal Agents";
		}

		if (G_BOUNTY > 20000)
		{
			return "WANTED: Local Law";
		}

		if (G_BOUNTY > 5000)
		{
			return "Discovered By Rival Gang";
		}

		return "Well Hidden";
	}
	
	int CalcBountyPayment()
	{
		if (G_FUNDS < 100)
		{
			return 0;
		}
		
		if (G_FUNDS < G_BOUNTY)
		{
			return G_FUNDS - (G_FUNDS % 100);
		}

		return G_BOUNTY;
	}

	class MenuItemPayBounty : public MenuItemDefault
	{
		int m_amount;

		virtual void OnSelect()
		{
			DioTrace("Paying %d of bounty", m_amount);

			G_BOUNTY -= m_amount;
			G_FUNDS -= m_amount;

			if (G_BOUNTY < 100)
			{
				G_PENDING_RAID = false;
			}

			IO::WriteDAT();

			ReopenMenuNextTick = M_MENU_CAMP;
		}

	public: MenuItemPayBounty(std::string caption, int amount) : MenuItemDefault(caption), m_amount(amount) {}
	};

	class MenuItemUpgradeCamp : public MenuItemDefault
	{
		int m_amount;

		virtual void OnSelect()
		{
			if (G_FUNDS >= m_amount)
			{
				DioTrace("Upgrading camp from level %d to %d", G_CAMP_UPGR, G_CAMP_UPGR + 1);

				G_CAMP_UPGR++;
				G_FUNDS -= m_amount;

				IO::WriteDAT();

				close_menu_gen = true;

				GEN::ClearScriptCamera(true);

				GEN::fadeout();

				CAMP::DepopulateCamp();
				CAMP::DeleteCamp();

				WAIT(100);

				campSpawned = false;
				campgroundLoaded = true;
				AllowCampPopulation = true;

				CAMP::TeleportToCamp();

				GEN::fadein();
			}
			else
			{
				M_GEN::PlayPurchaseNoise(false);
				display("~COLOR_YELLOW~Cannot afford ~COLOR_WHITE~to upgrade.");
			}
		}

	public: MenuItemUpgradeCamp(std::string caption, int amount) : MenuItemDefault(caption), m_amount(amount) {}
	};

	class MenuItemDisbandGang : public MenuItemDefault
	{
		MenuController* menuController;
		
		virtual void OnSelect()
		{
			M_GEN::ClearAllOpenMenus(menuController); 
			
			GEN::fadeout();

			CAMP::TeleportToCamp();

			GEN::FreezePlayer(true);

			CAMP::DepopulateCamp();
			CAMP::DeleteCamp();
			hasCamp = false;
			DAT = {GANG_NONE, DISTRICT_INVALID, -1, -1, -1, -1};
			CampInventory->SetDefaultInventory();

			IO::WriteDAT();
			IO::ReadDAT();

			GEN::fadein();

			GEN::FreezePlayer(false);
		}

	public: MenuItemDisbandGang(MenuController* controller) : MenuItemDefault("DISBAND GANG"), menuController(controller) {}
	};

	MenuBase* CreateGangDisbandMenu(MenuController* controller)
	{
		auto menu = new MenuBase(new MenuItemTitle("ARE YOU SURE?"));
		controller->RegisterMenu(menu);

		menu->AddItem(new M_GEN::MenuItemBack(controller));

		menu->AddItem(new MenuItemDefault("Your camp will be dismantled and gang members dismissed."));
		menu->AddItem(new MenuItemDefault("All gang funds and assets will be forfeited."));

		menu->AddItem(new MenuItemDisbandGang(controller));

		return menu;
	}
	
	MenuBase* CreateCampMenu(MenuController* controller)
	{
		auto menu = new MenuBase(new MenuItemTitle("CAMP MANAGEMENT"));
		controller->RegisterMenu(menu);

		int maxmembers = 0;
		int maxfunds = 0;

		switch (G_CAMP_UPGR)
		{
			case 0: maxmembers = UPGR_0_MEMBERS; maxfunds = UPGR_0_FUNDS; break;
			case 1: maxmembers = UPGR_1_MEMBERS; maxfunds = UPGR_1_FUNDS; break;
			case 2: maxmembers = UPGR_2_MEMBERS; maxfunds = UPGR_2_FUNDS; break;
			case 3: maxmembers = UPGR_3_MEMBERS; maxfunds = UPGR_3_FUNDS; break;
		}

		menu->AddItem(new MenuItemDefault("STATS:"));
		menu->AddItem(new MenuItemDefault(Common::Format("Gang Members - %d / %d", G_MEMBERCOUNT, maxmembers)));
		menu->AddItem(new MenuItemDefault(Common::Format("Gang Funds - %s / %s", M_GEN::CalcMoneyStr(G_FUNDS, true), M_GEN::CalcMoneyStr(maxfunds, true))));
		menu->AddItem(new MenuItemDefault(Common::Format("Camp Upgrade Level - %d / 3", G_CAMP_UPGR)));
		menu->AddItem(new MenuItemDefault(Common::Format("Gang Bounty - %s", M_GEN::CalcMoneyStr(G_BOUNTY, false))));
		menu->AddItem(new MenuItemDefault(Common::Format("Camp Status - %s", GetCampStatusStr().c_str())));

		menu->AddItem(new MenuItemDefault("OPTIONS:"));

		int payment = CalcBountyPayment();

		if (G_BOUNTY < 100)
		{
			menu->AddItem(new MenuItemDefault("[No Bounty To Pay]"));
		}
		else if (payment == 0)
		{
			menu->AddItem(new MenuItemDefault("[Cannot Pay Bounty]"));
		}
		else
		{
			menu->AddItem(new MenuItemPayBounty(Common::Format("Pay %s Off Bounty", M_GEN::CalcMoneyStr(payment, false)), payment));
		}

		if (G_CAMP_UPGR == 0)
		{
			menu->AddItem(new MenuItemUpgradeCamp("Upgrade Camp To Level 1 - $80", 8000));
		}
		else if (G_CAMP_UPGR == 1)
		{
			menu->AddItem(new MenuItemUpgradeCamp("Upgrade Camp To Level 2 - $350", 35000));
		}
		else if (G_CAMP_UPGR == 2)
		{
			menu->AddItem(new MenuItemUpgradeCamp("Upgrade Camp To Level 3 - $900", 90000));
		}

		menu->AddItem(new MenuItemMenu("Relocate Camp", M_INIT::CreateLocationMenu(controller, G_GANG, true)));
		menu->AddItem(new MenuItemMenu("Disband Gang", CreateGangDisbandMenu(controller)));

		menu->AddItem(new M_GEN::MenuItemBack(controller));

		return menu;
	}
}

namespace M_ARMORY
{
	class MenuItemBuySellWeapon : public MenuItemDefault
	{
		int m_value;
		int m_price;
		bool m_sell;

		virtual void OnSelect()
		{
			const std::string debugname = Common::HashToStr(m_value);

			if (m_sell)
			{
				if (CampInventory->HasItem(m_value))
				{
					DioTrace("Selling weapon %s", debugname.c_str());
					M_GEN::PlayPurchaseNoise();
					G_FUNDS += m_price;
					CampInventory->RemoveItem(m_value);
					IO::WriteDAT();
					ReopenMenuNextTick = M_MENU_WEAPON_SELL;
				}
				else
				{
					DioTrace("Can't sell item %s if we don't have any", debugname.c_str());
					M_GEN::PlayPurchaseNoise(false);
					display("Your gang ~COLOR_YELLOW~does not own ~COLOR_WHITE~any of those.");
				}
			}
			else
			{
				if (G_FUNDS < m_price)
				{
					DioTrace("We can't buy item %s (%d) with only %s", debugname.c_str(), m_price, M_GEN::CalcMoneyStr(G_FUNDS, true).c_str());
					M_GEN::PlayPurchaseNoise(false);
					display("~COLOR_YELLOW~Cannot afford ~COLOR_WHITE~to purchase.");
				}
				else
				{
					DioTrace("Buying item %s", debugname.c_str());
					M_GEN::PlayPurchaseNoise();
					G_FUNDS -= m_price;
					CampInventory->AddItem(m_value);
					IO::WriteDAT();
					ReopenMenuNextTick = M_MENU_WEAPON_BUY;

					if (m_value == ARMOR_LEVEL_1 || m_value == ARMOR_LEVEL_2 || m_value == ARMOR_LEVEL_3)
					{
						ReopenMenuNextTick = M_MENU_ARMOR;
					}
				}
			}

			CAMP::AvailableLongArms = CampInventory->GetAvailableWeapons(true);
		}
		public: MenuItemBuySellWeapon(std::string caption, int value, int price, bool sell) : MenuItemDefault(caption), m_value(value), m_price(price), m_sell (sell) {}
	};
	
	MenuBase* CreateWeaponBuyMenu(MenuController* controller)
	{
		MenuBase* menu = new MenuBase(new MenuItemTitle("PURCHASE WEAPONS"));
		controller->RegisterMenu(menu);

		menu->AddItem(new MenuItemDefault(Common::Format("Gang Funds - %s", M_GEN::CalcMoneyStr(G_FUNDS, true))));
		//menu->AddItem(new MenuItemDefault("DMG = Damage, ACC = Accuracy"));

		//menu->AddItem(new MenuItemDefault("SIDEARMS"));
		for (int i = 0; i < SidearmTierCount; i++)
		{
			StaticWeaponData WeaponData = InventoryManager::GetStaticWeaponData(SidearmTierList[i]);

			menu->AddItem(new MenuItemBuySellWeapon(
				Common::Format("%s%s%s%d%s%d%s%s%s%d",
					WeaponData.name.c_str(),
					WeaponData.spacing.c_str(),
					"DMG: ",
					WeaponData.damage,
					"  ACC: ",
					WeaponData.accuracy,
					"  BUY: ",
					M_GEN::CalcMoneyStr(WeaponData.buy, false).c_str(),
					"  OWNED: ",
					CampInventory->GetItemCount(WeaponData.hash)
				),
				WeaponData.hash, WeaponData.buy, false)
			);
		}

		//menu->AddItem(new MenuItemDefault("LONGARMS"));
		for (int i = 0; i < LongarmTierCount; i++)
		{
			StaticWeaponData WeaponData = InventoryManager::GetStaticWeaponData(LongarmTierList[i]);

			/*if (i == 3)
			{
				menu->AddItem(new MenuItemDefault("V See More Below V"));
				menu->AddItem(new MenuItemDefault(Common::Format("Gang Funds - %s", M_GEN::CalcMoneyStr(G_FUNDS, true))));
			}*/

			menu->AddItem(new MenuItemBuySellWeapon(
				Common::Format("%s%s%s%d%s%d%s%s%s%d",
					WeaponData.name.c_str(),
					WeaponData.spacing.c_str(),
					"DMG: ",
					WeaponData.damage,
					"  ACC: ",
					WeaponData.accuracy,
					"  BUY: ",
					M_GEN::CalcMoneyStr(WeaponData.buy, false).c_str(),
					"  OWNED: ",
					CampInventory->GetItemCount(WeaponData.hash)
				),
				WeaponData.hash, WeaponData.buy, false)
			);
		}

		menu->AddItem(new M_GEN::MenuItemBack(controller));

		return menu;
	}

	MenuBase* CreateWeaponSellMenu(MenuController* controller)
	{
		MenuBase* menu = new MenuBase(new MenuItemTitle("VIEW & SELL WEAPONS"));
		controller->RegisterMenu(menu);

		//menu->AddItem(new MenuItemDefault("DMG = Damage, ACC = Accuracy"));
		menu->AddItem(new MenuItemDefault("If a gang member dies, their carried weapons are lost."));
		menu->AddItem(new MenuItemDefault("Loot their body before the mission ends to recover them."));

		menu->AddItem(new MenuItemDefault("Cattleman Revolver       DMG: 20  ACC: 50  [DEFAULT]"));

		const std::vector<StaticWeaponData> AllWeaponDatas;

		int TotalAdded = 0;

		for (int i = 0; i < SidearmTierCount; i++)
		{
			int AmountOwned = CampInventory->GetItemCount(SidearmTierList[i]);
			
			if (AmountOwned > 0)
			{
				StaticWeaponData WeaponData = InventoryManager::GetStaticWeaponData(SidearmTierList[i]);
				
				menu->AddItem(new MenuItemBuySellWeapon(
					Common::Format("%s%s%s%d%s%d%s%s%s%d",
						WeaponData.name.c_str(),
						WeaponData.spacing.c_str(),
						"DMG: ",
						WeaponData.damage,
						"  ACC: ",
						WeaponData.accuracy,
						"  SELL: ",
						M_GEN::CalcMoneyStr(WeaponData.sell, false).c_str(),
						"  OWNED: ",
						AmountOwned
					),
					WeaponData.hash, WeaponData.sell, true)
				);

				TotalAdded++;
			}
		}

		for (int i = 0; i < LongarmTierCount; i++)
		{
			int AmountOwned = CampInventory->GetItemCount(LongarmTierList[i]);

			if (AmountOwned > 0)
			{
				StaticWeaponData WeaponData = InventoryManager::GetStaticWeaponData(LongarmTierList[i]);

				/*if (TotalAdded == 6)
				{
					menu->AddItem(new MenuItemDefault("V See More Below V"));
				}*/

				menu->AddItem(new MenuItemBuySellWeapon(
					Common::Format("%s%s%s%d%s%d%s%s%s%d",
						WeaponData.name.c_str(),
						WeaponData.spacing.c_str(),
						"DMG: ",
						WeaponData.damage,
						"  ACC: ",
						WeaponData.accuracy,
						"  SELL: ",
						M_GEN::CalcMoneyStr(WeaponData.sell, false).c_str(),
						"  OWNED: ",
						AmountOwned
					),
					WeaponData.hash, WeaponData.sell, true)
				);

				TotalAdded++;
			}
		}

		menu->AddItem(new M_GEN::MenuItemBack(controller));

		return menu;
	}

	MenuBase* CreateArmorMenu(MenuController* controller)
	{
		MenuBase* menu = new MenuBase(new MenuItemTitle("PURCHASE ARMOR"));
		controller->RegisterMenu(menu);

		menu->AddItem(new MenuItemDefault(Common::Format("Gang Funds - %s", M_GEN::CalcMoneyStr(G_FUNDS, true))));
		menu->AddItem(new MenuItemDefault("Armor cannot be sold and is lost when the gang member dies."));
		
		menu->AddItem(new MenuItemDefault(Common::Format("No Armor        HP: %d  [DEFAULT]", InventoryManager::GetStaticArmorData(ARMOR_LEVEL_0).health)));

		for (int i = 0; i < ArmorTierCount; i++)
		{
			StaticArmorData ArmorData = InventoryManager::GetStaticArmorData(ArmorTierList[i]);
			
			menu->AddItem(new MenuItemBuySellWeapon(
				Common::Format("Level %d Armor - HP: %d  BUY: %s  OWNED: %d",
					i+1,
					ArmorData.health,
					M_GEN::CalcMoneyStr(ArmorData.buy, false).c_str(),
					CampInventory->GetItemCount(ArmorTierList[i])
				), 
				ArmorTierList[i], ArmorData.buy, false));
		}

		menu->AddItem(new M_GEN::MenuItemBack(controller));

		return menu;
	}
	
	MenuBase* CreateArmoryMenu(MenuController* controller)
	{
		auto menu = new MenuBase(new MenuItemTitle("ARMORY"));
		controller->RegisterMenu(menu);

		menu->AddItem(new MenuItemMenu("Manage Current Weapons", CreateWeaponSellMenu(controller)));
		menu->AddItem(new MenuItemMenu("Purchase Weapons", CreateWeaponBuyMenu(controller)));
		menu->AddItem(new MenuItemMenu("Purchase Armor", CreateArmorMenu(controller)));

		menu->AddItem(new M_GEN::MenuItemBack(controller));

		return menu;
	}
}

namespace M_HORSE
{
	class MenuItemBuySellHorse : public MenuItemDefault
	{
		int m_value;
		int m_price;
		bool m_sell;

		virtual void OnSelect()
		{
			const std::string debugname = Common::HashToStr(m_value);
			
			if (m_sell)
			{
				if (CampInventory->HasItem(m_value))
				{
					DioTrace("Selling horse %s", debugname.c_str());

					M_GEN::PlayPurchaseNoise();
					G_FUNDS += m_price;
					
					bool delayedrespawn = m_value == CampInventory->GetBestHorse();

					CampInventory->RemoveItem(m_value);
					IO::WriteDAT();
					ReopenMenuNextTick = M_MENU_HORSE_SELL;

					if (delayedrespawn)
					{
						CAMP::SpawnCampHorse();
					}
				}
				else
				{
					DioTrace("Can't sell horse %s if we don't have any", debugname.c_str());
					M_GEN::PlayPurchaseNoise(false);
					display("Your gang ~COLOR_YELLOW~does not own ~COLOR_WHITE~any of those.");
				}
			}
			else
			{
				if (CampInventory->HasItem(m_value))
				{
					DioTrace("We can't buy horse %s because we already have one in inventory", debugname.c_str());
				}
				else if (G_FUNDS < m_price)
				{
					DioTrace("We can't buy horse %s (%d) with only %s", debugname.c_str(), m_price, M_GEN::CalcMoneyStr(G_FUNDS, true).c_str());
					M_GEN::PlayPurchaseNoise(false);
					display("~COLOR_YELLOW~Cannot afford ~COLOR_WHITE~to purchase.");
				}
				else
				{
					DioTrace("Buying horse %s", debugname.c_str());
					M_GEN::PlayPurchaseNoise();
					G_FUNDS -= m_price;
					CampInventory->AddItem(m_value);
					IO::WriteDAT();
					ReopenMenuNextTick = M_MENU_HORSE_BUY;

					if (m_value == CampInventory->GetBestHorse())
					{
						CAMP::SpawnCampHorse();
					}
				}
			}
		}
	public: MenuItemBuySellHorse(std::string caption, int value, int price, bool sell) : MenuItemDefault(caption), m_value(value), m_price(price), m_sell(sell) {}
	};

	MenuBase* CreateHorseBuyMenu(MenuController* controller)
	{
		MenuBase* menu = new MenuBase(new MenuItemTitle("PURCHASE HORSES"));
		controller->RegisterMenu(menu);

		menu->AddItem(new MenuItemDefault(Common::Format("Gang Funds - %s", M_GEN::CalcMoneyStr(G_FUNDS, true).c_str())));
		menu->AddItem(new MenuItemDefault("Horses are permanent until you sell them."));

		const std::string ownedstr = "[PURCHASED]";

		for (int i = 0; i < HorseTierCount; i++)
		{
			StaticHorseData HorseData = InventoryManager::GetStaticHorseData(HorseTierList[i]);
			
			menu->AddItem(new MenuItemBuySellHorse(
				Common::Format("%s%s%s",
					HorseData.name.c_str(),
					HorseData.spacing.c_str(),
					CampInventory->HasItem(HorseData.hash) ? ownedstr : Common::Format("BUY: %s", M_GEN::CalcMoneyStr(HorseData.buy, false).c_str())
				),
				HorseData.hash, HorseData.buy, false)
			);
		}

		menu->AddItem(new M_GEN::MenuItemBack(controller));

		return menu;
	}

	MenuBase* CreateHorseSellMenu(MenuController* controller)
	{
		MenuBase* menu = new MenuBase(new MenuItemTitle("VIEW & SELL HORSES"));
		controller->RegisterMenu(menu);

		menu->AddItem(new MenuItemDefault("Morgan [DEFAULT]"));
		
		std::vector<Hash> OwnedHorses = CampInventory->GetAllHorses();

		for (int i = 0; i < OwnedHorses.size(); i++)
		{
			StaticHorseData HorseData = InventoryManager::GetStaticHorseData(OwnedHorses[i]);

			menu->AddItem(new MenuItemBuySellHorse(
				Common::Format("%s%s%s",
					HorseData.name.c_str(),
					HorseData.spacing.c_str(),
					Common::Format("SELL: %s", M_GEN::CalcMoneyStr(HorseData.sell, false).c_str())
				),
				HorseData.hash, HorseData.sell, true)
			);
		}

		menu->AddItem(new M_GEN::MenuItemBack(controller));

		return menu;
	}
	
	MenuBase* CreateHorseMenu(MenuController* controller)
	{
		auto menu = new MenuBase(new MenuItemTitle("STABLE"));
		controller->RegisterMenu(menu);

		menu->AddItem(new MenuItemMenu("Manage Current Horses", CreateHorseSellMenu(controller)));
		menu->AddItem(new MenuItemMenu("Purchase Horses", CreateHorseBuyMenu(controller)));

		menu->AddItem(new M_GEN::MenuItemBack(controller));

		return menu;
	}
}

namespace M_MISSION
{
	void CheckPendingCampRaid()
	{
		if (G_PENDING_RAID)
		{
			return;
		}

		if (G_MISSIONS_THIS_SESSION == 0)
		{
			return;
		}

		int count = G_MISSIONS_THIS_SESSION + 1;

		if (G_BOUNTY < 5000)
		{
			return;
		}
		else if (G_BOUNTY > 5000 && G_BOUNTY < 20000)
		{
			if (count % 4 == 0)
			{
				DioTrace(DIOTAG "4 missions in a row between 50 and 200 bounty, adding pending raid");
				G_PENDING_RAID = true;
			}
		}
		else if (G_BOUNTY > 20000 && G_BOUNTY < 50000)
		{
			if (count % 4 == 0)
			{
				DioTrace(DIOTAG "4 missions in a row between 200 and 500 bounty, adding pending raid");
				G_PENDING_RAID = true;
			}
		}
		else if (G_BOUNTY > 50000 && G_BOUNTY < 100000)
		{
			if (count % 3 == 0)
			{
				DioTrace(DIOTAG "3 missions in a row between 500 and 1000 bounty, adding pending raid");
				G_PENDING_RAID = true;
			}
		}
		else if (G_BOUNTY > 100000)
		{
			if (count % 3 == 0)
			{
				DioTrace(DIOTAG "3 missions in a row over 1000 bounty, adding pending raid");
				G_PENDING_RAID = true;
			}
		}
	}
	
	class MenuItemStartMission : public MenuItemDefault
	{
		MissionType m_id;
		MissionStaticData data;

		virtual void OnSelect()
		{
			GEN::ClearWantedStatus();
			
			data = MissionFactory::GetMissionStaticData(m_id);
			
			if (G_MEMBERCOUNT < data.RequiredMembers)
			{
				display("You do not have enough ~COLOR_YELLOW~gang members ~COLOR_WHITE~for this heist.");
			}
			else if (G_FUNDS < data.RequiredFee)
			{
				display("You do not have enough ~COLOR_YELLOW~funds ~COLOR_WHITE~to plan this heist.");
			}
			else
			{
				if (data.RequiredFee > 0)
				{
					DioTrace("Purchasing mission %d for %s", data.StaticMissionID, M_GEN::CalcMoneyStr(data.RequiredFee, true));
					G_FUNDS -= data.RequiredFee;
					IO::WriteDAT();
				}

				close_menu_gen = true;
				CAMP::StartMission(data.StaticMissionID);
			}
		}

	public: MenuItemStartMission(MissionType id, std::string caption = "START HEIST") : MenuItemDefault(caption), m_id(id) {}
	};

	class MenuItemEndMission : public MenuItemDefault
	{
		MenuController* menucontroller;
		bool m_success;

		virtual void OnSelect()
		{
			menucontroller->AllowBack = true;
			menucontroller->PopMenu();

			GEN::FreezePlayer(true);
			GEN::fadeout();

			GEN::ClearWantedStatus();
			PED::CLEAR_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, REL_PLAYER, REL_COP);

			Vector3 loc = ENTITY::GET_ENTITY_COORDS(PlayerPed, true, true);
			int iVar1 = 344970;
			iVar1 |= 1 | 1048576 | 524288 | 24 | 40;
			MISC::CLEAR_AREA(loc.x, loc.y, loc.z, 150.0f, iVar1);
			MISC::CLEAR_AREA(loc.x, loc.y, loc.z, 150.0f, 2097152);
			PERSISTENCE::PERSISTENCE_REMOVE_ALL_ENTITIES_IN_AREA(loc.x, loc.y, loc.z, 150.0f);

			AUDIO::TRIGGER_MUSIC_EVENT("STOP_MUSIC_8S");

			AllowCampPopulation = true;

			if (m_success)
			{
				CAMP::TeleportToCamp();
			}

			CAMP::PopulateCamp();

			GEN::fadein();
			GEN::FreezePlayer(false);
		}
	public: MenuItemEndMission(MenuController* controller, bool success) : MenuItemDefault("FINISH"), menucontroller(controller), m_success(success) {}
	};

	MenuBase* CreateMissionDescriptionMenu(MenuController* controller, MissionStaticData missiondata)
	{
		std::string title;

		for (int i = 0; i < missiondata.StaticMissionName.length(); i++)
		{
			title += toupper(missiondata.StaticMissionName[i]);
		}

		MenuBase* menu = new MenuBase(new MenuItemTitle(title));
		controller->RegisterMenu(menu);

		std::string line1 = missiondata.StaticMissionDescription;
		std::string line2;
		int split = (int)missiondata.StaticMissionDescription.find('\n');

		if (split > 0)
		{
			line1 = missiondata.StaticMissionDescription.substr(0, split);
			line2 = missiondata.StaticMissionDescription.substr(missiondata.StaticMissionDescription.find('\n') + 1);
		}

		menu->AddItem(new MenuItemDefault(line1));

		if (line2.size() > 0)
		{
			menu->AddItem(new MenuItemDefault(line2));
		}

		//menu->AddItem(new M_GEN::MenuItemBlank());

		if (missiondata.RequiredMembers > 0)
		{
			menu->AddItem(new MenuItemDefault(Common::Format("%d / %d GANG MEMBERS", G_MEMBERCOUNT, missiondata.RequiredMembers)));
		}
		
		if (missiondata.RequiredFee > 0)
		{
			menu->AddItem(new MenuItemDefault(Common::Format("%s / %s PLANNING FEE", M_GEN::CalcMoneyStr(G_FUNDS, true), M_GEN::CalcMoneyStr(missiondata.RequiredFee, false))));
		}

		if (missiondata.PotentialRewardCash > 0)
		{
#ifdef MONEY_MULTIPLIER
			int take = missiondata.PotentialRewardCash * MONEY_MULTIPLIER;
			DioTrace("Potential Take %d multiplied by %d = %d", missiondata.PotentialRewardCash, MONEY_MULTIPLIER, take)
			menu->AddItem(new MenuItemDefault(Common::Format("POTENTIAL TAKE: %s", M_GEN::CalcMoneyStr(take, true))));
#else
			menu->AddItem(new MenuItemDefault(Common::Format("POTENTIAL TAKE: %s", M_GEN::CalcMoneyStr(missiondata.PotentialRewardCash, true))));
#endif	
		}

		if (missiondata.PotentialRewardMembers > 0)
		{
			menu->AddItem(new MenuItemDefault(Common::Format("POTENTIAL NEW RECRUITS: %d", missiondata.PotentialRewardMembers)));
		}

		if (missiondata.PotentialRewardBounty > 0)
		{
			menu->AddItem(new MenuItemDefault(Common::Format("POTENTIAL BOUNTY INCREASE: %s", M_GEN::CalcMoneyStr(missiondata.PotentialRewardBounty, false))));
		}

		//menu->AddItem(new M_GEN::MenuItemBlank());
		menu->AddItem(new MenuItemStartMission(missiondata.StaticMissionID));
		menu->AddItem(new M_GEN::MenuItemBack(controller));

		return menu;
	}

	MenuBase* CreateMissionMenu(MenuController* controller)
	{
		auto menu = new MenuBase(new MenuItemTitle("AVAILABLE MISSIONS"));
		controller->RegisterMenu(menu);

		DioTrace("Populating missions menu for camp %d upgrade %d", G_CAMP, G_CAMP_UPGR);

		if (G_PENDING_RAID)
		{
			MissionType raidType = (G_BOUNTY > 20000) ? MissionType::CAMPRAID_LAW : MissionType::CAMPRAID_GANG;
			
			if (G_BOUNTY > 100000)
			{
				raidType = MissionType::CAMPRAID_PINKERTONS;
			}

			MissionStaticData availablemission = MissionFactory::GetMissionStaticData(raidType);

			menu->AddItem(new MenuItemMenu(availablemission.StaticMissionName, CreateMissionDescriptionMenu(controller, availablemission)));
			
			menu->AddItem(new M_GEN::MenuItemBack(controller));

			return menu;
		}

		if (G_CAMP_UPGR < 3)
		{
			menu->AddItem(new MenuItemDefault("Upgrade your camp to unlock more missions."));
		}

		std::vector<MissionType> allmissions = MissionFactory::GetAvailableMissionsForCampUpgrade(G_CAMP, G_CAMP_UPGR);

		if (allmissions.empty())
		{
			DioTrace(DIOTAG1("ERROR") "CAMP UPGRADE HAS NO AVAILABLE MISSIONS");
		}

		for (int i = 0; i < allmissions.size(); i++)
		{
			MissionType missionid = allmissions[i];
			
			MissionStaticData availablemission = MissionFactory::GetMissionStaticData(missionid);
			
			menu->AddItem(new MenuItemMenu(availablemission.StaticMissionName, CreateMissionDescriptionMenu(controller, availablemission)));
		}
		
		menu->AddItem(new M_GEN::MenuItemBack(controller));

		return menu;
	}

	MenuBase* DisplayCachedMissionResultsMenu(MenuController* controller, MissionResults results)
	{
		KillCustomFailEffects();
		
		bool missionsuccess = results.MissionResultStatus == M_EXIT_SUCCESS;

		GEN::ClearWantedStatus();

		MenuBase* menu = new MenuBase(new MenuItemTitle(Common::Format("MISSION %s", missionsuccess ? "SUCCESS" : "FAILURE")));
		controller->RegisterMenu(menu);

		if (results.MissionFailStr.size() > 0 && results.MissionFailStr != "NONE")
		{
			menu->AddItem(new MenuItemDefault(results.MissionFailStr));
		}

		menu->AddItem(new MenuItemDefault(Common::Format("GANG MEMBERS LOST: %d", results.Casualties.size())));
		
		if (missionsuccess)
		{
			if (results.RewardMembers > 0)
			{
				menu->AddItem(new MenuItemDefault(Common::Format("GANG MEMBERS RECRUITED: %d", results.RewardMembers)));
			}
			
			if (results.RewardCash > 0)
			{
				menu->AddItem(new MenuItemDefault(
					Common::Format(
						"CASH REWARD: %s (YOUR CUT: %s)", 
						M_GEN::CalcMoneyStr(results.RewardCash, true).c_str(), 
						M_GEN::CalcMoneyStr(MissionController::CalculatePlayerCutFromMoneyAmount(results.RewardCash), true).c_str()
					)
				));
			}

			if (results.RewardBounty > 0)
			{
				menu->AddItem(new MenuItemDefault(Common::Format("GANG BOUNTY INCREASE: %s", M_GEN::CalcMoneyStr(results.RewardBounty, true))));
			}
		}
		else
		{
			MissionStaticData data = MissionFactory::GetMissionStaticData(results.MissionResultID);
			int retryfee = data.RequiredFee;
			int retrymembers = data.RequiredMembers;
			std::string retrystr = "RETRY HEIST";

			if (retryfee > 0 && retrymembers > 0)
			{
				retrystr = Common::Format("RETRY HEIST (%s / %s fee -  %d / %d members)", M_GEN::CalcMoneyStr(G_FUNDS, true), M_GEN::CalcMoneyStr(retryfee, false), G_MEMBERCOUNT, retrymembers);
			}
			else if (retryfee > 0)
			{
				retrystr = Common::Format("RETRY HEIST (%s / %s fee)", M_GEN::CalcMoneyStr(G_FUNDS, true), M_GEN::CalcMoneyStr(retryfee, false));
			}
			else if (retrymembers > 0)
			{
				retrystr = Common::Format("RETRY HEIST (%d / %d members)", G_MEMBERCOUNT, retrymembers);
			}

			menu->AddItem(new MenuItemStartMission(results.MissionResultID, retrystr));
		}

		menu->AddItem(new MenuItemEndMission(controller, missionsuccess));

		return menu;
	}
}

void ScriptShutdown()
{
	DioScope(DIOTAG);

	IO::datFile.close();
	
	GEN::ClearScriptCamera(true);

	CAMP::DepopulateCamp();
	CAMP::DeleteCamp();

	GEN::FreezePlayer(false);
	ENTITY::FREEZE_ENTITY_POSITION(PLAYER::PLAYER_PED_ID(), false);

	KillCustomFailEffects();

	GEN::fadein();
}

Vector3 DebugData[3] = {{0,0,-100}, {0,0,0}, {1,1,1}};
int DebugDataIndex = 0;
int DebugVectorIndex = 0;

void ScriptMain()
{
	DioRestoreIndent(0);
	
	if (firstLoad)
	{
		if (!IO::startup())
		{
			return;
		}

		srand(MISC::GET_GAME_TIMER());

		INIT::startup();
		CAMP::startup();
		GANG::startup();

		CampInventory = new InventoryManager();

		IO::ReadDAT();
		firstLoad = false;

		INI::SetDutchGangBlock();
	}

#ifdef DEBUG_CAMERA
	freecam = new FreeCam();
#endif

	auto menuController = new MenuController();

	while (true)
	{
#ifdef DEBUG_CAMERA
		freecam->HandleCam();
#endif
		PlayerPed = PLAYER::PLAYER_PED_ID();
		PlayerHorse = PLAYER::_GET_SADDLE_HORSE_FOR_PLAYER(0);

		if (GRAPHICS::ANIMPOSTFX_IS_RUNNING("MissionFail01"))
		{
			HUD::HIDE_HUD_AND_RADAR_THIS_FRAME();
		}

		if (DLC::GET_IS_LOADING_SCREEN_ACTIVE())
		{
			DioTrace(DIOTAG1("WARNING") "LOADGAME DETECTED, RESETTING MOD");

			while (DLC::GET_IS_LOADING_SCREEN_ACTIVE() || !PLAYER::IS_PLAYER_CONTROL_ON(0))
			{
				WAIT(1);
			}

			IO::WriteDAT();

			CAMP::DepopulateCamp();
			CAMP::DeleteCamp();
			campSpawned = false;

			hasCamp = false;

			IO::ReadDAT();
		}

		//////////////////////////////////////// INITIAL SETUP WITH STRANGE MAN //////////////////////////////

		INIT::init();
		INIT::update();

		if (open_init && !menuController->HasActiveMenu())
		{
			open_init = false;
			auto mainMenu = M_INIT::CreateInitMenu(menuController);
			MenuInput::MenuInputBeep();
			menuController->PushMenu(mainMenu);
			menuController->AllowBack = false;
			GEN::FreezePlayer(true);
		}
		else if (close_init && relocation)
		{
			DioTrace(DIOTAG "close_init relocation");
			
			MenuInput::MenuInputBeep();
			M_GEN::ClearAllOpenMenus(menuController);

			IO::WriteDAT();

			GEN::fadeout();

			HUD::_UIPROMPT_SET_ENABLED(INIT::initPrompt, false);
			HUD::_UIPROMPT_SET_VISIBLE(INIT::initPrompt, false);

			running_init = false;
			close_init = false;
		}
		else if (close_init && menuController->HasActiveMenu())
		{
			DioTrace(DIOTAG "close_init HasActiveMenu");

			MenuInput::MenuInputBeep();
			M_GEN::ClearAllOpenMenus(menuController);

			IO::WriteDAT();

			GEN::fadeout();

			HUD::_UIPROMPT_SET_ENABLED(INIT::initPrompt, false);
			HUD::_UIPROMPT_SET_VISIBLE(INIT::initPrompt, false);

			running_init = false;
			close_init = false;
			CAMP::TeleportToCamp();
			GEN::fadein();
		}

		///////////////////////////////////////////////// DEBUG //////////////////////////////////////////////
#ifdef DEBUG_COMMANDS

		PLAYER::SET_PLAYER_CAN_USE_COVER(0, false);
		ENTITY::SET_ENTITY_INVINCIBLE(PlayerHorse, true);

		// F1 - Sandbox
		if (IsKeyJustUp(VK_F1))
		{
			
		}
		
		// Page Up - Teleport to Camp
		if (IsKeyJustUp(VK_PRIOR)) 
		{
			SanityCheck();
			GEN::fadeout();
			CAMP::TeleportToCamp();
			GEN::fadein();
		}
		
		// Page Down - Teleport to Waypoint
		if (IsKeyJustUp(VK_NEXT)) 
		{
			Vector3 dest = MAP::_GET_WAYPOINT_COORDS();
			ENTITY::SET_ENTITY_COORDS(PlayerPed, dest.x, dest.y, -199.9999f, true, true, true, true);
		}

		// Delete - Delete Camp & Clear Area
		if (IsKeyJustUp(VK_DELETE))
		{
			DioTrace(DIOTAG1("DEBUG") "Deleting Camp");
			CAMP::DepopulateCamp();
			CAMP::DeleteCamp();
			hasCamp = false;

			M_GEN::ClearAllOpenMenus(menuController);

			GEN::FreezePlayer(false);

			KillCustomFailEffects();

			GEN::ClearWantedStatus();

			Common::ClearArea(ENTITY::GET_ENTITY_COORDS(PlayerPed, true, true), 300.0f);
		}

		// F5 - Print Player/Entity Coords & Heading
		if (IsKeyJustUp(VK_F5))
		{
			Vector3 pp = ENTITY::GET_ENTITY_COORDS(PlayerPed, true, true);
			int ph = (int)ENTITY::GET_ENTITY_HEADING(PlayerPed);

			float ground = 0;
			MISC::GET_GROUND_Z_FOR_3D_COORD(pp.x, pp.y, pp.z, &ground, true);

			DioTrace("PLAYER POSITION: {{%.2ff, %.2ff, %.2ff}, %d}  PLAYER ACTUAL: %f GROUND Z ACTUAL: %f", pp.x, pp.y, ground, ph, pp.z, ground );

			Entity e = 0;
			PLAYER::GET_ENTITY_PLAYER_IS_FREE_AIMING_AT(0, &e);

			if (ENTITY::DOES_ENTITY_EXIST(e))
			{
				Vector3 ec = ENTITY::GET_ENTITY_COORDS(e, true, true);
				DioTrace("AIMED ENTITY: 0x%X {%.2ff, %.2ff, %.2ff, %.1ff}", ENTITY::GET_ENTITY_MODEL(e), ec.x, ec.y, ec.z, ENTITY::GET_ENTITY_HEADING(e));
			}
		}

		// F6 - Reserved for Photo Mode

		// F7 - Audio Test
		if (IsKeyJustUp(VK_F7))
		{
			ScriptedSpeechParams params{"", 0, 0, MISC::GET_HASH_KEY("SPEECH_PARAMS_BEAT_SHOUTED_CLEAR_NOSUB"), 0, true, 0, 0};

			AUDIO::PLAY_PED_AMBIENT_SPEECH_NATIVE(PlayerPed, (Any*)&params);
		}
		
		// F8 - Quickstart Mission
		if (IsKeyJustUp(VK_F8))
		{
			if (!campSpawned)
			{
				SanityCheck();
				GEN::fadeout();
				CAMP::TeleportToCamp();
				GEN::fadein();
			}
			
			if (CampMissionController != NULL && !CampMissionController->HasActiveMission())
			{
				MissionType manualmission = MissionType::ROBBERY_TRAIN;

				DioTrace("Manually Starting Mission %d", manualmission);
				CAMP::StartMission(manualmission);
			}
		}
		
		// F9 - Respawn Camp
		if (IsKeyJustUp(VK_F9))
		{
			DioTrace(DIOTAG1("DEBUG") "Respawning Camp");

			KillCustomFailEffects();

			CAMP::DepopulateCamp();
			CAMP::DeleteCamp();
			WAIT(100);
			campSpawned = false;
			campgroundLoaded = CAMP::IsCampGroundLoaded();
			AllowCampPopulation = true;

			IO::ReadDAT();
		}

		// F10 - Free Camera
		if (IsKeyJustUp(VK_F10))
		{
#ifdef DEBUG_CAMERA
			freecam->ToggleEnabledState();

			if (!freecam->GetEnabled())
			{
				MISC::SET_TIME_SCALE(1.0);
			}

			HUD::DISPLAY_HUD(!freecam->GetEnabled());
#endif
		}

		// RESERVED FOR MARKER TOOLS
		// F11
		// F12
		// (Top Row) Plus
		// (Top Row) Minus

		// (Keypad) Plus - Play Music
		if (IsKeyJustUp(VK_ADD))
		{
			display("Start");
			AUDIO::TRIGGER_MUSIC_EVENT("CACR3_JUMP");
		}

		// (Keypad) Minus - Stop Music
		if (IsKeyJustUp(VK_SUBTRACT))
		{
			display("Stop");
			AUDIO::TRIGGER_MUSIC_EVENT("FSM1_FAIL");
		}
#endif
#ifdef DEBUG_MARKERS
		if (IsKeyJustUp(VK_F11))
		{
			if (DebugData[0].z != -100)
			{
				DebugData[0] = { 0, 0, -100 };
			}
			else
			{
				DebugData[0] = ENTITY::GET_ENTITY_COORDS(PLAYER::PLAYER_PED_ID(), true, true);
			}
		}

		if (DebugData[0].z != -100)
		{
			HUD::_DISPLAY_TEXT(Common::Format("POS: %.2f %.2f %.2f", DebugData[0].x, DebugData[0].y, DebugData[0].z).c_str(), 0.1f, 0.05f);
			HUD::_DISPLAY_TEXT(Common::Format("ROT: %.2f %.2f %.2f", DebugData[1].x, DebugData[1].y, DebugData[1].z).c_str(), 0.1f, 0.10f);
			HUD::_DISPLAY_TEXT(Common::Format("SCL: %.2f %.2f %.2f", Common::WorldFloatToMarkerScale(DebugData[2].x), Common::WorldFloatToMarkerScale(DebugData[2].y), Common::WorldFloatToMarkerScale(DebugData[2].z)).c_str(), 0.1f, 0.15f);

			HUD::_DISPLAY_TEXT("->", 0.05f, 0.05f + (DebugDataIndex * 0.05f));
			HUD::_DISPLAY_TEXT("^", 0.2f + (DebugVectorIndex * 0.1f), 0.2f);

			if (IsKeyJustUp(VK_DOWN) && DebugDataIndex < 2)
			{
				DebugDataIndex++;
			}

			if (IsKeyJustUp(VK_UP) && DebugDataIndex > 0)
			{
				DebugDataIndex--;
			}

			if (IsKeyJustUp(VK_RIGHT) && DebugVectorIndex < 2)
			{
				DebugVectorIndex++;
			}

			if (IsKeyJustUp(VK_LEFT) && DebugVectorIndex > 0)
			{
				DebugVectorIndex--;
			}

			const float step = 0.05f;

			if (IsKeyDown(VK_OEM_PLUS))
			{
				switch (DebugVectorIndex)
				{
				case 0: DebugData[DebugDataIndex].x += step; break;
				case 1: DebugData[DebugDataIndex].y += step; break;
				case 2: DebugData[DebugDataIndex].z += step; break;
				}
			}

			if (IsKeyDown(VK_OEM_MINUS))
			{
				switch (DebugVectorIndex)
				{
				case 0: DebugData[DebugDataIndex].x -= step; break;
				case 1: DebugData[DebugDataIndex].y -= step; break;
				case 2: DebugData[DebugDataIndex].z -= step; break;
				}
			}

			Common::DrawDebugRectangle(DebugData[0], DebugData[1], DebugData[2], "COLOR_BLUE", false);

			if (IsKeyJustUp(VK_F12))
			{
				//DioTrace("VOLUME::_CREATE_VOLUME_BOX(%.2ff, %.2ff, %.2ff, %.2ff, %.2ff, %.2ff, %.2ff, %.2ff, %.2ff);",
				DioTrace("{%.2ff, %.2ff, %.2ff}, {%.2ff, %.2ff, %.2ff}, {%.2ff, %.2ff, %.2ff}",
					DebugData[0].x, DebugData[0].y, DebugData[0].z,
					DebugData[1].x, DebugData[1].y, DebugData[1].z,
					Common::WorldFloatToMarkerScale(DebugData[2].x), Common::WorldFloatToMarkerScale(DebugData[2].y), Common::WorldFloatToMarkerScale(DebugData[2].z)
				);
			}
		}
#endif
		////////////////////////////////////////////// MAIN CAMP LOGIC ///////////////////////////////////////

		if (hasCamp && !MISC::GET_MISSION_FLAG())
		{
			campgroundLoaded = CAMP::IsCampGroundLoaded();

			if (!campSpawned)
			{
				CAMP::init();
			}

			if (campSpawned)
			{
				CAMP::update();
			}

			if (relocation)
			{
				CAMP::DepopulateCamp();
				CAMP::DeleteCamp();

				CAMP::TeleportToCamp();
				
				GEN::fadein();
				
				relocation = false;
			}

			if (open_camp && !menuController->HasActiveMenu())
			{
				open_camp = false;
				MenuInput::MenuInputBeep();
				auto campMenu = M_CAMP::CreateCampMenu(menuController);
				menuController->PushMenu(campMenu);
				menuController->AllowBack = false;
				GEN::FreezePlayer(true);
			}
			
			if (open_armory && !menuController->HasActiveMenu())
			{
				open_armory = false;
				MenuInput::MenuInputBeep();
				auto armoryMenu = M_ARMORY::CreateArmoryMenu(menuController);
				menuController->PushMenu(armoryMenu);
				menuController->AllowBack = false;
				GEN::FreezePlayer(true);
			}

			if (open_horse && !menuController->HasActiveMenu())
			{
				open_horse = false;
				MenuInput::MenuInputBeep();
				auto horseMenu = M_HORSE::CreateHorseMenu(menuController);
				menuController->PushMenu(horseMenu);
				menuController->AllowBack = false;
				GEN::FreezePlayer(true);
			}

			if (open_mission && !menuController->HasActiveMenu())
			{
				open_mission = false;
				MenuInput::MenuInputBeep();
				auto missionMenu = M_MISSION::CreateMissionMenu(menuController);
				menuController->PushMenu(missionMenu);
				menuController->AllowBack = false;
				GEN::FreezePlayer(true);
			}

			if (ReopenMenuNextTick > M_MENU_NONE)
			{
				M_GEN::ClearAllOpenMenus(menuController);

				switch (ReopenMenuNextTick)
				{
					case M_MENU_CAMP:
						menuController->PushMenu(M_CAMP::CreateCampMenu(menuController));
						break;
				
					case M_MENU_WEAPON_BUY:
						menuController->PushMenu(M_ARMORY::CreateArmoryMenu(menuController));
						menuController->PushMenu(M_ARMORY::CreateWeaponBuyMenu(menuController));
						break;

					case M_MENU_WEAPON_SELL:
						menuController->PushMenu(M_ARMORY::CreateArmoryMenu(menuController));
						menuController->PushMenu(M_ARMORY::CreateWeaponSellMenu(menuController));
						break;

					case M_MENU_ARMOR:
						menuController->PushMenu(M_ARMORY::CreateArmoryMenu(menuController));
						menuController->PushMenu(M_ARMORY::CreateArmorMenu(menuController));
						break;

					case M_MENU_HORSE_BUY:
						menuController->PushMenu(M_HORSE::CreateHorseMenu(menuController));
						menuController->PushMenu(M_HORSE::CreateHorseBuyMenu(menuController));
						break;

					case M_MENU_HORSE_SELL:
						menuController->PushMenu(M_HORSE::CreateHorseMenu(menuController));
						menuController->PushMenu(M_HORSE::CreateHorseSellMenu(menuController));
						break;
				}
				
				ReopenMenuNextTick = M_MENU_NONE;

				menuController->AllowBack = false;
				GEN::FreezePlayer(true);
			}

			if (close_menu_gen)
			{
				close_menu_gen = false;
				MenuInput::MenuInputBeep();
				M_GEN::ClearAllOpenMenus(menuController);
				GEN::FreezePlayer(false);
			}

			if (menuController->HasActiveMenu() && PLAYER::IS_PLAYER_CONTROL_ON(0))
			{
#ifndef DEBUG_CAMERA_LOG
				DioTrace(DIOTAG1("WARNING") "Player regained control while menu is open, refreezing");
				GEN::FreezePlayer(true);
#endif
			}
		}

		/////////////////////////////////////////////// MISSION LOGIC ////////////////////////////////////////

		bool NeedsMissionShutdown = false;

		if (CampMissionController != NULL)
		{
			if (CampMissionController->HasActiveMission())
			{
				CampMissionController->Update();

				if (CampMissionController->ActiveMissionFinishedLoading)
				{
					GEN::fadein();
					GEN::FreezePlayer(false);
					CampMissionController->ActiveMissionFinishedLoading = false;
				}
			}
			else if (!CampMissionController->ServedCachedMissionResults)
			{
				MissionResults results = CampMissionController->CachedMissionResults;
				
				bool SkipResults = false;

				switch (results.MissionResultStatus)
				{
					case M_EXIT_FAILURE:
						break;
				
					case M_EXIT_FAILURE_GEN_ENTERED_CAMP:
						results.MissionFailStr = "You got too close to an active Story camp.";
						break;

					case M_EXIT_FAILURE_GEN_PLAYER_DIED:
						results.MissionFailStr = "You died.";
						break;

					case M_EXIT_FAILURE_LAW_ALERTED:
						results.MissionFailStr = "You alerted the law prematurely.";
						break;

					case M_EXIT_FAILURE_ABANDONED_OBJECTIVE:
						results.MissionFailStr = "You abandoned the objective.";
						break;

					case M_EXIT_FAILURE_GEN_PLAYER_ARRESTED:
					case M_EXIT_FAILURE_GEN_MISSION_STARTED:
					case M_EXIT_ERROR_TERMINATE:
						results.MissionFailStr = "TERMINATED";
						SkipResults = true;
						break;

					default:
						results.MissionFailStr = "NONE";
						break;
				}

				DioTrace("EXITING MISSION %d WITH CODE %d", results.MissionResultID, results.MissionResultStatus);
				DioTrace("FAIL STR: %s", results.MissionFailStr.c_str());
				if (results.MissionFailStr == "NONE")
				{
					DioIndentIn();
					DioTrace(
						"BOUNTY:  %s (%d)\n"
						"MONEY:   %s (%d)\n"
						"MEMBERS: %d"
						,
						M_GEN::CalcMoneyStr(results.RewardBounty, false), results.RewardBounty,
						M_GEN::CalcMoneyStr(results.RewardCash, true), results.RewardCash,
						results.RewardMembers
					);
					DioIndentOut();
				}

				DioTrace("CASUALITES: %d", results.Casualties.size());

				std::vector<Hash> LostEquipment;

				if (results.Casualties.size() > 0)
				{
					DioTrace("EQUIPMENT LOST:");

					for (int i = 0; i < results.Casualties.size(); i++)
					{
						MissionCompanionInfo casualty = results.Casualties[i];

						if (casualty.CompanionUniqueID != COMP_LOOTED)
						{
							if (casualty.CompanionLongarm > 0)
							{
								LostEquipment.emplace_back(casualty.CompanionLongarm);
							}
							else if(casualty.CompanionSidearm > 0)
							{
								LostEquipment.emplace_back(casualty.CompanionSidearm);
							}
							else
							{
								DioTrace(DIOTAG1("WARNING") "Ped was not carrying any weapons to lose");
							}
						}

						LostEquipment.emplace_back(casualty.CompanionArmor);
					}
				}

				DioIndentIn();
				for (int i = 0; i < LostEquipment.size(); i++)
				{
					DioTrace("%s", Common::HashToStr(LostEquipment[i]).c_str());
					CampInventory->RemoveItem(LostEquipment[i]);
				}
				DioIndentOut();

				G_MEMBERCOUNT -= (int)results.Casualties.size();

				if (results.MissionResultStatus == M_EXIT_SUCCESS)
				{
					int PlayerCut = 0;
					
					if (results.RewardCash > 0)
					{
#ifdef MONEY_MULTIPLIER
						int oldCash = results.RewardCash;
						results.RewardCash *= MONEY_MULTIPLIER;
						DioTrace("Cash %d multiplied by %d = %d", oldCash, MONEY_MULTIPLIER, results.RewardCash)
#endif
						PlayerCut = MissionController::CalculatePlayerCutFromMoneyAmount(results.RewardCash);
						int CampCut = results.RewardCash - PlayerCut;

						DioTrace("CASH REWARD CAMP CUT: %d", CampCut);
						G_FUNDS += CampCut;
					}
					
					DioTrace("CASH REWARD PLAYER CUT: %d", PlayerCut);
					MONEY::_MONEY_INCREMENT_CASH_BALANCE(PlayerCut, 0);

					G_MEMBERCOUNT += results.RewardMembers;
					G_BOUNTY += results.RewardBounty;

					M_MISSION::CheckPendingCampRaid();
					
					if (results.MissionResultID == MissionType::CAMPRAID_GANG ||
						results.MissionResultID == MissionType::CAMPRAID_LAW ||
						results.MissionResultID == MissionType::CAMPRAID_PINKERTONS)
					{
						DioTrace("RAID SUCCESSFULLY COMPLETED");
						G_PENDING_RAID = false;
					}
					else
					{
						G_MISSIONS_THIS_SESSION++;
						DioTrace("MISSIONS THIS SESSION: %d", G_MISSIONS_THIS_SESSION);
					}
				}

				IO::WriteDAT();

				if (results.MissionResultStatus == M_EXIT_FAILURE_GEN_MISSION_STARTED)
				{
					NeedsMissionShutdown = true;
				}

				if (results.MissionResultStatus == M_EXIT_FAILURE_GEN_PLAYER_DIED)
				{
					WAIT(25000);
				}

				if (results.MissionResultStatus == M_EXIT_FAILURE_LAW_ALERTED || results.MissionResultStatus == M_EXIT_FAILURE_ABANDONED_OBJECTIVE)
				{
					WAIT(4000);
				}

				GEN::ClearWantedStatus();

				if (!SkipResults)
				{
					MenuInput::MenuInputBeep();
					auto resultsMenu = M_MISSION::DisplayCachedMissionResultsMenu(menuController, results);
					menuController->PushMenu(resultsMenu);
					menuController->AllowBack = false;
					GEN::FreezePlayer(true, true);
				}
				else
				{
					if (CAM::IS_SCREEN_FADED_OUT())
					{
						GEN::fadein();
					}
					GEN::FreezePlayer(false);
				}

				if (results.MissionResultStatus == M_EXIT_ERROR_TERMINATE)
				{
					UILOG::_UILOG_SET_CACHED_OBJECTIVE("~COLOR_RED~ERROR:~COLOR_WHITE~ Could not start mission! Please report this to the mod developer.");
					UILOG::_UILOG_PRINT_CACHED_OBJECTIVE();

					AllowCampPopulation = true;
					CAMP::PopulateCamp();
				}

				CampMissionController->ServedCachedMissionResults = true;

				if (ENTITY::DOES_ENTITY_EXIST(CAMP::CurrentCampMaxim))
				{
					VEHICLE::SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(CAMP::CurrentCampMaxim, false);
				}
			}
			else if (MISC::GET_MISSION_FLAG())
			{
				NeedsMissionShutdown = true;
			}
		}
		else if (MISC::GET_MISSION_FLAG())
		{
			NeedsMissionShutdown = true;
		}

		//////////////////////////////////////////////////////////////////////////////////////////////////////

		if (NeedsMissionShutdown && campSpawned)
		{
			DioTrace(DIOTAG1("WARNING") "Player started mission, shutting down");

			IO::WriteDAT();

			CAMP::DepopulateCamp();
			CAMP::DeleteCamp();
			campSpawned = false;
		}

		menuController->Update();
		WAIT(0);
	}
}

#define _DIO_MODULE_NAME "GangMod"
#include <dio_trace.h>
