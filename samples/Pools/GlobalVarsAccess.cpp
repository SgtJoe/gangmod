
#include "precomp.h"
#include "GlobalVarsAccess.h"

bool GlobalVarsAccess::HAS_REGION_FLAG(int region, int flag)
{
	if (!RegionIDValid(region))
	{
		DioTrace(DIOTAG1("ERROR") "Invalid Region %d", region);
		return false;
	}

	UINT64* GlobalVar = GetRegionGlobal(region);

	return (*GlobalVar & flag) != 0;
}

void GlobalVarsAccess::APPLY_REGION_FLAG(int region, int flag)
{
	if (!RegionIDValid(region))
	{
		DioTrace(DIOTAG1("ERROR") "Invalid Region %d", region);
		return;
	}

	UINT64* GlobalVar = GetRegionGlobal(region);

	*GlobalVar = (*GlobalVar | flag);
}

void GlobalVarsAccess::REMOVE_REGION_FLAG(int region, int flag)
{
	if (!RegionIDValid(region))
	{
		DioTrace(DIOTAG1("ERROR") "Invalid Region %d", region);
		return;
	}

	UINT64* GlobalVar = GetRegionGlobal(region);

	*GlobalVar = (*GlobalVar - (*GlobalVar & flag));
}

std::string GlobalVarsAccess::RegionIDToFriendlyName(int region)
{
	std::string name;

	switch (region)
	{
		case REGION_BAY_SAINT_DENIS: name = "Saint Denis"; break;
		case REGION_GRT_BLACKWATER: name = "Blackwater"; break;
		case REGION_HRT_VALENTINE: name = "Valentine"; break;
		case REGION_SCM_RHODES: name = "Rhodes"; break;
		case REGION_CHO_ARMADILLO: name = "Armadillo"; break;
		case REGION_GAP_TUMBLEWEED: name = "Tumbleweed"; break;

		default:
			name = Common::Format("Invalid Region %d", region);
	}

	return name;
}

std::string GlobalVarsAccess::RegionHashToIDName(Hash region)
{
	std::string name = "none";

	for (int i = 0; i < REGION_COUNT; i++)
	{
		if (region == MISC::GET_HASH_KEY(REGION_STRING_ARRAY[i].c_str()))
		{
			name = REGION_STRING_ARRAY[i];
		}
	}

	if (name == "none")
	{
		DioTrace(DIOTAG1("ERROR") "Invalid Region Hash %x", region);
	}

	return name;
}

Hash GlobalVarsAccess::RegionIDToHash(int region, bool outline)
{
	std::string str;

	switch (region)
	{
		case REGION_BAY_SAINT_DENIS: str = "REGION_BAY_SAINT_DENIS"; break;
		case REGION_GRT_BLACKWATER: str = "REGION_GRT_BLACKWATER"; break;
		case REGION_HRT_VALENTINE: str = "REGION_HRT_VALENTINE"; break;
		case REGION_SCM_RHODES: str = "REGION_SCM_RHODES"; break;
		case REGION_CHO_ARMADILLO: str = "REGION_CHO_ARMADILLO"; break;
		case REGION_GAP_TUMBLEWEED: str = "REGION_GAP_TUMBLEWEED"; break;

		default:
			DioTrace(DIOTAG1("ERROR") "Invalid Region %d", region);
	}

	if (outline)
	{
		str += "_OUTLINE";
	}

	return MISC::GET_HASH_KEY(str.c_str());
}

void GlobalVarsAccess::SetRegionLocked(int id, bool locked)
{
	DioTrace(DIOTAG "Setting %s locked %d", RegionIDToFriendlyName(id).c_str(), (int)locked);

	MAP::_MAP_DISABLE_REGION_BLIP(RegionIDToHash(id, false));
	MAP::_MAP_DISABLE_REGION_BLIP(RegionIDToHash(id, true));

	REMOVE_REGION_FLAG(id, (int)REGION_FLAG::LOCKDOWN);
	REMOVE_REGION_FLAG(id, (int)REGION_FLAG::PERMANENT_LOCKDOWN);
	REMOVE_REGION_FLAG(id, (int)REGION_FLAG::WANTED_ZONE);
	
	if (locked)
	{
		APPLY_REGION_FLAG(id, (int)REGION_FLAG::LOCKDOWN);
		APPLY_REGION_FLAG(id, (int)REGION_FLAG::PERMANENT_LOCKDOWN);
		APPLY_REGION_FLAG(id, (int)REGION_FLAG::WANTED_ZONE);

		MAP::_MAP_ENABLE_REGION_BLIP(RegionIDToHash(id, true), MapColorHashes::BLIP_STYLE_WANTED_REGION);
		MAP::_MAP_ENABLE_REGION_BLIP(RegionIDToHash(id, false), MapColorHashes::BLIP_STYLE_WANTED_REGION);
	}
}

void GlobalVarsAccess::SET_WORLD_STATE_FLAG(WorldState worldState, bool on)
{
	int iVar0;
	int iVar1;
	int iVar2;

	iVar0 = (int)worldState;
	iVar1 = (iVar0 / 31);
	iVar2 = (iVar0 % 31);

	//										Global_40.f_283[iVar1]
	UINT64* Global_40_f_283_iVar1 = getGlobalPtr(40 + 283 + 1 + iVar1);

	if (on)
	{
		MISC::SET_BIT(Global_40_f_283_iVar1, iVar2);
	}
	else
	{
		MISC::CLEAR_BIT(Global_40_f_283_iVar1, iVar2);
	}

	UINT64* Global_1934765 = getGlobalPtr(1934765);
	*Global_1934765 = 0;
}
