
#include "precomp.h"
#include "FreeRoam.h"

const MissionStaticData& FreeRoam::GetStaticMissionData()
{
	static MissionStaticData missiondata = {MissionType::FREEROAM, 0, 0, 0, 0, 0, "Free Roam", "Take your gang out on an expedition.", COMPCOUNT_MIN, false, MUSIC_DEFAULT, false};
	return missiondata;
}

bool FreeRoam::SetMissionParamsForCamp(int camp)
{
	switch (camp)
	{
		case DISTRICT_GRIZZLIES_WEST:
			CampPoint = {-925.723f, 1563.88f, 237.44f};
			CampRadius = 14;
			return true;

		case DISTRICT_CUMBERLAND_FOREST:
			CampPoint = {741.61f, 1192.49f, 142.76f};
			CampRadius = 17;
			return true;

		case DISTRICT_BLUEGILL_MARSH:
			CampPoint = {1953.97f, -478.75f, 40.73f};
			CampRadius = 16;
			return true;

		case DISTRICT_RIO_BRAVO:
			CampPoint = {-4916.46f, -3145.15f, -14.17f};
			CampRadius = 11;
			return true;
	}

	DioTrace(DIOTAG1("ERROR") "CANNOT SET MISSION %d PARAMS FOR CAMP %d - ABORTING MISSION", FreeRoam::GetStaticMissionData().StaticMissionID, camp);
	return false;
}

void FreeRoam::CommonInit()
{
	const MissionStaticData& thisMissionData = FreeRoam::GetStaticMissionData();
	SetVarsFromStaticData(thisMissionData);

	MissionAssetsToLoad = {};

	ObjectivesList = {
		"You are now in ~COLOR_YELLOW~free roam",
		"Return to your ~COLOR_YELLOW~camp~COLOR_WHITE~ to finish the mission"
	};

	LoadAllMissionAssets();
	SpawnGangMembers(thisMissionData.NumMembersToSpawn);

	WAIT(2000);
}

FreeRoam::FreeRoam()
{
	DioScope(DIOTAG);
}

FreeRoam::~FreeRoam()
{
	DioScope(DIOTAG);
}

void FreeRoam::Setup(int campid, const std::string& gangstr, const std::vector<MissionCompanionInfo>& companions)
{
	DioScope(DIOTAG "campid %d gangstr %s", campid, gangstr.c_str());

	GangName = gangstr;
	GangMemberTemplates = companions;

	CommonInit();

	if (!SetMissionParamsForCamp(campid))
	{
		ExitMissionFlag = M_EXIT_ERROR_TERMINATE;
		return;
	}
}

void FreeRoam::ObjectiveInit_0()
{
	ObjectiveSet(0);
	ActivateObjective_0 = false;
	RunObjective_0 = true;

	CurrentMusicStage = STAGE_MUTED;

	SetCompanionPlayerCollision(false);

	for (int i = 0; i < SpawnedGangMembers.size(); i++)
	{
		ENTITY::SET_ENTITY_INVINCIBLE(SpawnedGangMembers[i].member, true);
	}

	AllowWanted = true;
	AllowCompanionManualControl = true;
}

void FreeRoam::ObjectiveLogic_0()
{
	if (Common::GetDistBetweenCoords(PlayerCoords, CampPoint) > CampRadius * 2.5f)
	{
		RunObjective_0 = false;
		ActivateObjective_1 = true;
	}
}

void FreeRoam::ObjectiveInit_1()
{
	ObjectiveSet(1);
	ActivateObjective_1 = false;
	RunObjective_1 = true;

	CampBlip = ObjectiveBlipCoords(CampPoint, CampRadius, false, ObjectivesList[1]);
}

void FreeRoam::ObjectiveLogic_1()
{
	if (Common::GetDistBetweenCoords(PlayerCoords, CampPoint) < CampRadius * 0.95f)
	{
		RunObjective_1 = false;
		MissionSuccess();
	}
}

void FreeRoam::TrackRewards()
{
	RewardBounty_Active = RewardBounty_Potential;
	RewardCash_Active = RewardCash_Potential;
	RewardMembers_Active = RewardMembers_Potential;
}

void FreeRoam::Update()
{
	if (TimeMissionFail > 0 && MISC::GET_GAME_TIMER() - TimeMissionFail > TimeMissionFailDelay)
	{
		DioTrace("Exit Fail Triggered");

		ExitMissionFlag = M_EXIT_FAILURE;
	}
	else if (TimeMissionSuccess > 0 && MISC::GET_GAME_TIMER() - TimeMissionSuccess > TimeMissionSuccessDelay)
	{
		DioTrace("Exit Win Triggered");

		ExitMissionFlag = M_EXIT_SUCCESS;
	}
	else if (ActivateObjective_0)
	{
		ObjectiveInit_0();
	}
	else if (ActivateObjective_1)
	{
		ObjectiveInit_1();
	}
	else if (RunObjective_0)
	{
		ObjectiveLogic_0();
	}
	else if (RunObjective_1)
	{
		ObjectiveLogic_1();
	}
}
