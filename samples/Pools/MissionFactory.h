
#pragma once
#include "Common.h"
#include "MissionBase.h"

namespace MissionFactory
{
	MissionBase* SpawnMission(MissionType missionID);

	const MissionStaticData& GetMissionStaticData(MissionType missionID);

	std::vector<MissionType> GetAvailableMissionsForCampUpgrade(int camp, int upgrade);
}
