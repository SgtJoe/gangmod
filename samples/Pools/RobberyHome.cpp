
#include "precomp.h"
#include "RobberyHome.h"

const MissionStaticData& RobberyHome::GetStaticMissionData()
{
	static MissionStaticData missiondata = {
		MissionType::ROBBERY_HOME,
		1000, 6, 3000, 6000, 0,
		"Home Invasion",
		"Rural folk always keep some money stashed on their ranches.",
		COMPCOUNT_SMALL,
		false,
		MUSIC_ODRISCOLLTHEME,
		false
	};
	return missiondata;
}

RobberyHome::RobberyHome()
{
	DioScope(DIOTAG);
}

RobberyHome::~RobberyHome()
{
	DioScope(DIOTAG);
}

void RobberyHome::Setup(int campid, const std::string& gangstr, const std::vector<MissionCompanionInfo>& companions)
{
	DioScope(DIOTAG "campid %d gangstr %s", campid, gangstr.c_str());

	GangName = gangstr;
	GangMemberTemplates = companions;

	if (!SetMissionParamsForCamp(campid))
	{
		ExitMissionFlag = M_EXIT_ERROR_TERMINATE;
		return;
	}

	CommonInit();
}

bool RobberyHome::SetMissionParamsForCamp(int camp)
{
	SetGenericLawModels(camp);

	bool success = false;

	RobberyHomeData variant1, variant2, variant3, variant4, variant5;

	switch (camp)
	{
		case DISTRICT_GRIZZLIES_WEST:
			variant1.DebugName = "Test";
			variant1.BlipPoint = {-1016.40f, 1685.84f, 241.45f};
			variant1.ExtraPropsets =
			{
				{"pg_ambient_camp_add_packwagonconvoy01", true, {-1020.88f, 1693.56f, 241.78f}, -90,
					{
						{{-1013.48f, 1690.34f, 241.29f}, {0.00f, 0.00f, 6.00f}, {3.82f, 2.52f, 5.03f}},
						{{-1009.83f, 1694.65f, 242.32f}, {0.00f, 0.00f, 12.00f}, {2.52f, 4.23f, 4.12f}}
					}
				},
				{"pg_ambient_camp_add_central_firebasin01", true, {-1013.77f, 1694.62f, 243.04f}, -90},
				{"pg_ambient_camp_add_clothesline01", true, {-1014.57f, 1684.71f, 237.19f}, 125},
				{"pg_ambient_camp_add_lamppost01", true, {-1017.84f, 1680.79f, 240.99f}, 0, {{-1022.67f, 1681.69f, 240.61f}, {0.00f, 0.00f, 57.00f}, {1.51f, 1.51f, 3.02f}}}
			};
			variant1.DefenderSpawns = {
				{{-1024.33f, 1680.85f, 241.17f}, 192},
				{{-1015.23f, 1695.19f, 242.80f}, 263},
				{{-1012.62f, 1692.53f, 242.35f}, 37},
				{{-1021.63f, 1690.35f, 244.31f}, 206}
			};
			variant1.CivilianSpawns = {
				{{-1011.82f, 1679.23f, 237.82f}, 172},
				{{-1022.58f, 1695.34f, 244.31f}, 16},
				{{-1013.14f, 1695.77f, 243.08f}, 139}
			};
			variant1.LawSpawns = {{-1084.03f, 1543.70f, 252.23f}};
			variant1.StashAmount = 4000;
			variant1.StashPos = {{-1021.98f, 1695.46f, 244.31f}, 305};

			variant2.DebugName = "Children";
			variant2.BlipPoint = {-693.74f, 1040.05f, 133.74f};
			variant2.ExtraPropsets =
			{
				{"pg_ambient_camp_add_clothesline01", true, {-688.92f, 1035.14f, 133.08f}, 120},
				{"pg_ambient_camp_add_central_firerock01", true, {-701.32f, 1042.37f, 133.34f}, 90},
				{"pg_ambient_camp_add_central_seat04", true, {-701.32f, 1042.37f, 133.14f}, 90},
				{"pg_ambient_camp_add_packwagonpacked01", true, {-694.10f, 1034.89f, 133.13f}, 150, {{{-699.29f, 1029.84f, 132.73f}, {0.00f, 0.00f, 0.00f}, {4.73f, 2.72f, 6.04f}}}}
			};
			variant2.DefenderSpawns = {
				{{-699.01f, 1040.69f, 133.49f}, 63},
				{{-698.00f, 1027.63f, 133.03f}, 192},
				{{-693.13f, 1041.59f, 133.84f}, 131},
				{{-689.60f, 1034.92f, 133.28f}, 295},
				{{-683.41f, 1039.40f, 133.17f}, 249}
			};
			variant2.CivilianSpawns = {
				{{-702.79f, 1043.55f, 133.25f}, 223},
				{{-688.59f, 1029.70f, 133.09f}, 198},
				{{-681.86f, 1037.68f, 132.96f}, 329}
			};
			variant2.LawSpawns = {{-692.10f, 1156.48f, 146.55f}, {-808.68f, 1190.67f, 155.27f}, {-619.39f, 1209.99f, 169.98f}};
			variant2.StashAmount = 4500;
			variant2.StashPos = {{-689.53f, 1041.31f, 134.00f}, 220};

			variant3.DebugName = "Mountainman";
			variant3.BlipPoint = {-1957.87f, 2159.33f, 326.00f};
			variant3.ExtraPropsets =
			{
				{"pg_ambient_camp_add_packwagonconvoy01", true, {-1960.69f, 2150.30f, 325.42f}, -70,
					{
						{{-1952.49f, 2149.91f, 325.53f}, {0.00f, 0.00f, 24.00f}, {4.12f, 2.31f, 5.53f}},
						{{-1950.89f, 2155.10f, 324.29f}, {0.00f, 0.00f, 122.75f}, {5.13f, 2.31f, 5.53f}}
					}
				},
				{"pg_ambient_camp_add_central_firebasin01", true, {-1955.26f, 2152.77f, 326.10f}, 110},
				{"pg_ambient_camp_add_snow01", true, {-1957.34f, 2165.18f, 325.88f}, 180, {{{-1952.68f, 2163.40f, 325.11f}, {0.00f, 0.00f, 36.35f}, {1.51f, 1.51f, 3.92f}}}},
				{"pg_ambient_camp_add_pelts01", true, {-1957.34f, 2160.18f, 325.92f}, 210, {{{-1951.41f, 2160.64f, 325.37f}, {0.00f, 0.00f, 0.00f}, {1.61f, 2.62f, 5.84f}}}}
			};
			variant3.DefenderSpawns = {
				{{-1948.11f, 2158.87f, 324.78f}, 245},
				{{-1952.87f, 2160.86f, 325.91f}, 285},
				{{-1960.05f, 2158.05f, 326.56f}, 274},
				{{-1953.60f, 2154.28f, 325.48f}, 121},
				{{-1958.24f, 2163.42f, 326.11f}, 327}
			};
			variant3.CivilianSpawns = {
				{{-1950.28f, 2158.20f, 325.30f}, 62},
				{{-1953.96f, 2151.49f, 325.71f}, 53},
				{{-1962.72f, 2158.23f, 326.59f}, 300}
			};
			variant3.LawSpawns = {{-1827.38f, 2201.49f, 310.62f}};
			variant3.StashAmount = 5000;
			variant3.StashPos = {{-1963.18f, 2158.32f, 326.60f}, 80};

			variant4.DebugName = "Flaco";
			variant4.BlipPoint = {-962.11f, 2180.96f, 340.00f};
			variant4.ExtraPropsets = {};
			variant4.DefenderSpawns = {
				{{-966.70f, 2191.67f, 339.88f}, 39},
				{{-967.46f, 2182.59f, 339.90f}, 105},
				{{-971.93f, 2184.84f, 339.75f}, 324},
				{{-962.64f, 2170.41f, 340.14f}, 222},
				{{-948.27f, 2172.17f, 341.22f}, 56},
				{{-945.56f, 2168.89f, 341.03f}, 53}
			};
			variant4.CivilianSpawns = {
				{{-965.98f, 2188.38f, 339.92f}, 185},
				{{-970.94f, 2186.58f, 339.82f}, 200},
				{{-959.84f, 2169.98f, 340.27f}, 105}
			};
			variant4.LawSpawns = {{-1106.98f, 2296.51f, 325.52f}};
			variant4.StashAmount = 6000;
			variant4.StashPos = {{-945.84f, 2167.46f, 341.19f}, 217};

			variant5.DebugName = "Miners";
			variant5.BlipPoint = {-1256.03f, 1178.77f, 177.03f};
			variant5.ExtraPropsets = {};
			variant5.DefenderSpawns = {
				{{-966.70f, 2191.67f, 339.88f}, 39},
				{{-967.46f, 2182.59f, 339.90f}, 105},
				{{-971.93f, 2184.84f, 339.75f}, 324},
				{{-962.64f, 2170.41f, 340.14f}, 222},
				{{-948.27f, 2172.17f, 341.22f}, 56},
				{{-945.56f, 2168.89f, 341.03f}, 53}
			};
			variant5.CivilianSpawns = {};
			variant5.LawSpawns = {{-1106.98f, 2296.51f, 325.52f}};
			variant5.StashAmount = 6000;
			variant5.StashPos = {{-945.84f, 2167.46f, 341.19f}, 217};

			AllAvailableVariants = {variant1, variant2, variant3, variant4};
			success = true;
			break;
			
		case DISTRICT_CUMBERLAND_FOREST:
			variant1.DebugName = "Carmody Dell";
			variant1.BlipPoint = {782.08f, 846.80f, 117.96f};
			variant1.DefenderSpawns = {
				{{783.67f, 849.84f, 117.59f}, 261},
				{{768.00f, 877.01f, 119.95f}, 249},
				{{769.92f, 870.84f, 119.95f}, 4},
				{{761.46f, 859.27f, 126.25f}, 27},
				{{759.27f, 839.94f, 120.08f}, 92},
				{{776.51f, 844.66f, 117.91f}, 99},
				{{770.62f, 853.46f, 117.40f}, 28}
			};
			variant1.CivilianSpawns = {
				{{776.78f, 848.10f, 120.92f}, 205},
				{{772.47f, 844.01f, 117.92f}, 106}
			};
			variant1.LawSpawns = {{871.37f, 781.99f, 111.95f}, {688.57f, 889.57f, 134.84f}};
			variant1.StashAmount = 6000;
			variant1.StashPos = {{776.08f, 849.98f, 117.91f}, 114};

			AllAvailableVariants = {variant1};
			success = true;
			break;

		case DISTRICT_BLUEGILL_MARSH:
			variant1.DebugName = "Trappers Cabin";
			variant1.BlipPoint = {2102.00f, -293.91f, 40.79f};
			variant1.DefenderSpawns = {
				{{2101.05f, -310.65f, 40.83f}, 196},
				{{2098.62f, -299.76f, 40.68f}, 243},
				{{2101.62f, -302.01f, 40.64f}, 53},
				{{2113.36f, -289.91f, 40.65f}, 182},
				{{2110.06f, -274.62f, 41.08f}, 314},
				{{2101.54f, -277.43f, 41.96f}, 6},
				{{2104.20f, -290.27f, 41.98f}, 91}
			};
			variant1.CivilianSpawns = {
				{{2101.44f, -290.36f, 41.98f}, 261},
				{{2102.97f, -280.20f, 42.00f}, 81}
			};
			variant1.LawSpawns = {{2124.20f, -387.73f, 40.52f}};
			variant1.StashAmount = 5000;
			variant1.StashPos = {{2105.98f, -292.02f, 40.81f}, 343};

			variant2.DebugName = "Houseboat";
			variant2.BlipPoint = {2313.72f, -338.33f, 41.45f};
			variant2.DefenderSpawns = {
				{{2309.26f, -331.91f, 43.53f}, 208},
				{{2307.25f, -335.68f, 40.91f}, 46},
				{{2311.62f, -326.41f, 40.91f}, 307},
				{{2310.63f, -329.23f, 43.53f}, 331}
			};
			variant2.CivilianSpawns = {
				{{2304.99f, -334.85f, 40.92f}, 259}
			};
			variant2.LawSpawns = {{2496.81f, -359.28f, 40.63f}};
			variant2.StashAmount = 4000;
			variant2.StashPos = {{2308.89f, -330.38f, 40.90f}, 56};

			variant3.DebugName = "Hagen Orchards";
			variant3.BlipPoint = {2067.65f, -851.30f, 42.36f};
			variant3.DefenderSpawns = {
				{{2069.05f, -857.65f, 42.35f}, 170},
				{{2074.00f, -852.74f, 42.35f}, 276},
				{{2068.61f, -845.99f, 42.35f}, 94},
				{{2056.88f, -852.65f, 42.35f}, 359},
				{{2057.07f, -849.00f, 42.35f}, 179},
				{{2065.20f, -821.73f, 41.80f}, 0},
				{{2046.46f, -822.41f, 41.95f}, 289},
				{{2049.36f, -821.97f, 42.06f}, 106},
				{{2071.47f, -854.25f, 42.36f}, 272} 
			};
			variant3.CivilianSpawns = {
				{{2067.02f, -854.57f, 42.36f}, 354},
				{{2070.30f, -850.63f, 42.36f}, 256},
				{{2055.71f, -821.36f, 41.95f}, 355}
			};
			variant3.LawSpawns = {{2156.63f, -958.12f, 41.59f}, {1952.35f, -928.58f, 42.00f}, {2156.63f, -958.12f, 41.59f}};
			variant3.StashAmount = 6000;
			variant3.StashPos = {{2064.54f, -851.58f, 42.36f}, 90};

			AllAvailableVariants = {variant1, variant2, variant3};
			success = true;
			break;

		case DISTRICT_RIO_BRAVO:
			variant1.DebugName = "Silent Stead";
			variant1.BlipPoint = {-5549.47f, -2407.82f, -10.18f};
			variant1.DefenderSpawns = {
				{{-5550.41f, -2403.14f, -9.74f}, 197},
				{{-5558.44f, -2394.11f, -9.92f}, 191},
				{{-5558.75f, -2390.74f, -9.77f}, 110},
				{{-5554.24f, -2392.65f, -10.05f}, 282},
				{{-5550.49f, -2397.86f, -9.75f}, 10}
			};
			variant1.CivilianSpawns = {
				{{-5551.15f, -2391.05f, -10.16f}, 124}
			};
			variant1.LawSpawns = {{-5428.35f, -2367.12f, -8.39f}};
			variant1.StashAmount = 5000;
			variant1.StashPos = {{-5555.22f, -2399.83f, -9.74f}, 114};

			variant2.DebugName = "Lake Don Julio House";
			variant2.BlipPoint = {-3431.20f, -3301.27f, -6.77f};
			variant2.DefenderSpawns = {
				{{-3427.33f, -3297.82f, -7.03f}, 76},
				{{-3405.43f, -3292.36f, -6.99f}, 212},
				{{-3404.64f, -3293.87f, -6.71f}, 38},
				{{-3400.94f, -3299.91f, -5.44f}, 3},
				{{-3393.44f, -3301.42f, -6.22f}, 235},
				{{-3390.96f, -3302.21f, -6.17f}, 66},
				{{-3393.77f, -3305.27f, -6.19f}, 340},
				{{-3393.09f, -3291.01f, -7.33f}, 20}
			};
			variant2.CivilianSpawns = {
				{{-3402.38f, -3306.38f, -5.46f}, 84},
				{{-3415.62f, -3293.32f, -7.04f}, 352}
			};
			variant2.ExtraPropsets =
			{
				{"pg_ambient_camp_add_packwagonpacked01", true, {-3416.08f, -3291.69f, -6.98f}, 268.0f}
			};
			variant2.LawSpawns = {{-3548.09f, -3252.24f, 4.96f}};
			variant2.StashAmount = 6000;
			variant2.StashPos = {{-3399.54f, -3307.45f, -5.46f}, 176};

			AllAvailableVariants = {variant2, variant2};
			success = true;
			break;
	}

	if (success)
	{
		ChosenVariant = AllAvailableVariants[rand() % AllAvailableVariants.size()];
		ActivePursuitParams.ScriptedLawSpawnPoints = ChosenVariant.LawSpawns;

		DioTrace("Chose %s", ChosenVariant.DebugName.c_str());

		ScriptsKillList = {
			MISC::GET_HASH_KEY("shack_starvingchildren"),
			MISC::GET_HASH_KEY("starvingchildren"),
			MISC::GET_HASH_KEY("carmodydell"),
			MISC::GET_HASH_KEY("orangeplantation")
		};
	}
	else
	{
		DioTrace(DIOTAG1("ERROR") "CANNOT SET MISSION %d PARAMS FOR CAMP %d - ABORTING MISSION", RobberyHome::GetStaticMissionData().StaticMissionID, camp);
	}

	return success;
}

void RobberyHome::CommonInit()
{
	const MissionStaticData& thisMissionData = RobberyHome::GetStaticMissionData();
	SetVarsFromStaticData(thisMissionData);

	ObjectivesList = {
		"Approach the ~COLOR_YELLOW~homestead",
		"Disperse the ~COLOR_RED~owners",
		"Find and steal the ~COLOR_YELLOW~money stash",
		"Lose the ~COLOR_RED~law"
	};

	if (ChosenVariant.DebugName == "Miners")
	{
		DioTrace(DIOTAG "Mining Override");
		DefenderModel = "A_M_M_ASBMINER_01";
		ObjectivesList[0] = "Approach the ~COLOR_YELLOW~location";
	}

	if (ChosenVariant.DebugName == "Carmody Dell")
	{
		SetGenericLawModels(DISTRICT_HEARTLAND);
	}

	MissionAssetsToLoad = {
		{"ANM", LootAnimDict},
		{"PED", DefenderModel},
		{"PED", CivilianModel}
	};

	AddHorseToMissionAssets(HORSE_SADDLER);

	for (ExtraProps propset : ChosenVariant.ExtraPropsets)
	{
		bool duplicate = false;
		
		for (MissionAsset asset : MissionAssetsToLoad)
		{
			if (propset.model == asset.asset)
			{
				duplicate = true;
				break;
			}
		}

		if (!duplicate)
		{
			MissionAssetsToLoad.push_back({"SET", propset.model});
		}
	}

	LoadAllMissionAssets();
	SpawnGangMembers(thisMissionData.NumMembersToSpawn);

	WAIT(2000);
}

void RobberyHome::ObjectiveInit_0()
{
	ObjectiveSet(0);
	ActivateObjective_0 = false;
	RunObjective_0 = true;

	for (ExtraProps propset : ChosenVariant.ExtraPropsets)
	{
		SpawnPropSet(propset.model, propset.pos, propset.rot);
		
		for (const NavBlocker& blocker : propset.blockers)
		{
			SpawnVolume(blocker.pos, blocker.rot, blocker.scl, true);
		}
	}

	if (ChosenVariant.DebugName == "Flaco")
	{
		const Hash FlacoIMAP = -1344255754;

		if (STREAMING::IS_IPL_ACTIVE_HASH(FlacoIMAP))
		{
			DioTrace("Flaco IMAP already active");
		}
		else
		{
			STREAMING::REQUEST_IPL_HASH(FlacoIMAP);
		}
	}

	AreaBlip = ObjectiveBlipCoords(ChosenVariant.BlipPoint, 35, true, ObjectivesList[0]);

	if (ChosenVariant.DebugName == "Miners")
	{
		for (int i = 0; i < ChosenVariant.DefenderSpawns.size(); ++i)
		{
			Ped defender = SpawnPed(DefenderModel, ChosenVariant.DefenderSpawns[i].p, ChosenVariant.DefenderSpawns[i].h, 255);

			SetPedType(defender, (i % 3 == 0) ? PedType::SHOTGUN_WEAK : PedType::SIDEARM_REGULAR);

			Defenders.emplace_back(defender);
		}
	}
	else
	{
		Ped longarmDefender = SpawnPed(DefenderModel, ChosenVariant.DefenderSpawns[0].p, ChosenVariant.DefenderSpawns[0].h, 255);
		SetPedType(longarmDefender, PedType::REPEATER_WEAK);
		Defenders.emplace_back(longarmDefender);

		for (int i = 1; i < ChosenVariant.DefenderSpawns.size(); ++i)
		{
			Ped sidearmDefender = SpawnPed(DefenderModel, ChosenVariant.DefenderSpawns[i].p, ChosenVariant.DefenderSpawns[i].h, 255);
			SetPedType(sidearmDefender, PedType::SIDEARM_WEAK);
			Defenders.emplace_back(sidearmDefender);
		}
	}

	for (Ped defender : Defenders)
	{
		PED::SET_PED_RELATIONSHIP_GROUP_HASH(defender, REL_IGNORE_EVERYTHING);
	}

	for (int i = 0; i < ChosenVariant.CivilianSpawns.size(); ++i)
	{
		Ped civilian = SpawnPed(CivilianModel, ChosenVariant.CivilianSpawns[i].p, ChosenVariant.CivilianSpawns[i].h, 255);
		PED::SET_PED_RELATIONSHIP_GROUP_HASH(civilian, REL_IGNORE_EVERYTHING);
		Common::SetPedHonorModifier(civilian, HONOR_NEGATIVE);
		Civilians.emplace_back(civilian);
	}

#ifdef DEBUG_FAST_MISSION
	ENTITY::_SET_ENTITY_COORDS_AND_HEADING(PlayerPed, ChosenVariant.LawSpawns[0],
		MISC::GET_HEADING_FROM_VECTOR_2D(ChosenVariant.BlipPoint.x - ChosenVariant.LawSpawns[0].x, ChosenVariant.BlipPoint.y - ChosenVariant.LawSpawns[0].y), true, true, true);
	ENTITY::SET_ENTITY_COORDS(PlayerPed, ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PlayerPed, 0, 40, 5), true, true, true, false);
	ENTITY::PLACE_ENTITY_ON_GROUND_PROPERLY(PlayerPed, true);
	CAM::SET_GAMEPLAY_CAM_RELATIVE_HEADING(0, 1);
	CAM::SET_GAMEPLAY_CAM_RELATIVE_PITCH(0, 1);
#endif
}

void RobberyHome::ObjectiveLogic_0()
{
	if (Common::GetDistBetweenCoords(PlayerCoords, ChosenVariant.BlipPoint) < 35)
	{
		MAP::REMOVE_BLIP(&AreaBlip);

		if (ChosenVariant.DebugName == "Miners")
		{
			DioTrace(DIOTAG "Mining Blip Override");
			Vector3 newCoords = { -1404.76f, 1149.86f, 224.26f };
			ChosenVariant.BlipPoint = newCoords;
			AreaBlip = ObjectiveBlipCoords(newCoords, 35, true, ObjectivesList[0]);
			ChosenVariant.DebugName = "MinersOverrideDone";
			return;
		}

		RunObjective_0 = false;
		ActivateObjective_1 = true;
	}
}

void RobberyHome::ObjectiveInit_1()
{
	ObjectiveSet(1);
	ActivateObjective_1 = false;
	RunObjective_1 = true;

	DisableLaw();
	
	CheckAbandon = true;
	AbandonCoords = ChosenVariant.BlipPoint;
	AbandonRadius = 40.0f;

	for (Ped defender : Defenders)
	{
		MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_ENEMY, defender);
		PED::SET_PED_RELATIONSHIP_GROUP_HASH(defender, REL_PLAYER_ENEMY);
		TASK::TASK_COMBAT_HATED_TARGETS(defender, 50.0f);
		PED::SET_PED_CONFIG_FLAG(defender, PCF_DisableLadderClimbing, true);
		//ENTITY::SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(defender, true);
	}
	
	CompanionsBeginCombat();

	for (Ped civ : Civilians)
	{
		PED::SET_PED_RELATIONSHIP_GROUP_HASH(civ, REL_CIV);
		TASK::TASK_FLEE_PED(civ, PlayerPed, 4, 0, -1082130432 /* Float: -1f */, -1, 0);
	}

	if (!Civilians.empty())
	{
		ScriptedSpeechParams params{"LAW_HAIL", 0, 0, MISC::GET_HASH_KEY("SPEECH_PARAMS_BEAT_SHOUTED_CLEAR_NOSUB"), 0, true, 0, 0 };
		AUDIO::PLAY_PED_AMBIENT_SPEECH_NATIVE(Civilians[0], (Any*)&params);
	}

	CurrentMusicStage = STAGE_ACTION1;
}

void RobberyHome::ObjectiveLogic_1()
{
	for (Ped defender : Defenders)
	{
		if (PED::GET_PED_CONFIG_FLAG(defender, PCF_DisableLadderClimbing, true) && PED::IS_PED_DEAD_OR_DYING(defender, true))
		{
			DioTrace("Defender down");
			PED::SET_PED_CONFIG_FLAG(defender, PCF_DisableLadderClimbing, false);
			defenderCasualties++;
		}
	}
	
	int lawThreshhold = int(Defenders.size() * 0.5f);

	if (!LAW::IS_LAW_INCIDENT_ACTIVE(0) && defenderCasualties >= lawThreshhold)
	{
		DioTrace("Defenders casualties reached law threshold of %d/%d", lawThreshhold, Defenders.size());
		
		CurrentMusicStage = STAGE_ACTION2;

		AllowWanted = true;
		BecomeWanted(CRIME_MURDER, 3);
		SetDispatchService(false);
		ActivePursuitParams.DispatchResetDelay = -1;
		ActivePursuitParams.CompanionCombatOnSpotted = true;

		CreateScriptedRespondingLawPed(GenericLawLeader, PedType::SIDEARM_REGULAR, HORSE_SADDLER, ActivePursuitParams.ScriptedLawSpawnPoints[0], ChosenVariant.BlipPoint);
		CreateScriptedRespondingLawPed(GenericLawFollower, PedType::REPEATER_WEAK, HORSE_SADDLER, ActivePursuitParams.ScriptedLawSpawnPoints[0], ChosenVariant.BlipPoint);
		CreateScriptedRespondingLawPed(GenericLawFollower, PedType::SHOTGUN_WEAK, HORSE_SADDLER, ActivePursuitParams.ScriptedLawSpawnPoints[0], ChosenVariant.BlipPoint);
	}

	WantedLogic();

	int defenderRetreatThreshhold = (Defenders.size() <= 6) ? int(Defenders.size() - 1) : int(Defenders.size() * 0.75f);

	if (defenderCasualties >= defenderRetreatThreshhold)
	{
		DioTrace("Defenders casualties reached retreat threshold of %d/%d", defenderRetreatThreshhold, Defenders.size());
		RunObjective_1 = false;
		ActivateObjective_2 = true;
	}
}

void RobberyHome::ObjectiveInit_2()
{
	ObjectiveSet(2);
	ActivateObjective_2 = false;
	RunObjective_2 = true;

	if (PlayerIncognito)
	{
		CompanionsClearTasks();
		CompanionsRegroup();

		SetCompanionPlayerCollision(false);

		PlayCompanionSpeech("CALLOUT_FORGET_FLEEING_NEUTRAL");
	}

	for (Ped defender : Defenders)
	{
		PED::SET_PED_RELATIONSHIP_GROUP_HASH(defender, REL_CIV);
		Common::SetPedHonorModifier(defender, HONOR_NEGATIVE); 
		
		if (!PED::IS_PED_DEAD_OR_DYING(defender, true))
		{
			Blip dblip = MAP::GET_BLIP_FROM_ENTITY(defender);
			MAP::REMOVE_BLIP(&dblip);

			TASK::CLEAR_PED_TASKS(defender, true, true);
			TASK::TASK_FLEE_PED(defender, PlayerPed, 4, 0, -1082130432 /* Float: -1f */, -1, 0);
		}
	}

	AreaBlip = ObjectiveBlipCoords(ChosenVariant.BlipPoint, 20, false, ObjectivesList[2]);

	LootPrompt = HUD::_UIPROMPT_REGISTER_BEGIN();
	HUD::_UIPROMPT_SET_CONTROL_ACTION(LootPrompt, MISC::GET_HASH_KEY("INPUT_LOOT"));
	HUD::_UIPROMPT_SET_TEXT(LootPrompt, MISC::VAR_STRING(10, "LITERAL_STRING", "Take Stash"));
	HUD::_UIPROMPT_SET_STANDARDIZED_HOLD_MODE(LootPrompt, 1);
	HUD::_UIPROMPT_REGISTER_END(LootPrompt);
	HUD::_UIPROMPT_SET_ENABLED(LootPrompt, false);
	HUD::_UIPROMPT_SET_VISIBLE(LootPrompt, false);
}

void RobberyHome::ObjectiveLogic_2()
{
	WantedLogic();

	if (CompanionsInCombat && ActivePursuitParams.AllScriptedPedsKilled)
	{
		CompanionsClearTasks();
		CompanionsDisband();

		PlayCompanionSpeech("WON_FIGHT");

		CurrentMusicStage = STAGE_ACTION1;
	}

	if (BUILTIN::VDIST2(PlayerCoords.x, PlayerCoords.y, PlayerCoords.z, ChosenVariant.StashPos.p.x, ChosenVariant.StashPos.p.y, ChosenVariant.StashPos.p.z) < 2.0)
	{
		HUD::_UIPROMPT_SET_VISIBLE(LootPrompt, true);
		HUD::_UIPROMPT_SET_ENABLED(LootPrompt, true);
	}
	else
	{
		HUD::_UIPROMPT_SET_VISIBLE(LootPrompt, false);
		HUD::_UIPROMPT_SET_ENABLED(LootPrompt, false);
	}

	if (HUD::_UIPROMPT_HAS_HOLD_MODE_COMPLETED(LootPrompt))
	{
		HUD::_UIPROMPT_SET_VISIBLE(LootPrompt, false);
		HUD::_UIPROMPT_SET_ENABLED(LootPrompt, false);

		TASK::CLEAR_PED_TASKS(PlayerPed, true, true);

		Vector3 slideCoord = ChosenVariant.StashPos.p;

		WEAPON::SET_CURRENT_PED_WEAPON(PlayerPed, WEAPON_UNARMED, true, WEAPON_ATTACH_POINT_HAND_PRIMARY, false, false);

		int sequence = 0;
		TASK::OPEN_SEQUENCE_TASK(&sequence);

		TASK::TASK_PED_SLIDE_TO_COORD(0, slideCoord.x, slideCoord.y, slideCoord.z, ChosenVariant.StashPos.h, 2.0f);
		TASK::TASK_PLAY_ANIM(0, LootAnimDict.c_str(), LootAnim.c_str(), 1.0, 1.0f, -1, 0, 0, 0, 0, 0, 0, 0);
		TASK::TASK_PAUSE(0, 1);

		TASK::CLOSE_SEQUENCE_TASK(sequence);
		TASK::TASK_PERFORM_SEQUENCE(PlayerPed, sequence);
		TASK::CLEAR_SEQUENCE_TASK(&sequence);
		
		MAP::REMOVE_BLIP(&AreaBlip);

		RunObjective_2 = false;
		ActivateObjective_3 = true;
	}
}

void RobberyHome::ObjectiveInit_3()
{
	ObjectiveSet(3);
	ActivateObjective_3 = false;
	RunObjective_3 = true;

	CheckAbandon = false;

	PlayCompanionSpeech("GANG_CALLOUT_LAW_SPOTTED");

	AllowCompanionManualControl = true;
	
	if (ActivePursuitParams.ScriptedLawSpawnPoints.size() > 1)
	{
		DioTrace("Spawning backup law at all %d secondary points", ActivePursuitParams.ScriptedLawSpawnPoints.size() - 1);
		for (int i = 1; i < ActivePursuitParams.ScriptedLawSpawnPoints.size(); ++i)
		{
			CreateScriptedRespondingLawPed(GenericLawLeader, PedType::SIDEARM_REGULAR, HORSE_SADDLER, ActivePursuitParams.ScriptedLawSpawnPoints[i], ChosenVariant.BlipPoint);
			for (int j = 0; j < 2; ++j)
			{
				CreateScriptedRespondingLawPed(GenericLawFollower, PedType::REPEATER_REGULAR, HORSE_SADDLER, ActivePursuitParams.ScriptedLawSpawnPoints[i], ChosenVariant.BlipPoint);
			}
		}
	}
	else
	{
		DioTrace("Spawning backup law at single point");
		CreateScriptedRespondingLawPed(GenericLawLeader, PedType::SIDEARM_REGULAR, HORSE_SADDLER, ActivePursuitParams.ScriptedLawSpawnPoints[0], ChosenVariant.BlipPoint);
		for (int j = 0; j < 3; ++j)
		{
			CreateScriptedRespondingLawPed(GenericLawFollower, PedType::REPEATER_REGULAR, HORSE_SADDLER, ActivePursuitParams.ScriptedLawSpawnPoints[0], ChosenVariant.BlipPoint);
		}
		CreateScriptedRespondingLawPed(GenericLawFollower, PedType::SNIPER_WEAK, HORSE_SADDLER, ActivePursuitParams.ScriptedLawSpawnPoints[0], ChosenVariant.BlipPoint);
	}

	CurrentMusicStage = STAGE_ACTION1;

	ActivePursuitParams.MusicStageOnSpotted = STAGE_ACTION2;
	ActivePursuitParams.AllScriptedPedsKilled = false;
	PlayerIncognito = true;
	PlayerPursuit = false;
}

void RobberyHome::ObjectiveLogic_3()
{
	if (TASK::GET_SEQUENCE_PROGRESS(PlayerPed) == 2)
	{
		CompanionsRegroup();

		SetCompanionPlayerCollision(false);
	}
	
	if (ActivePursuitParams.AllScriptedPedsKilled)
	{
		ClearWanted(false);
	}

	if (!WantedLogic())
	{
		CurrentMusicStage = STAGE_AFTERACTION;
		UpdateMusic();

		RunObjective_3 = false;
		MissionSuccess();
	}
}

void RobberyHome::TrackRewards()
{
	RewardBounty_Active = PlayerIncognito ? int(RewardBounty_Potential * 0.25f) : RewardBounty_Potential;
	
	RewardMembers_Active = RewardMembers_Potential;
	
	RewardCash_Active = ChosenVariant.StashAmount;
}

void RobberyHome::Update()
{
	if (ChosenVariant.DebugName == "Children")
	{
		Ped peds[1024];
		int count = worldGetAllPeds(peds, 1024);

		for (int i = 0; i < count; i++)
		{
			Ped p = peds[i];

			if (PED::IS_PED_MODEL(peds[i], MISC::GET_HASH_KEY("U_M_Y_SHACKSTARVINGKID_01")))
			{
				DioTrace("Removing stray child");
				ENTITY::SET_ENTITY_AS_MISSION_ENTITY(p, true, true);
				PED::DELETE_PED(&p);
				break;
			}
		}
	}
	
	if (TimeMissionFail > 0 && MISC::GET_GAME_TIMER() - TimeMissionFail > TimeMissionFailDelay)
	{
		DioTrace("Exit Fail Triggered");

		ExitMissionFlag = M_EXIT_FAILURE;
	}
	else if (TimeMissionSuccess > 0 && MISC::GET_GAME_TIMER() - TimeMissionSuccess > TimeMissionSuccessDelay)
	{
		DioTrace("Exit Win Triggered");

		ExitMissionFlag = M_EXIT_SUCCESS;
	}
	else if (ActivateObjective_0)
	{
		ObjectiveInit_0();
	}
	else if (ActivateObjective_1)
	{
		ObjectiveInit_1();
	}
	else if (ActivateObjective_2)
	{
		ObjectiveInit_2();
	}
	else if (ActivateObjective_3)
	{
		ObjectiveInit_3();
	}
	else if (RunObjective_0)
	{
		ObjectiveLogic_0();
	}
	else if (RunObjective_1)
	{
		ObjectiveLogic_1();
	}
	else if (RunObjective_2)
	{
		ObjectiveLogic_2();
	}
	else if (RunObjective_3)
	{
		ObjectiveLogic_3();
	}
}
