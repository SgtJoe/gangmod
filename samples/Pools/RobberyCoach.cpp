
#include "precomp.h"
#include "RobberyCoach.h"

const MissionStaticData& RobberyCoach::GetStaticMissionData()
{
	static MissionStaticData missiondata = {
		MissionType::ROBBERY_COACH,
		0, 0, 1000, 3500, 0, 
		"Coach Robbery", 
		"Stagecoaches always carry good loot.\nRich passengers, valuable goods, or even cold hard cash!", 
		COMPCOUNT_MIN, 
		false, 
		MUSIC_DEFAULT,
		false
	};
	return missiondata;
}

RobberyCoach::RobberyCoach()
{
	DioScope(DIOTAG);
}

RobberyCoach::~RobberyCoach()
{
	DioScope(DIOTAG);
}

void RobberyCoach::Setup(int campid, const std::string& gangstr, const std::vector<MissionCompanionInfo>& companions)
{
	DioScope(DIOTAG "campid %d gangstr %s", campid, gangstr.c_str());

	GangName = gangstr;
	GangMemberTemplates = companions;

	if (!SetMissionParamsForCamp(campid))
	{
		ExitMissionFlag = M_EXIT_ERROR_TERMINATE;
		return;
	}

	CommonInit();
}

RobberyCoach::RobberyCoachType RobberyCoach::ChooseCoachType()
{
	RobberyCoachType chosenType = RobberyCoachType::Cargo;
	std::string chosenTypeStr = "Cargo";

	int random = rand() % 100;

	if (random <= 20)
	{
		chosenType = RobberyCoachType::BankingTrap;
		chosenTypeStr = "Banking Trap";
	}
	else if (random <= 45)
	{
		chosenType = RobberyCoachType::BankingLegit;
		chosenTypeStr = "Banking Legit";
	}
	else if (random <= 70)
	{
		chosenType = RobberyCoachType::Passengers;
		chosenTypeStr = "Passengers";
	}

	DioTrace("Random: %d Coach Type %s", random, chosenTypeStr.c_str());

	return chosenType;
}

bool RobberyCoach::SetMissionParamsForCamp(int camp)
{
	bool success = false;

	RobberyCoachData variant1, variant2, variant3;

	switch (camp)
	{
		case DISTRICT_GRIZZLIES_WEST:
			variant1.AmbushPoint = {-677.45f, 1163.64f, 150.99f};
			variant1.WagonSpawn = {{-613.51f, 1216.27f, 171.18f}, 136};
			variant1.WagonDestination = {-714.53f, 1154.75f, 146.49f};
			variant1.LawSpawns = {{-499.07f, 1281.44f, 169.69f}, {-705.24f, 1056.87f, 133.35f}};

			variant2.AmbushPoint = {-153.70f, 1812.98f, 208.27f};
			variant2.WagonSpawn = {{-79.46f, 1836.51f, 208.17f}, 111};
			variant2.WagonDestination = {-142.61f, 1790.10f, 198.43f};
			variant2.LawSpawns = {{-1.55f, 1865.69f, 190.30f}, {-226.61f, 1750.52f, 195.10f}};

			variant3.AmbushPoint = {-959.09f, 876.42f, 131.57f};
			variant3.WagonSpawn = {{-913.17f, 879.81f, 136.89f}, 108};
			variant3.WagonDestination = {-997.29f, 863.66f, 121.15f};
			variant3.LawSpawns = {{-857.47f, 957.99f, 142.59f}, {-1100.18f, 847.94f, 118.84f}};

			AllAvailableVariants = {variant1, variant2, variant3};
			success = true;
			break;

		case DISTRICT_CUMBERLAND_FOREST:
			variant1.AmbushPoint = {658.39f, 1534.00f, 179.16f};
			variant1.WagonSpawn = {{663.44f, 1467.43f, 181.18f}, 10};
			variant1.WagonDestination = {635.74f, 1576.12f, 185.07f};
			variant1.LawSpawns = {{668.70f, 1410.62f, 180.36f}, {708.47f, 1418.23f, 177.04f}};

			variant2.AmbushPoint = {56.87f, 1099.99f, 176.52f};
			variant2.WagonSpawn = {{106.59f, 1094.14f, 179.00f}, 90};
			variant2.WagonDestination = {9.52f, 1096.87f, 173.10f};
			variant2.LawSpawns = {{167.51f, 1119.00f, 174.35f}, {-66.00f, 1134.33f, 162.15f}};

			AllAvailableVariants = {variant1, variant2};
			success = true;
			break;

		case DISTRICT_BLUEGILL_MARSH:
			variant1.AmbushPoint = {1667.94f, -767.46f, 41.20f};
			variant1.WagonSpawn = {{1671.40f, -713.97f, 40.64f}, 222};
			variant1.WagonDestination = {1722.15f, -766.28f, 41.34f};
			variant1.LawSpawns = {{1610.79f, -642.09f, 45.70f}, {1787.76f, -886.55f, 40.63f}};

			variant2.AmbushPoint = {2354.15f, -1107.00f, 45.26f};
			variant2.WagonSpawn = {{2403.81f, -1133.52f, 45.74f}, 83};
			variant2.WagonDestination = {2320.44f, -1086.02f, 42.96f};
			variant2.LawSpawns = {{2409.96f, -1207.78f, 44.66f}, {2313.13f, -1175.22f, 42.32f}};

			variant3.AmbushPoint = {2036.78f, -1235.40f, 42.15f};
			variant3.WagonSpawn = {{2065.74f, -1198.51f, 43.08f}, 182};
			variant3.WagonDestination = {2066.27f, -1310.06f, 41.17f};
			variant3.LawSpawns = {{2136.28f, -1319.21f, 41.51f}, {2063.12f, -1145.85f, 40.66f}};

			AllAvailableVariants = {variant1, variant2, variant3};
			success = true;
			break;

		case DISTRICT_RIO_BRAVO:
			variant1.AmbushPoint = {-5201.97f, -2686.07f, 3.20f};
			variant1.WagonSpawn = {{-5228.92f, -2631.08f, -11.09f}, 241};
			variant1.WagonDestination = {-5146.05f, -2675.43f, -8.79f};
			variant1.LawSpawns = {{-5304.62f, -2614.12f, -11.13f}, {-5142.17f, -2700.08f, -8.44f}};

			variant2.AmbushPoint = {-4285.90f, -2506.82f, 8.04f};
			variant2.WagonSpawn = {{-4249.79f, -2574.41f, 12.83f}, 16};
			variant2.WagonDestination = {-4294.11f, -2456.56f, 7.95f};
			variant2.LawSpawns = {{-4251.36f, -2639.27f, 9.37f}, {-4302.22f, -2389.22f, 11.93f}};

			AllAvailableVariants = {variant1, variant2};
			success = true;
			break;
	}

	if (success)
	{
		ChosenVariant = AllAvailableVariants[rand() % AllAvailableVariants.size()];
		ActivePursuitParams.ScriptedLawSpawnPoints = ChosenVariant.LawSpawns;

		SetGenericLawModels(Common::GetDistrictFromCoords(ChosenVariant.LawSpawns[0]));

		ChosenVariant.CoachType = ChooseCoachType();
	}
	else
	{
		DioTrace(DIOTAG1("ERROR") "CANNOT SET MISSION %d PARAMS FOR CAMP %d - ABORTING MISSION", RobberyCoach::GetStaticMissionData().StaticMissionID, camp);
	}

	return success;
}

void RobberyCoach::CommonInit()
{
	const MissionStaticData& thisMissionData = RobberyCoach::GetStaticMissionData();
	SetVarsFromStaticData(thisMissionData);

	if (ChosenVariant.CoachType == RobberyCoachType::BankingTrap ||
		ChosenVariant.CoachType == RobberyCoachType::BankingLegit)
	{
		WagonModel = "STAGECOACH004X";
		WagonExtraPropset = "PG_MISSION_UTP2_COACHLOCKBOX";
	}

	MissionAssetsToLoad = {
		{"VEH", WagonModel},
		{"SET", WagonExtraPropset},
		{"ANM", WagonManualLootAnimDict},
		{"PED", WagonGuardModel},
		{"PED", MalePassengerModel},
		{"PED", FemalePassengerModel},
		{"PED", "S_M_M_MARSHALLSRURAL_01"}
	};

	AddHorseToMissionAssets(HORSE_SADDLER);
	AddHorseToMissionAssets(HORSE_MUSTANG);

	ObjectivesList = {
		"Go to the ~COLOR_YELLOW~location",
		"Eliminate the ~COLOR_RED~coach guards",
		"PLACEHOLDER",
		"Lose the ~COLOR_RED~law"
	};

	switch (ChosenVariant.CoachType)
	{
		case RobberyCoachType::BankingTrap:
			ObjectivesList[2] = "Eliminate the ~COLOR_RED~US Marshals";
			ObjectivesList[3] = "Loot the ~COLOR_YELLOW~coach";
			break;

		case RobberyCoachType::BankingLegit:
			ObjectivesList[2] = "Loot the ~COLOR_YELLOW~coach";
			break;

		case RobberyCoachType::Passengers:
			ObjectivesList[2] = "Tie or kill then ~COLOR_YELLOW~loot~COLOR_WHITE~ the passengers";
			break;

		default:
			ObjectivesList[2] = "Open and loot the ~COLOR_YELLOW~coach lockbox";
	}

	LoadAllMissionAssets();
	SpawnGangMembers(thisMissionData.NumMembersToSpawn);

	WAIT(2000);
}

void RobberyCoach::ObjectiveInit_0()
{
	ObjectiveSet(0);
	ActivateObjective_0 = false;
	RunObjective_0 = true;
	
	AmbushBlip = ObjectiveBlipCoords(ChosenVariant.AmbushPoint, 5, true, ObjectivesList[0]);

#ifdef DEBUG_FAST_MISSION
	ENTITY::_SET_ENTITY_COORDS_AND_HEADING(PlayerPed, ChosenVariant.AmbushPoint.x, ChosenVariant.AmbushPoint.y - 10, ChosenVariant.AmbushPoint.z, 0, true, true, true);
	ENTITY::PLACE_ENTITY_ON_GROUND_PROPERLY(PlayerPed, true);
	CAM::SET_GAMEPLAY_CAM_RELATIVE_HEADING(0, 1);
	CAM::SET_GAMEPLAY_CAM_RELATIVE_PITCH(0, 1);
#endif
}

void RobberyCoach::ObjectiveLogic_0()
{
	if (Common::GetDistBetweenCoords(PlayerCoords, ChosenVariant.AmbushPoint) < 5.0f)
	{
		MAP::REMOVE_BLIP(&AmbushBlip);

		RunObjective_0 = false;
		ActivateObjective_1 = true;
	}
}

void RobberyCoach::ObjectiveInit_1()
{
	ObjectiveSet(1);
	ActivateObjective_1 = false;
	RunObjective_1 = true;

	DisableLaw();

	AllowCompanionManualControl = true;

	PlayCompanionSpeech("ARRIVAL_ENTER_TRAP");

	Common::ClearArea(ChosenVariant.WagonSpawn.p, 100);

	Wagon = SpawnVehicle(WagonModel, ChosenVariant.WagonSpawn.p, ChosenVariant.WagonSpawn.h);

	VEHICLE::SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(Wagon, false);
	VEHICLE::SET_BREAKABLE_VEHICLE_LOCKS_UNBREAKABLE(Wagon, true);
	Common::SetEntityProofs(Wagon, true, true);

	TASK::_SET_SCENARIO_TYPE_ENABLED_HASH(MISC::GET_HASH_KEY("RANSACK_COACH_BOOT"), false);
	TASK::_SET_SCENARIO_TYPE_ENABLED_HASH(MISC::GET_HASH_KEY("RANSACK_COACH_BOOT_WIP"), false);
	
	if (!Common::IsDarkOut())
	{
		PROPSET::_DELETE_PROP_SET(PROPSET::_GET_VEHICLE_LIGHT_PROP_SET(Wagon), true, true);
	}

	if (ChosenVariant.CoachType == RobberyCoachType::Passengers)
	{
		VEHICLE::_SET_FORCE_COACH_ROBBERY_LOOT(Wagon, MISC::GET_HASH_KEY("COACH2_MARY3"));
		
		PROPSET::_ADD_PROP_SET_FOR_VEHICLE(Wagon, MISC::GET_HASH_KEY(WagonExtraPropset.c_str()));
	}
	
	if (ChosenVariant.CoachType == RobberyCoachType::BankingLegit ||
		ChosenVariant.CoachType == RobberyCoachType::BankingTrap)
	{
		PROPSET::_ADD_PROP_SET_FOR_VEHICLE(Wagon, MISC::GET_HASH_KEY(WagonExtraPropset.c_str()));
	}
	
	WagonDriver = SpawnPed(WagonGuardModel, Common::ApplyRandomSmallOffsetToCoords(ChosenVariant.WagonSpawn.p, 2.0f), 0, 255);
	MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_ENEMY, WagonDriver);
	PED::SET_PED_RELATIONSHIP_GROUP_HASH(WagonDriver, REL_COP);
	SetPedType(WagonDriver, PedType::SIDEARM_REGULAR);
	PED::SET_PED_INTO_VEHICLE(WagonDriver, Wagon, SEAT_DRIVER);
	TASK::TASK_VEHICLE_GOTO_NAVMESH(WagonDriver, Wagon, ChosenVariant.WagonDestination.x, ChosenVariant.WagonDestination.y, ChosenVariant.WagonDestination.z, 3.0f, 0, 0);
	PED::SET_PED_KEEP_TASK(WagonDriver, true);

	if (ChosenVariant.CoachType == RobberyCoachType::Passengers ||
		ChosenVariant.CoachType == RobberyCoachType::BankingLegit ||
		ChosenVariant.CoachType == RobberyCoachType::BankingTrap)
	{
		Ped WagonPassgr = SpawnPed(WagonGuardModel, Common::ApplyRandomSmallOffsetToCoords(ChosenVariant.WagonSpawn.p, 2.0f), 0, 255);
		MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_ENEMY, WagonPassgr);
		PED::SET_PED_RELATIONSHIP_GROUP_HASH(WagonPassgr, REL_COP);
		SetPedType(WagonPassgr, PedType::SHOTGUN_WEAK);
		PED::SET_PED_INTO_VEHICLE(WagonPassgr, Wagon, SEAT_FRONT_RIGHT);
		WagonGuards.emplace_back(WagonPassgr);
	}

	std::vector<std::string> guardHorses = InventoryManager::GetStaticHorseData(HORSE_SADDLER).PedModels;

	if (ChosenVariant.CoachType == RobberyCoachType::BankingLegit/* ||
		ChosenVariant.CoachType == RobberyCoachType::BankingTrap*/)
	{
		Ped WagonGuard1 = SpawnPed(WagonGuardModel, Common::ApplyRandomSmallOffsetToCoords(ChosenVariant.WagonSpawn.p, 2.0f), 0, 255);
		Ped WagonGuard2 = SpawnPed(WagonGuardModel, Common::ApplyRandomSmallOffsetToCoords(ChosenVariant.WagonSpawn.p, 2.0f), 0, 255);
		Ped WagonGuard3 = SpawnPed(WagonGuardModel, Common::ApplyRandomSmallOffsetToCoords(ChosenVariant.WagonSpawn.p, 2.0f), 0, 255);
		Ped GuardHorse1 = SpawnPed(guardHorses[rand() % guardHorses.size()], ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Wagon, 4, 0, 2), ChosenVariant.WagonSpawn.h, 255);
		Ped GuardHorse2 = SpawnPed(guardHorses[rand() % guardHorses.size()], ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Wagon, -4, 0, 2), ChosenVariant.WagonSpawn.h, 255);
		Ped GuardHorse3 = SpawnPed(guardHorses[rand() % guardHorses.size()], ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Wagon, 0, -6, 2), ChosenVariant.WagonSpawn.h, 255);
		
		MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_ENEMY, WagonGuard1);
		MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_ENEMY, WagonGuard2);
		MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_ENEMY, WagonGuard3);

		DefaultHorseSetup(GuardHorse1);
		DefaultHorseSetup(GuardHorse2);
		DefaultHorseSetup(GuardHorse3);
		PED::SET_PED_CONFIG_FLAG(GuardHorse1, PCF_BlockMountHorsePrompt, true);
		PED::SET_PED_CONFIG_FLAG(GuardHorse2, PCF_BlockMountHorsePrompt, true);
		PED::SET_PED_CONFIG_FLAG(GuardHorse3, PCF_BlockMountHorsePrompt, true);

		PED::SET_PED_RELATIONSHIP_GROUP_HASH(WagonGuard1, REL_COP);
		PED::SET_PED_RELATIONSHIP_GROUP_HASH(WagonGuard2, REL_COP);
		PED::SET_PED_RELATIONSHIP_GROUP_HASH(WagonGuard3, REL_COP);

		SetPedType(WagonGuard1, PedType::REPEATER_WEAK);
		SetPedType(WagonGuard2, PedType::REPEATER_WEAK);
		SetPedType(WagonGuard3, PedType::REPEATER_WEAK);

		PED::SET_PED_ONTO_MOUNT(WagonGuard1, GuardHorse1, -1, true);
		PED::SET_PED_ONTO_MOUNT(WagonGuard2, GuardHorse2, -1, true);
		PED::SET_PED_ONTO_MOUNT(WagonGuard3, GuardHorse3, -1, true);

		WagonGuards.emplace_back(WagonGuard1);
		WagonGuards.emplace_back(WagonGuard2);
		WagonGuards.emplace_back(WagonGuard3);

		TASK::TASK_FOLLOW_TO_OFFSET_OF_ENTITY(GuardHorse1, Wagon, {-5, 0, 0}, 1.0f, -1, 3, 1, 1, 0, 0, 1, false);
		TASK::TASK_FOLLOW_TO_OFFSET_OF_ENTITY(GuardHorse2, Wagon, {5, 0, 0 }, 1.0f, -1, 3, 1, 1, 0, 0, 1, false);
		TASK::TASK_FOLLOW_TO_OFFSET_OF_ENTITY(GuardHorse3, Wagon, {0, -6, 0 }, 1.0f, -1, 3, 1, 1, 0, 0, 1, false);
	}

	if (ChosenVariant.CoachType == RobberyCoachType::Passengers)
	{
		PassengerM = SpawnPed(MalePassengerModel, Common::ApplyRandomSmallOffsetToCoords(ChosenVariant.WagonSpawn.p, 2.0f), 0, -1);
		PassengerF = SpawnPed(FemalePassengerModel, Common::ApplyRandomSmallOffsetToCoords(ChosenVariant.WagonSpawn.p, 2.0f), 0, -1);

		PED::SET_PED_RELATIONSHIP_GROUP_HASH(PassengerM, REL_CIV);
		PED::SET_PED_RELATIONSHIP_GROUP_HASH(PassengerF, REL_CIV);

		PED::SET_PED_INTO_VEHICLE(PassengerM, Wagon, SEAT_ANY_PASSENGER);
		PED::SET_PED_INTO_VEHICLE(PassengerF, Wagon, SEAT_ANY_PASSENGER);

		Common::SetEntityProofs(PassengerM, true, false);
		Common::SetEntityProofs(PassengerF, true, false);
	}
}

void RobberyCoach::ObjectiveLogic_1()
{
	bool onlyDriver = ChosenVariant.CoachType == RobberyCoachType::Cargo;
	
	if (ENTITY::DOES_ENTITY_EXIST(PassengerM) && ENTITY::DOES_ENTITY_EXIST(PassengerF))
	{
		TASK::CLEAR_PED_TASKS(PassengerM, true, true);
		TASK::CLEAR_PED_TASKS(PassengerF, true, true);
	}

	if (!StartedWagonAttack)
	{
		bool wagonThreatened = false;

		if (PED::IS_PED_IN_COMBAT(WagonDriver, PlayerPed) ||
			(PLAYER::IS_PLAYER_FREE_AIMING(0) && PED::CAN_PED_SEE_ENTITY(WagonDriver, PlayerPed, true, true) == 1) ||
			PED::IS_PED_DEAD_OR_DYING(WagonDriver, true))
		{
			wagonThreatened = true;
		}

		for (int i = 0; i < WagonGuards.size(); i++)
		{
			if (PED::IS_PED_IN_COMBAT(WagonGuards[i], PlayerPed) ||
				(PLAYER::IS_PLAYER_FREE_AIMING(0) && PED::CAN_PED_SEE_ENTITY(WagonGuards[i], PlayerPed, true, true) == 1) ||
				PED::IS_PED_DEAD_OR_DYING(WagonGuards[i], true))
			{
				wagonThreatened = true;
			}
		}

		if (wagonThreatened || CompanionManualControlStatus == CompanionControlStatus::ATTACKING)
		{
			DioTrace("Starting wagon attack");
			StartedWagonAttack = true;

			CurrentMusicStage = STAGE_ACTION1;

			if (onlyDriver)
			{
				PED::SET_PED_RELATIONSHIP_GROUP_HASH(WagonDriver, REL_PLAYER_ENEMY);
				TASK::CLEAR_PED_TASKS(WagonDriver, true, true);
				TASK::TASK_COMBAT_PED(WagonDriver, PlayerPed, 0, 0);
			}

			for (int i = 0; i < WagonGuards.size(); i++)
			{
				TASK::CLEAR_PED_TASKS(PED::GET_MOUNT(WagonGuards[i]), true, true);
				TASK::CLEAR_PED_TASKS(WagonGuards[i], true, true);

				PED::SET_PED_RELATIONSHIP_GROUP_HASH(WagonGuards[i], REL_PLAYER_ENEMY);

				int sequence = 0;
				TASK::OPEN_SEQUENCE_TASK(&sequence);

				if (PED::IS_PED_ON_MOUNT(WagonGuards[i]))
				{
					TASK::TASK_DISMOUNT_ANIMAL(0, 0, 0, 0, 0, 0);
				}
				TASK::TASK_COMBAT_PED(0, PlayerPed, 0, 0);

				TASK::CLOSE_SEQUENCE_TASK(sequence);
				TASK::TASK_PERFORM_SEQUENCE(WagonGuards[i], sequence);
				TASK::CLEAR_SEQUENCE_TASK(&sequence);
			}

			AllowCompanionManualControl = false;

			if (CompanionManualControlStatus == CompanionControlStatus::FOLLOWING)
			{
				CompanionsBeginCombat();
			}
#ifdef DEBUG_FAST_MISSION
			//for (int i = 0; i < WagonGuards.size(); i++)
			//{
			//	PED::EXPLODE_PED_HEAD(WagonGuards[i], WEAPON_REVOLVER_CATTLEMAN);
			//}
#endif
			if (ChosenVariant.CoachType == RobberyCoachType::BankingTrap)
			{
				DioTrace("Trap has sprung");

				VEHICLE::BRING_VEHICLE_TO_HALT(Wagon, 0, -1, true);

				int sequence = 0;
				TASK::OPEN_SEQUENCE_TASK(&sequence);
				TASK::TASK_DISMOUNT_ANIMAL(0, 0, 0, 0, 0, 0);
				TASK::TASK_COMBAT_PED(0, PlayerPed, 0, 0);
				TASK::CLOSE_SEQUENCE_TASK(sequence);

				TASK::TASK_PERFORM_SEQUENCE(WagonDriver, sequence);

				for (int i = 0; i < WagonGuards.size(); i++)
				{
					TASK::TASK_PERFORM_SEQUENCE(WagonGuards[i], sequence);
				}

				TASK::CLEAR_SEQUENCE_TASK(&sequence);

				RunObjective_1 = false;
				ActivateObjective_2 = true;
				return;
			}
		}
	}

	if (PED::IS_PED_DEAD_OR_DYING(WagonDriver, true) && VEHICLE::IS_VEHICLE_DRIVEABLE(Wagon, true, true))
	{
		DioTrace("Wagon driver killed");

		DetachAllHorsesFromWagon(Wagon);
	}
	else if (!PED::IS_PED_DEAD_OR_DYING(WagonDriver, true) && !VEHICLE::IS_VEHICLE_DRIVEABLE(Wagon, true, true) && PED::GET_PED_RELATIONSHIP_GROUP_HASH(WagonDriver) != REL_PLAYER_ENEMY)
	{
		DioTrace("Wagon driver still alive but horses killed or loose");

		PED::SET_PED_RELATIONSHIP_GROUP_HASH(WagonDriver, REL_PLAYER_ENEMY);

		PED::EXPLODE_PED_HEAD(WagonDriver, WEAPON_REVOLVER_CATTLEMAN);

		DetachAllHorsesFromWagon(Wagon);
	}
	else if (!onlyDriver && CompanionsInCombat)
	{
		TASK::TASK_VEHICLE_GOTO_NAVMESH(WagonDriver, Wagon, ChosenVariant.WagonDestination.x, ChosenVariant.WagonDestination.y, ChosenVariant.WagonDestination.z, 10.0f, 0, 0);
	}

	if (ENTITY::DOES_ENTITY_EXIST(Wagon) && !PED::IS_PED_DEAD_OR_DYING(WagonDriver, true) && Common::GetDistBetweenCoords(WagonCoords, ChosenVariant.WagonDestination) < 5.0f)
	{
		MissionFailStr = "The wagon escaped.";
		MissionFail();
	}

	bool alldead = true;

	if (!PED::IS_PED_DEAD_OR_DYING(WagonDriver, true))
	{
		alldead = false;
	}

	for (int i = 0; i < WagonGuards.size(); i++)
	{
		if (!PED::IS_PED_DEAD_OR_DYING(WagonGuards[i], true))
		{
			alldead = false;
		}
	}

	if (alldead)
	{
		RunObjective_1 = false;
		ActivateObjective_2 = true;
	}
}

void RobberyCoach::BankCoachLootingInit()
{
	ENTITY::FREEZE_ENTITY_POSITION(Wagon, true);

	LootPromptL = HUD::_UIPROMPT_REGISTER_BEGIN();
	HUD::_UIPROMPT_SET_CONTROL_ACTION(LootPromptL, MISC::GET_HASH_KEY("INPUT_LOOT"));
	HUD::_UIPROMPT_SET_TEXT(LootPromptL, MISC::VAR_STRING(10, "LITERAL_STRING", "Loot Coach"));
	HUD::_UIPROMPT_SET_STANDARDIZED_HOLD_MODE(LootPromptL, 1);
	HUD::_UIPROMPT_REGISTER_END(LootPromptL);
	HUD::_UIPROMPT_SET_ENABLED(LootPromptL, false);
	HUD::_UIPROMPT_SET_VISIBLE(LootPromptL, false);

	LootPromptR = HUD::_UIPROMPT_REGISTER_BEGIN();
	HUD::_UIPROMPT_SET_CONTROL_ACTION(LootPromptR, MISC::GET_HASH_KEY("INPUT_LOOT"));
	HUD::_UIPROMPT_SET_TEXT(LootPromptR, MISC::VAR_STRING(10, "LITERAL_STRING", "Loot Coach"));
	HUD::_UIPROMPT_SET_STANDARDIZED_HOLD_MODE(LootPromptR, 1);
	HUD::_UIPROMPT_REGISTER_END(LootPromptR);
	HUD::_UIPROMPT_SET_ENABLED(LootPromptR, false);
	HUD::_UIPROMPT_SET_VISIBLE(LootPromptR, false);

	float adjustedZ = 0;

	LootPosL = ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Wagon, -1.3f, 0, 0);
	LootPosR = ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Wagon, 1.3f, 0, 0);

	MISC::GET_GROUND_Z_FOR_3D_COORD(LootPosL.x, LootPosL.y, LootPosL.z, &adjustedZ, true);

	if (adjustedZ < 0.1)
	{
		DioTrace(DIOTAG1("WARNING") "Could not adjust Z for left pos %.2ff %.2ff %.2ff", LootPosL.x, LootPosL.y, LootPosL.z);
	}
	else
	{
		LootPosL.z = adjustedZ;
	}

	MISC::GET_GROUND_Z_FOR_3D_COORD(LootPosR.x, LootPosR.y, LootPosR.z, &adjustedZ, true);

	if (adjustedZ < 0.1)
	{
		DioTrace(DIOTAG1("WARNING") "Could not adjust Z for right pos %.2ff %.2ff %.2ff", LootPosR.x, LootPosR.y, LootPosR.z);
	}
	else
	{
		LootPosR.z = adjustedZ;
	}
}

void RobberyCoach::ObjectiveInit_2()
{
	ObjectiveSet(2);
	ActivateObjective_2 = false;
	RunObjective_2 = true;

	CompanionsClearTasks();
	CompanionsRegroup();

	CurrentMusicStage = STAGE_AFTERACTION;

	VEHICLE::SET_BREAKABLE_VEHICLE_LOCKS_UNBREAKABLE(Wagon, false);

	TASK::_SET_SCENARIO_TYPE_ENABLED_HASH(MISC::GET_HASH_KEY("RANSACK_COACH_BOOT"), true);
	TASK::_SET_SCENARIO_TYPE_ENABLED_HASH(MISC::GET_HASH_KEY("RANSACK_COACH_BOOT_WIP"), true);
	
	if (ChosenVariant.CoachType == RobberyCoachType::Cargo)
	{
		WagonBlip = MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_LOOT_OBJECTIVE, Wagon);
		MAP::SET_BLIP_SPRITE(WagonBlip, MISC::GET_HASH_KEY("blip_robbery_coach"), true);
		MAP::_SET_BLIP_NAME_FROM_PLAYER_STRING(WagonBlip, ObjectivesList[2].c_str());
	}
	else if (ChosenVariant.CoachType == RobberyCoachType::Passengers)
	{
		int sequence = 0;
		TASK::OPEN_SEQUENCE_TASK(&sequence);

		TASK::TASK_LEAVE_ANY_VEHICLE(0, 0, 0);
		TASK::TASK_HANDS_UP(0, -1, PlayerPed, 0, false);
		TASK::TASK_STAND_STILL(0, -1);

		TASK::CLOSE_SEQUENCE_TASK(sequence);

		TASK::TASK_PERFORM_SEQUENCE(PassengerM, sequence);
		TASK::TASK_PERFORM_SEQUENCE(PassengerF, sequence);

		TASK::CLEAR_SEQUENCE_TASK(&sequence);

		WAIT(1000);
	}
	else if (ChosenVariant.CoachType == RobberyCoachType::BankingLegit)
	{
		WagonBlip = MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_LOOT_OBJECTIVE, Wagon);
		MAP::SET_BLIP_SPRITE(WagonBlip, MISC::GET_HASH_KEY("blip_robbery_coach"), true);
		MAP::_SET_BLIP_NAME_FROM_PLAYER_STRING(WagonBlip, ObjectivesList[2].c_str());

		BankCoachLootingInit();
	}
	else if (ChosenVariant.CoachType == RobberyCoachType::BankingTrap)
	{
		CurrentMusicStage = STAGE_ACTION2;
		
		AllowCompanionManualControl = false;

		AllowWanted = true;
		BecomeWanted(CRIME_STAGECOACH_ROBBERY, 2);
		SetDispatchService(false);
		ActivePursuitParams.DispatchResetDelay = -1;
		ActivePursuitParams.CompanionCombatOnSpotted = true;
		
		Ped marshal1 = SpawnPed("S_M_M_MARSHALLSRURAL_01", Common::ApplyRandomSmallOffsetToCoords(ChosenVariant.WagonSpawn.p, 2.0f), 0, 255);
		Ped marshal2 = SpawnPed("S_M_M_MARSHALLSRURAL_01", Common::ApplyRandomSmallOffsetToCoords(ChosenVariant.WagonSpawn.p, 2.0f), 0, 255);
		
		PED::SET_PED_INTO_VEHICLE(marshal1, Wagon, SEAT_ANY_PASSENGER);
		PED::SET_PED_INTO_VEHICLE(marshal2, Wagon, SEAT_ANY_PASSENGER);

		SetPedType(marshal1, PedType::SHOTGUN_REGULAR);
		SetPedType(marshal2, PedType::SHOTGUN_REGULAR);

		PED::SET_PED_RELATIONSHIP_GROUP_HASH(marshal1, REL_PLAYER_ENEMY);
		PED::SET_PED_RELATIONSHIP_GROUP_HASH(marshal2, REL_PLAYER_ENEMY);

		TASK::TASK_COMBAT_HATED_TARGETS(marshal1, 50);
		TASK::TASK_COMBAT_HATED_TARGETS(marshal2, 50);

		AUDIO::SET_AMBIENT_VOICE_NAME(marshal1, "0995_S_M_M_DISPATCHLEADERRURAL_WHITE_01");
		AUDIO::SET_AMBIENT_VOICE_NAME(marshal2, "0996_S_M_M_DISPATCHLEADERRURAL_WHITE_02");

		WAIT(3000);

		//ScriptedSpeechParams params{"ARRIVAL_COMBAT_NEUTRAL", "0995_S_M_M_DISPATCHLEADERRURAL_WHITE_01", 0, MISC::GET_HASH_KEY("SPEECH_PARAMS_BEAT_SHOUTED_CLEAR_NOSUB"), 0, true, 0, 0};
		//AUDIO::PLAY_PED_AMBIENT_SPEECH_NATIVE(marshal1, (Any*)&params);

		const char* sConvoRoot = "CHRC4_IG2";
		const int iSingleLineIndex = 6;

		Ped marshal = (!PED::IS_PED_DEAD_OR_DYING(marshal2, true)) ? marshal2 : marshal1;

		AUDIO::CREATE_NEW_SCRIPTED_CONVERSATION(sConvoRoot);

		AUDIO::ADD_PED_TO_CONVERSATION(sConvoRoot, marshal, "Comp4_Driver");
		AUDIO::ADD_PED_TO_CONVERSATION(sConvoRoot, marshal, "Comp4_Marshal");
		AUDIO::ADD_PED_TO_CONVERSATION(sConvoRoot, marshal, "LENNY");

		AUDIO::PRELOAD_SCRIPT_CONVERSATION(sConvoRoot, true, true, true);
		AUDIO::START_PRELOADED_CONVERSATION(sConvoRoot);

		AUDIO::_0x40CA665AB9D8D505(sConvoRoot, iSingleLineIndex);

		WAIT(2500);

		if (!PED::IS_PED_DEAD_OR_DYING(WagonDriver, true))
		{
			PED::SET_PED_AS_COP(WagonDriver, true);
			Blip newBlip = MAP::GET_BLIP_FROM_ENTITY(WagonDriver);
			MAP::_BLIP_SET_STYLE(newBlip, BLIP_STYLE_COP);
			MAP::BLIP_ADD_MODIFIER(newBlip, BLIP_MODIFIER_ENEMY_ON_GUARD_NO_CONE);
			MAP::BLIP_ADD_MODIFIER(newBlip, BLIP_MODIFIER_ENEMY_IS_ALERTED);
		}

		for (int i = 0; i < WagonGuards.size(); ++i)
		{
			if (!PED::IS_PED_DEAD_OR_DYING(WagonGuards[i], true))
			{
				PED::SET_PED_AS_COP(WagonGuards[i], true);
				Blip newBlip = MAP::GET_BLIP_FROM_ENTITY(WagonGuards[i]);
				MAP::_BLIP_SET_STYLE(newBlip, BLIP_STYLE_COP);
				MAP::BLIP_ADD_MODIFIER(newBlip, BLIP_MODIFIER_ENEMY_ON_GUARD_NO_CONE);
				MAP::BLIP_ADD_MODIFIER(newBlip, BLIP_MODIFIER_ENEMY_IS_ALERTED);
			}
		}

		Blip marshalblip = MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_COP, marshal1);
		MAP::BLIP_ADD_MODIFIER(marshalblip, BLIP_MODIFIER_ENEMY_IS_ALERTED);

		marshalblip = MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_COP, marshal2);
		MAP::BLIP_ADD_MODIFIER(marshalblip, BLIP_MODIFIER_ENEMY_IS_ALERTED);

		for (int i = 0; i < ChosenVariant.LawSpawns.size(); ++i)
		{
			CreateScriptedRespondingLawPed("S_M_M_MARSHALLSRURAL_01", PedType::SHOTGUN_WEAK, HORSE_MUSTANG, ChosenVariant.LawSpawns[i], WagonCoords);
			CreateScriptedRespondingLawPed("S_M_M_MARSHALLSRURAL_01", PedType::SNIPER_WEAK, HORSE_MUSTANG, ChosenVariant.LawSpawns[i], WagonCoords);
		}
	}

	CheckAbandon = true;
	AbandonCoords = WagonCoords;
	AbandonRadius = 60.0f;
}

bool RobberyCoach::BankCoachLootingUpdate()
{
	if (BUILTIN::VDIST2(PlayerCoords.x, PlayerCoords.y, PlayerCoords.z, LootPosL.x, LootPosL.y, LootPosL.z) < 2.0)
	{
		HUD::_UIPROMPT_SET_VISIBLE(LootPromptL, true);
		HUD::_UIPROMPT_SET_ENABLED(LootPromptL, true);
	}
	else
	{
		HUD::_UIPROMPT_SET_VISIBLE(LootPromptL, false);
		HUD::_UIPROMPT_SET_ENABLED(LootPromptL, false);
	}

	if (BUILTIN::VDIST2(PlayerCoords.x, PlayerCoords.y, PlayerCoords.z, LootPosR.x, LootPosR.y, LootPosR.z) < 2.0)
	{
		HUD::_UIPROMPT_SET_VISIBLE(LootPromptR, true);
		HUD::_UIPROMPT_SET_ENABLED(LootPromptR, true);
	}
	else
	{
		HUD::_UIPROMPT_SET_VISIBLE(LootPromptR, false);
		HUD::_UIPROMPT_SET_ENABLED(LootPromptR, false);
	}
	
	bool left = HUD::_UIPROMPT_HAS_HOLD_MODE_COMPLETED(LootPromptL);
	bool right = HUD::_UIPROMPT_HAS_HOLD_MODE_COMPLETED(LootPromptR);

	if (left || right)
	{
		HUD::_UIPROMPT_SET_VISIBLE(LootPromptL, false);
		HUD::_UIPROMPT_SET_VISIBLE(LootPromptR, false);
		HUD::_UIPROMPT_SET_ENABLED(LootPromptL, false);
		HUD::_UIPROMPT_SET_ENABLED(LootPromptR, false);

		TASK::CLEAR_PED_TASKS(PlayerPed, true, true);

		Vector3 slideCoord = left ? LootPosL : LootPosR;
		float wagonHeading = ENTITY::GET_ENTITY_HEADING(Wagon);

		VEHICLE::SET_VEHICLE_DOOR_BROKEN(Wagon, left ? 0 : 2, false);

		WEAPON::SET_CURRENT_PED_WEAPON(PlayerPed, WEAPON_UNARMED, true, WEAPON_ATTACH_POINT_HAND_PRIMARY, false, false);

		if (!STREAMING::HAS_ANIM_DICT_LOADED(WagonManualLootAnimDict.c_str()))
		{
			DioTrace(DIOTAG1("WARNING") "Manual anim unloaded");
		}

		int sequence = 0;
		TASK::OPEN_SEQUENCE_TASK(&sequence);

		TASK::TASK_PED_SLIDE_TO_COORD(0, slideCoord.x, slideCoord.y, slideCoord.z, (left ? wagonHeading - 90 : wagonHeading + 90), 2.0f);
		TASK::TASK_PLAY_ANIM(0, WagonManualLootAnimDict.c_str(), WagonManualLootAnim.c_str(), 1.0, 1.0f, -1, 0, 0, 0, 0, 0, 0, 0);

		TASK::CLOSE_SEQUENCE_TASK(sequence);
		TASK::TASK_PERFORM_SEQUENCE(PlayerPed, sequence);
		TASK::CLEAR_SEQUENCE_TASK(&sequence);

		return true;
	}

	return false;
}

void RobberyCoach::ObjectiveLogic_2()
{
	if (ChosenVariant.CoachType == RobberyCoachType::Cargo)
	{
		if (TASK::GET_RANSACK_SCENARIO_POINT_PED_IS_USING(PlayerPed) != 0)
		{
			MAP::REMOVE_BLIP(&WagonBlip);
			RunObjective_2 = false;
			ActivateObjective_3 = true;
		}
	}
	else if (ChosenVariant.CoachType == RobberyCoachType::Passengers)
	{
		int sequence = 0;
		TASK::OPEN_SEQUENCE_TASK(&sequence);

		TASK::TASK_HANDS_UP(0, 999, PlayerPed, 0, false);
		TASK::TASK_STAND_STILL(0, 999);

		TASK::SET_SEQUENCE_TO_REPEAT(sequence, true);
		TASK::CLOSE_SEQUENCE_TASK(sequence);

		if (!PED::IS_PED_DEAD_OR_DYING(PassengerM, true) && TASK::GET_SEQUENCE_PROGRESS(PassengerM) == -1)
		{
			TASK::TASK_PERFORM_SEQUENCE(PassengerM, sequence);
			DioTrace("Hands up M");
			WAIT(500);
		}

		if (!PED::IS_PED_DEAD_OR_DYING(PassengerM, true) && TASK::GET_SEQUENCE_PROGRESS(PassengerF) == -1)
		{
			TASK::TASK_PERFORM_SEQUENCE(PassengerF, sequence);
			DioTrace("Hands up F");
			WAIT(500);
		}
		
		TASK::CLEAR_SEQUENCE_TASK(&sequence);
		
		if (ENTITY::_IS_ENTITY_FULLY_LOOTED(PassengerM) || ENTITY::_IS_ENTITY_FULLY_LOOTED(PassengerF))
		{
			RunObjective_2 = false;
			ActivateObjective_3 = true;
		}
	}
	else if (ChosenVariant.CoachType == RobberyCoachType::BankingLegit)
	{
		if (BankCoachLootingUpdate())
		{
			MAP::REMOVE_BLIP(&WagonBlip); 
			WAIT(4500);
			RunObjective_2 = false;
			ActivateObjective_3 = true;
		}
	}
	else if (ChosenVariant.CoachType == RobberyCoachType::BankingTrap)
	{
		VEHICLE::BRING_VEHICLE_TO_HALT(Wagon, 0, -1, true);
		
		bool alldead = true;

		if (!PED::IS_PED_DEAD_OR_DYING(WagonDriver, true))
		{
			alldead = false;
		}

		for (int i = 0; i < WagonGuards.size(); i++)
		{
			if (!PED::IS_PED_DEAD_OR_DYING(WagonGuards[i], true))
			{
				alldead = false;
			}
		}

		if (!ActivePursuitParams.AllScriptedPedsKilled)
		{
			WantedLogic();
			alldead = false;
		}

		if (alldead)
		{
			ClearWanted(false);
			RunObjective_2 = false;
			ActivateObjective_3 = true;
		}
	}
}

void RobberyCoach::ObjectiveInit_3()
{
	ObjectiveSet(3);
	ActivateObjective_3 = false;
	RunObjective_3 = true;

	if (ChosenVariant.CoachType == RobberyCoachType::BankingTrap)
	{
		CurrentMusicStage = STAGE_AFTERACTION;
		
		CompanionsClearTasks();
		CompanionsRegroup();

		WagonBlip = MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_LOOT_OBJECTIVE, Wagon);
		MAP::SET_BLIP_SPRITE(WagonBlip, MISC::GET_HASH_KEY("blip_robbery_coach"), true);
		MAP::_SET_BLIP_NAME_FROM_PLAYER_STRING(WagonBlip, ObjectivesList[3].c_str());

		BankCoachLootingInit();

		DetachAllHorsesFromWagon(Wagon);

		return;
	}

	CheckAbandon = false;

	AllowCompanionManualControl = true;

	AllowWanted = true;
	BecomeWanted(CRIME_STAGECOACH_ROBBERY, 2);
	SetDispatchService(false);
	ActivePursuitParams.DispatchResetDelay = -1;
	ActivePursuitParams.CompanionCombatOnSpotted = true;

	WAIT(3000);

	CreateScriptedRespondingLawPed(GenericLawLeader, PedType::SIDEARM_REGULAR, HORSE_MORGAN, ActivePursuitParams.ScriptedLawSpawnPoints[0], WagonCoords);
	for (int j = 0; j < 2; ++j)
	{
		CreateScriptedRespondingLawPed(GenericLawFollower, PedType::REPEATER_REGULAR, HORSE_MORGAN, ActivePursuitParams.ScriptedLawSpawnPoints[0], WagonCoords);
	}
}

void RobberyCoach::ObjectiveLogic_3()
{
	if (ChosenVariant.CoachType == RobberyCoachType::BankingTrap)
	{
		if (BankCoachLootingUpdate())
		{
			MAP::REMOVE_BLIP(&WagonBlip);
			WAIT(5000);
			RunObjective_3 = false;
			MissionSuccess();
		}
		
		return;
	}

	if (PlayerIncognito && Common::GetDistBetweenCoords(WagonCoords, PlayerCoords) > 150)
	{
		ClearWanted(false);
	}

	if (ActivePursuitParams.AllScriptedPedsKilled)
	{
		ClearWanted(false);
	}

	if (!WantedLogic())
	{
		CurrentMusicStage = STAGE_AFTERACTION;
		UpdateMusic();

		RunObjective_3 = false;
		MissionSuccess();
	}
}

void RobberyCoach::TrackRewards()
{
	RewardBounty_Active = PlayerIncognito ? int(RewardBounty_Potential * 0.25f) : RewardBounty_Potential;

	RewardMembers_Active = RewardMembers_Potential;

	switch (ChosenVariant.CoachType)
	{
		case RobberyCoachType::BankingTrap:
		case RobberyCoachType::BankingLegit:
			RewardCash_Active = RewardCash_Potential; // 3500;
			break;

		case RobberyCoachType::Passengers:
			RewardCash_Active = 2500;
			break;

		default:
			RewardCash_Active = 1500;
	}
}

void RobberyCoach::Update()
{
	if (ENTITY::DOES_ENTITY_EXIST(Wagon))
	{
		WagonCoords = ENTITY::GET_ENTITY_COORDS(Wagon, true, true);

		if (!ENTITY::IS_ENTITY_UPRIGHT(Wagon, 50))
		{
			MissionFailStr = "The coach was wrecked.";
			MissionFail();
		}
	}

	if (TimeMissionFail > 0 && MISC::GET_GAME_TIMER() - TimeMissionFail > TimeMissionFailDelay)
	{
		DioTrace("Exit Fail Triggered");

		ExitMissionFlag = M_EXIT_FAILURE;
	}
	else if (TimeMissionSuccess > 0 && MISC::GET_GAME_TIMER() - TimeMissionSuccess > TimeMissionSuccessDelay)
	{
		DioTrace("Exit Win Triggered");

		ExitMissionFlag = M_EXIT_SUCCESS;
	}
	else if (ActivateObjective_0)
	{
		ObjectiveInit_0();
	}
	else if (ActivateObjective_1)
	{
		ObjectiveInit_1();
	}
	else if (ActivateObjective_2)
	{
		ObjectiveInit_2();
	}
	else if (ActivateObjective_3)
	{
		ObjectiveInit_3();
	}
	else if (RunObjective_0)
	{
		ObjectiveLogic_0();
	}
	else if (RunObjective_1)
	{
		ObjectiveLogic_1();
	}
	else if (RunObjective_2)
	{
		ObjectiveLogic_2();
	}
	else if (RunObjective_3)
	{
		ObjectiveLogic_3();
	}
}
