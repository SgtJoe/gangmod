#pragma once

#include "Common.h"
#include "MissionBase.h"

class JailbreakChain : public MissionBase
{
	bool SetMissionParamsForCamp(int camp) override;

	struct JailbreakChainData
	{
		std::string DebugName;
		Vector3 BlipPoint;
		std::vector<LocationData> PrisonerSpawns;
		std::vector<LocationData> OfficerSpawns;
		std::vector<LocationData> GuardSpawns;
		Vector3 LawSpawn;
	};

	std::vector<JailbreakChainData> AllAvailableCollections;
	JailbreakChainData ChosenCollection;

	Blip AreaBlip = 0;

	bool GuardsAlerted = false;
	bool GuardsCombat = false;
	int GuardSpotTime = 0;

	int GuardCasualties = 0;
	int PrisonerCasualties = 0;

	bool PrisonersAttack = false;

	std::vector<Ped> SpawnedGuards;
	std::vector<Ped> SpawnedPrisoners;

	void CommonInit() override;
	void TrackRewards() override;

	bool ActivateObjective_0 = true;
	bool ActivateObjective_1 = false;
	bool ActivateObjective_2 = false;
	bool ActivateObjective_3 = false;

	bool RunObjective_0 = false;
	bool RunObjective_1 = false;
	bool RunObjective_2 = false;
	bool RunObjective_3 = false;

	void ObjectiveInit_0();
	void ObjectiveInit_1();
	void ObjectiveInit_2();

	void ObjectiveLogic_0();
	void ObjectiveLogic_1();
	void ObjectiveLogic_2();

public:
	JailbreakChain();
	~JailbreakChain();

	void Setup(int campid, const std::string& gangstr, const std::vector<MissionCompanionInfo>& companions) override;

	void Update() override;

	static const MissionStaticData& GetStaticMissionData();
};
