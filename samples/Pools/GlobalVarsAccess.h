
#pragma once
#include "Common.h"

enum REGION
{
	REGION_BAY_SAINT_DENIS = 5,
	REGION_GRT_BLACKWATER = 38,
	REGION_HRT_VALENTINE = 76,
	REGION_SCM_RHODES = 105,
	REGION_CHO_ARMADILLO = 120,
	REGION_GAP_TUMBLEWEED = 115
};

const int REGION_COUNT = 6;
const int REGION_ARRAY[REGION_COUNT] = {
	REGION_BAY_SAINT_DENIS,
	REGION_GRT_BLACKWATER,
	REGION_HRT_VALENTINE,
	REGION_SCM_RHODES,
	REGION_CHO_ARMADILLO,
	REGION_GAP_TUMBLEWEED
};
const std::string REGION_STRING_ARRAY[REGION_COUNT] = {
	"REGION_BAY_SAINT_DENIS",
	"REGION_GRT_BLACKWATER",
	"REGION_HRT_VALENTINE",
	"REGION_SCM_RHODES",
	"REGION_CHO_ARMADILLO",
	"REGION_GAP_TUMBLEWEED"
};

enum class REGION_FLAG : int
{
	// Enables extra law spawning and patrols
	LOCKDOWN = 33554432,
	// Sets Wanted Dead Or Alive status on map and sets location UI to Restricted Area
	PERMANENT_LOCKDOWN = 16777216,
	// Makes lawmen engage on sight
	WANTED_ZONE = 67108864
};

enum MapColorHashes : Hash
{
	// Dark Red
	BLIP_STYLE_REGION_LOCKDOWN = 0xB232154F,
	// Light Red
	BLIP_STYLE_WANTED_REGION = 0xA7AA4808,
	// White
	BLIP_STYLE_WHITE = 0xE15AFF64
};

class GlobalVarsAccess
{
	static inline bool RegionIDValid(int id) { return !(id <= -1 || id > 120); }
	
	static inline UINT64* GetRegionGlobal(int region) { return getGlobalPtr(23118 + 1 + (region * 11)); }

	static bool HAS_REGION_FLAG(int region, int flag);
	static void APPLY_REGION_FLAG(int region, int flag);
	static void REMOVE_REGION_FLAG(int region, int flag);

	static std::string RegionIDToFriendlyName(int region);
	static std::string RegionHashToIDName(Hash region);
	static Hash RegionIDToHash(int region, bool outline);

public:
	static void SetRegionLocked(int id, bool locked);

	static inline void SetPlayerFlag(int flag)
	{
		UINT64* uParam0 = getGlobalPtr(1935630);
		
		*uParam0 = (*uParam0 | flag);
	}

	static void SET_WORLD_STATE_FLAG(WorldState worldState, bool on);
};
