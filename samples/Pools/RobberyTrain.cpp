
#include "precomp.h"
#include "RobberyTrain.h"

const MissionStaticData& RobberyTrain::GetStaticMissionData()
{
	static MissionStaticData missiondata = {
		MissionType::ROBBERY_TRAIN,
		2500, 15, 3500, 15000, 0,
		"Train Robbery", 
		"Stopping a train is difficult, but the rewards are worth it.",
		COMPCOUNT_MEDIUM, 
		true,
		MUSIC_DEFAULT,
		false
	};
	return missiondata;
}

RobberyTrain::RobberyTrain()
{
	DioScope(DIOTAG);
}

RobberyTrain::~RobberyTrain()
{
	DioScope(DIOTAG);

	VEHICLE::SET_RANDOM_TRAINS(true);

	DisableTrainControls(false);

	PAD::DISABLE_CONTROL_ACTION(0, INPUT_INTERACT_LOCKON_POS, false);
	PAD::DISABLE_CONTROL_ACTION(1, INPUT_INTERACT_LOCKON_POS, false);
}

void RobberyTrain::Setup(int campid, const std::string& gangstr, const std::vector<MissionCompanionInfo>& companions)
{
	DioScope(DIOTAG "campid %d gangstr %s", campid, gangstr.c_str());

	GangName = gangstr;
	GangMemberTemplates = companions;

	if (!SetMissionParamsForCamp(campid))
	{
		ExitMissionFlag = M_EXIT_ERROR_TERMINATE;
		return;
	}

	CommonInit();
}

bool RobberyTrain::SetMissionParamsForCamp(int camp)
{
	bool success = false;

	TrainData variant1, variant2;

	switch (camp)
	{
		case DISTRICT_GRIZZLIES_WEST:
			variant1.AmbushPoint = {-744.00f, 1000.26f, 115.54f};
			variant1.TrainSpawn = {-679.21f, 962.96f, 119.80f};
			variant1.ReverseDirection = true;

			AllAvailableVariants = {variant1};
			success = true;
			break;

		case DISTRICT_CUMBERLAND_FOREST:
			variant1.AmbushPoint = {883.97f, 877.75f, 113.77f};
			variant1.TrainSpawn = {860.88f, 754.96f, 109.17f};
			variant1.ReverseDirection = false;

			AllAvailableVariants = {variant1};
			success = true;
			break;

		case DISTRICT_BLUEGILL_MARSH:
			variant1.AmbushPoint = {2726.26f, 36.00f, 50.53f};
			variant1.TrainSpawn = {2781.08f, 48.37f, 45.99f};
			variant1.ReverseDirection = false;

			variant2.AmbushPoint = {2110.04f, -1310.58f, 41.61f};
			variant2.TrainSpawn = {2157.95f, -1245.02f, 42.41f};
			variant2.ReverseDirection = false;

			AllAvailableVariants = {variant1, variant2};
			success = true;
			break;

		case DISTRICT_RIO_BRAVO:
			variant1.AmbushPoint = {-4920.30f, -3125.87f, -14.46f};
			variant1.TrainSpawn = {-5051.48f, -3128.34f, -15.46f};
			variant1.ReverseDirection = true;

			variant2.AmbushPoint = {-5865.89f, -3440.92f, -24.33f};
			variant2.TrainSpawn = {-5945.13f, -3375.03f, -23.00f};
			variant2.ReverseDirection = false;

			AllAvailableVariants = {variant1, variant2};
			success = true;
			break;
	}

	if (success)
	{
		ChosenVariant = AllAvailableVariants[rand() % AllAvailableVariants.size()];

		ScriptsKillList = {MISC::GET_HASH_KEY("trainrobbery_ambient")};
	}
	else
	{
		DioTrace(DIOTAG1("ERROR") "CANNOT SET MISSION %d PARAMS FOR CAMP %d - ABORTING MISSION", RobberyTrain::GetStaticMissionData().StaticMissionID, camp);
	}

	return success;
}

void RobberyTrain::CommonInit()
{
	const MissionStaticData& thisMissionData = RobberyTrain::GetStaticMissionData();
	SetVarsFromStaticData(thisMissionData);
	
	SetGenericLawModels(DISTRICT_GRIZZLIES_WEST);

	MissionAssetsToLoad = {
		{"PED", "S_M_M_UNITRAINENGINEER_01"},
		{"PED", "A_M_M_UniCoachGuards_01"},
		{"PED", "A_M_M_UPPERTRAINPASSENGERS_01"},
		{"PED", "A_F_M_UPPERTRAINPASSENGERS_01"},
		{"SET", "PG_MISSION_WINTER4_CAR3"}
	};

	for (int i = 0; i < TRAIN_MODELS_SIZE; ++i)
	{
		MissionAssetsToLoad.push_back({"VEH", TRAIN_MODELS[i]});
	}

	AddHorseToMissionAssets(HORSE_SADDLER);

	ObjectivesList = {
		"Go to the ~COLOR_YELLOW~location",
		"Threaten or hijack the ~COLOR_YELLOW~train driver",
		"Rob the ~COLOR_YELLOW~passengers",
		"~COLOR_YELLOW~Rob more~COLOR_WHITE~ or ~COLOR_RED~escape",
		"Lose the ~COLOR_RED~law"
	};

	LoadAllMissionAssets();
	SpawnGangMembers(thisMissionData.NumMembersToSpawn);

	WAIT(2000);
}

void RobberyTrain::DisableTrainControls(bool disable)
{
	for (int i = 0; i <= 1; ++i)
	{
		PAD::DISABLE_CONTROL_ACTION(i, INPUT_VEH_HANDCART_ACCELERATE, disable);
		PAD::DISABLE_CONTROL_ACTION(i, INPUT_VEH_HORN, disable);
		PAD::DISABLE_CONTROL_ACTION(i, INPUT_VEH_CAR_AIM, disable);
		PAD::DISABLE_CONTROL_ACTION(i, INPUT_VEH_EXIT, disable);
	}
}

void RobberyTrain::ObjectiveInit_0()
{
	DioScope(DIOTAG);
	
	ObjectiveSet(0);
	ActivateObjective_0 = false;
	RunObjective_0 = true;
	
	AmbushBlip = ObjectiveBlipCoords(ChosenVariant.AmbushPoint, 15, true, ObjectivesList[0]);

#ifdef DEBUG_FAST_MISSION
	ENTITY::_SET_ENTITY_COORDS_AND_HEADING(PlayerPed, ChosenVariant.AmbushPoint.x, ChosenVariant.AmbushPoint.y + 18, ChosenVariant.AmbushPoint.z + 10, 
		MISC::GET_HEADING_FROM_VECTOR_2D(ChosenVariant.TrainSpawn.x - ChosenVariant.AmbushPoint.x, ChosenVariant.TrainSpawn.y - ChosenVariant.AmbushPoint.y), true, true, true);
	ENTITY::PLACE_ENTITY_ON_GROUND_PROPERLY(PlayerPed, true);
	CAM::SET_GAMEPLAY_CAM_RELATIVE_HEADING(0, 1);
	CAM::SET_GAMEPLAY_CAM_RELATIVE_PITCH(0, 1);

	ENTITY::_SET_ENTITY_COORDS_AND_HEADING(PlayerHorse, ChosenVariant.AmbushPoint.x, ChosenVariant.AmbushPoint.y + 24, ChosenVariant.AmbushPoint.z + 15,
		MISC::GET_HEADING_FROM_VECTOR_2D(ChosenVariant.TrainSpawn.x - ChosenVariant.AmbushPoint.x, ChosenVariant.TrainSpawn.y - ChosenVariant.AmbushPoint.y), true, true, true);
	ENTITY::PLACE_ENTITY_ON_GROUND_PROPERLY(PlayerPed, true);
#endif
}

void RobberyTrain::ObjectiveLogic_0()
{
	if (Common::GetDistBetweenCoords(PlayerCoords, ChosenVariant.AmbushPoint) < 15)
	{
		MAP::REMOVE_BLIP(&AmbushBlip);

		RunObjective_0 = false;
		ActivateObjective_1 = true;
	}
}

void RobberyTrain::PopulatePassengers(int car, Vector3 offset)
{
	Ped civ = SpawnPed(Common::Format("A_%s_M_UPPERTRAINPASSENGERS_01", rand() % 100 < 35 ? "F" : "M"), ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(TrainCars[car], offset), 0, 255);

	ENTITY::SET_ENTITY_INVINCIBLE(civ, true);

	PED::SET_PED_RELATIONSHIP_GROUP_HASH(civ, REL_CIV);

	WEAPON::REMOVE_ALL_PED_WEAPONS(civ, true, true);

	PED::SET_PED_CONFIG_FLAG(civ, PCF_AlwaysRejectPlayerRobberyAttempt, false);
	PED::SET_PED_CONFIG_FLAG(civ, PCF_BlockRobberyInteractionEscape, true);
	PED::SET_PED_CONFIG_FLAG(civ, PCF_AllowRobberyWhenInjured, true);
	PED::SET_PED_CONFIG_FLAG(civ, PCF_ForceOfferItemOnReceivingRobberyInteraction, true);

	Passengers.emplace_back(civ);
}

void RobberyTrain::ObjectiveInit_1()
{
	ObjectiveSet(1);
	ActivateObjective_1 = false;
	RunObjective_1 = true;

	CurrentMusicStage = STAGE_ACTION1;

	DisableLaw();

	VEHICLE::DELETE_ALL_TRAINS();

	Common::ClearArea(ChosenVariant.TrainSpawn, 100.0f);

	TrainHandle = SpawnTrain(TRAIN_NAME, ChosenVariant.TrainSpawn, ChosenVariant.ReverseDirection);

	while (VEHICLE::_GET_TRAIN_CARRIAGE_TRAILER_NUMBER(TrainHandle) != TRAINCAR_AMOUNT)
	{
		WAIT(0);
	}

	for (int i = 0; i < TRAINCAR_AMOUNT; ++i)
	{
		TrainCars[i] = VEHICLE::GET_TRAIN_CARRIAGE(TrainHandle, i);

		ENTITY::SET_ENTITY_INVINCIBLE(TrainCars[i], true);
	}

	Locomotive = TrainCars[TRAINCAR_0_LOCOMOTIVE];

	PROPSET::_ADD_PROP_SET_FOR_VEHICLE(TrainCars[TRAINCAR_3_FLAT], MISC::GET_HASH_KEY("PG_MISSION_WINTER4_CAR3"));
	PATHFIND::_NAVMESH_ASSIGN_NAVMESH_TO_VEHICLE(TrainCars[TRAINCAR_3_FLAT], "navmesh_pg_mission_winter4_car3");

	WAIT(1000);

	Conductor = SpawnPed("S_M_M_UNITRAINENGINEER_01", ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Locomotive, 0, 0, 10), 0, 255, 9);

	PED::SET_PED_RELATIONSHIP_GROUP_HASH(Conductor, REL_CIV);

	WEAPON::REMOVE_ALL_PED_WEAPONS(Conductor, true, true);

	EVENT::SET_DECISION_MAKER(Conductor, MISC::GET_HASH_KEY("EMPTY"));

	PED::SET_PED_CONFIG_FLAG(Conductor, PCF_OneShotWillKillPed, true);
	PED::SET_PED_CONFIG_FLAG(Conductor, PCF_AlwaysRejectPlayerRobberyAttempt, false);
	PED::SET_PED_CONFIG_FLAG(Conductor, PCF_BlockRobberyInteractionEscape, true);
	PED::SET_PED_CONFIG_FLAG(Conductor, PCF_AllowRobberyWhenInjured, true);
	PED::SET_PED_CONFIG_FLAG(Conductor, PCF_ForceOfferItemOnReceivingRobberyInteraction, true);

	Common::SetPedHonorModifier(Conductor, HONOR_NEGATIVE);

	PED::SET_PED_INTO_VEHICLE(Conductor, Locomotive, SEAT_DRIVER);

	int voxIndex = rand() % 3; 
	std::string voxStr = Common::Format("%d_S_M_M_COACHTAXIDRIVER_01_WHITE_0%d", 1029 + voxIndex, 1 + voxIndex);
	AUDIO::SET_AMBIENT_VOICE_NAME(Conductor, voxStr.c_str());

	StopDriverPrompt1 = HUD::_UIPROMPT_REGISTER_BEGIN();
	HUD::_UIPROMPT_SET_CONTROL_ACTION(StopDriverPrompt1, INPUT_INTERACT_LOCKON_NEG);
	HUD::_UIPROMPT_SET_TEXT(StopDriverPrompt1, MISC::VAR_STRING(10, "LITERAL_STRING", "Stop The Train"));
	HUD::_UIPROMPT_SET_STANDARD_MODE(StopDriverPrompt1, true);
	HUD::_UIPROMPT_REGISTER_END(StopDriverPrompt1);
#ifndef DEBUG_FAST_MISSION
	HUD::_UIPROMPT_SET_ENABLED(StopDriverPrompt1, false);
#else
	HUD::_UIPROMPT_SET_ENABLED(StopDriverPrompt1, true);
#endif
	HUD::_UIPROMPT_SET_VISIBLE(StopDriverPrompt1, true);
	HUD::_UIPROMPT_SET_GROUP(StopDriverPrompt1, HUD::_UIPROMPT_GET_GROUP_ID_FOR_TARGET_ENTITY(Conductor), 0);

	StopDriverPrompt2 = HUD::_UIPROMPT_REGISTER_BEGIN();
	HUD::_UIPROMPT_SET_CONTROL_ACTION(StopDriverPrompt2, INPUT_INTERACT_LOCKON_NEG);
	HUD::_UIPROMPT_SET_TEXT(StopDriverPrompt2, MISC::VAR_STRING(10, "LITERAL_STRING", "Stop The Train!"));
	HUD::_UIPROMPT_SET_STANDARD_MODE(StopDriverPrompt2, true);
	HUD::_UIPROMPT_REGISTER_END(StopDriverPrompt2);
	HUD::_UIPROMPT_SET_ENABLED(StopDriverPrompt2, false);
	HUD::_UIPROMPT_SET_VISIBLE(StopDriverPrompt2, false);
	HUD::_UIPROMPT_SET_GROUP(StopDriverPrompt2, HUD::_UIPROMPT_GET_GROUP_ID_FOR_TARGET_ENTITY(Conductor), 0);

	PromptEnableTime = MISC::GET_GAME_TIMER();

	for (const TrainGuard& spawn : GuardSpawns)
	{
		Ped guard = SpawnPed("A_M_M_UniCoachGuards_01", 
			ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(TrainCars[spawn.car], {spawn.offset.x, spawn.offset.y, 1.15f}), 
			ENTITY::GET_ENTITY_HEADING(TrainCars[spawn.car]) - spawn.heading,
		255);

		SetPedType(guard, spawn.weapons);

		int voxIndex = rand() % 9;
		std::string voxStr = Common::Format("0%d_S_M_M_DISPATCHLAWRURAL_WHITE_0%d", 986 + voxIndex, 1 + voxIndex);
		AUDIO::SET_AMBIENT_VOICE_NAME(guard, voxStr.c_str());

		Blip guardBlip = MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_ENEMY, guard);
		MAP::BLIP_ADD_MODIFIER(guardBlip, BLIP_MODIFIER_ENEMY_GUNSHOTS_ONLY);

#ifndef DEBUG_FAST_MISSION
		PED::SET_PED_RELATIONSHIP_GROUP_HASH(guard, REL_PLAYER_ENEMY);
#else
		PED::SET_PED_RELATIONSHIP_GROUP_HASH(guard, REL_IGNORE_EVERYTHING);
#endif
		PED::SET_PED_COMBAT_MOVEMENT(guard, 1);
		PED::SET_PED_COMBAT_ATTRIBUTES(guard, CA_PERMIT_CHARGE_BEYOND_DEFENSIVE_AREA, false);

		PED::_SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_ENTITY(guard, TrainCars[spawn.car], {0,0,0}, 30.0f, 0, false);

		//TASK::TASK_STAND_GUARD(guard, ENTITY::GET_ENTITY_COORDS(guard, true, true), ENTITY::GET_ENTITY_HEADING(guard), "");
		TASK::TASK_STAND_STILL(guard, 3000);
	}

	VEHICLE::_TRIGGER_TRAIN_WHISTLE(TrainHandle, "MOVING", false, false);

	VEHICLE::_SET_TRAIN_MAX_SPEED(TrainHandle, InitialTrainSpeed);
	VEHICLE::SET_TRAIN_CRUISE_SPEED(TrainHandle, InitialTrainSpeed);
	VEHICLE::SET_TRAIN_SPEED(TrainHandle, InitialTrainSpeed);

	TrainBlip = ObjectiveBlipEntity(Locomotive, false, ObjectivesList[1]);
	MAP::SET_BLIP_SPRITE(TrainBlip, MISC::GET_HASH_KEY("blip_ambient_train"), false);
	
	PopulatePassengers(TRAINCAR_6_PASSENGER, {0, -2, 2});
	PopulatePassengers(TRAINCAR_6_PASSENGER, {0, -1, 2});
	PopulatePassengers(TRAINCAR_6_PASSENGER, {0, 0, 2});
	PopulatePassengers(TRAINCAR_6_PASSENGER, {0, 0, 2});
	PopulatePassengers(TRAINCAR_6_PASSENGER, {0, 1, 2});
	PopulatePassengers(TRAINCAR_6_PASSENGER, {0, 2, 2});
	PopulatePassengers(TRAINCAR_6_PASSENGER, {0, 3, 2});

	PopulatePassengers(TRAINCAR_7_PASSENGER, {0, -2, 2});
	PopulatePassengers(TRAINCAR_7_PASSENGER, {0, -1, 2});
	PopulatePassengers(TRAINCAR_7_PASSENGER, {0, -1, 2});
	PopulatePassengers(TRAINCAR_7_PASSENGER, {0, 0, 2});
	PopulatePassengers(TRAINCAR_7_PASSENGER, {0, 1, 2});
	PopulatePassengers(TRAINCAR_7_PASSENGER, {0, 1, 2});
	PopulatePassengers(TRAINCAR_7_PASSENGER, {0, 2, 2});
	PopulatePassengers(TRAINCAR_7_PASSENGER, {0, 3, 2});

	PlayCompanionSpeech("MOVE_IN");

	CompanionsDisband();

	for (int i = 0; i < SpawnedGangMembers.size(); ++i)
	{
		ENTITY::SET_ENTITY_INVINCIBLE(SpawnedGangMembers[i].member, true);
		TASK::TASK_FOLLOW_TO_OFFSET_OF_ENTITY(SpawnedGangMembers[i].member, TrainCars[TRAINCAR_6_PASSENGER], {}, 30.0f, -1, 8.0f, true, false, false, false, false, false);
	}
}

void RobberyTrain::ObjectiveLogic_1()
{
	DisableTrainControls(true);

	bool stopped = VEHICLE::IS_VEHICLE_STOPPED(TrainHandle);
	Ped driver = VEHICLE::GET_DRIVER_OF_VEHICLE(TrainHandle);

#ifndef DEBUG_FAST_MISSION
	if (PromptEnableTime != 0 && MISC::GET_GAME_TIMER() - PromptEnableTime > 5000)
	{
		DioTrace("Enabling prompt");
		PromptEnableTime = 0;
		HUD::_UIPROMPT_SET_ENABLED(StopDriverPrompt1, true);
	}
#endif

	if (driver != Conductor)
	{
		HUD::_UIPROMPT_SET_VISIBLE(StopDriverPrompt1, false);
		HUD::_UIPROMPT_SET_ENABLED(StopDriverPrompt1, false);
		HUD::_UIPROMPT_SET_VISIBLE(StopDriverPrompt2, false);
		HUD::_UIPROMPT_SET_ENABLED(StopDriverPrompt2, false);
	}

	if (!TrainHaltingDriver)
	{
		if (HUD::_UIPROMPT_HAS_STANDARD_MODE_COMPLETED(StopDriverPrompt1, 0) && StopDriverPromptStage == 0)
		{
			DioTrace("Indimidation 1");

			PlayPlayerSpeech("RIDER_CALL_OUT_AGAIN_LAST_CHANCE");

			HUD::_UIPROMPT_SET_ENABLED(StopDriverPrompt1, false);
			HUD::_UIPROMPT_SET_VISIBLE(StopDriverPrompt1, false);
			HUD::_UIPROMPT_SET_VISIBLE(StopDriverPrompt2, true);

			PromptRespondTime = MISC::GET_GAME_TIMER();
			StopDriverPromptStage = 1;
		}
		else if (!HUD::_UIPROMPT_IS_ENABLED(StopDriverPrompt2) && MISC::GET_GAME_TIMER() - PromptRespondTime > 3000 && StopDriverPromptStage == 1)
		{
			DioTrace("Indimidation 1.5");

			HUD::_UIPROMPT_SET_ENABLED(StopDriverPrompt2, true);
			PromptRespondTime = 0;
			StopDriverPromptStage = 2;
		}
		else if (HUD::_UIPROMPT_HAS_STANDARD_MODE_COMPLETED(StopDriverPrompt2, 0) && StopDriverPromptStage == 2)
		{
			DioTrace("Indimidation 2");

			PlayPlayerSpeech("RIDER_CALL_OUT_AGAIN_LAST_CHANCE");

			HUD::_UIPROMPT_SET_ENABLED(StopDriverPrompt2, false);
			HUD::_UIPROMPT_SET_VISIBLE(StopDriverPrompt2, false);

			PromptRespondTime = MISC::GET_GAME_TIMER();
			StopDriverPromptStage = 3;
		}
		else if (MISC::GET_GAME_TIMER() - PromptRespondTime > 2500 && StopDriverPromptStage == 3)
		{
			DioTrace("Indimidation complete");

			ScriptedSpeechParams params{"RT_INTIMIDATED_ROB_DEFUSE", 0, 0, MISC::GET_HASH_KEY("SPEECH_PARAMS_BEAT_SHOUTED_CLEAR_NOSUB"), 0, true, 0, 0};
			AUDIO::PLAY_PED_AMBIENT_SPEECH_NATIVE(Conductor, (Any*)&params);

			TrainHaltingDriver = true;
			StopDriverPromptStage = 4;
		}
	}
	
	if (!PED::IS_PED_DEAD_OR_DYING(Conductor, true) && EVENT::IS_SHOCKING_EVENT_IN_SPHERE(0x09099FC1, ENTITY::GET_ENTITY_COORDS(Locomotive, true, true), 50.0f) && TrainHijackTime == 0)
	{
		TrainHijackTime = MISC::GET_GAME_TIMER();
	}
	else if (TrainHijackTime != 0 && MISC::GET_GAME_TIMER() - TrainHijackTime > 1600)
	{
		Vector3 pos = ENTITY::GET_ENTITY_COORDS(Conductor, true, true);
		MISC::SHOOT_SINGLE_BULLET_BETWEEN_COORDS(pos, {pos.x + 0.001f, pos.y, pos.z - 0.001f}, 9999, false, WEAPON_REVOLVER_CATTLEMAN, PlayerPed, false, true, 100.0f, true);
	}
	else if (TrainHijackTime != 0 && MISC::GET_GAME_TIMER() - TrainHijackTime > 2200)
	{
		DioTrace("Hijack failed, hard killing");
		TrainHijackTime = 0;
		ENTITY::_CHANGE_ENTITY_HEALTH(Conductor, -9999.9f, PlayerPed, WEAPON_REVOLVER_CATTLEMAN);
	}

	if (PED::IS_PED_DEAD_OR_DYING(Conductor, true) && TrainHijackTime != 0)
	{
		TrainHijackTime = 0;
	}

	if (stopped && driver != PlayerPed)
	{
		DioTrace("Stopped and player is out, continuing");
		VEHICLE::SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(TrainHandle, false);
		PLAYER::SET_PLAYER_CONTROL(0, true, 256, true);

		RunObjective_1 = false;
		ActivateObjective_2 = true;
	}
	else if (stopped && driver == PlayerPed && !PlayerLeavingTrain)
	{
		DioTrace("Player exiting");
		PlayerLeavingTrain = true;
		PLAYER::SET_PLAYER_CONTROL(0, false, 256, true);
		TASK::TASK_LEAVE_ANY_VEHICLE(PlayerPed, 0, 0);
		VEHICLE::_SET_TRAIN_HALT(TrainHandle);
		VEHICLE::_SET_TRAIN_MAX_SPEED(TrainHandle, 0);
	}
	else if (!stopped && driver == PlayerPed && !TrainHaltingPlayer)
	{
		DioTrace("Player entered train");
		TrainHaltingPlayer = true;
	}
	else if (TrainHaltingDriver || !ENTITY::DOES_ENTITY_EXIST(driver) || PED::IS_PED_DEAD_OR_DYING(driver, true))
	{
		if (TrainHaltingDriver && ENTITY::DOES_ENTITY_EXIST(driver) && !PED::IS_PED_DEAD_OR_DYING(driver, true))
		{
			VEHICLE::_SET_TRAIN_HALT(TrainHandle);
			ActiveTrainSpeed -= 0.03f;
		}

		VEHICLE::SET_TRAIN_CRUISE_SPEED(TrainHandle, ActiveTrainSpeed);
		VEHICLE::SET_TRAIN_SPEED(TrainHandle, ActiveTrainSpeed);
	}

	for (Ped civ : Passengers)
	{
		if (!PED::IS_PED_USING_ANY_SCENARIO(civ))
		{
			TASK::TASK_USE_NEAREST_TRAIN_SCENARIO_TO_COORD_WARP(civ, ENTITY::GET_ENTITY_COORDS(civ, true, true), 5.0f);
			PED::_SET_PED_SCARED_WHEN_USING_SCENARIO(civ, true);
			PED::SET_PED_PANIC_EXIT_SCENARIO(civ, ENTITY::GET_ENTITY_COORDS(PlayerPed, true, true));
		}
	}
}

void RobberyTrain::ObjectiveInit_2()
{
	ObjectiveSet(2);
	ActivateObjective_2 = false;
	RunObjective_2 = true;

	CurrentMusicStage = STAGE_AFTERACTION;

	for (int i = 0; i < SpawnedGangMembers.size(); ++i)
	{
		TASK::CLEAR_PED_TASKS_IMMEDIATELY(SpawnedGangMembers[i].member, true, true);
		ENTITY::SET_ENTITY_INVINCIBLE(SpawnedGangMembers[i].member, false);
		ENTITY::SET_ENTITY_COORDS(SpawnedGangMembers[i].member,
			Common::ApplyRandomSmallOffsetToCoords(VEHICLE::_GET_NEAREST_TRAIN_TRACK_POSITION(ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(TrainCars[TRAINCAR_9_CABOOSE], {0, -35, 0})), 1.5f),
			true, true, true, false);
		PED::REGISTER_HATED_TARGETS_AROUND_PED(SpawnedGangMembers[i].member, 200.0f);
		TASK::TASK_COMBAT_HATED_TARGETS(SpawnedGangMembers[i].member, 200.0f);
	}

	SetCompanionPlayerCollision(false);

	CheckAbandon = true;
	AbandonCoords = ENTITY::GET_ENTITY_COORDS(Locomotive, true, true);
	AbandonRadius = 170.0f;

	MAP::REMOVE_BLIP(&TrainBlip);

	if (PED::GET_VEHICLE_PED_IS_IN(PlayerPed, false) == Locomotive || PED::GET_VEHICLE_PED_IS_ENTERING(PlayerPed) == Locomotive)
	{
		TASK::CLEAR_PED_TASKS_IMMEDIATELY(PlayerPed, true, true);
	}

	if (!PED::IS_PED_DEAD_OR_DYING(Conductor, true))
	{
		int sequence;
		TASK::OPEN_SEQUENCE_TASK(&sequence);

		TASK::TASK_LEAVE_ANY_VEHICLE(0, 0, 0);
		TASK::TASK_COWER(0, -1, PlayerPed, 0);

		TASK::CLOSE_SEQUENCE_TASK(sequence);
		TASK::TASK_PERFORM_SEQUENCE(Conductor, sequence);
		TASK::CLEAR_SEQUENCE_TASK(&sequence);

		EVENT::_SET_DECISION_MAKER_DEFAULT(Conductor);
		TASK::TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Conductor, false);
		PED::SET_PED_CONFIG_FLAG(Conductor, PCF_DisableTalkTo, false);
	}

	for (int i = 0; i < Passengers.size(); ++i)
	{
		Vector3 pos = ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Passengers[i], 0, 0.5f, 0);
		ENTITY::DELETE_ENTITY(&Passengers[i]);

		Ped civ = SpawnPed(Common::Format("A_%s_M_UPPERTRAINPASSENGERS_01", rand() % 100 < 35 ? "F" : "M"), pos, 0, 255);

		PED::SET_PED_RELATIONSHIP_GROUP_HASH(civ, REL_CIV);

		Common::SetPedHonorModifier(civ, HONOR_NONE);

		PED::SET_PED_CONFIG_FLAG(civ, 225, false);
		PED::SET_PED_CONFIG_FLAG(civ, 130, false);
		PED::SET_PED_CONFIG_FLAG(civ, 178, true);
		PED::SET_PED_CONFIG_FLAG(civ, 317, true);
		PED::SET_PED_CONFIG_FLAG(civ, 356, true);
		PED::SET_PED_CONFIG_FLAG(civ, 406, true);
		PED::SET_PED_FLEE_ATTRIBUTES(civ, 32768, true);
		WEAPON::REMOVE_ALL_PED_WEAPONS(civ, true, true);
		PED::_SET_PED_INTERACTION_PERSONALITY(civ, MISC::GET_HASH_KEY("SCRIPTEDTIMIDROB"));
		PED::_SET_PED_MOTIVATION(civ, 8, 1000.0f, PlayerPed);
		PED::_SET_PED_ACTION_DISABLE_FLAG(civ, 13);
		PED::_SET_PED_GRAPPLE_FLAG(civ, 1, 0);
		//func_1966(civ, func_734(joaat("REWARD_PED_SMALL"), 0, -1));
		TASK::_0x41D1331AFAD5A091(civ, 2, 0);
		TASK::_0x41D1331AFAD5A091(civ, 1, 0);
		PED::_REQUEST_MOTION_TYPE_ASSET(MISC::GET_HASH_KEY("INTIMIDATED_ON_KNEES"), civ);
		PED::_REQUEST_MOTION_TYPE_ASSET(MISC::GET_HASH_KEY("INTIMIDATED_ON_ASS"), civ);
		PED::_REQUEST_MOTION_TYPE_ASSET(MISC::GET_HASH_KEY("INTIMIDATED_ON_FEET_REACTION"), civ);
		PED::_0xD355E2F1BB41087E(civ, 5.0f);

		PED::SET_PED_CONFIG_FLAG(civ, PCF_AlwaysRejectPlayerRobberyAttempt, false);
		PED::SET_PED_CONFIG_FLAG(civ, PCF_BlockRobberyInteractionEscape, true);
		PED::SET_PED_CONFIG_FLAG(civ, PCF_AllowRobberyWhenInjured, true);
		PED::SET_PED_CONFIG_FLAG(civ, PCF_ForceOfferItemOnReceivingRobberyInteraction, true);
		PED::SET_PED_CONFIG_FLAG(civ, PCF_AllowInCombatInteractionLockonOnTargetPed, true);
		PED::SET_PED_CONFIG_FLAG(civ, PCF_ForceInteractionLockonOnTargetPed, true);

		PED::SET_PED_CONFIG_FLAG(civ, PCF_DisableTalkTo, false);

		PED::SET_PED_CONFIG_FLAG(civ, PCF_DisableDeadEyeTagging, false);

		TASK::TASK_TURN_PED_TO_FACE_ENTITY(civ, PlayerPed, -1, 0, 0, 0);

		PED::_SET_PED_CROUCH_MOVEMENT(civ, true, 0, true);

		ObjectiveBlipEntity(civ, false, "Passenger");

		Passengers[i] = civ;
	}
}

void RobberyTrain::RobberyLogic()
{
	for (Ped civ : Passengers)
	{
		if (PED::IS_PED_DEAD_OR_DYING(civ, true) || PED::GET_PED_CONFIG_FLAG(civ, PCF_Knockedout, true) || PED::GET_PED_CONFIG_FLAG(civ, PCF_DisableTalkTo, true))
		{
			if (PED::IS_PED_DEAD_OR_DYING(civ, true) || PED::GET_PED_CONFIG_FLAG(civ, PCF_Knockedout, true))
			{
				DioTrace("Passenger killed!");
				PassengersKilled++;
			}

			for (int i = 0; i < Passengers.size(); ++i)
			{
				if (Passengers[i] == civ)
				{
					Passengers.erase(Passengers.begin() + i);

					return;
				}
			}
		}

		bool robStart = PED::GET_PED_CONFIG_FLAG(civ, PCF_DisableDeadEyeTagging, true);
		bool robInProgress = PED::_0xE33F98BD76490ABC(civ, 0, true);

		Entity e;
		PLAYER::GET_PLAYER_INTERACTION_TARGET_ENTITY(0, &e, true, true);

		if (!robInProgress && (PLAYER::IS_PLAYER_FREE_AIMING_AT_ENTITY(0, civ) || civ == e))
		{
			TASK::CLEAR_PED_TASKS(civ, true, false);
		}

		if (robInProgress && !robStart)
		{
			DioTrace("Starting robbery");
			PED::SET_PED_CONFIG_FLAG(civ, PCF_DisableDeadEyeTagging, true);
		}
#ifdef DEBUG_MARKERS
		else if (robInProgress && robStart)
		{
			int iVar0, iVar1, iVar2, bVar3;
			HUD::_GET_COLOR_FROM_NAME(MISC::GET_HASH_KEY("COLOR_YELLOW"), &iVar0, &iVar1, &iVar2, &bVar3);
			GRAPHICS::_DRAW_MARKER(MARKERTYPE_CYLINDER, ENTITY::GET_ENTITY_COORDS(civ, true, true), {0, 0, 0}, {0, 0, 0}, {1, 1, 1}, iVar0, iVar1, iVar2, bVar3, true, false, 2, true, 0, 0, false);
		}
#endif
		else if (robStart && !robInProgress)
		{
			DioTrace("Ending robbery");
			PED::SET_PED_CONFIG_FLAG(civ, PCF_DisableTalkTo, true);

			PassengersRobbed++;

			Blip b = MAP::GET_BLIP_FROM_ENTITY(civ);
			MAP::REMOVE_BLIP(&b);

			TASK::CLEAR_PED_TASKS(civ, true, false);
			TASK::_TASK_INTIMIDATED_2(civ, PlayerPed, 0, 0, 0, 1, 0, 0, 83968);
			PED::_PED_EMOTIONAL_PRESET_LOCO_MOTION(civ, "TaskIntimidated_OnAss", PlayerPed, -1, 4);
			PED::FORCE_PED_AI_AND_ANIMATION_UPDATE(civ, false, false);
			TASK::TASK_LOOK_AT_ENTITY(civ, PlayerPed, -1, 0, 51, 0);
		}
	}
}

void RobberyTrain::ObjectiveLogic_2()
{
	RobberyLogic();
	
	if (PassengersRobbed >= 6 || Passengers.empty())
	{
		RunObjective_2 = false;
		ActivateObjective_3 = true;
	}
}

void RobberyTrain::ObjectiveInit_3()
{
	ObjectiveSet(3);
	ActivateObjective_3 = false;
	RunObjective_3 = true;

	CompanionsRegroup();

	AllowCompanionManualControl = true;

	CheckAbandon = false;

	LAW::_SET_LAW_REGION(0, 0xCC4672FA /*LAW_REGION_CORNWALL*/, 0);

	AllowWanted = true;
	BecomeWanted(CRIME_TRAIN_ROBBERY, 5);

	ActivePursuitParams.ScriptedLawSpawnPoints = {VEHICLE::_GET_NEAREST_TRAIN_TRACK_POSITION(ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Locomotive, 0, 40, 0))};

	CreateScriptedRespondingLawPed(GenericLawLeader, PedType::SIDEARM_REGULAR, HORSE_SADDLER, ActivePursuitParams.ScriptedLawSpawnPoints[0], PlayerCoords);

	for (int j = 0; j < 3; ++j)
	{
		CreateScriptedRespondingLawPed(GenericLawFollower, PedType::REPEATER_EXPERT, HORSE_SADDLER, ActivePursuitParams.ScriptedLawSpawnPoints[0], PlayerCoords);
	}

	CreateScriptedRespondingLawPed(GenericLawFollower, PedType::SNIPER_REGULAR, HORSE_SADDLER, ActivePursuitParams.ScriptedLawSpawnPoints[0], PlayerCoords);

	CurrentMusicStage = STAGE_ACTION1;

	ActivePursuitParams.MusicStageOnSpotted = STAGE_ACTION2;
	PlayerIncognito = true;
	PlayerPursuit = false;
}

void RobberyTrain::ObjectiveLogic_3()
{
	if (!AllPassengersRobbed && Passengers.empty())
	{
		DioTrace("All passengers robbed");
		AllPassengersRobbed = true;
		ObjectiveSet(4);
	}
	else if (!AllPassengersRobbed)
	{
		RobberyLogic();
	}
	
	if (!WantedLogic())
	{
		CurrentMusicStage = STAGE_AFTERACTION;
		UpdateMusic();

		RunObjective_3 = false;
		MissionSuccess();
	}
}

void RobberyTrain::TrackRewards()
{
	RewardBounty_Active = PlayerIncognito ? int(RewardBounty_Potential * 0.25f) : RewardBounty_Potential;
	
	RewardMembers_Active = RewardMembers_Potential;

	RewardCash_Active = PassengersRobbed * 1000;
}

void RobberyTrain::Update()
{
#ifdef DEBUG_MARKERS
	if (ENTITY::DOES_ENTITY_EXIST(Conductor))
	{
		int iVar0, iVar1, iVar2, bVar3;
		HUD::_GET_COLOR_FROM_NAME(MISC::GET_HASH_KEY("COLOR_RED"), &iVar0, &iVar1, &iVar2, &bVar3);

		for (int i = 0; i < TRAINCAR_AMOUNT; ++i)
		{
			Hash marker = MISC::GET_HASH_KEY(Common::Format("MARKERTYPE_NUM_%d", i).c_str());
			Vector3 pos = ENTITY::GET_ENTITY_COORDS(TrainCars[i], true, true);
			GRAPHICS::_DRAW_MARKER(marker, {pos.x, pos.y, pos.z + 6}, {0, 0, 0}, {0, 0, 0}, {2, 2, 2}, iVar0, iVar1, iVar2, bVar3, false, true, 2, false, 0, 0, false);
		}

		if (IsKeyJustUp(VK_NUMPAD0))
		{
			helper = !helper;
			PLAYER::SET_PLAYER_CONTROL(0, !helper, 256, true);
			ENTITY::FREEZE_ENTITY_POSITION(PlayerPed, helper);
		}

		if (helper)
		{
			HUD::_DISPLAY_TEXT(Common::Format("%s %.2f", mode == 0 ? "X" : (mode == 1 ? "Y" : "R"), offset[mode]).c_str(), 0.1f, 0.05f);

			const float step = mode == 2 ? 5.0f : 0.02f;

			if (IsKeyJustUp(VK_NUMPAD7))
			{
				mode = 0;
			}
			else if (IsKeyJustUp(VK_NUMPAD8))
			{
				mode = 1;
			}
			else if (IsKeyJustUp(VK_NUMPAD9))
			{
				mode = 2;
			}
			else if (IsKeyDown(VK_NUMPAD4))
			{
				offset[mode] -= step;
			}
			else if (IsKeyDown(VK_NUMPAD6))
			{
				offset[mode] += step;
			}

			Vector3 pos = ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(TrainCars[helpercar], offset[0], offset[1], 1.15f);
			float rot = ENTITY::GET_ENTITY_HEADING(TrainCars[helpercar]) + offset[2];

			TASK::CLEAR_PED_TASKS_IMMEDIATELY(PlayerPed, true, true);
			ENTITY::_SET_ENTITY_COORDS_AND_HEADING(PlayerPed, pos, rot, true, true, true);

			if (IsKeyJustUp(VK_NUMPAD5))
			{
				DioTrace("{%.2ff, %.2ff}, %.0f", offset[0], offset[1], offset[2]);
			}
		}
	}
#endif

	VEHICLE::SET_RANDOM_TRAINS(false);

	PAD::DISABLE_CONTROL_ACTION(0, INPUT_INTERACT_LOCKON_POS, true);
	PAD::DISABLE_CONTROL_ACTION(1, INPUT_INTERACT_LOCKON_POS, true);

	if (ENTITY::DOES_ENTITY_EXIST(Locomotive) && RunObjective_1)
	{
		Vector3 trainCoords = ENTITY::GET_ENTITY_COORDS(Locomotive, true, true);
		
		if (ZONE::_GET_MAP_ZONE_AT_COORDS(trainCoords, 1) != 0)
		{
			TASK::TASK_LEAVE_ANY_VEHICLE(PlayerPed, 0, 0);
			VEHICLE::SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(TrainHandle, false);

			MissionFailStr = "The train reached a populated area.";
			MissionFail();
		}
		else if (Common::GetDistBetweenCoords(trainCoords, PlayerCoords) > (ENTITY::DOES_ENTITY_EXIST(PED::GET_MOUNT(PlayerPed)) ? 230.0f : 180.0f))
		{
			MissionFailStr = "The train has rolled away.";
			MissionFail();
		}
	}

#ifndef DEBUG_FAST_MISSION
	if (PassengersRobbed < 6 && PassengersKilled >= 9)
	{
		MissionFailStr = "Too many passengers were killed.";
		MissionFail();
	}
#endif

	if (TimeMissionFail > 0 && MISC::GET_GAME_TIMER() - TimeMissionFail > TimeMissionFailDelay)
	{
		DioTrace("Exit Fail Triggered");

		ExitMissionFlag = M_EXIT_FAILURE;
	}
	else if (TimeMissionSuccess > 0 && MISC::GET_GAME_TIMER() - TimeMissionSuccess > TimeMissionSuccessDelay)
	{
		DioTrace("Exit Win Triggered");

		ExitMissionFlag = M_EXIT_SUCCESS;
	}
	else if (ActivateObjective_0)
	{
		ObjectiveInit_0();
	}
	else if (ActivateObjective_1)
	{
		ObjectiveInit_1();
	}
	else if (ActivateObjective_2)
	{
		ObjectiveInit_2();
	}
	else if (ActivateObjective_3)
	{
		ObjectiveInit_3();
	}
	else if (RunObjective_0)
	{
		ObjectiveLogic_0();
	}
	else if (RunObjective_1)
	{
		ObjectiveLogic_1();
	}
	else if (RunObjective_2)
	{
		ObjectiveLogic_2();
	}
	else if (RunObjective_3)
	{
		ObjectiveLogic_3();
	}
}
