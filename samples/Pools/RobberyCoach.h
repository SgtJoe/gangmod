#pragma once

#include "Common.h"
#include "MissionBase.h"

class RobberyCoach : public MissionBase
{
	enum class RobberyCoachType
	{
		Cargo,
		Passengers,
		BankingLegit,
		BankingTrap
	};

	RobberyCoachType ChooseCoachType();

	bool SetMissionParamsForCamp(int camp) override;

	struct RobberyCoachData
	{
		RobberyCoachType CoachType;
		Vector3 AmbushPoint;
		LocationData WagonSpawn;
		Vector3 WagonDestination;
		std::vector<Vector3> LawSpawns;
	};

	std::vector<RobberyCoachData> AllAvailableVariants;
	RobberyCoachData ChosenVariant;

	Blip AmbushBlip = 0;

	std::string WagonModel = "COACH2";
	std::string WagonExtraPropset = "PG_VEH_COACH2_1";

	const std::string WagonManualLootAnim = "coin_exit";
	const std::string WagonManualLootAnimDict = "mech_pickup@loot@vaults@lrg@grab";

	Vehicle Wagon = 0;
	Vector3 WagonCoords = {0, 0, 0};
	Blip WagonBlip = 0;

	Ped WagonDriver = 0;
	std::string WagonGuardModel = "A_M_M_UniCoachGuards_01";
	std::vector<Ped> WagonGuards;

	bool StartedWagonAttack = false;

	std::string MalePassengerModel = "A_M_M_SclFancyTravellers_01";
	std::string FemalePassengerModel = "A_F_M_SclFancyTravellers_01";
	Ped PassengerM = 0;
	Ped PassengerF = 0;

	Prompt LootPromptL = 0;
	Prompt LootPromptR = 0;
	Vector3 LootPosR = {0, 0, 0};
	Vector3 LootPosL = {0, 0, 0};

	void CommonInit() override;
	void TrackRewards() override;

	bool ActivateObjective_0 = true;
	bool ActivateObjective_1 = false;
	bool ActivateObjective_2 = false;
	bool ActivateObjective_3 = false;

	bool RunObjective_0 = false;
	bool RunObjective_1 = false;
	bool RunObjective_2 = false;
	bool RunObjective_3 = false;

	void ObjectiveInit_0();
	void ObjectiveInit_1();
	void ObjectiveInit_2();
	void ObjectiveInit_3();

	void ObjectiveLogic_0();
	void ObjectiveLogic_1();
	void ObjectiveLogic_2();
	void ObjectiveLogic_3();

	void BankCoachLootingInit();
	bool BankCoachLootingUpdate();

public:
	RobberyCoach();
	~RobberyCoach();

	void Setup(int campid, const std::string& gangstr, const std::vector<MissionCompanionInfo>& companions) override;

	void Update() override;

	static const MissionStaticData& GetStaticMissionData();
};
