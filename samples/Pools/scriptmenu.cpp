
#include "precomp.h"
#include "scriptmenu.h"

void DrawGameText(float x, float y, char* str, bool title = false)
{
	//HUD::_DISPLAY_TEXT(MISC::VAR_STRING(10, "LITERAL_STRING", str), x, y);

	const char* font;

	if (title)
	{
		font = "title1";
	}
	else
	{
		font = "body";
	}
	
	std::string Template = Common::Format("<FONT FACE='$%s'>%s</FONT>", font, str);
	const char* InsertedTemplate = MISC::VAR_STRING(10, "LITERAL_STRING", _strdup(Template.c_str()));
	HUD::_DISPLAY_TEXT(MISC::VAR_STRING(42, "COLOR_STRING", 0, InsertedTemplate), x, y);
}

void DrawRect(float lineLeft, float lineTop, float lineWidth, float lineHeight, int r, int g, int b, int a)
{
	GRAPHICS::DRAW_RECT((lineLeft + (lineWidth * 0.5f)), (lineTop + (lineHeight * 0.5f)), lineWidth, lineHeight, r, g, b, a, 0, 0);
}

void MenuItemBase::WaitAndDraw(int ms)
{
	DWORD time = GetTickCount() + ms;
	bool waited = false;
	while (GetTickCount() < time || !waited)
	{
		WAIT(0);
		waited = true;
		if (auto menu = GetMenu())
			menu->OnDraw();
	}
}

void MenuItemBase::SetStatusText(std::string text, int ms)
{
	MenuController* controller;
	if (m_menu && (controller = m_menu->GetController()))
		controller->SetStatusText(text, ms);
}

void MenuItemBase::OnDraw(float lineTop, float lineLeft, bool active, bool titleoverride)
{
	if (titleoverride)
	{
		ColorRgba color = {255, 255, 255, 255}; //active ? m_colorTextActive : m_colorText;
		
		HUD::SET_TEXT_SCALE(0.0, (m_lineHeight * 7.78f) * 1.8f);
		HUD::_SET_TEXT_COLOR(color.r, color.g, color.b, color.a);
		HUD::SET_TEXT_CENTRE(true);
		HUD::SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0);

		lineLeft = (GetLineWidth() * 0.5f) + 0.03f;

		DrawGameText(lineLeft + m_textLeft, lineTop + m_lineHeight / 4.5f, const_cast<char*>(GetCaption().c_str()), true);

		float boxWidth = GetLineWidth() * 0.8f;
		//float boxWidth = (int)GetCaption().size() * 0.022f;

		GRAPHICS::DRAW_SPRITE("generic_textures", "menu_header_1a", lineLeft + m_textLeft, lineTop + m_lineHeight * 0.71f, boxWidth, 0.1f, 0, 255, 255, 255, 255, true);
	}
	else
	{
		ColorRgba color = {255, 255, 255, 255}; //active ? m_colorTextActive : m_colorText;
		
		HUD::SET_TEXT_SCALE(0.0, m_lineHeight * 6.5f);
		HUD::_SET_TEXT_COLOR(color.r, color.g, color.b, color.a);
		HUD::SET_TEXT_CENTRE(0);
		HUD::SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0);
		
		DrawGameText(lineLeft + m_textLeft, lineTop + m_lineHeight / 4.5f, const_cast<char*>(GetCaption().c_str()));

		float bPx = (GetLineWidth() * 0.5f) + 0.03f;
		float bPy = lineTop + (GetLineHeight() * 0.5f);

		float bSx = GetLineWidth() * 0.95f;
		float bSy = GetLineHeight() * 0.85f;

		if (active)
		{
			GRAPHICS::DRAW_SPRITE("generic_textures", "hud_menu_5a", bPx, bPy, bSx * 1.01f, bSy * 1.15f, 0, 255, 0, 0, 150, false);
		}

		GRAPHICS::DRAW_SPRITE("generic_textures", "hud_menu_5a", bPx, bPy, bSx, bSy, 0, 25, 25, 25, 255, false);
	}
}

float MenuItemTitle::GetLineHeight()
{
	return MenuItemBase::GetLineHeight() * 1.8f;
}

void MenuItemTitle::OnDraw(float lineTop, float lineLeft, bool active)
{
	MenuItemBase::OnDraw(lineTop, lineLeft, active, true);
}

void MenuItemSwitchable::OnDraw(float lineTop, float lineLeft, bool active)
{
	MenuItemDefault::OnDraw(lineTop, lineLeft, active);
	float lineWidth = GetLineWidth();
	float lineHeight = GetLineHeight();
	ColorRgba color = active ? GetColorTextActive() : GetColorText();
	HUD::SET_TEXT_SCALE(0.0, lineHeight * 8.0f);
	HUD::_SET_TEXT_COLOR(color.r, color.g, color.b, static_cast<int>(color.a / 1.1f));
	HUD::SET_TEXT_CENTRE(0);
	HUD::SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0);
	DrawGameText(lineLeft + lineWidth - lineWidth / 6.35f, lineTop + lineHeight / 4.8f, GetState() ? "[Y]" : "[N]");
}

void MenuItemMenu::OnDraw(float lineTop, float lineLeft, bool active)
{
	MenuItemDefault::OnDraw(lineTop, lineLeft, active);
	float lineWidth = GetLineWidth();
	float lineHeight = GetLineHeight();
	ColorRgba color = active ? GetColorTextActive() : GetColorText();
	HUD::SET_TEXT_SCALE(0.0, lineHeight * 8.0f);
	HUD::_SET_TEXT_COLOR(color.r, color.g, color.b, color.a / 2);
	HUD::SET_TEXT_CENTRE(0);
	HUD::SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0);
	DrawGameText(lineLeft + lineWidth - lineWidth / 8, lineTop + lineHeight / 3.5f, "*");
}

void MenuItemMenu::OnSelect()
{
	if (auto parentMenu = GetMenu())
		if (auto controller = parentMenu->GetController())
			controller->PushMenu(m_menu);
}

void MenuBase::OnDraw()
{
	float lineTop = MenuBase_menuTop;
	float lineLeft = MenuBase_menuLeft;

	if (m_itemTitle->GetClass() == eMenuItemClass::ListTitle)
	{
		reinterpret_cast<MenuItemListTitle*>(m_itemTitle)->SetCurrentItemInfo(GetActiveItemIndex() + 1, static_cast<int>(m_items.size()));
	}

	m_itemTitle->OnDraw(lineTop, lineLeft, false);
	
	lineTop += m_itemTitle->GetLineHeight();
	
	for (int i = 0; i < MenuBase_linesPerScreen; i++)
	{
		int itemIndex = m_activeScreenIndex * MenuBase_linesPerScreen + i;
		if (itemIndex == m_items.size())
			break;
		MenuItemBase* item = m_items[itemIndex];
		item->OnDraw(lineTop, lineLeft, m_activeLineIndex == i);
		lineTop += item->GetLineHeight() - item->GetLineHeight() * MenuBase_lineOverlap;
	}
}

const float px = 0.72f;
const float py = 0.37f;

const float sx = 0.44f;
const float sy = 0.45f;

void GangSelectMenu::OnDraw()
{
	MenuBase::OnDraw();

	const std::vector<std::string> alldict = {
		"pausemenu_player",
		"frontend_feed",
		"cmpndm_gangs",
		"pausemenu_textures",
		"sp_missions_9",
		"sp_missions_22"
	};

	int dict = 0;
	const char* pic = "undiscovered";

	for (int i = 0; i < alldict.size(); i++)
	{
		if (!TXD::HAS_STREAMED_TEXTURE_DICT_LOADED(alldict[i].c_str()))
		{
			TXD::REQUEST_STREAMED_TEXTURE_DICT(alldict[i].c_str(), 0);

			while (!TXD::HAS_STREAMED_TEXTURE_DICT_LOADED(alldict[i].c_str()))
			{
				WAIT(0);
			}
		}
	}

	switch (GetActiveItemIndex())
	{
		case GANG_ODRISCOLLS:
			dict = 2;
			pic = "cmpndm_odriscolls_photo";
			break;

		case GANG_EXCONFEDS:
			dict = 2;
			pic = "cmpndm_lemoyne_raiders_photo";
			break;

		case GANG_WAPITI:
			dict = 4;
			pic = "mission_ntv3";
			break;

		case GANG_SKINNERS:
			dict = 2;
			pic = "cmpndm_skinner_brothers_photo";
			break;

		case GANG_MEXICANS:
			dict = 2;
			pic = "cmpndm_del_lobos_photo";
			break;

		case GANG_DUTCH:
			dict = 5;
			pic = "mission_irdtpi";
			break;

		case GANG_CUSTOM:
			dict = 1;
			pic = "series_12_player_bkg";
			break;

		default:
			dict = 0;
			pic = "undiscovered";
	}

	GRAPHICS::DRAW_SPRITE("pausemenu_textures", "menu_ink_4", px, py, sx, sy, 0, 0, 0, 0, 255, false);
	GRAPHICS::DRAW_SPRITE(alldict[dict].c_str(), pic, px, py, sx * 0.92f, sy * 0.86f, 0, 255, 255, 255, 255, false);
}

void LocSelectMenu::OnDraw()
{
	MenuBase::OnDraw();

	const std::vector<std::string> alldict = {
		"pausemenu_player",
		"sp_missions_1",
		"sp_missions_9",
		"sp_missions_3",
		"pausemenu_textures"
	};

	int dict = 0;
	const char* pic = "undiscovered";

	for (int i = 0; i < alldict.size(); i++)
	{
		if (!TXD::HAS_STREAMED_TEXTURE_DICT_LOADED(alldict[i].c_str()))
		{
			TXD::REQUEST_STREAMED_TEXTURE_DICT(alldict[i].c_str(), 0);

			while (!TXD::HAS_STREAMED_TEXTURE_DICT_LOADED(alldict[i].c_str()))
			{
				WAIT(0);
			}
		}
	}

	const std::string loc = GetCurrentItem()->GetCaption();

	if (loc == "West Grizzlies, Ambarino")
	{
		dict = 1;
		pic = "mission_wnt2";
	}
	else if (loc == "Cumberland Forest, New Hanover")
	{
		dict = 2;
		pic = "mission_rmnr1";
	}
	else if (loc == "Bluewater Marsh, Lemoyne")
	{
		dict = 3;
		pic = "mission_gry1";
	}
	else if (loc == "[JOHN ONLY] Rio Bravo, New Austin")
	{
		dict = 0;
		pic = "player_john_toxicity";
	}

	GRAPHICS::DRAW_SPRITE("pausemenu_textures", "menu_ink_4", px, py, sx, sy, 0, 0, 0, 0, 255, false);
	GRAPHICS::DRAW_SPRITE(alldict[dict].c_str(), pic, px, py, sx * 0.92f, sy * 0.86f, 0, 255, 255, 255, 255, false);
}

int MenuBase::OnInput()
{
	const int itemCount = static_cast<int>(m_items.size());
	const int itemsLeft = itemCount % MenuBase_linesPerScreen;
	const int screenCount = itemCount / MenuBase_linesPerScreen + (itemsLeft ? 1 : 0);
	const int lineCountLastScreen = itemsLeft ? itemsLeft : MenuBase_linesPerScreen;

	auto buttons = MenuInput::GetButtonState();

	int waitTime = 0;

	if (buttons.a || buttons.b || buttons.up || buttons.down)
	{
		MenuInput::MenuInputBeep();
		waitTime = buttons.b ? 200 : 150;
	}

	if (buttons.a)
	{
		int activeItemIndex = GetActiveItemIndex();
		m_items[activeItemIndex]->OnSelect();
	}
	else
		if (buttons.b)
		{
			if (auto controller = GetController())
				controller->PopMenu();
		}
		else
			if (buttons.up)
			{
				if (m_activeLineIndex-- == 0)
				{
					if (m_activeScreenIndex == 0)
					{
						m_activeScreenIndex = screenCount - 1;
						m_activeLineIndex = lineCountLastScreen - 1;
					}
					else
					{
						m_activeScreenIndex--;
						m_activeLineIndex = MenuBase_linesPerScreen - 1;
					}
				}
			}
			else
				if (buttons.down)
				{
					m_activeLineIndex++;
					if (m_activeLineIndex == ((m_activeScreenIndex == (screenCount - 1)) ? lineCountLastScreen : MenuBase_linesPerScreen))
					{
						if (m_activeScreenIndex == screenCount - 1)
							m_activeScreenIndex = 0;
						else
							m_activeScreenIndex++;
						m_activeLineIndex = 0;
					}
				}

	return waitTime;
}

void MenuController::DrawStatusText()
{
	if (GetTickCount() < m_statusTextMaxTicks)
	{
		HUD::SET_TEXT_SCALE(0.55, 0.55);
		HUD::_SET_TEXT_COLOR(255, 255, 255, 255);
		HUD::SET_TEXT_CENTRE(1);
		HUD::SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0);
		DrawGameText(0.5, 0.5, const_cast<char*>(m_statusText.c_str()));
	}
}