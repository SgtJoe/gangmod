#pragma once

#include "Common.h"
#include "MissionBase.h"

class RobberyStore : public MissionBase
{
	const Hash CashRegisterHash = 0xC1CE73D1;
	const Hash CashRegisterHashAlt1 = 0xB5705435;
	const Hash CashRegisterHashAlt2 = 0xFE3267B8;
	
	struct StoreInfo
	{
		Vector3 StoreEntryPos;

		std::string ShopkeeperModel;
		Vector3 ShopkeeperPos;
		float ShopkeeperRot;
		bool ShopkeeperAggressive;

		Vector3 RegisterPos;
		float RegisterRot;
		int RegisterAmount;

		Vector3 GangPositions[2];

		std::vector<Hash> DoorsToUnlock;
	};
	
	struct TownInfo
	{
		std::vector<StoreInfo> Shops;
		Hash TownScriptHash;
		int TownRegion;
		int TownDistrict;
		Vector3 TownCenter;
		float TownRadius;
		LocationData StartLoc;
	};
	
	const TownInfo ValentineTownInfo = 
	{
		{
			// General Store
			{
				{-319.82f, 796.81f, 116.92f},
				"A_M_M_VALFARMER_01", {-324.10f, 803.44f, 117.88f}, 194, false,
				{-323.34f, 804.04f, 117.93f}, -80, 2500,
				{{-324.15f, 797.94f, 117.88f}, {-322.14f, 802.97f, 117.88f}},
				{706990067}
			},
			// Hotel
			{
				{-326.97f, 776.89f, 116.44f},
				"A_M_M_HTLFANCYTRAVELLERS_01", {-325.36f, 773.09f, 117.44f}, 10, false,
				{-326.32f, 773.64f, 117.49f}, 10, 2700,
				{{-323.15f, 773.93f, 117.44f}, {-329.81f, 774.12f, 117.44f}},
				{1879307167}
			},
			// Doctor
			{
				{-283.33f, 804.02f, 119.38f},
				"A_M_M_HTLFANCYTRAVELLERS_01", {-288.00f, 803.92f, 119.38f}, -80, false,
				{-287.40f, 805.17f, 119.44f}, -80, 3200,
				{{-285.38f, 804.14f, 119.38f}, {-284.66f, 807.48f, 119.38f}},
				{3588026089}
			},
			// Gunsmith
			{
				{-282.94f, 784.50f, 119.38f},
				"S_M_M_VALCOWPOKE_01", {-282.13f, 778.84f, 119.38f}, 0, true,
				{-280.30f, 779.74f, 119.55f}, 0, 4000,
				{{-283.03f, 780.76f, 119.38f}, {-280.23f, 783.43f, 119.38f}},
				{475159788}
			}
		},
		MISC::GET_HASH_KEY("valentine"),
		REGION_HRT_VALENTINE,
		DISTRICT_HEARTLAND,
		{-303.53f, 789.64f, 116.91f},
		110.0f,
		{{-384.71f, 753.18f, 114.50f}, 294}
	};

	const TownInfo SaintDenisTownInfo =
	{
		{
			// Tailor
			{
				{2552.36f, -1175.39f, 52.31f},
				"G_M_M_UNIBRONTEGOONS_01", {2554.96f, -1166.90f, 52.68f}, 178, true,
				{2555.87f, -1167.70f, 53.73f}, 180, 4000,
				{{2555.90f, -1171.34f, 52.68f}, {2550.49f, -1166.33f, 52.68f}},
				{2611615676}
			},
			// Doctor
			{
				{2716.46f, -1229.47f, 49.37f},
				"A_M_M_HTLFANCYTRAVELLERS_01", {2721.35f, -1231.82f, 49.37f}, 90, false,
				{2720.58f, -1232.96f, 50.42f}, 90, 3000,
				{{2718.09f, -1232.73f, 49.37f}, {2718.38f, -1227.56f, 49.37f}},
				{79213682, 82263429}
			},
			// Gunsmith
			{
				{2719.90f, -1282.75f, 48.63f},
				"A_M_M_SDCHINATOWN_01", {2716.81f, -1287.06f, 48.64f}, 25, false,
				{2717.70f, -1285.79f, 49.68f}, 25, 3000,
				{{2720.24f, -1286.31f, 48.63f}, {2713.43f, -1285.86f, 48.63f}},
				{1057071735, 841127028}
			},
			// General Store
			{
				{2824.12f, -1314.41f, 45.76f},
				"S_M_M_MARKETVENDOR_01", {2824.57f, -1319.49f, 45.76f}, 320, false,
				{2825.68f, -1319.38f, 46.81f}, 320, 3500,
				{{2826.33f, -1314.38f, 45.76f}, {2829.37f, -1316.20f, 45.76f}},
				{1051874490, 3986240381, 4114891219, 4234072328}
			}
		},
		MISC::GET_HASH_KEY("saintdenis"),
		REGION_BAY_SAINT_DENIS,
		DISTRICT_BAYOU_NWA,
		{2710.08f, -1243.77f, 49.14f},
		250.0f,
		{{2660.43f, -1086.79f, 46.93f}, 189}
	};

	const TownInfo TumbleweedTownInfo =
	{
		{
			// General Store
			{
				{-5491.00f, -2941.79f, -1.40f},
				"A_M_M_VALFARMER_01", {-5486.13f, -2937.86f, -1.40f}, 127, false,
				{-5486.97f, -2937.98f, -0.36f}, 127.0f, 3000,
				{{-5487.66f, -2935.05f, -1.40f}, {-5491.07f, -2938.54f, -1.40f}}
			},
			// Gunsmith
			{
				{-5509.92f, -2961.93f, -1.63f},
				"S_M_M_VALCOWPOKE_01", {-5506.50f, -2963.77f, -1.64f}, 105, true,
				{-5507.03f, -2964.93f, -0.59f}, 105.0f, 4000,
				{{-5506.90f, -2961.75f, -1.63f}, {-5509.17f, -2968.56f, -1.63f}},
				{1128355328}
			}
		},
		MISC::GET_HASH_KEY("tumbleweed"),
		REGION_GAP_TUMBLEWEED,
		DISTRICT_GAPTOOTH_RIDGE,
		{-5502.01f, -2948.15f, -2.71f},
		60.0f,
		{{-5459.10f, -2947.14f, -2.02f}, 91}
	};

	TownInfo CurrentTownInfo;
	StoreInfo CurrentShop;

	Ped Shopkeeper = 0;
	Object CashRegister = 0;
	Blip CashRegisterBlip = 0;

	void EnableRegisterScenarios(bool on);

	std::vector<Blip> SpawnedEntryBlips;

	const std::string RegisterLootAnimPed = "enter_picklockoxxo";
	const std::string RegisterLootAnimObj = "enter_picklockoxxo_reg";
	const std::string RegisterLootAnimDict = "mech_pickup@loot@cash_register@open_grab";

	Prompt LootPrompt = 0;

	bool SetMissionParamsForCamp(int camp) override;

	void CommonInit() override;
	void TrackRewards() override;

	bool ActivateObjective_0 = true;
	bool ActivateObjective_1 = false;
	bool ActivateObjective_2 = false;

	bool RunObjective_0 = false;
	bool RunObjective_1 = false;
	bool RunObjective_2 = false;

	void ObjectiveInit_0();
	void ObjectiveInit_1();
	void ObjectiveInit_2();

	void ObjectiveLogic_0();
	void ObjectiveLogic_1();
	void ObjectiveLogic_2();

public:
	RobberyStore();
	~RobberyStore();

	void Setup(int campid, const std::string& gangstr, const std::vector<MissionCompanionInfo>& companions) override;

	void Update() override;

	static const MissionStaticData& GetStaticMissionData();
};
