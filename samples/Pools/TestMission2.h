#pragma once

#include "Common.h"
#include "MissionBase.h"

class TestMission2 : public MissionBase
{
	Volume _grizzliesBlocker = 0;

	bool SetMissionParamsForCamp(int camp) override;

	// Location Specific Variables
	Vector3 WagonSpawnPoint = {0, 0, 0};
	float WagonSpawnHeading = 0.0f;

	bool SwitchSpawnSide = false;

	std::string GuardsModel;
	std::string GuardsScenario;

	LocationData LawSpawn1 = {};
	LocationData LawSpawn2 = {};

	// Generic Variables
	const std::string RespondersModel = "S_M_M_MARSHALLSRURAL_01";
	const std::string WagonModel = "STAGECOACH004X";

	Blip AreaBlip = 0;
	const float AreaBlipRadius = 50.0f;

	Object Safe = 0;
	Vector3 SafeCoords = {0, 0, 0};
	float SafeHeading = 0.0f;
	Vector3 SafeInteractionPoint = {0, 0, 0};
	Blip SafeBlip = 0;

	bool SafeExploded = false;
	bool SafeLooted = false;
	bool BombPlanted = false;
	bool ReadyToLoot = false;
	
	enum MissionSpecificCompanionTasks
	{
		COMPTASK_TAKE_COVER = 1,
		COMPTASK_COMMENT_ON_SAFE = 2,
		COMPTASK_LOOTBODIES = 3
	};

	const Hash SafeHash = 0x45FCD11E;
	const std::string SafeOpenDict = "script_story@wnt4@ig@ig_24_gang_looting";
	const std::string SafeOpenAnim = "ig24_safe_open_s_safe01x";

	const std::string LootModel = "p_moneystack01x";
	const std::string LootAnim = "ig13_14_grab_money_front01_player_zero";
	const std::string LootDict = "script_story@mud5@ig@ig_13_14_grab_bag_rob_safe";

	Object SpawnedLoot1 = 0;
	Object SpawnedLoot2 = 0;

	std::vector<Ped> InitialGuards;
	bool GuardsAlerted = false;
	bool GuardsCombat = false;
	int GuardSpotTime = 0;

	Prompt BreachPrompt = 0;
	Prompt LootPrompt = 0;

	void CommonInit() override;
	void TrackRewards() override;

	bool ActivateObjective_0 = true;
	bool ActivateObjective_1 = false;
	bool ActivateObjective_2 = false;
	bool ActivateObjective_3 = false;

	bool RunObjective_0 = false;
	bool RunObjective_1 = false;
	bool RunObjective_2 = false;
	bool RunObjective_3 = false;

	void ObjectiveInit_0();
	void ObjectiveInit_1();
	void ObjectiveInit_2();
	void ObjectiveInit_3();

	void ObjectiveLogic_0();
	void ObjectiveLogic_1();
	void ObjectiveLogic_2();
	void ObjectiveLogic_3();

public:
	TestMission2();
	~TestMission2();

	void Setup(int campid, const std::string& gangstr, const std::vector<MissionCompanionInfo>& companions) override;

	void Update() override;

	static const MissionStaticData& GetStaticMissionData();
};
