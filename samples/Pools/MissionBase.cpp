
#include "precomp.h"
#include "MissionBase.h"

void MissionBase::LoadAllMissionAssets()
{
	DioScope(DIOTAG);

	for (int i = 0; i < GangMemberTemplates.size(); i++)
	{
		bool duplicate = false;

		for (MissionAsset asset : MissionAssetsToLoad)
		{
			if (GangMemberTemplates[i].CompanionModel == asset.asset)
			{
				duplicate = true;
				break;
			}
		}

		if (!duplicate)
		{
			MissionAssetsToLoad.push_back({"PED", GangMemberTemplates[i].CompanionModel});
		}
	}

	if (!GenericLawLeader.empty())
	{
		MissionAssetsToLoad.push_back({"PED", GenericLawLeader});
	}

	if (!GenericLawFollower.empty() && GenericLawFollower != GenericLawLeader)
	{
		MissionAssetsToLoad.push_back({"PED", GenericLawFollower});
	}

	std::vector<std::string> events = GetAllMusicEvents();

	for (int i = 0; i < events.size(); i++)
	{
		MissionAssetsToLoad.push_back({"MUS", events[i]});
	}

	for (int i = 0; i < MissionAssetsToLoad.size(); i++)
	{
		bool success = false;
		
		MissionAsset loadme = MissionAssetsToLoad[i];
		
		Hash assethash = MISC::GET_HASH_KEY(loadme.asset.c_str());

		if (loadme.type == "PED" || loadme.type == "VEH" || loadme.type == "OBJ")
		{
			if (!STREAMING::IS_MODEL_VALID(assethash) || !STREAMING::IS_MODEL_IN_CDIMAGE(assethash))
			{
				DioTrace(DIOTAG1("ERROR") "BOGUS %s %s", loadme.type.c_str(), loadme.asset.c_str());
			}
			else
			{
				STREAMING::REQUEST_MODEL(assethash, true);

				while (!STREAMING::HAS_MODEL_LOADED(assethash))
				{
					WAIT(1);
				}

				success = true;
			}
		}
		else if (loadme.type == "SET")
		{
			int timeout = 0;
			
			if (STREAMING::IS_MODEL_VALID(assethash) || STREAMING::IS_MODEL_IN_CDIMAGE(assethash))
			{
				DioTrace(DIOTAG1("ERROR") "%s IS NOT A PROPSET", loadme.asset.c_str());
			}
			else
			{
				PROPSET::_REQUEST_PROP_SET(assethash);

				success = true;

				while (!PROPSET::_HAS_PROP_SET_LOADED(assethash))
				{
					timeout++;

					if (timeout > 1000)
					{
						DioTrace(DIOTAG1("ERROR") "TIMEOUT LOADING PROPSET %s", loadme.asset.c_str());
						success = false;
						break;
					}

					WAIT(1);
				}
			}
		}
		else if (loadme.type == "SCN")
		{
			int timeout = 0;

			if (!STREAMING::_HAS_SCENARIO_TYPE_LOADED(assethash, false))
			{
				STREAMING::_REQUEST_SCENARIO_TYPE(assethash, 15, 0, 0);

				success = true;

				while (!STREAMING::_HAS_SCENARIO_TYPE_LOADED(assethash, false))
				{
					timeout++;

					if (timeout > 500)
					{
						DioTrace(DIOTAG1("ERROR") "TIMEOUT LOADING SCENARIO %s", loadme.asset.c_str());
						success = false;
						break;
					}

					WAIT(1);
				}
			}
		}
		else if (loadme.type == "ANM")
		{
			if (!STREAMING::DOES_ANIM_DICT_EXIST(loadme.asset.c_str()))
			{
				DioTrace(DIOTAG1("ERROR") "BAD DICTIONARY %s", loadme.asset.c_str());
			}
			else
			{
				STREAMING::REQUEST_ANIM_DICT(loadme.asset.c_str());

				while (!STREAMING::HAS_ANIM_DICT_LOADED(loadme.asset.c_str()))
				{
					WAIT(1);
				}

				success = true;
			}
		}
		else if (loadme.type == "MUS")
		{
			AUDIO::PREPARE_MUSIC_EVENT(loadme.asset.c_str());
			success = true;
		}
		else if (loadme.type == "AUD")
		{
			AUDIO::PREPARE_SOUNDSET(loadme.asset.c_str(), true);
			success = true;
		}
		else
		{
			DioTrace(DIOTAG1("ERROR") "UNKNOWN ASSET TYPE %s", loadme.type.c_str());
		}

		if (success)
		{
			DioTrace("Loaded %s %s", loadme.type.c_str(), loadme.asset.c_str());
		}
	}

	FinishedLoadingAssets = true;
}

void MissionBase::AddHorseToMissionAssets(Hash horse)
{
	std::vector<std::string> lawhorses = InventoryManager::GetStaticHorseData(horse).PedModels;

	for (int i = 0; i < lawhorses.size(); i++)
	{
		MissionAssetsToLoad.push_back({"PED", lawhorses[i]});
	}
}

void MissionBase::ArmyHelper(Ped spawned, const std::string& name)
{
	if (name == "S_M_M_Army_01")
	{
		PED::_SET_PED_OUTFIT_PRESET(spawned, 52, false);
	}
	else if (name == "S_M_Y_Army_01")
	{
		const int AllowedSkinsLength = 25;
		const int AllowedSkins[AllowedSkinsLength] = {0, 3, 5, 6, 7, 9, 11, 13, 14, 16, 17, 20, 21, 22, 23, 26, 27, 28, 30, 32, 33, 35, 37, 39, 40};
		
		PED::_SET_PED_OUTFIT_PRESET(spawned, AllowedSkins[rand() % AllowedSkinsLength], false);
	}
}

Ped MissionBase::SpawnPed(const std::string& name, const Vector3& pos, float heading, int skin, int customrandmax)
{
	Ped spawned = PED::CREATE_PED(MISC::GET_HASH_KEY(name.c_str()), pos.x, pos.y, pos.z, float(heading), false, true, false, false);

	if (!ENTITY::DOES_ENTITY_EXIST(spawned))
	{
		DioTrace(DIOTAG1("ERROR") "FAILED TO SPAWN %s", name.c_str());
		return NULL;
	}

	ENTITY::SET_ENTITY_AS_MISSION_ENTITY(spawned, true, true);

	SpawnedMissionEntities.emplace_back(spawned);

	DIOTRACE_EXTRA("Setting outfit for %s", name.c_str());
	
	if (skin == 255 && customrandmax > 0)
	{
		DIOTRACE_EXTRA("Custom Randomized Range %d", customrandmax);
		PED::_SET_PED_OUTFIT_PRESET(spawned, rand() % customrandmax, false);
	}
	else if (skin > -1 && skin != 255)
	{
		DIOTRACE_EXTRA("Explicit Outfit %d", skin);
		PED::_SET_PED_OUTFIT_PRESET(spawned, skin, false);
	}
	else
	{
		DIOTRACE_EXTRA("Random Outfit");
		PED::_SET_RANDOM_OUTFIT_VARIATION(spawned, true);
	}

	ArmyHelper(spawned, name);

	PED::_UPDATE_PED_VARIATION(spawned, true, true, true, true, true);

	ENTITY::PLACE_ENTITY_ON_GROUND_PROPERLY(spawned, true);
	
	if (PED::IS_PED_HUMAN(spawned))
	{
		WEAPON::REMOVE_ALL_PED_WEAPONS(spawned, true, true);
		PED::SET_PED_CONFIG_FLAG(spawned, PCF_DisableMountSpawning, true);
	}

	TASK::CLEAR_PED_TASKS_IMMEDIATELY(spawned, true, true);
	TASK::TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(spawned, true);

	Common::SetPedHonorModifier(spawned, HONOR_NONE);

	return spawned;
}

void MissionBase::DefaultHorseSetup(Ped horse)
{
	PED::_SET_PED_BODY_COMPONENT(horse, MISC::GET_HASH_KEY("META_HORSE_SADDLE_ONLY"));
	PED::_SET_PED_FACE_FEATURE(horse, 41611, float(rand() % 2));
}

void MissionBase::DetachAllHorsesFromWagon(Vehicle wagon)
{
	int iVar0 = 0;

	while (iVar0 <= (6 - 1))
	{
		Ped iVar1 = VEHICLE::_GET_PED_IN_DRAFT_HARNESS(wagon, iVar0);

		if (ENTITY::DOES_ENTITY_EXIST(iVar1) /*&& !PED::IS_PED_INJURED(iVar1)*/)
		{
			VEHICLE::_DETACH_DRAFT_VEHICLE_HARNESS_FROM_INDEX(wagon, iVar0);
		}
		iVar0++;
	}

	VEHICLE::BRING_VEHICLE_TO_HALT(wagon, 0, -1, true);
}

void MissionBase::SetPedType(Ped ped, PedType type)
{
	int sidearm = WEAPON_REVOLVER_CATTLEMAN;
	int longarm = 0;
	int health = 1;
	int accuracy = 1;
	int ability = CAL_POOR;
	
	if (type == PedType::LONGARMS_MINIMUM || type >= PedType::MAX_TOTAL)
	{
		DioTrace(DIOTAG1("WARNING") "Invalid PedType %d", static_cast<int>(type));
	}

	switch (type)
	{
		case PedType::SIDEARM_WEAK:
		case PedType::REPEATER_WEAK:
		case PedType::RIFLE_WEAK:
		case PedType::SHOTGUN_WEAK:
		case PedType::SNIPER_WEAK:
			health = 100;
			accuracy = 10;
			ability = CAL_POOR;
			break;

		case PedType::SIDEARM_REGULAR:
		case PedType::REPEATER_REGULAR:
		case PedType::RIFLE_REGULAR:
		case PedType::SHOTGUN_REGULAR:
		case PedType::SNIPER_REGULAR:
			health = 160;
			accuracy = 20;
			ability = CAL_AVERAGE;
			break;

		case PedType::SIDEARM_EXPERT:
		case PedType::REPEATER_EXPERT:
		case PedType::RIFLE_EXPERT:
		case PedType::SHOTGUN_EXPERT:
		case PedType::SNIPER_EXPERT:
			health = 250;
			accuracy = 40;
			ability = CAL_PROFESSIONAL;
			break;
	}

	if (type < PedType::LONGARMS_MINIMUM)
	{
		accuracy = int(accuracy * 1.2);
	}

	switch (type)
	{
		case PedType::SNIPER_WEAK:
			accuracy = 25;
			break;

		case PedType::SNIPER_REGULAR:
			accuracy = 40;
			break;

		case PedType::SNIPER_EXPERT:
			accuracy = 75;
			break;
	}

	switch (type)
	{
		case PedType::SIDEARM_WEAK: sidearm = WEAPON_REVOLVER_CATTLEMAN; break;
		case PedType::SIDEARM_REGULAR: sidearm = WEAPON_REVOLVER_SCHOFIELD; break;
		case PedType::SIDEARM_EXPERT: sidearm = WEAPON_PISTOL_M1899; break;

		case PedType::REPEATER_WEAK: longarm = WEAPON_REPEATER_CARBINE; break;
		case PedType::REPEATER_REGULAR: longarm = WEAPON_REPEATER_WINCHESTER; break;
		case PedType::REPEATER_EXPERT: longarm = WEAPON_REPEATER_HENRY; break;

		case PedType::RIFLE_WEAK: longarm = WEAPON_RIFLE_SPRINGFIELD; break;
		case PedType::RIFLE_REGULAR:
		case PedType::RIFLE_EXPERT: longarm = WEAPON_RIFLE_BOLTACTION; break;

		case PedType::SHOTGUN_WEAK: longarm = WEAPON_SHOTGUN_DOUBLEBARREL; break;
		case PedType::SHOTGUN_REGULAR: longarm = WEAPON_SHOTGUN_PUMP; break;
		case PedType::SHOTGUN_EXPERT: longarm = WEAPON_SHOTGUN_SEMIAUTO; break;

		case PedType::SNIPER_WEAK:
		case PedType::SNIPER_REGULAR: longarm = WEAPON_SNIPERRIFLE_ROLLINGBLOCK; break;
		case PedType::SNIPER_EXPERT: longarm = WEAPON_SNIPERRIFLE_CARCANO; break;
	}
	
	DIOTRACE_EXTRA("TYPE %d SIDE: %s LONG: %s HP: %d, ACC: %d", static_cast<int>(type), Common::HashToStr(sidearm).c_str(), Common::HashToStr(longarm).c_str(), health, accuracy);

	WEAPON::GIVE_WEAPON_TO_PED(ped, sidearm, 6, false, true, WEAPON_ATTACH_POINT_PISTOL_R, false, 0.5f, 1.0f, ADD_REASON_DEFAULT, true, 0.95, false);
	
	if (longarm != 0)
	{
		WEAPON::GIVE_WEAPON_TO_PED(ped, longarm, 6, true, false, WEAPON_ATTACH_POINT_HAND_PRIMARY, false, 0.5f, 1.0f, ADD_REASON_DEFAULT, true, 0.95, false);
	}

	ENTITY::SET_ENTITY_HEALTH(ped, health, 0);

	PED::SET_PED_ACCURACY(ped, accuracy);

	PED::SET_PED_COMBAT_ABILITY(ped, ability);

	WEAPON::SET_PED_DROPS_WEAPONS_WHEN_DEAD(ped, false);
	WEAPON::SET_ALLOW_ANY_WEAPON_DROP(ped, false);
}

Vehicle MissionBase::SpawnVehicle(const std::string& name, const Vector3& pos, float heading, bool nohorses)
{
	Vehicle spawned = 0;
	
	if (nohorses)
	{
		spawned = VEHICLE::CREATE_VEHICLE(MISC::GET_HASH_KEY(name.c_str()), pos.x, pos.y, pos.z, float(heading), true, true, true, true);
	}
	else
	{
		spawned = VEHICLE::_CREATE_DRAFT_VEHICLE(MISC::GET_HASH_KEY(name.c_str()), pos.x, pos.y, pos.z, float(heading), false, true, false, -1, false);
	}

	if (!ENTITY::DOES_ENTITY_EXIST(spawned))
	{
		DioTrace(DIOTAG1("ERROR") "FAILED TO SPAWN %s", name.c_str());
		return NULL;
	}

	ENTITY::SET_ENTITY_AS_MISSION_ENTITY(spawned, true, true);

	SpawnedMissionEntities.emplace_back(spawned);

	ENTITY::PLACE_ENTITY_ON_GROUND_PROPERLY(spawned, true);

	return spawned;
}

Vehicle MissionBase::SpawnTrain(const std::string& name, const Vector3& pos, bool reverseDirection)
{
	Vehicle spawned = 0;

	spawned = VEHICLE::_CREATE_MISSION_TRAIN(MISC::GET_HASH_KEY(name.c_str()), pos, reverseDirection, false, true, false);

	if (!ENTITY::DOES_ENTITY_EXIST(spawned))
	{
		DioTrace(DIOTAG1("ERROR") "FAILED TO SPAWN %s", name.c_str());
		return NULL;
	}

	ENTITY::SET_ENTITY_AS_MISSION_ENTITY(spawned, true, true);

	SpawnedMissionTrains.emplace_back(spawned);

	return spawned;
}

Object MissionBase::SpawnProp(const std::string& name, const Vector3& pos, float heading, bool dynamic)
{
	Object spawned = OBJECT::CREATE_OBJECT_NO_OFFSET(MISC::GET_HASH_KEY(name.c_str()), pos.x, pos.y, pos.z, false, true, dynamic, false);

	if (!ENTITY::DOES_ENTITY_EXIST(spawned))
	{
		DioTrace(DIOTAG1("ERROR") "FAILED TO SPAWN %s", name.c_str());
		return NULL;
	}

	ENTITY::SET_ENTITY_HEADING(spawned, heading);

	ENTITY::SET_ENTITY_AS_MISSION_ENTITY(spawned, true, true);

	SpawnedMissionEntities.emplace_back(spawned);

	return spawned;
}

Object MissionBase::SpawnProp(Hash hash, const Vector3& pos, float heading, bool dynamic)
{
	Object spawned = OBJECT::CREATE_OBJECT_NO_OFFSET(hash, pos.x, pos.y, pos.z, false, true, dynamic, false);

	if (!ENTITY::DOES_ENTITY_EXIST(spawned))
	{
		DioTrace(DIOTAG1("ERROR") "FAILED TO SPAWN %x", hash);
		return NULL;
	}

	ENTITY::SET_ENTITY_HEADING(spawned, heading);

	ENTITY::SET_ENTITY_AS_MISSION_ENTITY(spawned, true, true);

	SpawnedMissionEntities.emplace_back(spawned);

	return spawned;
}

PropSet MissionBase::SpawnPropSet(const std::string& name, const Vector3& pos, float heading)
{
	PropSet spawned = 0;
	
	Hash rootHash = MISC::GET_HASH_KEY("s_crateseat03x");

	STREAMING::REQUEST_MODEL(rootHash, true);

	while (!STREAMING::HAS_MODEL_LOADED(rootHash))
	{
		WAIT(0);
	}

	Object root = SpawnProp(rootHash, pos, heading);

	ENTITY::SET_ENTITY_COLLISION(root, false, false);
	ENTITY::SET_ENTITY_COMPLETELY_DISABLE_COLLISION(root, true, false);
	ENTITY::SET_ENTITY_INVINCIBLE(root, true);
	ENTITY::SET_ENTITY_ALPHA(root, 0, false);
	ENTITY::SET_ENTITY_HEADING(root, heading);
	ENTITY::FREEZE_ENTITY_POSITION(root, true);

	WAIT(500);

	spawned = PROPSET::CREATE_PROP_SET_INSTANCE_ATTACHED_TO_ENTITY(MISC::GET_HASH_KEY(name.c_str()), 0, 0, 0, root, 0, false, 1, false);

	if (!PROPSET::DOES_PROP_SET_EXIST(spawned))
	{
		DioTrace(DIOTAG1("ERROR") "FAILED TO SPAWN %s", name.c_str());
		return NULL;
	}

	return spawned;
}

Volume MissionBase::SpawnVolume(const Vector3& pos, const Vector3& rot, const Vector3& scl, bool navblocker)
{
	Volume spawned = VOLUME::_CREATE_VOLUME_BOX(pos.x, pos.y, pos.z, rot.x, rot.y, rot.z, scl.x, scl.y, scl.z);

	if (!VOLUME::DOES_VOLUME_EXIST(spawned))
	{
		DioTrace(DIOTAG1("ERROR") "FAILED TO SPAWN VOLUME {%.2f %.2f %.2f} {%.2f %.2f %.2f} {%.2f %.2f %.2f}",
			pos.x, pos.y, pos.z, rot.x, rot.y, rot.z, scl.x, scl.y, scl.z);

		return NULL;
	}

	if (navblocker)
	{
		if (!PATHFIND::_ADD_NAVMESH_BLOCKING_VOLUME(spawned, 7))
		{
			DioTrace(DIOTAG1("WARNING") "Failed to add navblocker");
		}
		else
		{
			DIOTRACE_EXTRA(DIOTAG "Successfully created navblocker");
		}
	}

	ENTITY::SET_ENTITY_AS_MISSION_ENTITY(spawned, true, true);

	SpawnedMissionVolumes.emplace_back(spawned);

	return spawned;
}

void MissionBase::SpawnGangMembers(int amount, bool noHorses)
{
	DioScope(DIOTAG);

#ifdef DEBUG_NO_ALLIES
	DioTrace("Spawning zero companions for debug");
	return;
#endif

	if (amount == 0)
	{
		DioTrace(DIOTAG1("WARNING") "Spawning zero companions");
		return;
	}
	
	if (amount > 7)
	{
		DioTrace(DIOTAG1("ERROR") "AMOUNT %d EXCEEDS GROUP LIMIT OF 8 (7 + PLAYER)", amount);
		return;
	}
	
	int templatecount = 0;

	for (const MissionCompanionInfo& info : GangMemberTemplates)
	{
		if (info.IsHuman)
		{
			templatecount++;
		}
	}

	if (templatecount == 0)
	{
		DioTrace(DIOTAG1("ERROR") "NO PED TEMPLATES TO SPAWN COMPANIONS FROM");
		return;
	}
	else if (templatecount < amount)
	{
		DioTrace(DIOTAG1("WARNING") "Not enough templates, reducing amount to %d", templatecount);
		amount = templatecount;
	}

	int PlayerGroup = PLAYER::GET_PLAYER_GROUP(0);

	if (!PED::DOES_GROUP_EXIST(PlayerGroup))
	{
		PlayerGroup = PED::GET_PED_GROUP_INDEX(PlayerPed);
	}

	if (!PED::DOES_GROUP_EXIST(PlayerGroup))
	{
		DioTrace(DIOTAG1("ERROR") "FAILED CREATING PLAYER GROUP");
		return;
	}

	PED::SET_PED_AS_GROUP_LEADER(PlayerPed, PlayerGroup, true);
	PED::SET_GROUP_FORMATION(PlayerGroup, 0);
	PED::SET_GROUP_FORMATION_SPACING(PlayerGroup, 10.0f, -1, -10);
	PED::SET_GROUP_SEPARATION_RANGE(PlayerGroup, 9999999.99);

	const float spacing = 1.5;
	int col = 0;
	int row = 0;

	bool snow = Common::GetDistrictFromCoords(PlayerCoords) == DISTRICT_GRIZZLIES_WEST;

	for (int i = 0; i < amount; ++i)
	{
		std::vector<MissionCompanionInfo> companioninfos;
		int numcompanioninfos = 0;

		for (const MissionCompanionInfo& info : GangMemberTemplates)
		{
			if (!PED::_IS_THIS_MODEL_A_HORSE(MISC::GET_HASH_KEY(info.CompanionModel.c_str())))
			{
				companioninfos.emplace_back(info);
			}
		}

		numcompanioninfos = (int)companioninfos.size();
		
		if (numcompanioninfos <= 0)
		{
			DioTrace(DIOTAG1("ERROR") "COULD NOT FIND ANY VALID PEDS IN COMPANION TEMPLATE LIST");
		}
		else
		{
			MissionCompanionInfo compinfo = companioninfos[i];
			
			std::string voxOverride = !compinfo.CompanionVoices.empty() ? compinfo.CompanionVoices[0] : "___________________";

			row = i / 4;

			Vector3 spawnloc = ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PlayerPed, (float(col) * spacing) - (spacing * 1.5f), (row * -1.5f) - 2.5f, 1.0f);

			col++;
			if (col > 3)
			{
				col = 0;
			}
			
			if (voxOverride == "Custom")
			{
				compinfo.CompanionSkin = -1;
			}

			Ped compped = SpawnPed(compinfo.CompanionModel, spawnloc, ENTITY::GET_ENTITY_HEADING(PlayerPed), compinfo.CompanionSkin);
			
			if (voxOverride.substr(0, 5) == "Dutch")
			{
				_dutchGang = true;
				
				DUTCH_HELPERS::SetPedAppearance(compped, compinfo.CompanionModel, snow, voxOverride.substr(5) == "_Robbery");
			}

			PED::SET_PED_RELATIONSHIP_GROUP_HASH(compped, REL_GANG_DUTCHS);
			PED::SET_PED_LASSO_HOGTIE_FLAG(compped, 0, false);
			PED::SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(compped, false);
			
			Blip compblip = MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_COMPANION, compped);
			MAP::_SET_BLIP_NAME_FROM_PLAYER_STRING(compblip, GangName.c_str());

			SpawnedGangMember spawnedcomp = {compped, COMP_OK};

			std::string vox = "NONE";

			if (PED::IS_PED_HUMAN(compped))
			{
				WEAPON::GIVE_WEAPON_TO_PED(compped, compinfo.CompanionSidearm, 6, false, true, WEAPON_ATTACH_POINT_PISTOL_R, false, 0.5f, 1.0f, 752097756, true, 0.05f, false);
				spawnedcomp.sidearm = compinfo.CompanionSidearm;

				if (compinfo.CompanionLongarm > 0)
				{
					WEAPON::GIVE_WEAPON_TO_PED(compped, compinfo.CompanionLongarm, 6, true, false, WEAPON_ATTACH_POINT_RIFLE, false, 0.5f, 1.0f, 752097756, true, 0.05f, false);
					spawnedcomp.longarm = compinfo.CompanionLongarm;
				}

				spawnedcomp.armor = compinfo.CompanionArmor;

				PED::SET_PED_MAX_HEALTH(compped, compinfo.CompanionHealth);
				ENTITY::SET_ENTITY_HEALTH(compped, compinfo.CompanionHealth, 0);
				PED::SET_PED_ACCURACY(compped, compinfo.CompanionAccuracy);
				PED::SET_PED_COMBAT_MOVEMENT(compped, 2);
				PED::SET_PED_COMBAT_ABILITY(compped, CAL_PROFESSIONAL);

#ifdef DEBUG_OP_ALLIES
				PED::SET_PED_ACCURACY(compped, 100);
				ENTITY::SET_ENTITY_INVINCIBLE(compped, true);
				PED::SET_PED_CAN_RAGDOLL(compped, false);
#endif
#ifdef DEBUG_FRIENDLY_FIRE
				PED::SET_PED_CONFIG_FLAG(compped, PCF_CanBeAttackedByFriendlyPed, true);
#endif
				WEAPON::SET_PED_DROPS_WEAPONS_WHEN_DEAD(compped, false);
				WEAPON::SET_ALLOW_ANY_WEAPON_DROP(compped, false);

				if (voxOverride == "Dutch")
				{
					PED::SET_PED_MAX_HEALTH(compped, 9000);
					ENTITY::SET_ENTITY_HEALTH(compped, 9000, 0);
					ENTITY::SET_ENTITY_INVINCIBLE(compped, true);
				}
				else if (!compinfo.CompanionVoices.empty() && voxOverride != "Custom")
				{
					vox = compinfo.CompanionVoices[rand() % compinfo.CompanionVoices.size()];
					AUDIO::SET_AMBIENT_VOICE_NAME(compped, vox.c_str());
				}

				if (PED::IS_PED_MODEL(compped, MISC::GET_HASH_KEY("G_M_Y_UNIEXCONFEDS_02")))
				{
					AUDIO::SET_AMBIENT_VOICE_NAME(compped, "1026_G_M_Y_UNIEXCONFEDS_01_WHITE_04");
				}
			}
			else
			{
				ENTITY::SET_ENTITY_INVINCIBLE(compped, true);
				PED::SET_PED_CAN_RAGDOLL(compped, false);
			}

			Common::SetEntityProofs(compped, true, true);

			PED::SET_PED_CONFIG_FLAG(compped, PCF_DisableMountSpawning, true);
			PED::_SET_PED_PROMPT_NAME(compped, GangName.c_str());

			DioTrace("Spawned companion %s (%d/%d) - skin %d voice %s carrying %s %s %s acc %d hp %d", 
				compinfo.CompanionModel.c_str(), i+1, amount, compinfo.CompanionSkin, vox.c_str(), 
				Common::HashToStr(spawnedcomp.sidearm).c_str(), Common::HashToStr(spawnedcomp.longarm).c_str(), Common::HashToStr(spawnedcomp.armor).c_str(), 
				compinfo.CompanionAccuracy, compinfo.CompanionHealth);

			PED::SET_PED_AS_GROUP_MEMBER(compped, PlayerGroup);
			PED::SET_PED_CAN_TELEPORT_TO_GROUP_LEADER(compped, PlayerGroup, true);

			DECORATOR::DECOR_SET_INT(compped, "SH_CMP_companion", 1);
			PED::SET_PED_CONFIG_FLAG(compped, PCF_UseFollowLeaderThreatResponse, true);
			
			SpawnedGangMembers.emplace_back(spawnedcomp);
		}
	}

	for (const MissionCompanionInfo& info : GangMemberTemplates)
	{
		if (info.CompanionModel == CampDogModel)
		{
			Ped dog = SpawnPed(CampDogModel, ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PlayerPed, -2, 2, 0), ENTITY::GET_ENTITY_HEADING(PlayerPed), 0);

			PED::SET_PED_RELATIONSHIP_GROUP_HASH(dog, REL_GANG_DUTCHS);
			ENTITY::SET_ENTITY_INVINCIBLE(dog, true);
			PED::SET_PED_CAN_RAGDOLL(dog, false);
			PED::SET_PED_LASSO_HOGTIE_FLAG(dog, 0, false);

			TASK::TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(dog, true);

			Blip compblip = MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_COMPANION, dog);
			MAP::_SET_BLIP_NAME_FROM_PLAYER_STRING(compblip, GangName.c_str());

			DECORATOR::DECOR_SET_INT(dog, "SH_CMP_companion", 1);
			PED::SET_PED_CONFIG_FLAG(dog, PCF_UseFollowLeaderThreatResponse, true);
			
			DioTrace("Spawned companion %s", CampDogModel.c_str());

			SpawnedGangMembers.push_back({dog, COMP_OK});
		}
	}

	if (noHorses)
	{
		DioTrace(DIOTAG "Skipping horse spawns as requested");
		return;
	}

	if (ENTITY::DOES_ENTITY_EXIST(PlayerHorse) && !SpawnedGangMembers.empty())
	{
		PlayerHorseGroup = PED::GET_PED_GROUP_INDEX(PlayerHorse);

		if (!PED::DOES_GROUP_EXIST(PlayerHorseGroup))
		{
			PlayerHorseGroup = PED::CREATE_GROUP(0);
		}

		if (!PED::DOES_GROUP_EXIST(PlayerHorseGroup))
		{
			DioTrace(DIOTAG1("ERROR") "FAILED CREATING HORSE GROUP");
			return;
		}

		PED::SET_PED_AS_GROUP_LEADER(PlayerHorse, PlayerHorseGroup, true);
		PED::SET_GROUP_FORMATION(PlayerHorseGroup, 0);
		PED::SET_GROUP_FORMATION_SPACING(PlayerHorseGroup, 20.0f, -1, -10);
		PED::SET_GROUP_SEPARATION_RANGE(PlayerHorseGroup, 9999999.99);

		std::vector<MissionCompanionInfo> companionhorses;
		int numhorses = 0;

		for (int i = 0; i < templatecount; i++)
		{
			if (PED::_IS_THIS_MODEL_A_HORSE(MISC::GET_HASH_KEY(GangMemberTemplates[i].CompanionModel.c_str())))
			{
				companionhorses.emplace_back(GangMemberTemplates[i]);
			}
		}

		numhorses = (int)companionhorses.size();

		if (numhorses > 0)
		{
			const float spacing_horse = 1.7;
			int col_horse = 0;
			int row_horse = 0;

			std::vector<Ped> NeedsHorse;

			for (int i = 0; i < SpawnedGangMembers.size(); i++)
			{
				if (PED::IS_PED_HUMAN(SpawnedGangMembers[i].member))
				{
					NeedsHorse.emplace_back(SpawnedGangMembers[i].member);
				}
			}

			int adjustedamount = (int)NeedsHorse.size();

			for (int i = 0; i < adjustedamount; i++)
			{
				MissionCompanionInfo chosenhorse = companionhorses[rand() % numhorses];
				
				DioTrace("Spawning companion horse %s (%d/%d)", chosenhorse.CompanionModel.c_str(), i+1, adjustedamount);

				row_horse = i / 4;

				Vector3 spawnloc = ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PlayerHorse, (float(col_horse) * spacing_horse) + spacing_horse, -3.0f * row_horse, 1.0);

				col_horse++;
				if (col_horse > 3)
				{
					col_horse = 0;
				}

				Ped horse = SpawnPed(chosenhorse.CompanionModel, spawnloc, ENTITY::GET_ENTITY_HEADING(PlayerHorse), -1);

				PED::SET_PED_CONFIG_FLAG(horse, PCF_BlockMountHorsePrompt, true);
				PED::SET_PED_CONFIG_FLAG(horse, PCF_DisableHorseGunshotFleeResponse, true);
				PED::SET_PED_CONFIG_FLAG(horse, PCF_DisableHorseShunting, true);
				PED::SET_PED_RELATIONSHIP_GROUP_HASH(horse, REL_GANG_DUTCHS);
				ENTITY::SET_ENTITY_INVINCIBLE(horse, true);
				PED::SET_PED_CAN_RAGDOLL(horse, false);
				PED::SET_PED_LASSO_HOGTIE_FLAG(horse, 0, false);

				DECORATOR::DECOR_SET_BOOL(horse, "HorseCompanion", true);
				DefaultHorseSetup(horse);

				TASK::TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(horse, true);

				if (ENTITY::DOES_ENTITY_EXIST(NeedsHorse[i]))
				{
					for (int j = 0; j < SpawnedGangMembers.size(); j++)
					{
						if (SpawnedGangMembers[j].member == NeedsHorse[i])
						{
							SpawnedGangMembers[j].OwnedHorse = horse;
							//PED::SET_PED_OWNS_ANIMAL(SpawnedGangMembers[j].member, horse, true);
						}
					}
				}
				else
				{
					DioTrace(DIOTAG1("ERROR") "ATTEMPTING TO ASSIGN HORSE TO NONEXISTENT PED");
				}

				PED::SET_PED_AS_GROUP_MEMBER(horse, PlayerHorseGroup);
				PED::SET_PED_CAN_TELEPORT_TO_GROUP_LEADER(horse, PlayerHorseGroup, true);
			}
		}
		else
		{
			DioTrace(DIOTAG1("ERROR") "COULD NOT FIND ANY HORSES IN COMPANION TEMPLATE LIST");
		}
	}
	else
	{
		DioTrace(DIOTAG1("WARNING") "Player doesn't have a saddle horse or we have no companions, not spawning any horses");
	}
}

void MissionBase::PlayPlayerSpeech(const std::string& line)
{
	ScriptedSpeechParams params{line.c_str(), 0, 0, MISC::GET_HASH_KEY("SPEECH_PARAMS_BEAT_SHOUTED_CLEAR_NOSUB"), 0, true, 0, 0};

	AUDIO::PLAY_PED_AMBIENT_SPEECH_NATIVE(PlayerPed, (Any*)&params);
}

void MissionBase::PlayCompanionSpeech(const std::string& line)
{
	if (SpawnedGangMembers.size() == 0)
	{
		return;
	}

	std::string newLine = line;

	if (_dutchGang)
	{
		newLine = DUTCH_HELPERS::ConvertCommentString(line);

		if (newLine.empty())
		{
			DioTrace("Not playing unhandled Dutch chatter %s", line.c_str());
			return;
		}
	}

	struct PedDistanceCombo
	{
		Ped ped;
		float distance;
	};

	PedDistanceCombo ShortestDistance = {0, 999.9f};

	for (int i = 0; i < SpawnedGangMembers.size(); i++)
	{
		Ped testped = SpawnedGangMembers[i].member;
		Vector3 pedpos = ENTITY::GET_ENTITY_COORDS(testped, true, true);
		float distance = BUILTIN::VDIST2(PlayerCoords.x, PlayerCoords.y, PlayerCoords.z, pedpos.x, pedpos.y, pedpos.z);

		if (distance < ShortestDistance.distance && !PED::IS_PED_DEAD_OR_DYING(testped, true))
		{
			ShortestDistance.ped = testped;
			ShortestDistance.distance = distance;
		}
	}

	if (ENTITY::DOES_ENTITY_EXIST(ShortestDistance.ped))
	{
		DioTrace("Playing companion chatter %s from distance: %.2f", newLine.c_str(), ShortestDistance.distance);

		ScriptedSpeechParams params{newLine.c_str(), 0, 0, MISC::GET_HASH_KEY("SPEECH_PARAMS_BEAT_SHOUTED_CLEAR_NOSUB"), 0, true, 0, 0};

		AUDIO::PLAY_PED_AMBIENT_SPEECH_NATIVE(ShortestDistance.ped, (Any*)&params);
	}
	else
	{
		DioTrace("Couldn't find any suitable companions, not playing chatter");
	}
}

void MissionBase::TrackPlayer()
{
	GlobalVarsAccess::SetPlayerFlag(262144);
	
	PlayerPed = PLAYER::PLAYER_PED_ID();
	PlayerCoords = ENTITY::GET_ENTITY_COORDS(PlayerPed, true, true);

	PlayerHorse = PLAYER::_GET_SADDLE_HORSE_FOR_PLAYER(0);
	
	PlayerHorseCoords = ENTITY::GET_ENTITY_COORDS(PlayerHorse, true, true);

	// Moved to SpawnGangMembers
	/*PlayerHorseGroup = PED::GET_PED_GROUP_INDEX(PlayerHorse);

	if (!PED::DOES_GROUP_EXIST(PlayerHorseGroup))
	{
		PlayerHorseGroup = PED::CREATE_GROUP(0);
	}

	PED::SET_PED_AS_GROUP_LEADER(PlayerHorse, PlayerHorseGroup, true);
	PED::SET_GROUP_FORMATION(PlayerHorseGroup, 0);
	PED::SET_GROUP_FORMATION_SPACING(PlayerHorseGroup, 10.0f, -1, -10);
	PED::SET_GROUP_SEPARATION_RANGE(PlayerHorseGroup, 9999999.99);

	if (ENTITY::DOES_ENTITY_EXIST(PED::GET_MOUNT(PlayerPed)) && PED::GET_MOUNT(PlayerPed) != PlayerHorse)
	{
		DioTrace("Swapping PlayerHorse");

		PED::REMOVE_GROUP(PlayerHorseGroup);
		PlayerHorse = PED::GET_MOUNT(PlayerPed);
	}*/
}

void MissionBase::TrackSpawnedGangMembers()
{
	if (SpawnedGangMembers.empty())
	{
		DioTrace(DIOTAG1("ERROR") "CALLING COMPANION TRACKING FUNCTION WITH NO COMPANIONS");
		return;
	}

	for (int i = 0; i < SpawnedGangMembers.size(); i++)
	{
		Ped mped = SpawnedGangMembers[i].member;
		Ped mhorse = SpawnedGangMembers[i].OwnedHorse;
		Vector3 mcoords = ENTITY::GET_ENTITY_COORDS(mped, true, true);
		float mheading = ENTITY::GET_ENTITY_HEADING(mped);

		if (PED::IS_PED_ON_MOUNT(mped) && !PED::IS_PED_ON_MOUNT(PlayerPed) && CompanionsInCombat)
		{
			DioTrace("Shoving companion in combat off mount");
			TASK::CLEAR_PED_TASKS_IMMEDIATELY(mped, true, true);
			TASK::TASK_COMBAT_HATED_TARGETS(mped, 100);
		}
		else if (PED::IS_PED_ON_MOUNT(mped) && PED::GET_MOUNT(mped) != mhorse)
		{
			DioTrace("Shoving companion off non-owned horse");
			TASK::CLEAR_PED_TASKS_IMMEDIATELY(mped, true, true);
		}

		if ((FIRE::IS_ENTITY_ON_FIRE(mped) || PED::IS_PED_DEAD_OR_DYING(mped, true)) && SpawnedGangMembers[i].status == COMP_OK)
		{
			SpawnedGangMembers[i].status = COMP_DEAD_NOTPROCESSED;
			SpawnedGangMembers[i].TimeOfDeath = MISC::GET_GAME_TIMER();

			_currentMissionCasualties.push_back({"", -1, {}, true, -1, -1, SpawnedGangMembers[i].sidearm, SpawnedGangMembers[i].longarm, SpawnedGangMembers[i].armor, SpawnedGangMembers[i].member});

			DIOTRACE_EXTRA("Companion died");

			//ClearArea(mcoords, 3.0);

			Blip compblip = MAP::GET_BLIP_FROM_ENTITY(mped);
			MAP::REMOVE_BLIP(&compblip);
			
			if (ENTITY::DOES_ENTITY_EXIST(SpawnedGangMembers[i].OwnedHorse))
			{
				ENTITY::DELETE_ENTITY(&SpawnedGangMembers[i].OwnedHorse);
			}

			if (FIRE::IS_ENTITY_ON_FIRE(mped))
			{
				SpawnedGangMembers[i].status = COMP_DEAD_FIRE;
			}

			PlayCompanionSpeech("CALLOUT_GETTING_TORN_APART");
		}

		if (PED::IS_PED_DEAD_OR_DYING(mped, true) && SpawnedGangMembers[i].status == COMP_DEAD_NOTPROCESSED && MAP::_DOES_ENTITY_HAVE_BLIP(mped))
		{
			DIOTRACE_EXTRA("Clearing area of dead companion");

			SpawnedGangMembers[i].status = COMP_DEAD;

			//ClearArea(mcoords, 3.0);

			Blip bodyblip = MAP::GET_BLIP_FROM_ENTITY(mped);

			MAP::SET_BLIP_SPRITE(bodyblip, MISC::GET_HASH_KEY("blip_ambient_ped_downed"), true);
			MAP::_SET_BLIP_NAME_FROM_PLAYER_STRING(bodyblip, "Recover Weapons");
			MAP::BLIP_ADD_MODIFIER(bodyblip, BLIP_MODIFIER_POSSE_ALLY);
		}

		if (ENTITY::_IS_ENTITY_FULLY_LOOTED(mped) && SpawnedGangMembers[i].status != COMP_LOOTED)
		{
			DioTrace("Companion looted");
			SpawnedGangMembers[i].status = COMP_LOOTED;

			for (int j = 0; j < _currentMissionCasualties.size(); j++)
			{
				if (_currentMissionCasualties[j].CompanionUniqueID == SpawnedGangMembers[i].member)
				{
					DioTrace("Flagging casualty %d ", j);
					_currentMissionCasualties[j].CompanionUniqueID = COMP_LOOTED;
				}
			}

			// No need to do anything with the blip, game automatically does it for us
		}

		if (SpawnedGangMembers[i].status == COMP_OK &&
			ENTITY::DOES_ENTITY_EXIST(SpawnedGangMembers[i].OwnedHorse) && 
			PED::IS_PED_ON_MOUNT(PlayerPed) &&
			!PED::IS_PED_ON_MOUNT(SpawnedGangMembers[i].member) &&
			INTERIOR::GET_INTERIOR_FROM_ENTITY(SpawnedGangMembers[i].member) == 0 &&
			!CompanionsInCombat
			)
		{
			Vector3 gpos = ENTITY::GET_ENTITY_COORDS(SpawnedGangMembers[i].member, true, true);
			
			Ped horse = SpawnedGangMembers[i].OwnedHorse;

			if (PED::IS_PED_DEAD_OR_DYING(horse, true))
			{
				PED::REVIVE_INJURED_PED(horse);
				PED::_SET_PED_DAMAGE_CLEANLINESS(horse, 2);
				ENTITY::SET_ENTITY_INVINCIBLE(horse, true);
			}
			
			TASK::CLEAR_PED_TASKS_IMMEDIATELY(horse, true, true);

			ENTITY::SET_ENTITY_COORDS(horse, gpos.x, gpos.y, gpos.z + 3, true, true, true, false);

			PED::SET_PED_ONTO_MOUNT(SpawnedGangMembers[i].member, horse, -1, true);

			ENTITY::PLACE_ENTITY_ON_GROUND_PROPERLY(horse, true);
		}
	}
}

void MissionBase::TrackCompanionControl()
{
	if (!AllowCompanionManualControl)
	{
		return;
	}
	
	std::string text = _usingController ? "Press RB to " : "Press " + _companionControlKeyString + " to ";

	if (CompanionManualControlStatus == CompanionControlStatus::FOLLOWING)
	{
		text += CompanionControlAttack;
	}
	else if (CompanionManualControlStatus == CompanionControlStatus::ATTACKING)
	{
		text += CompanionControlRegroup;
	}

	std::string Template = Common::Format("<FONT FACE='$body'>%s</FONT>", text.c_str());
	const char* InsertedTemplate = MISC::VAR_STRING(10, "LITERAL_STRING", _strdup(Template.c_str()));
	
	HUD::SET_TEXT_SCALE(0.0, 0.40f);
	HUD::_SET_TEXT_COLOR(255, 255, 255, 255);
	HUD::SET_TEXT_CENTRE(false);
	HUD::SET_TEXT_DROPSHADOW(3, 255, 255, 255, 255);
	HUD::_DISPLAY_TEXT(MISC::VAR_STRING(42, "COLOR_STRING", 0, InsertedTemplate), 0.62f, 0.96f);

	if (CompanionsControlPressed())
	{
		if (CompanionManualControlStatus == CompanionControlStatus::FOLLOWING)
		{
			CompanionManualControlStatus = CompanionControlStatus::ATTACKING;
			CompanionsBeginCombat(true);

			PlayPlayerSpeech(ENTITY::GET_ENTITY_MODEL(PlayerPed) == MISC::GET_HASH_KEY("PLAYER_ZERO") ? "PLAYER_COMBAT_ENGAGE_GANG" : "PLAYER_REQUEST_ADVANCE");
		}
		else if (CompanionManualControlStatus == CompanionControlStatus::ATTACKING)
		{
			CompanionManualControlStatus = CompanionControlStatus::FOLLOWING;
			CompanionsClearTasks();
		}
	}
}

bool MissionBase::CompanionsControlPressed()
{
	bool pressed = false;
	
	if (!AllowCompanionManualControl)
	{
		DioTrace(DIOTAG "Ignoring press because control is disabled");
		pressed = false;
	}

	if (IsKeyJustUp(_companionControlKey) || (_usingController && PAD::IS_CONTROL_JUST_RELEASED(2, INPUT_FRONTEND_RB)))
	{
		pressed = true;
	}
	
	return pressed;
}

void MissionBase::CompanionsBeginCombat(bool noComment)
{
	CompanionsInCombat = true;
	CompanionManualControlStatus = CompanionControlStatus::ATTACKING;

	for (int i = 0; i < SpawnedGangMembers.size(); i++)
	{
		if (!PED::IS_PED_MODEL(SpawnedGangMembers[i].member, MISC::GET_HASH_KEY(CampDogModel.c_str())))
		{
			TASK::TASK_COMBAT_HATED_TARGETS(SpawnedGangMembers[i].member, 100);
		}
	}

	if (noComment)
	{
		return;
	}

	const int CombatBeginResponseCount = 8;
	const std::string CombatBeginResponses[CombatBeginResponseCount] = {
		"ARRIVAL_COMBAT_NEUTRAL",
		"ARRIVAL_COMBAT_RAID_MALE",
		"ARRIVAL_COMBAT_RAID_NEUTRAL",
		"CALLOUT_JUST_KILL_EM_NEUTRAL",
		"CALLOUT_JUST_KILL_EM_MALE",
		"CALLOUT_EASY_PICKINGS_NEUTRAL",
		"OPENS_FIRE",
		"TAUNT_GEN_NEUTRAL"
	};

	PlayCompanionSpeech(CombatBeginResponses[rand() % CombatBeginResponseCount]);
}

void MissionBase::CompanionsClearTasks()
{
	CompanionsInCombat = false;
	CompanionManualControlStatus = CompanionControlStatus::FOLLOWING;
	
	for (int i = 0; i < SpawnedGangMembers.size(); i++)
	{
		TASK::CLEAR_PED_TASKS(SpawnedGangMembers[i].member, true, true);
	}
}

void MissionBase::CompanionsDisband()
{
	DioTrace("Scattering companions");
	
	for (int i = 0; i < SpawnedGangMembers.size(); i++)
	{
		PED::REMOVE_PED_FROM_GROUP(SpawnedGangMembers[i].member);
	}
}

void MissionBase::CompanionsRegroup()
{
	DioTrace("Regrouping companions");
	
	int PlayerGroup = PLAYER::GET_PLAYER_GROUP(0);

	if (!PED::DOES_GROUP_EXIST(PlayerGroup))
	{
		PlayerGroup = PED::GET_PED_GROUP_INDEX(PlayerPed);
	}

	if (!PED::DOES_GROUP_EXIST(PlayerGroup))
	{
		DioTrace(DIOTAG1("ERROR") "FAILED CREATING PLAYER GROUP");
		return;
	}

	PED::SET_PED_AS_GROUP_LEADER(PlayerPed, PlayerGroup, true);
	PED::SET_GROUP_FORMATION(PlayerGroup, 0);
	PED::SET_GROUP_FORMATION_SPACING(PlayerGroup, 10.0f, -1, -10);
	PED::SET_GROUP_SEPARATION_RANGE(PlayerGroup, 9999999.99);
	
	for (int i = 0; i < SpawnedGangMembers.size(); i++)
	{
		PED::SET_PED_AS_GROUP_MEMBER(SpawnedGangMembers[i].member, PlayerGroup);
		PED::SET_PED_CAN_TELEPORT_TO_GROUP_LEADER(SpawnedGangMembers[i].member, PlayerGroup, true);

		DECORATOR::DECOR_SET_INT(SpawnedGangMembers[i].member, "SH_CMP_companion", 1);
		PED::SET_PED_CONFIG_FLAG(SpawnedGangMembers[i].member, PCF_UseFollowLeaderThreatResponse, true);
	}
}

void MissionBase::SetCompanionPlayerCollision(bool on)
{
	for (int i = 0; i < SpawnedGangMembers.size(); i++)
	{
		ENTITY::SET_ENTITY_NO_COLLISION_ENTITY(SpawnedGangMembers[i].member, PlayerPed, on);
	}
}

std::vector<std::string> MissionBase::GetAllMusicEvents()
{
	if (CurrentMusicTrack == MUSIC_DEFAULT)
	{
		return {
			"MURDER_SCENE_STOP",
			"FSM1_FAIL",
			"MURDER_SCENE_START",
			"CACR01_APPROACH",
			"HIDEOUT_SPC_DOOR_BURST",
			"CACR3_JUMP",
			"HIDEOUT_SPC_CABIN"
		};
	}
	else if (CurrentMusicTrack == MUSIC_SEAN_RESCUE)
	{
		return {
			"FSM1_FAIL",
			"HIDEOUT_SPC_START",
			"HIDEOUT_SPC_ALL_DEAD",
			"HIDEOUT_SPC_ALERT",
			"HIDEOUT_SPC_DOOR_BURST",
			"HIDEOUT_SPC_CABIN"
		};
	}
	else if (CurrentMusicTrack == MUSIC_ODRISCOLLTHEME)
	{
		return {
			"RDST21_GUARDS_RESTART",
			"HIDEOUT_SPC_START",
			"CACR01_ACTION",
			"HIDEOUT_SPC_DOOR_BURST",
			"CACR3_JUMP",
			"HIDEOUT_SPC_ALERT"
		};
	}
	else if (CurrentMusicTrack == MUSIC_SHADYBELLE)
	{
		return {
			"RESAM_START",
			"HIDEOUT_SPC_ALERT",
			"CACR01_ACTION",
			"HIDEOUT_SPC_ALL_DEAD",
		};
	}
	else if (CurrentMusicTrack == MUSIC_PINKERTONS)
	{
		return {
			"GNG1_START_CS",
			"HIDEOUT_SPC_ALL_DEAD",
			"CACR01_APPROACH",
			"CACR3_JUMP",
		};
	}
	else if (CurrentMusicTrack == MUSIC_BASIN)
	{
		return {
			"ODRISC1_RESTART_1",
			"ODRISC1_RIDE_OUT_CME",
			"ODRISC1_SHOOTOUT",
			"ODRISC1_REINFORCEMENTS",
			"ODRISC1_HOGTIE"
		};
	}
	else if (CurrentMusicTrack == MUSIC_SISIKA)
	{
		return {
			"GNG3_RESTART_1",
			"CACR01_APPROACH",
			"CACR01_ACTION",
			"GNG3_OVER_BRIDGE",
			"GNG3_ROW_HOME"
		};
	}
	else
	{
		DioTrace(DIOTAG1("WARNING") "CurrentMusicTrack has no events");
	}
	
	return {};
}

void MissionBase::MusicStartupDelay()
{
//#ifndef DEBUG_FAST_MISSION
	WAIT(3000);
//#endif
}

void MissionBase::UpdateMusic()
{
	if (FailScreenActive)
	{
		AUDIO::TRIGGER_MUSIC_EVENT("STOP_MUSIC_8S");
		CurrentMusicStage = STAGE_START;
		return;
	}
	
	if (CurrentMusicStage == STAGE_MUTED)
	{
		AUDIO::TRIGGER_MUSIC_EVENT("MURDER_SCENE_STOP");
		AUDIO::TRIGGER_MUSIC_EVENT("STOP_MUSIC_8S");
		AUDIO::TRIGGER_MUSIC_EVENT("FSM1_FAIL"); 
		return;
	}

	if (MusicLoopOverride && !MusicOverride)
	{
		return;
	}

	if (CurrentMusicTrack == MUSIC_DEFAULT)
	{
		int district = Common::GetDistrictFromCoords(PlayerCoords);
		
		switch (CurrentMusicStage)
		{
			case STAGE_START:
				DioTrace("Starting up Default");
				AUDIO::TRIGGER_MUSIC_EVENT("MURDER_SCENE_STOP");
				AUDIO::TRIGGER_MUSIC_EVENT("FSM1_FAIL");

				if (district == DISTRICT_GAPTOOTH_RIDGE ||
					district == DISTRICT_RIO_BRAVO ||
					district == DISTRICT_CHOLLA_SPRINGS ||
					district == DISTRICT_HENNIGANS_STEAD)
				{
					DioTrace("Setting New Austin ambience");
					AUDIO::SET_AMBIENT_ZONE_LIST_STATE("AZL_Hideout_Fort_Mercer", true, true);
				}

				MusicStartupDelay();

				AUDIO::TRIGGER_MUSIC_EVENT("MURDER_SCENE_START");
				CurrentMusicStage = STAGE_CALM;
				return;

			case STAGE_CALM:
				AUDIO::TRIGGER_MUSIC_EVENT("CACR01_APPROACH");
				return;

			case STAGE_ACTION1:
				AUDIO::TRIGGER_MUSIC_EVENT("HIDEOUT_SPC_DOOR_BURST");
				return;

			case STAGE_ACTION2:
				AUDIO::TRIGGER_MUSIC_EVENT("CACR3_JUMP");
				return;

			case STAGE_AFTERACTION:
				AUDIO::TRIGGER_MUSIC_EVENT("HIDEOUT_SPC_CABIN");
				return;
		}
	}
	else if (CurrentMusicTrack == MUSIC_SEAN_RESCUE)
	{
		switch (CurrentMusicStage)
		{
			case STAGE_START:
				DioTrace("Starting up Sean Rescue");
				AUDIO::TRIGGER_MUSIC_EVENT("FSM1_FAIL");
				MusicStartupDelay();
				AUDIO::TRIGGER_MUSIC_EVENT("HIDEOUT_SPC_START");
				CurrentMusicStage = STAGE_CALM;
				return;

			case STAGE_CALM:
				AUDIO::TRIGGER_MUSIC_EVENT("HIDEOUT_SPC_ALL_DEAD");
				return;

			case STAGE_ACTION1:
				AUDIO::TRIGGER_MUSIC_EVENT("HIDEOUT_SPC_ALERT");
				return;

			case STAGE_ACTION2:
				AUDIO::TRIGGER_MUSIC_EVENT("HIDEOUT_SPC_DOOR_BURST");
				return;

			case STAGE_AFTERACTION:
				AUDIO::TRIGGER_MUSIC_EVENT("HIDEOUT_SPC_CABIN");
				return;
		}
	}
	else if (CurrentMusicTrack == MUSIC_ODRISCOLLTHEME)
	{
		switch (CurrentMusicStage)
		{
			case STAGE_START:
				DioTrace("Starting up O Driscoll's");
				AUDIO::TRIGGER_MUSIC_EVENT("RDST21_GUARDS_RESTART");
				MusicStartupDelay();
				CurrentMusicStage = STAGE_CALM;
				return;

			case STAGE_CALM:
				AUDIO::TRIGGER_MUSIC_EVENT("CACR01_ACTION");
				return;

			case STAGE_ACTION1:
				AUDIO::TRIGGER_MUSIC_EVENT("HIDEOUT_SPC_DOOR_BURST");
				return;

			case STAGE_ACTION2:
				AUDIO::TRIGGER_MUSIC_EVENT("CACR3_JUMP");
				return;

			case STAGE_AFTERACTION:
				AUDIO::TRIGGER_MUSIC_EVENT("HIDEOUT_SPC_ALERT");
				return;
		}
	}
	else if (CurrentMusicTrack == MUSIC_SHADYBELLE)
	{
		switch (CurrentMusicStage)
		{
		case STAGE_START:
			DioTrace("Starting up Shady Belle Theme");
			AUDIO::TRIGGER_MUSIC_EVENT("RESAM_START");
			MusicStartupDelay();
			CurrentMusicStage = STAGE_CALM;
			return;

		case STAGE_CALM:
			AUDIO::TRIGGER_MUSIC_EVENT("RESAM_START");
			return;

		case STAGE_ACTION1:
			AUDIO::TRIGGER_MUSIC_EVENT("HIDEOUT_SPC_ALERT");
			return;

		case STAGE_ACTION2:
			AUDIO::TRIGGER_MUSIC_EVENT("CACR01_ACTION");
			return;

		case STAGE_AFTERACTION:
			AUDIO::TRIGGER_MUSIC_EVENT("HIDEOUT_SPC_ALL_DEAD");
			return;
		}
	}
	else if (CurrentMusicTrack == MUSIC_PINKERTONS)
	{
		switch (CurrentMusicStage)
		{
			case STAGE_START:
				DioTrace("Starting up Pinkertons");
				AUDIO::TRIGGER_MUSIC_EVENT("GNG1_START_CS");
				MusicStartupDelay();
				CurrentMusicStage = STAGE_CALM;
				return;

			case STAGE_CALM:
				AUDIO::TRIGGER_MUSIC_EVENT("HIDEOUT_SPC_ALL_DEAD");
				return;

			case STAGE_ACTION1:
				AUDIO::TRIGGER_MUSIC_EVENT("CACR01_APPROACH");
				return;

			case STAGE_ACTION2:
				AUDIO::TRIGGER_MUSIC_EVENT("CACR3_JUMP");
				return;

			case STAGE_AFTERACTION:
				AUDIO::TRIGGER_MUSIC_EVENT("HIDEOUT_SPC_ALL_DEAD");
				return;
			}
	}
	else if (CurrentMusicTrack == MUSIC_BASIN)
	{
		switch (CurrentMusicStage)
		{
			case STAGE_START:
				DioTrace("Starting up Ewing Basin");
				AUDIO::TRIGGER_MUSIC_EVENT("ODRISC1_RESTART_1");
				DioTrace(DIOTAG "ODRISC1_RESTART_1");
				MusicStartupDelay();
				break;

			case STAGE_CALM:
				AUDIO::TRIGGER_MUSIC_EVENT("ODRISC1_RIDE_OUT_CME");
				DioTrace(DIOTAG "ODRISC1_RIDE_OUT_CME");
				break;

			case STAGE_ACTION1:
				AUDIO::TRIGGER_MUSIC_EVENT("ODRISC1_SHOOTOUT");
				DioTrace(DIOTAG "ODRISC1_SHOOTOUT");
				break;

			case STAGE_ACTION2:
				AUDIO::TRIGGER_MUSIC_EVENT("ODRISC1_REINFORCEMENTS");
				DioTrace(DIOTAG "ODRISC1_REINFORCEMENTS");
				break;

			case STAGE_AFTERACTION:
				AUDIO::TRIGGER_MUSIC_EVENT("ODRISC1_HOGTIE");
				DioTrace(DIOTAG "ODRISC1_HOGTIE");
				break;
		}

		MusicOverride = false;
	}
	else if (CurrentMusicTrack == MUSIC_SISIKA)
	{
		switch (CurrentMusicStage)
		{
			case STAGE_START:
				DioTrace("Starting up Sisika");
				AUDIO::TRIGGER_MUSIC_EVENT("GNG3_RESTART_1");
				MusicStartupDelay();
				CurrentMusicStage = STAGE_CALM;
				return;

			case STAGE_CALM:
				AUDIO::TRIGGER_MUSIC_EVENT("CACR01_APPROACH");
				return;

			case STAGE_ACTION1:
				AUDIO::TRIGGER_MUSIC_EVENT("CACR01_ACTION");
				return;

			case STAGE_ACTION2:
				AUDIO::TRIGGER_MUSIC_EVENT("GNG3_OVER_BRIDGE");
				return;

			case STAGE_AFTERACTION:
				AUDIO::TRIGGER_MUSIC_EVENT("GNG3_ROW_HOME");
				return;
		}
	}
	else
	{
		DioTrace(DIOTAG1("WARNING") "CurrentMusicTrack unhandled");
	}
}

MissionBase::MissionBase()
{
	DioScope(DIOTAG);
	
	_storyCamps[0] = {-112.00f, -25.07f, 95.69f}; // Horseshoe Overlook
	_storyCamps[1] = {682.23f, -1228.29f, 44.53f}; // Clemens Point
	_storyCamps[2] = {1897.81f, -1863.09f, 43.15f}; // Shady Belle
	_storyCamps[3] = {2271.38f, -759.16f, 42.38f}; // Lakay
	_storyCamps[4] = {2354.30f, 1373.09f, 106.13f}; // Beaver Hollow
	_storyCamps[5] = {-1644.63f, -1367.58f, 83.98f}; // Beecher's Hope

	RewardBounty_Potential = 0; 
	RewardCash_Potential = 0; 
	RewardMembers_Potential = 0;

	RewardBounty_Active = 0;
	RewardCash_Active = 0;
	RewardMembers_Active = 0;

	PlayerPed = PLAYER::PLAYER_PED_ID();
	PlayerCoords = ENTITY::GET_ENTITY_COORDS(PlayerPed, true, true);

	PlayerHorse = PLAYER::_GET_SADDLE_HORSE_FOR_PLAYER(0);
	PlayerHorseCoords = ENTITY::GET_ENTITY_COORDS(PlayerHorse, true, true);

	POPULATION::DISABLE_AMBIENT_ROAD_POPULATION(false);

	_companionControlKey = INI::GetCompanionControlKey();
	_companionControlKeyString = INI::InputKeyToStr(_companionControlKey);

	DioTrace("Assigned control 0x%X - %s", _companionControlKey, _companionControlKeyString.c_str());

	LAW::_PAUSE_BOUNTY_HUNTER_COOLDOWN(MISC::GET_HASH_KEY("BOUNTYHUNTERSGLOBALCOOLDOWN"), true, 0);
}

void MissionBase::ReleaseSpawnedMissionEntities()
{
	DioTrace("Releasing mission entities");
	
	bool instant = false;

	if (ExitMissionFlag == M_EXIT_NONE)
	{
		AUDIO::TRIGGER_MUSIC_EVENT("STOP_MUSIC_8S");
		DioTrace(DIOTAG1("WARNING") "RELEASING ENTITIES WITHOUT MISSION EXIT FLAG");
		instant = true;
	}

	if (ExitMissionFlag == M_EXIT_ERROR_TERMINATE || ExitMissionFlag == M_EXIT_FAILURE_GEN_MISSION_STARTED || ExitMissionFlag == M_EXIT_FAILURE_GEN_ENTERED_CAMP)
	{
		instant = true;
	}

	for (int i = 0; i < SpawnedGangMembers.size(); i++)
	{
		if (PED::IS_PED_DEAD_OR_DYING(SpawnedGangMembers[i].member, true))
		{
			ENTITY::DELETE_ENTITY(&SpawnedGangMembers[i].member);
		}
		else
		{
			SpawnedMissionEntities.emplace_back(SpawnedGangMembers[i].member);
		}

		if (ENTITY::DOES_ENTITY_EXIST(SpawnedGangMembers[i].OwnedHorse))
		{
			SpawnedMissionEntities.emplace_back(SpawnedGangMembers[i].OwnedHorse);
		}
	}
	
	if (instant)
	{
		for (int i = 0; i < SpawnedMissionEntities.size(); i++)
		{
			ENTITY::DELETE_ENTITY(&SpawnedMissionEntities[i]);
		}

		for (int i = 0; i < SpawnedMissionTrains.size(); i++)
		{
			VEHICLE::DELETE_MISSION_TRAIN(&SpawnedMissionTrains[i]);
		}
	}
	else
	{
		for (int i = 0; i < SpawnedMissionEntities.size(); i++)
		{
			Blip b = MAP::GET_BLIP_FROM_ENTITY(SpawnedMissionEntities[i]);
			MAP::REMOVE_BLIP(&b);

			ENTITY::SET_ENTITY_AS_NO_LONGER_NEEDED(&SpawnedMissionEntities[i]);
		}

		for (int i = 0; i < SpawnedMissionTrains.size(); i++)
		{
			VEHICLE::SET_MISSION_TRAIN_AS_NO_LONGER_NEEDED(&SpawnedMissionTrains[i], true);
		}
	}

	// Propsets are automatically deleted when their root object is deleted

	for (int i = 0; i < SpawnedMissionVolumes.size(); i++)
	{
		PATHFIND::_REMOVE_NAVMESH_BLOCKING_VOLUME(SpawnedMissionVolumes[i]);
		VOLUME::_DELETE_VOLUME(SpawnedMissionVolumes[i]);
	}

	for (int i = 0; i < SpawnedMissionBlips.size(); i++)
	{
		MAP::REMOVE_BLIP(&SpawnedMissionBlips[i]);
	}

	SpawnedGangMembers.clear();
	SpawnedMissionEntities.clear();
	SpawnedMissionTrains.clear();
	SpawnedMissionPropsets.clear();
	SpawnedMissionVolumes.clear();
	SpawnedMissionBlips.clear();
}

MissionBase::~MissionBase()
{
	DioScope(DIOTAG);
	
	ReleaseSpawnedMissionEntities();

	ObjectiveClear();

	ClearWanted(false);
	EnableLaw();
	SetDispatchService(true);
	
	PED::CLEAR_RELATIONSHIP_BETWEEN_GROUPS(PED::GET_RELATIONSHIP_BETWEEN_GROUPS(REL_PLAYER, REL_COP), REL_PLAYER, REL_COP);
	PED::CLEAR_RELATIONSHIP_BETWEEN_GROUPS(PED::GET_RELATIONSHIP_BETWEEN_GROUPS(REL_PLAYER, REL_BOUNTY_HUNTER), REL_PLAYER, REL_BOUNTY_HUNTER);

	PED::CLEAR_RELATIONSHIP_BETWEEN_GROUPS(PED::GET_RELATIONSHIP_BETWEEN_GROUPS(REL_GANG_DUTCHS, REL_COP), REL_GANG_DUTCHS, REL_COP);
	PED::CLEAR_RELATIONSHIP_BETWEEN_GROUPS(PED::GET_RELATIONSHIP_BETWEEN_GROUPS(REL_GANG_DUTCHS, REL_BOUNTY_HUNTER), REL_GANG_DUTCHS, REL_BOUNTY_HUNTER);

	POPULATION::ENABLE_AMBIENT_ROAD_POPULATION();

	LAW::_PAUSE_BOUNTY_HUNTER_COOLDOWN(MISC::GET_HASH_KEY("BOUNTYHUNTERSGLOBALCOOLDOWN"), false, 0);
}

void MissionBase::SetVarsFromStaticData(const MissionStaticData& thisMissionData, bool hasMusicOverride)
{
	ThisMissionID = thisMissionData.StaticMissionID;
	RewardBounty_Potential = thisMissionData.PotentialRewardBounty;
	RewardCash_Potential = thisMissionData.PotentialRewardCash;
	RewardMembers_Potential = thisMissionData.PotentialRewardMembers;

	if (!hasMusicOverride)
	{
		CurrentMusicTrack = thisMissionData.MusicType;
	}

	MissionLawOverride = thisMissionData.OverrideCampLaw;
}

void MissionBase::ObjectiveClear()
{
	UILOG::_UILOG_SET_CACHED_OBJECTIVE(" ");
	UILOG::_UILOG_PRINT_CACHED_OBJECTIVE();
	UILOG::_UILOG_CLEAR_CACHED_OBJECTIVE();
}

void MissionBase::ObjectiveSet(int index)
{
	DioTrace("Starting Objective %d", index);

	if (index >= ObjectivesList.size())
	{
		DioTrace(DIOTAG1("ERROR") "MISSION DOES NOT HAVE OBJECTIVE INDEX %d", index);
		ObjectiveClear();
		return;
	}
	
	if (FailScreenActive)
	{
		ObjectiveClear();
		return;
	}

	UILOG::_UILOG_SET_CACHED_OBJECTIVE(ObjectivesList[index].c_str());
	UILOG::_UILOG_PRINT_CACHED_OBJECTIVE();
}

void MissionBase::ObjectiveTempOverride(const std::string& str)
{
	std::string origStr = UILOG::_UILOG_GET_CACHED_OBJECTIVE();

	UILOG::_UILOG_SET_CACHED_OBJECTIVE(str.c_str());
	UILOG::_UILOG_PRINT_CACHED_OBJECTIVE();
	UILOG::_UILOG_SET_CACHED_OBJECTIVE(origStr.c_str());
}

Blip MissionBase::ObjectiveBlipCoords(const Vector3& coords, float radius, bool GPS, const std::string& str)
{
	if (radius < 1.0f)
	{
		radius = 1.0f;
	}

	Blip spawned = MAP::BLIP_ADD_FOR_RADIUS(BLIP_STYLE_LOOT_OBJECTIVE, coords.x, coords.y, coords.z, radius);

	if (!MAP::DOES_BLIP_EXIST(spawned))
	{
		DioTrace(DIOTAG1("ERROR") "FAILED TO SPAWN OBJECTIVE BLIP FOR COORDS %.2f, %.2f, %.2f", coords.x, coords.y, coords.z);
		return NULL;
	}

	MAP::SET_BLIP_SPRITE(spawned, MISC::GET_HASH_KEY("blip_objective"), true);

	MAP::_SET_BLIP_NAME_FROM_PLAYER_STRING(spawned, str.c_str());

	MAP::BLIP_ADD_MODIFIER(spawned, BLIP_MODIFIER_HIDE_HEIGHT_MARKER);

	if (GPS)
	{
		MAP::BLIP_ADD_MODIFIER(spawned, BLIP_MODIFIER_FORCE_GPS);
	}

	SpawnedMissionBlips.emplace_back(spawned);

	return spawned;
}

Blip MissionBase::ObjectiveBlipEntity(Entity ent, bool GPS, const std::string& str)
{
	Blip spawned = MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_LOOT_OBJECTIVE, ent);

	if (!MAP::DOES_BLIP_EXIST(spawned))
	{
		DioTrace(DIOTAG1("ERROR") "FAILED TO SPAWN ENTITY BLIP");
		return NULL;
	}

	MAP::SET_BLIP_SPRITE(spawned, MISC::GET_HASH_KEY("blip_objective"), true);

	MAP::_SET_BLIP_NAME_FROM_PLAYER_STRING(spawned, str.c_str());

	MAP::BLIP_ADD_MODIFIER(spawned, BLIP_MODIFIER_HIDE_HEIGHT_MARKER);

	if (GPS)
	{
		MAP::BLIP_ADD_MODIFIER(spawned, BLIP_MODIFIER_FORCE_GPS);
	}

	SpawnedMissionBlips.emplace_back(spawned);

	return spawned;
}

void MissionBase::DisableLaw()
{
	PLAYER::SET_WANTED_LEVEL_MULTIPLIER(0.0);
}

void MissionBase::EnableLaw()
{
	PLAYER::SET_WANTED_LEVEL_MULTIPLIER(1.0);
}

void MissionBase::SetDispatchService(bool bParam0)
{
	AllowAmbientLaw = bParam0;
	
	MISC::ENABLE_DISPATCH_SERVICE(15, bParam0);
	MISC::ENABLE_DISPATCH_SERVICE(6, bParam0);
	MISC::ENABLE_DISPATCH_SERVICE(1, bParam0);
	MISC::ENABLE_DISPATCH_SERVICE(16, bParam0);
	MISC::ENABLE_DISPATCH_SERVICE(2, bParam0);
	MISC::ENABLE_DISPATCH_SERVICE(3, bParam0);
	MISC::ENABLE_DISPATCH_SERVICE(4, bParam0);
	MISC::ENABLE_DISPATCH_SERVICE(5, bParam0);
	MISC::ENABLE_DISPATCH_SERVICE(7, bParam0);
	MISC::ENABLE_DISPATCH_SERVICE(9, bParam0);
	MISC::ENABLE_DISPATCH_SERVICE(10, bParam0);
	MISC::ENABLE_DISPATCH_SERVICE(11, bParam0);
	MISC::ENABLE_DISPATCH_SERVICE(12, bParam0);
	MISC::ENABLE_DISPATCH_SERVICE(13, bParam0);
	MISC::ENABLE_DISPATCH_SERVICE(14, bParam0);
}

void MissionBase::BecomeWanted(Hash crime, int intensity, bool noWait)
{
	PlayCompanionSpeech("GANG_CALLOUT_LAW_SPOTTED");

	EnableLaw();

	if (noWait)
	{
		LAW::_REPORT_CRIME(0, crime, 0, 0, false);
		PLAYER::REPORT_POLICE_SPOTTED_PLAYER(0);

		LAW::_FORCE_LAW_ON_LOCAL_PLAYER_IMMEDIATELY();

		if (intensity > 0)
		{
			LAW::_SET_WANTED_INTENSITY_FOR_PLAYER(0, intensity);
		}

		return;
	}

	int timeout = 0;

	while (!LAW::IS_LAW_INCIDENT_ACTIVE(0))
	{
		LAW::_REPORT_CRIME(0, crime, 0, 0, false);
		PLAYER::REPORT_POLICE_SPOTTED_PLAYER(0);

		if (intensity > 0)
		{
			LAW::_SET_WANTED_INTENSITY_FOR_PLAYER(0, intensity);
		}

		if (timeout > 100)
		{
			UILOG::_UILOG_SET_CACHED_OBJECTIVE("~COLOR_ORANGE~WARNING:~COLOR_WHITE~ Law is disabled. Please turn off Never Wanted to continue.");
			UILOG::_UILOG_PRINT_CACHED_OBJECTIVE();
		}
		else
		{
			timeout++;
		}
		
		WAIT(0);
	}

	DioTrace(DIOTAG "Law activated after %d frames", timeout);
}

void MissionBase::ClearWanted(bool modifyrelation)
{
	WantedZoneStaysWithPlayer = false;
	
	if (modifyrelation)
	{
		PED::SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, REL_PLAYER, REL_COP);
	}

	LAW::_SET_ALLOW_DISABLED_LAW_RESPONSES(false);
	LAW::_SET_BOUNTY_HUNTER_PURSUIT_CLEARED();
	LAW::_CLEAR_WANTED_INTENSITY_FOR_PLAYER(PLAYER::PLAYER_ID());
	LAW::_0xBCC6DC59E32A2BDC(PLAYER::PLAYER_ID());
	LAW::_0x292AD61A33A7A485();
	LAW::_ENABLE_DISPATCH_LAW(true);
	LAW::_ENABLE_DISPATCH_LAW_2(true);
	LAW::_SET_LAW_DISABLED(false);
	LAW::_0xDE5FAA741A781F73(PLAYER::PLAYER_ID(), 0);
	LAW::_SET_DISPATCH_MULTIPLIER_OVERRIDE(-1.0f);
}

void MissionBase::SetGenericLawModels(int district)
{
	switch (district)
	{
		// Mountains Law
		case DISTRICT_GRIZZLIES_EAST:
		case DISTRICT_GRIZZLIES_WEST:
			GenericLawLeader = "S_M_M_DispatchLeaderRural_01";
			GenericLawFollower = "S_M_M_DispatchLawRural_01";
			break;

		// US Army
		case DISTRICT_CUMBERLAND_FOREST:
			GenericLawLeader = "S_M_M_Army_01";
			GenericLawFollower = "S_M_Y_Army_01";
			break;

		// Plains Law
		case DISTRICT_HEARTLAND:
			GenericLawLeader = "U_M_M_VALSHERIFF_01";
			GenericLawFollower = "S_M_M_AmbientLawRural_01";
			break;

		// Lemoyne Law
		case DISTRICT_SCARLETT_MEADOWS:
			GenericLawLeader = "S_M_M_AmbientLawRural_01";
			GenericLawFollower = "A_M_M_RHDDEPUTYRESIDENT_01";
			break;

		// Saint Denis Police
		case DISTRICT_BAYOU_NWA:
		case DISTRICT_BLUEGILL_MARSH:
			GenericLawLeader = "S_M_M_DispatchLeaderPolice_01";
			GenericLawFollower = "S_M_M_DispatchPolice_01";
			break;

		// Blackwater Police
		case DISTRICT_GREAT_PLAINS:
		case DISTRICT_TALL_TREES:
			GenericLawLeader = "S_M_M_PINLAW_01";
			GenericLawFollower = "S_M_M_AMBIENTBLWPOLICE_01";
			break;

		// Desert Law
		case DISTRICT_GAPTOOTH_RIDGE:
		case DISTRICT_RIO_BRAVO:
		case DISTRICT_CHOLLA_SPRINGS:
		case DISTRICT_HENNIGANS_STEAD:
			GenericLawLeader = "S_M_M_AmbientLawRural_01";
			GenericLawFollower = "S_M_M_TumDeputies_01";
			break;
		
		// Guarma
		case DISTRICT_GUAMA:
			GenericLawLeader = "S_M_M_FussarHenchman_01";
			GenericLawFollower = "S_M_M_FussarHenchman_01";
			break;

		// Generic Ambient Law
		default:
			GenericLawLeader = "S_M_M_DispatchLeaderRural_01";
			GenericLawFollower = "S_M_M_AmbientLawRural_01";
	}
}

void MissionBase::MissionSuccess()
{
	if (TimeMissionSuccess > 0)
	{
		return;
	}

	TimeMissionSuccess = MISC::GET_GAME_TIMER();

	CompanionsClearTasks();
	PlayCompanionSpeech("WON_FIGHT");
}

void MissionBase::MissionFail()
{
	FailScreenActive = true;

	ObjectiveClear();

	ClearWanted(false);

	if (TimeMissionFail > 0 || GRAPHICS::ANIMPOSTFX_IS_RUNNING("MissionFail01"))
	{
		return;
	}

	TimeMissionFail = MISC::GET_GAME_TIMER();

	ENTITY::SET_ENTITY_INVINCIBLE(PlayerPed, true);
	PLAYER::SET_PLAYER_CONTROL(0, false, 0, false);

	MISC::SET_TIME_SCALE(TimeMissionFailSlowdown);
	AUDIO::START_AUDIO_SCENE("MISSION_FAILED_SCENE");
	AUDIO::START_AUDIO_SCENE("DYING_SCENE");
	AUDIO::PLAY_SOUND_FRONTEND("DEATH_SCREEN_ENTER", "DEATH_FAIL_RESPAWN_SOUNDS", true, 0);
	GRAPHICS::ANIMPOSTFX_PLAY("MissionFail01");
}

void MissionBase::CreateScriptedRespondingLawPed(const std::string& model, PedType weapons, Hash HORSE_TYPE, Vector3 spawn, Vector3 destination)
{
	std::vector<std::string> lawhorses = InventoryManager::GetStaticHorseData(HORSE_TYPE).PedModels;

	Ped lawman = SpawnPed(model, spawn, 0, 255);

	SetPedType(lawman, weapons);

	PED::SET_PED_SEEING_RANGE(lawman, 35.0f);

	Blip lawblip = MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_ENEMY, lawman);
	MAP::SET_BLIP_SPRITE(lawblip, MISC::GET_HASH_KEY("blip_ambient_law"), true);
	MAP::BLIP_ADD_MODIFIER(lawblip, BLIP_MODIFIER_ENEMY_ON_GUARD);

	ActivePursuitParams.ScriptedLawPeds.emplace_back(lawman);

	if (HORSE_TYPE == HORSE_INVALID)
	{
		TASK::TASK_FOLLOW_NAV_MESH_TO_COORD(lawman, destination.x, destination.y, destination.z, 5, -1, 10.0f, 4194304, 40000.0f);
	}
	else
	{
		Ped lawhorse = SpawnPed(lawhorses[rand() % lawhorses.size()], Common::ApplyRandomSmallOffsetToCoords(spawn, 2.5f), 0, 255);

		DefaultHorseSetup(lawhorse);
		PED::SET_PED_CONFIG_FLAG(lawhorse, PCF_BlockMountHorsePrompt, true);

		PED::SET_PED_ONTO_MOUNT(lawman, lawhorse, -1, true);

		TASK::TASK_FOLLOW_NAV_MESH_TO_COORD(lawhorse, destination.x, destination.y, destination.z, 5, -1, 10.0f, 4194304, 40000.0f);
	}
}

bool MissionBase::WantedLogic(bool forceNoWantedOverride)
{
	if (!LAW::IS_LAW_INCIDENT_ACTIVE(0) && !forceNoWantedOverride)
	{
		return false;
	}

	if (TimeWantedStarted == 0)
	{
		TimeWantedStarted = MISC::GET_GAME_TIMER();
		DioTrace("Starting pursuit");
	}

	if (ActivePursuitParams.DispatchResetDelay > 0 && MISC::GET_GAME_TIMER() - TimeWantedStarted > ActivePursuitParams.DispatchResetDelay)
	{
		DioTrace("Resetting dispatch");
		SetDispatchService(true);
		ActivePursuitParams.DispatchResetDelay = 0;
	}
	else if (ActivePursuitParams.DispatchResetDelay == -1)
	{
		SetDispatchService(false);
	}

	if (!ActivePursuitParams.ScriptedLawPeds.empty() && PlayerIncognito)
	{
		for (int i = 0; i < ActivePursuitParams.ScriptedLawPeds.size(); i++)
		{
			if (PlayerIncognito && PED::CAN_PED_SEE_ENTITY(ActivePursuitParams.ScriptedLawPeds[i], PlayerPed, true, true) == 1)
			{
				DioTrace("Player spotted, attacking");
				PlayerIncognito = false;
				
				ScriptedSpeechParams params{"ITS_THEM_EXTREME", 0, 0, MISC::GET_HASH_KEY("SPEECH_PARAMS_BEAT_SHOUTED_CLEAR_NOSUB"), 0, true, 0, 0};
				AUDIO::PLAY_PED_AMBIENT_SPEECH_NATIVE(ActivePursuitParams.ScriptedLawPeds[i], (Any*)&params);
				//WAIT(1500);

				for (int j = 0; j < ActivePursuitParams.ScriptedLawPeds.size(); j++)
				{
					Vector3 lawmanCoords = ENTITY::GET_ENTITY_COORDS(ActivePursuitParams.ScriptedLawPeds[j], true, true);
					float lawmanDistance = Common::GetDistBetweenCoords(PlayerCoords, lawmanCoords);

					if (lawmanDistance > 150)
					{
						DioTrace("Lawman is too far from us at %.2f, killing", lawmanDistance);
						PED::EXPLODE_PED_HEAD(ActivePursuitParams.ScriptedLawPeds[j], WEAPON_REVOLVER_CATTLEMAN);
					}
				}
			}
		}
	}
	else if (!ActivePursuitParams.ScriptedLawPeds.empty() && !PlayerIncognito && !PlayerPursuit)
	{
		for (int i = 0; i < ActivePursuitParams.ScriptedLawPeds.size(); i++)
		{
			Ped lawman = ActivePursuitParams.ScriptedLawPeds[i];

			Blip lawblip = MAP::GET_BLIP_FROM_ENTITY(lawman);
			MAP::BLIP_ADD_MODIFIER(lawblip, BLIP_MODIFIER_ENEMY_IS_ALERTED);

			PED::REMOVE_PED_FROM_GROUP(lawman);

			TASK::CLEAR_PED_TASKS(PED::GET_MOUNT(lawman), true, true);
			TASK::CLEAR_PED_TASKS(lawman, true, true);
			TASK::TASK_COMBAT_PED(lawman, PlayerPed, 0, 0);
		}

		if (ActivePursuitParams.CompanionCombatOnSpotted)
		{
			CompanionsBeginCombat();
		}

		CurrentMusicStage = ActivePursuitParams.MusicStageOnSpotted;

		PlayerPursuit = true;
	}
	
	if (!ActivePursuitParams.ScriptedLawPeds.empty())
	{
		bool alldead = true;

		for (int i = 0; i < ActivePursuitParams.ScriptedLawPeds.size(); i++)
		{
			if (!PED::IS_PED_DEAD_OR_DYING(ActivePursuitParams.ScriptedLawPeds[i], true))
			{
				alldead = false;
			}
		}

		if (alldead)
		{
			DioTrace("All scripted lawmen killed");
			ActivePursuitParams.AllScriptedPedsKilled = true;
			ActivePursuitParams.ScriptedLawPeds.clear();
		}
	}

	return true;
}

MissionResults MissionBase::GetMissionResults()
{
	return {
		ThisMissionID,
		ExitMissionFlag,
		MissionFailStr,
		FailScreenActive,
		_currentMissionCasualties,
		RewardBounty_Active,
		RewardCash_Active,
		RewardMembers_Active
	};
}

bool MissionBase::TriggeredGenericFailureCondition()
{
	PlayerPed = PLAYER::PLAYER_PED_ID();
	PlayerCoords = ENTITY::GET_ENTITY_COORDS(PlayerPed, true, true);

	PlayerHorse = PLAYER::_GET_SADDLE_HORSE_FOR_PLAYER(0);
	PlayerHorseCoords = ENTITY::GET_ENTITY_COORDS(PlayerHorse, true, true);

	if (PLAYER::IS_PLAYER_BEING_ARRESTED(0, true))
	{
		ExitMissionFlag = M_EXIT_FAILURE_GEN_PLAYER_ARRESTED;
		return true;
	}

	if (PLAYER::IS_PLAYER_DEAD(0))
	{
		ExitMissionFlag = M_EXIT_FAILURE_GEN_PLAYER_DIED;
		return true;
	}

	if (MISC::GET_MISSION_FLAG())
	{
		ExitMissionFlag = M_EXIT_FAILURE_GEN_MISSION_STARTED;
		return true;
	}

	if (LAW::IS_LAW_INCIDENT_ACTIVE(0) && !AllowWanted)
	{
		ExitMissionFlag = M_EXIT_FAILURE_LAW_ALERTED;
		WAIT(1000);
		MissionFail();
		ClearWanted(true);
		return true;
	}

	if (CheckAbandon && AbandonCoords.x != 0 && AbandonRadius > 0)
	{
		bool outside = MISC::GET_DISTANCE_BETWEEN_COORDS(PlayerCoords.x, PlayerCoords.y, PlayerCoords.z, AbandonCoords.x, AbandonCoords.y, AbandonCoords.z, false) > AbandonRadius;
		
		if (outside && AbandonTime == 0)
		{
			AbandonTime = MISC::GET_GAME_TIMER();
			ObjectiveTempOverride("~COLOR_YELLOW~Return to your objective!");
		}
		else if (outside && MISC::GET_GAME_TIMER() - AbandonTime > MaxAbandonTime)
		{
			ExitMissionFlag = M_EXIT_FAILURE_ABANDONED_OBJECTIVE;
			MissionFail();
			return true;
		}
		else if (!outside)
		{
			AbandonTime = 0;
		}
		else
		{
			DioTrace("%d", MISC::GET_GAME_TIMER() - AbandonTime);
		}
	}

	int camp = -1;

	if (SCRIPTS::_GET_NUMBER_OF_REFERENCES_OF_SCRIPT_WITH_NAME_HASH(MISC::GET_HASH_KEY("CAMP_HORSESHOEOVERLOOK")) > 0)
	{
		camp = 0;
	}
	else if (SCRIPTS::_GET_NUMBER_OF_REFERENCES_OF_SCRIPT_WITH_NAME_HASH(MISC::GET_HASH_KEY("CAMP_CLEMENSPOINT")) > 0)
	{
		camp = 1;
	}
	else if (SCRIPTS::_GET_NUMBER_OF_REFERENCES_OF_SCRIPT_WITH_NAME_HASH(MISC::GET_HASH_KEY("CAMP_SHADYBELLE")) > 0)
	{
		camp = 2;
	}
	else if (SCRIPTS::_GET_NUMBER_OF_REFERENCES_OF_SCRIPT_WITH_NAME_HASH(MISC::GET_HASH_KEY("CAMP_LAKAY")) > 0)
	{
		camp = 3;
	}
	else if (SCRIPTS::_GET_NUMBER_OF_REFERENCES_OF_SCRIPT_WITH_NAME_HASH(MISC::GET_HASH_KEY("CAMP_BEAVERHOLLOW")) > 0)
	{
		camp = 4;
	}
	else if (SCRIPTS::_GET_NUMBER_OF_REFERENCES_OF_SCRIPT_WITH_NAME_HASH(MISC::GET_HASH_KEY("CAMP_BEECHERSHOPE")) > 0)
	{
		camp = 5;
	}
	else
	{
		_lastStoryCampReminder = 0;
		_lastStoryCampReminderTriggerDistance = 0;
	}

	if (camp >= 0 && Common::GetDistBetweenCoords(PlayerCoords, _storyCamps[camp]) > 500)
	{
		camp = -1;
		_lastStoryCampReminder = 0;
		_lastStoryCampReminderTriggerDistance = 0;
	}

	if (camp >= 0 && MISC::GET_GAME_TIMER() - _lastStoryCampReminder > 30000)
	{
		ObjectiveTempOverride("~COLOR_YELLOW~WARNING:~COLOR_WHITE~ Approaching a ~COLOR_YELLOW~story camp~COLOR_WHITE~ will fail the heist!");
		_lastStoryCampReminder = MISC::GET_GAME_TIMER();
		_lastStoryCampReminderTriggerDistance = Common::GetDistBetweenCoords(PlayerCoords, _storyCamps[camp]);
	}

	if (camp >= 0)
	{
		if (Common::GetDistBetweenCoords(PlayerCoords, _storyCamps[camp]) < _lastStoryCampReminderTriggerDistance * 0.95)
		{
			ExitMissionFlag = M_EXIT_FAILURE_GEN_ENTERED_CAMP;
			return true;
		}
	}

	return false;
}

void MissionBase::common_update()
{
	_usingController = !PAD::_IS_USING_KEYBOARD(0);
	
	TrackPlayer();
	TrackRewards();

	if (!SpawnedGangMembers.empty())
	{
		TrackSpawnedGangMembers();
		TrackCompanionControl();
	}

	UpdateMusic();

	if (WantedZoneStaysWithPlayer)
	{
		PLAYER::REPORT_POLICE_SPOTTED_PLAYER(0);
	}

	if (ThisMissionID == MissionType::FREEROAM)
	{
		return;
	}

	SCRIPTS::SCRIPT_THREAD_ITERATOR_RESET();
	int ThreadID = SCRIPTS::SCRIPT_THREAD_ITERATOR_GET_NEXT_THREAD_ID();

	while (ThreadID > 0)
	{
		Hash ThreadHash = SCRIPTS::_GET_HASH_OF_THREAD(ThreadID);

		for (Hash kill : ScriptsKillList)
		{
			if (ThreadHash == kill)
			{
				SCRIPTS::TERMINATE_THREAD(ThreadID);
			}
		}

		for (int i = 0; i < AlwaysKillScriptsLength; ++i)
		{
			if (ThreadHash == AlwaysKillScripts[i])
			{
				SCRIPTS::TERMINATE_THREAD(ThreadID);
				break;
			}
		}

		ThreadID = SCRIPTS::SCRIPT_THREAD_ITERATOR_GET_NEXT_THREAD_ID();
	}

	Ped peds[1024];
	int count = worldGetAllPeds(peds, 1024);

	for (int i = 0; i < count; i++)
	{
		Ped p = peds[i];
		Hash rel = PED::GET_PED_RELATIONSHIP_GROUP_HASH(p);

		if ((rel == REL_WILD_ANIMAL_PREDATOR || rel == REL_ALLIGATOR || (rel == REL_COP && !AllowAmbientLaw)) && !ENTITY::IS_ENTITY_A_MISSION_ENTITY(p))
		{
			ENTITY::SET_ENTITY_AS_MISSION_ENTITY(p, true, true);
			ENTITY::DELETE_ENTITY(&p);
		}
	}
}
