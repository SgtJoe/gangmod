
#include "precomp.h"
#include "JailbreakWagon.h"

const MissionStaticData& JailbreakWagon::GetStaticMissionData()
{
	static MissionStaticData missiondata = {
		MissionType::JAILBREAK_WAGON,
		0, 0, 1000, 0, 2, 
		"Wagon Jailbreak", 
		"Two outlaws are being transported by wagon.\nIf we free them, they can be persuaded to join us.", 
		COMPCOUNT_MIN, 
		false, 
		MUSIC_DEFAULT,
		false
	};
	return missiondata;
}

JailbreakWagon::JailbreakWagon()
{
	DioScope(DIOTAG);
}

JailbreakWagon::~JailbreakWagon()
{
	DioScope(DIOTAG);
}

void JailbreakWagon::Setup(int campid, const std::string& gangstr, const std::vector<MissionCompanionInfo>& companions)
{
	DioScope(DIOTAG "campid %d gangstr %s", campid, gangstr.c_str());

	GangName = gangstr;
	GangMemberTemplates = companions;

	if (!SetMissionParamsForCamp(campid))
	{
		ExitMissionFlag = M_EXIT_ERROR_TERMINATE;
		return;
	}

	CommonInit();
}

bool JailbreakWagon::SetMissionParamsForCamp(int camp)
{
	bool success = false;

	JailbreakWagonData collection1, collection2, collection3;

	switch (camp)
	{
		case DISTRICT_GRIZZLIES_WEST:
			PrisonerModel = "G_M_M_UNIMICAHGOONS_01";

			collection1.AmbushPoint = {-1441.91f, 1614.33f, 245.10f};
			collection1.WagonSpawn = {{-1508.33f, 1669.28f, 239.05f}, 240};
			collection1.WagonDestination = {-1430.70f, 1571.70f, 236.58f};
			collection1.LawSpawns = {{-1630.42f, 1679.12f, 237.87f}/*, {-1414.82f, 1833.85f, 298.46f}, {-1259.12f, 1537.27f, 295.53f}*/};
			
			collection2.AmbushPoint = {-1915.91f, 1230.43f, 185.20f};
			collection2.WagonSpawn = {{-1900.43f, 1307.94f, 196.96f}, 160};
			collection2.WagonDestination = {-1930.58f, 1192.17f, 179.48f};
			collection2.LawSpawns = {{-1988.33f, 1117.19f, 170.43f}};

			collection3.AmbushPoint = {-1140.58f, 1225.33f, 217.59f};
			collection3.WagonSpawn = {{-1175.98f, 1298.61f, 231.57f}, 193};
			collection3.WagonDestination = {-1113.53f, 1198.96f, 206.16f};
			collection3.LawSpawns = {{-1099.33f, 1066.00f, 160.33f}};

			AllAvailableCollections = {collection1, collection2, collection3};
			success = true;
			break;

		case DISTRICT_CUMBERLAND_FOREST:
			PrisonerModel = "A_M_M_WapWarriors_01";

			collection1.AmbushPoint = {658.39f, 1534.00f, 179.16f};
			collection1.WagonSpawn = {{662.23f, 1472.57f, 181.64f}, 6};
			collection1.WagonDestination = {635.74f, 1576.12f, 185.07f};
			collection1.LawSpawns = {{668.70f, 1410.62f, 180.36f}};

			collection2.AmbushPoint = {56.87f, 1099.99f, 176.52f};
			collection2.WagonSpawn = {{110.36f, 1093.25f, 178.90f}, 88};
			collection2.WagonDestination = {9.52f, 1096.87f, 173.10f};
			collection2.LawSpawns = {{167.51f, 1119.00f, 174.35f}};

			AllAvailableCollections = {collection1, collection2};
			success = true;
			break;

		case DISTRICT_BLUEGILL_MARSH:
			PrisonerModel = "G_M_M_UNICRIMINALS_01";

			collection1.AmbushPoint = {2175.65f, -929.62f, 40.09f};
			collection1.WagonSpawn = {{2212.25f, -996.98f, 44.04f}, 49};
			collection1.WagonDestination = {2124.87f, -932.00f, 41.26f};
			collection1.LawSpawns = {{2284.14f, -1054.87f, 42.72f}};

			collection2.AmbushPoint = {2623.16f, -754.83f, 41.24f};
			collection2.WagonSpawn = {{2575.19f, -806.44f, 41.23f}, 349};
			collection2.WagonDestination = {2592.89f, -700.90f, 41.87f};
			collection2.LawSpawns = {{2622.28f, -925.66f, 41.41f}};

			AllAvailableCollections = {collection1, collection2};
			success = true;
			break;

		case DISTRICT_RIO_BRAVO:
			PrisonerModel = "G_M_M_UNICRIMINALS_01";

			collection1.AmbushPoint = {-5331.71f, -3062.14f, -3.91f};
			collection1.WagonSpawn = {{-5301.53f, -3024.87f, -6.48f}, 164};
			collection1.WagonDestination = {-5305.44f, -3108.41f, -15.89f};
			collection1.LawSpawns = {{-5449.41f, -3101.43f, -4.12f}};

			collection2.AmbushPoint = {-3905.08f, -2678.12f, -7.06f};
			collection2.WagonSpawn = {{-3943.63f, -2721.09f, -15.44f}, 267};
			collection2.WagonDestination = {-3840.90f, -2717.20f, -16.08f};
			collection2.LawSpawns = {{-3735.63f, -2660.61f, -15.40f}};

			AllAvailableCollections = {collection1, collection2};
			success = true;
			break;
	}

	if (success)
	{
		ChosenCollection = AllAvailableCollections[rand() % AllAvailableCollections.size()];
		ActivePursuitParams.ScriptedLawSpawnPoints = ChosenCollection.LawSpawns;
	}
	else
	{
		DioTrace(DIOTAG1("ERROR") "CANNOT SET MISSION %d PARAMS FOR CAMP %d - ABORTING MISSION", JailbreakWagon::GetStaticMissionData().StaticMissionID, camp);
	}

	return success;
}

void JailbreakWagon::CommonInit()
{
	const MissionStaticData& thisMissionData = JailbreakWagon::GetStaticMissionData();
	SetVarsFromStaticData(thisMissionData);

	SetGenericLawModels(Common::GetDistrictFromCoords(ChosenCollection.WagonSpawn.p));

	MissionAssetsToLoad = {
		{"VEH", WagonModel},
		{"SET", WagonLights},
		{"PED", PrisonerModel}
	};

	AddHorseToMissionAssets(HORSE_MORGAN);

	ObjectivesList = {
		"Go to the ~COLOR_YELLOW~location",
		"Eliminate the ~COLOR_RED~wagon guards",
		"Shoot the lock to ~COLOR_YELLOW~free the prisoners",
		"Lose the ~COLOR_RED~law"
	};

	LoadAllMissionAssets();
	SpawnGangMembers(thisMissionData.NumMembersToSpawn);

	WAIT(2000);
}

void JailbreakWagon::ObjectiveInit_0()
{
	ObjectiveSet(0);
	ActivateObjective_0 = false;
	RunObjective_0 = true;
	
	AmbushBlip = ObjectiveBlipCoords(ChosenCollection.AmbushPoint, 5, true, ObjectivesList[0]);

#ifdef DEBUG_FAST_MISSION
	ENTITY::_SET_ENTITY_COORDS_AND_HEADING(PlayerPed, ChosenCollection.AmbushPoint.x, ChosenCollection.AmbushPoint.y + 10, ChosenCollection.AmbushPoint.z + 5, 0, true, true, true);
	ENTITY::PLACE_ENTITY_ON_GROUND_PROPERLY(PlayerPed, true);
	CAM::SET_GAMEPLAY_CAM_RELATIVE_HEADING(0, 1);
	CAM::SET_GAMEPLAY_CAM_RELATIVE_PITCH(0, 1);
#endif
}

void JailbreakWagon::ObjectiveLogic_0()
{
	if (Common::GetDistBetweenCoords(PlayerCoords, ChosenCollection.AmbushPoint) < 5.0f)
	{
		MAP::REMOVE_BLIP(&AmbushBlip);

		RunObjective_0 = false;
		ActivateObjective_1 = true;
	}
}

void JailbreakWagon::ObjectiveInit_1()
{
	ObjectiveSet(1);
	ActivateObjective_1 = false;
	RunObjective_1 = true;

	DisableLaw();

	AllowCompanionManualControl = true;

	PlayCompanionSpeech("ARRIVAL_ENTER_TRAP");

	Common::ClearArea(ChosenCollection.WagonSpawn.p, 15);

	Wagon = SpawnVehicle(WagonModel, ChosenCollection.WagonSpawn.p, ChosenCollection.WagonSpawn.h);

	VEHICLE::SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(Wagon, false);
	VEHICLE::SET_BREAKABLE_VEHICLE_LOCKS_UNBREAKABLE(Wagon, true);
	Common::SetEntityProofs(Wagon, true, true);

	if (Common::IsDarkOut())
	{
		PROPSET::_ADD_LIGHT_PROP_SET_TO_VEHICLE(Wagon, MISC::GET_HASH_KEY(WagonLights.c_str()));
	}

	std::vector<std::string> guardHorses = InventoryManager::GetStaticHorseData(HORSE_MORGAN).PedModels;

	WagonDriver = SpawnPed(GenericLawLeader, Common::ApplyRandomSmallOffsetToCoords(ChosenCollection.WagonSpawn.p, 2.0f), 0, 255);
	Ped WagonPassgr = SpawnPed(GenericLawFollower, Common::ApplyRandomSmallOffsetToCoords(ChosenCollection.WagonSpawn.p, 2.0f), 0, 255);
	Ped WagonGuard1 = SpawnPed(GenericLawFollower, Common::ApplyRandomSmallOffsetToCoords(ChosenCollection.WagonSpawn.p, 2.0f), 0, 255);
	Ped WagonGuard2 = SpawnPed(GenericLawFollower, Common::ApplyRandomSmallOffsetToCoords(ChosenCollection.WagonSpawn.p, 2.0f), 0, 255);
	Ped GuardHorse1 = SpawnPed(guardHorses[rand() % guardHorses.size()], ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Wagon, 4, 0, 2), ChosenCollection.WagonSpawn.h, 255);
	Ped GuardHorse2 = SpawnPed(guardHorses[rand() % guardHorses.size()], ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Wagon, -4, 0, 2), ChosenCollection.WagonSpawn.h, 255);
	Prisoner1 = SpawnPed(PrisonerModel, Common::ApplyRandomSmallOffsetToCoords(ChosenCollection.WagonSpawn.p, 2.0f), 0, -1);
	Prisoner2 = SpawnPed(PrisonerModel, Common::ApplyRandomSmallOffsetToCoords(ChosenCollection.WagonSpawn.p, 2.0f), 0, -1);

	PED::SET_PED_AS_COP(WagonDriver, true);
	PED::SET_PED_AS_COP(WagonPassgr, true);
	PED::SET_PED_AS_COP(WagonGuard1, true);
	PED::SET_PED_AS_COP(WagonGuard2, true);
	
	MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_ENEMY, WagonDriver);
	MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_ENEMY, WagonPassgr);
	MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_ENEMY, WagonGuard1);
	MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_ENEMY, WagonGuard2);

	DefaultHorseSetup(GuardHorse1);
	DefaultHorseSetup(GuardHorse2);
	PED::SET_PED_CONFIG_FLAG(GuardHorse1, PCF_BlockMountHorsePrompt, true);
	PED::SET_PED_CONFIG_FLAG(GuardHorse2, PCF_BlockMountHorsePrompt, true);

	PED::SET_PED_RELATIONSHIP_GROUP_HASH(WagonDriver, REL_COP);
	PED::SET_PED_RELATIONSHIP_GROUP_HASH(WagonPassgr, REL_COP);
	PED::SET_PED_RELATIONSHIP_GROUP_HASH(WagonGuard1, REL_COP);
	PED::SET_PED_RELATIONSHIP_GROUP_HASH(WagonGuard2, REL_COP);
	PED::SET_PED_RELATIONSHIP_GROUP_HASH(Prisoner1, REL_IGNORE_EVERYTHING);
	PED::SET_PED_RELATIONSHIP_GROUP_HASH(Prisoner2, REL_IGNORE_EVERYTHING);

	SetPedType(WagonDriver, PedType::SIDEARM_REGULAR);
	SetPedType(WagonPassgr, PedType::SIDEARM_REGULAR);
	SetPedType(WagonGuard1, PedType::REPEATER_WEAK);
	SetPedType(WagonGuard2, PedType::REPEATER_WEAK);

	PED::SET_PED_INTO_VEHICLE(WagonDriver, Wagon, SEAT_DRIVER);
	PED::SET_PED_INTO_VEHICLE(WagonPassgr, Wagon, SEAT_FRONT_RIGHT);
	PED::SET_PED_INTO_VEHICLE(Prisoner1, Wagon, SEAT_ANY_PASSENGER);
	PED::SET_PED_INTO_VEHICLE(Prisoner2, Wagon, SEAT_ANY_PASSENGER);
	PED::SET_PED_ONTO_MOUNT(WagonGuard1, GuardHorse1, -1, true);
	PED::SET_PED_ONTO_MOUNT(WagonGuard2, GuardHorse2, -1, true);

	WagonGuards.emplace_back(WagonPassgr);
	WagonGuards.emplace_back(WagonGuard1);
	WagonGuards.emplace_back(WagonGuard2);

	TASK::TASK_VEHICLE_GOTO_NAVMESH(WagonDriver, Wagon, ChosenCollection.WagonDestination.x, ChosenCollection.WagonDestination.y, ChosenCollection.WagonDestination.z, 3.0f, 0, 0);
	TASK::TASK_FOLLOW_TO_OFFSET_OF_ENTITY(GuardHorse1, Wagon, {-5, 0, 0}, 1.0f, -1, 3.0f, true, false, false, false, true, false);
	TASK::TASK_FOLLOW_TO_OFFSET_OF_ENTITY(GuardHorse2, Wagon, {5, 0, 0}, 1.0f, -1, 3.0f, true, false, false, false, true, false);

	PED::SET_PED_KEEP_TASK(WagonDriver, true);
}

void JailbreakWagon::ObjectiveLogic_1()
{
	if (!ENTITY::DOES_ENTITY_EXIST(Prisoner1Horse) || !ENTITY::DOES_ENTITY_EXIST(Prisoner2Horse))
	{
		for (int i = 0; i < 6; ++i)
		{
			Ped horse = VEHICLE::_GET_PED_IN_DRAFT_HARNESS(Wagon, i);

			if (ENTITY::DOES_ENTITY_EXIST(horse) && !ENTITY::DOES_ENTITY_EXIST(Prisoner1Horse))
			{
				Prisoner1Horse = horse;
			}
			else if (ENTITY::DOES_ENTITY_EXIST(horse) && !ENTITY::DOES_ENTITY_EXIST(Prisoner2Horse))
			{
				Prisoner2Horse = horse;
			}
		}
	}
	
	TASK::CLEAR_PED_TASKS(Prisoner1, true, true);
	TASK::CLEAR_PED_TASKS(Prisoner2, true, true);
	
	if (!StartedWagonAttack)
	{
		bool wagonThreatened = false;

		if (PED::IS_PED_IN_COMBAT(WagonDriver, PlayerPed) ||
			(PLAYER::IS_PLAYER_FREE_AIMING(0) && PED::CAN_PED_SEE_ENTITY(WagonDriver, PlayerPed, true, true) == 1) ||
			PED::IS_PED_DEAD_OR_DYING(WagonDriver, true))
		{
			wagonThreatened = true;
		}

		for (int i = 0; i < WagonGuards.size(); i++)
		{
			if (PED::IS_PED_IN_COMBAT(WagonGuards[i], PlayerPed) ||
				(PLAYER::IS_PLAYER_FREE_AIMING(0) && PED::CAN_PED_SEE_ENTITY(WagonGuards[i], PlayerPed, true, true) == 1) ||
				PED::IS_PED_DEAD_OR_DYING(WagonGuards[i], true))
			{
				wagonThreatened = true;
			}
		}

		if (wagonThreatened || CompanionManualControlStatus == CompanionControlStatus::ATTACKING)
		{
			DioTrace("Starting wagon attack");
			StartedWagonAttack = true;

			AllowCompanionManualControl = false;

			CurrentMusicStage = STAGE_ACTION1;

			for (int j = 0; j < WagonGuards.size(); j++)
			{
				PED::SET_PED_RELATIONSHIP_GROUP_HASH(WagonGuards[j], REL_PLAYER_ENEMY);
				TASK::TASK_COMBAT_PED(WagonGuards[j], PlayerPed, 0, 0);
			}

			if (CompanionManualControlStatus == CompanionControlStatus::FOLLOWING)
			{
				CompanionsBeginCombat();
			}
		}
	}

	if (PED::IS_PED_DEAD_OR_DYING(WagonDriver, true) && VEHICLE::IS_VEHICLE_DRIVEABLE(Wagon, true, true))
	{
		DioTrace("Wagon driver killed");
		
		DetachAllHorsesFromWagon(Wagon);
	}
	/*else if (ENTITY::DOES_ENTITY_EXIST(Prisoner1Horse) && ENTITY::DOES_ENTITY_EXIST(Prisoner2Horse) && VEHICLE::IS_VEHICLE_DRIVEABLE(Wagon, true, true) &&
			(PED::IS_PED_DEAD_OR_DYING(Prisoner1Horse, true) || PED::IS_PED_DEAD_OR_DYING(Prisoner2Horse, true))
		)*/
	else if (!PED::IS_PED_DEAD_OR_DYING(WagonDriver, true) && !VEHICLE::IS_VEHICLE_DRIVEABLE(Wagon, true, true) && PED::GET_PED_RELATIONSHIP_GROUP_HASH(WagonDriver) != REL_PLAYER_ENEMY)
	{
		DioTrace("Wagon driver still alive but horses killed or loose");

		PED::SET_PED_RELATIONSHIP_GROUP_HASH(WagonDriver, REL_PLAYER_ENEMY);

		DetachAllHorsesFromWagon(Wagon);

		//TASK::CLEAR_PED_TASKS(WagonDriver, true, true);
		//WEAPON::SET_CURRENT_PED_WEAPON(WagonDriver, WEAPON::GET_BEST_PED_WEAPON(WagonDriver, true, true), true, WEAPON_ATTACH_POINT_HAND_PRIMARY, true, true);
		//TASK::TASK_COMBAT_PED(WagonDriver, PlayerPed, 0, 0);
		PED::EXPLODE_PED_HEAD(WagonDriver, WEAPON_REVOLVER_CATTLEMAN);
	}
	else if (CompanionsInCombat)
	{
		TASK::TASK_VEHICLE_GOTO_NAVMESH(WagonDriver, Wagon, ChosenCollection.WagonDestination.x, ChosenCollection.WagonDestination.y, ChosenCollection.WagonDestination.z, 10.0f, 0, 0);
	}
	
	if (ENTITY::DOES_ENTITY_EXIST(Wagon) && !PED::IS_PED_DEAD_OR_DYING(WagonDriver, true) && Common::GetDistBetweenCoords(WagonCoords, ChosenCollection.WagonDestination) < 5.0f)
	{
		MissionFailStr = "The wagon escaped.";
		MissionFail();
	}

	bool alldead = true;

	if (!PED::IS_PED_DEAD_OR_DYING(WagonDriver, true))
	{
		alldead = false;
	}

	for (int i = 0; i < WagonGuards.size(); i++)
	{
		if (!PED::IS_PED_DEAD_OR_DYING(WagonGuards[i], true))
		{
			alldead = false;
		}
	}

	if (alldead)
	{
		RunObjective_1 = false;
		ActivateObjective_2 = true;
	}
}

void JailbreakWagon::ObjectiveInit_2()
{
	ObjectiveSet(2);
	ActivateObjective_2 = false;
	RunObjective_2 = true;

	CompanionsClearTasks();
	CompanionsRegroup();

	VEHICLE::SET_BREAKABLE_VEHICLE_LOCKS_UNBREAKABLE(Wagon, false);

	WagonBlip = MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_LOOT_OBJECTIVE, Wagon);
	MAP::SET_BLIP_SPRITE(WagonBlip, MISC::GET_HASH_KEY("blip_ambient_wagon"), true);
	MAP::_SET_BLIP_NAME_FROM_PLAYER_STRING(WagonBlip, ObjectivesList[2].c_str());

	CheckAbandon = true;
	AbandonCoords = WagonCoords;
	AbandonRadius = 40.0f;

	AllowWanted = true;
	BecomeWanted(CRIME_JAIL_BREAK, 2);
	SetDispatchService(false);
	ActivePursuitParams.DispatchResetDelay = -1;
	ActivePursuitParams.CompanionCombatOnSpotted = true;
	ActivePursuitParams.MusicStageOnSpotted = STAGE_ACTION2;
	WantedZoneStaysWithPlayer = true;

	PrisonerFreeTime = MISC::GET_GAME_TIMER();
}

void JailbreakWagon::ObjectiveLogic_2()
{
	TASK::CLEAR_PED_TASKS(Prisoner1, true, true);
	TASK::CLEAR_PED_TASKS(Prisoner2, true, true);

	if (PrisonerFreeTime > 0 && MISC::GET_GAME_TIMER() - PrisonerFreeTime > 5000)
	{
		ScriptedSpeechParams params{"CALL_FOR_SUPPORT", 0, 0, MISC::GET_HASH_KEY("SPEECH_PARAMS_BEAT_SHOUTED_CLEAR_NOSUB"), 0, true, 0, 0};
		AUDIO::PLAY_PED_AMBIENT_SPEECH_NATIVE(Prisoner1, (Any*)&params);
		PrisonerFreeTime = 0;
	}
	
	if (VEHICLE::_GET_BREAKABLE_VEHICLE_LOCKS_STATE(Wagon) != 2 ||
		!PED::IS_PED_IN_ANY_VEHICLE(Prisoner1, false) ||
		!PED::IS_PED_IN_ANY_VEHICLE(Prisoner2, false))
	{
		MAP::REMOVE_BLIP(&WagonBlip);
		RunObjective_2 = false;
		ActivateObjective_3 = true;
	}
}

void JailbreakWagon::ObjectiveInit_3()
{
	WantedZoneStaysWithPlayer = false;

	DioTrace("Prisoners bailing");

	PED::SET_PED_RELATIONSHIP_GROUP_HASH(Prisoner1, REL_IGNORE_EVERYTHING);
	PED::SET_PED_RELATIONSHIP_GROUP_HASH(Prisoner2, REL_IGNORE_EVERYTHING);

	TASK::TASK_FLEE_PED(Prisoner1, PlayerPed, 4, 0, -1082130432, -1, 0);
	TASK::TASK_FLEE_PED(Prisoner2, PlayerPed, 4, 0, -1082130432, -1, 0);

	/*PED::SET_PED_RELATIONSHIP_GROUP_HASH(Prisoner1, REL_GANG_DUTCHS);
	PED::SET_PED_RELATIONSHIP_GROUP_HASH(Prisoner2, REL_GANG_DUTCHS);

	PED::SET_PED_LASSO_HOGTIE_FLAG(Prisoner1, 0, false);
	PED::SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(Prisoner1, false);
	MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_COMPANION, Prisoner1);

	PED::SET_PED_LASSO_HOGTIE_FLAG(Prisoner2, 0, false);
	PED::SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(Prisoner2, false);
	MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_COMPANION, Prisoner2);*/

	WAIT(2000);

	ScriptedSpeechParams params{"GENERIC_THANKS", 0, 0, MISC::GET_HASH_KEY("SPEECH_PARAMS_BEAT_SHOUTED_CLEAR_NOSUB"), 0, true, 0, 0};
	AUDIO::PLAY_PED_AMBIENT_SPEECH_NATIVE(Prisoner2, (Any*)&params);

	WAIT(4000);

	/*SetPedType(Prisoner1, PedType::SIDEARM_EXPERT);
	SetPedType(Prisoner2, PedType::SIDEARM_EXPERT);

	WEAPON::REMOVE_ALL_PED_WEAPONS(Prisoner1, true, true);
	WEAPON::REMOVE_ALL_PED_WEAPONS(Prisoner2, true, true);

	WEAPON::GIVE_WEAPON_TO_PED(Prisoner1, WEAPON_REVOLVER_CATTLEMAN, 6, true, false, WEAPON_ATTACH_POINT_HAND_PRIMARY, false, 0.5f, 1.0f, ADD_REASON_DEFAULT, true, 0.95, false);
	WEAPON::GIVE_WEAPON_TO_PED(Prisoner2, WEAPON_REVOLVER_CATTLEMAN, 6, true, false, WEAPON_ATTACH_POINT_HAND_PRIMARY, false, 0.5f, 1.0f, ADD_REASON_DEFAULT, true, 0.95, false);*/

	params.speechName = "COMBAT_FLEE_CALL_OUT";
	AUDIO::PLAY_PED_AMBIENT_SPEECH_NATIVE(Prisoner1, (Any*)&params);

	WAIT(2000);
	
	/*TASK::CLEAR_PED_TASKS_IMMEDIATELY(Prisoner1, false, false);
	TASK::CLEAR_PED_TASKS_IMMEDIATELY(Prisoner2, false, false);
	
	SpawnedGangMember p1 = { Prisoner1, COMP_OK, ARMOR_LEVEL_0, WEAPON_REVOLVER_CATTLEMAN, 0, Prisoner1Horse, 0 };
	SpawnedGangMembers.emplace_back(p1);

	SpawnedGangMember p2 = { Prisoner2, COMP_OK, ARMOR_LEVEL_0, WEAPON_REVOLVER_CATTLEMAN, 0, Prisoner2Horse, 0 };
	SpawnedGangMembers.emplace_back(p2);

	CompanionsDisband();
	CompanionsClearTasks();
	CompanionsRegroup();*/

	//TASK::TASK_FOLLOW_TO_OFFSET_OF_ENTITY(Prisoner1, PlayerPed, 0, -5, 0, 0, 0, 2, false, false, false, false, false);
	//TASK::TASK_FOLLOW_TO_OFFSET_OF_ENTITY(Prisoner2, PlayerPed, 0, -5, 0, 0, 0, 2, false, false, false, false, false);

	for (int i = 0; i < ActivePursuitParams.ScriptedLawSpawnPoints.size(); ++i)
	{
		CreateScriptedRespondingLawPed(GenericLawLeader, PedType::SIDEARM_REGULAR, HORSE_MORGAN, ActivePursuitParams.ScriptedLawSpawnPoints[i], WagonCoords);

		for (int j = 0; j < 2; ++j)
		{
			CreateScriptedRespondingLawPed(GenericLawFollower, PedType::REPEATER_REGULAR, HORSE_MORGAN, ActivePursuitParams.ScriptedLawSpawnPoints[i], WagonCoords);
		}
	}

	ObjectiveSet(3);
	ActivateObjective_3 = false;
	RunObjective_3 = true;

	CheckAbandon = false;

	AllowCompanionManualControl = true;
}

void JailbreakWagon::ObjectiveLogic_3()
{
	if (PlayerIncognito && Common::GetDistBetweenCoords(WagonCoords, PlayerCoords) > 150)
	{
		ClearWanted(false);
	}

	if (ActivePursuitParams.AllScriptedPedsKilled)
	{
		ClearWanted(false);
	}

	if (!WantedLogic())
	{
		CurrentMusicStage = STAGE_AFTERACTION;
		UpdateMusic(); 
		
		RunObjective_3 = false;
		MissionSuccess();
	}
}

void JailbreakWagon::TrackRewards()
{
	RewardBounty_Active = PlayerIncognito ? int(RewardBounty_Potential * 0.25f) : RewardBounty_Potential;

	RewardCash_Active = RewardCash_Potential;
	RewardMembers_Active = RewardMembers_Potential;
}

void JailbreakWagon::Update()
{
	if (ENTITY::DOES_ENTITY_EXIST(Wagon))
	{
		WagonCoords = ENTITY::GET_ENTITY_COORDS(Wagon, true, true);
	}

	if (ENTITY::DOES_ENTITY_EXIST(Prisoner1) && PED::IS_PED_DEAD_OR_DYING(Prisoner1, true) && !Prisoner1Dead)
	{
		Prisoner1Dead = true;
	}

	if (ENTITY::DOES_ENTITY_EXIST(Prisoner2) && PED::IS_PED_DEAD_OR_DYING(Prisoner2, true) && !Prisoner2Dead)
	{
		Prisoner2Dead = true;
	}

	if ((Prisoner1Dead && Prisoner2Dead) && TimeMissionFail < 0)
	{
		MissionFailStr = "Both prisoners were killed.";
		MissionFail();
	}

	if (TimeMissionFail > 0 && MISC::GET_GAME_TIMER() - TimeMissionFail > TimeMissionFailDelay)
	{
		DioTrace("Exit Fail Triggered");

		ExitMissionFlag = M_EXIT_FAILURE;
	}
	else if (TimeMissionSuccess > 0 && MISC::GET_GAME_TIMER() - TimeMissionSuccess > TimeMissionSuccessDelay)
	{
		DioTrace("Exit Win Triggered");

		ExitMissionFlag = M_EXIT_SUCCESS;
	}
	else if (ActivateObjective_0)
	{
		ObjectiveInit_0();
	}
	else if (ActivateObjective_1)
	{
		ObjectiveInit_1();
	}
	else if (ActivateObjective_2)
	{
		ObjectiveInit_2();
	}
	else if (ActivateObjective_3)
	{
		ObjectiveInit_3();
	}
	else if (RunObjective_0)
	{
		ObjectiveLogic_0();
	}
	else if (RunObjective_1)
	{
		ObjectiveLogic_1();
	}
	else if (RunObjective_2)
	{
		ObjectiveLogic_2();
	}
	else if (RunObjective_3)
	{
		ObjectiveLogic_3();
	}
}
