#pragma once

#include "Common.h"
#include "MissionBase.h"

class CampRaidLaw : public MissionBase
{
protected:
	Vector3 CampPoint = {0,0,0};
	float CampRadius = 10.0f;

	int waveTimer = 0;

	struct SniperData
	{
		LocationData spawn;
		Vector3 destination;
	};

	Hash LawHorse = HORSE_WALKER;

	std::vector<LocationData> FootSpawns;
	std::vector<LocationData> RoadSpawns;
	std::vector<LocationData> FlankingSpawns;
	std::vector<SniperData> SniperSpawnDatas;
	
	Ped SpawnEnemy(std::string model, Vector3 pos, float rot, PedType type, bool rider, bool finalWave = false);
	Ped SpawnEnemySniper(Vector3 pos, float rot, Vector3 target, PedType type);

	std::vector<Ped> SpawnedEnemies;
	std::vector<Ped> SpawnedFinalEnemies;
	std::vector<Ped> SpawnedSniperEnemies;

	virtual void SpawnFirstWave();
	virtual void SpawnSecondWave();
	virtual void SpawnFinalWave();

	void DoggoLogic();

	bool SetMissionParamsForCamp(int camp) override;

	void CommonInit() override;
	void TrackRewards() override;

	bool ActivateObjective_0 = true;
	bool ActivateObjective_1 = false;
	bool ActivateObjective_2 = false;
	bool ActivateObjective_3 = false;

	bool RunObjective_0 = false;
	bool RunObjective_1 = false;
	bool RunObjective_2 = false;
	bool RunObjective_3 = false;

	void ObjectiveInit_0();
	void ObjectiveInit_1();
	void ObjectiveInit_2();
	void ObjectiveInit_3();

	void ObjectiveLogic_0();
	void ObjectiveLogic_1();
	void ObjectiveLogic_2();
	void ObjectiveLogic_3();

public:
	CampRaidLaw();
	~CampRaidLaw();

	void Setup(int campid, const std::string& gangstr, const std::vector<MissionCompanionInfo>& companions) override;

	void Update() override;

	static const MissionStaticData& GetStaticMissionData();
};
