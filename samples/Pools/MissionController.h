#pragma once

#include "Common.h"
#include "MissionBase.h"

class MissionController
{
	MissionBase* _activeMission = nullptr;

	void destroyCurrentMission();

public:
	MissionController() {}
	~MissionController();
	
	inline bool HasActiveMission() const { return _activeMission != nullptr; }
	bool ActiveMissionHasLawOverride();
	bool ActiveMissionFinishedLoading = false;

	MissionResults CachedMissionResults = {MissionType::INVALID, -1, "NONE", false, {}, -1};
	bool ServedCachedMissionResults = true;

	bool StartMission(MissionType missionid, int campid, const std::string& gangstr,
					const std::vector<MissionCompanionInfo>& companions, std::function<MissionBase* (MissionType)> spawnerFunc);

	void Update();

	static int CalculatePlayerCutFromMoneyAmount(int amount);
};
