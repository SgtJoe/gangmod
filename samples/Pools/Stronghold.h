#pragma once

#include "Common.h"
#include "MissionBase.h"

class Stronghold : public MissionBase
{
	struct StrongholdData
	{
		std::string DebugName;
		Vector3 BlipPoint;
		float BlipRadius = 5.0f;
		std::vector<ExtraProps> ExtraProps;
		std::vector<LocationData> GangSpawns;
		std::vector<LocationData> GangSniperSpawns;
		std::vector<LocationData> GangMaximSpawns;
		std::vector<Vector3> GangReinforcementSpawns;
		std::vector<LocationData> StashSpawns;
		std::vector<Vector3> StashBlips;
		int StashAmount;
	};

	std::vector<StrongholdData> AllAvailableVariants;
	StrongholdData ChosenVariant;

	Blip AreaBlip = 0;
	std::vector<Blip> StashBlips;

	std::string GangModel;

	std::vector<Ped> Gangsters;
	std::vector<Vehicle> Maxims;

	int StashIndex = 0;

	int GangsterCasualties = 0;

	bool GuardsAlerted = false;
	bool GuardsCombat = false;
	int GuardSpotTime = 0;

	int CommentTimer = 0;

	bool MaximSpawned = false;

	bool SetMissionParamsForCamp(int camp) override;

	void CommonInit() override;
	void TrackRewards() override;

	bool ActivateObjective_0 = true;
	bool ActivateObjective_1 = false;
	bool ActivateObjective_2 = false;
	bool ActivateObjective_3 = false;

	bool RunObjective_0 = false;
	bool RunObjective_1 = false;
	bool RunObjective_2 = false;
	bool RunObjective_3 = false;

	void ObjectiveInit_0();
	void ObjectiveInit_1();
	void ObjectiveInit_2();
	void ObjectiveInit_3();

	void ObjectiveLogic_0();
	void ObjectiveLogic_1();
	void ObjectiveLogic_2();
	void ObjectiveLogic_3();

public:
	Stronghold();
	~Stronghold();

	void Setup(int campid, const std::string& gangstr, const std::vector<MissionCompanionInfo>& companions) override;

	void Update() override;

	static const MissionStaticData& GetStaticMissionData();
};
