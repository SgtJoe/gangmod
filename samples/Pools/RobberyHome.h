#pragma once

#include "Common.h"
#include "MissionBase.h"

class RobberyHome : public MissionBase
{
	struct RobberyHomeData
	{
		std::string DebugName;
		Vector3 BlipPoint;
		std::vector<ExtraProps> ExtraPropsets;
		std::vector<LocationData> DefenderSpawns;
		std::vector<LocationData> CivilianSpawns;
		std::vector<Vector3> LawSpawns;
		int StashAmount;
		LocationData StashPos;
	};

	std::vector<RobberyHomeData> AllAvailableVariants;
	RobberyHomeData ChosenVariant;
	
	std::string DefenderModel = "A_M_M_FARMTRAVELERS_COOL_01";
	std::string CivilianModel = "A_F_M_FAMILYTRAVELERS_COOL_01";

	Blip AreaBlip = 0;

	std::vector<Ped> Defenders;
	std::vector<Ped> Civilians;

	int defenderCasualties = 0;

	const std::string LootAnim = "base";
	const std::string LootAnimDict = "mech_ransack@player@chimney@70cm@empty@a";

	Prompt LootPrompt = 0;

	bool SetMissionParamsForCamp(int camp) override;
	
	void CommonInit() override;
	void TrackRewards() override;

	bool ActivateObjective_0 = true;
	bool ActivateObjective_1 = false;
	bool ActivateObjective_2 = false;
	bool ActivateObjective_3 = false;

	bool RunObjective_0 = false;
	bool RunObjective_1 = false;
	bool RunObjective_2 = false;
	bool RunObjective_3 = false;

	void ObjectiveInit_0();
	void ObjectiveInit_1();
	void ObjectiveInit_2();
	void ObjectiveInit_3();

	void ObjectiveLogic_0();
	void ObjectiveLogic_1();
	void ObjectiveLogic_2();
	void ObjectiveLogic_3();

public:
	RobberyHome();
	~RobberyHome();

	void Setup(int campid, const std::string& gangstr, const std::vector<MissionCompanionInfo>& companions) override;

	void Update() override;

	static const MissionStaticData& GetStaticMissionData();
};
