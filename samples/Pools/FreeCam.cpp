
#include "precomp.h"
#include "FreeCam.h"

void FreeCam::setCamState(bool on)
{
	CAM::SET_CAM_ACTIVE(_camHandle, on);
	CAM::RENDER_SCRIPT_CAMS(on, false, 3000, true, false, 1);
}

void FreeCam::resetCam()
{
	_camPos = CAM::GET_GAMEPLAY_CAM_COORD();
	_camRot = CAM::GET_GAMEPLAY_CAM_ROT(0);
	_camFOV = DefaultFieldOfView;

	CAM::SET_CAM_COORD(_camHandle, _camPos.x, _camPos.y, _camPos.z);
	CAM::SET_CAM_ROT(_camHandle, _camRot.x, _camRot.y, _camRot.z, 0);
	CAM::SET_CAM_FOV(_camHandle, _camFOV);
}

void FreeCam::HandleCam()
{
	if (CAM::DOES_CAM_EXIST(_camHandle))
	{
		Hash master = MISC::GET_HASH_KEY("s_crateseat03x");
		if (!STREAMING::HAS_MODEL_LOADED(master))
		{
			STREAMING::REQUEST_MODEL(master, true);
			while (!STREAMING::HAS_MODEL_LOADED(master))
			{
				WAIT(0);
			}
		}

		Object ref = OBJECT::CREATE_OBJECT_NO_OFFSET(master, _camPos.x, _camPos.y, _camPos.z, true, true, false, false);
		ENTITY::FREEZE_ENTITY_POSITION(ref, true);
		ENTITY::_SET_ENTITY_COORDS_AND_HEADING(ref, _camPos.x, _camPos.y, _camPos.z, _camRot.z, true, true, true);

		if (_enabled && !CAM::IS_CAM_ACTIVE(_camHandle))
		{
			setCamState(true);
			resetCam();
			MISC::SET_TIME_SCALE(0.0);
		}
		else if (!_enabled)
		{
			resetCam();
			setCamState(false);
			ENTITY::DELETE_ENTITY(&ref);
			//MISC::SET_TIME_SCALE(1.0);
			return;
		}

		if (IsKeyDown(VK_UP))
		{
			_camPos = ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ref, 0, 0.05, 0);
		}

		if (IsKeyDown(VK_DOWN))
		{
			_camPos = ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ref, 0, -0.05, 0);
		}

		if (IsKeyDown(VK_LEFT))
		{
			_camPos = ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ref, -0.05, 0, 0);
		}

		if (IsKeyDown(VK_RIGHT))
		{
			_camPos = ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ref, 0.05, 0, 0);
		}

		if (IsKeyDown(VK_CONTROL))
		{
			_camPos.z -= 0.05;
		}

		if (IsKeyDown(VK_SPACE))
		{
			_camPos.z += 0.05;
		}

		if (IsKeyJustUp(VK_ADD))
		{
			_camFOV -= 10;
		}

		if (IsKeyJustUp(VK_SUBTRACT))
		{
			_camFOV += 10;
		}

		CAM::SET_CAM_COORD(_camHandle, _camPos.x, _camPos.y, _camPos.z);

		_camRot = CAM::GET_GAMEPLAY_CAM_ROT(0);
		CAM::SET_CAM_ROT(_camHandle, _camRot.x, _camRot.y, _camRot.z, 0);

		CAM::SET_CAM_FOV(_camHandle, _camFOV);

		ENTITY::DELETE_ENTITY(&ref);

#ifdef DEBUG_CAMERA_LOG
		DioTrace("{%.2ff, %.2ff, %.2ff} {%.2ff, %.2ff, %.2ff} %d", _camPos.x, _camPos.y, _camPos.z, _camRot.x, _camRot.y, _camRot.z, (int)_camFOV);
#endif
	}
	else
	{
		static const Hash InitialCamHash = 26379945;
		_camHandle = CAM::CREATE_CAMERA(InitialCamHash, true);
		_camHandle = _camHandle;
		CAM::SET_CAM_ACTIVE(_camHandle, false);
	}
}
