
#include "precomp.h"
#include "TestMission2.h"

const MissionStaticData& TestMission2::GetStaticMissionData()
{
	static MissionStaticData missiondata = {MissionType::TESTMISSION2, 0, 3, 1500, 4025, 0, "Test Mission 2", "More advanced test mission.", COMPCOUNT_MEDIUM, true, MUSIC_DEFAULT, false};
	return missiondata;
}

TestMission2::TestMission2()
{
	DioScope(DIOTAG);
}

TestMission2::~TestMission2()
{
	DioScope(DIOTAG);
}

void TestMission2::Setup(int campid, const std::string& gangstr, const std::vector<MissionCompanionInfo>& companions)
{
	DioScope(DIOTAG "campid %d gangstr %s", campid, gangstr.c_str());

	GangName = gangstr;
	GangMemberTemplates = companions;

	CommonInit();

	if (!SetMissionParamsForCamp(campid))
	{
		ExitMissionFlag = M_EXIT_ERROR_TERMINATE;
		return;
	}
}

bool TestMission2::SetMissionParamsForCamp(int camp)
{
	switch (camp)
	{
		case DISTRICT_GRIZZLIES_WEST:
			WagonSpawnPoint = {-854.13f, 1422.85f, 232.18f};
			WagonSpawnHeading = 41;
			SwitchSpawnSide = false;
			GuardsModel = "S_M_M_DispatchLawRural_01";
			GuardsScenario = "WORLD_CAMP_GUARD_COLD";
			LawSpawn1 = {{-816.69f, 1237.85f, 168.86f}, 15};
			LawSpawn2 = {{-969.23f, 1525.91f, 244.11f}, 310};
			return true;
	}

	DioTrace(DIOTAG1("ERROR") "CANNOT SET MISSION %d PARAMS FOR CAMP %d - ABORTING MISSION", TestMission2::GetStaticMissionData().StaticMissionID, camp);
	return false;
}

void TestMission2::CommonInit()
{
	const MissionStaticData& thisMissionData = TestMission2::GetStaticMissionData();
	SetVarsFromStaticData(thisMissionData);

	MissionAssetsToLoad = {
		{"PED", "S_M_M_DispatchLawRural_01"},
		{"PED", "S_M_M_MARSHALLSRURAL_01"},
		{"VEH", "STAGECOACH004X"},
		{"OBJ", LootModel},
		{"SCN", "WORLD_CAMP_GUARD_COLD"},
		{"SCN", "WORLD_HUMAN_GUARD_LAZY"},
		{"SCN", "WORLD_HUMAN_GUARD_SCOUT"}
	};

	if (!STREAMING::HAS_ANIM_DICT_LOADED(SafeOpenDict.c_str()))
	{
		STREAMING::REQUEST_ANIM_DICT(SafeOpenDict.c_str());
	}

	if (!STREAMING::HAS_ANIM_DICT_LOADED(LootDict.c_str()))
	{
		STREAMING::REQUEST_ANIM_DICT(LootDict.c_str());
	}

	AddHorseToMissionAssets(HORSE_MUSTANG);

	ObjectivesList = {
		"Go to the ~COLOR_YELLOW~area",
		"Eliminate the ~COLOR_RED~guards",
		"Breach and loot the ~COLOR_YELLOW~safe",
		"Lose the ~COLOR_RED~law"
	};

	LoadAllMissionAssets();
	SpawnGangMembers(thisMissionData.NumMembersToSpawn);

	WAIT(2000);
}

void TestMission2::ObjectiveInit_0()
{
	ObjectiveSet(0);
	ActivateObjective_0 = false;
	RunObjective_0 = true;

	Common::ClearArea(WagonSpawnPoint, 100);

	Vehicle wagon = SpawnVehicle(WagonModel, WagonSpawnPoint, WagonSpawnHeading, true);
	VEHICLE::SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(wagon, false);
	Common::SetEntityProofs(wagon, false, true);

	SafeCoords = ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(wagon, SwitchSpawnSide ? -2.0f : 2.0f, 0.0f, 0.0f);
	SafeHeading = SwitchSpawnSide ? WagonSpawnHeading + 90 : WagonSpawnHeading - 90;

	Safe = SpawnProp(SafeHash, SafeCoords, 0);

	ENTITY::PLACE_ENTITY_ON_GROUND_PROPERLY(Safe, true);
	ENTITY::SET_ENTITY_ROTATION(Safe, 0, 0, 0, 0, false);
	ENTITY::SET_ENTITY_HEADING(Safe, SafeHeading + 180);
	ENTITY::FREEZE_ENTITY_POSITION(Safe, true);
	ENTITY::SET_ENTITY_INVINCIBLE(Safe, true);

	const int guardcount = 5;
	const LocationData guardpos[guardcount] = {
		{ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(wagon, SwitchSpawnSide ? 2.0f : -2.0f, 0.0f, 0.0f), SafeHeading + 180},
		{ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(wagon, SwitchSpawnSide ? -2.5f : 2.5f, 1.5f, 0.0f), SafeHeading},
		{ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(wagon, SwitchSpawnSide ? -2.5f : 2.5f, -1.5f, 0.0f), SafeHeading},
		{ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(wagon, SwitchSpawnSide ? -5.0f : 5.0f, 6.0f, 0.0f), SafeHeading + 90},
		{ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(wagon, SwitchSpawnSide ? -5.0f : 5.0f, -6.0f, 0.0f), SafeHeading - 90}
	};

	for (int i = 0; i < guardcount; i++)
	{
		Ped guard = SpawnPed(GuardsModel, guardpos[i].p, guardpos[i].h, 255);
		
		SetPedType(guard, PedType::REPEATER_WEAK);

		InitialGuards.emplace_back(guard);

		PED::SET_PED_RELATIONSHIP_GROUP_HASH(guard, REL_IGNORE_EVERYTHING);

		TASK::TASK_STAND_GUARD(guard, guardpos[i].p.x, guardpos[i].p.y, guardpos[i].p.z, guardpos[i].h, GuardsScenario.c_str());
	}

	AreaBlip = ObjectiveBlipCoords(WagonSpawnPoint, AreaBlipRadius, true, ObjectivesList[0]);
	
#ifdef DEBUG_FAST_MISSION
	ENTITY::_SET_ENTITY_COORDS_AND_HEADING(PlayerPed, -888.14f, 1467.98f, 239.72f, 220, true, true, true);
	ENTITY::PLACE_ENTITY_ON_GROUND_PROPERLY(PlayerPed, true);
	CAM::SET_GAMEPLAY_CAM_RELATIVE_HEADING(0, 1);
	CAM::SET_GAMEPLAY_CAM_RELATIVE_PITCH(0, 1);
#endif

	_grizzliesBlocker = VOLUME::_CREATE_VOLUME_BOX(-842.30f, 1394.48f, 220.39f, 0, 0, 0, float(500 / 0.497), float(20 / 0.497), 100);
	if (!PATHFIND::_ADD_NAVMESH_BLOCKING_VOLUME(_grizzliesBlocker, 7))
	{
		DioTrace(DIOTAG1("WARNING") "Failed to add navblocker");
	}
	ENTITY::SET_ENTITY_AS_MISSION_ENTITY(_grizzliesBlocker, true, true);
	SpawnedMissionVolumes.emplace_back(_grizzliesBlocker);
}

void TestMission2::ObjectiveLogic_0()
{
	if (Common::GetDistBetweenCoords(PlayerCoords, WagonSpawnPoint) < AreaBlipRadius)
	{
		MAP::REMOVE_BLIP(&AreaBlip);

		RunObjective_0 = false;
		ActivateObjective_1 = true;
	}
}

void TestMission2::ObjectiveInit_1()
{
	ObjectiveSet(1);
	ActivateObjective_1 = false;
	RunObjective_1 = true;

	DisableLaw();

	CheckAbandon = true;
	AbandonCoords = SafeCoords;
	AbandonRadius = AreaBlipRadius;

	for (int i = 0; i < InitialGuards.size(); i++)
	{
		PED::SET_PED_RELATIONSHIP_GROUP_HASH(InitialGuards[i], REL_PLAYER_ENEMY);

		PED::SET_PED_SEEING_RANGE(InitialGuards[i], AreaBlipRadius * 0.5f);
		PED::SET_PED_COMBAT_MOVEMENT(InitialGuards[i], 1);
		PED::SET_PED_COMBAT_ATTRIBUTES(InitialGuards[i], CA_PERMIT_CHARGE_BEYOND_DEFENSIVE_AREA, false);

		PED::SET_PED_SPHERE_DEFENSIVE_AREA(InitialGuards[i], WagonSpawnPoint.x, WagonSpawnPoint.y, WagonSpawnPoint.z, 15, 0, false, 0);
		
		Blip guardblip = MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_ENEMY, InitialGuards[i]);
		MAP::BLIP_ADD_MODIFIER(guardblip, BLIP_MODIFIER_ENEMY_ON_GUARD);
	}
}

void TestMission2::ObjectiveLogic_1()
{
	if (MISC::IS_BULLET_IN_AREA(WagonSpawnPoint.x, WagonSpawnPoint.y, WagonSpawnPoint.z, AreaBlipRadius, true) || 
		FIRE::IS_EXPLOSION_IN_SPHERE(-1, WagonSpawnPoint.x, WagonSpawnPoint.y, WagonSpawnPoint.z, AreaBlipRadius))
	{
		GuardsAlerted = true;
	}

	if (!GuardsAlerted && GuardSpotTime == 0)
	{
		for (int i = 0; i < InitialGuards.size(); i++)
		{
			bool spottedplayer = PED::CAN_PED_SEE_ENTITY(InitialGuards[i], PlayerPed, true, true) == 1;

			if (GuardSpotTime == 0 && PED::IS_PED_IN_COMBAT(InitialGuards[i], PlayerPed))
			{
				GuardsAlerted = true;
				spottedplayer = true;
				GuardSpotTime = MISC::GET_GAME_TIMER();
			}

			if (GuardSpotTime == 0 && spottedplayer)
			{
				GuardSpotTime = MISC::GET_GAME_TIMER();

				ScriptedSpeechParams params{"ITS_THEM_EXTREME", 0, 0, MISC::GET_HASH_KEY("SPEECH_PARAMS_BEAT_SHOUTED_CLEAR_NOSUB"), 0, true, 0, 0 };
				AUDIO::PLAY_PED_AMBIENT_SPEECH_NATIVE(InitialGuards[i], (Any*)&params);
				
				TASK::CLEAR_PED_TASKS(InitialGuards[i], true, true);
				TASK::TASK_AIM_AT_ENTITY(InitialGuards[i], PlayerPed, 9000, 0, 0);
			}
		}
	}
	else if (!GuardsAlerted && MISC::GET_GAME_TIMER() - GuardSpotTime > 2000)
	{
		GuardsAlerted = true;
	}
	else if (!GuardsCombat && GuardsAlerted)
	{
		GuardsCombat = true;

		for (int i = 0; i < InitialGuards.size(); i++)
		{
			MAP::BLIP_REMOVE_MODIFIER(MAP::GET_BLIP_FROM_ENTITY(InitialGuards[i]), BLIP_MODIFIER_ENEMY_ON_GUARD);

			TASK::CLEAR_PED_TASKS(InitialGuards[i], true, true);
			TASK::TASK_COMBAT_HATED_TARGETS(InitialGuards[i], 100);

			CompanionsBeginCombat();

			CurrentMusicStage = STAGE_ACTION1;
		}
	}
	else
	{
		bool alldead = true;

#ifdef DEBUG_FAST_MISSION
		for (int i = 0; i < InitialGuards.size(); i++)
		{
			PED::EXPLODE_PED_HEAD(InitialGuards[i], WEAPON_REVOLVER_CATTLEMAN);
		}
#endif
		for (int i = 0; i < InitialGuards.size(); i++)
		{
			if (!PED::IS_PED_DEAD_OR_DYING(InitialGuards[i], true))
			{
				alldead = false;
			}
		}

		if (alldead)
		{
			RunObjective_1 = false;
			ActivateObjective_2 = true;
		}
	}
}

void TestMission2::ObjectiveInit_2()
{
	ObjectiveSet(2);
	ActivateObjective_2 = false;
	RunObjective_2 = true;

	CurrentMusicStage = STAGE_AFTERACTION;
	
	SafeBlip = MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_LOOT_OBJECTIVE, Safe);
	MAP::SET_BLIP_SPRITE(SafeBlip, MISC::GET_HASH_KEY("blip_weapon_throwable"), true);
	MAP::_SET_BLIP_NAME_FROM_PLAYER_STRING(SafeBlip, ObjectivesList[2].c_str());

	SafeInteractionPoint = ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Safe, 0.2, -1, 0);

	BreachPrompt = HUD::_UIPROMPT_REGISTER_BEGIN();
	HUD::_UIPROMPT_SET_CONTROL_ACTION(BreachPrompt, MISC::GET_HASH_KEY("INPUT_IGNITE"));
	HUD::_UIPROMPT_SET_TEXT(BreachPrompt, MISC::VAR_STRING(10, "LITERAL_STRING", "Place Dynamite"));
	HUD::_UIPROMPT_SET_STANDARDIZED_HOLD_MODE(BreachPrompt, 1);
	HUD::_UIPROMPT_REGISTER_END(BreachPrompt);
	HUD::_UIPROMPT_SET_ENABLED(BreachPrompt, true);
	HUD::_UIPROMPT_SET_VISIBLE(BreachPrompt, false);

	LootPrompt = HUD::_UIPROMPT_REGISTER_BEGIN();
	HUD::_UIPROMPT_SET_CONTROL_ACTION(LootPrompt, MISC::GET_HASH_KEY("INPUT_LOOT"));
	HUD::_UIPROMPT_SET_TEXT(LootPrompt, MISC::VAR_STRING(10, "LITERAL_STRING", "Loot Safe"));
	HUD::_UIPROMPT_SET_STANDARDIZED_HOLD_MODE(LootPrompt, 1);
	HUD::_UIPROMPT_REGISTER_END(LootPrompt);
	HUD::_UIPROMPT_SET_ENABLED(LootPrompt, false);
	HUD::_UIPROMPT_SET_VISIBLE(LootPrompt, false);
}

void TestMission2::ObjectiveLogic_2()
{
	if (!STREAMING::HAS_ANIM_DICT_LOADED(SafeOpenDict.c_str()))
	{
		STREAMING::REQUEST_ANIM_DICT(SafeOpenDict.c_str());
	}

	if (!STREAMING::HAS_ANIM_DICT_LOADED(LootDict.c_str()))
	{
		STREAMING::REQUEST_ANIM_DICT(LootDict.c_str());
	}
	
	if (!BombPlanted && BUILTIN::VDIST2(PlayerCoords.x, PlayerCoords.y, PlayerCoords.z, SafeInteractionPoint.x, SafeInteractionPoint.y, SafeInteractionPoint.z) < 2.0)
	{
		HUD::_UIPROMPT_SET_VISIBLE(BreachPrompt, true);
	}
	else if (!BombPlanted && BUILTIN::VDIST2(PlayerCoords.x, PlayerCoords.y, PlayerCoords.z, SafeInteractionPoint.x, SafeInteractionPoint.y, SafeInteractionPoint.z) > 2.0)
	{
		HUD::_UIPROMPT_SET_VISIBLE(BreachPrompt, false);
	}
	
	if (!BombPlanted && HUD::_UIPROMPT_HAS_HOLD_MODE_COMPLETED(BreachPrompt))
	{
		BombPlanted = true;
		CompanionTaskControl = COMPTASK_TAKE_COVER;

		HUD::_UIPROMPT_SET_VISIBLE(BreachPrompt, false);
		HUD::_UIPROMPT_SET_ENABLED(BreachPrompt, false);

		WEAPON::_ADD_AMMO_TO_PED_BY_TYPE(PlayerPed, MISC::GET_HASH_KEY("AMMO_DYNAMITE"), 8, ADD_REASON_DEFAULT);
		WEAPON::SET_CURRENT_PED_WEAPON(PlayerPed, WEAPON_THROWN_DYNAMITE, true, WEAPON_ATTACH_POINT_HAND_PRIMARY, false, false);

		for (int i = 0; i < InitialGuards.size(); i++)
		{
			ENTITY::SET_ENTITY_NO_COLLISION_ENTITY(InitialGuards[i], PlayerPed, false);
		}

		int sequence = 0;
		TASK::OPEN_SEQUENCE_TASK(&sequence);

		TASK::TASK_PED_SLIDE_TO_COORD(0, SafeInteractionPoint.x, SafeInteractionPoint.y, SafeInteractionPoint.z, SafeHeading + 180, 0.75);
		TASK::TASK_PLANT_BOMB(0, 0, 0, 0, 0);

		TASK::CLOSE_SEQUENCE_TASK(sequence);
		TASK::TASK_PERFORM_SEQUENCE(PlayerPed, sequence);
		TASK::CLEAR_SEQUENCE_TASK(&sequence);
	}

	if (CompanionTaskControl == COMPTASK_TAKE_COVER)
	{
		CompanionTaskControl = 0;

		CompanionsClearTasks();
		CompanionsDisband();

		for (int i = 0; i < SpawnedGangMembers.size(); i++)
		{
			TASK::TASK_SEEK_COVER_FROM_POS(SpawnedGangMembers[i].member, WagonSpawnPoint.x, WagonSpawnPoint.y, WagonSpawnPoint.z, 5000, 0, 0, 0);
		}
	}

	bool bombexploded =
		FIRE::IS_EXPLOSION_IN_SPHERE(EXP_TAG_DYNAMITE, SafeCoords.x, SafeCoords.y, SafeCoords.z, 2.5f) ||
		FIRE::IS_EXPLOSION_IN_SPHERE(EXP_TAG_DYNAMITESTACK, SafeCoords.x, SafeCoords.y, SafeCoords.z, 2.5f) ||
		FIRE::IS_EXPLOSION_IN_SPHERE(EXP_TAG_DYNAMITE_ARROW, SafeCoords.x, SafeCoords.y, SafeCoords.z, 2.5f) ||
		FIRE::IS_EXPLOSION_IN_SPHERE(EXP_TAG_DYNAMITE_VOLATILE, SafeCoords.x, SafeCoords.y, SafeCoords.z, 2.5f) ||
		FIRE::IS_EXPLOSION_IN_SPHERE(EXP_TAG_PLACED_DYNAMITE, SafeCoords.x, SafeCoords.y, SafeCoords.z, 2.5f);

	if (!BombPlanted && !SafeExploded && bombexploded && !ENTITY::DOES_ENTITY_EXIST(SpawnedLoot1))
	{
		BombPlanted = true;
		SafeExploded = true;
		HUD::_UIPROMPT_SET_VISIBLE(BreachPrompt, false);
		HUD::_UIPROMPT_SET_ENABLED(BreachPrompt, false);
	}

	if (/*SafeExploded &&*/ bombexploded && !ENTITY::DOES_ENTITY_EXIST(SpawnedLoot1))
	{
		SafeExploded = true;
		
		CompanionTaskControl = COMPTASK_COMMENT_ON_SAFE;
		CompanionTaskTimer = MISC::GET_GAME_TIMER();
		CompanionsClearTasks();
		CompanionsRegroup();

		MAP::SET_BLIP_SPRITE(SafeBlip, MISC::GET_HASH_KEY("blip_cash_bag"), false);

		ENTITY::PLAY_ENTITY_ANIM(Safe, SafeOpenAnim.c_str(), SafeOpenDict.c_str(), 1.0, false, true, true, 0.0, 0);

		SpawnedLoot1 = SpawnProp(LootModel, SafeCoords, 0);
		SpawnedLoot2 = SpawnProp(LootModel, SafeCoords, 0);

		ENTITY::SET_ENTITY_COMPLETELY_DISABLE_COLLISION(Safe, true, false);

		ENTITY::SET_ENTITY_COMPLETELY_DISABLE_COLLISION(SpawnedLoot1, true, false);
		ENTITY::SET_ENTITY_COORDS(SpawnedLoot1, SafeCoords.x - 0.15f, SafeCoords.y - 0.18f, SafeCoords.z - 0.75f, false, false, false, false);
		ENTITY::FREEZE_ENTITY_POSITION(SpawnedLoot1, true);

		ENTITY::SET_ENTITY_COMPLETELY_DISABLE_COLLISION(SpawnedLoot2, true, false);
		ENTITY::SET_ENTITY_COORDS(SpawnedLoot2, SafeCoords.x - 0.15f, SafeCoords.y - 0.18f, SafeCoords.z - 0.75f, false, false, false, false);
		ENTITY::FREEZE_ENTITY_POSITION(SpawnedLoot2, true);

		HUD::_UIPROMPT_SET_VISIBLE(BreachPrompt, false);
		HUD::_UIPROMPT_SET_ENABLED(BreachPrompt, false);

		HUD::_UIPROMPT_SET_ENABLED(LootPrompt, true);
	}

	if (CompanionTaskControl == COMPTASK_COMMENT_ON_SAFE && MISC::GET_GAME_TIMER() - CompanionTaskTimer > 4500)
	{
		CompanionTaskControl = 0;
		CompanionTaskTimer = 0;
		PlayCompanionSpeech("COME_SEE_THIS");
		CompanionsClearTasks();
		CompanionsDisband();
	}

	if (!bombexploded && SafeExploded && !SafeLooted && BUILTIN::VDIST2(PlayerCoords.x, PlayerCoords.y, PlayerCoords.z, SafeInteractionPoint.x, SafeInteractionPoint.y, SafeInteractionPoint.z) < 2.0)
	{
		HUD::_UIPROMPT_SET_VISIBLE(LootPrompt, true);
	}
	else if (!bombexploded && SafeExploded && !SafeLooted && BUILTIN::VDIST2(PlayerCoords.x, PlayerCoords.y, PlayerCoords.z, SafeInteractionPoint.x, SafeInteractionPoint.y, SafeInteractionPoint.z) > 2.0)
	{
		HUD::_UIPROMPT_SET_VISIBLE(LootPrompt, false);
	}

	if (!SafeLooted && HUD::_UIPROMPT_HAS_HOLD_MODE_COMPLETED(LootPrompt))
	{
		SafeLooted = true;

		HUD::_UIPROMPT_SET_VISIBLE(LootPrompt, false);
		HUD::_UIPROMPT_SET_ENABLED(LootPrompt, false);

		TASK::CLEAR_PED_TASKS(PlayerPed, true, true);
		TASK::TASK_PED_SLIDE_TO_COORD(PlayerPed, SafeInteractionPoint.x, SafeInteractionPoint.y, SafeInteractionPoint.z, SafeHeading + 180, 0.75f);
		WAIT(1500);
	}

	if (SafeLooted && TASK::GET_SEQUENCE_PROGRESS(PlayerPed) == -1)
	{
		TASK::CLEAR_PED_TASKS_IMMEDIATELY(PlayerPed, true, true);
		ENTITY::SET_ENTITY_HEADING(PlayerPed, SafeHeading + 180);
		TASK::TASK_PLAY_ANIM(PlayerPed, LootDict.c_str(), LootAnim.c_str(), 1.0, 1.0f, -1, 0, 0, 0, 0, 0, 0, 0);
		WAIT(2500);

		ENTITY::SET_ENTITY_VISIBLE(SpawnedLoot1, false);
		ENTITY::SET_ENTITY_VISIBLE(SpawnedLoot2, false);
		
		MAP::REMOVE_BLIP(&SafeBlip);

		RunObjective_2 = false;
		ActivateObjective_3 = true;
	}
}

void TestMission2::ObjectiveInit_3()
{
	ObjectiveSet(3);
	ActivateObjective_3 = false;
	RunObjective_3 = true;

	PATHFIND::_REMOVE_NAVMESH_BLOCKING_VOLUME(_grizzliesBlocker);

	CompanionsRegroup();

	CheckAbandon = false;

	AllowWanted = true;
	BecomeWanted(CRIME_STAGECOACH_ROBBERY, 5);
	SetDispatchService(false);

	WAIT(1500);

	std::vector<std::string> lawhorses = InventoryManager::GetStaticHorseData(HORSE_MUSTANG).PedModels;

	std::string lawvoices[3] = {"0995_S_M_M_DISPATCHLEADERRURAL_WHITE_01", "0996_S_M_M_DISPATCHLEADERRURAL_WHITE_02", "0997_S_M_M_DISPATCHLEADERRURAL_WHITE_03"};

	for (int i = 1; i <= 4; i++)
	{
		Ped lawman = SpawnPed("S_M_M_MARSHALLSRURAL_01", LawSpawn1.p, LawSpawn1.h, 255);
		
		AUDIO::SET_AMBIENT_VOICE_NAME(lawman, lawvoices[rand() % 3].c_str());

		if (i == 3)
		{
			SetPedType(lawman, PedType::SHOTGUN_EXPERT);
		}
		else if (i == 4)
		{
			SetPedType(lawman, PedType::SNIPER_EXPERT);
		}
		else
		{
			SetPedType(lawman, PedType::REPEATER_REGULAR);
		}

		Blip lawblip = MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_ENEMY, lawman);
		MAP::SET_BLIP_SPRITE(lawblip, MISC::GET_HASH_KEY("blip_ambient_law"), true);
		MAP::BLIP_ADD_MODIFIER(lawblip, BLIP_MODIFIER_ENEMY_ON_GUARD);

		PED::SET_PED_SEEING_RANGE(lawman, 35);

		Ped lawhorse = SpawnPed(lawhorses[rand() % lawhorses.size()], Common::ApplyRandomSmallOffsetToCoords(LawSpawn1.p, 2.5f), LawSpawn1.h, 255);

		PED::SET_PED_ONTO_MOUNT(lawman, lawhorse, -1, true);

		DefaultHorseSetup(lawhorse);
		PED::SET_PED_CONFIG_FLAG(lawhorse, PCF_BlockMountHorsePrompt, true);
		
		TASK::TASK_FOLLOW_NAV_MESH_TO_COORD(lawhorse, SafeInteractionPoint.x, SafeInteractionPoint.y, SafeInteractionPoint.z, 5, -1, 10.0f, 4194304, 40000.0f);
		
		ActivePursuitParams.ScriptedLawPeds.emplace_back(lawman);
	}

	for (int i = 1; i <= 4; i++)
	{
		Ped lawman = SpawnPed("S_M_M_MARSHALLSRURAL_01", LawSpawn2.p, LawSpawn2.h, 255);

		AUDIO::SET_AMBIENT_VOICE_NAME(lawman, lawvoices[rand() % 3].c_str());

		if (i == 3)
		{
			SetPedType(lawman, PedType::SHOTGUN_EXPERT);
		}
		else if (i == 4)
		{
			SetPedType(lawman, PedType::SNIPER_EXPERT);
		}
		else
		{
			SetPedType(lawman, PedType::REPEATER_REGULAR);
		}

		Blip lawblip = MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_ENEMY, lawman);
		MAP::SET_BLIP_SPRITE(lawblip, MISC::GET_HASH_KEY("blip_ambient_law"), true);
		MAP::BLIP_ADD_MODIFIER(lawblip, BLIP_MODIFIER_ENEMY_ON_GUARD);

		PED::SET_PED_SEEING_RANGE(lawman, 35);

		Ped lawhorse = SpawnPed(lawhorses[rand() % lawhorses.size()], Common::ApplyRandomSmallOffsetToCoords(LawSpawn2.p, 2.5f), LawSpawn2.h, 255);

		PED::SET_PED_ONTO_MOUNT(lawman, lawhorse, -1, true);

		DefaultHorseSetup(lawhorse);
		PED::SET_PED_CONFIG_FLAG(lawhorse, PCF_BlockMountHorsePrompt, true);

		TASK::TASK_FOLLOW_NAV_MESH_TO_COORD(lawhorse, SafeInteractionPoint.x, SafeInteractionPoint.y, SafeInteractionPoint.z, 5, -1, 10.0f, 4194304, 40000.0f);

		ActivePursuitParams.ScriptedLawPeds.emplace_back(lawman);
	}

	PED::SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, REL_PLAYER, REL_COP);
	PED::SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, REL_GANG_DUTCHS, REL_COP);

	ActivePursuitParams.DispatchResetDelay = -1;
	ActivePursuitParams.CompanionCombatOnSpotted = true;
}

void TestMission2::ObjectiveLogic_3()
{
	if (ActivePursuitParams.DispatchResetDelay != 0)
	{
		bool alldead = true;

		for (int i = 0; i < ActivePursuitParams.ScriptedLawPeds.size(); i++)
		{
			if (!PED::IS_PED_DEAD_OR_DYING(ActivePursuitParams.ScriptedLawPeds[i], true))
			{
				alldead = false;
			}
		}
		
		if (alldead)
		{
			DioTrace("All scripted lawmen killed, resetting dispatch");
			SetDispatchService(true);
			ActivePursuitParams.DispatchResetDelay = 0;
		}
	}
	
	if (PlayerIncognito && MISC::GET_DISTANCE_BETWEEN_COORDS(PlayerCoords.x, PlayerCoords.y, PlayerCoords.z, SafeInteractionPoint.x, SafeInteractionPoint.y, SafeInteractionPoint.z, false) > 300)
	{
		ClearWanted(false);
	}

	if (!LAW::IS_LAW_INCIDENT_ACTIVE(0))
	{
		CurrentMusicStage = STAGE_AFTERACTION;
		UpdateMusic();
	}
	
	if (!WantedLogic())
	{
		RunObjective_3 = false;
		ExitMissionFlag = M_EXIT_SUCCESS;
	}
}

void TestMission2::TrackRewards()
{
	RewardBounty_Active = RewardBounty_Potential;
	RewardCash_Active = RewardCash_Potential;
	RewardMembers_Active = RewardMembers_Potential;
}

void TestMission2::Update()
{
	if (TimeMissionFail > 0 && MISC::GET_GAME_TIMER() - TimeMissionFail > TimeMissionFailDelay)
	{
		DioTrace("Exit Fail Triggered");

		ExitMissionFlag = M_EXIT_FAILURE;
	}
	else if (TimeMissionSuccess > 0 && MISC::GET_GAME_TIMER() - TimeMissionSuccess > TimeMissionSuccessDelay)
	{
		DioTrace("Exit Win Triggered");

		ExitMissionFlag = M_EXIT_SUCCESS;
	}
	else if (ActivateObjective_0)
	{
		ObjectiveInit_0();
	}
	else if (ActivateObjective_1)
	{
		ObjectiveInit_1();
	}
	else if (ActivateObjective_2)
	{
		ObjectiveInit_2();
	}
	else if (ActivateObjective_3)
	{
		ObjectiveInit_3();
	}
	else if (RunObjective_0)
	{
		ObjectiveLogic_0();
	}
	else if (RunObjective_1)
	{
		ObjectiveLogic_1();
	}
	else if (RunObjective_2)
	{
		ObjectiveLogic_2();
	}
	else if (RunObjective_3)
	{
		ObjectiveLogic_3();
	}
}
