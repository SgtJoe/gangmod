#pragma once

#include "Common.h"
#include "MissionBase.h"

class JailbreakWagon : public MissionBase
{
	bool SetMissionParamsForCamp(int camp) override;

	struct JailbreakWagonData
	{
		Vector3 AmbushPoint;
		LocationData WagonSpawn;
		Vector3 WagonDestination;
		std::vector<Vector3> LawSpawns;
	};

	std::vector<JailbreakWagonData> AllAvailableCollections;
	JailbreakWagonData ChosenCollection;

	std::string PrisonerModel;

	Blip AmbushBlip = 0;
	const std::string WagonModel = "WAGONPRISON01X";
	const std::string WagonLights = "PG_VEH_WAGONPRISON01X_LANTERNS01";
	Vehicle Wagon = 0;
	Vector3 WagonCoords = {0, 0, 0};
	Blip WagonBlip = 0;

	Ped WagonDriver = 0;
	std::vector<Ped> WagonGuards;

	bool StartedWagonAttack = false;

	Ped Prisoner1 = 0;
	Ped Prisoner2 = 0;
	Ped Prisoner1Horse = 0;
	Ped Prisoner2Horse = 0;

	bool Prisoner1Dead = false;
	bool Prisoner2Dead = false;

	int PrisonerFreeTime = 0;

	void CommonInit() override;
	void TrackRewards() override;

	bool ActivateObjective_0 = true;
	bool ActivateObjective_1 = false;
	bool ActivateObjective_2 = false;
	bool ActivateObjective_3 = false;

	bool RunObjective_0 = false;
	bool RunObjective_1 = false;
	bool RunObjective_2 = false;
	bool RunObjective_3 = false;

	void ObjectiveInit_0();
	void ObjectiveInit_1();
	void ObjectiveInit_2();
	void ObjectiveInit_3();

	void ObjectiveLogic_0();
	void ObjectiveLogic_1();
	void ObjectiveLogic_2();
	void ObjectiveLogic_3();

public:
	JailbreakWagon();
	~JailbreakWagon();

	void Setup(int campid, const std::string& gangstr, const std::vector<MissionCompanionInfo>& companions) override;

	void Update() override;

	static const MissionStaticData& GetStaticMissionData();
};
