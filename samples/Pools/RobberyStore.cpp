
#include "precomp.h"
#include "RobberyStore.h"

const MissionStaticData& RobberyStore::GetStaticMissionData()
{
	static MissionStaticData missiondata = {
		MissionType::ROBBERY_STORE,
		0, 4, 2000, 4000, 0,
		"Store Robbery",
		"Emptying cash registers is a classic way to get rich quick.",
		COMPCOUNT_MIN,
		true,
		MUSIC_DEFAULT,
		false
	};
	return missiondata;
}

void RobberyStore::EnableRegisterScenarios(bool on)
{
	TASK::SET_SCENARIO_TYPE_ENABLED("PROP_PLAYER_ROB_CASH_REGISTER", on);
	TASK::SET_SCENARIO_TYPE_ENABLED("PROP_PLAYER_CASH_REGISTER_CLERK_OPENED_BENCHMARK_TEST", on);
	TASK::SET_SCENARIO_TYPE_ENABLED("PROP_PLAYER_CASH_REGISTER_CLERK_OPENED", on);
	TASK::SET_SCENARIO_TYPE_ENABLED("PROP_PLAYER_CASH_REGISTER_LOCKBREAK", on);
	TASK::SET_SCENARIO_TYPE_ENABLED("PROP_PLAYER_CASH_REGISTER_OPEN", on);
	TASK::SET_SCENARIO_TYPE_ENABLED("PROP_PLAYER_CASH_REGISTER_OPEN_GUN", on);
	TASK::SET_SCENARIO_TYPE_ENABLED("PROP_PLAYER_CASH_REGISTER", on);
}

RobberyStore::RobberyStore()
{
	DioScope(DIOTAG);
	EnableRegisterScenarios(false);
}

RobberyStore::~RobberyStore()
{
	DioScope(DIOTAG); 
	EnableRegisterScenarios(true);

	GlobalVarsAccess::SetRegionLocked(CurrentTownInfo.TownRegion, false);
}

void RobberyStore::Setup(int campid, const std::string& gangstr, const std::vector<MissionCompanionInfo>& companions)
{
	DioScope(DIOTAG "campid %d gangstr %s", campid, gangstr.c_str());

	GangName = gangstr;
	GangMemberTemplates = companions;

	if (!SetMissionParamsForCamp(campid))
	{
		ExitMissionFlag = M_EXIT_ERROR_TERMINATE;
		return;
	}

	CommonInit();
}

bool RobberyStore::SetMissionParamsForCamp(int camp)
{
	bool success = false;

	switch (camp)
	{
		case DISTRICT_GRIZZLIES_WEST:
		case DISTRICT_CUMBERLAND_FOREST:
			CurrentTownInfo = ValentineTownInfo;
			success = true;
			break;

		case DISTRICT_BLUEGILL_MARSH:
			CurrentTownInfo = SaintDenisTownInfo;
			success = true;
			break;

		case DISTRICT_RIO_BRAVO:
			CurrentTownInfo = TumbleweedTownInfo;
			success = true;
			break;
	}

	ScriptsKillList = {
		MISC::GET_HASH_KEY("shop_bait"),
		MISC::GET_HASH_KEY("shop_barber"),
		MISC::GET_HASH_KEY("shop_butcher"),
		MISC::GET_HASH_KEY("shop_doctor"),
		MISC::GET_HASH_KEY("shop_dynamic"),
		MISC::GET_HASH_KEY("shop_fence"),
		MISC::GET_HASH_KEY("shop_general"),
		MISC::GET_HASH_KEY("shop_gunsmith"),
		MISC::GET_HASH_KEY("shop_horse_shop"),
		MISC::GET_HASH_KEY("shop_horse_shop_sp"),
		MISC::GET_HASH_KEY("shop_hotel"),
		MISC::GET_HASH_KEY("shop_market"),
		MISC::GET_HASH_KEY("shop_net_barber"),
		MISC::GET_HASH_KEY("shop_newspaper_boy"),
		MISC::GET_HASH_KEY("shop_pearson"),
		MISC::GET_HASH_KEY("shop_photo_studio"),
		MISC::GET_HASH_KEY("shop_post_office"),
		MISC::GET_HASH_KEY("shop_tailor"),
		MISC::GET_HASH_KEY("shop_train_station"),
		MISC::GET_HASH_KEY("shop_trapper"),
		MISC::GET_HASH_KEY("shoprobberies"),
		MISC::GET_HASH_KEY("shoprobberies_dynamic")
	};

	if (success)
	{
		ScriptsKillList.emplace_back(CurrentTownInfo.TownScriptHash);
		SetGenericLawModels(CurrentTownInfo.TownDistrict);
	}
	else
	{
		DioTrace(DIOTAG1("ERROR") "CANNOT SET MISSION %d PARAMS FOR CAMP %d - ABORTING MISSION", RobberyStore::GetStaticMissionData().StaticMissionID, camp);
	}

	return success;
}

void RobberyStore::CommonInit()
{
	const MissionStaticData& thisMissionData = RobberyStore::GetStaticMissionData();
	SetVarsFromStaticData(thisMissionData);

	MissionAssetsToLoad = {
		{"SCN", "WORLD_HUMAN_VAL_BANKTELLER"},
		{"ANM", RegisterLootAnimDict}
	};

	for (int i = 0; i < CurrentTownInfo.Shops.size(); ++i)
	{
		MissionAssetsToLoad.push_back({"PED", CurrentTownInfo.Shops[i].ShopkeeperModel});
	}

	ObjectivesList = {
		"Choose a ~COLOR_YELLOW~store",
		"Rob the ~COLOR_YELLOW~register",
		"Lose the ~COLOR_RED~law"
	};

	LoadAllMissionAssets();
	SpawnGangMembers(thisMissionData.NumMembersToSpawn);

	WAIT(2000);
}

void RobberyStore::ObjectiveInit_0()
{
	ObjectiveSet(0);
	ActivateObjective_0 = false;
	RunObjective_0 = true;

	CurrentMusicStage = STAGE_MUTED;
	UpdateMusic();

	GlobalVarsAccess::SetRegionLocked(CurrentTownInfo.TownRegion, false);

	CLOCK::ADVANCE_CLOCK_TIME_TO(12, 0, 0);
	MISC::SET_WEATHER_TYPE(MISC::GET_HASH_KEY("SUNNY"), true, true, true, 0, true);

	ENTITY::_SET_ENTITY_COORDS_AND_HEADING(PlayerPed, CurrentTownInfo.StartLoc.p.x, CurrentTownInfo.StartLoc.p.y, CurrentTownInfo.StartLoc.p.z, CurrentTownInfo.StartLoc.h, true, true, true);

	Vector3 HorseStart = ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PlayerPed, 2, -4, 0);
	ENTITY::_SET_ENTITY_COORDS_AND_HEADING(PlayerHorse, HorseStart.x, HorseStart.y, HorseStart.z, CurrentTownInfo.StartLoc.h, true, true, true);

	ENTITY::PLACE_ENTITY_ON_GROUND_PROPERLY(PlayerPed, true);
	ENTITY::PLACE_ENTITY_ON_GROUND_PROPERLY(PlayerHorse, true);
	CAM::SET_GAMEPLAY_CAM_RELATIVE_HEADING(0, 1);
	CAM::SET_GAMEPLAY_CAM_RELATIVE_PITCH(0, 1);

	CheckAbandon = true;
	AbandonCoords = CurrentTownInfo.TownCenter;
	AbandonRadius = CurrentTownInfo.TownRadius;

	for (int i = 0; i < CurrentTownInfo.Shops.size(); ++i)
	{
		SpawnedEntryBlips.emplace_back(ObjectiveBlipCoords(CurrentTownInfo.Shops[i].StoreEntryPos, 7, false, ObjectivesList[0]));
	}

	CurrentMusicStage = STAGE_START;
	UpdateMusic();

#ifdef DEBUG_FAST_MISSION
	ENTITY::_SET_ENTITY_COORDS_AND_HEADING(PlayerPed, CurrentTownInfo.TownCenter.x, CurrentTownInfo.TownCenter.y, CurrentTownInfo.TownCenter.z, CurrentTownInfo.StartLoc.h, true, true, true);
#endif
}

void RobberyStore::ObjectiveLogic_0()
{
	for (int i = 0; i < CurrentTownInfo.Shops.size(); ++i)
	{
		if (Common::GetDistBetweenCoords(PlayerCoords, CurrentTownInfo.Shops[i].StoreEntryPos) < 5.0f && !PED::IS_PED_ON_MOUNT(PlayerPed))
		{
			CurrentShop = CurrentTownInfo.Shops[i];
			
			RunObjective_0 = false;
			ActivateObjective_1 = true;
		}
	}
}

void RobberyStore::ObjectiveInit_1()
{
	ObjectiveSet(1);
	ActivateObjective_1 = false;
	RunObjective_1 = true;

	CurrentMusicStage = STAGE_ACTION1;
	UpdateMusic();

	DisableLaw();
	
	for (int i = 0; i < SpawnedEntryBlips.size(); ++i)
	{
		MAP::REMOVE_BLIP(&SpawnedEntryBlips[i]);
	}

	Vector3 spos = CurrentShop.ShopkeeperPos;

	Common::ClearArea(spos, 20.0f);

	Shopkeeper = SpawnPed(CurrentShop.ShopkeeperModel, spos, CurrentShop.ShopkeeperRot, 255);
	PED::SET_PED_RELATIONSHIP_GROUP_HASH(Shopkeeper, REL_CIV);
	Common::SetPedHonorModifier(Shopkeeper, HONOR_NEGATIVE);

	if (CurrentShop.ShopkeeperAggressive)
	{
		if (CurrentShop.ShopkeeperModel == "G_M_M_UNIBRONTEGOONS_01")
		{
			SetPedType(Shopkeeper, PedType::SIDEARM_EXPERT);
			Common::SetPedHonorModifier(Shopkeeper, HONOR_NONE);
		}
		else
		{
			SetPedType(Shopkeeper, PedType::SHOTGUN_WEAK);
		}
	}
	else
	{
		TASK::TASK_START_SCENARIO_AT_POSITION(Shopkeeper, MISC::GET_HASH_KEY("WORLD_HUMAN_VAL_BANKTELLER"), spos.x, spos.y, spos.z, CurrentShop.ShopkeeperRot, -1, false, false, 0, 0, false);
	}

	CashRegister = OBJECT::GET_CLOSEST_OBJECT_OF_TYPE(CurrentShop.RegisterPos.x, CurrentShop.RegisterPos.y, CurrentShop.RegisterPos.z, 5.0f, CashRegisterHash, false, false, false);
	
	if (!ENTITY::DOES_ENTITY_EXIST(CashRegister))
	{
		CashRegister = OBJECT::GET_CLOSEST_OBJECT_OF_TYPE(CurrentShop.RegisterPos.x, CurrentShop.RegisterPos.y, CurrentShop.RegisterPos.z, 5.0f, CashRegisterHashAlt1, false, false, false);
	}

	if (!ENTITY::DOES_ENTITY_EXIST(CashRegister))
	{
		CashRegister = OBJECT::GET_CLOSEST_OBJECT_OF_TYPE(CurrentShop.RegisterPos.x, CurrentShop.RegisterPos.y, CurrentShop.RegisterPos.z, 5.0f, CashRegisterHashAlt2, false, false, false);
	}

	if (!ENTITY::DOES_ENTITY_EXIST(CashRegister))
	{
		DioTrace("Could not hijack any vanilla registers, spawning new one");
		CashRegister = SpawnProp(CashRegisterHash, CurrentShop.RegisterPos, 0);
	}

	ENTITY::SET_ENTITY_AS_MISSION_ENTITY(CashRegister, true, true);
	SpawnedMissionEntities.emplace_back(CashRegister);

	ENTITY::SET_ENTITY_HEADING(CashRegister, CurrentShop.RegisterRot);
	ENTITY::FREEZE_ENTITY_POSITION(CashRegister, true);
	ENTITY::SET_ENTITY_INVINCIBLE(CashRegister, true);

	CashRegisterBlip = MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_LOOT_OBJECTIVE, CashRegister);
	MAP::SET_BLIP_SPRITE(CashRegisterBlip, MISC::GET_HASH_KEY("blip_ambient_loan_shark"), true);
	MAP::_SET_BLIP_NAME_FROM_PLAYER_STRING(CashRegisterBlip, ObjectivesList[1].c_str());

	CompanionsDisband();

	SetCompanionPlayerCollision(false);

	for (int i = 0; i < 2; ++i)
	{
		Ped gangster = SpawnedGangMembers[i].member;
		Vector3 pos = CurrentShop.GangPositions[i];

		if (!PED::IS_PED_DEAD_OR_DYING(gangster, true))
		{
			WEAPON::SET_CURRENT_PED_WEAPON(gangster, WEAPON::GET_BEST_PED_WEAPON(gangster, true, true), true, WEAPON_ATTACH_POINT_HAND_PRIMARY, false, false);
			
			int sequence = 0;
			TASK::OPEN_SEQUENCE_TASK(&sequence);

			TASK::TASK_FOLLOW_NAV_MESH_TO_COORD(0, pos.x, pos.y, pos.z, 5.0f, -1, 0.5f, 0, 0);
			TASK::TASK_PAUSE(0, 50);
			TASK::TASK_AIM_GUN_AT_ENTITY(0, Shopkeeper, 12000, false, 0);

			TASK::CLOSE_SEQUENCE_TASK(sequence);
			TASK::TASK_PERFORM_SEQUENCE(gangster, sequence);
			TASK::CLEAR_SEQUENCE_TASK(&sequence);
		}
	}

	AbandonCoords = CurrentShop.RegisterPos;
	AbandonRadius = 15.0f;

	LootPrompt = HUD::_UIPROMPT_REGISTER_BEGIN();
	HUD::_UIPROMPT_SET_CONTROL_ACTION(LootPrompt, MISC::GET_HASH_KEY("INPUT_LOOT"));
	HUD::_UIPROMPT_SET_TEXT(LootPrompt, MISC::VAR_STRING(10, "LITERAL_STRING", "Loot Register"));
	HUD::_UIPROMPT_SET_STANDARDIZED_HOLD_MODE(LootPrompt, 1);
	HUD::_UIPROMPT_REGISTER_END(LootPrompt);
	HUD::_UIPROMPT_SET_ENABLED(LootPrompt, false);
	HUD::_UIPROMPT_SET_VISIBLE(LootPrompt, false);
}

void RobberyStore::ObjectiveLogic_1()
{
	if (ENTITY::DOES_ENTITY_EXIST(Shopkeeper) && PED::IS_PED_DEAD_OR_DYING(Shopkeeper, true))
	{
		PlayCompanionSpeech("GENERIC_CURSE_HIGH");
		CompanionsClearTasks();

		ENTITY::SET_ENTITY_AS_NO_LONGER_NEEDED(&Shopkeeper);

		Vector3 s = CurrentShop.ShopkeeperPos;
		PED::SET_PED_NON_CREATION_AREA(s.x - 5, s.y - 5, s.z - 5, s.x + 5, s.y + 5, s.z + 5);

		AllowWanted = true;
		BecomeWanted(CRIME_BURGLARY, 2);
		ActivePursuitParams.DispatchResetDelay = 3000;

		PED::CLEAR_PED_NON_CREATION_AREA();
	}
	else if (ENTITY::DOES_ENTITY_EXIST(Shopkeeper) && CurrentShop.ShopkeeperAggressive && 
			((TASK::GET_SEQUENCE_PROGRESS(SpawnedGangMembers[0].member) == 1 || TASK::GET_SEQUENCE_PROGRESS(SpawnedGangMembers[1].member) == 1)	||
			PLAYER::IS_PLAYER_FREE_AIMING_AT_ENTITY(0, Shopkeeper)))
	{
		PED::SET_PED_RELATIONSHIP_GROUP_HASH(Shopkeeper, REL_PLAYER_ENEMY);
		TASK::TASK_COMBAT_HATED_TARGETS(Shopkeeper, 30.0f);

		Vector3 s = CurrentShop.ShopkeeperPos;
		PED::SET_PED_NON_CREATION_AREA(s.x - 5, s.y - 5, s.z - 5, s.x + 5, s.y + 5, s.z + 5);

		WAIT(3000);

		AllowWanted = true;
		BecomeWanted(CRIME_BURGLARY, 2);
		ActivePursuitParams.DispatchResetDelay = 9000;

		PED::CLEAR_PED_NON_CREATION_AREA();

		CompanionsBeginCombat();
	}
	else if (ENTITY::DOES_ENTITY_EXIST(Shopkeeper) && (TASK::GET_SEQUENCE_PROGRESS(SpawnedGangMembers[0].member) == 1 || TASK::GET_SEQUENCE_PROGRESS(SpawnedGangMembers[1].member) == 1))
	{
		DioTrace("Shopkeeper reacting");
		
		PlayCompanionSpeech("ARRIVAL_SURRENDER_ROB_MALE");
		
		TASK::TASK_HANDS_UP(Shopkeeper, 9999, PlayerPed, 0, false);

		ENTITY::SET_ENTITY_AS_NO_LONGER_NEEDED(&Shopkeeper);
	}

	Vector3 slideCoord = ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(CashRegister, 0, -0.84f, 0);

	if (Common::GetDistBetweenCoords(PlayerCoords, slideCoord) < 0.3f && TASK::GET_SEQUENCE_PROGRESS(PlayerPed) < 0)
	{
		HUD::_UIPROMPT_SET_VISIBLE(LootPrompt, true);
		HUD::_UIPROMPT_SET_ENABLED(LootPrompt, true);
	}
	else
	{
		HUD::_UIPROMPT_SET_VISIBLE(LootPrompt, false);
		HUD::_UIPROMPT_SET_ENABLED(LootPrompt, false);
	}

	if (HUD::_UIPROMPT_HAS_HOLD_MODE_COMPLETED(LootPrompt))
	{
		HUD::_UIPROMPT_SET_VISIBLE(LootPrompt, false);
		HUD::_UIPROMPT_SET_ENABLED(LootPrompt, false);
		
		WEAPON::SET_CURRENT_PED_WEAPON(PlayerPed, WEAPON_UNARMED, true, WEAPON_ATTACH_POINT_HAND_PRIMARY, false, false);

		int sequence = 0;
		TASK::OPEN_SEQUENCE_TASK(&sequence);
		TASK::TASK_PED_SLIDE_TO_COORD(0, slideCoord.x, slideCoord.y, slideCoord.z, CurrentShop.RegisterRot, 0.0f);
		TASK::TASK_PLAY_ANIM(0, RegisterLootAnimDict.c_str(), RegisterLootAnimPed.c_str(), 1.0, 1.0f, -1, 0, 0, 0, 0, 0, 0, 0);
		TASK::CLOSE_SEQUENCE_TASK(sequence);
		TASK::TASK_PERFORM_SEQUENCE(PlayerPed, sequence);
		TASK::CLEAR_SEQUENCE_TASK(&sequence);
	}

	if (TASK::GET_SEQUENCE_PROGRESS(PlayerPed) > 0)
	{
		ENTITY::PLAY_ENTITY_ANIM(CashRegister, RegisterLootAnimObj.c_str(), RegisterLootAnimDict.c_str(), 0, false, true, false, 0, 0);
		
		RunObjective_1 = false;
		ActivateObjective_2 = true;
	}
}

void RobberyStore::ObjectiveInit_2()
{
	ObjectiveSet(2);
	ActivateObjective_2 = false;
	RunObjective_2 = true;

	CheckAbandon = false;

	MAP::REMOVE_BLIP(&CashRegisterBlip);

	if (!CompanionsInCombat)
	{
		CompanionsClearTasks();
		CompanionsRegroup();
	}

	AllowCompanionManualControl = true;

	Vector3 s = CurrentShop.ShopkeeperPos;
	PED::SET_PED_NON_CREATION_AREA(s.x - 5, s.y - 5, s.z - 5, s.x + 5, s.y + 5, s.z + 5);

	AllowWanted = true;
	SetDispatchService(true);
	BecomeWanted(CRIME_BURGLARY, 2);
	ActivePursuitParams.DispatchResetDelay = 1000;
	ActivePursuitParams.CompanionCombatOnSpotted = true;

	WAIT(3000);

	Common::ClearArea(CurrentShop.ShopkeeperPos, 20.0f);

	PED::CLEAR_PED_NON_CREATION_AREA();
}

void RobberyStore::ObjectiveLogic_2()
{
	if (!WantedLogic())
	{
		CurrentMusicStage = STAGE_AFTERACTION;
		UpdateMusic();

		RunObjective_2 = false;
		MissionSuccess();
	}
}

void RobberyStore::TrackRewards()
{
	RewardBounty_Active = RewardBounty_Potential;
	RewardMembers_Active = RewardMembers_Potential;
	
	RewardCash_Active = CurrentShop.RegisterAmount;
}

void RobberyStore::Update()
{
	for (Hash door : CurrentShop.DoorsToUnlock)
	{
		Common::SetDoorState(door, DOORSTATE_UNLOCKED);
	}
	
	if (TimeMissionFail > 0 && MISC::GET_GAME_TIMER() - TimeMissionFail > TimeMissionFailDelay)
	{
		DioTrace("Exit Fail Triggered");

		ExitMissionFlag = M_EXIT_FAILURE;
	}
	else if (TimeMissionSuccess > 0 && MISC::GET_GAME_TIMER() - TimeMissionSuccess > TimeMissionSuccessDelay)
	{
		DioTrace("Exit Win Triggered");

		ExitMissionFlag = M_EXIT_SUCCESS;
	}
	else if (ActivateObjective_0)
	{
		ObjectiveInit_0();
	}
	else if (ActivateObjective_1)
	{
		ObjectiveInit_1();
	}
	else if (ActivateObjective_2)
	{
		ObjectiveInit_2();
	}
	else if (RunObjective_0)
	{
		ObjectiveLogic_0();
	}
	else if (RunObjective_1)
	{
		ObjectiveLogic_1();
	}
	else if (RunObjective_2)
	{
		ObjectiveLogic_2();
	}
}
