
#include "precomp.h"
#include "JailbreakSisika.h"

const MissionStaticData& JailbreakSisika::GetStaticMissionData()
{
	static MissionStaticData missiondata = {
		MissionType::JAILBREAK_SISIKA,
		4000, 20, 5000, 0, 18,
		"Sisika Jailbreak",
		"The Sisika Penitentiary is an infamous island fortress.\nSneak inside and release the prisoners.",
		COMPCOUNT_MAX,
		false,
		MUSIC_SISIKA,
		false
	};
	return missiondata;
}

JailbreakSisika::JailbreakSisika()
{
	DioScope(DIOTAG);
}

JailbreakSisika::~JailbreakSisika()
{
	DioScope(DIOTAG);

	PLAYER::SET_PLAYER_CONTROL(0, true, 256, true);

	PAD::ENABLE_CONTROL_ACTION(0, INPUT_VEH_EXIT, true);

	SetAllowSprinting(true);

	if (AlarmPlaying)
	{
		SetAlarm(false);
	}

	if (PlayerDisguised)
	{
		const std::vector<std::string> outfits = {
			"META_OUTFIT_DEFAULT",
			"META_OUTFIT_WARM_WEATHER",
			"META_OUTFIT_GUNSLINGER",
			"META_OUTFIT_COOL_WEATHER"
		};
		
		PED::_SET_PED_OUTFIT_PRESET(PlayerPed, 0, true);
		
		for (std::string outfit : outfits)
		{
			if (PED::_DOES_METAPED_OUTFIT_EXIST_FOR_PED_MODEL(MISC::GET_HASH_KEY(outfit.c_str()), ENTITY::GET_ENTITY_MODEL(PlayerPed)))
			{
				PED::_SET_PED_BODY_COMPONENT(PlayerPed, MISC::GET_HASH_KEY(outfit.c_str()));
				break;
			}
		}

		PED::_UPDATE_PED_VARIATION(PlayerPed, true, true, true, true, true);
	}
}

void JailbreakSisika::Setup(int campid, const std::string& gangstr, const std::vector<MissionCompanionInfo>& companions)
{
	DioScope(DIOTAG "campid %d gangstr %s", campid, gangstr.c_str());

	GangName = gangstr;
	GangMemberTemplates = companions;

	if (!SetMissionParamsForCamp(campid))
	{
		ExitMissionFlag = M_EXIT_ERROR_TERMINATE;
		return;
	}

	CommonInit();
}

bool JailbreakSisika::SetMissionParamsForCamp(int camp)
{
	return true;
}

void JailbreakSisika::CommonInit()
{
	const MissionStaticData& thisMissionData = JailbreakSisika::GetStaticMissionData();
	SetVarsFromStaticData(thisMissionData);

	SetGenericLawModels(DISTRICT_BLUEGILL_MARSH);

	MissionAssetsToLoad = {
		{"PED", UniformGuardModel},
		{"PED", MercGuardModel},
		{"PED", PrisonerModel},
		{"PED", "MSP_GANG3_MALES_01"},
		{"PED", "PLAYER_ZERO"},
		{"PED", "RE_DEADJOHN_MALES_01"},
		{"SCN", "WORLD_HUMAN_GUARD_SCOUT"},
		{"SCN", "WORLD_HUMAN_GUARD_MILITARY"},
		{"SCN", "WORLD_HUMAN_FARMER_RAKE"},
		{"SCN", "WORLD_HUMAN_FARMER_WEEDING"},
		{"SCN", "WORLD_HUMAN_STERNGUY_IDLES"},
		{"SCN", "WORLD_HUMAN_SMOKE"},
		{"VEH", "rowboat"},
		{"VEH", "keelboat"},
		{"VEH", "BREACH_CANNON"},
		{"ANM", CannonDict},
		{"AUD", "GNG3_Sounds"}
	};

	AddHorseToMissionAssets(HORSE_MUSTANG);

	ObjectivesList = {
		"PLAN A: Silently eliminate the ~COLOR_RED~lookout",
		"Take position in the ~COLOR_YELLOW~tree line",
		"Sneak through the field and knock out the ~COLOR_YELLOW~guard",
		"Approach the ~COLOR_YELLOW~bridge",
		"Cross the ~COLOR_YELLOW~front gate~COLOR_WHITE~ without entering water",
		"Escort the prisoners to your ~COLOR_YELLOW~getaway location~COLOR_WHITE~ without entering water",
		"PLAN B: Secure the ~COLOR_YELLOW~bridge",
		"Use the ~COLOR_YELLOW~cannon~COLOR_WHITE~ to open the gates",
		"Secure the ~COLOR_YELLOW~interior",
		"Regroup at the ~COLOR_YELLOW~getaway location",
		"Eliminate the ~COLOR_RED~reinforcements"
	};

	ScriptsKillList = {
		MISC::GET_HASH_KEY("sisikapenitentiary"),
		MISC::GET_HASH_KEY("region_law_patrol_creator")
	};

	LoadAllMissionAssets();
	SpawnGangMembers(thisMissionData.NumMembersToSpawn, true);

	WAIT(2000);
}

void JailbreakSisika::ClearIsland()
{
	Common::ClearArea(IslandCenter, IslandRadius);
}

void JailbreakSisika::ApplyGuardHat(Ped ped)
{
	PED::_SET_META_PED_TAG(ped,
		MISC::GET_HASH_KEY("HAT_MR1_026"),
		MISC::GET_HASH_KEY("HAT_MR1_026_C1_999_AB"),
		MISC::GET_HASH_KEY("HAT_MR1_026_C1_000_NM"),
		0x542FCD2F,
		MISC::GET_HASH_KEY("METAPED_TINT_HAT_WEATHERED"),
		142, 21, 21);

	PED::_UPDATE_PED_VARIATION(ped, true, true, true, true, true);
}

void JailbreakSisika::ApplyGuardUniformRemoved(Ped guard)
{
	PED::REMOVE_TAG_FROM_META_PED(guard, TAG_HATS, 1);
	PED::REMOVE_TAG_FROM_META_PED(guard, TAG_COATS, 1);
	PED::REMOVE_TAG_FROM_META_PED(guard, TAG_BADGES, 1);
	PED::REMOVE_TAG_FROM_META_PED(guard, TAG_SHIRTS, 1);
	PED::REMOVE_TAG_FROM_META_PED(guard, TAG_TORSOS, 1);
	PED::REMOVE_TAG_FROM_META_PED(guard, TAG_PANTS, 1);
	PED::REMOVE_TAG_FROM_META_PED(guard, TAG_BOOTS, 1);

	PED::_SET_META_PED_TAG(guard,
		MISC::GET_HASH_KEY("UPPERTORSO_MR1_016"),
		MISC::GET_HASH_KEY("HAND_MR1_000_C0_008_AB"),
		MISC::GET_HASH_KEY("HAND_MR1_000_C0_000_NM"),
		0x6B504BEC,
		MISC::GET_HASH_KEY("METAPED_TINT_GENERIC"),
		0, 72, 73);

	PED::_SET_META_PED_TAG(guard,
		MISC::GET_HASH_KEY("LOWERTORSO_MR1_005"),
		MISC::GET_HASH_KEY("FEET_MR1_000_C0_008_AB"),
		MISC::GET_HASH_KEY("FEET_MR1_000_C0_000_NM"),
		MISC::GET_HASH_KEY("FEET_MR1_000_C0_000_M"),
		MISC::GET_HASH_KEY("METAPED_TINT_GENERIC"),
		0, 72, 73);

	PED::_SET_META_PED_TAG(guard,
		MISC::GET_HASH_KEY("UNIONSUITBUTTONED_MR1_000"),
		MISC::GET_HASH_KEY("UNIONSUITBUTTONED_MR1_000_C1_999_AB"),
		MISC::GET_HASH_KEY("UNIONSUITBUTTONED_MR1_000_C1_999_NM"),
		0x5DCD6700,
		MISC::GET_HASH_KEY("METAPED_TINT_GENERIC"),
		15, 15, 15);

	PED::_UPDATE_PED_VARIATION(guard, true, true, true, true, true);
}

void JailbreakSisika::ApplyGuardUniformToPlayer()
{
	const Hash guardOutfit = 0xF1344FDA;

	const Hash playerModel = ENTITY::GET_ENTITY_MODEL(PlayerPed);

	PED::_0xE3144B932DFDFF65(PlayerPed, 0, -1, true, true);
	PED::CLEAR_PED_DAMAGE_DECAL_BY_ZONE(PlayerPed, 10, "ALL");
	PED::CLEAR_PED_ENV_DIRT(PlayerPed);
	PED::CLEAR_PED_BLOOD_DAMAGE(PlayerPed);
	
	if (playerModel == MISC::GET_HASH_KEY("PLAYER_ZERO"))
	{
		DioTrace(DIOTAG "Player is Arthur, applying meta outfit directly");
		
		PED::_SET_PED_BODY_COMPONENT(PlayerPed, guardOutfit);
	}
	else if (playerModel == MISC::GET_HASH_KEY("PLAYER_THREE"))
	{
		DioTrace(DIOTAG "Player is John, applying meta outfit components");

		PED::_SET_PED_BODY_COMPONENT(PlayerPed, 0x93A4B224);
		PED::REMOVE_TAG_FROM_META_PED(PlayerPed, TAG_TORSOS, 1);
		PED::REMOVE_TAG_FROM_META_PED(PlayerPed, TAG_LEGS, 1);

		PED::_SET_META_PED_TAG(PlayerPed,
			MISC::GET_HASH_KEY("PLAYER_ZERO_NUDE_UPPER_000_LONG_SLEEVE"),
			MISC::GET_HASH_KEY("PLAYER_THREE_MS1_NUDE_UPPER_000_C0_000_AB"),
			MISC::GET_HASH_KEY("PLAYER_THREE_MS1_NUDE_UPPER_000_C0_000_NM"),
			MISC::GET_HASH_KEY("PLAYER_THREE_MS1_NUDE_UPPER_000_C0_000_M"),
			0, 0, 0, 0);

		PED::_SET_META_PED_TAG(PlayerPed,
			MISC::GET_HASH_KEY("PLAYER_ZERO_COAT_018"),
			MISC::GET_HASH_KEY("PLAYER_ZERO_COAT_018_C0_000_AB"),
			MISC::GET_HASH_KEY("PLAYER_ZERO_COAT_018_C0_000_NM"),
			0x8B1DBA84,
			0, 0, 0, 0);

		PED::_SET_META_PED_TAG(PlayerPed,
			0x6AEC5960,
			MISC::GET_HASH_KEY("PLAYER_ZERO_PANTS_014_C0_000_AB"),
			MISC::GET_HASH_KEY("PLAYER_ZERO_PANTS_014_C0_000_NM"),
			0xAE07A9F8,
			0, 0, 0, 0);

		PED::_SET_META_PED_TAG(PlayerPed,
			MISC::GET_HASH_KEY("PLAYER_ZERO_BOOT_011"),
			MISC::GET_HASH_KEY("PLAYER_ZERO_BOOT_011_C0_000_AB"),
			MISC::GET_HASH_KEY("PLAYER_ZERO_BOOT_011_C0_000_NM"),
			0x3A49E328,
			0, 0, 0, 0);

		PED::_SET_META_PED_TAG(PlayerPed,
			0x0DC4B2CC,
			MISC::GET_HASH_KEY("PLAYER_ZERO_HOLSTER_BELT_002_C0_000_AB"),
			MISC::GET_HASH_KEY("PLAYER_ZERO_HOLSTER_BELT_002_C0_000_NM"),
			0x7A3B57D0,
			0, 0, 0, 0);

		PED::_SET_META_PED_TAG(PlayerPed,
			0xDD626A7B,
			MISC::GET_HASH_KEY("PLAYER_ZERO_HOLSTER_CUP_020_C0_000_AB"),
			MISC::GET_HASH_KEY("PLAYER_ZERO_HOLSTER_CUP_020_C0_000_NM"),
			0xC357E09A,
			0, 0, 0, 0);

		PED::_SET_META_PED_TAG(PlayerPed,
			0x469BA060,
			MISC::GET_HASH_KEY("PLAYER_ZERO_BADGE_001_C0_000_AB"),
			MISC::GET_HASH_KEY("PLAYER_ZERO_BADGE_001_C0_000_NM"),
			0x2FDDBD2D,
			0, 0, 0, 0);
	}
	else
	{
		DioTrace(DIOTAG "Player is an NPC, applying NPC components");

		const int tagCount = 49;
		const Hash tags[tagCount] = {TAG_HATS, TAG_SHIRTS, TAG_VESTS, TAG_PANTS, TAG_SPURS, TAG_UNKNOWN_1, TAG_CHAPS, TAG_CLOAKS, TAG_MP_SKIRTS, TAG_GUNBELT_AMMUNITION,
			TAG_BADGES, TAG_GUNS, TAG_MASKS, TAG_MP_MASKS, TAG_SPATS, TAG_NECKWEAR, TAG_APRONS, TAG_BOOTS, TAG_ACCESSORIES, TAG_MP_RINGS_RIGHT_HAND, TAG_MP_RINGS_LEFT_HAND,
			TAG_MP_BRACELETS, TAG_MP_GAUNTLETS, TAG_NECKTIES, TAG_SHEATHS, TAG_UNKNOWN_2, TAG_LEGS, TAG_MP_LEG_ATTACHMENTS, TAG_LOADOUTS, TAG_SUSPENDERS, TAG_UNKNOWN_3,
			TAG_SATCHELS, TAG_GUNBELTS, TAG_BELTS, TAG_BELT_BUCKLE, TAG_TORSOS, TAG_OFFHAND_HOLSTERS, TAG_SIDEBURNS, TAG_PRIMARY_HOLSTERS, TAG_BANDOLIER_AMMUNITION,
			TAG_UNKNOWN_4, TAG_COATS, TAG_MP_COATS, TAG_MP_PONCHOS, TAG_MP_ARMOR, TAG_EYEWEAR, TAG_GLOVES, TAG_MUSTACHE, TAG_MP_MUSTACHE
		};

		for (int i = 0; i < tagCount; ++i)
		{
			PED::REMOVE_TAG_FROM_META_PED(PlayerPed, tags[i], 1);
		}

		PED::_SET_META_PED_TAG(PlayerPed,
			MISC::GET_HASH_KEY("POLICECOMBO_MR1_001"),
			MISC::GET_HASH_KEY("POLICECOMBO_MR1_001_C1_000_AB"),
			MISC::GET_HASH_KEY("POLICECOMBO_MR1_001_C1_000_NM"),
			0x208D4B1C,
			MISC::GET_HASH_KEY("METAPED_TINT_COMBINED_LEATHER"),
			0, 0, 0);

		PED::_SET_META_PED_TAG(PlayerPed,
			MISC::GET_HASH_KEY("UPPERTORSO_MR1_001"),
			MISC::GET_HASH_KEY("HAND_MR1_000_C0_008_AB"),
			MISC::GET_HASH_KEY("HAND_MR1_000_C0_000_NM"),
			0x6B504BEC,
			MISC::GET_HASH_KEY("METAPED_TINT_GENERIC"),
			0, 72, 73);
	}

	ApplyGuardHat(PlayerPed);
}

void JailbreakSisika::SetFrontGate(bool unlocked, bool forceOpened)
{
	const Hash door1 = 0x8DF24802;
	const Hash door2 = 0xBC3CA496;
	
	if (unlocked)
	{
		Common::SetDoorState(door1, DOORSTATE_UNLOCKED);
		Common::SetDoorState(door2, DOORSTATE_UNLOCKED);

		if (forceOpened)
		{
			OBJECT::DOOR_SYSTEM_SET_OPEN_RATIO(door1, -1.0f, true);
			OBJECT::DOOR_SYSTEM_SET_OPEN_RATIO(door2, -1.0f, true);
		}
	}
	else
	{
		Common::SetDoorState(door1, DOORSTATE_LOCKED_UNBREAKABLE);
		Common::SetDoorState(door2, DOORSTATE_LOCKED_UNBREAKABLE);
	}
}

void JailbreakSisika::StealthLogic()
{
	if (!StealthActive)
	{
		return;
	}

	bool playerShooting = PED::IS_PED_SHOOTING(PlayerPed) && !WEAPON::_IS_WEAPON_SILENT(WEAPON::_GET_PED_CURRENT_HELD_WEAPON(PlayerPed));
	bool guardShooting = false;
	bool gangShooting = false;

	for (Ped guard : SpawnedGuardsAllGlobal)
	{
		if (ENTITY::DOES_ENTITY_EXIST(guard) && PED::IS_PED_SHOOTING(guard))
		{
			guardShooting = true;
			break;
		}
	}

	for (int i = 0; i < SpawnedGangMembers.size(); i++)
	{
		if (PED::IS_PED_SHOOTING(SpawnedGangMembers[i].member))
		{
			gangShooting = true;
			break;
		}
	}

	if (FIRE::IS_EXPLOSION_IN_SPHERE(-1, IslandCenter, IslandRadius) || playerShooting || guardShooting || gangShooting)
	{
		DioTrace(DIOTAG "Generic gunfire detected");
		StealthBroken();
		return;
	}

	if (PlayerDisguised)
	{
		for (Ped guard : SpawnedGuardsAllGlobal)
		{
			if (ENTITY::DOES_ENTITY_EXIST(guard) && !PED::IS_PED_DEAD_OR_DYING(guard, true) && PED::IS_PED_IN_COMBAT(PlayerPed, guard))
			{
				DioTrace(DIOTAG "Player got in a fight with a guard");
				StealthBroken();
				return;
			}
		}

		SetAllowSprinting(Common::GetDistBetweenCoords(PlayerCoords, PrisonCenter) > PrisonRadius && !StealthEscorting);

		SetFrontGate(true);

		const float visionRange = 9.0f;
		const float confrontRange = 6.0f;
		
		const float patrolSpeed = 0.75f;
		
		for (int i = 0; i < ActiveSusGuards.size(); ++i)
		{
			const SusGuardData guard = ActiveSusGuards[i];

			if (!ENTITY::DOES_ENTITY_EXIST(guard.ped) || PED::IS_PED_DEAD_OR_DYING(guard.ped, true))
			{
				break;
			}

			const Vector3 guardPos = ENTITY::GET_ENTITY_COORDS(guard.ped, true, true);

#ifdef DEBUG_MARKERS
			Common::DrawDebugCircle(ENTITY::GET_ENTITY_COORDS(guard.ped, true, true), visionRange, 0.5f, "COLOR_YELLOW", true, true);
			Common::DrawDebugCircle(ENTITY::GET_ENTITY_COORDS(guard.ped, true, true), confrontRange, 0.5f, "COLOR_RED", true, true);
#endif
			PED::SET_PED_SEEING_RANGE(guard.ped, visionRange);

			if (guard.state == SusState::Patrolling)
			{
				if (guard.patrolPath.empty())
				{
					DioTrace(DIOTAG1("ERROR") "GUARD IN PATROL STATE WITHOUT PATROL PATH");
					ActiveSusGuards[i].state = SusState::Idle;
					break;
				}

#ifdef DEBUG_MARKERS
				Hash progress = 0;
				switch (guard.patrolProgress)
				{
					case 0: progress = MARKERTYPE_NUM_0; break;
					case 1: progress = MARKERTYPE_NUM_1; break;
					case 2: progress = MARKERTYPE_NUM_2; break;
					case 3: progress = MARKERTYPE_NUM_3; break;
					case 4: progress = MARKERTYPE_NUM_4; break;
					case 5: progress = MARKERTYPE_NUM_5; break;
					case 6: progress = MARKERTYPE_NUM_6; break;
					case 7: progress = MARKERTYPE_NUM_7; break;
					case 8: progress = MARKERTYPE_NUM_8; break;
					case 9: progress = MARKERTYPE_NUM_9; break;
				}
				GRAPHICS::_DRAW_MARKER(progress, {guardPos.x, guardPos.y, guardPos.z + 2}, {0,0,0}, {0,0,0}, {0.3f, 0.3f, 0.3f}, 255, 255, 255, 255, false, true, 0, false, 0, 0, false);
#endif
				if (guard.patrolProgress == -1)
				{
					DIOTRACE_EXTRA("Starting new patrol");
					TASK::CLEAR_PED_TASKS(guard.ped, true, true);
					TASK::TASK_GO_STRAIGHT_TO_COORD(guard.ped, guard.patrolPath[0], patrolSpeed, -1, 40000.0f, 0.5f, 0);
					ActiveSusGuards[i].patrolProgress = 0;
				}
				else if (ENTITY::IS_ENTITY_AT_COORD(guard.ped, guard.patrolPath[guard.patrolProgress], {2, 2, 2}, false, true, 0))
				{
					DIOTRACE_EXTRA("Reached patrol point %d", ActiveSusGuards[i].patrolProgress);

					if (guard.patrolProgress + 1 >= (int)guard.patrolPath.size())
					{
						ActiveSusGuards[i].patrolProgress = 0;
						DIOTRACE_EXTRA("Finished patrol loop, restarting at patrol point %d", ActiveSusGuards[i].patrolProgress);
					}
					else
					{
						ActiveSusGuards[i].patrolProgress++;
						DIOTRACE_EXTRA("Moving to patrol point %d", ActiveSusGuards[i].patrolProgress);
					}

					int sequence;
					TASK::OPEN_SEQUENCE_TASK(&sequence);
					
					TASK::TASK_STAND_STILL(0, 3000);
					TASK::TASK_GO_STRAIGHT_TO_COORD(0, guard.patrolPath[ActiveSusGuards[i].patrolProgress], patrolSpeed, -1, 40000.0f, 0.5f, 0);

					TASK::CLOSE_SEQUENCE_TASK(sequence);
					TASK::TASK_PERFORM_SEQUENCE(guard.ped, sequence);
					TASK::CLEAR_SEQUENCE_TASK(&sequence);
				}
			}

			bool seePlayer = Common::GetDistBetweenCoords(PlayerCoords, guardPos) < visionRange &&
				(PED::CAN_PED_SEE_ENTITY(guard.ped, PlayerPed, true, true) == 1 || PED::IS_PED_HEADTRACKING_PED(guard.ped, PlayerPed));

			if (seePlayer && !PED::IS_PED_FACING_PED(guard.ped, PlayerPed, 90))
			{
				TASK::TASK_CLEAR_LOOK_AT(guard.ped);
				seePlayer = false;
			}

			if (Common::GetDistBetweenCoords(PlayerCoords, guardPos) < visionRange * 1.75f)
			{
				float px = 0.0f;
				float py = 0.0f;

				float sx = 0.035f;
				float sy = 0.047f;

				GRAPHICS::GET_SCREEN_COORD_FROM_WORLD_COORD(guardPos.x, guardPos.y, guardPos.z + 1.2f, &px, &py);

				std::string color = seePlayer ? "COLOR_YELLOW" : "COLOR_WHITE";

				if (guard.state == SusState::Confronting)
				{
					color = "COLOR_RED";
				}

				int iVar0, iVar1, iVar2, bVar3;
				HUD::_GET_COLOR_FROM_NAME(MISC::GET_HASH_KEY(color.c_str()), &iVar0, &iVar1, &iVar2, &bVar3);

				GRAPHICS::DRAW_SPRITE("blips", "blip_ambient_eyewitness", px, py, sx, sy, 0, iVar0, iVar1, iVar2, bVar3, true);
			}

			if (seePlayer)
			{
				TASK::TASK_LOOK_AT_ENTITY(guard.ped, PlayerPed, 100, 2096, 31, 0);

				if (guard.stateTimer == 0)
				{
					DIOTRACE_EXTRA("Player entered vision");
					ActiveSusGuards[i].stateTimer = MISC::GET_GAME_TIMER();
				}
				else if (!ENTITY::DOES_ENTITY_EXIST(ConfrontingGuard) && guard.state != SusState::Confronting && MISC::GET_GAME_TIMER() - guard.stateTimer > 2000 &&
					Common::GetDistBetweenCoords(PlayerCoords, ENTITY::GET_ENTITY_COORDS(guard.ped, true, true)) <= confrontRange)
				{
					DIOTRACE_EXTRA("Confronting player");

					ScriptedSpeechParams params{"LAW_INTERACT_HIGH_SUSPICION", 0, 0, MISC::GET_HASH_KEY("SPEECH_PARAMS_BEAT_SHOUTED_CLEAR_NOSUB"), 0, true, 0, 0};
					AUDIO::PLAY_PED_AMBIENT_SPEECH_NATIVE(guard.ped, (Any*)&params);

					ActiveSusGuards[i].stateTimer = MISC::GET_GAME_TIMER();
					ActiveSusGuards[i].state = SusState::Confronting;
					ConfrontingGuard = guard.ped;

					TASK::CLEAR_PED_TASKS(guard.ped, true, true);
					TASK::TASK_TURN_PED_TO_FACE_ENTITY(guard.ped, PlayerPed, -1, 0, 0, 0);
					TASK::TASK_PLAY_EMOTE_WITH_HASH(guard.ped, EMOTE_TYPE_ACTION, EMOTE_PM_UPPERBODY, KIT_EMOTE_ACTION_POINT_1, true, false, true, false, true);

					if (guard.patrolProgress == -1)
					{
						MAP::BLIP_ADD_MODIFIER(MAP::GET_BLIP_FROM_ENTITY(guard.ped), BLIP_MODIFIER_WITNESS_UNIDENTIFIED);
					}
				}
				else if (guard.state == SusState::Confronting && MISC::GET_GAME_TIMER() - guard.stateTimer > 7000)
				{
					DioTrace(DIOTAG "Player was detected by a suspicious guard");

					ScriptedSpeechParams params{"GENERIC_SHOCKED_HIGH", 0, 0, MISC::GET_HASH_KEY("SPEECH_PARAMS_BEAT_SHOUTED_CLEAR_NOSUB"), 0, true, 0, 0};
					AUDIO::PLAY_PED_AMBIENT_SPEECH_NATIVE(guard.ped, (Any*)&params);

					AUDIO::PLAY_SOUND_FROM_ENTITY("POLICE_WHISTLE_MULTI", guard.ped, "GNG3_Sounds", false, 0, 0);

					StealthBroken();
					return;
				}
			}
			else if (!seePlayer && guard.stateTimer > 0)
			{
				DIOTRACE_EXTRA("Player left vision");
				
				ActiveSusGuards[i].stateTimer = 0;

				if (guard.state == SusState::Confronting)
				{
					if (guard.patrolProgress > -1)
					{
						ActiveSusGuards[i].state = SusState::Patrolling;

						DIOTRACE_EXTRA("Resuming patrol to point %d", guard.patrolProgress);
						TASK::TASK_GO_STRAIGHT_TO_COORD(guard.ped, guard.patrolPath[guard.patrolProgress], patrolSpeed, -1, 40000.0f, 0.5f, 0);
					}
					else
					{
						ActiveSusGuards[i].state = SusState::Idle;
					}

					ConfrontingGuard = 0;
				}
			}
		}
	}
}

void JailbreakSisika::StealthBroken()
{
	StealthActive = false;
	AlarmTimer = MISC::GET_GAME_TIMER();
	CurrentMusicStage = STAGE_ACTION1;
	
	MAP::REMOVE_BLIP(&ObjectiveBlip);

	SetAllowSprinting(true);

	CompanionsClearTasks();
	CompanionsRegroup();

	AllowCompanionManualControl = true;

	if (StealthPassed)
	{
		for (int i = 0; i < SpawnedGangMembers.size(); i++)
		{
			ENTITY::SET_ENTITY_INVINCIBLE(SpawnedGangMembers[i].member, true);
		}
	}

	int newObjective = 0;

	if (ObjectiveIndexStealth < 8)
	{
		DioTrace("Stealth blown before reaching bridge");
		newObjective = 0;

		SpawnLoudPeds = true;

		SetFrontGate(false);
	}
	else if (ObjectiveIndexStealth < 10)
	{
		DioTrace("Stealth blown before prisoners spawned");
		newObjective = 4;

		ObjectivesList[8] = Common::Format("PLAN B: %s", ObjectivesList[8].c_str());
	}
	else if (StealthEscorting)
	{
		DioTrace("Stealth blown but we have the prisoners");
		newObjective = 6;
	}

	DioTrace("Switching to Loud objective %d", newObjective);
	ObjectiveIndexStealth = -1;
	ObjectiveIndexLoud = newObjective;
	
	for (Ped guard : SpawnedGuardsAllGlobal)
	{
		if (ENTITY::DOES_ENTITY_EXIST(guard) && !PED::IS_PED_DEAD_OR_DYING(guard, true))
		{
			PED::SET_PED_RELATIONSHIP_GROUP_HASH(guard, REL_PLAYER_ENEMY);

			PED::SET_PED_SEEING_RANGE(guard, 60.0f);
			
			Blip b = MAP::GET_BLIP_FROM_ENTITY(guard);
			MAP::REMOVE_BLIP(&b);

			b = MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_COP, guard);
			MAP::BLIP_ADD_MODIFIER(b, BLIP_MODIFIER_ENEMY_IS_ALERTED);
			MAP::BLIP_REMOVE_MODIFIER(b, BLIP_MODIFIER_ENEMY_ON_GUARD);

			if (Common::GetDistBetweenCoords(ENTITY::GET_ENTITY_COORDS(guard, true, true), PrisonCenter) < PrisonRadius)
			{
				PED::SET_PED_SPHERE_DEFENSIVE_AREA(guard, PrisonCenter, PrisonRadius, 0, false, 0);

				PED::SET_PED_COMBAT_ATTRIBUTES(guard, CA_PERMIT_CHARGE_BEYOND_DEFENSIVE_AREA, false);
				PED::SET_PED_COMBAT_ATTRIBUTES(guard, CA_CAN_CHARGE, false);
				PED::SET_PED_COMBAT_ATTRIBUTES(guard, CA_CAN_CHASE_TARGET_ON_FOOT, false);
				PED::SET_PED_COMBAT_ATTRIBUTES(guard, CA_USE_COVER, true);
				PED::SET_PED_COMBAT_ATTRIBUTES(guard, CA_SWITCH_TO_ADVANCE_IF_CANT_FIND_COVER, false);
				PED::SET_PED_COMBAT_ATTRIBUTES(guard, CA_DISABLE_RETREAT_DUE_TO_TARGET_PROXIMITY, true);
			}

			TASK::CLEAR_PED_TASKS(guard, true, true);
			TASK::TASK_COMBAT_PED(guard, PlayerPed, 0, 0);
		}
	}

	for (Ped prisoner : SpawnedPrisonersOutside)
	{
		if (ENTITY::DOES_ENTITY_EXIST(prisoner) && !PED::IS_PED_DEAD_OR_DYING(prisoner, true))
		{
			EVENT::SET_DECISION_MAKER(prisoner, MISC::GET_HASH_KEY("EMPTY"));
			
			TASK::CLEAR_PED_TASKS(prisoner, true, true);
			TASK::TASK_FOLLOW_NAV_MESH_TO_COORD(prisoner, Common::ApplyRandomSmallOffsetToCoords(RegroupCoords, 2.0f), 20.0f, -1, 0.5f, 0, 250);
		}
	}

	for (Ped prisoner : SpawnedPrisonersInside)
	{
		if (ENTITY::DOES_ENTITY_EXIST(prisoner) && !PED::IS_PED_DEAD_OR_DYING(prisoner, true))
		{
			TASK::CLEAR_PED_TASKS(prisoner, true, true);
			TASK::TASK_COWER(prisoner, 9000, PlayerPed, "");

			Blip b = MAP::GET_BLIP_FROM_ENTITY(prisoner);
			MAP::REMOVE_BLIP(&b);
		}
	}
}

void JailbreakSisika::SetAlarm(bool on)
{
	DioTrace(DIOTAG "%s", Common::BoolToStr(on).c_str());

	if (on)
	{
		AUDIO::SET_AMBIENT_ZONE_STATE("AZ_sisika_penitentiary_alarms", true, true);
		AUDIO::_PLAY_SOUND_FROM_POSITION("alarm_bell", 3237.66f, -591.83f, 49.26f, "GNG3_Sounds", false, 0, true, 0);
	}
	else
	{
		AUDIO::SET_AMBIENT_ZONE_STATE("AZ_sisika_penitentiary_alarms", false, true);
		AUDIO::_STOP_SOUND_WITH_NAME("alarm_bell", "GNG3_Sounds");
	}

	AlarmPlaying = on;
}

void JailbreakSisika::SetAllowSprinting(bool allow)
{
#ifdef DEBUG_FAST_MISSION
	allow = true;
#endif
	
	if (!allow)
	{
		PED::_SET_PED_CROUCH_MOVEMENT(PlayerPed, false, 0, false);
		PAD::DISABLE_CONTROL_ACTION(0, INPUT_SPRINT, true);
		PAD::DISABLE_CONTROL_ACTION(0, INPUT_DUCK, true);
	}
	else
	{
		PAD::ENABLE_CONTROL_ACTION(0, INPUT_SPRINT, true);
		PAD::ENABLE_CONTROL_ACTION(0, INPUT_DUCK, true);
	}
}

Ped JailbreakSisika::SpawnGuard(const Vector3& pos, float rot, PedType weapons, bool sus)
{
	Ped guard = SpawnPed(sus ? UniformGuardModel : MercGuardModel, pos, rot, 255);

	SetPedType(guard, weapons);

	TASK::TASK_STAND_GUARD(guard, pos, rot, weapons < PedType::LONGARMS_MINIMUM ? "WORLD_HUMAN_STERNGUY_IDLES" : "WORLD_HUMAN_GUARD_MILITARY");
	
	SpawnedGuardsAllGlobal.emplace_back(guard);

	if (sus)
	{
		SpawnedGuardsSus.emplace_back(guard);

		if (StealthActive)
		{
			const std::vector<std::string> guardVoices = {
				"0987_S_M_M_DISPATCHLAWRURAL_WHITE_02",
				"0989_S_M_M_DISPATCHLAWRURAL_WHITE_04",
				"0991_S_M_M_DISPATCHLAWRURAL_WHITE_06",
				"0993_S_M_M_DISPATCHLAWRURAL_WHITE_08",
				"0994_S_M_M_DISPATCHLAWRURAL_WHITE_09"
			};

			AUDIO::SET_AMBIENT_VOICE_NAME(guard, guardVoices[rand() % guardVoices.size()].c_str());

			PED::SET_PED_CONFIG_FLAG(guard, PCF_DisableTalkTo, true);

			PED::SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(guard, false);

			Blip b = MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_COP, guard);
			MAP::BLIP_ADD_MODIFIER(b, BLIP_MODIFIER_ENEMY_ON_GUARD);
			MAP::BLIP_ADD_MODIFIER(b, BLIP_MODIFIER_WITNESS_INVESTIGATING);
		}
	}
	else
	{
		SpawnedGuardsNeutral.emplace_back(guard);
	}

	return guard;
}

Ped JailbreakSisika::SpawnReinforcement(const Vector3& pos, float rot, PedType weapons, bool defensive, bool merc, bool mounted)
{
	Ped guard = SpawnPed(merc ? MercGuardModel : UniformGuardModel, pos, rot, 255);

	SetPedType(guard, weapons);

	PED::SET_PED_RELATIONSHIP_GROUP_HASH(guard, REL_PLAYER_ENEMY);

	if (defensive)
	{
		PED::SET_PED_SPHERE_DEFENSIVE_AREA(guard, ENTITY::GET_ENTITY_COORDS(guard, true, true), 10.0f, 0, false, 0);

		PED::SET_PED_COMBAT_ATTRIBUTES(guard, CA_PERMIT_CHARGE_BEYOND_DEFENSIVE_AREA, false);
		PED::SET_PED_COMBAT_ATTRIBUTES(guard, CA_CAN_CHARGE, false);
		PED::SET_PED_COMBAT_ATTRIBUTES(guard, CA_CAN_CHASE_TARGET_ON_FOOT, false);
		PED::SET_PED_COMBAT_ATTRIBUTES(guard, CA_USE_COVER, true);
		PED::SET_PED_COMBAT_ATTRIBUTES(guard, CA_SWITCH_TO_ADVANCE_IF_CANT_FIND_COVER, false);
		PED::SET_PED_COMBAT_ATTRIBUTES(guard, CA_DISABLE_RETREAT_DUE_TO_TARGET_PROXIMITY, true);
	}
	else if (mounted)
	{
		std::vector<std::string> horseData = InventoryManager::GetStaticHorseData(HORSE_MUSTANG).PedModels;

		Ped horse = SpawnPed(horseData[rand() % horseData.size()], Common::ApplyRandomSmallOffsetToCoords(pos, 5.0f), rot, 255);

		DefaultHorseSetup(horse);
		PED::SET_PED_CONFIG_FLAG(horse, PCF_BlockMountHorsePrompt, true);

		Horses.emplace_back(horse);

		PED::SET_PED_ONTO_MOUNT(guard, horse, -1, true);
	}
	
	MAP::BLIP_ADD_MODIFIER(MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_COP, guard), BLIP_MODIFIER_ENEMY_IS_ALERTED);

	TASK::CLEAR_PED_TASKS(guard, true, true);
	TASK::TASK_COMBAT_PED(guard, PlayerPed, 0, 0);

	SpawnedGuardsAllGlobal.emplace_back(guard);

	return guard;
}

void JailbreakSisika::SpawnOutsidePeds()
{
	DioScope(DIOTAG);

	WatchtowerGuard = SpawnPed(MercGuardModel.c_str(), {3150.23f, -494.03f, 48.19f}, 216, 50);

	SetPedType(WatchtowerGuard, PedType::SNIPER_EXPERT);

	PED::SET_PED_COMBAT_MOVEMENT(WatchtowerGuard, 0);
	PED::SET_PED_RELATIONSHIP_GROUP_HASH(WatchtowerGuard, REL_PLAYER_ENEMY);
	MAP::BLIP_ADD_MODIFIER(MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_COP, WatchtowerGuard), BLIP_MODIFIER_ENEMY_ON_GUARD);

	TASK::TASK_STAND_GUARD(WatchtowerGuard, ENTITY::GET_ENTITY_COORDS(WatchtowerGuard, true, true), ENTITY::GET_ENTITY_HEADING(WatchtowerGuard), "WORLD_HUMAN_GUARD_SCOUT");

	SpawnedGuardsAllGlobal.emplace_back(WatchtowerGuard);
	
	const std::vector<LocationData> extraGuards = {
		{{3189.15f, -613.38f, 41.26f}, 92},
		{{3182.02f, -611.21f, 41.14f}, 304},
		{{3216.71f, -546.10f, 41.90f}, 241},
		{{3239.07f, -516.43f, 41.79f}, 216},
		{{3249.94f, -586.83f, 42.04f}, 60},
		{{3237.53f, -591.39f, 41.88f}, 331},
		{{3263.49f, -514.17f, 41.80f}, 134},
		{{3231.55f, -568.07f, 41.69f}, 293}
	};

	for (const LocationData& spawn : extraGuards)
	{
		Ped guard = SpawnPed(MercGuardModel.c_str(), spawn.p, spawn.h, 255);

		SetPedType(guard, PedType::REPEATER_REGULAR);

		PED::SET_PED_RELATIONSHIP_GROUP_HASH(guard, REL_PLAYER_ENEMY);
		MAP::BLIP_ADD_MODIFIER(MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_COP, guard), BLIP_MODIFIER_ENEMY_ON_GUARD);

		PED::SET_PED_SEEING_RANGE(guard, 25.0f);

		TASK::TASK_STAND_GUARD(guard, ENTITY::GET_ENTITY_COORDS(guard, true, true), ENTITY::GET_ENTITY_HEADING(guard), "WORLD_HUMAN_GUARD_SCOUT");

		SpawnedGuardsNeutral.emplace_back(guard);
		SpawnedGuardsAllGlobal.emplace_back(guard);
	}

	const std::vector<LocationData> prisoners = {
		{{3234.47f, -566.19f, 41.78f}, 332},
		{{3235.99f, -567.06f, 41.79f}, 332},
		{{3237.46f, -567.87f, 41.83f}, 335}
	};

	for (const LocationData& spawn : prisoners)
	{
		Ped prisoner = SpawnPed(PrisonerModel.c_str(), spawn.p, spawn.h, 255);

		WEAPON::REMOVE_ALL_PED_WEAPONS(prisoner, true, true);

		PED::SET_PED_RELATIONSHIP_GROUP_HASH(prisoner, REL_CIV);

		Common::SetPedHonorModifier(prisoner, HONOR_NEGATIVE);

		TASK::TASK_START_SCENARIO_IN_PLACE_HASH(prisoner,
			rand() % 100 < 30 ? MISC::GET_HASH_KEY("WORLD_HUMAN_FARMER_RAKE") : MISC::GET_HASH_KEY("WORLD_HUMAN_FARMER_WEEDING"), -1, true, 0, spawn.h, true);

		PED::SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(prisoner);

		SpawnedPrisonersOutside.emplace_back(prisoner);
	}

	Milliken = SpawnPed("MSP_GANG3_MALES_01", {3234.65f, -575.60f, 41.75f}, 95, 0);
	ApplyGuardHat(Milliken);
	SetPedType(Milliken, PedType::SIDEARM_WEAK);
	PED::SET_PED_LASSO_HOGTIE_FLAG(Milliken, 0, false);
	PED::SET_PED_RELATIONSHIP_GROUP_HASH(Milliken, REL_PLAYER_ENEMY);
	PED::SET_PED_SEEING_RANGE(Milliken, 35.0f);
	TASK::TASK_STAND_GUARD(Milliken, ENTITY::GET_ENTITY_COORDS(Milliken, true, true), ENTITY::GET_ENTITY_HEADING(Milliken), "");

	SpawnedGuardsAllGlobal.emplace_back(Milliken);

	MillikenPartner = SpawnPed(UniformGuardModel.c_str(), {3235.59f, -572.42f, 41.69f}, 124, 7);
	SetPedType(MillikenPartner, PedType::REPEATER_EXPERT);
	PED::SET_PED_RELATIONSHIP_GROUP_HASH(MillikenPartner, REL_PLAYER_ENEMY);
	MAP::BLIP_ADD_MODIFIER(MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_COP, MillikenPartner), BLIP_MODIFIER_ENEMY_ON_GUARD);
	PED::SET_PED_SEEING_RANGE(MillikenPartner, 35.0f);
	TASK::TASK_STAND_GUARD(MillikenPartner, ENTITY::GET_ENTITY_COORDS(MillikenPartner, true, true), ENTITY::GET_ENTITY_HEADING(MillikenPartner), "");

	SpawnedGuardsSus.emplace_back(MillikenPartner);
	SpawnedGuardsAllGlobal.emplace_back(MillikenPartner);
	
	// Bridge Sentries
	ActiveSusGuards.push_back({MillikenPartner});
	ActiveSusGuards.push_back({SpawnGuard({3313.30f, -591.20f, 41.88f}, 123, PedType::REPEATER_EXPERT, true), SusState::Patrolling, {{3313.30f, -591.20f, 41.88f}, {3303.19f, -599.88f, 41.97f}}});
	SpawnGuard({3317.91f, -601.03f, 43.10f}, 100, PedType::SIDEARM_REGULAR, false);
	SpawnGuard({3312.00f, -600.37f, 42.24f}, 320, PedType::REPEATER_REGULAR, false);

	// Cannon Guards
	ActiveSusGuards.push_back({SpawnGuard({3330.67f, -629.81f, 42.62f}, 38, PedType::REPEATER_WEAK, true)});
	SpawnGuard({3334.01f, -626.90f, 42.65f}, 19, PedType::REPEATER_WEAK, false);

	// Gate Guard
	GateGuard = SpawnGuard({3351.71f, -643.72f, 44.29f}, 22, PedType::REPEATER_REGULAR, false);

	Cannon = SpawnVehicle("breach_cannon", CannonPos, CannonRot + 51, true);
	ENTITY::SET_ENTITY_INVINCIBLE(Cannon, true);
	ENTITY::FREEZE_ENTITY_POSITION(Cannon, true);
	VEHICLE::SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(Cannon, false);
}

void JailbreakSisika::RemoveOutsidePeds()
{
	DioTrace(DIOTAG);

	for (int i = 0; i < SpawnedPrisonersOutside.size(); ++i)
	{
		ENTITY::DELETE_ENTITY(&SpawnedPrisonersOutside[i]);
	}
	SpawnedPrisonersOutside.clear();

	for (int i = 0; i < SpawnedGuardsAllGlobal.size(); ++i)
	{
		if (Common::GetDistBetweenCoords(ENTITY::GET_ENTITY_COORDS(SpawnedGuardsAllGlobal[i], true, true), PrisonCenter) > PrisonRadius)
		{
			for (int j = 0; j < ActiveSusGuards.size(); ++j)
			{
				if (ActiveSusGuards[j].ped == SpawnedGuardsAllGlobal[i])
				{
					ActiveSusGuards.erase(ActiveSusGuards.begin() + j);
					break;
				}
			}
			
			ENTITY::DELETE_ENTITY(&SpawnedGuardsAllGlobal[i]);
		}
	}
}

void JailbreakSisika::SpawnInsidePeds()
{
	DioScope(DIOTAG);

	CourtyardGuard = SpawnGuard({3334.54f, -695.73f, 43.00f}, 305, PedType::SIDEARM_REGULAR, false);

	Warden = SpawnGuard({3339.19f, -669.74f, 44.79f}, 199, PedType::SIDEARM_REGULAR, true);
	ActiveSusGuards.push_back({Warden});

	// Sus Guards
	ActiveSusGuards.push_back({SpawnGuard({3330.79f, -657.03f, 44.67f}, 298, PedType::REPEATER_WEAK, true)});
	ActiveSusGuards.push_back({SpawnGuard({3354.69f, -709.49f, 44.02f}, 330, PedType::REPEATER_WEAK, true)});
	ActiveSusGuards.push_back({SpawnGuard({3352.33f, -661.70f, 44.53f}, 286, PedType::REPEATER_WEAK, true)});
	ActiveSusGuards.push_back({SpawnGuard({3377.86f, -661.96f, 45.81f}, 122, PedType::SIDEARM_WEAK, true)});
	ActiveSusGuards.push_back({SpawnGuard({3354.23f, -664.67f, 44.46f}, 351, PedType::SIDEARM_WEAK, true)});
	ActiveSusGuards.push_back({SpawnGuard({3369.31f, -711.78f, 44.11f}, 307, PedType::SIDEARM_WEAK, true)});
	ActiveSusGuards.push_back({SpawnGuard({3348.30f, -683.73f, 43.09f}, 293, PedType::SIDEARM_WEAK, true)});

	ActiveSusGuards.push_back({
		SpawnGuard({3360.75f, -666.26f, 44.91f}, 286, PedType::SIDEARM_WEAK, true), SusState::Patrolling, 
		{{3360.75f, -666.26f, 44.91f}, {3341.10f, -700.07f, 43.03f}, {3328.33f, -688.24f, 43.03f}, {3324.65f, -675.83f, 44.58f}}
	});

	// Neutral Guards
	SpawnGuard({3363.89f, -667.08f, 45.25f}, 102, PedType::REPEATER_WEAK, false);
	SpawnGuard({3333.52f, -700.19f, 43.02f}, 315, PedType::REPEATER_WEAK, false);
	SpawnGuard({3375.16f, -679.74f, 45.44f}, 91, PedType::SIDEARM_WEAK, false);
	SpawnGuard({3372.23f, -681.47f, 45.36f}, 359, PedType::SIDEARM_WEAK, false);

	// Wall and Tower Guards
	SpawnGuard({3337.59f, -648.91f, 50.85f}, 19, PedType::SNIPER_WEAK, true);
	SpawnGuard({3375.90f, -638.02f, 50.85f}, 17, PedType::SNIPER_WEAK, true);
	SpawnGuard({3319.91f, -675.06f, 50.85f}, 110, PedType::SNIPER_WEAK, true);
	SpawnGuard({3361.04f, -679.05f, 50.86f}, 58, PedType::RIFLE_WEAK, true);
}

void JailbreakSisika::Objective0_Stealth_Sentry_Init()
{
	ObjectiveSet(0);

	ENTITY::_SET_ENTITY_COORDS_AND_HEADING(PlayerPed, {3108.86f, -493.71f, 40.29f}, 274, true, true, true);
	CAM::SET_GAMEPLAY_CAM_RELATIVE_HEADING(0, 1);
	CAM::SET_GAMEPLAY_CAM_RELATIVE_PITCH(0, 1);

	DisableLaw();

	GenericTimer = MISC::GET_GAME_TIMER();

	SpawnOutsidePeds();

	SetFrontGate(false);

	const std::vector<Vector4> boatSpawns = {
		{3098.42f, -500.00f, 40.58f, 286.8f},
		{3099.21f, -494.06f, 40.58f, 244.9f},
		{3100.96f, -490.61f, 40.58f, 260.6f},
		{3103.47f, -486.85f, 40.76f, 246.9f},
		{3105.17f, -483.46f, 40.58f, 280.6f},
		{3105.23f, -479.96f, 40.58f, 298.7f}
	};

	for (const Vector4& spawn : boatSpawns)
	{
		Vehicle boat = SpawnVehicle("ROWBOAT", {spawn.x, spawn.y, spawn.z + 0.5f}, spawn.w, true);

		ENTITY::SET_ENTITY_INVINCIBLE(boat, true);
		ENTITY::FREEZE_ENTITY_POSITION(boat, true);
		ENTITY::_SET_ENTITY_COORDS_AND_HEADING_NO_OFFSET(boat, spawn.x, spawn.y, spawn.z, spawn.w, true, true);
		VEHICLE::SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(0, false);

		GangBoats.emplace_back(boat);
	}

	CheckAbandon = true;
	AbandonCoords = ENTITY::GET_ENTITY_COORDS(WatchtowerGuard, true, true);
	AbandonRadius = 45.0f;

#ifdef DEBUG_FAST_MISSION
	ENTITY::_SET_ENTITY_COORDS_AND_HEADING(WatchtowerGuard, {3141.48f, -488.75f, 41.39f}, 319, true, true, true);
	TASK::CLEAR_PED_TASKS_IMMEDIATELY(WatchtowerGuard, true, true);
#endif

	for (int i = 0; i < SpawnedGangMembers.size(); i++)
	{
		Ped gangster = SpawnedGangMembers[i].member;

		WEAPON::SET_CURRENT_PED_WEAPON(gangster, WEAPON::GET_BEST_PED_WEAPON(gangster, true, true), true, WEAPON_ATTACH_POINT_HAND_PRIMARY, false, false);

		TASK::TASK_SEEK_COVER_FROM_POS(gangster, ENTITY::GET_ENTITY_COORDS(WatchtowerGuard, true, true), -1, 0, 0, 0);
	}

	ObjectiveIndexStealth++;
}

void JailbreakSisika::Objective0_Stealth_Sentry_Run()
{
#ifdef DEBUG_FAST_MISSION
	PED::EXPLODE_PED_HEAD(WatchtowerGuard, WEAPON_BOW);
#endif
	
	if (GenericTimer != 0 && MISC::GET_GAME_TIMER() - GenericTimer > 500)
	{
		PlayCompanionSpeech("ARRIVAL_ENTER_TRAP");
		GenericTimer = 0;
	}

	if (PED::IS_PED_DEAD_OR_DYING(WatchtowerGuard, true))
	{
		for (int i = 0; i < SpawnedGuardsAllGlobal.size(); ++i)
		{
			if (SpawnedGuardsAllGlobal[i] == WatchtowerGuard)
			{
				SpawnedGuardsAllGlobal.erase(SpawnedGuardsAllGlobal.begin() + i);
				break;
			}
		}
		
		ObjectiveIndexStealth++;
	}
}

void JailbreakSisika::Objective1_Stealth_Treeline_Init()
{
	ObjectiveSet(1);

#ifdef DEBUG_FAST_MISSION
	ENTITY::SET_ENTITY_COORDS(PlayerPed, {3173.21f, -540.75f, 42.59f}, 231, true, true, false);
#endif

	ObjectiveBlip = ObjectiveBlipCoords({3195.14f, -552.81f, 41.71f}, 5.0f, true, ObjectivesList[1]);

	AbandonCoords = MAP::GET_BLIP_COORDS(ObjectiveBlip);
	AbandonRadius = 100.0f;

	CompanionsDisband();
	CompanionsClearTasks();

	for (int i = 0; i < SpawnedGangMembers.size(); ++i)
	{
		TASK::TASK_FOLLOW_NAV_MESH_TO_COORD(SpawnedGangMembers[i].member, Common::ApplyRandomSmallOffsetToCoords({3173.25f, -563.69f, 42.12f}, 5.0f), 10.0f, -1, 0.1f, 0, 250);
	}

	ObjectiveIndexStealth++;
}

void JailbreakSisika::Objective1_Stealth_Treeline_Run()
{
#ifdef DEBUG_MARKERS
	for (Ped guard : SpawnedGuardsNeutral)
	{
		Common::DrawDebugCircle(ENTITY::GET_ENTITY_COORDS(guard, true, true), 25.0f, 0.9f, "COLOR_BLACK", true, true);
	}

	Common::DrawDebugCircle(ENTITY::GET_ENTITY_COORDS(Milliken, true, true), 35.0f, 0.9f, "COLOR_BLUE", true, true);
	Common::DrawDebugCircle(ENTITY::GET_ENTITY_COORDS(MillikenPartner, true, true), 35.0f, 0.9f, "COLOR_BLUE", true, true);
#endif

	for (Ped guard : SpawnedGuardsAllGlobal)
	{
		if (ENTITY::DOES_ENTITY_EXIST(guard) && PED::IS_PED_DEAD_OR_DYING(guard, true))
		{
			StealthBroken();
			return;
		}
	}

	for (Ped prisoner : SpawnedPrisonersOutside)
	{
		if (ENTITY::DOES_ENTITY_EXIST(prisoner) && PED::IS_PED_DEAD_OR_DYING(prisoner, true))
		{
			StealthBroken();
			return;
		}
	}

	if (Common::GetDistBetweenCoords(PlayerCoords, MAP::GET_BLIP_COORDS(ObjectiveBlip)) < 5.0f)
	{
		MAP::REMOVE_BLIP(&ObjectiveBlip);

		for (int i = 0; i < SpawnedGangMembers.size(); ++i)
		{
			PED::_SET_PED_CROUCH_MOVEMENT(SpawnedGangMembers[i].member, true, 0, false);
			TASK::TASK_TURN_PED_TO_FACE_ENTITY(SpawnedGangMembers[i].member, MillikenPartner, 7000, 0, 0, 0);
		}

		ObjectiveIndexStealth++;
	}
}

void JailbreakSisika::Objective2_Stealth_Disguise_Init()
{
	ObjectiveSet(2);

	ObjectiveBlip = ObjectiveBlipEntity(Milliken, false, ObjectivesList[2]);

	AbandonCoords = ENTITY::GET_ENTITY_COORDS(Milliken, true, true);
	AbandonRadius = 75.0f;

#ifndef DEBUG_FAST_MISSION
	TASK::TASK_START_SCENARIO_AT_POSITION(Milliken, MISC::GET_HASH_KEY("WORLD_HUMAN_SMOKE"), {3227.06f, -615.65f, 42.24f}, 236, -1, false, false, "", 0, false);
#else
	TASK::TASK_FOLLOW_NAV_MESH_TO_COORD(Milliken, {3227.06f, -615.65f, 42.24f}, 10.0f, -1, 0.1f, 0, 236);
#endif

	int sequence;
	TASK::OPEN_SEQUENCE_TASK(&sequence);

#ifndef DEBUG_FAST_MISSION
	TASK::TASK_TURN_PED_TO_FACE_ENTITY(0, Milliken, 5000, 0, 0, 0);
	TASK::TASK_STAND_STILL(0, 5000);
	TASK::TASK_GO_STRAIGHT_TO_COORD(0, {3282.94f, -603.36f, 42.07f}, 1.0f, -1, 40000.0f, 0.1f, 0);
#else
	TASK::TASK_GO_STRAIGHT_TO_COORD(0, {3282.94f, -603.36f, 42.07f}, 10.0f, -1, 40000.0f, 0.5f, 0);
#endif
	TASK::TASK_FOLLOW_NAV_MESH_TO_COORD(0, {3316.92f, -587.71f, 41.89f}, 2.0f, -1, 0.1f, 0, 43);
	TASK::TASK_STAND_GUARD(0, {3316.92f, -587.71f, 41.89f}, 43, "WORLD_HUMAN_GUARD_MILITARY");

	TASK::CLOSE_SEQUENCE_TASK(sequence);
	TASK::TASK_PERFORM_SEQUENCE(MillikenPartner, sequence);
	TASK::CLEAR_SEQUENCE_TASK(&sequence);

	ObjectiveIndexStealth++;
}

void JailbreakSisika::Objective2_Stealth_Disguise_Run()
{
#ifdef DEBUG_MARKERS
	for (Ped guard : SpawnedGuardsNeutral)
	{
		Common::DrawDebugCircle(ENTITY::GET_ENTITY_COORDS(guard, true, true), 25.0f, 0.9f, "COLOR_BLACK", true, true);
	}

	Common::DrawDebugCircle(ENTITY::GET_ENTITY_COORDS(MillikenPartner, true, true), 35.0f, 0.9f, "COLOR_BLUE", true, true);
#endif

	AbandonCoords = ENTITY::GET_ENTITY_COORDS(Milliken, true, true);
	
	for (Ped guard : SpawnedGuardsAllGlobal)
	{
		if (ENTITY::DOES_ENTITY_EXIST(guard) && guard != Milliken && PED::IS_PED_DEAD_OR_DYING(guard, true))
		{
			StealthBroken();
			return;
		}
	}

	for (Ped prisoner : SpawnedPrisonersOutside)
	{
		if (PED::IS_PED_DEAD_OR_DYING(prisoner, true))
		{
			StealthBroken();
			return;
		}
	}

	if (!ReadyToDisguise && PED::IS_PED_DEAD_OR_DYING(Milliken, true))
	{
		for (int i = 0; i < SpawnedGuardsAllGlobal.size(); ++i)
		{
			if (SpawnedGuardsAllGlobal[i] == Milliken)
			{
				SpawnedGuardsAllGlobal.erase(SpawnedGuardsAllGlobal.begin() + i);
				break;
			}
		}

		ObjectivesList[2] = "Loot the ~COLOR_YELLOW~guard";
		ObjectiveSet(2);

		ReadyToDisguise = true;

		ENTITY::_SET_ENTITY_CARRYING_FLAG(Milliken, 7, false);

		AbandonRadius = 10.0f;
	}

	if (ReadyToDisguise && ENTITY::_IS_ENTITY_FULLY_LOOTED(Milliken))
	{
		ClearIsland();
		
		ApplyGuardUniformToPlayer();
		ApplyGuardUniformRemoved(Milliken);
		
		PlayerDisguised = true;

		ObjectiveIndexStealth++;
	}
}

void JailbreakSisika::Objective3_Stealth_Bridge_Init()
{
	ObjectiveSet(3);

	MAP::REMOVE_BLIP(&ObjectiveBlip);

	ObjectiveBlip = ObjectiveBlipCoords({3276.29f, -599.94f, 42.12f}, 10.0f, true, ObjectivesList[3]);

	AbandonCoords = {3276.29f, -599.94f, 42.12f};
	AbandonRadius = 65.0f;

	for (Ped guard : SpawnedGuardsAllGlobal)
	{
		if (ENTITY::DOES_ENTITY_EXIST(guard) && !PED::IS_PED_DEAD_OR_DYING(guard, true))
		{
			PED::SET_PED_RELATIONSHIP_GROUP_HASH(guard, REL_CIV);

			PED::SET_PED_CONFIG_FLAG(guard, PCF_DisableTalkTo, false);

			Blip b = MAP::GET_BLIP_FROM_ENTITY(guard);
			MAP::REMOVE_BLIP(&b);
		}
	}

	const std::vector<std::string> guardVoices = {
		"0987_S_M_M_DISPATCHLAWRURAL_WHITE_02",
		"0989_S_M_M_DISPATCHLAWRURAL_WHITE_04",
		"0991_S_M_M_DISPATCHLAWRURAL_WHITE_06",
		"0993_S_M_M_DISPATCHLAWRURAL_WHITE_08",
		"0994_S_M_M_DISPATCHLAWRURAL_WHITE_09"
	};

	for (Ped guard : SpawnedGuardsSus)
	{
		AUDIO::SET_AMBIENT_VOICE_NAME(guard, guardVoices[rand() % guardVoices.size()].c_str());
		
		PED::SET_PED_CONFIG_FLAG(guard, PCF_DisableTalkTo, true);

		PED::SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(guard, false);

		Blip b = MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_COP, guard);
		MAP::BLIP_ADD_MODIFIER(b, BLIP_MODIFIER_ENEMY_ON_GUARD);
		MAP::BLIP_ADD_MODIFIER(b, BLIP_MODIFIER_WITNESS_INVESTIGATING);
	}

	ObjectiveIndexStealth++;
}

void JailbreakSisika::Objective3_Stealth_Bridge_Run()
{
	if (Common::GetDistBetweenCoords(PlayerCoords, MAP::GET_BLIP_COORDS(ObjectiveBlip)) < 10.0f)
	{
		MAP::REMOVE_BLIP(&ObjectiveBlip);

		ObjectiveIndexStealth++;
	}
}

void JailbreakSisika::Objective4_Stealth_Courtyard_Init()
{
	ObjectiveSet(4);

	MAP::REMOVE_BLIP(&ObjectiveBlip);

	ObjectiveBlip = ObjectiveBlipCoords({3352.31f, -650.08f, 44.30f}, 10.0f, false, ObjectivesList[4]);

	AbandonCoords = MAP::GET_BLIP_COORDS(ObjectiveBlip);
	AbandonRadius = Common::GetDistBetweenCoords(PlayerCoords, AbandonCoords) + 5.0f;

	SpawnInsidePeds();

	ObjectiveIndexStealth++;
}

void JailbreakSisika::Objective4_Stealth_Courtyard_Run()
{
	if (!MAP::_IS_BLIP_ATTACHED_TO_ANY_ENTITY(ObjectiveBlip) && Common::GetDistBetweenCoords(PlayerCoords, MAP::GET_BLIP_COORDS(ObjectiveBlip)) < 10.0f)
	{
		MAP::REMOVE_BLIP(&ObjectiveBlip);

		AbandonCoords = PrisonCenter;
		AbandonRadius = PrisonRadius + 10.0f;

		TASK::TASK_LOOK_AT_ENTITY(GateGuard, PlayerPed, 5000, 0, 0, 0);

		ScriptedSpeechParams params{"GREET_MARSHAL", 0, 0, MISC::GET_HASH_KEY("SPEECH_PARAMS_BEAT_SHOUTED_CLEAR_NOSUB"), 0, true, 0, 0};
		AUDIO::PLAY_PED_AMBIENT_SPEECH_NATIVE(GateGuard, (Any*)&params);

		ObjectivesList[4] = "Make your way to the ~COLOR_YELLOW~courtyard";
		ObjectiveSet(4);

		ObjectiveBlip = ObjectiveBlipEntity(CourtyardGuard, true, ObjectivesList[4]);

		for (int i = 0; i < SpawnedGangMembers.size(); i++)
		{
			ENTITY::_SET_ENTITY_COORDS_AND_HEADING(SpawnedGangMembers[i].member, Common::ApplyRandomSmallOffsetToCoords({3282.43f, -574.20f, 42.04f}, 5.0f), 232, true, true, true);

			PED::_SET_PED_CROUCH_MOVEMENT(SpawnedGangMembers[i].member, true, 0, true);
		}

		RemoveOutsidePeds();
	}
	else if (MAP::_IS_BLIP_ATTACHED_TO_ANY_ENTITY(ObjectiveBlip) && Common::GetDistBetweenCoords(PlayerCoords, MAP::GET_BLIP_COORDS(ObjectiveBlip)) < 5.0f)
	{
		MAP::REMOVE_BLIP(&ObjectiveBlip);

		TASK::TASK_WANDER_IN_AREA(CourtyardGuard, PrisonCenter, 20.0f, 0, 0, 0);

		ScriptedSpeechParams params{"GOODBYE_START", 0, 0, MISC::GET_HASH_KEY("SPEECH_PARAMS_BEAT_SHOUTED_CLEAR_NOSUB"), 0, true, 0, 0};
		AUDIO::PLAY_PED_AMBIENT_SPEECH_NATIVE(CourtyardGuard, (Any*)&params);

		ObjectiveIndexStealth++;
	}
}

void JailbreakSisika::Objective5_Stealth_Escort_Init()
{
	ObjectiveSet(5);

	MAP::REMOVE_BLIP(&ObjectiveBlip);

	ObjectiveBlip = ObjectiveBlipCoords({3351.13f, -646.82f, 44.29f}, 10.0f, true, ObjectivesList[5]);

	AbandonCoords = IslandCenter;
	AbandonRadius = IslandRadius;

	TASK::CLEAR_PED_TASKS_IMMEDIATELY(Warden, true, true);
	TASK::TASK_STAND_GUARD(Warden, {3327.49f, -675.22f, 44.58f}, 139, "WORLD_HUMAN_STERNGUY_IDLES");

	for (int i = 0; i <= 6; i++)
	{
		Ped prisoner = SpawnPed(PrisonerModel.c_str(), {3338.81f, -668.37f, 44.79f}, 200, 255);

		SetPedType(prisoner, PedType::SIDEARM_EXPERT);
		WEAPON::REMOVE_ALL_PED_WEAPONS(prisoner, true, true);
		
		PED::SET_PED_RELATIONSHIP_GROUP_HASH(prisoner, REL_CIV);

		Common::SetPedHonorModifier(prisoner, HONOR_NEGATIVE);

		EVENT::SET_DECISION_MAKER(prisoner, MISC::GET_HASH_KEY("EMPTY"));

		ENTITY::SET_ENTITY_NO_COLLISION_ENTITY(prisoner, PlayerPed, false);

		MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_COMPANION, prisoner);

		TASK::TASK_FOLLOW_TO_OFFSET_OF_ENTITY(prisoner, PlayerPed, {0, -3, 0}, 0.5f, -1, 1.2f, true, false, false, false, false, false);

		SpawnedPrisonersInside.emplace_back(prisoner);
	}

	Ped leader = SpawnPed(GenericLawLeader, {3314.19f, -600.13f, 42.45f}, 216, 16);
	SetPedType(leader, PedType::SIDEARM_EXPERT);

	EVENT::SET_DECISION_MAKER(leader, MISC::GET_HASH_KEY("EMPTY"));

	SpawnedGuardsAllGlobal.emplace_back(leader);

	AUDIO::SET_AMBIENT_VOICE_NAME(leader, "0995_S_M_M_DISPATCHLEADERRURAL_WHITE_01");

	TASK::TASK_TURN_PED_TO_FACE_ENTITY(leader, PlayerPed, -1, 0, 0, 0);

	EscortConfronters = {
		leader,
		SpawnGuard({3313.35f, -597.42f, 42.09f}, 222, PedType::REPEATER_EXPERT, true),
		SpawnGuard({3310.75f, -599.05f, 42.05f}, 224, PedType::REPEATER_EXPERT, true)
	};

	for (Ped confronter : EscortConfronters)
	{
		PED::SET_PED_RELATIONSHIP_GROUP_HASH(confronter, REL_CIV);

		PED::SET_PED_CONFIG_FLAG(leader, PCF_OneShotWillKillPed, true);
	}

	StealthEscorting = true;

	ObjectiveIndexStealth++;
}

void JailbreakSisika::Objective5_Stealth_Escort_Run()
{
	const Vector3 confrontCoords = {3315.04f, -601.10f, 42.71f};

#ifdef DEBUG_MARKERS
	Common::DrawDebugCircle(confrontCoords, 10.0f, 0.9f, "COLOR_WHITE");
#endif

	Ped leader = EscortConfronters[0];

	if (MAP::DOES_BLIP_EXIST(ObjectiveBlip) && Common::GetDistBetweenCoords(PlayerCoords, MAP::GET_BLIP_COORDS(ObjectiveBlip)) < 10.0f)
	{
		DioTrace(DIOTAG "Reached gate, setting StealthPassed");
		
		StealthPassed = true;

		MAP::REMOVE_BLIP(&ObjectiveBlip);

		ObjectiveBlip = ObjectiveBlipCoords(RegroupCoords, 10.0f, true, ObjectivesList[5]);
	}
	else if (MAP::DOES_BLIP_EXIST(ObjectiveBlip) && Common::GetDistBetweenCoords(PlayerCoords, confrontCoords) < 10.0f)
	{
		DioTrace(DIOTAG "Reached bridge confronters");

		MAP::REMOVE_BLIP(&ObjectiveBlip);

		for (Ped confronter : EscortConfronters)
		{
			TASK::TASK_LOOK_AT_ENTITY(confronter, PlayerPed, -1, 2096, 31, 0);
		}

		ScriptedSpeechParams params{"LAW_INTERACT_HOLD_UP", 0, 0, MISC::GET_HASH_KEY("SPEECH_PARAMS_BEAT_SHOUTED_CLEAR_NOSUB"), 0, true, 0, 0};
		AUDIO::PLAY_PED_AMBIENT_SPEECH_NATIVE(leader, (Any*)&params);
		
		TASK::CLEAR_PED_TASKS_IMMEDIATELY(leader, true, true);
		TASK::TASK_PLAY_EMOTE_WITH_HASH(leader, EMOTE_TYPE_ACTION, EMOTE_PM_FULLBODY, KIT_EMOTE_ACTION_POINT_1, false, false, false, true, true);

		GenericTimer = MISC::GET_GAME_TIMER();
	}
	else if (GenericTimer != 0 && !WEAPON::HAS_PED_GOT_WEAPON(leader, WEAPON_MELEE_KNIFE, 0, false) && !MAP::DOES_BLIP_EXIST(ObjectiveBlip) && MISC::GET_GAME_TIMER() - GenericTimer > 4500)
	{
		DioTrace(DIOTAG "Leader confronting");

		WEAPON::GIVE_WEAPON_TO_PED(leader, WEAPON_REVOLVER_DOUBLEACTION, 1, true, false, WEAPON_ATTACH_POINT_HAND_PRIMARY, true, 0, 0, ADD_REASON_DEFAULT, true, 0, false);
		
		ScriptedSpeechParams params{"LAW_PLAYER_BOUNTY_IDENTIFIED_ESCALATED", 0, 0, MISC::GET_HASH_KEY("SPEECH_PARAMS_BEAT_SHOUTED_CLEAR_NOSUB"), 0, true, 0, 0};
		AUDIO::PLAY_PED_AMBIENT_SPEECH_NATIVE(leader, (Any*)&params);

		TASK::TASK_AIM_GUN_AT_ENTITY(leader, PlayerPed, -1, true, 0);

		GenericTimer = MISC::GET_GAME_TIMER();
	}
	else if (GenericTimer != 0 && WEAPON::HAS_PED_GOT_WEAPON(leader, WEAPON_REVOLVER_DOUBLEACTION, 0, false) && !MAP::DOES_BLIP_EXIST(ObjectiveBlip) && MISC::GET_GAME_TIMER() - GenericTimer > 1500)
	{
		DioTrace(DIOTAG "Gang attacking");

		for (int i = 0; i < SpawnedGangMembers.size(); i++)
		{
			PED::_SET_PED_CROUCH_MOVEMENT(SpawnedGangMembers[i].member, true, 0, true);
			TASK::TASK_COMBAT_PED(SpawnedGangMembers[i].member, leader, 0, 0);
		}

		AllowCompanionManualControl = true;

		GenericTimer = 0;
	}
}

void JailbreakSisika::Objective6_Loud_Bridge_Init()
{
	ObjectiveSet(6);

	AbandonCoords = IslandCenter;
	AbandonRadius = IslandRadius;

	CompanionsClearTasks();
	CompanionsRegroup();
	CompanionsBeginCombat();

	AllowCompanionManualControl = true;

	for (int i = 0; i < SpawnedGangMembers.size(); i++)
	{
		PED::_SET_PED_CROUCH_MOVEMENT(SpawnedGangMembers[i].member, false, 0, false);
	}

	SpawnedGuardsSus.emplace_back(Milliken);
	SpawnedGuardsSus.emplace_back(GateGuard);
	SpawnGuard({3325.34f, -593.84f, 41.87f}, 132, PedType::REPEATER_WEAK, true);
	SpawnGuard({3333.59f, -642.31f, 42.93f}, 40, PedType::REPEATER_WEAK, true);
	SpawnGuard({3340.48f, -638.09f, 43.25f}, 48, PedType::REPEATER_WEAK, true);
	SpawnGuard({3321.95f, -595.43f, 41.92f}, 126, PedType::REPEATER_WEAK, true);

	ObjectiveBlip = ObjectiveBlipCoords(BridgeCoords, BridgeRadius, true, ObjectivesList[6]);
	
	for (Ped guard : SpawnedGuardsSus)
	{
		PED::SET_PED_SPHERE_DEFENSIVE_AREA(guard, BridgeCoords, BridgeRadius, 0, false, 0);

		PED::SET_PED_COMBAT_ATTRIBUTES(guard, CA_PERMIT_CHARGE_BEYOND_DEFENSIVE_AREA, false);
		PED::SET_PED_COMBAT_ATTRIBUTES(guard, CA_CAN_CHARGE, false);
		PED::SET_PED_COMBAT_ATTRIBUTES(guard, CA_CAN_CHASE_TARGET_ON_FOOT, false);
		PED::SET_PED_COMBAT_ATTRIBUTES(guard, CA_USE_COVER, true);
		PED::SET_PED_COMBAT_ATTRIBUTES(guard, CA_SWITCH_TO_ADVANCE_IF_CANT_FIND_COVER, false);
		PED::SET_PED_COMBAT_ATTRIBUTES(guard, CA_DISABLE_RETREAT_DUE_TO_TARGET_PROXIMITY, true);

		PED::SET_PED_RELATIONSHIP_GROUP_HASH(guard, REL_PLAYER_ENEMY);

		if (!MAP::_DOES_ENTITY_HAVE_BLIP(guard))
		{
			Blip b = MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_COP, guard);
			MAP::BLIP_ADD_MODIFIER(b, BLIP_MODIFIER_ENEMY_IS_ALERTED);
			MAP::BLIP_REMOVE_MODIFIER(b, BLIP_MODIFIER_ENEMY_ON_GUARD);
		}

		TASK::CLEAR_PED_TASKS(guard, true, true);
		TASK::TASK_COMBAT_PED(guard, PlayerPed, 0, 0);
	}

	ObjectiveIndexLoud++;
}

void JailbreakSisika::Objective6_Loud_Bridge_Run()
{
#ifdef DEBUG_MARKERS
	Common::DrawDebugCircle(BridgeCoords, BridgeRadius, 0.7f, "COLOR_RED");
#endif

	if (Common::GetDistBetweenCoords(PlayerCoords, BridgeCoords) < BridgeRadius)
	{
		bool clear = true;
		
		for (Ped guard : SpawnedGuardsAllGlobal)
		{
			if (ENTITY::DOES_ENTITY_EXIST(guard) && !PED::IS_PED_DEAD_OR_DYING(guard, true) &&
				Common::GetDistBetweenCoords(ENTITY::GET_ENTITY_COORDS(guard, true, true), BridgeCoords) < BridgeRadius)
			{
#ifdef DEBUG_FAST_MISSION
				PED::EXPLODE_PED_HEAD(guard, WEAPON_REVOLVER_CATTLEMAN);
#endif
				clear = false;
				break;
			}
		}

		if (clear)
		{
			ObjectiveIndexLoud++;
		}
	}
}

void JailbreakSisika::Objective7_Loud_Cannon_Init()
{
	ObjectiveSet(7);

	AbandonCoords = BridgeCoords;
	AbandonRadius = BridgeRadius;

	MAP::REMOVE_BLIP(&ObjectiveBlip);

	ObjectiveBlip = ObjectiveBlipEntity(Cannon, true, ObjectivesList[7]);
	MAP::SET_BLIP_SPRITE(ObjectiveBlip, MISC::GET_HASH_KEY("blip_weapon_cannon"), false);

	CannonPrompt = HUD::_UIPROMPT_REGISTER_BEGIN();
	HUD::_UIPROMPT_SET_CONTROL_ACTION(CannonPrompt, INPUT_ENTER);
	HUD::_UIPROMPT_SET_TEXT(CannonPrompt, MISC::VAR_STRING(10, "LITERAL_STRING", "Turn Cannon"));
	HUD::_UIPROMPT_SET_STANDARD_MODE(CannonPrompt, 1);
	HUD::_UIPROMPT_REGISTER_END(CannonPrompt);
	HUD::_UIPROMPT_SET_ENABLED(CannonPrompt, false);
	HUD::_UIPROMPT_SET_VISIBLE(CannonPrompt, false);

	for (Ped prisoner : SpawnedPrisonersOutside)
	{
		ENTITY::DELETE_ENTITY(&prisoner);
	}

	SpawnedPrisonersOutside.clear();

	ObjectiveIndexLoud++;
}

void JailbreakSisika::Objective7_Loud_Cannon_Run()
{
	ENTITY::SET_ENTITY_COLLISION(Cannon, false, false);
	
	if (CurrentCannonStatus == CannonStatus::IDLE)
	{
		if (Common::GetDistBetweenCoords(PlayerCoords, ENTITY::GET_ENTITY_COORDS(Cannon, true, true)) <= 2.5f)
		{
			HUD::_UIPROMPT_SET_ENABLED(CannonPrompt, true);
			HUD::_UIPROMPT_SET_VISIBLE(CannonPrompt, true);
		}
		else
		{
			HUD::_UIPROMPT_SET_ENABLED(CannonPrompt, false);
			HUD::_UIPROMPT_SET_VISIBLE(CannonPrompt, false);
		}

		if (HUD::_UIPROMPT_HAS_STANDARD_MODE_COMPLETED(CannonPrompt, 0))
		{
			DioTrace(DIOTAG "Starting cannon turn");

			CurrentCannonStatus = CannonStatus::TURNING;

			HUD::_UIPROMPT_SET_ENABLED(CannonPrompt, false);
			HUD::_UIPROMPT_SET_VISIBLE(CannonPrompt, false);

			AllowCompanionManualControl = false;
			CompanionsDisband();

			PLAYER::SET_PLAYER_CONTROL(0, false, 256, true);

			ENTITY::SET_ENTITY_INVINCIBLE(PlayerPed, true);

			WEAPON::_HIDE_PED_WEAPONS(PlayerPed, 0, true);

			PED::_SET_PED_CROUCH_MOVEMENT(PlayerPed, false, 0, true);

			int sequence = 0;
			TASK::OPEN_SEQUENCE_TASK(&sequence);
			TASK::TASK_PED_SLIDE_TO_COORD(0, ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Cannon, 0.49f, -0.9f, 0), ENTITY::GET_ENTITY_HEADING(Cannon) + 80, -1);
			TASK::TASK_PLAY_ANIM(0, CannonDict.c_str(), CannonAnim.c_str(), 1.0f, 1.0f, -1, 0, 0, 0, 0, 0, 0, 0);
			TASK::CLOSE_SEQUENCE_TASK(sequence);
			TASK::TASK_PERFORM_SEQUENCE(PlayerPed, sequence);
			TASK::CLEAR_SEQUENCE_TASK(&sequence);

			while (TASK::GET_SEQUENCE_PROGRESS(PlayerPed) < 0)
			{
				DioTrace(DIOTAG "Waiting for sequence to register");
				WAIT(0);
				DisableLaw();
				ClearIsland();
			}
		}
	}
	else if (CurrentCannonStatus == CannonStatus::TURNING)
	{
		for (Ped guard : SpawnedGuardsAllGlobal)
		{
			if (ENTITY::DOES_ENTITY_EXIST(guard) && !PED::IS_PED_DEAD_OR_DYING(guard, true) &&
				Common::GetDistBetweenCoords(ENTITY::GET_ENTITY_COORDS(guard, true, true), ENTITY::GET_ENTITY_COORDS(Cannon, true, true)) <= 1.5f)
			{
				PED::EXPLODE_PED_HEAD(guard, WEAPON_REVOLVER_CATTLEMAN);
				ENTITY::SET_ENTITY_COLLISION(guard, false, false);
			}
		}

		while (TASK::GET_SEQUENCE_PROGRESS(PlayerPed) > -1)
		{
			if (TASK::GET_SEQUENCE_PROGRESS(PlayerPed) >= 1 &&
				ENTITY::_GET_ENTITY_ANIM_CURRENT_TIME(PlayerPed, CannonDict.c_str(), CannonAnim.c_str()) > 0.13f)
			{
				ENTITY::FREEZE_ENTITY_POSITION(PlayerPed, true);
				ENTITY::FREEZE_ENTITY_POSITION(Cannon, false);

				ENTITY::SET_ENTITY_HEADING(Cannon, ENTITY::GET_ENTITY_HEADING(Cannon) - 0.21f);
			}

			if (ENTITY::_GET_ENTITY_ANIM_CURRENT_TIME(PlayerPed, CannonDict.c_str(), CannonAnim.c_str()) > 0.36f)
			{
				ENTITY::FREEZE_ENTITY_POSITION(PlayerPed, false);
				TASK::CLEAR_PED_TASKS(PlayerPed, true, true);
			}

			WAIT(0);
			DisableLaw();
			ClearIsland();
		}

		ENTITY::_SET_ENTITY_COORDS_AND_HEADING_NO_OFFSET(Cannon, CannonPos, CannonRot, true, true);

		ENTITY::SET_ENTITY_COLLISION(Cannon, true, false);

		ENTITY::SET_ENTITY_INVINCIBLE(PlayerPed, false);

		PLAYER::SET_PLAYER_CONTROL(0, true, 256, true);

		VEHICLE::SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(Cannon, true);

		PAD::DISABLE_CONTROL_ACTION(0, INPUT_VEH_EXIT, true);
	}

	if (PED::GET_VEHICLE_PED_IS_USING(PlayerPed) == Cannon && MISC::IS_PROJECTILE_IN_AREA({3351.07f, -638.03f, 44.13f}, {3348.05f, -647.97f, 50.85f}, true))
	{
		DioTrace(DIOTAG "Cannon fired");

		SetFrontGate(true, true);

		PAD::ENABLE_CONTROL_ACTION(0, INPUT_VEH_EXIT, true);

		TASK::TASK_LEAVE_VEHICLE(PlayerPed, Cannon, 0, 0);

		ENTITY::SET_ENTITY_INVINCIBLE(Cannon, false);
		ENTITY::FREEZE_ENTITY_POSITION(Cannon, false);
		VEHICLE::SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(Cannon, false);

		CompanionsRegroup();
		AllowCompanionManualControl = true;

		ObjectiveIndexLoud++;
	}
}

void JailbreakSisika::Objective8_Loud_Courtyard_Init()
{
	ObjectiveSet(8);

	MAP::REMOVE_BLIP(&ObjectiveBlip);

	ObjectiveBlip = ObjectiveBlipCoords(YardCoords, YardRadius, false, ObjectivesList[8]);

	AbandonCoords = PrisonCenter;
	AbandonRadius = PrisonRadius + 15.0f;

	if (SpawnLoudPeds)
	{
		DioTrace(DIOTAG "Spawning Loud peds");

		// Walls and Towers
		SpawnReinforcement({3364.05f, -681.61f, 50.87f}, 27, PedType::RIFLE_WEAK, true);
		SpawnReinforcement({3362.14f, -683.24f, 50.87f}, 129, PedType::RIFLE_WEAK, true);
		SpawnReinforcement({3364.95f, -679.84f, 50.87f}, 358, PedType::RIFLE_WEAK, true);
		SpawnReinforcement({3369.59f, -686.53f, 49.85f}, 38, PedType::RIFLE_WEAK, true);
		SpawnReinforcement({3328.71f, -702.90f, 50.85f}, 211, PedType::RIFLE_WEAK, true);
		SpawnReinforcement({3330.53f, -704.88f, 50.85f}, 237, PedType::RIFLE_WEAK, true);
		SpawnReinforcement({3324.19f, -684.33f, 50.85f}, 282, PedType::RIFLE_WEAK, true);
		SpawnReinforcement({3321.23f, -673.95f, 50.85f}, 274, PedType::RIFLE_WEAK, true);

		// Front Gate area 
		SpawnReinforcement({3351.54f, -665.88f, 44.68f}, 279, PedType::SHOTGUN_WEAK, true);
		SpawnReinforcement({3348.52f, -667.92f, 44.68f}, 292, PedType::SIDEARM_REGULAR, true);
		SpawnReinforcement({3360.17f, -648.73f, 45.30f}, 118, PedType::SHOTGUN_WEAK, true);
		SpawnReinforcement({3362.42f, -656.76f, 45.27f}, 44, PedType::SIDEARM_REGULAR, true);

		// Courtyard
		SpawnReinforcement({3356.01f, -685.88f, 43.04f}, 108, PedType::SHOTGUN_WEAK, true);
		SpawnReinforcement({3346.11f, -685.47f, 43.00f}, 310, PedType::SIDEARM_REGULAR, true);
		SpawnReinforcement({3352.29f, -694.14f, 43.08f}, 6, PedType::SIDEARM_REGULAR, true);
		SpawnReinforcement({3330.67f, -689.18f, 42.96f}, 292, PedType::SIDEARM_REGULAR, true);

		// Back area
		SpawnReinforcement({3351.91f, -708.06f, 44.02f}, 17, PedType::REPEATER_WEAK, true);
		SpawnReinforcement({3359.80f, -702.39f, 44.01f}, 16, PedType::REPEATER_WEAK, true);
		SpawnReinforcement({3364.27f, -696.56f, 44.01f}, 91, PedType::REPEATER_WEAK, true);
	}

	ObjectiveIndexLoud++;
}

void JailbreakSisika::Objective8_Loud_Courtyard_Run()
{
#ifdef DEBUG_MARKERS
	Common::DrawDebugCircle(YardCoords, YardRadius, 1.8f, "COLOR_RED");
#endif

	if (Common::GetDistBetweenCoords(PlayerCoords, YardCoords) < YardRadius)
	{
		bool clear = true;

		for (Ped guard : SpawnedGuardsAllGlobal)
		{
			if (ENTITY::DOES_ENTITY_EXIST(guard) && !PED::IS_PED_DEAD_OR_DYING(guard, true) &&
				Common::GetDistBetweenCoords(ENTITY::GET_ENTITY_COORDS(guard, true, true), YardCoords) < YardRadius)
			{
#ifdef DEBUG_FAST_MISSION
				PED::EXPLODE_PED_HEAD(guard, WEAPON_REVOLVER_CATTLEMAN);
#endif
				clear = false;
				break;
			}
		}

		if (clear)
		{
			for (int i = 0; i <= 8; i++)
			{
				Ped prisoner = SpawnPed(PrisonerModel.c_str(), {3339.64f, -670.34f, 44.68f}, 200, 255);

				SetPedType(prisoner, PedType::SIDEARM_EXPERT);
				WEAPON::REMOVE_ALL_PED_WEAPONS(prisoner, true, true);

				PED::SET_PED_RELATIONSHIP_GROUP_HASH(prisoner, REL_CIV);

				Common::SetPedHonorModifier(prisoner, HONOR_NEGATIVE);

				EVENT::SET_DECISION_MAKER(prisoner, MISC::GET_HASH_KEY("EMPTY"));

				ENTITY::SET_ENTITY_NO_COLLISION_ENTITY(prisoner, PlayerPed, false);

				TASK::TASK_FOLLOW_NAV_MESH_TO_COORD(prisoner, Common::ApplyRandomSmallOffsetToCoords(RegroupCoords, 2.0f), 20.0f, -1, 0.5f, 0, 250);

				SpawnedPrisonersInside.emplace_back(prisoner);
			}

			SpawnReinforcement({3347.90f, -648.60f, 50.85f}, 203, PedType::RIFLE_WEAK, true);
			SpawnReinforcement({3355.84f, -646.34f, 50.85f}, 190, PedType::RIFLE_WEAK, true);
			SpawnReinforcement({3364.29f, -643.45f, 50.85f}, 178, PedType::RIFLE_WEAK, true);
			SpawnReinforcement({3368.86f, -642.02f, 50.85f}, 165, PedType::RIFLE_WEAK, true);
			SpawnReinforcement({3328.03f, -651.41f, 50.85f}, 15, PedType::RIFLE_WEAK, true);

			Ped sniper = SpawnReinforcement({3293.01f, -659.84f, 54.03f}, 320, PedType::SNIPER_WEAK, false, true);
			PED::SET_PED_COMBAT_MOVEMENT(sniper, 0);

			SpawnReinforcement({3314.31f, -594.03f, 41.91f}, 218, PedType::REPEATER_REGULAR, true, true);
			SpawnReinforcement({3309.74f, -599.93f, 42.06f}, 225, PedType::SHOTGUN_WEAK, true, true);
			SpawnReinforcement({3302.21f, -601.21f, 41.96f}, 226, PedType::SIDEARM_REGULAR, true, true);
			
			ObjectiveIndexLoud++;
		}
	}
}

void JailbreakSisika::Objective9_Loud_Regroup_Init()
{
	ObjectiveSet(9);

	AbandonCoords = IslandCenter;
	AbandonRadius = IslandRadius;
	
	MAP::REMOVE_BLIP(&ObjectiveBlip);

	ObjectiveBlip = ObjectiveBlipCoords(RegroupCoords, RegroupRadius, false, ObjectivesList[9]);

	ObjectiveIndexLoud++;
}

void JailbreakSisika::Objective9_Loud_Regroup_Run()
{
	if (Common::GetDistBetweenCoords(PlayerCoords, RegroupCoords) < RegroupRadius)
	{
		MAP::REMOVE_BLIP(&ObjectiveBlip);
		
		ObjectiveIndexLoud++;
	}
}

void JailbreakSisika::Objective10_Loud_Defend_Init()
{
	ObjectiveSet(10);

	AbandonCoords = RegroupCoords;
	AbandonRadius = RegroupRadius;

	for (Ped prisoner : SpawnedPrisonersInside)
	{
		if (ENTITY::DOES_ENTITY_EXIST(prisoner) && !PED::IS_PED_DEAD_OR_DYING(prisoner, true))
		{
			TASK::CLEAR_PED_TASKS(prisoner, true, true);
			TASK::TASK_COWER(prisoner, -1, 0, "");
		}
	}

	SpawnedGuardsNeutral.clear();

	for (Ped guard : SpawnedGuardsAllGlobal)
	{
		if (ENTITY::DOES_ENTITY_EXIST(guard) && !PED::IS_PED_DEAD_OR_DYING(guard, true))
		{
			Blip b = MAP::GET_BLIP_FROM_ENTITY(guard);
			MAP::REMOVE_BLIP(&b);
		}
	}

	for (int i = 0; i < 3; ++i)
	{
		SpawnedGuardsNeutral.emplace_back(SpawnReinforcement(Common::ApplyRandomSmallOffsetToCoords({3210.21f, -557.13f, 41.70f}, 5.0f), 45, PedType::REPEATER_WEAK, false, true, true));
	}

	if (!StealthPassed)
	{
		for (int i = 0; i < 3; ++i)
		{
			SpawnedGuardsNeutral.emplace_back(SpawnReinforcement(Common::ApplyRandomSmallOffsetToCoords({3233.29f, -489.52f, 41.56f}, 5.0f), 45, PedType::REPEATER_WEAK, false, true, true));
		}

		GuardBoat = SpawnVehicle("keelboat", {3060.70f, -570.41f, 40.90f}, 0, true);

		VEHICLE::SET_BOAT_ANCHOR(GuardBoat, false);
		VEHICLE::SET_BOAT_SINKS_WHEN_WRECKED(GuardBoat, true);
		VEHICLE::_SET_BOAT_MOVEMENT_RESISTANCE(GuardBoat, 5.0f);

		Ped driver = SpawnPed(GenericLawLeader, {3090.68f, -596.93f, 42.57f}, 101, 255);
		PED::SET_PED_INTO_VEHICLE(driver, GuardBoat, SEAT_DRIVER);
		TASK::TASK_BOAT_MISSION(driver, GuardBoat, 0, 0, {3092.96f, -480.38f, 40.90f}, 4, 50.0f, 16777216, 4.0f, 20509);

		Boatmen = {
			driver,
			SpawnPed(GenericLawFollower, ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(GuardBoat, -1.5f, 0, 2), 0, 255),
			SpawnPed(GenericLawFollower, ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(GuardBoat, -1.5f, -2, 2), 0, 255),
			SpawnPed(GenericLawFollower, ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(GuardBoat, 0, 3.5f, 2), 0, 255)
		};

		for (Ped boatman : Boatmen)
		{
			SetPedType(boatman, PedType::REPEATER_REGULAR);

			PED::SET_PED_RELATIONSHIP_GROUP_HASH(boatman, REL_PLAYER_ENEMY);

			EVENT::SET_DECISION_MAKER(boatman, MISC::GET_HASH_KEY("EMPTY"));

			MAP::BLIP_ADD_MODIFIER(MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_COP, boatman), BLIP_MODIFIER_ENEMY_IS_ALERTED);

			PED::SET_PED_COMBAT_MOVEMENT(boatman, 0);

			SpawnedGuardsAllGlobal.emplace_back(boatman);
			SpawnedGuardsNeutral.emplace_back(boatman);

			if (boatman != driver)
			{
				TASK::TASK_COMBAT_PED(boatman, PlayerPed, 0, 0);
			}
		}
	}

	ObjectiveIndexLoud++;
}

void JailbreakSisika::Objective10_Loud_Defend_Run()
{
#ifdef DEBUG_MARKERS
	Common::DrawDebugCircle(RegroupCoords, RegroupRadius, 1.5f, "COLOR_RED");
#endif

	if (!Boatmen.empty())
	{
		if (PED::IS_PED_IN_ANY_VEHICLE(Boatmen[0], false) && Common::GetDistBetweenCoords(ENTITY::GET_ENTITY_COORDS(GuardBoat, true, true), {3090.96f, -491.78f, 40.90f}) < 5.0f)
		{
			DioTrace(DIOTAG "Boat reached destination");
			TASK::CLEAR_PED_TASKS_IMMEDIATELY(Boatmen[0], true, true);
			TASK::TASK_COMBAT_PED(Boatmen[0], PlayerPed, 0, 0);
		}

		for (Ped boatman : Boatmen)
		{
			if (!PED::IS_PED_DEAD_OR_DYING(boatman, true) && PED::IS_PED_SWIMMING(boatman))
			{
				PED::EXPLODE_PED_HEAD(boatman, WEAPON_DROWNING);
			}
		}
	}

	for (Ped horse : Horses)
	{
		if (!ENTITY::DOES_ENTITY_EXIST(PED::_GET_RIDER_OF_MOUNT(horse, true)) && !PED::GET_PED_CONFIG_FLAG(horse, PCF_AlwaysRejectPlayerRobberyAttempt, true))
		{
			TASK::TASK_FLEE_PED(horse, PlayerPed, 4, 0, -1082130432 /* Float: -1f */, -1, 0);
			PED::SET_PED_CONFIG_FLAG(horse, PCF_AlwaysRejectPlayerRobberyAttempt, true);
		}
	}

	bool allDead = true;

	for (Ped guard : SpawnedGuardsNeutral)
	{
		if (!PED::IS_PED_DEAD_OR_DYING(guard, true))
		{
			allDead = false;
			break;
		}
	}

	if (allDead)
	{
		TeleportAndFinish();
	}
}

void JailbreakSisika::TeleportAndFinish()
{
	DioTrace(DIOTAG);
	
	ObjectiveIndexStealth = -1;
	ObjectiveIndexLoud = -1;

	CheckAbandon = false;

	AllowCompanionManualControl = false;
	
	CompanionsClearTasks();
	CompanionsRegroup();

	MAP::REMOVE_BLIP(&ObjectiveBlip);

	CurrentMusicStage = STAGE_AFTERACTION;
	UpdateMusic();

	CAM::DO_SCREEN_FADE_OUT(3000);
	
	WAIT(3000);

	WEAPON::_HIDE_PED_WEAPONS(PlayerPed, 0, true);
	TASK::CLEAR_PED_TASKS_IMMEDIATELY(PlayerPed, true, true);

	ENTITY::_SET_ENTITY_COORDS_AND_HEADING(PlayerPed, {2883.86f, -261.98f, 41.12f}, 61, true, true, true);
	CAM::SET_GAMEPLAY_CAM_RELATIVE_HEADING(180, 1);
	CAM::SET_GAMEPLAY_CAM_RELATIVE_PITCH(0, 1);

	for (Vehicle boat : GangBoats)
	{
		ENTITY::DELETE_ENTITY(&boat);
	}

	GangBoats.clear();

	const std::vector<Vector4> boatSpawns = {
		{2881.19f, -279.45f, 40.95f, 23.0f},
		{2886.22f, -276.29f, 40.96f, 42.2f},
		{2889.98f, -273.21f, 40.88f, 28.9f},
		{2894.70f, -270.88f, 40.88f, 16.3f},
		{2898.11f, -267.18f, 41.12f, 84.6f},
		{2901.61f, -263.71f, 40.98f, 62.2f}
	};

	for (const Vector4& spawn : boatSpawns)
	{
		Vehicle boat = SpawnVehicle("ROWBOAT", {spawn.x, spawn.y, spawn.z + 0.5f}, spawn.w, true);
		VEHICLE::SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(0, false);
	}

	for (Ped prisoner : SpawnedPrisonersInside)
	{
		PED::REVIVE_INJURED_PED(prisoner);

		TASK::CLEAR_PED_TASKS_IMMEDIATELY(prisoner, true, true);

		ENTITY::_SET_ENTITY_COORDS_AND_HEADING(prisoner, Common::ApplyRandomSmallOffsetToCoords(ENTITY::GET_ENTITY_COORDS(PlayerPed, true, true), 5.0f), ENTITY::GET_ENTITY_HEADING(PlayerPed), true, true, true);

		ENTITY::PLACE_ENTITY_ON_GROUND_PROPERLY(prisoner, true);

		TASK::TASK_FLEE_PED(prisoner, PlayerPed, 4, 0, -1082130432 /* Float: -1f */, -1, 0);
	}

	CAM::DO_SCREEN_FADE_IN(3000);

	MissionSuccess();
}

void JailbreakSisika::TrackRewards()
{
	RewardBounty_Active = StealthPassed ? int(RewardBounty_Potential * 0.25f) : RewardBounty_Potential;

	RewardCash_Active = RewardCash_Potential;

	RewardMembers_Active = RewardMembers_Potential;
}

void JailbreakSisika::Update()
{
	CLOCK::SET_CLOCK_TIME(11, 25, 0);
	MISC::SET_WEATHER_TYPE(MISC::GET_HASH_KEY("OVERCAST"), true, true, true, 0, true);

	Ped peds[1024];
	int count = worldGetAllPeds(peds, 1024);

	for (int i = 0; i < count; i++)
	{
		Ped p = peds[i];

		if ((PED::IS_PED_MODEL(p, MISC::GET_HASH_KEY("S_M_M_SKPGUARD_01")) || PED::IS_PED_MODEL(p, MISC::GET_HASH_KEY("A_M_M_SKPPRISONER_01"))) && !ENTITY::IS_ENTITY_A_MISSION_ENTITY(p))
		{
			ENTITY::SET_ENTITY_AS_MISSION_ENTITY(p, true, true);
			ENTITY::DELETE_ENTITY(&p);
		}
	}

	StealthLogic();

	if (PlayerDisguised)
	{
		PED::REMOVE_TAG_FROM_META_PED(PlayerPed, TAG_BANDOLIER_AMMUNITION, 1);
		PED::REMOVE_TAG_FROM_META_PED(PlayerPed, TAG_GUNBELT_AMMUNITION, 1);

		if (StealthActive && PED::_GET_PLAYER_PED_WATER_DEPTH(PlayerPed) > 0.2f)
		{
			MissionFailStr = "You ruined your disguise.";
			MissionFail();
		}
	}

	if (!AlarmPlaying && AlarmTimer != 0 && MISC::GET_GAME_TIMER() - AlarmTimer > 8000)
	{
		SetAlarm(true);
		AlarmTimer = MISC::GET_GAME_TIMER();
		CurrentMusicStage = STAGE_ACTION2;

		for (Ped prisoner : SpawnedPrisonersInside)
		{
			if (ENTITY::DOES_ENTITY_EXIST(prisoner) && !PED::IS_PED_DEAD_OR_DYING(prisoner, true))
			{
				TASK::CLEAR_PED_TASKS(prisoner, true, true);
				TASK::TASK_FOLLOW_NAV_MESH_TO_COORD(prisoner, Common::ApplyRandomSmallOffsetToCoords({3105.83f, -493.58f, 40.26f}, 5.0f), 20.0f, -1, 0.5f, 0, 250);
			}
		}
	}
	else if (AlarmPlaying && AlarmTimer != 0 && MISC::GET_GAME_TIMER() - AlarmTimer > 20000)
	{
		SetAlarm(false);
		AlarmTimer = 0;
	}

	if (!StealthActive && ObjectiveIndexStealth > -1)
	{
		StealthBroken();
	}

	if (TimeMissionFail > 0 && MISC::GET_GAME_TIMER() - TimeMissionFail > TimeMissionFailDelay)
	{
		DioTrace("Exit Fail Triggered");

		ExitMissionFlag = M_EXIT_FAILURE;
	}
	else if (TimeMissionSuccess > 0 && MISC::GET_GAME_TIMER() - TimeMissionSuccess > TimeMissionSuccessDelay)
	{
		DioTrace("Exit Win Triggered");

		ExitMissionFlag = M_EXIT_SUCCESS;
	}
	else if (ObjectiveIndexStealth > -1)
	{
		StealthObjectives[ObjectiveIndexStealth]();
	}
	else if (ObjectiveIndexLoud > -1)
	{
		LoudObjectives[ObjectiveIndexLoud]();
	}
}
