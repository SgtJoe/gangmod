
#include "precomp.h"
#include "Common.h"

const Hash LongarmTierList[LongarmTierCount] = {
	WEAPON_REPEATER_CARBINE,
	WEAPON_REPEATER_WINCHESTER,
	WEAPON_SHOTGUN_DOUBLEBARREL,
	WEAPON_SHOTGUN_PUMP,
	WEAPON_RIFLE_SPRINGFIELD,
	WEAPON_RIFLE_BOLTACTION,
	WEAPON_SNIPERRIFLE_ROLLINGBLOCK,
	WEAPON_SNIPERRIFLE_CARCANO
};

const Hash SidearmTierList[SidearmTierCount] = {
	WEAPON_REVOLVER_DOUBLEACTION,
	WEAPON_REVOLVER_SCHOFIELD,
	WEAPON_PISTOL_VOLCANIC
};

const Hash ArmorTierList[ArmorTierCount] = {
	ARMOR_LEVEL_1,
	ARMOR_LEVEL_2,
	ARMOR_LEVEL_3,
};

const Hash HorseTierList[HorseTierCount] = {
	HORSE_WALKER,
	HORSE_SADDLER,
	HORSE_ARDENNES,
	HORSE_NOKOTA,
	HORSE_MUSTANG,
	HORSE_ARABIAN,
	HORSE_FOXTROTTER
};

namespace Common
{
	std::string Format(const char* fmt, ...)
	{
		char buf[2048] = {};
		va_list va_alist;
		va_start(va_alist, fmt);
		vsnprintf(buf, _countof(buf), fmt, va_alist);
		va_end(va_alist);
		return std::string(buf);
	}

	std::string HashToStr(Hash h)
	{
		const char* name = "";
		char buf[10] = {};

		switch (h)
		{
			case 0: name = ""; break;
			
			// Weapons
			case WEAPON_BOW: name = "WEAPON_BOW"; break;
			case WEAPON_KIT_BINOCULARS: name = "WEAPON_KIT_BINOCULARS"; break;
			case WEAPON_LASSO: name = "WEAPON_LASSO"; break;
			case WEAPON_MELEE_CLEAVER: name = "WEAPON_MELEE_CLEAVER"; break;
			case WEAPON_MELEE_KNIFE: name = "WEAPON_MELEE_KNIFE"; break;
			case WEAPON_MELEE_LANTERN: name = "WEAPON_MELEE_LANTERN"; break;
			case WEAPON_MELEE_MACHETE: name = "WEAPON_MELEE_MACHETE"; break;
			case WEAPON_MELEE_TORCH: name = "WEAPON_MELEE_TORCH"; break;
			case WEAPON_MOONSHINEJUG: name = "WEAPON_MOONSHINEJUG"; break;
			case WEAPON_PISTOL_M1899: name = "WEAPON_PISTOL_M1899"; break;
			case WEAPON_PISTOL_MAUSER: name = "WEAPON_PISTOL_MAUSER"; break;
			case WEAPON_PISTOL_MAUSER_DRUNK: name = "WEAPON_PISTOL_MAUSER_DRUNK"; break;
			case WEAPON_PISTOL_SEMIAUTO: name = "WEAPON_PISTOL_SEMIAUTO"; break;
			case WEAPON_PISTOL_VOLCANIC: name = "WEAPON_PISTOL_VOLCANIC"; break;
			case WEAPON_REPEATER_CARBINE: name = "WEAPON_REPEATER_CARBINE"; break;
			case WEAPON_REPEATER_EVANS: name = "WEAPON_REPEATER_EVANS"; break;
			case WEAPON_REPEATER_HENRY: name = "WEAPON_REPEATER_HENRY"; break;
			case WEAPON_REPEATER_LANCASTER: name = "WEAPON_REPEATER_LANCASTER"; break;
			case WEAPON_REPEATER_WINCHESTER: name = "WEAPON_REPEATER_WINCHESTER"; break;
			case WEAPON_REVOLVER_CATTLEMAN: name = "WEAPON_REVOLVER_CATTLEMAN"; break;
			case WEAPON_REVOLVER_CATTLEMAN_DUALWIELD: name = "WEAPON_REVOLVER_CATTLEMAN_DUALWIELD"; break;
			case WEAPON_REVOLVER_CATTLEMAN_HOSEA: name = "WEAPON_REVOLVER_CATTLEMAN_HOSEA"; break;
			case WEAPON_REVOLVER_CATTLEMAN_JOHN: name = "WEAPON_REVOLVER_CATTLEMAN_JOHN"; break;
			case WEAPON_REVOLVER_CATTLEMAN_MEXICAN: name = "WEAPON_REVOLVER_CATTLEMAN_MEXICAN"; break;
			case WEAPON_REVOLVER_CATTLEMAN_PIG: name = "WEAPON_REVOLVER_CATTLEMAN_PIG"; break;
			case WEAPON_REVOLVER_DOUBLEACTION: name = "WEAPON_REVOLVER_DOUBLEACTION"; break;
			case WEAPON_REVOLVER_DOUBLEACTION_DUALWIELD: name = "WEAPON_REVOLVER_DOUBLEACTION_DUALWIELD"; break;
			case WEAPON_REVOLVER_DOUBLEACTION_EXOTIC: name = "WEAPON_REVOLVER_DOUBLEACTION_EXOTIC"; break;
			case WEAPON_REVOLVER_DOUBLEACTION_GAMBLER: name = "WEAPON_REVOLVER_DOUBLEACTION_GAMBLER"; break;
			case WEAPON_REVOLVER_DOUBLEACTION_MICAH: name = "WEAPON_REVOLVER_DOUBLEACTION_MICAH"; break;
			case WEAPON_REVOLVER_LEMAT: name = "WEAPON_REVOLVER_LEMAT"; break;
			case WEAPON_REVOLVER_SCHOFIELD: name = "WEAPON_REVOLVER_SCHOFIELD"; break;
			case WEAPON_RIFLE_BOLTACTION: name = "WEAPON_RIFLE_BOLTACTION"; break;
			case WEAPON_RIFLE_SPRINGFIELD: name = "WEAPON_RIFLE_SPRINGFIELD"; break;
			case WEAPON_RIFLE_VARMINT: name = "WEAPON_RIFLE_VARMINT"; break;
			case WEAPON_SHOTGUN_DOUBLEBARREL: name = "WEAPON_SHOTGUN_DOUBLEBARREL"; break;
			case WEAPON_SHOTGUN_DOUBLEBARREL_EXOTIC: name = "WEAPON_SHOTGUN_DOUBLEBARREL_EXOTIC"; break;
			case WEAPON_SHOTGUN_FOLDING: name = "WEAPON_SHOTGUN_FOLDING"; break;
			case WEAPON_SHOTGUN_PUMP: name = "WEAPON_SHOTGUN_PUMP"; break;
			case WEAPON_SHOTGUN_REPEATING: name = "WEAPON_SHOTGUN_REPEATING"; break;
			case WEAPON_SHOTGUN_SAWEDOFF: name = "WEAPON_SHOTGUN_SAWEDOFF"; break;
			case WEAPON_SHOTGUN_SEMIAUTO: name = "WEAPON_SHOTGUN_SEMIAUTO"; break;
			case WEAPON_SNIPERRIFLE_CARCANO: name = "WEAPON_SNIPERRIFLE_CARCANO"; break;
			case WEAPON_SNIPERRIFLE_ROLLINGBLOCK: name = "WEAPON_SNIPERRIFLE_ROLLINGBLOCK"; break;
			case WEAPON_SNIPERRIFLE_ROLLINGBLOCK_EXOTIC: name = "WEAPON_SNIPERRIFLE_ROLLINGBLOCK_EXOTIC"; break;
			case WEAPON_UNARMED: name = "WEAPON_UNARMED"; break;

			// Armor
			case ARMOR_LEVEL_0: name = "ARMOR_LEVEL_0"; break;
			case ARMOR_LEVEL_1: name = "ARMOR_LEVEL_1"; break;
			case ARMOR_LEVEL_2: name = "ARMOR_LEVEL_2"; break;
			case ARMOR_LEVEL_3: name = "ARMOR_LEVEL_3"; break;

			// Horses
			case HORSE_MORGAN: name = "HORSE_MORGAN"; break;
			case HORSE_WALKER: name = "HORSE_WALKER"; break;
			case HORSE_SADDLER: name = "HORSE_SADDLER"; break;
			case HORSE_ARDENNES: name = "HORSE_ARDENNES"; break;
			case HORSE_NOKOTA: name = "HORSE_NOKOTA"; break;
			case HORSE_MUSTANG: name = "HORSE_MUSTANG"; break;
			case HORSE_ARABIAN: name = "HORSE_ARABIAN"; break;
			case HORSE_FOXTROTTER: name = "HORSE_FOXTROTTER"; break;

			// Relationships
			case REL_IGNORE_EVERYTHING: name = "REL_IGNORE_EVERYTHING"; break;
			case REL_GANG_DUTCHS: name = "REL_GANG_DUTCHS"; break;
			case REL_PLAYER: name = "REL_PLAYER"; break;
			case REL_PLAYER_ALLY: name = "REL_PLAYER_ALLY"; break;
			case REL_PLAYER_ENEMY: name = "REL_PLAYER_ENEMY"; break;
			case REL_COP: name = "REL_COP"; break;
			case REL_BOUNTY_HUNTER: name = "REL_BOUNTY_HUNTER"; break;
			case REL_CIV: name = "REL_CIV"; break;

			default:
				snprintf(buf, _countof(buf), "0x%X", h);
				name = buf;
		}

		return std::string(name);
	}

	void SetEntityProofs(Entity entity, bool fire, bool explosion)
	{
		if (fire)
		{
			ENTITY::SET_ENTITY_PROOFS(entity, 2, false);
		}

		if (explosion)
		{
			ENTITY::SET_ENTITY_PROOFS(entity, 4, false);
		}
	}

	void SetDoorState(Hash door, int state)
	{
		if (!OBJECT::IS_DOOR_REGISTERED_WITH_SYSTEM(door))
		{
			OBJECT::_ADD_DOOR_TO_SYSTEM_NEW(door, true, false, false, 0, 0, false);
		}

		if (OBJECT::DOOR_SYSTEM_GET_DOOR_STATE(door) == state)
		{
			return;
		}

		if (state > DOORSTATE_UNLOCKED)
		{
			OBJECT::_DOOR_SYSTEM_SET_ABLE_TO_CHANGE_OPEN_RATIO_WHILE_LOCKED(door, true);
			OBJECT::DOOR_SYSTEM_SET_OPEN_RATIO(door, 0, true);
		}
		
		OBJECT::DOOR_SYSTEM_SET_DOOR_STATE(door, state);
	}

	int GetDistrictFromCoords(Vector3 coords)
	{
		Hash zone = ZONE::_GET_MAP_ZONE_AT_COORDS(coords, 10);
		
		switch (zone)
		{
			case 0x78BFE1AC: return DISTRICT_BAYOU_NWA;
			case 0x3108C492: return DISTRICT_BIG_VALLEY;
			case 0x4DFA0B50: return DISTRICT_BLUEGILL_MARSH;
			case 0x6D67801E: return DISTRICT_CUMBERLAND_FOREST;
			case 0x1C68EA97: return DISTRICT_GREAT_PLAINS;
			case 0xF8D68DC1: return DISTRICT_GRIZZLIES_EAST;
			case 0x62162401: return DISTRICT_GRIZZLIES_WEST;
			case 0xE1736CD7: return DISTRICT_GUAMA;
			case 0x07D4FF5F: return DISTRICT_HEARTLAND;
			case 0x0AA5F25D: return DISTRICT_ROANOKE_RIDGE;
			case 0xCC7C3314: return DISTRICT_SCARLETT_MEADOWS;
			case 0x6467EF09: return DISTRICT_TALL_TREES;
			case 0x84D7AD0E: return DISTRICT_GAPTOOTH_RIDGE;
			case 0x8016C23F: return DISTRICT_RIO_BRAVO;
			case 0xF9831C72: return DISTRICT_CHOLLA_SPRINGS;
			case 0x35390B10: return DISTRICT_HENNIGANS_STEAD;
		}
		
		DioTrace(DIOTAG1("ERROR") "INVALID DISTRICT HASH 0x%X", zone);
		return DISTRICT_INVALID;
	}

	void ClearArea(const Vector3& area, float radius)
	{
		int iVar1 = 344970;
		iVar1 |= 1 | 1048576 | 524288 | 24 | 40;

		MISC::CLEAR_AREA(area.x, area.y, area.z, radius, iVar1);

		MISC::CLEAR_AREA(area.x, area.y, area.z, radius, 2097152);

		PERSISTENCE::PERSISTENCE_REMOVE_ALL_ENTITIES_IN_AREA(area.x, area.y, area.z, radius);
	}
	
	Vector3 ApplyRandomSmallOffsetToCoords(Vector3 v, float multiplier)
	{
		v.x += ((((((rand() % 10) + 5) * 0.1f) - 1) * 2.5f) * multiplier);
		v.y += ((((((rand() % 10) + 5) * 0.1f) - 1) * 2.5f) * multiplier);
		v.z += 2;

		return v;
	}

	float GetDistBetweenCoords(const Vector3& c1, const Vector3& c2)
	{
		return BUILTIN::VDIST(c1.x, c1.y, c1.z, c2.x, c2.y, c2.z);
	}

	void DrawDebugCircle(const Vector3& position, float radius, float height, const std::string& color, bool bob, bool upsidedown)
	{
#ifdef DEBUG_MARKERS
		Vector3 pos = position;
		Vector3 dir = {0, 0, 0};
		Vector3 rot = {0, 0, 0};

		if (upsidedown)
		{
			rot.x = 180;
		}

		float scale = WorldFloatToMarkerScale(radius);

		Vector3 scl = {scale, scale, WorldFloatToMarkerScale(height)};

		int iVar0, iVar1, iVar2, bVar3;
		HUD::_GET_COLOR_FROM_NAME(MISC::GET_HASH_KEY(color.c_str()), &iVar0, &iVar1, &iVar2, &bVar3);

		GRAPHICS::_DRAW_MARKER(MARKERTYPE_CYLINDER, pos.x, pos.y, pos.z, dir.x, dir.y, dir.z, rot.x, rot.y, rot.z, scl.x, scl.y, scl.z, iVar0, iVar1, iVar2, bVar3, bob, false, 2, false, 0, 0, false);
#endif
	}

	void DrawDebugRectangle(const Vector3& position, const Vector3& rotation, const Vector3& scale, const std::string& color, bool bob, bool rotate)
	{
#ifdef DEBUG_MARKERS
		Vector3 pos = position;
		Vector3 dir = {0, 0, 0};
		Vector3 rot = rotation;

		Vector3 scl = {WorldFloatToMarkerScale(scale.x), WorldFloatToMarkerScale(scale.y), WorldFloatToMarkerScale(scale.z)};

		int iVar0, iVar1, iVar2, bVar3;
		HUD::_GET_COLOR_FROM_NAME(MISC::GET_HASH_KEY(color.c_str()), &iVar0, &iVar1, &iVar2, &bVar3);

		GRAPHICS::_DRAW_MARKER(MARKERTYPE_CUBE, pos.x, pos.y, pos.z, dir.x, dir.y, dir.z, rot.x, rot.y, rot.z, scl.x, scl.y, scl.z, iVar0, iVar1, iVar2, bVar3, bob, false, 2, rotate, 0, 0, false);
#endif
	}
}

std::vector<std::string> DutchGangBlock;

namespace INI
{
	std::string InputKeyToStr(int key)
	{
		std::string str;

		switch (key)
		{
			case VK_LBUTTON: str = "LMB"; break;
			case VK_RBUTTON: str = "RMB"; break;
			case VK_BACK: str = "BACKSPACE"; break;
			case VK_TAB: str = "TAB"; break;
			case VK_CLEAR: str = "CLEAR"; break;
			case VK_RETURN: str = "ENTER"; break;
			case VK_SHIFT: str = "SHIFT"; break;
			case VK_CONTROL: str = "CTRL"; break;
			case VK_MENU: str = "ALT"; break;
			case VK_PAUSE: str = "PAUSE"; break;
			case VK_CAPITAL: str = "CAPS LOCK"; break;
			case VK_ESCAPE: str = "ESC"; break;
			case VK_SPACE: str = "SPACEBAR"; break;
			case VK_PRIOR: str = "PAGE UP"; break;
			case VK_NEXT: str = "PAGE DOWN"; break;
			case VK_END: str = "END"; break;
			case VK_HOME: str = "HOME"; break;
			case VK_LEFT: str = "LEFT ARROW"; break;
			case VK_UP: str = "UP ARROW"; break;
			case VK_RIGHT: str = "RIGHT ARROW"; break;
			case VK_DOWN: str = "DOWN ARROW"; break;
			case VK_SELECT: str = "SELECT"; break;
			case VK_PRINT: str = "PRINT"; break;
			case VK_EXECUTE: str = "EXECUTE"; break;
			case VK_SNAPSHOT: str = "PRINT SCREEN"; break;
			case VK_INSERT: str = "INS"; break;
			case VK_DELETE: str = "DEL"; break;
			case VK_HELP: str = "HELP"; break;
			case 0x30: str = "0"; break;
			case 0x31: str = "1"; break;
			case 0x32: str = "2"; break;
			case 0x33: str = "3"; break;
			case 0x34: str = "4"; break;
			case 0x35: str = "5"; break;
			case 0x36: str = "6"; break;
			case 0x37: str = "7"; break;
			case 0x38: str = "8"; break;
			case 0x39: str = "9"; break;
			case 0x41: str = "A"; break;
			case 0x42: str = "B"; break;
			case 0x43: str = "C"; break;
			case 0x44: str = "D"; break;
			case 0x45: str = "E"; break;
			case 0x46: str = "F"; break;
			case 0x47: str = "G"; break;
			case 0x48: str = "H"; break;
			case 0x49: str = "I"; break;
			case 0x4A: str = "J"; break;
			case 0x4B: str = "K"; break;
			case 0x4C: str = "L"; break;
			case 0x4D: str = "M"; break;
			case 0x4E: str = "N"; break;
			case 0x4F: str = "O"; break;
			case 0x50: str = "P"; break;
			case 0x51: str = "Q"; break;
			case 0x52: str = "R"; break;
			case 0x53: str = "S"; break;
			case 0x54: str = "T"; break;
			case 0x55: str = "U"; break;
			case 0x56: str = "V"; break;
			case 0x57: str = "W"; break;
			case 0x58: str = "X"; break;
			case 0x59: str = "Y"; break;
			case 0x5A: str = "Z"; break;
			case VK_NUMPAD0: str = "Numpad 0"; break;
			case VK_NUMPAD1: str = "Numpad 1"; break;
			case VK_NUMPAD2: str = "Numpad 2"; break;
			case VK_NUMPAD3: str = "Numpad 3"; break;
			case VK_NUMPAD4: str = "Numpad 4"; break;
			case VK_NUMPAD5: str = "Numpad 5"; break;
			case VK_NUMPAD6: str = "Numpad 6"; break;
			case VK_NUMPAD7: str = "Numpad 7"; break;
			case VK_NUMPAD8: str = "Numpad 8"; break;
			case VK_NUMPAD9: str = "Numpad 9"; break;
			case VK_MULTIPLY: str = "Multiply"; break;
			case VK_ADD: str = "Add"; break;
			case VK_SEPARATOR: str = "Separator"; break;
			case VK_SUBTRACT: str = "Subtract"; break;
			case VK_DECIMAL: str = "Decimal"; break;
			case VK_DIVIDE: str = "Divide"; break;
			case VK_F1: str = "F1"; break;
			case VK_F2: str = "F2"; break;
			case VK_F3: str = "F3"; break;
			case VK_F4: str = "F4"; break;
			case VK_F5: str = "F5"; break;
			case VK_F6: str = "F6"; break;
			case VK_F7: str = "F7"; break;
			case VK_F8: str = "F8"; break;
			case VK_F9: str = "F9"; break;
			case VK_F10: str = "F10"; break;
			case VK_F11: str = "F11"; break;
			case VK_F12: str = "F12"; break;
			case VK_F13: str = "F13"; break;
			case VK_F14: str = "F14"; break;
			case VK_F15: str = "F15"; break;
			case VK_F16: str = "F16"; break;
			case VK_F17: str = "F17"; break;
			case VK_F18: str = "F18"; break;
			case VK_F19: str = "F19"; break;
			case VK_F20: str = "F20"; break;
			case VK_F21: str = "F21"; break;
			case VK_F22: str = "F22"; break;
			case VK_F23: str = "F23"; break;
			case VK_F24: str = "F24"; break;
			case VK_NUMLOCK: str = "NUM LOCK"; break;
			case VK_SCROLL: str = "SCROLL LOCK"; break;
			case VK_LSHIFT: str = "Left SHIFT"; break;
			case VK_RSHIFT: str = "Right SHIFT"; break;
			case VK_LCONTROL: str = "Left CONTROL"; break;
			case VK_RCONTROL: str = "Right CONTROL"; break;
			case VK_LMENU: str = "Left MENU"; break;
			case VK_RMENU: str = "Right MENU"; break;
			case VK_OEM_1: str = "Semicolon"; break;
			case VK_OEM_PLUS: str = "Plus"; break;
			case VK_OEM_COMMA: str = "Comma"; break;
			case VK_OEM_MINUS: str = "Minus"; break;
			case VK_OEM_PERIOD: str = "Period"; break;
			case VK_OEM_2: str = "Question Mark"; break;
			case VK_OEM_3: str = "Tilde"; break;
			case VK_OEM_4: str = "Left Bracket"; break;
			case VK_OEM_5: str = "Backslash"; break;
			case VK_OEM_6: str = "Right Bracket"; break;
			case VK_OEM_7: str = "Quote"; break;

			default:
				DioTrace(DIOTAG1("WARNING") "Invalid key 0x%X", key);
				str = "[INVALID KEY]";
		}
		
		return str;
	}

	bool DoesIniExist()
	{
		std::ifstream accessTest("GangMod.ini");

		if (!accessTest.is_open())
		{
			DioTrace(DIOTAG1("ERROR") "COULD NOT OPEN INI");
			return false;
		}

		accessTest.close();

		return true;
	}

	int GetCompanionControlKey()
	{
		if (!DoesIniExist())
		{
			DioTrace(DIOTAG1("WARNING") "Cannot access ini, returning default value VK_NUMPAD5");
			return VK_NUMPAD5;
		}
		
		std::ifstream ini("GangMod.ini", std::ios::in);
		std::string line;

		while (std::getline(ini, line))
		{
			if (line == "[CompanionControlKey]")
			{
				std::getline(ini, line);

				line = line.substr(line.find("0x") + 2);

				int key;
				std::stringstream ss;
				ss << std::hex << line;
				ss >> key;

				ini.close();

				return key;
			}
		}
		
		ini.close();

		DioTrace(DIOTAG1("WARNING") "Could not find entry, returning default value VK_NUMPAD5");
		return VK_NUMPAD5;
	}

	std::vector<std::string> GetDutchGangBlock()
	{
		return DutchGangBlock;
	}

	void SetDutchGangBlock()
	{
		DutchGangBlock.clear();
		
		if (!DoesIniExist())
		{
			DioTrace(DIOTAG1("WARNING") "Cannot access ini, setting empty");
			return;
		}

		std::ifstream ini("GangMod.ini", std::ios::in);
		std::string line;

		while (std::getline(ini, line))
		{
			if (line == "[VDLGangSetup]")
			{
				while (std::getline(ini, line))
				{
					std::string person;
					std::string status;
					
					if (line.empty())
					{
						continue;
					}
					else if (line.substr(0, 1) == "[")
					{
						ListDutchGangBlock();

						ini.close();
						return;
					}
					else if (line.find("=") == std::string::npos)
					{
						continue;
					}
					else
					{
						for (int i = 0; i < line.size(); ++i)
						{
							line[i] = toupper(line[i]);
						}
						
						person = line.substr(0, line.find("="));
						status = line.substr(line.find("=") + 1);

						if (DUTCH_HELPERS::StringValid(person) && DUTCH_HELPERS::StringValid(status) && status != "UNUSED")
						{
							DutchGangBlock.emplace_back(line);
						}
					}
				}

				ini.close();
				return;
			}
		}

		ini.close();
		 
		DioTrace(DIOTAG1("WARNING") "Could not find entry, setting empty");
	}

	void ListDutchGangBlock()
	{
#ifdef DEBUG_EXTRA_TRACE
		
		DioScope(DIOTAG);

		for (const std::string& line : DutchGangBlock)
		{
			DioTrace("%s", line.c_str());
		}
#endif
	}

	std::string GetCustomGangFixedPed(int fPedIndex)
	{
		if (!DoesIniExist())
		{
			DioTrace(DIOTAG1("WARNING") "Cannot access ini, returning default ped");
			return DEFAULT_PED;
		}

		std::string MANAGER = DEFAULT_PED;
		std::string ARMORY = DEFAULT_PED;
		std::string HORSE = DEFAULT_PED;
		std::string MISSION = DEFAULT_PED;

		std::ifstream ini("GangMod.ini", std::ios::in);
		std::string line;

		while (std::getline(ini, line))
		{
			if (line == "[CustomGangSetup]")
			{
				while (std::getline(ini, line))
				{
					std::string status;
					std::string ped;

					if (line.empty() || line.find("=") == std::string::npos)
					{
						continue;
					}
					else
					{
						status = line.substr(0, line.find("="));
						ped = line.substr(line.find("=") + 1);

						if (ped.empty())
						{
							ped = DEFAULT_PED;
						}

						if (!STREAMING::IS_MODEL_VALID(MISC::GET_HASH_KEY(ped.c_str())))
						{
							DioTrace(DIOTAG1("WARNING") "Invalid ped %s", line.c_str());
							ped = DEFAULT_PED;
						}

						if (status == "CAMP_MANAGER")
						{
							MANAGER = ped;
						}
						else if (status == "CAMP_ARMORY")
						{
							ARMORY = ped;
						}
						else if (status == "CAMP_HORSE")
						{
							HORSE = ped;
						}
						else if (status == "CAMP_MISSION")
						{
							MISSION = ped;
						}
					}
				}
			}
		}

		ini.close();

		switch (fPedIndex)
		{
			case FPED_MANAGER: return MANAGER;
			case FPED_ARMORY: return ARMORY;
			case FPED_HORSE: return HORSE;
			case FPED_MISSION: return MISSION;
			
			default:
				DioTrace(DIOTAG1("ERROR") "BAD FIXED PED INDEX %d", fPedIndex);
		}

		return DEFAULT_PED;
	}

	std::vector<std::string> GetCustomGangGenericPeds()
	{
		if (!DoesIniExist())
		{
			DioTrace(DIOTAG1("WARNING") "Cannot access ini, returning default ped");
			return {DEFAULT_PED};
		}

		std::vector<std::string> peds;

		std::ifstream ini("GangMod.ini", std::ios::in);
		std::string line;

		while (std::getline(ini, line))
		{
			if (line == "AMBIENT_MEMBERS=")
			{
				while (std::getline(ini, line))
				{
					if (line.empty())
					{
						continue;
					}
					else
					{
						if (STREAMING::IS_MODEL_VALID(MISC::GET_HASH_KEY(line.c_str())))
						{
							peds.emplace_back(line);
						}
						else
						{
							DioTrace(DIOTAG1("WARNING") "Invalid ped %s", line.c_str());
						}
					}
				}

				ini.close();
				return peds;
			}
		}

		ini.close();

		DioTrace(DIOTAG1("WARNING") "Could not find entry, returning default ped");
		return {DEFAULT_PED};
	}
}

namespace DUTCH_HELPERS
{
	std::string GetFixedPed(int fpedIndex)
	{
		std::string MANAGER = "CS_dutch";
		std::string ARMORY = "CS_MicahBell";
		std::string HORSE = "CS_kieran";
		std::string MISSION = "CS_hoseamatthews";
		
		if (!DutchGangBlock.empty())
		{
			for (const std::string& line : DutchGangBlock)
			{
				std::string person = line.substr(0, line.find("="));
				std::string status = line.substr(line.find("=") + 1);

				if (status == "CAMP_MANAGER")
				{
					MANAGER = RenameFriendlyToPed(person);
				}
				else if (status == "CAMP_ARMORY")
				{
					ARMORY = RenameFriendlyToPed(person);
				}
				else if (status == "CAMP_HORSE")
				{
					HORSE = RenameFriendlyToPed(person);
				}
				else if (status == "CAMP_MISSION")
				{
					MISSION = RenameFriendlyToPed(person);
				}
			}
		}

		switch (fpedIndex)
		{
			case FPED_MANAGER: return MANAGER;
			case FPED_ARMORY: return ARMORY;
			case FPED_HORSE: return HORSE;
			case FPED_MISSION: return MISSION;
			
			default:
				DioTrace(DIOTAG1("ERROR") "BAD FIXED PED INDEX %d", fpedIndex);
		}

		return "PLAYER_ZERO";
	}

	std::vector<std::string> GetAmbientPeds()
	{
		if (DutchGangBlock.empty())
		{
			return {};
		}

		std::vector<std::string> peds;

		for (const std::string& line : DutchGangBlock)
		{
			std::string person = line.substr(0, line.find("="));
			std::string status = line.substr(line.find("=") + 1);

			if (status == "GUARD" || status == "AMBIENT")
			{
				peds.emplace_back(RenameFriendlyToPed(person));
			}
		}

		return peds;
	}

	std::vector<std::string> GetHeistPeds()
	{
		if (DutchGangBlock.empty())
		{
			return {};
		}

		std::vector<std::string> peds;

		for (const std::string& line : DutchGangBlock)
		{
			std::string person = line.substr(0, line.find("="));
			std::string status = line.substr(line.find("=") + 1);

			if (status == "GUARD")
			{
				peds.emplace_back(RenameFriendlyToPed(person));
			}
		}

		if (peds.empty())
		{
			peds = {"CS_JOHNMARSTON", "CS_CHARLESSMITH"};
		}

		return peds;
	}

	void SetPedAppearance(Ped p, std::string pedName, bool snow, bool robbery)
	{
		if (robbery)
		{
			snow = false;
		}
		
		PED::_SET_PED_BODY_COMPONENT(p, snow ? MISC::GET_HASH_KEY("META_OUTFIT_COLD_WEATHER") : MISC::GET_HASH_KEY("META_OUTFIT_WARM_WEATHER"));

		for (int i = 0; i < pedName.size(); ++i)
		{
			pedName[i] = toupper(pedName[i]);
		}

		if (pedName == "PLAYER_ZERO")
		{
			PED::_SET_PED_BODY_COMPONENT(p, snow ? MISC::GET_HASH_KEY("META_OUTFIT_COLD_WEATHER") : MISC::GET_HASH_KEY("META_OUTFIT_COOL_WEATHER"));
		}

		if (pedName == "CS_JOHNMARSTON")
		{
			PED::_SET_PED_BODY_COMPONENT(p, snow ? MISC::GET_HASH_KEY("META_OUTFIT_COOL_WEATHER") : MISC::GET_HASH_KEY("META_OUTFIT_WARM_WEATHER"));
		}

		if (pedName == "CS_SEAN")
		{
			PED::_SET_PED_BODY_COMPONENT(p, snow ? 0xEBC710E7 : MISC::GET_HASH_KEY("META_OUTFIT_WARM_WEATHER"));
		}

		if (pedName == "CS_CLEET" || pedName == "CS_JOE")
		{
			PED::_SET_PED_BODY_COMPONENT(p, snow ? 0x64BA68C8 : MISC::GET_HASH_KEY("META_OUTFIT_DEFAULT"));
		}

		if (pedName == "CS_JOSIAHTRELAWNY")
		{
			PED::_SET_PED_BODY_COMPONENT(p, snow ? 0x3EB5190A : MISC::GET_HASH_KEY("META_OUTFIT_WARM_WEATHER"));
		}

		if (pedName == "CS_KAREN")
		{
			PED::_SET_PED_BODY_COMPONENT(p, snow ? MISC::GET_HASH_KEY("META_OUTFIT_COLD_WEATHER") : 0x14443E7E);
		}

		if (robbery)
		{
			PED::_SET_PED_BODY_COMPONENT(p, 0xBE61FA14);
		}
	}

	std::string RenameFriendlyToPed(const std::string& str)
	{
		if (str == "ARTHUR")
		{
			return "PLAYER_ZERO";
		}
		if (str == "JOHN")
		{
			return "CS_JOHNMARSTON";
		}
		if (str == "DUTCH")
		{
			return "CS_DUTCH";
		}
		if (str == "HOSEA")
		{
			return "CS_HOSEAMATTHEWS";
		}
		if (str == "BILL")
		{
			return "CS_BILLWILLIAMSON";
		}
		if (str == "CHARLES")
		{
			return "CS_CHARLESSMITH";
		}
		if (str == "JAVIER")
		{
			return "CS_JAVIERESCUELLA";
		}
		if (str == "SEAN")
		{
			return "CS_SEAN";
		}
		if (str == "LENNY")
		{
			return "CS_LENNY";
		}
		if (str == "KIERAN")
		{
			return "CS_KIERAN";
		}
		if (str == "MICAH")
		{
			return "CS_MICAHBELL";
		}
		if (str == "CLEET")
		{
			return "CS_CLEET";
		}
		if (str == "JOE")
		{
			return "CS_JOE";
		}
		if (str == "PEARSON")
		{
			return "CS_MRPEARSON";
		}
		if (str == "UNCLE")
		{
			return "CS_UNCLE";
		}
		if (str == "STRAUSS")
		{
			return "CS_LEOSTRAUSS";
		}
		if (str == "TRELAWNY")
		{
			return "CS_JOSIAHTRELAWNY";
		}
		if (str == "SWANSON")
		{
			return "CS_REVSWANSON";
		}
		if (str == "KAREN")
		{
			return "CS_KAREN";
		}
		if (str == "MARYBETH")
		{
			return "CS_MARYBETH";
		}
		if (str == "TILLY")
		{
			return "CS_TILLY";
		}
		if (str == "MOLLY")
		{
			return "CS_MOLLYOSHEA";
		}
		if (str == "SADIE")
		{
			return "CS_MRSADLER";
		}
		if (str == "GRIMSHAW")
		{
			return "CS_SUSANGRIMSHAW";
		}
		if (str == "ABIGAIL")
		{
			return "CS_ABIGAILROBERTS";
		}

		DioTrace(DIOTAG1("ERROR") "CANNOT CONVERT %s", str.c_str());
		return "";
	}

	std::string RenamePedToFriendly(const std::string& str)
	{
		if (str == "PLAYER_ZERO")
		{
			return "ARTHUR";
		}
		if (str == "CS_JOHNMARSTON")
		{
			return "JOHN";
		}
		if (str == "CS_DUTCH")
		{
			return "DUTCH";
		}
		if (str == "CS_HOSEAMATTHEWS")
		{
			return "HOSEA";
		}
		if (str == "CS_BILLWILLIAMSON")
		{
			return "BILL";
		}
		if (str == "CS_CHARLESSMITH")
		{
			return "CHARLES";
		}
		if (str == "CS_JAVIERESCUELLA")
		{
			return "JAVIER";
		}
		if (str == "CS_SEAN")
		{
			return "SEAN";
		}
		if (str == "CS_LENNY")
		{
			return "LENNY";
		}
		if (str == "CS_KIERAN")
		{
			return "KIERAN";
		}
		if (str == "CS_MICAHBELL")
		{
			return "MICAH";
		}
		if (str == "CS_CLEET")
		{
			return "CLEET";
		}
		if (str == "CS_JOE")
		{
			return "JOE";
		}
		if (str == "CS_MRPEARSON")
		{
			return "PEARSON";
		}
		if (str == "CS_UNCLE")
		{
			return "UNCLE";
		}
		if (str == "CS_LEOSTRAUSS")
		{
			return "STRAUSS";
		}
		if (str == "CS_JOSIAHTRELAWNY")
		{
			return "TRELAWNY";
		}
		if (str == "CS_REVSWANSON")
		{
			return "SWANSON";
		}
		if (str == "CS_KAREN")
		{
			return "KAREN";
		}
		if (str == "CS_MARYBETH")
		{
			return "MARYBETH";
		}
		if (str == "CS_TILLY")
		{
			return "TILLY";
		}
		if (str == "CS_MOLLYOSHEA")
		{
			return "MOLLY";
		}
		if (str == "CS_MRSADLER")
		{
			return "SADIE";
		}
		if (str == "CS_SUSANGRIMSHAW")
		{
			return "GRIMSHAW";
		}
		if (str == "CS_ABIGAILROBERTS")
		{
			return "ABIGAIL";
		}

		DioTrace(DIOTAG1("ERROR") "CANNOT CONVERT %s", str.c_str());
		return "";
	}

	bool StringValid(const std::string& str)
	{
		return (
			str == "ARTHUR" ||
			str == "PLAYER_ZERO" ||
			str == "JOHN" ||
			str == "CS_JOHNMARSTON" ||
			str == "DUTCH" ||
			str == "CS_DUTCH" ||
			str == "HOSEA" ||
			str == "CS_HOSEAMATTHEWS" ||
			str == "BILL" ||
			str == "CS_BILLWILLIAMSON" ||
			str == "CHARLES" ||
			str == "CS_CHARLESSMITH" ||
			str == "JAVIER" ||
			str == "CS_JAVIERESCUELLA" ||
			str == "SEAN" ||
			str == "CS_SEAN" ||
			str == "LENNY" ||
			str == "CS_LENNY" ||
			str == "KIERAN" ||
			str == "CS_KIERAN" ||
			str == "MICAH" ||
			str == "CS_MICAHBELL" ||
			str == "CLEET" ||
			str == "CS_CLEET" ||
			str == "JOE" ||
			str == "CS_JOE" ||
			str == "PEARSON" ||
			str == "CS_MRPEARSON" ||
			str == "UNCLE" ||
			str == "CS_UNCLE" ||
			str == "STRAUSS" ||
			str == "CS_LEOSTRAUSS" ||
			str == "TRELAWNY" ||
			str == "CS_JOSIAHTRELAWNY" ||
			str == "SWANSON" ||
			str == "CS_REVSWANSON" ||
			str == "KAREN" ||
			str == "CS_KAREN" ||
			str == "MARYBETH" ||
			str == "CS_MARYBETH" ||
			str == "TILLY" ||
			str == "CS_TILLY" ||
			str == "MOLLY" ||
			str == "CS_MOLLYOSHEA" ||
			str == "SADIE" ||
			str == "CS_MRSADLER" ||
			str == "GRIMSHAW" ||
			str == "CS_SUSANGRIMSHAW" ||
			str == "ABIGAIL" ||
			str == "CS_ABIGAILROBERTS" ||
			str == "CAMP_MANAGER" ||
			str == "CAMP_MISSION" ||
			str == "CAMP_HORSE" ||
			str == "CAMP_ARMORY" ||
			str == "GUARD" ||
			str == "AMBIENT" ||
			str == "UNUSED"
		);
	}

	std::string ConvertCommentString(const std::string& str)
	{
		if (str == "GENERIC_CURSE_HIGH")
		{
			return "GENERIC_CURSE_HIGH";
		}

		if (str == "GANG_CALLOUT_LAW_SPOTTED")
		{
			return "SCOUT_SPOTTED";
		}

		if (str == "WON_FIGHT")
		{
			return "POST_WANTED_TRANSITION";
		}

		if (str == "HUNKER_DOWN")
		{
			return "THERES_MORE";
		}

		if (str == "ARRIVAL_COMBAT_NEUTRAL" ||
			str == "ARRIVAL_COMBAT_RAID_MALE" ||
			str == "ARRIVAL_COMBAT_RAID_NEUTRAL" ||
			str == "CALLOUT_JUST_KILL_EM_NEUTRAL" ||
			str == "CALLOUT_JUST_KILL_EM_MALE" ||
			str == "CALLOUT_EASY_PICKINGS_NEUTRAL" ||
			str == "OPENS_FIRE" ||
			str == "TAUNT_GEN_NEUTRAL" ||
			str == "TAUNT_GEN_GROUP")
		{
			return "ALLY_TAUNT_ENEMY";
		}

		return "";
	}
}
