#pragma once

#include "Common.h"
#include "MissionBase.h"

class TestMission : public MissionBase
{
	bool SetMissionParamsForCamp(int camp) override;

	// Location Specific Variables
	Vector3 DutchCoords = {0, 0 ,0};
	Vector3 HideCoords = {0, 0 ,0};
	Vector3 EnemySpawn1 = {0, 0 ,0};
	float EnemyHeading1 = 0.0f;
	Vector3 EnemySpawn2 = {0, 0 ,0};
	float EnemyHeading2 = 0.0f;

	// Generic Variables
	Ped dutch = 0;
	Blip AreaBlip = 0;
	std::vector<Ped> micahs;

	void CommonInit() override;
	void TrackRewards() override;

	bool ActivateObjective_0 = true;
	bool ActivateObjective_1 = false;
	bool ActivateObjective_2 = false;

	bool RunObjective_0 = false;
	bool RunObjective_1 = false;
	bool RunObjective_2 = false;

	void ObjectiveInit_0();
	void ObjectiveInit_1();
	void ObjectiveInit_2();

	void ObjectiveLogic_0();
	void ObjectiveLogic_1();
	void ObjectiveLogic_2();

public:
	TestMission();
	~TestMission();

	void Setup(int campid, const std::string& gangstr, const std::vector<MissionCompanionInfo>& companions) override;

	void Update() override;

	static const MissionStaticData& GetStaticMissionData();
};
