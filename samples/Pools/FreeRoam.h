#pragma once

#include "Common.h"
#include "MissionBase.h"

class FreeRoam : public MissionBase
{
	bool SetMissionParamsForCamp(int camp) override;

	Vector3 CampPoint;
	float CampRadius = 1.0f;

	Blip CampBlip = 0;

	void CommonInit() override;
	void TrackRewards() override;

	bool ActivateObjective_0 = true;
	bool ActivateObjective_1 = false;

	bool RunObjective_0 = false;
	bool RunObjective_1 = false;

	void ObjectiveInit_0();
	void ObjectiveInit_1();

	void ObjectiveLogic_0();
	void ObjectiveLogic_1();

public:
	FreeRoam();
	~FreeRoam();

	void Setup(int campid, const std::string& gangstr, const std::vector<MissionCompanionInfo>& companions) override;

	void Update() override;

	static const MissionStaticData& GetStaticMissionData();
};
