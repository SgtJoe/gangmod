
#include "precomp.h"
#include "MissionController.h"

MissionController::~MissionController()
{
	DioScope(DIOTAG);

	destroyCurrentMission();
}

void MissionController::destroyCurrentMission()
{
	if (_activeMission != nullptr)
	{
		delete _activeMission;
		_activeMission = nullptr;
	}
}

bool MissionController::ActiveMissionHasLawOverride()
{
	if (_activeMission != nullptr)
	{
		return _activeMission->MissionLawOverride;
	}

	return false;
}

bool MissionController::StartMission(MissionType missionid, int campid, const std::string& gangstr,
									const std::vector<MissionCompanionInfo>& companions, std::function<MissionBase*(MissionType)> spawnerFunc)
{
	DioTrace(DIOTAG "missionid %d campid %d", missionid, campid);

	bool bOk = false;

	destroyCurrentMission();

	ActiveMissionFinishedLoading = false;

	_activeMission = spawnerFunc(missionid);

	bOk = HasActiveMission();

	if (bOk)
	{
		_activeMission->Setup(campid, gangstr, companions);
	}

	return bOk;
}

void MissionController::Update()
{
	if (_activeMission == nullptr)
	{
		DioTrace(DIOTAG1("ERROR") "UPDATING NON EXISTING MISSION");
		return;
	}

	if (_activeMission->ExitMissionFlag == M_EXIT_NONE && !_activeMission->TriggeredGenericFailureCondition())
	{
		_activeMission->common_update();
		_activeMission->Update();

		if (_activeMission->FinishedLoadingAssets)
		{
			ActiveMissionFinishedLoading = true;
			_activeMission->FinishedLoadingAssets = false;
		}
	}
	else
	{
		CachedMissionResults = _activeMission->GetMissionResults();
		ServedCachedMissionResults = false;

		destroyCurrentMission();
	}
}

int MissionController::CalculatePlayerCutFromMoneyAmount(int amount)
{
	return int(amount * 0.05f);
}
