#pragma once

#include "Common.h"

class InventoryManager
{
	std::vector<Hash> _inventory;

public:
	InventoryManager();
	~InventoryManager();

	void ListInventory();

	void SetDefaultInventory();

	void AddItem(Hash item);
	void RemoveItem(Hash item);
	bool HasItem(Hash item);
	int GetItemCount(Hash item);
	bool IsItemDefault(Hash item);
	bool IsItemValid(Hash item);

	std::vector<Hash> GetAvailableWeapons(bool longarms);
	std::vector<Hash> GetAvailableArmor();

	Hash GetBestHorse();
	std::vector<Hash> GetAllHorses();

	static StaticWeaponData GetStaticWeaponData(Hash weapon);
	static StaticArmorData GetStaticArmorData(Hash armor);
	static StaticHorseData GetStaticHorseData(Hash horsetype);
};
