
#include "precomp.h"
#include "CampRaidGang.h"

const MissionStaticData& CampRaidGang::GetStaticMissionData()
{
	static MissionStaticData missiondata = {
		MissionType::CAMPRAID_GANG,
		0, 0, 0, 0, 0,
		"CAMP DEFENSE",
		"Your Gang Bounty has attracted the attention of a rival gang!\nPrepare to fight!",
		COMPCOUNT_MEDIUM,
		false,
		MUSIC_SHADYBELLE,
		true
	};
	return missiondata;
}

CampRaidGang::CampRaidGang()
{
	DioScope(DIOTAG);
}

CampRaidGang::~CampRaidGang()
{
	DioScope(DIOTAG);
}

void CampRaidGang::Setup(int campid, const std::string& gangstr, const std::vector<MissionCompanionInfo>& companions)
{
	DioScope(DIOTAG "campid %d gangstr %s", campid, gangstr.c_str());

	GangName = gangstr;
	GangMemberTemplates = companions;

	if (!SetMissionParamsForCamp(campid))
	{
		ExitMissionFlag = M_EXIT_ERROR_TERMINATE;
		return;
	}

	CommonInit();
}

bool CampRaidGang::SetMissionParamsForCamp(int camp)
{
	switch (camp)
	{
		case DISTRICT_GRIZZLIES_WEST:
			GangModel = "G_M_M_UNIMICAHGOONS_01";
			CampPoint = {-922.07f, 1563.32f, 237.43f};
			CampRadius = 18;
			RoadSpawns = {{{-894.09f, 1476.51f, 240.46f}, 35}};
			FootSpawns = {
				{{-919.04f, 1499.44f, 244.43f}, 349},
				{{-889.25f, 1503.64f, 245.29f}, 25},
				{{-869.62f, 1509.33f, 248.15f}, 31}
			};
			FlankingSpawns = {
				{{-866.66f, 1544.62f, 251.39f}, 76},
				{{-959.23f, 1517.06f, 244.68f}, 333}
			};
			SniperSpawnDatas = {{{{-951.40f, 1512.06f, 247.92f}, 326}, {-945.13f, 1520.80f, 250.80f}}};
			return true;

		case DISTRICT_CUMBERLAND_FOREST:
			GangModel = "G_M_M_UNICRIMINALS_01";
			CampPoint = {741.61f, 1192.49f, 142.76f};
			CampRadius = 17;
			RoadSpawns = {{{844.80f, 1155.78f, 145.92f}, 102}};
			FootSpawns = {
				{{829.86f, 1139.08f, 140.12f}, 76},
				{{820.69f, 1119.27f, 134.07f}, 35},
				{{776.08f, 1134.37f, 134.00f}, 53}
			};
			FlankingSpawns = {
				{{758.81f, 1260.00f, 154.39f}, 170}
			};
			SniperSpawnDatas = {{{{845.32f, 1202.45f, 159.72f}, 113}, {845.32f, 1202.45f, 159.72f}}};
			return true;

		case DISTRICT_BLUEGILL_MARSH:
			GangModel = "G_M_M_UNICRIMINALS_01";
			CampPoint = {1953.97f, -478.75f, 40.73f};
			CampRadius = 14;
			RoadSpawns = {{{2027.57f, -434.77f, 41.67f}, 140}};
			FootSpawns = {
				{{2034.08f, -451.37f, 40.90f}, 112},
				{{2029.26f, -460.23f, 40.72f}, 101},
				{{2020.23f, -485.15f, 40.68f}, 124}
			};
			FlankingSpawns = {
				{{1978.07f, -432.75f, 40.88f}, 150}
			};
			SniperSpawnDatas = {{{{1946.36f, -411.56f, 43.76f}, 83}, {1946.36f, -411.56f, 43.76f}}};
			return true;

		case DISTRICT_RIO_BRAVO:
			GangModel = "G_M_M_UNICRIMINALS_01";
			CampPoint = {-4916.46f, -3145.15f, -14.17f};
			CampRadius = 9;
			RoadSpawns = {{{-5009.00f, -3111.81f, -17.52f}, 267}};
			FootSpawns = {
				{{-4934.76f, -3095.29f, -18.40f}, 199},
				{{-4953.01f, -3097.65f, -18.66f}, 203}
			};
			FlankingSpawns = {
				{{-4981.38f, -3124.64f, -16.16f}, 255}
			};
			SniperSpawnDatas = {{{{-4881.84f, -3112.81f, -8.72f}, 137}, {-4881.84f, -3112.81f, -8.72f}}};
			return true;
	}

	DioTrace(DIOTAG1("ERROR") "CANNOT SET MISSION %d PARAMS FOR CAMP %d - ABORTING MISSION", CampRaidGang::GetStaticMissionData().StaticMissionID, camp);
	return false;
}

void CampRaidGang::CommonInit()
{
	const MissionStaticData& thisMissionData = CampRaidGang::GetStaticMissionData();
	SetVarsFromStaticData(thisMissionData);

	MissionAssetsToLoad = {
		{"PED", GangModel}
	};

	AddHorseToMissionAssets(HORSE_ARDENNES);

	ObjectivesList = {
		"~COLOR_YELLOW~Defend your camp"
	};

	LoadAllMissionAssets();
	SpawnGangMembers(thisMissionData.NumMembersToSpawn, true);

	WAIT(2000);
}

void CampRaidGang::ObjectiveInit_0()
{
	ActivateObjective_0 = false;
	RunObjective_0 = true;

	CompanionsClearTasks();
	CompanionsDisband();

	AllowCompanionManualControl = false;

	ENTITY::SET_ENTITY_COORDS(PlayerPed, CampPoint.x, CampPoint.y, CampPoint.z, true, true, true, false);
	
	for (int i = 0; i < SpawnedGangMembers.size(); ++i)
	{
		Vector3 offset = Common::ApplyRandomSmallOffsetToCoords(CampPoint, 3.0f);

		ENTITY::SET_ENTITY_COORDS(SpawnedGangMembers[i].member, offset.x, offset.y, offset.z, true, true, true, false);
		ENTITY::PLACE_ENTITY_ON_GROUND_PROPERLY(SpawnedGangMembers[i].member, true);
	}

	for (const SniperData& s : SniperSpawnDatas)
	{
		PATHFIND::NAVMESH_REQUEST_PATH(0, s.spawn.p.x, s.spawn.p.y, s.spawn.p.z, s.destination.x, s.destination.y, s.destination.z, 23);
	}

#ifndef DEBUG_FAST_MISSION
	AbandonCoords = CampPoint;
	AbandonRadius = CampRadius * 2.5f;
	CheckAbandon = true;
#endif
}

void CampRaidGang::ObjectiveLogic_0()
{
	RunObjective_0 = false;
	ActivateObjective_1 = true;
}

void CampRaidGang::ObjectiveInit_1()
{
	ActivateObjective_1 = false;
	RunObjective_1 = true;

	DisableLaw();

	waveTimer = MISC::GET_GAME_TIMER();

	PlayCompanionSpeech("HUNKER_DOWN");

	for (int i = 0; i < SpawnedGangMembers.size(); ++i)
	{
		Ped gangster = SpawnedGangMembers[i].member;
		
		WEAPON::SET_CURRENT_PED_WEAPON(gangster, WEAPON::GET_BEST_PED_WEAPON(gangster, true, true), true, WEAPON_ATTACH_POINT_HAND_PRIMARY, false, false);

		TASK::TASK_SEEK_COVER_FROM_POS(gangster, RoadSpawns[0].p.x, RoadSpawns[0].p.y, RoadSpawns[0].p.z, 5000, 0, 0, 0);
	}
}

void CampRaidGang::ObjectiveLogic_1()
{
	if (MISC::GET_GAME_TIMER() - waveTimer > 7000)
	{
		RunObjective_1 = false;
		ActivateObjective_2 = true;
	}
}

Ped CampRaidGang::SpawnEnemy(Vector3 pos, float rot, PedType type, bool rider, bool finalWave)
{
	Ped enemy = SpawnPed(GangModel, Common::ApplyRandomSmallOffsetToCoords(pos), rot, 255);
	
	PED::SET_PED_RELATIONSHIP_GROUP_HASH(enemy, REL_PLAYER_ENEMY);
	MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_ENEMY, enemy);

	SetPedType(enemy, type);

	if (rider)
	{
		std::vector<std::string> horseData = InventoryManager::GetStaticHorseData(HORSE_ARDENNES).PedModels;

		Ped horse = SpawnPed(horseData[rand() % horseData.size()], Common::ApplyRandomSmallOffsetToCoords(pos, 5.0f), rot, 255);

		DefaultHorseSetup(horse);
		PED::SET_PED_CONFIG_FLAG(horse, PCF_BlockMountHorsePrompt, true);

		PED::SET_PED_ONTO_MOUNT(enemy, horse, -1, true);
	}

	PED::REGISTER_HATED_TARGETS_AROUND_PED(enemy, 150.0f);
	TASK::TASK_COMBAT_HATED_TARGETS(enemy, 150.0f);

	if (finalWave)
	{
		SpawnedFinalEnemies.emplace_back(enemy);
	}
	else
	{
		SpawnedEnemies.emplace_back(enemy);
	}

	return enemy;
}

Ped CampRaidGang::SpawnEnemySniper(Vector3 pos, float rot, Vector3 target, PedType type)
{
	Ped sniper = SpawnPed(GangModel, pos, rot, 255);

	PED::SET_PED_RELATIONSHIP_GROUP_HASH(sniper, REL_PLAYER_ENEMY);
	Blip sniperblip = MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_ENEMY, sniper);
	MAP::BLIP_ADD_MODIFIER(sniperblip, BLIP_MODIFIER_ENEMY_GUNSHOTS_ONLY);
	MAP::BLIP_ADD_MODIFIER(sniperblip, BLIP_MODIFIER_PING_GUNSHOT);
	MAP::SET_BLIP_SPRITE(sniperblip, MISC::GET_HASH_KEY("blip_region_hunting"), true);
	MAP::_SET_BLIP_NAME_FROM_PLAYER_STRING(sniperblip, "Enemy Sniper");

	SetPedType(sniper, type);

	PED::SET_PED_COMBAT_MOVEMENT(sniper, 0);
	PED::SET_PED_SEEING_RANGE(sniper, 150.0f);

	ENTITY::SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(sniper, true);
	
	SpawnedSniperEnemies.emplace_back(sniper);

	int sequence = 0;
	TASK::OPEN_SEQUENCE_TASK(&sequence);

	TASK::TASK_GO_STRAIGHT_TO_COORD(0, target.x, target.y, target.z, 5.0f, 20000, rot, 1056964608 /* Float: 0.5f */, 0);
	TASK::TASK_PAUSE(0, 1);
	TASK::TASK_COMBAT_HATED_TARGETS(0, 150.0f);

	TASK::CLOSE_SEQUENCE_TASK(sequence);
	TASK::TASK_PERFORM_SEQUENCE(sniper, sequence);
	TASK::CLEAR_SEQUENCE_TASK(&sequence);

	return sniper;
}

void CampRaidGang::SpawnFirstWave()
{
	DioTrace(DIOTAG);
	
	// Sidearm Riders
	for (int i = 0; i < RoadSpawns.size(); ++i)
	{
		SpawnEnemy(RoadSpawns[i].p, RoadSpawns[i].h, PedType::SIDEARM_REGULAR, true);
	}
	
	// On Foot Sidearms + Repeater
	for (int i = 0; i < 3; ++i)
	{
		LocationData spawn = FootSpawns[rand() % FootSpawns.size()];

		SpawnEnemy(spawn.p, spawn.h, (i == 0) ? PedType::REPEATER_WEAK : PedType::SIDEARM_REGULAR, false);
	}
}

void CampRaidGang::SpawnSecondWave()
{
	DioTrace(DIOTAG);

	// Shotgun + Sidearm Riders
	for (int i = 0; i < 2; ++i)
	{
		LocationData spawn = RoadSpawns[rand() % RoadSpawns.size()];

		SpawnEnemy(spawn.p, spawn.h, (i == 0) ? PedType::SHOTGUN_WEAK : PedType::SIDEARM_REGULAR, true);
	}

	// On Foot Repeaters + Sidearms
	for (int i = 0; i < 3; ++i)
	{
		LocationData spawn = FootSpawns[rand() % FootSpawns.size()];

		SpawnEnemy(spawn.p, spawn.h, (i % 2 == 0) ? PedType::SIDEARM_REGULAR : PedType::REPEATER_REGULAR, false);
	}
}

void CampRaidGang::SpawnFinalWave()
{
	DioTrace(DIOTAG);

	// On Foot Repeaters + Shotgun
	for (int i = 0; i < 5; ++i)
	{
		LocationData spawn = FootSpawns[rand() % FootSpawns.size()];

		SpawnEnemy(spawn.p, spawn.h, (i % 2 == 0) ? PedType::SHOTGUN_WEAK : PedType::REPEATER_REGULAR, false, true);
	}

	// Sniper
	SpawnEnemySniper(SniperSpawnDatas[0].spawn.p, SniperSpawnDatas[0].spawn.h, SniperSpawnDatas[0].destination, PedType::SNIPER_WEAK);

	// Flanking Riflemen
	for (int i = 0; i < FlankingSpawns.size(); ++i)
	{
		for (int j = 0; j < 2; ++j)
		{
			LocationData spawn = FlankingSpawns[i];
			Ped flanker = SpawnEnemy(spawn.p, spawn.h, PedType::RIFLE_WEAK, false, false);
			ENTITY::SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(flanker, true);
		}
	}
}

void CampRaidGang::ObjectiveInit_2()
{
	ObjectiveSet(0);
	ActivateObjective_2 = false;
	RunObjective_2 = true;

	CurrentMusicStage = STAGE_ACTION1;

	PlayCompanionSpeech("TAUNT_GEN_GROUP");

	SpawnFirstWave();
	waveTimer = MISC::GET_GAME_TIMER();

	CompanionsBeginCombat(true);
	
	for (int i = 0; i < SpawnedGangMembers.size(); ++i)
	{
		Ped gangster = SpawnedGangMembers[i].member;

		PED::SET_PED_SPHERE_DEFENSIVE_AREA(gangster, CampPoint.x, CampPoint.y, CampPoint.z, CampRadius, 1, false, 0);

		PED::SET_PED_COMBAT_MOVEMENT(gangster, 0);
		PED::SET_PED_COMBAT_ATTRIBUTES(gangster, CA_PERMIT_CHARGE_BEYOND_DEFENSIVE_AREA, false);
		PED::SET_PED_COMBAT_ATTRIBUTES(gangster, CA_CAN_CHARGE, false);
		PED::SET_PED_COMBAT_ATTRIBUTES(gangster, CA_CAN_CHASE_TARGET_ON_FOOT, false);
		PED::SET_PED_COMBAT_ATTRIBUTES(gangster, CA_USE_COVER, true);
		PED::SET_PED_COMBAT_ATTRIBUTES(gangster, CA_SWITCH_TO_ADVANCE_IF_CANT_FIND_COVER, false);
		PED::SET_PED_COMBAT_ATTRIBUTES(gangster, CA_DISABLE_RETREAT_DUE_TO_TARGET_PROXIMITY, true);

		PED::SET_PED_COMBAT_RANGE(gangster, CR_FAR);

		PED::_SET_PED_CROUCH_MOVEMENT(gangster, true, 0, true);
	}
}

void CampRaidGang::ObjectiveLogic_2()
{
	bool allDead = true;

	for (Ped enemy : SpawnedEnemies)
	{
		if (!PED::IS_PED_DEAD_OR_DYING(enemy, true))
		{
			allDead = false;
			break;
		}
	}
	
	if (allDead || (waveTimer > 0 && MISC::GET_GAME_TIMER() - waveTimer > 20000))
	{
		SpawnSecondWave();
		waveTimer = 0;

		RunObjective_2 = false;
		ActivateObjective_3 = true;
	}
}

void CampRaidGang::ObjectiveInit_3()
{
	ActivateObjective_3 = false;
	RunObjective_3 = true;

	CurrentMusicStage = STAGE_ACTION2;

	waveTimer = MISC::GET_GAME_TIMER();
}

void CampRaidGang::ObjectiveLogic_3()
{
	if (waveTimer > 0)
	{
		bool stage3Early = true;

		for (Ped enemy : SpawnedEnemies)
		{
			if (!PED::IS_PED_DEAD_OR_DYING(enemy, true))
			{
				stage3Early = false;
				break;
			}
		}

		if (stage3Early || MISC::GET_GAME_TIMER() - waveTimer > 30000)
		{
			SpawnFinalWave();
			waveTimer = 0;
		}
	}
	else
	{
		bool allDead = true;

		for (Ped finalEnemy : SpawnedFinalEnemies)
		{
			if (!PED::IS_PED_DEAD_OR_DYING(finalEnemy, true))
			{
				allDead = false;
				break;
			}
		}

		if (allDead)
		{
			CurrentMusicStage = STAGE_AFTERACTION;
			UpdateMusic();

			for (int i = 0; i < SpawnedGangMembers.size(); ++i)
			{
				PED::_SET_PED_CROUCH_MOVEMENT(SpawnedGangMembers[i].member, false, 0, false);
			}

			CompanionsClearTasks();
			CompanionsRegroup();

			std::vector<Ped> fleePeds;

			for (Ped oldEnemy : SpawnedEnemies)
			{
				if (!PED::IS_PED_DEAD_OR_DYING(oldEnemy, true))
				{
					fleePeds.emplace_back(oldEnemy);
				}
			}

			for (Ped sniper : SpawnedSniperEnemies)
			{
				if (!PED::IS_PED_DEAD_OR_DYING(sniper, true))
				{
					fleePeds.emplace_back(sniper);
				}
			}

			for (Ped fleer : fleePeds)
			{
				PED::SET_PED_RELATIONSHIP_GROUP_HASH(fleer, REL_CIV);

				Blip dblip = MAP::GET_BLIP_FROM_ENTITY(fleer);
				MAP::REMOVE_BLIP(&dblip);

				TASK::CLEAR_PED_TASKS(fleer, true, true);
				TASK::TASK_FLEE_PED(fleer, PlayerPed, 4, 0, -1082130432 /* Float: -1f */, -1, 0);
			}

			WAIT(1500);

			PlayCompanionSpeech("CALLOUT_FORGET_FLEEING_NEUTRAL");

			WAIT(6000);

			RunObjective_3 = false;
			MissionSuccess();
		}
	}
}

void CampRaidGang::TrackRewards()
{
	RewardBounty_Active = RewardBounty_Potential;
	RewardCash_Active = RewardCash_Potential;
	RewardMembers_Active = RewardMembers_Potential;
}

void CampRaidGang::DoggoLogic()
{
	Ped doggo = 0;

	for (int i = 0; i < SpawnedGangMembers.size(); i++)
	{
		if (PED::IS_PED_MODEL(SpawnedGangMembers[i].member, MISC::GET_HASH_KEY(CampDogModel.c_str())))
		{
			doggo = SpawnedGangMembers[i].member;
			break;
		}
	}

	if (!ENTITY::DOES_ENTITY_EXIST(doggo))
	{
		return;
	}

	std::vector<Ped> potentialTargets;

	for (Ped enemy : SpawnedEnemies)
	{
		if (ENTITY::DOES_ENTITY_EXIST(enemy) && !PED::IS_PED_DEAD_OR_DYING(enemy, true))
		{
			potentialTargets.emplace_back(enemy);
		}
	}
	for (Ped enemy : SpawnedFinalEnemies)
	{
		if (ENTITY::DOES_ENTITY_EXIST(enemy) && !PED::IS_PED_DEAD_OR_DYING(enemy, true))
		{
			potentialTargets.emplace_back(enemy);
		}
	}

	bool hasTarget = false;

	for (Ped target : potentialTargets)
	{
		if (PED::IS_PED_IN_COMBAT(doggo, target) && !PED::IS_PED_DEAD_OR_DYING(target, true))
		{
			hasTarget = true;
			break;
		}
	}

	const float rangeMultiplier = 3.0f;

	if (!hasTarget && Common::GetDistBetweenCoords(CampPoint, ENTITY::GET_ENTITY_COORDS(doggo, true, true)) < CampRadius * rangeMultiplier)
	{
		for (Ped target : potentialTargets)
		{
			if (!PED::IS_PED_DEAD_OR_DYING(target, true) &&
				Common::GetDistBetweenCoords(CampPoint, ENTITY::GET_ENTITY_COORDS(target, true, true)) < CampRadius * rangeMultiplier &&
				!ENTITY::DOES_ENTITY_EXIST(PED::GET_MOUNT(target)) &&
				!PED::IS_PED_IN_ANY_VEHICLE(target, false))
			{
				DIOTRACE_EXTRA(DIOTAG "Assigning new target");
				TASK::TASK_COMBAT_PED(doggo, target, 0, 0);
				PED::SET_PED_CONFIG_FLAG(doggo, PCF_DisableDeadEyeTagging, false);
				break;
			}
		}
	}
	else if (!PED::GET_PED_CONFIG_FLAG(doggo, PCF_DisableDeadEyeTagging, true))
	{
		DIOTRACE_EXTRA(DIOTAG "Returning to camp radius");
		TASK::TASK_GO_STRAIGHT_TO_COORD(doggo, CampPoint, 5.0f, 0, 0, 0, 0);
		PED::SET_PED_CONFIG_FLAG(doggo, PCF_DisableDeadEyeTagging, true);
	}
}

void CampRaidGang::Update()
{
	DoggoLogic();
	
	for (Ped horse : SpawnedMissionEntities)
	{
		if (Common::PedIsHorse(horse) && !ENTITY::DOES_ENTITY_EXIST(PED::_GET_RIDER_OF_MOUNT(horse, true)) && !PED::GET_PED_CONFIG_FLAG(horse, PCF_AlwaysRejectPlayerRobberyAttempt, true))
		{
			TASK::TASK_FLEE_PED(horse, PlayerPed, 4, 0, -1082130432 /* Float: -1f */, -1, 0);
			PED::SET_PED_CONFIG_FLAG(horse, PCF_AlwaysRejectPlayerRobberyAttempt, true);
		}
	}

	if (TimeMissionFail > 0 && MISC::GET_GAME_TIMER() - TimeMissionFail > TimeMissionFailDelay)
	{
		DioTrace("Exit Fail Triggered");

		ExitMissionFlag = M_EXIT_FAILURE;
	}
	else if (TimeMissionSuccess > 0 && MISC::GET_GAME_TIMER() - TimeMissionSuccess > TimeMissionSuccessDelay)
	{
		DioTrace("Exit Win Triggered");

		ExitMissionFlag = M_EXIT_SUCCESS;
	}
	else if (ActivateObjective_0)
	{
		ObjectiveInit_0();
	}
	else if (ActivateObjective_1)
	{
		ObjectiveInit_1();
	}
	else if (ActivateObjective_2)
	{
		ObjectiveInit_2();
	}
	else if (ActivateObjective_3)
	{
		ObjectiveInit_3();
	}
	else if (RunObjective_0)
	{
		ObjectiveLogic_0();
	}
	else if (RunObjective_1)
	{
		ObjectiveLogic_1();
	}
	else if (RunObjective_2)
	{
		ObjectiveLogic_2();
	}
	else if (RunObjective_3)
	{
		ObjectiveLogic_3();
	}
}
