
#include "precomp.h"
#include "InventoryManager.h"

InventoryManager::InventoryManager()
{
	DioScope(DIOTAG);

	SetDefaultInventory();
}

InventoryManager::~InventoryManager()
{
	DioScope(DIOTAG);

	_inventory.clear();
}

void InventoryManager::ListInventory()
{
#ifdef DEBUG_COMMANDS
	if (_inventory.empty())
	{
		DioTrace("INVENTORY EMPTY");
		return;
	}

	DioTrace("INVENTORY:");

	for (int i = 0; i < _inventory.size(); i++)
	{
		DioTrace("	%d - %s", i, Common::HashToStr(_inventory[i]).c_str());
	}
#endif
}

void InventoryManager::SetDefaultInventory()
{
	_inventory = {
		WEAPON_REVOLVER_CATTLEMAN,
		ARMOR_LEVEL_0,
		HORSE_MORGAN
	};

#ifdef DEBUG_INVENTORY_OVERRIDE
	_inventory = {
		WEAPON_REVOLVER_SCHOFIELD,
		WEAPON_REVOLVER_SCHOFIELD,
		WEAPON_PISTOL_VOLCANIC,
		WEAPON_REPEATER_WINCHESTER,
		WEAPON_SHOTGUN_PUMP,
		WEAPON_SNIPERRIFLE_CARCANO,
		ARMOR_LEVEL_3,
		ARMOR_LEVEL_3,
		ARMOR_LEVEL_3,
		ARMOR_LEVEL_3,
		ARMOR_LEVEL_3,
		HORSE_MORGAN
	};
#endif
}

void InventoryManager::AddItem(Hash item)
{
	if (!IsItemValid(item))
	{
		DioTrace(DIOTAG1("WARNING") "Cannot add invalid item %s", Common::HashToStr(item).c_str());
		return;
	}
	
	DioTrace("Adding %s to inventory", Common::HashToStr(item).c_str());
	_inventory.emplace_back(item);
}

void InventoryManager::RemoveItem(Hash item)
{
	std::string itemstr = Common::HashToStr(item);
	
	if (IsItemDefault(item))
	{
		DioTrace(DIOTAG1("WARNING") "Cannot remove default item %s", itemstr.c_str());
		return;
	}

	if (_inventory.empty())
	{
		DioTrace(DIOTAG1("WARNING") "Attempting to remove item %s from empty inventory", itemstr.c_str());
		return;
	}

	for (int i = 0; i < _inventory.size(); i++)
	{
		if (_inventory[i] == item)
		{
			DioTrace("Removing %s from inventory slot %d", itemstr.c_str(), i);
			_inventory.erase(_inventory.begin() + i);
			return;
		}
	}
	
	DioTrace(DIOTAG1("WARNING") "Could not find item %s in inventory", itemstr.c_str());
}

bool InventoryManager::HasItem(Hash item)
{
	return GetItemCount(item) > 0;
}

int InventoryManager::GetItemCount(Hash item)
{
	if (_inventory.empty())
	{
		DioTrace(DIOTAG1("WARNING") "Attempting to count items in empty inventory");
		return 0;
	}

	int count = 0;

	for (int i = 0; i < _inventory.size(); i++)
	{
		if (_inventory[i] == item)
		{
			count++;
		}
	}

	return count;
}

bool InventoryManager::IsItemDefault(Hash item)
{
	bool isDefault = false;
	
	switch (item)
	{
		case WEAPON_REVOLVER_CATTLEMAN:
		case ARMOR_LEVEL_0:
		case HORSE_MORGAN:
			isDefault = true;
			break;

		default:
			isDefault = false;
	}

	return isDefault;
}

bool InventoryManager::IsItemValid(Hash item)
{
	bool isValid = false;

	switch (item)
	{
		case WEAPON_REPEATER_CARBINE:
		case WEAPON_REPEATER_WINCHESTER:
		case WEAPON_SHOTGUN_DOUBLEBARREL:
		case WEAPON_SHOTGUN_PUMP:
		case WEAPON_RIFLE_SPRINGFIELD:
		case WEAPON_RIFLE_BOLTACTION:
		case WEAPON_SNIPERRIFLE_ROLLINGBLOCK:
		case WEAPON_SNIPERRIFLE_CARCANO:

		case WEAPON_REVOLVER_DOUBLEACTION:
		case WEAPON_REVOLVER_SCHOFIELD:
		case WEAPON_PISTOL_VOLCANIC:

		case ARMOR_LEVEL_1:
		case ARMOR_LEVEL_2:
		case ARMOR_LEVEL_3:

		case HORSE_WALKER:
		case HORSE_SADDLER:
		case HORSE_ARDENNES:
		case HORSE_NOKOTA:
		case HORSE_MUSTANG:
		case HORSE_ARABIAN:
		case HORSE_FOXTROTTER:
			isValid = true;
			break;

		default:
			isValid = false;
	}

	return isValid;
}

std::vector<Hash> InventoryManager::GetAvailableWeapons(bool longarms)
{
	std::vector<Hash> guns;

	int count = longarms ? LongarmTierCount : SidearmTierCount;

	for (int i = 0; i < count; i++)
	{
		Hash GunHash = longarms ? LongarmTierList[i] : SidearmTierList[i];
		int AmountToAdd = GetItemCount(GunHash);

		DIOTRACE_EXTRA("adding %d %s", AmountToAdd, Common::HashToStr(GunHash).c_str());

		for (int j = 0; j < AmountToAdd; j++)
		{
			guns.emplace_back(GunHash);
		}
	}

	return guns;
}

std::vector<Hash> InventoryManager::GetAvailableArmor()
{
	std::vector<Hash> armors;

	for (int i = 0; i < ArmorTierCount; i++)
	{
		Hash ArmorHash = ArmorTierList[i];
		int AmountToAdd = GetItemCount(ArmorHash);

		DIOTRACE_EXTRA("adding %d %s", AmountToAdd, Common::HashToStr(ArmorHash).c_str());

		for (int j = 0; j < AmountToAdd; j++)
		{
			armors.emplace_back(ArmorHash);
		}
	}

	return armors;
}

Hash InventoryManager::GetBestHorse()
{
	Hash horse = HORSE_MORGAN;

	for (int i = (HorseTierCount - 1); i >= 0; i--)
	{
		Hash testhorse = HorseTierList[i];

		DIOTRACE_EXTRA("Checking for horse %s", Common::HashToStr(testhorse).c_str());

		if (HasItem(testhorse))
		{
			return testhorse;
		}
	}

	return horse;
}

std::vector<Hash> InventoryManager::GetAllHorses()
{
	std::vector<Hash> horses;

	for (int i = 0; i < HorseTierCount; i++)
	{
		Hash HorseHash = HorseTierList[i];
		
		if (HasItem(HorseHash))
		{
			DIOTRACE_EXTRA("Adding horse %s", Common::HashToStr(HorseHash).c_str());
			horses.emplace_back(HorseHash);
		}
	}

	return horses;
}

StaticWeaponData InventoryManager::GetStaticWeaponData(Hash weapon)
{
	switch (weapon)
	{
		// Default weapon
		case WEAPON_REVOLVER_CATTLEMAN: return { WEAPON_REVOLVER_CATTLEMAN, "Cattleman Revolver", "     ", 10, 35, 0, 0, false };

		// Sidearms
		case WEAPON_REVOLVER_DOUBLEACTION: return { WEAPON_REVOLVER_DOUBLEACTION,	"Doubleaction Revolver","    ",		30, 30, 1000, 600, false };
		case WEAPON_REVOLVER_SCHOFIELD: return { WEAPON_REVOLVER_SCHOFIELD,			"Schofield Revolver","        ",	40, 45, 1500, 900, false };
		case WEAPON_PISTOL_VOLCANIC: return { WEAPON_PISTOL_VOLCANIC,				"Volcanic Pistol","            ",	70, 50, 4000, 1800, false };

		// Longarms
		case WEAPON_REPEATER_CARBINE: return { WEAPON_REPEATER_CARBINE,					"Carbine Repeater", "         ",		35, 60, 2000, 1100, true };
		case WEAPON_REPEATER_WINCHESTER: return { WEAPON_REPEATER_WINCHESTER,			"Lancaster Repeater", "       ",		55, 60, 3500, 1400, true };
		case WEAPON_SHOTGUN_DOUBLEBARREL: return { WEAPON_SHOTGUN_DOUBLEBARREL,			"Double Barrel Shotgun", "   ",			90, 30, 4000, 1900, true };
		case WEAPON_SHOTGUN_PUMP: return { WEAPON_SHOTGUN_PUMP,							"Pump Action Shotgun", "    ",			75, 50, 5000, 2700, true };
		case WEAPON_RIFLE_SPRINGFIELD: return { WEAPON_RIFLE_SPRINGFIELD,				"Springfield Rifle", "           ",		60, 60, 4000, 2500, true };
		case WEAPON_RIFLE_BOLTACTION: return { WEAPON_RIFLE_BOLTACTION,					"Bolt Action Rifle", "           ",		70, 65, 6500, 3200, true };
		case WEAPON_SNIPERRIFLE_ROLLINGBLOCK: return { WEAPON_SNIPERRIFLE_ROLLINGBLOCK,	"Rolling Block Rifle", "        ",		80, 80, 9000, 4400, true };
		case WEAPON_SNIPERRIFLE_CARCANO: return { WEAPON_SNIPERRIFLE_CARCANO,			"Carcano Rifle", "              ",		95, 90, 12000, 5300, true };
	}

	return { 0, "UNKNOWN WEAPON", " ", 0, 0, 0, 0 };
}

StaticArmorData InventoryManager::GetStaticArmorData(Hash armor)
{
	switch (armor)
	{
		case ARMOR_LEVEL_1: return { 180, 2500 };
		case ARMOR_LEVEL_2: return { 370, 4500 };
		case ARMOR_LEVEL_3: return { 600, 7000 };
	}

	return { 90, 0 };
}

StaticHorseData InventoryManager::GetStaticHorseData(Hash horsetype)
{
	switch (horsetype)
	{
		case HORSE_WALKER: return {HORSE_WALKER, "Tennessee Walker", "         ", 9000, 6500,
			{
				"A_C_Horse_TennesseeWalker_BlackRabicano",
				"A_C_Horse_TennesseeWalker_Chestnut",
				"A_C_Horse_TennesseeWalker_DappleBay",
				"A_C_Horse_TennesseeWalker_FlaxenRoan",
				"A_C_Horse_TennesseeWalker_MahoganyBay",
				"A_C_Horse_TennesseeWalker_RedRoan"
			}
		};

		case HORSE_SADDLER: return {HORSE_SADDLER, "Kentucky Saddler", "          ", 13000, 8000,
			{
				"A_C_Horse_KentuckySaddle_Black",
				"A_C_Horse_KentuckySaddle_ChestnutPinto",
				"A_C_Horse_KentuckySaddle_Grey",
				"A_C_Horse_KentuckySaddle_SilverBay"
			}
		};

		case HORSE_ARDENNES: return {HORSE_ARDENNES, "Ardennes", "                   ", 26000, 10000,
			{
				"A_C_Horse_Ardennes_BayRoan",
				"A_C_Horse_Ardennes_IronGreyRoan",
				"A_C_Horse_Ardennes_StrawberryRoan"
			}
		};

		case HORSE_NOKOTA: return {HORSE_NOKOTA, "Nokota", "                      ", 45000, 20000,
			{
				"A_C_Horse_Nokota_BlueRoan",
				"A_C_Horse_Nokota_ReverseDappleRoan",
				"A_C_Horse_Nokota_WhiteRoan"
			}
		};

		case HORSE_MUSTANG: return {HORSE_MUSTANG, "Mustang", "                    ", 120000, 50000,
			{
				//"A_C_HORSE_MUSTANG_BLACKOVERO",
				//"A_C_HORSE_MUSTANG_BUCKSKIN",
				//"A_C_HORSE_MUSTANG_CHESTNUTTOVERO",
				"A_C_Horse_Mustang_GoldenDun",
				"A_C_Horse_Mustang_GrulloDun",
				//"A_C_HORSE_MUSTANG_REDDUNOVERO",
				"A_C_Horse_Mustang_TigerStripedBay",
				"A_C_Horse_Mustang_WildBay"
			}
		};

		case HORSE_ARABIAN: return {HORSE_ARABIAN, "Arabian", "                     ", 250000, 90000,
			{
				"A_C_Horse_Arabian_Black",
				"A_C_Horse_Arabian_Grey",
				"A_C_Horse_Arabian_RedChestnut",
				"A_C_Horse_Arabian_RoseGreyBay"
			}
		};

		case HORSE_FOXTROTTER: return {HORSE_FOXTROTTER, "Fox Trotter", "                  ", 400000, 180000,
			{
				"A_C_Horse_MissouriFoxTrotter_AmberChampagne",
				"A_C_HORSE_MISSOURIFOXTROTTER_BLUEROAN",
				"A_C_HORSE_MISSOURIFOXTROTTER_DAPPLEGREY",
				"A_C_Horse_MissouriFoxTrotter_SableChampagne",
				"A_C_Horse_MissouriFoxTrotter_SilverDapplePinto"
			}
		};
	}
	
	return { HORSE_MORGAN, "Morgan", " ", 0, 0,
		{
			"A_C_Horse_Morgan_Bay",
			"A_C_Horse_Morgan_BayRoan",
			"A_C_Horse_Morgan_FlaxenChestnut",
			"A_C_Horse_Morgan_Palomino"
		}
	};
}
