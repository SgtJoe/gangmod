
#include "precomp.h"
#include "JailbreakChain.h"

const MissionStaticData& JailbreakChain::GetStaticMissionData()
{
	static MissionStaticData missiondata = {
		MissionType::JAILBREAK_CHAIN,
		1000, 5, 2500, 0, 4, 
		"Chain Gang Rescue", 
		"The law has a chain gang working nearby.\nIf we free them, they can be persuaded to join us.", 
		COMPCOUNT_SMALL, 
		false, 
		MUSIC_SEAN_RESCUE,
		false
	};
	return missiondata;
}

JailbreakChain::JailbreakChain()
{
	DioScope(DIOTAG);
}

JailbreakChain::~JailbreakChain()
{
	DioScope(DIOTAG);
}

void JailbreakChain::Setup(int campid, const std::string& gangstr, const std::vector<MissionCompanionInfo>& companions)
{
	DioScope(DIOTAG "campid %d gangstr %s", campid, gangstr.c_str());

	GangName = gangstr;
	GangMemberTemplates = companions;

	if (!SetMissionParamsForCamp(campid))
	{
		ExitMissionFlag = M_EXIT_ERROR_TERMINATE;
		return;
	}

	CommonInit();
}

bool JailbreakChain::SetMissionParamsForCamp(int camp)
{
	bool success = false;

	JailbreakChainData collection1, collection2, collection3;

	switch (camp)
	{
		case DISTRICT_GRIZZLIES_WEST:
			collection1.DebugName = "Rocks";
			collection1.BlipPoint = {-539.30f, 1147.60f, 135.40f};
			collection1.PrisonerSpawns = {
				{{-544.59f, 1147.79f, 135.24f}, 59},
				{{-543.23f, 1149.92f, 135.40f}, 59},
				{{-540.98f, 1154.04f, 135.93f}, 14},
				{{-536.09f, 1162.20f, 139.29f}, 34}
			};
			collection1.OfficerSpawns = {
				{{-547.90f, 1149.05f, 138.73f}, 261}
			};
			collection1.GuardSpawns = {
				{{-541.32f, 1158.83f, 139.72f}, 193},
				{{-542.82f, 1138.37f, 134.64f}, 30},
				{{-538.00f, 1144.53f, 135.20f}, 37},
				{{-552.44f, 1125.97f, 133.78f}, 112},
				{{-539.89f, 1117.56f, 133.29f}, 185},
				{{-519.69f, 1147.23f, 135.77f}, 275}
			};
			collection1.LawSpawn = {-628.87f, 973.16f, 124.77f};

			collection2.DebugName = "Bridge";
			collection2.BlipPoint = {-749.21f, 940.05f, 97.66f};
			collection2.PrisonerSpawns = {
				{{-749.66f, 926.67f, 99.72f}, 167},
				{{-747.04f, 926.12f, 98.84f}, 165},
				{{-736.90f, 933.36f, 95.69f}, 285},
				{{-735.22f, 930.92f, 96.21f}, 277}
			};
			collection2.OfficerSpawns = {
				{{-740.50f, 934.82f, 95.92f}, 246},
				{{-742.67f, 927.40f, 96.78f}, 106}
			};
			collection2.GuardSpawns = {
				{{-758.97f, 946.45f, 100.65f}, 34},
				{{-749.00f, 932.72f, 97.60f}, 179},
				{{-740.81f, 929.50f, 95.62f}, 294},
				{{-731.67f, 920.79f, 91.20f}, 249}
			};
			collection2.LawSpawn = {-852.86f, 1073.72f, 148.06f};

			collection3.DebugName = "Tunnel";
			collection3.BlipPoint = {-976.21f, 676.85f, 96.05f};
			collection3.PrisonerSpawns = {
				{{-973.93f, 689.36f, 95.01f}, 240},
				{{-974.30f, 686.18f, 94.61f}, 252},
				{{-974.44f, 674.13f, 96.88f}, 274},
				{{-974.27f, 681.35f, 96.07f}, 279}
			};
			collection3.OfficerSpawns = {
				{{-978.78f, 690.30f, 94.75f}, 261},
				{{-978.78f, 676.35f, 94.89f}, 300}
			};
			collection3.GuardSpawns = {
				{{-982.68f, 686.54f, 93.97f}, 275},
				{{-979.01f, 682.14f, 93.20f}, 274},
				{{-988.21f, 675.45f, 91.99f}, 128},
				{{-990.93f, 695.14f, 96.24f}, 89},
				{{-980.56f, 654.82f, 95.91f}, 128}
			};
			collection3.LawSpawn = {-968.70f, 830.63f, 122.51f};

			AllAvailableCollections = {collection1, collection2, collection3};
			success = true;
			break;

		case DISTRICT_CUMBERLAND_FOREST:
			collection1.DebugName = "Bacchus Bridge";
			collection1.BlipPoint = {550.88f, 1580.20f, 163.63f};
			collection1.PrisonerSpawns = {
				{{551.80f, 1569.34f, 166.82f}, 138},
				{{548.44f, 1570.24f, 167.30f}, 151},
				{{548.94f, 1591.14f, 163.97f}, 293},
				{{553.25f, 1587.45f, 165.77f}, 344}
			};
			collection1.OfficerSpawns = {
				{{543.65f, 1587.71f, 164.90f}, 311},
				{{550.93f, 1575.58f, 164.64f}, 165}
			};
			collection1.GuardSpawns = {
				{{555.51f, 1560.02f, 174.62f}, 170},
				{{573.41f, 1568.48f, 167.11f}, 236},
				{{554.17f, 1574.50f, 164.43f}, 150},
				{{543.62f, 1585.46f, 165.28f}, 296},
				{{541.98f, 1600.74f, 164.56f}, 6},
				{{555.54f, 1579.13f, 164.00f}, 24}
			};
			collection1.LawSpawn = {663.92f, 1480.73f, 182.41f};

			collection2.DebugName = "Fort Wallace";
			collection2.BlipPoint = {204.88f, 1470.59f, 143.45f};
			collection2.PrisonerSpawns = {
				{{206.89f, 1463.71f, 144.73f}, 171},
				{{202.11f, 1463.43f, 144.60f}, 184},
				{{200.46f, 1464.54f, 144.17f}, 118},
				{{196.60f, 1465.97f, 143.53f}, 166}
			};
			collection2.OfficerSpawns = {
				{{211.68f, 1474.17f, 144.54f}, 153}
			};
			collection2.GuardSpawns = {
				{{212.11f, 1470.36f, 145.13f}, 149},
				{{204.68f, 1472.63f, 143.41f}, 172},
				{{198.53f, 1470.90f, 141.88f}, 198},
				{{190.29f, 1472.67f, 139.23f}, 76},
				{{219.19f, 1455.68f, 149.58f}, 232},
				{{223.57f, 1462.67f, 149.06f}, 244}
			};
			collection2.LawSpawn = {342.14f, 1410.48f, 174.05f};

			AllAvailableCollections = {collection1, collection2};
			success = true;
			break;

		case DISTRICT_BLUEGILL_MARSH:
		case DISTRICT_SCARLETT_MEADOWS:
			collection1.DebugName = "Lonnies Shack";
			collection1.BlipPoint = {1646.97f, -395.91f, 62.56f};
			collection1.PrisonerSpawns = {
				{{1641.77f, -384.73f, 66.80f}, 37},
				{{1643.95f, -383.26f, 66.63f}, 25},
				{{1647.80f, -386.05f, 63.38f}, 87},
				{{1647.96f, -379.79f, 66.62f}, 58}
			};
			collection1.OfficerSpawns = {
				{{1650.54f, -389.00f, 62.49f}, 36},
				{{1640.33f, -379.68f, 70.62f}, 225}
			};
			collection1.GuardSpawns = {
				{{1645.24f, -390.22f, 64.25f}, 16},
				{{1652.30f, -383.52f, 63.22f}, 76},
				{{1636.33f, -390.23f, 65.10f}, 323},
				{{1666.41f, -386.84f, 59.64f}, 207}
			};
			collection1.LawSpawn = {1640.94f, -536.89f, 45.66f};

			collection2.DebugName = "Face Rock";
			collection2.BlipPoint = {1126.71f, -653.85f, 89.98f};
			collection2.PrisonerSpawns = {
				{{1093.93f, -639.80f, 96.37f}, 146},
				{{1094.98f, -645.65f, 96.58f}, 75},
				{{1115.70f, -640.16f, 90.74f}, 68},
				{{1112.51f, -634.39f, 90.60f}, 185}
			};
			collection2.OfficerSpawns = {
				{{1101.49f, -656.17f, 98.87f}, 359}
			};
			collection2.GuardSpawns = {
				{{1095.87f, -651.16f, 97.71f}, 27},
				{{1093.58f, -634.64f, 95.19f}, 187},
				{{1098.21f, -624.66f, 91.50f}, 0},
				{{1110.31f, -630.70f, 90.59f}, 197},
				{{1120.03f, -639.07f, 89.51f}, 102},
				{{1126.51f, -652.55f, 90.08f}, 241}
			};
			collection2.LawSpawn = {1263.46f, -729.40f, 72.98f};

			AllAvailableCollections = {collection1, collection2};
			ScriptsKillList = {MISC::GET_HASH_KEY("lonniesshack")};
			success = true;
			break;

		case DISTRICT_RIO_BRAVO:
			collection1.DebugName = "Gaptooth Ridge";
			collection1.BlipPoint = {-5299.09f, -3017.41f, -6.55f};
			collection1.PrisonerSpawns = {
				{{-5297.30f, -3013.48f, -6.42f}, 297},
				{{-5294.19f, -3019.88f, -5.89f}, 330},
				{{-5300.91f, -3016.11f, -6.42f}, 87},
				{{-5302.07f, -3019.61f, -6.38f}, 57}
			};
			collection1.OfficerSpawns = {
				{{-5301.61f, -3009.24f, -6.42f}, 214}
			};
			collection1.GuardSpawns = {
				{{-5298.64f, -3008.52f, -6.44f}, 183},
				{{-5300.37f, -2995.42f, -6.52f}, 1},
				{{-5302.18f, -3023.71f, -6.40f}, 1},
				{{-5298.31f, -3022.27f, -6.45f}, 354},
				{{-5305.48f, -3037.30f, -5.11f}, 168}
			};
			collection1.LawSpawn = {-5487.40f, -3077.56f, -4.06f};

			collection2.DebugName = "Armadillo";
			collection2.BlipPoint = {-4010.20f, -2708.80f, -7.62f};
			collection2.PrisonerSpawns = {
				{{-4007.00f, -2705.61f, -8.31f}, 300},
				{{-4007.21f, -2707.68f, -7.77f}, 273},
				{{-4005.52f, -2711.78f, -7.89f}, 314},
				{{-4003.29f, -2713.71f, -8.38f}, 323}
			};
			collection2.OfficerSpawns = {
				{{-4014.32f, -2719.97f, 0.48f}, 312}
			};
			collection2.GuardSpawns = {
				{{-4013.43f, -2709.67f, -7.50f}, 289},
				{{-4006.00f, -2720.99f, -8.22f}, 339},
				{{-3996.48f, -2728.60f, -10.09f}, 226},
				{{-4002.49f, -2696.57f, -10.41f}, 13}
			};
			collection2.LawSpawn = {-3844.23f, -2677.47f, -15.12f};

			AllAvailableCollections = {collection1, collection2};
			success = true;
			break;
	}

	if (success)
	{
		ChosenCollection = AllAvailableCollections[rand() % AllAvailableCollections.size()];
		ActivePursuitParams.ScriptedLawSpawnPoints = {ChosenCollection.LawSpawn};
	}
	else
	{
		DioTrace(DIOTAG1("ERROR") "CANNOT SET MISSION %d PARAMS FOR CAMP %d - ABORTING MISSION", JailbreakChain::GetStaticMissionData().StaticMissionID, camp);
	}

	return success;
}

void JailbreakChain::CommonInit()
{
	const MissionStaticData& thisMissionData = JailbreakChain::GetStaticMissionData();
	SetVarsFromStaticData(thisMissionData);

	SetGenericLawModels(Common::GetDistrictFromCoords(ChosenCollection.BlipPoint));

	MissionAssetsToLoad = {
		{"PED", "A_M_M_SKPPRISONER_01"},
		{"SCN", "WORLD_HUMAN_PICKAXE_WALL"},
		{"SCN", "WORLD_HUMAN_STERNGUY_IDLES"},
		{"SCN", "WORLD_HUMAN_GUARD_MILITARY"}
	};

	AddHorseToMissionAssets(HORSE_WALKER);

	ObjectivesList = {
		"Go to the ~COLOR_YELLOW~location",
		"Eliminate the ~COLOR_RED~guards",
		"Lose the ~COLOR_RED~law"
	};

	LoadAllMissionAssets();
	SpawnGangMembers(thisMissionData.NumMembersToSpawn);

	WAIT(2000);
}

void JailbreakChain::ObjectiveInit_0()
{
	ObjectiveSet(0);
	ActivateObjective_0 = false;
	RunObjective_0 = true;
	
	AreaBlip = ObjectiveBlipCoords(ChosenCollection.BlipPoint, 50.0f, true, ObjectivesList[0]);

	for (const LocationData& data : ChosenCollection.PrisonerSpawns)
	{
		Ped prisoner = SpawnPed("A_M_M_SKPPRISONER_01", data.p, data.h, 255);

		PED::SET_PED_RELATIONSHIP_GROUP_HASH(prisoner, REL_IGNORE_EVERYTHING);

		SetPedType(prisoner, PedType::SIDEARM_EXPERT);
		WEAPON::REMOVE_ALL_PED_WEAPONS(prisoner, true, true);

		EVENT::SET_DECISION_MAKER(prisoner, MISC::GET_HASH_KEY("EMPTY"));

		PED::SET_PED_LASSO_HOGTIE_FLAG(prisoner, 0, false);
		PED::SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(prisoner, false);

		int sequence = 0;
		TASK::OPEN_SEQUENCE_TASK(&sequence);

		TASK::TASK_STAND_STILL(0, (rand() % 7) * 1000);
		TASK::TASK_START_SCENARIO_AT_POSITION(0, MISC::GET_HASH_KEY("WORLD_HUMAN_PICKAXE_WALL"), data.p, data.h, -1, false, true, 0, 0, false);

		TASK::CLOSE_SEQUENCE_TASK(sequence);
		TASK::TASK_PERFORM_SEQUENCE(prisoner, sequence);
		TASK::CLEAR_SEQUENCE_TASK(&sequence);

		SpawnedPrisoners.emplace_back(prisoner);
	}

	for (const LocationData& data : ChosenCollection.OfficerSpawns)
	{
		Ped officer = SpawnPed(GenericLawLeader, data.p, data.h, 255);

		PED::SET_PED_RELATIONSHIP_GROUP_HASH(officer, REL_IGNORE_EVERYTHING);

		SetPedType(officer, PedType::SIDEARM_REGULAR);

		TASK::TASK_START_SCENARIO_AT_POSITION(officer, MISC::GET_HASH_KEY("WORLD_HUMAN_STERNGUY_IDLES"), data.p, data.h, -1, false, true, 0, 0, false);

		SpawnedGuards.emplace_back(officer);
	}

	for (const LocationData& data : ChosenCollection.GuardSpawns)
	{
		Ped guard = SpawnPed(GenericLawFollower, data.p, data.h, 255);

		PED::SET_PED_RELATIONSHIP_GROUP_HASH(guard, REL_IGNORE_EVERYTHING);

		SetPedType(guard, PedType::REPEATER_WEAK);

		TASK::TASK_START_SCENARIO_AT_POSITION(guard, MISC::GET_HASH_KEY("WORLD_HUMAN_GUARD_MILITARY"), data.p, data.h, -1, false, true, 0, 0, false);

		SpawnedGuards.emplace_back(guard);
	}

#ifdef DEBUG_FAST_MISSION
	ENTITY::_SET_ENTITY_COORDS_AND_HEADING(PlayerPed, ChosenCollection.LawSpawn,
		MISC::GET_HEADING_FROM_VECTOR_2D(ChosenCollection.BlipPoint.x - ChosenCollection.LawSpawn.x, ChosenCollection.BlipPoint.y - ChosenCollection.LawSpawn.y), true, true, true);
	ENTITY::PLACE_ENTITY_ON_GROUND_PROPERLY(PlayerPed, true);
	CAM::SET_GAMEPLAY_CAM_RELATIVE_HEADING(0, 1);
	CAM::SET_GAMEPLAY_CAM_RELATIVE_PITCH(0, 1);
#endif
}

void JailbreakChain::ObjectiveLogic_0()
{
	if (Common::GetDistBetweenCoords(PlayerCoords, ChosenCollection.BlipPoint) < 50.0f)
	{
		MAP::REMOVE_BLIP(&AreaBlip);

		RunObjective_0 = false;
		ActivateObjective_1 = true;
	}
}

void JailbreakChain::ObjectiveInit_1()
{
	ObjectiveSet(1);
	ActivateObjective_1 = false;
	RunObjective_1 = true;

	DisableLaw();

	CheckAbandon = true;
	AbandonCoords = ChosenCollection.BlipPoint;
	AbandonRadius = 55.0f;

	Common::ClearArea(ChosenCollection.BlipPoint, 80.0f);

	for (Ped guard : SpawnedGuards)
	{
		MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_ENEMY, guard);
		PED::SET_PED_RELATIONSHIP_GROUP_HASH(guard, REL_PLAYER_ENEMY);

		PED::SET_PED_SEEING_RANGE(guard, 25.0f);

		PED::SET_PED_CONFIG_FLAG(guard, PCF_DisableLadderClimbing, true);
	}

	for (Ped prisoner : SpawnedPrisoners)
	{
		PED::SET_PED_RELATIONSHIP_GROUP_HASH(prisoner, REL_CIV);

		PED::SET_PED_CONFIG_FLAG(prisoner, PCF_DisableLadderClimbing, true);
	}

	AllowCompanionManualControl = true;
}

void JailbreakChain::ObjectiveLogic_1()
{
	if (!GuardsAlerted &&
		(
			FIRE::IS_EXPLOSION_IN_SPHERE(-1, ChosenCollection.BlipPoint, 50.0f) ||
			PED::IS_ANY_PED_SHOOTING_IN_AREA(ChosenCollection.BlipPoint.x - 100, ChosenCollection.BlipPoint.y - 100, ChosenCollection.BlipPoint.z - 100,
				ChosenCollection.BlipPoint.x + 100, ChosenCollection.BlipPoint.y + 100, ChosenCollection.BlipPoint.z + 100, false, false)
		)
	)
	{
		DioTrace("Guards alerted by gunfire or explosion");
		GuardsAlerted = true;
	}

	if (!GuardsAlerted && GuardSpotTime == 0)
	{
		for (Ped guard : SpawnedGuards)
		{
			bool spottedplayer = PED::CAN_PED_SEE_ENTITY(guard, PlayerPed, true, true) == 1;

			if (GuardSpotTime == 0 && PED::IS_PED_IN_COMBAT(guard, PlayerPed))
			{
				DioTrace("Guards alerted by player in combat");
				GuardsAlerted = true;
				spottedplayer = true;
				GuardSpotTime = MISC::GET_GAME_TIMER();
			}

			if (GuardSpotTime == 0 && spottedplayer)
			{
				DioTrace("Guard spotted player");
				GuardSpotTime = MISC::GET_GAME_TIMER();

				ScriptedSpeechParams params{"OPENS_FIRE", 0, 0, MISC::GET_HASH_KEY("SPEECH_PARAMS_BEAT_SHOUTED_CLEAR_NOSUB"), 0, true, 0, 0};
				AUDIO::PLAY_PED_AMBIENT_SPEECH_NATIVE(guard, (Any*)&params);

				TASK::CLEAR_PED_TASKS(guard, true, true);
				TASK::TASK_AIM_AT_ENTITY(guard, PlayerPed, 9000, 0, 0);
			}
		}
	}
	else if (!GuardsAlerted && MISC::GET_GAME_TIMER() - GuardSpotTime > 2000)
	{
		DioTrace("Guards alerted by spotted player");
		GuardsAlerted = true;
	}
	else if (!GuardsCombat && GuardsAlerted)
	{
		DioTrace("Guards in combat");
		GuardsCombat = true;

		AllowCompanionManualControl = false;
		CompanionsBeginCombat();

		for (Ped guard : SpawnedGuards)
		{
			PED::SET_PED_SEEING_RANGE(guard, 150.0f);

			TASK::CLEAR_PED_TASKS(guard, true, true);

			TASK::TASK_COMBAT_HATED_TARGETS(guard, 100);
		}

		for (Ped prisoner : SpawnedPrisoners)
		{
			TASK::_TASK_INTIMIDATED_2(prisoner, PLAYER::PLAYER_PED_ID(), rand() % 3, true, true, true, false, false, 0);
		}
		
		CurrentMusicStage = STAGE_ACTION1;
	}

	for (Ped guard : SpawnedGuards)
	{
		if (PED::GET_PED_CONFIG_FLAG(guard, PCF_DisableLadderClimbing, true) && PED::IS_PED_DEAD_OR_DYING(guard, true))
		{
			DioTrace("Guard down");
			PED::SET_PED_CONFIG_FLAG(guard, PCF_DisableLadderClimbing, false);
			GuardCasualties++;
		}
	}

	if (!PrisonersAttack && GuardCasualties >= 4)
	{
		PrisonersAttack = true;

		DioTrace("Prisoners attacking");

		for (Ped prisoner : SpawnedPrisoners)
		{
			Blip pBlip = MAP::BLIP_ADD_FOR_ENTITY(BLIP_STYLE_COMPANION, prisoner);
			MAP::_SET_BLIP_NAME_FROM_PLAYER_STRING(pBlip, "Prisoner");

			PED::SET_PED_RELATIONSHIP_GROUP_HASH(prisoner, REL_GANG_DUTCHS);

			TASK::CLEAR_PED_TASKS(prisoner, true, true);

			PED::SET_PED_COMBAT_MOVEMENT(prisoner, 3);
			PED::SET_PED_COMBAT_ATTRIBUTES(prisoner, CA_DISABLE_FLEE_FROM_COMBAT, true);
			PED::SET_PED_COMBAT_ATTRIBUTES(prisoner, CA_CAN_FIGHT_ARMED_PEDS_WHEN_NOT_ARMED, true);
			PED::SET_PED_COMBAT_ATTRIBUTES(prisoner, CA_DISABLE_BULLET_REACTIONS, true);

			PED::_SET_PED_BRAWLING_STYLE(prisoner, MISC::GET_HASH_KEY("BS_BRUISER"));

			TASK::TASK_COMBAT_HATED_TARGETS(prisoner, 50);
		}

		CurrentMusicStage = STAGE_ACTION2;
	}
	
	if (GuardCasualties >= SpawnedGuards.size())
	{
		RunObjective_1 = false;
		ActivateObjective_2 = true;
	}
}

void JailbreakChain::ObjectiveInit_2()
{
	ObjectiveSet(2);
	ActivateObjective_2 = false;
	RunObjective_2 = true;

	CompanionsClearTasks();
	CompanionsRegroup();

	AllowWanted = true;
	BecomeWanted(CRIME_JAIL_BREAK, 2);
	SetDispatchService(false);
	ActivePursuitParams.DispatchResetDelay = -1;
	ActivePursuitParams.CompanionCombatOnSpotted = true;
	ActivePursuitParams.MusicStageOnSpotted = STAGE_ACTION2;
	WantedZoneStaysWithPlayer = false;

	for (const Vector3& spawn : ActivePursuitParams.ScriptedLawSpawnPoints)
	{
		CreateScriptedRespondingLawPed(GenericLawLeader, PedType::SIDEARM_REGULAR, HORSE_WALKER, spawn, ChosenCollection.BlipPoint);

		for (int j = 0; j < 5; ++j)
		{
			CreateScriptedRespondingLawPed(GenericLawFollower, PedType::REPEATER_REGULAR, HORSE_WALKER, spawn, ChosenCollection.BlipPoint);
		}
	}

	for (Ped prisoner : SpawnedPrisoners)
	{
		PED::SET_PED_RELATIONSHIP_GROUP_HASH(prisoner, REL_CIV); 
		TASK::CLEAR_PED_TASKS(prisoner, true, true);
		TASK::TASK_SMART_FLEE_COORD(prisoner, ChosenCollection.LawSpawn, 1000, -1, 0, 5);
		
		Blip delblip = MAP::GET_BLIP_FROM_ENTITY(prisoner);
		MAP::REMOVE_BLIP(&delblip);
	}

	for (Ped prisoner : SpawnedPrisoners)
	{
		if (!PED::IS_PED_DEAD_OR_DYING(prisoner, true))
		{
			ScriptedSpeechParams params{"COMBAT_FLEE_CALL_OUT", 0, 0, MISC::GET_HASH_KEY("SPEECH_PARAMS_BEAT_SHOUTED_CLEAR_NOSUB"), 0, true, 0, 0};
			AUDIO::PLAY_PED_AMBIENT_SPEECH_NATIVE(prisoner, (Any*)&params);
			break;
		}
	}

	CheckAbandon = false;

	AllowCompanionManualControl = true;
}

void JailbreakChain::ObjectiveLogic_2()
{
	if (PlayerIncognito && Common::GetDistBetweenCoords(ChosenCollection.BlipPoint, PlayerCoords) > 150)
	{
		ClearWanted(false);
	}

	if (ActivePursuitParams.AllScriptedPedsKilled)
	{
		ClearWanted(false);
	}

	if (!WantedLogic())
	{
		CurrentMusicStage = STAGE_AFTERACTION;
		UpdateMusic(); 
		
		RunObjective_3 = false;
		MissionSuccess();
	}
}

void JailbreakChain::TrackRewards()
{
	RewardBounty_Active = PlayerIncognito ? int(RewardBounty_Potential * 0.25f) : RewardBounty_Potential;

	RewardCash_Active = RewardCash_Potential;
	RewardMembers_Active = RewardMembers_Potential;
}

void JailbreakChain::Update()
{
	CLOCK::SET_CLOCK_TIME(12, 0, 0);
	VEHICLE::DELETE_ALL_TRAINS();
	
	for (Ped prisoner : SpawnedPrisoners)
	{
		if (PED::GET_PED_CONFIG_FLAG(prisoner, PCF_DisableLadderClimbing, true) && PED::IS_PED_DEAD_OR_DYING(prisoner, true))
		{
			DioTrace("Prisoner down");
			PED::SET_PED_CONFIG_FLAG(prisoner, PCF_DisableLadderClimbing, false);
			PrisonerCasualties++;
		}
	}

	if (PrisonerCasualties >= 4 && TimeMissionFail < 0)
	{
		MissionFailStr = "All prisoners were killed.";
		MissionFail();
	}

	if (TimeMissionFail > 0 && MISC::GET_GAME_TIMER() - TimeMissionFail > TimeMissionFailDelay)
	{
		DioTrace("Exit Fail Triggered");

		ExitMissionFlag = M_EXIT_FAILURE;
	}
	else if (TimeMissionSuccess > 0 && MISC::GET_GAME_TIMER() - TimeMissionSuccess > TimeMissionSuccessDelay)
	{
		DioTrace("Exit Win Triggered");

		ExitMissionFlag = M_EXIT_SUCCESS;
	}
	else if (ActivateObjective_0)
	{
		ObjectiveInit_0();
	}
	else if (ActivateObjective_1)
	{
		ObjectiveInit_1();
	}
	else if (ActivateObjective_2)
	{
		ObjectiveInit_2();
	}
	else if (RunObjective_0)
	{
		ObjectiveLogic_0();
	}
	else if (RunObjective_1)
	{
		ObjectiveLogic_1();
	}
	else if (RunObjective_2)
	{
		ObjectiveLogic_2();
	}
}
