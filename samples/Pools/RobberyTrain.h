#pragma once

#include "Common.h"
#include "MissionBase.h"

class RobberyTrain : public MissionBase
{
	bool SetMissionParamsForCamp(int camp) override;

	struct TrainData
	{
		Vector3 AmbushPoint;
		Vector3 TrainSpawn;
		bool ReverseDirection;
	};

	std::vector<TrainData> AllAvailableVariants;
	TrainData ChosenVariant = {};

	const std::string TRAIN_NAME = "gunslinger3_config"; 

	static const int TRAIN_MODELS_SIZE = 8;
	const std::string TRAIN_MODELS[TRAIN_MODELS_SIZE] = {
		"PRIVATESTEAMER01X",
		"PRIVATECOALCAR01X",
		"PRIVATEBOXCAR04X",
		"PRIVATEFLATCAR01X",
		"PRIVATEBOXCAR01X",
		"PRIVATEPASSENGER01X",
		"PRIVATEDINING01X",
		"CABOOSE01X"
	};

	enum TrainCarIndex
	{
		TRAINCAR_0_LOCOMOTIVE,
		TRAINCAR_1_COAL,
		TRAINCAR_2_BOXOPEN,
		TRAINCAR_3_FLAT,
		TRAINCAR_4_BOXCLOSED,
		TRAINCAR_5_BOXCLOSED,
		TRAINCAR_6_PASSENGER,
		TRAINCAR_7_PASSENGER,
		TRAINCAR_8_BAR,
		TRAINCAR_9_CABOOSE,
		TRAINCAR_AMOUNT
	};

	Blip AmbushBlip = 0;
	Blip TrainBlip = 0;

	Prompt StopDriverPrompt1 = 0;
	Prompt StopDriverPrompt2 = 0;

	int StopDriverPromptStage = 0;

	const float InitialTrainSpeed = 12.2f;
	float ActiveTrainSpeed = InitialTrainSpeed;
	
	int TrainHijackTime = 0;
	int PromptEnableTime = 0;
	int PromptRespondTime = 0;

	bool TrainHaltingPlayer = false;
	bool TrainHaltingDriver = false;
	bool PlayerLeavingTrain = false;

	Vehicle TrainHandle = 0;
	Entity Locomotive = 0;
	Entity TrainCars[TRAINCAR_AMOUNT];

	struct TrainGuard
	{
		int car;
		Vector2 offset;
		float heading;
		PedType weapons;
	};

	const std::vector<TrainGuard> GuardSpawns = {
		{TRAINCAR_2_BOXOPEN, {-0.92f, 3.96f}, 75, PedType::SHOTGUN_REGULAR},
		{TRAINCAR_2_BOXOPEN, {0.76f, -3.76f}, 265, PedType::SIDEARM_WEAK},
		{TRAINCAR_3_FLAT, {-1.22f, 2.78f}, 255, PedType::REPEATER_EXPERT},
		{TRAINCAR_3_FLAT, {1.22f, 0.44f}, 100, PedType::REPEATER_EXPERT},
		{TRAINCAR_3_FLAT, {-0.58f, -5.10f}, 85, PedType::SIDEARM_WEAK},
		{TRAINCAR_9_CABOOSE, {-0.84f, 1.82f}, 75, PedType::RIFLE_EXPERT},
		{TRAINCAR_9_CABOOSE, {0.00f, -0.20f}, 270, PedType::SIDEARM_WEAK},
		{TRAINCAR_9_CABOOSE, {0.00f, -3.30f}, 180, PedType::RIFLE_EXPERT},
	};

	Ped Conductor = 0;
	std::vector<Ped> Passengers;

	int PassengersRobbed = 0;
	int PassengersKilled = 0;
	bool AllPassengersRobbed = false;

	void PopulatePassengers(int car, Vector3 offset);

	void RobberyLogic();

	void DisableTrainControls(bool disable);

	void CommonInit() override;
	void TrackRewards() override;

	bool ActivateObjective_0 = true;
	bool ActivateObjective_1 = false;
	bool ActivateObjective_2 = false;
	bool ActivateObjective_3 = false;

	bool RunObjective_0 = false;
	bool RunObjective_1 = false;
	bool RunObjective_2 = false;
	bool RunObjective_3 = false;

	void ObjectiveInit_0();
	void ObjectiveInit_1();
	void ObjectiveInit_2();
	void ObjectiveInit_3();

	void ObjectiveLogic_0();
	void ObjectiveLogic_1();
	void ObjectiveLogic_2();
	void ObjectiveLogic_3();

#ifdef DEBUG_MARKERS
	bool helper = false;
	int helpercar = TRAINCAR_9_CABOOSE;
	int mode = 0;
	float offset[3] = {0, 0, 0};
#endif

public:
	RobberyTrain();
	~RobberyTrain();

	void Setup(int campid, const std::string& gangstr, const std::vector<MissionCompanionInfo>& companions) override;

	void Update() override;

	static const MissionStaticData& GetStaticMissionData();
};
