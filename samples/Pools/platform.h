#ifndef __PLATFORM_H__
#define __PLATFORM_H__

// C runtime
//
#include <inttypes.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <assert.h>

// C++ runtime
//
#include <chrono>
#include <mutex>
#include <thread>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <fstream>
#include <functional>

// Windows API
//
#define STRICT 1
#define WIN32_LEAN_AND_MEAN

#include <sdkddkver.h>
#include <windows.h>
#include <WinError.h>

// Scripthook SDK
#include "natives.h"
#include "types.h"
#include "enums.h"


// DIO
//
#include <dio_api.h>

#endif
