#pragma once

#include "platform.h"

//#define DEBUG_COMMANDS
//#define DEBUG_FADE
//#define DEBUG_MARKERS
//#define DEBUG_DAT_OVERRIDE
//#define DEBUG_INVENTORY_OVERRIDE
//#define DEBUG_FAST_CAMP
//#define DEBUG_FAST_MISSION
//#define DEBUG_FRIENDLY_FIRE
//#define DEBUG_NO_ALLIES
//#define DEBUG_OP_ALLIES
//#define DEBUG_CAMERA
//#define DEBUG_CAMERA_LOG
//#define DEBUG_PROP_PLACEMENT

#define MONEY_MULTIPLIER 3

//#define DEBUG_EXTRA_TRACE

#ifdef DEBUG_EXTRA_TRACE
#define DIOTRACE_EXTRA DioTrace
#else
#define DIOTRACE_EXTRA(...) ((void)0)
#endif

namespace Common
{
	std::string Format(const char* fmt, ...);

	std::string HashToStr(Hash h);

	inline std::string BoolToStr(bool b) { return b ? "TRUE" : "FALSE"; };

	inline void SetPedHonorModifier(Ped ped, int honortype) { DECORATOR::DECOR_SET_INT(ped, "honor_override", honortype); }

	inline bool PedIsHorse(Ped p) { return PED::_IS_THIS_MODEL_A_HORSE(ENTITY::GET_ENTITY_MODEL(p)); }

	inline bool IsDarkOut() { return CLOCK::GET_CLOCK_HOURS() > 18 || CLOCK::GET_CLOCK_HOURS() < 6; }

	void SetEntityProofs(Entity entity, bool fire, bool explosion);

	void SetDoorState(Hash door, int state);
	
	int GetDistrictFromCoords(Vector3 coords);

	void ClearArea(const Vector3& area, float radius);

	float GetDistBetweenCoords(const Vector3& c1, const Vector3& c2);

	Vector3 ApplyRandomSmallOffsetToCoords(Vector3 v, float multiplier = 1.0f);

	inline float WorldFloatToMarkerScale(float radius) { return radius / 0.497f; };

	void DrawDebugCircle(const Vector3& position, float radius, float height, const std::string& color, bool bob = false, bool upsidedown = false);

	void DrawDebugRectangle(const Vector3& position, const Vector3& rotation, const Vector3& scale, const std::string& color, bool bob = false, bool upsidedown = false);
}

namespace INI
{
	std::string InputKeyToStr(int key);

	bool DoesIniExist();

	int GetCompanionControlKey();

	std::vector<std::string> GetDutchGangBlock();
	void SetDutchGangBlock();
	void ListDutchGangBlock();

	std::string GetCustomGangFixedPed(int fPedIndex);
	std::vector<std::string> GetCustomGangGenericPeds();
}

namespace DUTCH_HELPERS
{
	std::string GetFixedPed(int fpedIndex);

	std::vector<std::string> GetAmbientPeds();
	
	std::vector<std::string> GetHeistPeds();

	void SetPedAppearance(Ped p, std::string pedName, bool snow, bool robbery);

	std::string RenameFriendlyToPed(const std::string& str);
	std::string RenamePedToFriendly(const std::string& str);

	bool StringValid(const std::string& str);

	std::string ConvertCommentString(const std::string& str);
}

const char DEFAULT_PED[] = "CS_genstorymale";

enum GangIDs
{
	GANG_NONE = -1,
	GANG_ODRISCOLLS,
	GANG_EXCONFEDS,
	GANG_WAPITI,
	GANG_SKINNERS,
	GANG_MEXICANS,
	GANG_DUTCH,
	GANG_CUSTOM,
	GANG__MAXCOUNT
};

enum FixedPedType
{
	FPED_MANAGER = 0,
	FPED_ARMORY = 1,
	FPED_HORSE = 2,
	FPED_MISSION = 3,
	FPED__MAX = 4
};

struct LocationData
{
	Vector3 p;
	float h;
};

enum class MissionType
{
	INVALID = -1,
	TESTMISSION,
	TESTMISSION2,
	JAILBREAK_WAGON,
	JAILBREAK_CHAIN,
	JAILBREAK_TOWN,
	JAILBREAK_SISIKA,
	ROBBERY_COACH,
	ROBBERY_STORE,
	ROBBERY_HOME,
	ROBBERY_TRAIN,
	CAMPRAID_GANG,
	CAMPRAID_LAW,
	CAMPRAID_PINKERTONS,
	FREEROAM,
	STRONGHOLD,
	TOTAL_MISSIONS
};

struct MissionCompanionInfo
{
	std::string CompanionModel;
	int CompanionSkin;
	std::vector<std::string> CompanionVoices;
	bool IsHuman;
	int CompanionAccuracy;
	int CompanionHealth;
	Hash CompanionSidearm;
	Hash CompanionLongarm;
	Hash CompanionArmor;
	int CompanionUniqueID;
};

struct MissionResults
{
	MissionType MissionResultID;
	int MissionResultStatus;
	std::string MissionFailStr;
	bool FailScreenActive;
	std::vector<MissionCompanionInfo> Casualties;
	int RewardBounty;
	int RewardCash;
	int RewardMembers;
};

struct MissionStaticData
{
	MissionType StaticMissionID;
	int RequiredFee;
	int RequiredMembers;
	int PotentialRewardBounty;
	int PotentialRewardCash;
	int PotentialRewardMembers;
	std::string StaticMissionName;
	std::string StaticMissionDescription;
	int NumMembersToSpawn;
	bool UseRobberyPeds;
	int MusicType;
	bool OverrideCampLaw;
};

enum MissionExitFlags
{
	M_EXIT_NONE = -1,
	M_EXIT_SUCCESS = 0,
	M_EXIT_FAILURE = 1,
	M_EXIT_FAILURE_GEN_PLAYER_DIED = 2,
	M_EXIT_FAILURE_GEN_PLAYER_ARRESTED = 3,
	M_EXIT_FAILURE_GEN_ENTERED_CAMP = 4,
	M_EXIT_FAILURE_GEN_MISSION_STARTED = 5,
	M_EXIT_FAILURE_LAW_ALERTED = 6,
	M_EXIT_FAILURE_ABANDONED_OBJECTIVE = 7,
	M_EXIT_ERROR_TERMINATE = 8
};

enum CompanionStatus
{
	COMP_OK,
	COMP_DEAD_NOTPROCESSED,
	COMP_DEAD,
	COMP_DEAD_FIRE,
	COMP_LOOTED
};

enum CompanionCount
{
	COMPCOUNT_MIN = 2,
	COMPCOUNT_SMALL = 3,
	COMPCOUNT_MEDIUM = 4,
	COMPCOUNT_LARGE = 5,
	COMPCOUNT_MAX = 7,
};

const std::string CampDogModel = "A_C_DogHusky_01";

const int LongarmTierCount = 8;
const int SidearmTierCount = 3;

extern const Hash LongarmTierList[LongarmTierCount];

extern const Hash SidearmTierList[SidearmTierCount];

const int ArmorTierCount = 3;
extern const Hash ArmorTierList[ArmorTierCount];

const int HorseTierCount = 7;
extern const Hash HorseTierList[HorseTierCount];

struct StaticArmorData
{
	int health;
	int buy;
};

struct StaticWeaponData
{
	Hash hash;
	std::string name;
	std::string spacing;
	int damage;
	int accuracy;
	int buy;
	int sell;
	bool longarm;
};

struct StaticHorseData
{
	Hash hash;
	std::string name;
	std::string spacing;
	int buy;
	int sell;
	std::vector<std::string> PedModels;
};

enum HonorModifiers
{
	HONOR_NONE = 0,
	HONOR_POSITIVE = -1,
	HONOR_NEGATIVE = 1
};
